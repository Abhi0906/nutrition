/*This is for first guage meter for Calorie Log*/   
        
 /*End of Guage Meter of Calorie Log*/
 /*This is for first guage meter for body Weight*/
        $('.gauge1').gauge({
            values: {
                0 : '0',
                20: '2',
                40: '4',
                60: '6',
                80: '8',
                100: '10'
            },
            colors: {
                /*0 : '#666',
                9 : '#378618',
                60: '#ffa500',
                80: '#f00'*/
                0 : '#003d99',
                40 : '#33cc33',
                70: '#ff1a1a',
                100: '#f00'
            },
            angles: [
                180,
                360
            ],
            lineWidth: 10,
            arrowWidth: 5,
            arrowColor: '#ccc',
            inset:true,

            value: 75
        });


        // Second Guage Meter for body Fat Percentage
        $('.gauge2').gauge({
            values: {
                0 : '0',
                20: '2',
                40: '4',
                60: '6',
                80: '8',
                100: '10'
            },
            colors: {
                /*0 : '#666',
                9 : '#378618',
                60: '#ffa500',
                80: '#f00'*/
                0 : '#003d99',
                40 : '#33cc33',
                70: '#ff1a1a',
                100: '#f00'
            },
            angles: [
                180,
                360
            ],
            lineWidth: 10,
            arrowWidth: 5,
            arrowColor: '#ccc',
            inset:true,

            value: 50
        });

        // Third Guage Meter for body Measurements
        $('.gauge3').gauge({
            values: {
                0 : '0',
                20: '2',
                40: '4',
                60: '6',
                80: '8',
                100: '10'
            },
            colors: {
                /*0 : '#666',
                9 : '#378618',
                60: '#ffa500',
                80: '#f00'*/
                0 : '#003d99',
                40 : '#33cc33',
                70: '#ff1a1a',
                100: '#f00'
            },
            angles: [
                180,
                360
            ],
            lineWidth: 10,
            arrowWidth: 5,
            arrowColor: '#ccc',
            inset:true,

            value: 50
        });

        // Guage Meter for body Measurements for second container
        $('.gauge2-1').gauge({
            values: {
                0 : '0',
                20: '2',
                40: '4',
                60: '6',
                80: '8',
                100: '10'
            },
            colors: {
                /*0 : '#666',
                9 : '#378618',
                60: '#ffa500',
                80: '#f00'*/
                0 : '#003d99',
                40 : '#33cc33',
                70: '#ff1a1a',
                100: '#f00'
            },
            angles: [
                180,
                360
            ],
            lineWidth: 10,
            arrowWidth: 5,
            arrowColor: '#ccc',
            inset:true,

            value: 50
        });

        // second Guage Meter for body Measurements
        $('.gauge2-2').gauge({
            values: {
                0 : '0',
                20: '2',
                40: '4',
                60: '6',
                80: '8',
                100: '10'
            },
            colors: {
                /*0 : '#666',
                9 : '#378618',
                60: '#ffa500',
                80: '#f00'*/
                0 : '#003d99',
                40 : '#33cc33',
                70: '#ff1a1a',
                100: '#f00'
            },
            angles: [
                180,
                360
            ],
            lineWidth: 10,
            arrowWidth: 5,
            arrowColor: '#ccc',
            inset:true,

            value: 50
        });

        // Third Guage Meter for body Measurements
        $('.gauge2-3').gauge({
            values: {
                0 : '0',
                20: '2',
                40: '4',
                60: '6',
                80: '8',
                100: '10'
            },
            colors: {
                /*0 : '#666',
                9 : '#378618',
                60: '#ffa500',
                80: '#f00'*/
                0 : '#003d99',
                40 : '#33cc33',
                70: '#ff1a1a',
                100: '#f00'
            },
            angles: [
                180,
                360
            ],
            lineWidth: 10,
            arrowWidth: 5,
            arrowColor: '#ccc',
            inset:true,

            value: 50
        });