angular.module('app.core',['ngAnimate','elif','ngSanitize','ngTagsInput','ngCookies','ui.bootstrap', 'ui.calendar', 'ui.bootstrap.datetimepicker'])
.config(function(tagsInputConfigProvider) {
    tagsInputConfigProvider.setDefaults('tagsInput', {
        placeholder: ''
    });
});