 $("#contact_msg").hide();
 $("#contact").validate({
 	 
          rules: {
            name: {
              required: true,
              minlength: 3
            },
            email: {
              required: true,
              email: true
            }
          },
           messages: {
            name: "Please enter min. 3 letters",
            email: {
              required: "We need your email address to contact you",
              email: "Please enter a valid email address."
            }
          },
        submitHandler: function (form) {
           
    			var name = $('#name').val();
    			var email = $('#email').val();
    			var message = $('#message').val();
    			var post_data ={name:name,email:email,message:message};
    		 	HTTPRequest(post_data, 'post','/pages/send_email', function(datum, status, xhr){                    if(status == 'success'){
    			                   	if(datum.error == false){  
    			                  		$("#contact_msg").show();
                                setTimeout(function() { $("#contact_msg").fadeOut(); }, 5000);
                                $('#name').val('');
                                $('#email').val('');
                                $('#message').val('');
    		                    	}
    		              		}
    		                });	

    		        }
        });
        


