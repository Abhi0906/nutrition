var myApp = angular.module('authLogin');



myApp.controller('SignupCtrl', function (auth, $scope, $location, store,StoreService) {
  $scope.email = '';
  $scope.password = '';

  function onLoginSuccess(profile, token) {
   
   // $scope.message.text = '';
   console.log(profile);
    store.set('profile', profile);
    store.set('token', token);
    //$location.path('/');
  // $scope.loading = false;

  StoreService.save('api/v3/auth/login',profile).then(function(result) {
          if(result.error == false){
             window.location.href = '/dashboard';
          }
        },function(error) {
          console.log(error);
        });

  }

  function onLoginFailed() {
    
    //$scope.message.text = 'invalid credentials';
    //$scope.loading = false;
    console.log("fail");
  }

  $scope.reset = function() {
    auth.reset({
      email: 'hello@bye.com',
      password: 'hello',
      connection: 'Username-Password-Authentication'
    });
  }

  $scope.submit = function () {
    //$scope.message.text = 'loading...';
    //$scope.loading = true;
   
    auth.signup({
      connection: 'Username-Password-Authentication',
      username: $scope.email,
      password: $scope.password,
      authParams: {
        scope: 'openid name email'
      }
    }, onLoginSuccess, onLoginFailed);

  };

  $scope.doGoogleAuthWithPopup = function () {
    $scope.message.text = 'loading...';
    $scope.loading = true;

    auth.signin({
      popup: true,
      connection: 'google-oauth2',
      scope: 'openid name email'
    }, onLoginSuccess, onLoginFailed);
  };

});


