var auth0 = new auth0.WebAuth({
    domain:       Auth0Domain,
    clientID:     Auth0ClientID,
    redirectUri:  Auth0CallbackUrl,
    responseType: 'code',
    state: Auth0State
   // callbackOnLocationHash: true
  });
  /*auth0.checkSession({
    nonce: '1234',
    responseType: 'token id_token'
  }, function (err, authResult) {
      if (err) return console.log(err);
      if(authResult.accessToken){
        window.location = 'https://'+Auth0Domain+'/authorize?client_id='+Auth0ClientID+'&redirect_uri='+Auth0CallbackUrl+'&response_type=code&state='+window.location.href+'&scope=openid%20profile';
      }
  });
 //console.log(Auth0Domain);
 //console.log(Auth0ClientID);
 //console.log(Auth0State);
/*auth0.getSSOData(function (err, ssoData) {
  console.log(ssoData.sso);
  console.log(err);
    if (err) return console.log(err.message);
    if(ssoData.sso){
      window.location = "/dashboard";
    }
  });*/

var myApp = angular.module('authLogin', [
  'ngCookies','ngRoute', 'app.services'
]);
var user_email="";
var user_password="";

myApp.directive('showFocus', function($timeout) {
  return function(scope, element, attrs) {
    scope.$watch(attrs.showFocus,
      function (newValue) {
        $timeout(function() {
            newValue && element.focus();
        });
      },true);
  };
});

myApp.controller('AuthController', function ($scope, $location,$timeout,StoreService,$routeParams,ShowService) {



  var vm = this;
  vm.invalidCredentials = false;
  vm.loginLoader =false;
  vm.resetLoader =false;
  vm.resetServerError = false;
  vm.sentLinkValidation = false;
  vm.loadLoginPage = true;
  vm.loadSignUpPage = false;
  vm.loadResetPasswordPage = false;
  vm.autofocus = true;

  vm.authSignupError = false;
  vm.user_id=null;
  vm.signupLoader = false;
  vm.profileLoader = false;


  vm.displaySignUp = function(){
     vm.loadLoginPage = false;
     vm.loadSignUpPage = true;
     vm.loadRestPasswordPage = false;
      $('.back-top').trigger('click');

  }

  vm.displayResetPassword = function(){

     vm.loadResetPasswordPage = true;
     vm.loadLoginPage = false;
     vm.loadSignUpPage = false;
     console.log("dsdfasdfa");
      $('.back-top').trigger('click');

  }

  vm.displayLoginPage= function(){

     vm.loadLoginPage = true;
     vm.loadSignUpPage = false;
     vm.loadResetPasswordPage = false;
      $('.back-top').trigger('click');
  }

  vm.submit = function (e) {
    e.preventDefault();
    vm.loginLoader =true;
    auth0.login({
      realm: 'Username-Password-Authentication',
      username: vm.email,
      password: vm.password
    });
    $timeout(function() {
      vm.loginLoader =false;
      vm.invalidCredentials = true;
    },30000);
  };

  vm.sendResetLink = function(e){

      e.preventDefault();
      var data = {'email':vm.email};
      StoreService.save('api/v3/password/email',data).then(function(result) {
        console.log(result);
         if(result.error == false){

            vm.sentLinkValidation = false;
            vm.sentLinkSuccess = true;
            vm.sentLinkSuccessMessage = result.response.message;
           // window.location.href = "/sentpassword";
        }else{
         vm.sentLinkValidation = true;
         vm.sentLinkSuccess = false;
         vm.sentLinkErrorMessage = result.response.message;
      }
      },function(error) {
        console.log(error);
      });

  }

  vm.registerUser = function (e) {

      vm.signupLoader = true;
      e.preventDefault();
      var data = {};
      data = vm.user;
      data.from_server=1;
      user_email = vm.user.email;
      user_password = vm.user.password;
      //console.log(data); return false;
      StoreService.save('api/v3/patient/store',data).then(function(result) {

          if(result.error == false){

              auth0.login({
                connection: 'Username-Password-Authentication',
                username: user_email,
                password: user_password
              });

           }else{
            vm.authSignupError = true;
            vm.signupLoader = false;

          }
        },function(error) {
          console.log(error);
        });

  }



});

// myApp.controller('SignupController', function ($scope, $location, store,StoreService,$routeParams) {

//   var vm = this;
//   vm.authSignupError = false;
//   vm.user_id=null;
//   vm.signupLoader = false;
//   vm.profileLoader = false;

//   vm.submit = function (e) {
//     vm.signupLoader = true;
//     e.preventDefault();
//     var data = {};
//     data = vm.user;
//     data.from_server=1;
//     user_email = vm.user.email;
//     user_password = vm.user.password;
//     StoreService.save('api/v3/patient/store',data).then(function(result) {

//         if(result.error == false){
//           vm.user_id = result.response.user_id;
//           vm.signupLoader = false;
//           $location.url('profile/'+vm.user_id);
//          }else{
//           vm.authSignupError = true;
//           vm.signupLoader = false;

//         }
//       },function(error) {
//         console.log(error);
//       });

//  }

//  vm.updateProfile = function() {

//      vm.profileLoader = true;
//      var data = {};
//      data = vm.about_data;
//      data.user_id = $routeParams.user_id;
//      data.birth_date = moment($("#birth_date").val()).format('YYYY-MM-DD');
//      var years = moment().diff(data.birth_date, 'years');
//      data.activity_id=1
//      data.activity_value =1.2
//      if(years > 13){
//         vm.loder_about = !vm.loder_about;
//         StoreService.save('api/v3/patient/updateProfile',data).then(function(result) {
//         if(result.error == false){
//          //  $location.url('login');

//           auth0.login({
//             connection: 'Username-Password-Authentication',
//             username: user_email,
//             password: user_password
//           });

//         }

//         },function(error) {
//           console.log(error);
//         });
//     }
//     else{
//            vm.ageValidation = true;
//            vm.profileLoader = false;
//     }

//  }


// });

myApp.controller('ResetController', function ($scope, $location,ShowService,$window,$timeout,StoreService) {

  var vm = this;
  vm.user ={};
  vm.resetLoader =false;
  vm.resetServerError = false;
   vm.user.token=null;


   vm.resetPassword = function(e) {
       e.preventDefault();
        vm.resetLoader =true;
        //console.log(vm.user);return false;

        StoreService.save('api/v3/password/reset', vm.user).then(function(result) {

              if (result.error == false) {
                  if (result.status) {
                     window.location.href = '/auth/login';
                    //  vm.resetLoader =false;
                      // vm.message = "Password reset successfull!"
                      // $timeout(function() {
                      //   $location.url('login');
                      // }, 3000);
                  }
              } else {
                vm.resetLoader =false;
                vm.resetServerError = true;
                vm.resetErrorMsg = result.status;
                console.log(result);
              }
          }, function(error) {
              console.log(error);
          });
   }

    vm.createPassword = function(e) {
       e.preventDefault();
        vm.resetLoader =true;


         StoreService.save('api/v3/password/create', vm.user).then(function(result) {

              if (result.error == false) {
                      vm.resetLoader =false;
                       window.location.href = '/auth/login';

              } else {
                vm.resetLoader =false;
                vm.resetServerError = true;
                vm.resetErrorMsg = result.status;
                console.log(result);
              }
          }, function(error) {
              console.log(error);
          });
    }

    vm.getEmailid = function(){

        var token = vm.user.token;
        ShowService.get('api/v3/user/email_address',token).then(function(results){
         if(results.error == false){
           vm.user.email = results.response.reset_data.email;
         }
       });
    }


});
