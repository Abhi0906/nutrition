var auth0 = new Auth0({
    domain:       Auth0Domain,
    clientID:     Auth0ClientID,
    callbackURL:  Auth0CallbackUrl,
   // callbackOnLocationHash: true
  });

auth0.getSSOData(function (err, ssoData) {
    if (err) return console.log(err.message);
    if(ssoData.sso){
      window.location = "/dashboard";
    }
  });
var myApp = angular.module('authLogin', [
  'ngCookies','ngRoute', 'angular-storage', 'angular-jwt','app.services'
]);

myApp.config(function ($routeProvider, $httpProvider,
  $locationProvider, jwtInterceptorProvider) {
 
  $routeProvider
  .when('/logout',  {
    template: '',
    controller: 'LogoutController'
  })
  .when('/login',   {
    templateUrl: 'loadLogin',
    controller: 'LoginController',
    controllerAs: "login"
  })
  .when('/signup', {
    templateUrl: 'loadSignup',
    controller: 'SignupController',
    controllerAs: "signup"
 
  })
  .when('/profile/:user_id', {
    templateUrl: 'loadProfile',
    controller: 'SignupController',
    controllerAs: "profile"
 
  })
  .when('/reset', {
    templateUrl: 'loadReset',
    controller: 'LoginController',
    controllerAs: "reset"
 
  })
  .when('/reset_password/:token', {
    templateUrl: 'loadResetPassword',
    controller: 'LoginController',
    controllerAs: "resetpassword"
 
  })
  .when('/create_password/:token', {
    templateUrl: 'loadCreatePassword',
    controller: 'LoginController',
    controllerAs: "createpassword"
 
  })

  .otherwise({
        redirectTo: '/login'
  });

  



  //$locationProvider.hashPrefix('login');

  /*authProvider.init({
    domain: Auth0Domain,
    clientID: Auth0ClientID,
    sso: true,
    loginUrl: '/login'

  });

  authProvider.on('loginSuccess', function($location, profilePromise, idToken, store,StoreService) {
     
    profilePromise.then(function(profile) {
    

     store.set('profile', profile);
     store.set('token', idToken);
     
      StoreService.save('api/v3/auth/login',profile).then(function(result) {
          if(result.error == false){
             window.location.href = '/dashboard';
          }
        },function(error) {
          console.log(error);
    });

    });
  });


 
  jwtInterceptorProvider.tokenGetter = function(store) {
    return store.get('token');
  }

  // Add a simple interceptor that will fetch all requests and add the jwt token to its authorization header.
  // NOTE: in case you are calling APIs which expect a token signed with a different secret, you might
  // want to check the delegation-token example
  $httpProvider.interceptors.push('jwtInterceptor');
}).run(function($rootScope, auth, store, jwtHelper, $location) {
  $rootScope.$on('$locationChangeStart', function() {
    if (!auth.isAuthenticated) {
      var token = store.get('token');
      if (token) {
        if (!jwtHelper.isTokenExpired(token)) {
          auth.authenticate(store.get('profile'), token);
        } else {
          $location.path('/login');
        }
      }
    }

  });*/
});
