angular
    .module('app.services')
    .factory('StoreService', dataService);

function dataService($http,$log) {

    var data = {
        'save': save,
        'update': update

    };
    function makeRequest(url,data,method) {
        var requestUrl =  '/' + url;
        return $http({
            'url': requestUrl,
            'method': method,
             data:data,
            'headers': {
                'Content-Type': 'application/json'
            },
            'cache': true
        }).then(function(response){
            return response.data;
        }).catch(dataServiceError);
    }
    function save(url,data) {
        return makeRequest(url,data,'POST');
    }
    function update (url,id,data){
        return makeRequest(url+'/'+id, data,'PUT');
    }

    return data;

    function dataServiceError(errorResponse) {
        $log.error('XHR Failed for ShowService');
        $log.error(errorResponse);
        return errorResponse;
    }
}