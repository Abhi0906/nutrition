angular
.module('app.services')
.factory('utility',function(){
	var data = {
        'calculateMetricCalories': calculateMetricCalories,
        'calculateUsCalories': calculateUsCalories,
        'recursiveSearch': recursiveSearch,
        'calculateFoodCalories': calculateFoodCalories,
        'dateCompareValidation': dateCompareValidation,
        'getServingRange': getServingRange,
        'getDefaultServingRange':getDefaultServingRange
    };

    function calculateMetricCalories(gender, weight, height, age, activity){
    	var bmr = 0;
    	if(gender == 'Male'){
    		bmr = 10 * weight +6.25 * height - 5 * age + 5;
    	}else if(gender == 'Female'){
    		bmr = 10 * weight + 6.25 * height - 5 * age - 161;
    	}

    	if(!!activity){
    		bmr = bmr * parseFloat(activity);
    	}
    	return parseInt(bmr);
    };

    function calculateUsCalories(gender, weight, height_feet, height_inches, age, activity){
    	var weight_kg = parseFloat(parseFloat(weight) / 2.20462);
    	var height_cm = parseFloat((parseFloat(height_feet)*12 + parseFloat(height_inches))*2.54);

    	return calculateMetricCalories(gender, weight_kg, height_cm, age, activity);
    };

    function recursiveSearch(serving_value){

       //  var serving_quantity=0;
         if (serving_value.toLowerCase().indexOf("container") >= 0) {
            
            var arr = serving_value.split("(");
            if (arr.length > 1) {
                var innerString = arr[1].trim();
                if (innerString.indexOf("/") >= 0) {
                    return recursiveSearch(innerString);
                }else{
                    var space_arr = innerString.split(" ");
                   return recursiveSearch(space_arr[0]);
                }
            }
        }else if (serving_value.indexOf("/") >= 0) {
            
            var arr = serving_value.split("/")
            if (arr.length > 1) {
                var innerString = arr[0].trim();
                var arrInnerCompo = innerString.split(" ");
                if (arrInnerCompo.length == 2) {
                    var firstString = arrInnerCompo[0].trim();
                    var secondString = arrInnerCompo[1].trim();
                    var arr1FirstComponent= arr[1].trim();
                    var thirdString = Number(arr1FirstComponent.split(" ")[0]);
                    // console.log("firstString",firstString);
                    // console.log("secondString",secondString);
                    // console.log("thirdString",thirdString);
                    var dividevalue = Number(firstString) + Number(secondString) / Number(thirdString) ; 
                   return recursiveSearch(dividevalue.toString());

                }else{

                     var arrInner = innerString.split(" ");
                     var firstString = Number(arrInner[arrInner.length-1]);
                     var secondStringArr= arr[1].trim();
                     var secondString = Number(secondStringArr.split(" ")[0]);
                     var thirdString = firstString/secondString;
                     return recursiveSearch(thirdString.toString());

                }
                
            }
            
        }else{

            var final_value = serving_value.trim();
            var splitString = final_value.split(" ")[0]
            return Number(splitString);
        }
      
    }

    function calculateFoodCalories(no_of_servings,nutrition_obj,serving_size,serving_quantity){

        var default_calories = nutrition_obj.calories;
        var serving_size_value = Number(serving_size/serving_quantity);
        var update_calories = Number(default_calories)*Number(no_of_servings)*Number(serving_size_value);
        return update_calories;

    }

    function dateCompareValidation(startDate, endDate) {
        var errMessage_date = '';

        var curDate = new Date();

        if(new Date(startDate) > new Date(endDate)){
          errMessage_date = 'end_date';
          return errMessage_date;
        }
        
        if(new Date(startDate) < curDate){
           errMessage_date = 'start_date';
           return errMessage_date;
        }
    }

    function getServingRange(start, end, step){

        var range = [];
        var typeofStart = typeof start;
        var typeofEnd = typeof end;

        if (step === 0) {
            throw TypeError("Step cannot be zero.");
        }

        if (typeofStart == "undefined" || typeofEnd == "undefined") {
            throw TypeError("Must pass start and end arguments.");
        } else if (typeofStart != typeofEnd) {
            throw TypeError("Start and end arguments must be of same type.");
        }

        typeof step == "undefined" && (step = 1);

        if (end < start) {
            step = -step;
        }

        if (typeofStart == "number") {

            while (step > 0 ? end >= start : end <= start) {
                range.push(start);
                start += step;
            }

        } else if (typeofStart == "string") {

            if (start.length != 1 || end.length != 1) {
                throw TypeError("Only strings with one character are supported.");
            }

            start = start.charCodeAt(0);
            end = end.charCodeAt(0);

            while (step > 0 ? end >= start : end <= start) {
                range.push(String.fromCharCode(start));
                start += step;
            }

        } else {
            throw TypeError("Only string and number types are supported");
        }

        return range;


    }

    function getDefaultServingRange(user_gender){
        var serving_range = 5;
        //console.log("user_gender",user_gender);
        if(user_gender == 'Female'){
            serving_range =3;
        }
        return serving_range;
    }

    return data;
});