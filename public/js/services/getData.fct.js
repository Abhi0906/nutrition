angular
    .module('app.services')
    .factory('ShowService', dataService);

function dataService($http,$log) {
    var data = {
        'get': get,
        'search': search,
        'remove':remove,
        'query':query,
    };
    function makeRequest(url, params,method) {
        var requestUrl = '/' + url;
        if(params != null){
           var requestUrl = '/' + url+'?page=1';
        }
        angular.forEach(params, function(value, key){
            requestUrl = requestUrl + '&' + key + '=' + value;
        });
        return $http({
            'url': requestUrl,
            'method': method,
            'headers': {
                'Content-Type': 'application/json'
            },
            'cache': false
        }).then(function(response){
            return response.data;
        }).catch(dataServiceError);
    }
    function get(url,id) {
        return makeRequest(url+'/' + id, null,'GET');
    }
    function search(url,params,page) {
        return makeRequest(url,params ,'GET').then(function(data){
            return data;
        });
    }
    function query(url){

       return makeRequest(url, null,'GET');
    }
    function remove(url,id){
        return makeRequest(url+'/' + id, null,'DELETE');
    }
    return data;

    function dataServiceError(errorResponse) {
        $log.error('XHR Failed for ShowService');
        $log.error(errorResponse);
        return errorResponse;
    }
}