angular
.module('app.services')
.factory('EHRService',function($http,$log){
	var data = {
        'getPatientInfo': getPatientInfo
        
    };

    function getPatientInfo(patientEmail,serviceName){

        console.log(patientEmail);
        var obj ={};
         obj.params ={accessId: "genemedics",
          patientEmail:patientEmail,
          timestamp:"Mon 2008 Aug 2016 12 26 17 GMT"};
            obj.secretKey ='Password11!';
            obj.appName = 'GenemedicsEHR';
            obj.serviceName = serviceName;
            obj.method='GET';
            obj.host = 'emr.genemedics.com';
            obj.port = '8080';
            obj.ssl = true;

       return  callVistaService(obj);

       
    	
    };

    function callbackFromVista(data){
            console.log(data);
            return data;
        }
    
    function escape(string, encode) {
        if (encode === "escape") {
            var unreserved = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.~';
            var escString = '';
            var c;
            var hex;
            for (var i=0; i< string.length; i++) {
                c = string.charAt(i);
                if (unreserved.indexOf(c) !== -1) {
                    escString = escString + c;
                }
                else {
                    hex = string.charCodeAt(i).toString(16).toUpperCase();
                    //console.log(string + "; c=" + c + "; hex = " + hex);
                    if (hex.length === 1) hex = '0' + hex;
                    escString = escString + '%' + hex;
                }
            }
            return escString;
        }
        else {
            var enc = encodeURIComponent(string);
            return enc.replace(/\*/g, "%2A").replace(/\'/g, "%27").replace(/\!/g, "%21").replace(/\(/g, "%28").replace(/\)/g, "%29");
        }
    }

    function createStringToSign(action, includePort, encodeType) {
        var stringToSign;
        var name;
        var amp = '';
        var value;
        var keys = [];
        var index = 0;
        var pieces;
        var host = action.host;
        if (!includePort) {
            if (host.indexOf(":") !== -1) {
                pieces = host.split(":");
                host = pieces[0];
            }
        }
        var url = action.uri;
        var method = 'GET'; // should be action.method {some error in ewdjs};
        stringToSign = method + '\n' + host + '\n' + url + '\n';
        for (name in action.query) {
            if (name !== 'signature') {
                keys[index] = name;
                index++;
            }
        }
        keys.sort();
        for (var i=0; i < keys.length; i++) {
            name = keys[i];
            value = action.query[name];
            //console.log("name = " + name + "; value = " + value);
            stringToSign = stringToSign + amp + escape(name, encodeType) + '=' + escape(value, encodeType);
            amp = '&';
        }
        return stringToSign;
    }

    function digest(string, key, type) {
        // type = sha1|sha256|sha512
        //var hmac = crypto.createHmac(type, key.toString());
        var hash = CryptoJS.HmacSHA256(string, key.toString());
        var hashInBase64 = CryptoJS.enc.Base64.stringify(hash);
        //hmac.update(string);
        //return hmac.digest('base64');
        return hashInBase64;
    }

    function callVistaService(obj){

        var params = obj.params;
        var secretKey = obj.secretKey;
        var appName = obj.appName;
        var serviceName = obj.serviceName;
        var method = obj.method || 'GET';
        var post_data = '';
        var amp;
        var name;
        var timeout = obj.timeout || 120000;
        if (method === 'POST' || method === 'PUT') {
            post_data = obj.post_data;
        }
        var path = '/' + appName + '/' + serviceName;
        if (typeof obj.ewdjs === 'undefined') obj.ewdjs = true;
        if (obj.ewdjs) {
            path = '/json/' + appName + '/' + serviceName;
        }
        else {
            path = '/' + obj.params.rest_path;
            amp = '?';
            for (name in obj.params) {
                if (name !== 'accessId' && name.indexOf('rest_') === -1) {
                    path = path + amp + name + '=' + obj.params[name];
                    amp = '&';
                }
            }
         }
        var options = {
            hostname: obj.host,
            port: obj.port,
            method: method,
            path: path,
            agent: false,
            data: post_data,
            rejectUnauthorized: false // set this to true if remote site uses proper SSL certs
        };
        if (obj.ewdjs && secretKey) {
            var uri = options.path;
            params.timestamp = new Date().toUTCString();
            var amp = '?';
            for (name in params) {
                options.path = options.path + amp + escape(name, 'uri') + '=' + escape(params[name], 'uri');
                amp = '&';
            }
            var action = {
                host: options.hostname,
                query: params,
                uri: uri,
                method: options.method
            };
       // console.log(action); return false;
            var stringToSign = createStringToSign(action, false, "uri");
            var hash = digest(stringToSign, secretKey, 'sha256');
          //  alert(hash);
            options.path = options.path + '&signature=' + escape(hash, 'uri');        }
        var req = 'http';
        if (obj.ssl) {
            req = 'https';
        }
        var requestUrl = req + '://' + options.hostname + options.path;
        if (obj.returnUrl) {
            return requestUrl;
        }
       

        return $http({
            'url': requestUrl,
            'method':'GET',
            'headers': {
                'Content-Type': 'application/json'
            },
            'cache': false
          
        }).then(function(response){
             console.log("abhishek",response);
            return response.data;
        }).catch(dataServiceError);

    }
   
    return data;

     function dataServiceError(errorResponse) {
        $log.error('XHR Failed for ehrServices');
        $log.error(errorResponse);
        return errorResponse;
    }
});