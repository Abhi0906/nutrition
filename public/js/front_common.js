// Flash Messages

function showFlashMessage(title,text,type){
    swal({
        title: title,
        text: text,
        type: type,
        timer: 3000,
        showConfirmButton: false
    });
}

function showFlashMessageOverlay(title,text,type){
    swal({
        title: title,
        text: text,
        type: type,
        confirmButtonText: 'OK'
    });
}

function showFlashMessageConfirmation(title,text,type,callback){
    swal({
            title: title,
            text: text,
            type: type,
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        },
        callback
    );
}