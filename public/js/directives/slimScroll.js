angular.module('app.core')
.directive('slimScroll',function($timeout,$rootScope){


	function configureSlimScroll(scope,element,attrs){
        
        function setSlimScroll(){
            $(element).slimScroll({destroy: true});
            $(element).slimScroll({

                height: $(window).height() - $(element)[0].getBoundingClientRect().top - 30
            }); 
            if('lowMargin' in attrs){
                console.log($(element).closest('.slimScrollDiv'));
                $(element).closest('.slimScrollDiv').css('margin-left','-18px');
                $(element).closest('.slimScrollDiv').css('margin-right','-18px');
            }   
        }   
        $(window).resize(function(){ $timeout(function(){setSlimScroll();},500) });
        $(window).bind('orientationchange', setSlimScroll);
        $timeout(function(){setSlimScroll();},500)
        $rootScope.$on('resetScroll', function (e,args) {       
                $timeout(function(){setSlimScroll();},500);
        });
        
    }
    return {
        restrict: 'A',
		link:configureSlimScroll
	};
});


	   