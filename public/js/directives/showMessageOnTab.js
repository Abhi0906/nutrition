angular.module('authLogin')
   .directive('showMessageOnTab', [function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
            elem.bind("keydown keypress", function(event) {
                if (event.which === 9) {
                    var name = attrs.name
                    ctrl.$setDirty(name, true);
                }
            });
        }
    }
}]);