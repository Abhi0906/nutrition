angular.module('app.core')
.directive('inputDatepicker',function($timeout, $rootScope){
	function configureInputDatepicker(scope,element,attrs){

		var today = null;
		if('disabled' in attrs){
            var date = new Date();
         	today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        }

		$(element).datepicker({weekStart: 1,dateFormat:scope.format, endDate: today }).
	   				on('changeDate', function(e){
	   					$timeout(function() {

	   					  	if(e.dates.length){
								scope.date = e.format(scope.format);
		   					    scope.$apply();
		   					    $rootScope.$broadcast('updatedDate',scope.date);
	   						}
	   					}, 0);	   					
	   					$(this).datepicker('hide');
	   					
	   				});
	    scope.$watch('date', function(newValue,oldValue) {
	    	var nDate = new Date(newValue);

	    	$(element).datepicker('update', new Date(nDate.getFullYear(),nDate.getMonth(),nDate.getDate()));

	    		
	   	});	
	};
	return{
		template:'<input ng-model="date"  type="text" data-date-format="{{format}}" readonly="readonly" placeholder="{{format}}" class="form-control" >',
		scope: {
			format: '@',
			date: '='
		},
		link: configureInputDatepicker
	};
});
	