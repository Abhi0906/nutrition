angular.module('app.core').directive('foodFor', function() {
	return {
	restrict: 'E',
	require: 'ngModel',
	replace: true,
	link: function(scope, element, attrs) {

	  scope.food_for_names = [ "Any", "Male", "Female"];
	},
	template: '<select ng-options="food_for for food_for in food_for_names">\
		</select>',
	};
});