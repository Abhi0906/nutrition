angular.module('app.core')
.directive('interactiveBlock', function(){
	function configureBlock(scope,element,attrs){
		// Toggle block's content

		
        $(element).find('[data-toggle="block-toggle-content"]').on('click', function(){
	        var blockContent = $(this).closest('.block').find('.block-content');
	        if ($(this).hasClass('active')) {
	            blockContent.slideDown();
	        } else {
	            blockContent.slideUp();
	        }
	        $(this).toggleClass('active');
	    });

        // Toggle block fullscreen
        $(element).find('[data-toggle="block-toggle-fullscreen"]').on('click', function(){
            var block = $(this).closest('.block');
            if ($(this).hasClass('active')) {
                block.removeClass('block-fullscreen');
            } else {
                block.addClass('block-fullscreen');
            }
            $(this).toggleClass('active');
        });

        // Hide block
        $(element).find('[data-toggle="block-hide"]').on('click', function(){
            $(this).closest('.block').fadeOut();
        });
        if('closed' in attrs){
        	$(element).find('[data-toggle="block-toggle-content"]').click();
        }
        if('transparent' in attrs){
        	$(element).find('.block').css('border','none');
        	$(element).find('.block-title').css('background-color','transparent');
        }

        if('onEdit' in attrs){
        	scope.hasEditFunction = true;
        }
        
	}
	return {
		scope: {
			blockTitle : '=',
			remove:'&onRemove',
			editFunction: "&onEdit",
			titleClick: "&"
		},
		templateUrl : '/templates/interactive_block.html',
		transclude: true,
		link:configureBlock
	};
});
angular.module('app.core')
.directive('blockContent', function(){
	return {
		template : '<div class="block-content" style="display:block;" ng-transclude></div>',
		
		transclude: true,
	};
});

// angular.module('app.core')
// .directive('blockTitle', function(){
// 	return {
// 		scope:{},
// 		template : '<ng-transclude></ng-transclude>',
// 		transclude: true,
// 	};
// });

angular.module('app.core')
.directive('simpleBlock', function(){
	function configureBlock(scope,element,attrs){
		scope.right_block =false;
		scope.is_back_button =false;
		scope.right_block_title = "Expand All";
		if('rightTitle' in attrs){
			
			scope.right_block = true;
		}
		if('isBack' in attrs){
			
			scope.is_back_button = true;
		}
		scope.expandAll = function(){

			var blockContent = $('[data-toggle="block-toggle-content"]').closest('.block').find('.block-content');
	        
	        if (blockContent.css('display')=='block' || scope.right_block_title == "Collapse All") {
	             blockContent.slideUp();
	             $('[data-toggle="block-toggle-content"]').addClass('active');
	             scope.right_block_title = "Expand All";
	           
	        } else {
	            blockContent.slideDown();
	            $('[data-toggle="block-toggle-content"]').removeClass('active');
	            scope.right_block_title = "Collapse All";
	           
	        }
     	}
	}	
	return {
		scope: {
			blockTitle : '@?',
			back:'&?',
			icon: '@'
		},
		templateUrl : '/templates/simple_block.html',
		transclude: true,
		link:configureBlock
		
	};
});