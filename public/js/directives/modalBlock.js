angular.module('app.core')
.directive('modalBlock', function(){
	function configureBlock(scope,element,attrs){
	   console.log(scope);
	    scope.$watch('show', function(newValue,oldValue) {
	    	console.log("newValue----------"+newValue);
	    	
	    	if(scope.show){
	    		$(element).find('.modal').modal('show');
	    		scope.show = false;
	    	}
	    	
	   	});
        
        
    }
	return {
		scope: {
			title : '=',
			show : '='
		},
		templateUrl : '/templates/modal_block.html',
		transclude: true,
		link:configureBlock
	};
});
