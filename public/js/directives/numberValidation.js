
angular.module('app.core')
.directive('numberValidation', function(){
   
    return {
      require: 'ngModel',
      scope: {
                number: "@",
            },
      link: function(scope, element, attrs, modelCtrl) {
        modelCtrl.$parsers.push(function (inputValue) {
         if(inputValue == undefined || inputValue < 1 || inputValue == '.') {
              modelCtrl.$setViewValue(transformedInput);
              modelCtrl.$render();
          }
        var transformedInput = inputValue.replace(/[^0-9.]/g, ''); 
        if (transformedInput!=inputValue) {
              modelCtrl.$setViewValue(transformedInput);
              modelCtrl.$render();
           }       
            return transformedInput;        
        });
      }
    };
});
