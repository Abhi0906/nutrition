angular.module('app.core')
.directive('singleVideoImage', function(){
	function configureVideoImage(scope,element,attrs){
		scope.editable = true;
		scope.deletable = true;

		if('notEditable' in attrs){
			scope.editable = false;
		}

		if('notDeletable' in attrs){
			scope.deletable = false;
		}
		$(element).find('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
			disableOn: 700,
			type: 'iframe',
			mainClass: 'mfp-fade',
			removalDelay: 160,
			preloader: false,

			fixedContentPos: false
		});
	};
	
	return {
		scope: {
			videoimage : '=',
			noDetails:'@?',
			height: '@?',
			width: '@?',
			edit: '&?',
			remove: '&?',
			view: '&?'
		},
		templateUrl : '/templates/single_video_image.html',
		transclude: true,
		link:configureVideoImage
	};
});