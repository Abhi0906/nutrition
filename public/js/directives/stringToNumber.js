angular.module('app.core')
.directive('stringToNumber',function($timeout,$rootScope){

	function configureStringToNumber(scope,element,attrs,ngModel){
        
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseInt(value);
      });
        
    }
    return {
        require: 'ngModel',
		link:configureStringToNumber
	};
});


	   