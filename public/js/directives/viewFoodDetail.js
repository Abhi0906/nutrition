angular.module('app.core')
.directive('viewFoodDetail', function(){
	function configureFoodDetail(scope,element,attrs){
		
	};
	
	return {
		scope: {
			details : '=',
			noDetails:'@?',
			height: '@?',
			width: '@?',
			edit: '&?',
			remove: '&?'
		},
		templateUrl : '/templates/food_detail.html',
		transclude: true,
		link:configureFoodDetail
	};
});
