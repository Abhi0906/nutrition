angular.module('app.core')
.directive('weekdaySchedule',function($timeout){
	function configureWeekDaySchedule(scope,element,attrs){
		scope.week = [
			{dayNumber: '0', dayName: 'S'},
			{dayNumber: '1', dayName: 'M'},
			{dayNumber: '2', dayName: 'T'},
			{dayNumber: '3', dayName: 'W'},
			{dayNumber: '4', dayName: 'T'},
			{dayNumber: '5', dayName: 'F'},
			{dayNumber: '6', dayName: 'S'},
		];
	    	
	};
	return{
		scope: {
			selectedDays: '=?',

		},
		template:'<div style="margin-left:-10px">'+       
			        	'<label style="margin-left:10px" class="checkbox-inline" '+
			        		    'ng-repeat="day in week">'+
			                '<input type="checkbox" style="margin-right:1px;margin-top:2px"'+ 
	                    		 'checklist-model="selectedDays"' +
	                    		 'checklist-value="day.dayNumber">{{day.dayName}}'+
			            '</label>'+
		            '</div>',

		
		link: configureWeekDaySchedule
	};
});
