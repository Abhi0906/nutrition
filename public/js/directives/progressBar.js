angular.module('app.core')
.directive('progressBar',function($timeout, $rootScope){

	function configureProgressBar(scope,element,attrs){

	  $timeout(function() {
		   					callProgress(element);
	   					}, 2000);


	  function callProgress(element){

		  var percent = parseInt(scope.percentValue);
		  
		  if(percent > 0){
	        $(element).find('ul li.layer1 div').addClass('one');
	      }
	      if(percent > 10){
	       $(element).find('ul li.layer2 div').addClass('two');
	      }
	      if(percent > 20){
	       $(element).find('ul li.layer3 div').addClass('three');
	      }
	      if(percent > 30){
	       $(element).find('ul li.layer4 div').addClass('four');
	      }
	      if(percent > 40){
	       $(element).find('ul li.layer5 div').addClass('five');
	      }
	      if(percent > 50){
	      $(element).find('ul li.layer6 div').addClass('sixth');
	      }
	      if(percent > 60){
	       $(element).find('ul li.layer7 div').addClass('seven');
	      }
	      if(percent > 70){
	       $(element).find('ul li.layer8 div').addClass('eight');
	      }
	      if(percent > 80){
	       $(element).find('ul li.layer9 div').addClass('nine');
	      }
	      if(percent > 90){
	       $(element).find('ul li.layer10 div').addClass('ten');
	      }

	  }
   
	};
	return{
		templateUrl : '/templates/progress_block.html',
		scope: {
			percentValue: '@'
		},
		link: configureProgressBar
	};
});
	