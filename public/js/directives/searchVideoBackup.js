angular.module('app.core')
.directive('searchVideo', ['ShowService','$rootScope',function(ShowService,$rootScope){
	function configureSearchVideo(scope,element,attrs){
		scope.uncategorised = false;
        scope.showFilter = true;
        if('unCategorised' in attrs){
            scope.uncategorised = true;
        }

		scope.keyword = "";
		scope.minimun_workout_time = "";
		scope.maximum_workout_time = "";
		scope.minimun_calorie_burn = "";
		scope.maximum_calorie_burn = "";
        scope.assigned_properties ="";
		scope.diffculties = ['1','2','3','4','5'];
		scope.selectedDifficulties = [];
    	scope.body_focus = ['Core','Lower Body','Total Body','Upper Body'];
    	scope.selectedBodyFocus = [];
    	ShowService.query('api/v2/video/training_types').then(function(results){
            scope.training_types = results.response;
        });
        scope.selectedTrainingTypes = [];
        ShowService.query('api/v2/video/equipments').then(function(results){
            scope.equipments = results.response;
       	});
       	scope.selectedEquipments = [];
        var watchArray = ['keyword', 'minimun_workout_time','maximum_workout_time',
        'minimun_calorie_burn','maximum_calorie_burn','assigned_properties'];

        scope.$watchGroup(watchArray, function(newVal, oldVal) {
            scope.searchVideo();
        });

        scope.$watch('assigned_properties',function(newVal){
           if(newVal){
               scope.showFilter = false;
            }else{
              scope.showFilter = true;
            }
        });

        scope.$watchCollection('selectedDifficulties', function(newVal, oldVal) {
            scope.searchVideo();
        });
        scope.$watchCollection('selectedBodyFocus', function(newVal, oldVal) {
            scope.searchVideo();
        });
        scope.$watchCollection('selectedTrainingTypes', function(newVal, oldVal) {
            scope.searchVideo();
        });
        scope.$watchCollection('selectedEquipments', function(newVal, oldVal) {
            scope.searchVideo();
        });


       	scope.searchVideo = function(){
       	    console.log(scope.assigned_properties);
        	if(scope.keyword != "" ||
               scope.minimun_workout_time != "" ||
               scope.maximum_workout_time != "" ||
               scope.minimun_calorie_burn != "" ||
               scope.maximum_calorie_burn != "" ||
        	   scope.selectedDifficulties !="" ||
        	   scope.selectedBodyFocus !="" ||
        	   scope.selectedTrainingTypes !="" ||
        	   scope.selectedEquipments !=""  ||
               (scope.assigned_properties == true || scope.assigned_properties == false)
        	  ){
        		var params ={'keyword':scope.keyword,
        					 'minimun_workout_time':scope.minimun_workout_time,
        					 'maximum_workout_time':scope.maximum_workout_time,
        					 'minimun_calorie_burn':scope.minimun_calorie_burn,
        					 'maximum_calorie_burn':scope.maximum_calorie_burn,
        					 'diffculties':scope.selectedDifficulties,
        					 'body_focus':scope.selectedBodyFocus,
        					 'training_types':scope.selectedTrainingTypes,
        					 'equipments':scope.selectedEquipments,
                             'assigned_properties':scope.assigned_properties
        					};
        		ShowService.search('api/v2/video/list',params,1).then(function(results){
            		scope.videos = results.response.data;
            	});
        	}else{

        		ShowService.query('api/v2/video/list').then(function(results){
            		scope.videos = results.response.data;
            	});
        	}

    	}

    	scope.clearFilter = function(){
    		scope.keyword = "";
			scope.minimun_workout_time = "";
			scope.maximum_workout_time = "";
			scope.minimun_calorie_burn = "";
			scope.maximum_calorie_burn = "";
            scope.assigned_properties = "";
           	scope.selectedDifficulties = [];
			scope.selectedBodyFocus = [];
			scope.selectedTrainingTypes = [];
			scope.selectedEquipments = [];
            var checkboxElement = $(element).find(':checkbox');
            if ($(':checked').length) {
                checkboxElement.prop('checked', false);
            }

            scope.searchVideo();
    	}

    	scope.searchVideo();

        $rootScope.$on('videoEdited', function (e,args) {

            scope.searchVideo();
        });

	}
	return {
		scope: {
			videos: '=?',
			blockTitle: '@?'
		},

	    templateUrl : '/templates/search_video.html',
		link:configureSearchVideo
	};
}]);