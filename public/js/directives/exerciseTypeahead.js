angular.module('app.core')
.directive('exerciseTypeahead',function($timeout){
	function configureTypeahead(scope,element,attrs){
		var exercises = new Bloodhound({
			datumTokenizer: function (data) {
		 	    return Bloodhound.tokenizers.whitespace(data.name);
		    },
		    queryTokenizer: Bloodhound.tokenizers.whitespace,
		    remote: {
			    url: '/api/v3/exercise?keyword=%QUERY&limit=100',
			    wildcard: '%QUERY',
			    filter: function (exercise) {
			    	hideSpinner();		     		
		            // Map the remote source JSON array to a JavaScript object array
		             return $.map(exercise.response, function (exercise) {
		                return {
		                    name: exercise.name,
		                    pivot:exercise
		                };
		             });
			    }
		    }

		});

		// Initialize the Bloodhound suggestion engine
		 exercises.initialize();

		$(element).find('.typeahead').typeahead(null, {
		    name: 'exercises',
		    display: 'name',
		    minLength:3,
		    hint: false,
		    highlight: true,
		    limit: 50,
		    classNames: {
		     	suggestion: 'Typeahead-suggestion',
		    },
		    templates: {
			    empty: [
			        '<div class="empty-message">',
			        '<h5><div class="text-primary text-center"><strong>No results found</strong></div></h5>',
			        '</div>'
			    ].join('\n'),
			    suggestion: function(data) {
					return '<p><strong>' + data.name + '</strong> <br><small>' + data.pivot.time +"&nbsp;minutes,&nbsp;&nbsp;" + data.pivot.calories_burned+ "&nbsp;calc</small>" +'</p>';
				}
			},
	        source:  exercises.ttAdapter()
		}).on('typeahead:select', function(ev, data) {
			$timeout(function() {
			    scope.newExercise = data;
			    scope.newExercise.pivot.exercise_type = "Cardio";
			    scope.newExercise.pivot.exercise_date = moment().format('MMMM DD, YYYY');
			    scope.$apply();
			}, 0);
		}).on('typeahead:asyncrequest', function(ev, data) {
			//console.log("open..");
				$(".tt-hint").addClass("loading");
		});

		function hideSpinner(){
			//console.log("hide");
			$(".tt-hint").removeClass("loading");
		}
	};
	return{
		template:'<input  type="text"  placeholder="{{placeholder}}"  id="search_exercise" class="form-control typeahead" >',
		scope: {
			placeholder: '@',
			newExercise: '=?',
			clearTypeahead:'@?'
		},
		link: configureTypeahead
	};
});
