angular.module('app.core')
.directive('gaugeMeter',function($timeout){
	
  function configureGaugeMeter(scope,element,attrs){
  	console.log(attrs);
  		var value = parseInt(attrs['meterValue']);
  		$timeout(function() {
		   					callMeter(element, value);
	   					}, 1000);	
  		function callMeter(element, value){
  			alert(element+"-"+value);
  			$(element).gauge({
            values: {
                0 : '0',
                20: '2',
                40: '4',
                60: '6',
                80: '8',
                100: '10'
            },
            colors: {
                /*0 : '#666',
                9 : '#378618',
                60: '#ffa500',
                80: '#f00'*/
                0 : '#003d99',
                40: '#33cc33',
                70: '#ff1a1a',
                100:'#f00'
            },
            angles: [
                180,
                360
            ],
            lineWidth: 10,
            arrowWidth: 5,
            arrowColor: '#ccc',
            inset:true,
            value:value
        });
  		}
  		
	
	};
	return{
		
		link: configureGaugeMeter
	};
});
