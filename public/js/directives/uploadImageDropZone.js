angular.module('app.core')
.directive('uploadImageDropzone',function($timeout){
    function configureDropzone(scope,element,attrs){
        Dropzone.autoDiscover = false;
        scope.originalFile = scope.file;
        scope.oldImageName = "";

        if('uploadImageUrl' in attrs && 'removeImageUrl' in attrs){
            scope.uploadImageUrl = attrs['uploadImageUrl'];
            scope.removeImageUrl = attrs['removeImageUrl'];
        }
        else{
            return false;
        } 
        var acceptedFiles = 'image/*';     
        if('acceptedFiles' in attrs){
            acceptedFiles ="application/pdf";
        }

        if('oldFileName' in attrs){
                //console.log(attrs);
            $timeout(function() {
                 scope.oldImageName = attrs['oldFileName'];
                console.log("oldImageName",scope.oldImageName);
        },1000);
        }


        $(element).dropzone({
            url: scope.uploadImageUrl,
            maxFiles: 1,
            maxFilesize:4,
            paramName:'image_file',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            acceptedFiles:acceptedFiles,
            addRemoveLinks:true,
            uploadMultiple:false,
            init: function() {
                this.on("complete", function(data) {
                    if(data.status == 'success'){ 
                        var response = eval('(' + data.xhr.response + ')');
                      //  var destination_path = '/uploads/'+response.file_name;
                        $timeout(function() {
                           // $('#user_profle_pic').attr('src',destination_path);
                            scope.file = response.file_name;
                            scope.$apply();
                        }, 0);
                     
                    }
                });
                this.on("removedfile",function(file){
                    var file_name = file.name;
                    HTTPRequest('', 'get', scope.removeImageUrl+file_name, function(data,status,xhr){
                        if(status == 'success'){
                            if(data.error == false){
                              $timeout(function() {
                                    console.log(scope.oldImageName);
                                    scope.file = scope.oldImageName;
                                    scope.$apply();
                                }, 1000);


                            }
                        }
                    });
                    console.log(this.files);
                }); 
                this.on("addedfile", function() {
                    if (this.files[1]!=null){
                        this.removeFile(this.files[0]);
                    }
                });
            
            }
           
        });
    }
    return {
        scope: {
            file: '='
           
        },
        link:configureDropzone
    };
});


       