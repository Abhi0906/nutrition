angular.module('app.core')
.directive('searchVideo', ['ShowService','$rootScope',function(ShowService,$rootScope){
	function configureSearchVideo(scope,element,attrs){
		scope.uncategorised = false;
        scope.showFilter = true;
        if('unCategorised' in attrs){
            scope.uncategorised = true;
        }

		scope.keyword = "";
		scope.minimun_workout_time = "";
		scope.maximum_workout_time = "";
		scope.minimun_calorie_burn = "";
		scope.maximum_calorie_burn = "";
        scope.assigned_properties ="";
		scope.experiences = ['Beginner','Intermediate','Advanced'];
    	ShowService.query('api/v2/video/equipments').then(function(results){
            scope.equipments = results.response;
       	});

        ShowService.query('api/v2/video/body_parts').then(function(results){
            scope.body_parts = results.response;
        });

        var watchArray = ['keyword', 'minimun_workout_time','maximum_workout_time',
        'minimun_calorie_burn','maximum_calorie_burn','assigned_properties','experience','body_part','equipment'];

        scope.$watchGroup(watchArray, function(newVal, oldVal) {
            scope.searchVideo();
        });

        scope.$watch('assigned_properties',function(newVal){
           if(newVal){
               scope.showFilter = false;
            }else{
              scope.showFilter = true;
            }
        });

     	scope.searchVideo = function(){

        	if(scope.keyword != "" ||
                scope.minimun_workout_time != "" ||
                scope.maximum_workout_time != "" ||
                scope.minimun_calorie_burn != "" ||
                scope.maximum_calorie_burn != "" ||
                typeof scope.experience != "undefined" ||
                typeof scope.body_part != "undefined" ||
                typeof scope.equipment != "undefined" ||
                (scope.assigned_properties == true || scope.assigned_properties == false)
        	  ){
        		var params ={'keyword':scope.keyword,
        					 'minimun_workout_time':scope.minimun_workout_time,
        					 'maximum_workout_time':scope.maximum_workout_time,
           					 'minimun_calorie_burn':scope.minimun_calorie_burn,
        					 'maximum_calorie_burn':scope.maximum_calorie_burn,
        					 'experience':scope.experience,
        					 'body_part':scope.body_part,
	       					 'equipment':scope.equipment,
                             'assigned_properties':scope.assigned_properties
        					};
        		ShowService.search('api/v2/video/list',params,1).then(function(results){
            		scope.videos = results.response.data;
            	});
        	}else{

        		ShowService.query('api/v2/video/list').then(function(results){
            		scope.videos = results.response.data;
            	});
        	}

    	}

    	scope.clearFilter = function(){
    		scope.keyword = "";
			scope.minimun_workout_time = "";
			scope.maximum_workout_time = "";
			scope.minimun_calorie_burn = "";
			scope.maximum_calorie_burn = "";
            scope.assigned_properties = "";
           	scope.experience = "undefined";
			scope.body_part = "undefined";
			scope.equipment = "undefined";

            var checkboxElement = $(element).find(':checkbox');
            if ($(':checked').length) {
                checkboxElement.prop('checked', false);
            }

            scope.searchVideo();
    	}

    	scope.searchVideo();

        $rootScope.$on('videoEdited', function (e,args) {

            scope.searchVideo();
        });

	}
	return {
		scope: {
			videos: '=?',
			blockTitle: '@?'
		},

	    templateUrl : '/templates/search_video.html',
		link:configureSearchVideo
	};
}]);