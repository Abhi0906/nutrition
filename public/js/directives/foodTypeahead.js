angular.module('app.core')
.directive('foodTypeahead',function($timeout){
	function configureTypeahead(scope,element,attrs){
		var nutritions = new Bloodhound({
			datumTokenizer: function (data) {
		 	    return Bloodhound.tokenizers.whitespace(data.name);
		    },
		    queryTokenizer: Bloodhound.tokenizers.whitespace,
		    remote: {
			    url: '/api/v3/food?keyword=%QUERY&limit=100',
			    wildcard: '%QUERY',
			    filter: function (nutritons) {

			    	hideSpinner();
			  //  console.log(nutritons.response);			     		
		            // Map the remote source JSON array to a JavaScript object array
		             return $.map(nutritons.response, function (nutrition) {
		                return {
		                	 //console.log(nutrition);
		                    name: nutrition.name,
		                    nutrition:nutrition

		                };
		             });
			    }
		    }

		});

		// Initialize the Bloodhound suggestion engine
		 nutritions.initialize();

		$(element).find('.typeahead').typeahead(null, {
		    name: 'nutritions',
		    display: 'name',
		    minLength:3,
		    hint: false,
		    highlight: true,
		    limit: 50,
		    classNames: {
		     	suggestion: 'Typeahead-suggestion',
		    },
		    templates: {
			    empty: [
			        '<div class="empty-message">',
			        '<h5><div class="text-primary text-center"><strong>No results found</strong></div></h5>',
			        '</div>'
			    ].join('\n'),
			    suggestion: function(data) {
					return '<p><strong>' + data.name + '</strong> <br><small>' + data.nutrition.nutrition_brand.name +",&nbsp;&nbsp;"+ data.nutrition.serving_quantity + '&nbsp;'+ data.nutrition.serving_size_unit  + ",&nbsp;&nbsp;" + data.nutrition.calories + "&nbsp;cal</small>" +'</p>';
				}
			},
	        source:  nutritions.ttAdapter()
		}).on('typeahead:select', function(ev, data) {
			$timeout(function() {
			    scope.newNutrition = data;
			    scope.newNutrition.no_of_servings = 1;
			    scope.newNutrition.schedule_time = getScheduleTime();
			    scope.newNutrition.serving_date = moment().format('MMMM DD, YYYY');
			    scope.$apply();
			}, 0);
		}).on('typeahead:asyncrequest', function(ev, data) {
			//console.log("open..");
				$(".tt-hint").addClass("loading");
		});

		function getScheduleTime(){
			
			var today_hours = moment().format('HH');
			var schedule_time = 'Snacks';
			if(today_hours >=6 && today_hours < 11){
				schedule_time ='Breakfast';
			}else if(today_hours >=11 && today_hours < 15){
				schedule_time ='Lunch';
			}else if(today_hours >=19 && today_hours < 24){
				schedule_time ='Dinner';
			}else{
				schedule_time = 'Snacks';
			}
			return schedule_time;
		}
		function hideSpinner(){
			//console.log("hide");
			$(".tt-hint").removeClass("loading");
		}
	};
	return{
		template:'<input  type="text"  placeholder="{{placeholder}}"  id="search_food" class="form-control typeahead" >',
		scope: {
			placeholder: '@',
			newNutrition: '=?',
			clearTypeahead:'@?'
		},
		link: configureTypeahead
	};
});
