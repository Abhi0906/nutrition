angular.module('app.core')
.directive('inputTimepicker',function($timeout){
	function configureInputTimepicker(scope,element,attrs){
	    $(element).timepicker(
	    	{ minuteStep: 1,showSeconds: true,showMeridian: false}
	    	).
           	on('changeTime.timepicker',function(e){
		 		$timeout(function() {
					scope.time =e.time.value;
					scope.$apply();
			    }, 0);
	    	});				
	    scope.$watch('time', function(newValue,oldValue) {
	    	$(element).timepicker('setTime',newValue);	
	   	});	
	};
	return{
		template:'<input ng-model="time" type="text" class="form-control" >',

		scope: {
		//	format: '@',
			time: '='
		},
		link: configureInputTimepicker
	};
});
