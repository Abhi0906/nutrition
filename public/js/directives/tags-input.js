angular.module('app.core')
.directive('tagsInput',function($timeout){
	function configureTagsInput(scope,element,attrs){
	    $(element).tagsInput({ width: 'auto',
	    					    height: 'auto',
	    					    onChange:function(ele,ele_tages){
	    					    	//console.log(ele);
	    					    	//console.log(ele_tages);
	    					    	 $timeout(function() {
							          scope.ngModel = ele_tages;
							          scope.$apply();
							        }, 0);
	    					    }

	    					});

	   
	   
	};
	return {

		scope:{
	       ngModel:'='
	    },

		link: configureTagsInput
	};
});
