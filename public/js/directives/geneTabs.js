angular.module('app.core')
.directive('geneTabs', function(){
	function configureTabs(scope,element,attrs){
	    $(element).find('a').click(function(e){ e.preventDefault(); $(this).tab('show'); });
        
    }
	return {	
		
		link:configureTabs
	};
});
