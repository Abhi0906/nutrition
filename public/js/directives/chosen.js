angular.module('app.core')
.directive('chosen',function($timeout){
	
  function configureChosen(scope,element,attrs){
    var dataList;
    $(element).chosen({
      no_results_text: "Oops, nothing found!",
      width: "100%",
      max_selected_options: parseInt(attrs['maxSelectedOptions'])
    });
    $(element).chosen().change( function(e,val){  
      
        var selected; 
        if('multiple' in attrs){
            selected = $(element).chosen().val();
        }
        else{
            selected = $(element).find('option:selected').html();
        }
        console.log(selected);
        $timeout(function() {
          scope.ngModel = selected;
          scope.$apply();
        }, 1000);
    } ); 

    scope.$watch('chosen', function(value) {
      dataList = value;
      $(element).trigger('chosen:updated');
     
    });

    scope.$watch('ngModel', function(value) {      
      $timeout(function() {
          $(element).trigger('chosen:updated');
         
        },1000);
    });
	
	};
	return{
		scope:{
      chosen: '=',
      ngModel:'='
    }	,
		link: configureChosen
	};
});
