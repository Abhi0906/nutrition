angular.module('app.core')
.directive('singleImage', function(){
	function configureImage(scope,element,attrs){
		scope.editable = true;
		scope.deletable = true;

		if('notEditable' in attrs){
			scope.editable = false;
		}

		if('notDeletable' in attrs){
			scope.deletable = false;
		}
		
	};
	
	return {
		scope: {
			exercise : '=',
			noDetails:'@?',
			height: '@?',
			width: '@?',
			edit: '&?',
			remove: '&?',
			view: '&?'
		},
		templateUrl : '/templates/single_image.html',
		transclude: true,
		link:configureImage
	};
});