angular.module('app.core')
.directive('weightGraph',function(){
	function configureWeightGraph(scope,element,attrs){
			console.log(element);
		 // Get the elements where we will attach the charts
	            var chartClassic = $('#chart-classic');

	            // Random data for the charts
	            //var dataPounds = [[1, 1560], [2, 1650], [3, 1320], [4, 1950], [5, 1800], [6, 2400], [7, 2100], [8, 2550], [9, 3300], [10, 3900], [11, 4200], [12, 4500]];
	            var dataPounds = [[1, 30], [2, 40], [3, 50], [4, 35], [5, 65], [6, 48], [7, 25]];

	            // Array with month labels used in Classic and Stacked chart
	            var chartMonths = [[1, 'Sun'], [2, 'Mon'], [3, 'Tue'], [4, 'Wed'], [5, 'Thu'], [6, 'Fri'], [7, 'Sat']];

	            // Classic Chart
	            $.plot(chartClassic,
	                [
	                    {
	                        label: 'Weight',
	                        data: dataPounds,
	                        lines: {show: true, fill: true, fillColor: {colors: [{opacity: 0.25}, {opacity: 0.25}]}},
	                        points: {show: true, radius: 6}
	                    }
	                ],
	                {
	                    colors: ['#3498db', '#333333'],
	                    legend: {show: true, position: 'nw', margin: [15, 10]},
	                    grid: {borderWidth: 0, hoverable: true, clickable: true},
	                    yaxis: {ticks: 4, tickColor: '#eeeeee'},
	                    xaxis: {ticks: chartMonths, tickColor: '#ffffff'}
	                }
	            );

	            // Creating and attaching a tooltip to the classic chart
	            var previousPoint = null, ttlabel = null;
	            chartClassic.bind('plothover', function(event, pos, item) {

	                if (item) {
	                    if (previousPoint !== item.dataIndex) {
	                        previousPoint = item.dataIndex;

	                        $('#chart-tooltip').remove();
	                        var x = item.datapoint[0], y = item.datapoint[1];

	                        if (item.seriesIndex === 1) {
	                            ttlabel = '<strong>' + y + '</strong> sales';
	                        } else {
	                            ttlabel = '$ <strong>' + y + '</strong>';
	                        }

	                        $('<div id="chart-tooltip" class="chart-tooltip">' + ttlabel + '</div>')
	                            .css({top: item.pageY - 45, left: item.pageX + 5}).appendTo("body").show();
	                    }
	                }
	                else {
	                    $('#chart-tooltip').remove();
	                    previousPoint = null;
	                }
	            });
			
	};
	return{

		template:'<div id="chart-classic"  class="chart logWeightChart"></div>',
		scope: {

			weightdata: '=',
			date: '=',
			
		},
		link: configureWeightGraph
	};
});
