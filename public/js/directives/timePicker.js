angular.module('app.core')
.directive('timePicker',function($timeout){
	
  function configureTimePicker(scope,element,attrs){
  scope.timings = [
                { value: '', text: 'Select Time' },             
                { value: 0.00, text: "12:00 am"}, 
                // { value: "00:15:00", text: "12:15 am"}, 
                { value: 0.50, text: "12:30 am"}, 
                // { value: "00:45:00", text: "12:45 am"}, 
                { value: 1.00, text: "1:00 am"}, 
                // { value: "01:15:00", text: "1:15 am"}, 
                { value: 1.50, text: "1:30 am"}, 
                // { value: "01:45:00", text: "1:45 am"}, 
                { value: 2.00, text: "2:00 am"}, 
                // { value: "02:15:00", text: "2:15 am"}, 
                { value: 2.50, text: "2:30 am"}, 
                // { value: "02:45:00", text: "2:45 am"}, 
                { value: 3.00, text: "3:00 am"}, 
                // { value: "03:15:00", text: "3:15 am"}, 
                { value: 3.50, text: "3:30 am"}, 
                // { value: "03:45:00", text: "3:45 am"}, 
                { value: 4.00, text: "4:00 am"}, 
                // { value: "04:15:00", text: "4:15 am"}, 
                { value: 4.50, text: "4:30 am"}, 
                // { value: "04:45:00", text: "4:45 am"}, 
                { value: 5.00, text: "5:00 am"}, 
                // { value: "05:15:00", text: "5:15 am"}, 
                { value: 5.50, text: "5:30 am"}, 
                // { value: "05:45:00", text: "5:45 am"}, 
                { value: 6.00, text: "6:00 am"}, 
                // { value: "06:15:00", text: "6:15 am"}, 
                { value: 6.50, text: "6:30 am"}, 
                // { value: "06:45:00", text: "6:45 am"}, 
                { value: 7.00, text: "7:00 am"}, 
                // { value: "07:15:00", text: "7:15 am"}, 
                { value: 7.50, text: "7:30 am"}, 
                // { value: "07:45:00", text: "7:45 am"}, 
                { value: 8.00, text: "8:00 am"}, 
                // { value: "08:15:00", text: "8:15 am"}, 
                { value: 8.50, text: "8:30 am"}, 
                // { value: "08:45:00", text: "8:45 am"}, 
                { value: 9.00, text: "9:00 am"}, 
                // { value: "09:15:00", text: "9:15 am"}, 
                { value: 9.50, text: "9:30 am"}, 
                // { value: "09:45:00", text: "9:45 am"}, 
                { value: 10.00, text: "10:00 am"}, 
                // { value: "10:15:00", text: "10:15 am"}, 
                { value: 10.50, text: "10:30 am"}, 
                // { value: "10:45:00", text: "10:45 am"}, 
                { value: 11.00, text: "11:00 am"}, 
                // { value: "11:15:00", text: "11:15 am"}, 
                { value: 11.50, text: "11:30 am"}, 
                // { value: "11:45:00", text: "11:45 am"}, 
                { value: 12.00, text: "12:00 pm"}, 
                // { value: "12:15:00", text: "12:15 pm"}, 
                { value: 12.50, text: "12:30 pm"}, 
                // { value: "12:45:00", text: "12:45 pm"}, 
                { value: 13.00, text: "1:00 pm"}, 
                // { value: "13:15:00", text: "1:15 pm"}, 
                { value: 13.50, text: "1:30 pm"}, 
                // { value: "13:45:00", text: "1:45 pm"}, 
                { value: 14.00, text: "2:00 pm"}, 
                // { value: "14:15:00", text: "2:15 pm"}, 
                { value: 14.50, text: "2:30 pm"}, 
                // { value: "14:45:00", text: "2:45 pm"}, 
                { value: 15.00, text: "3:00 pm"}, 
                // { value: "15:15:00", text: "3:15 pm"}, 
                { value: 15.50, text: "3:30 pm"}, 
                // { value: "15:45:00", text: "3:45 pm"}, 
                { value: 16.00, text: "4:00 pm"}, 
                // { value: "16:15:00", text: "4:15 pm"}, 
                { value: 16.50, text: "4:30 pm"}, 
                // { value: "16:45:00", text: "4:45 pm"}, 
                { value: 17.00, text: "5:00 pm"}, 
                // { value: "17:15:00", text: "5:15 pm"}, 
                { value: 17.50, text: "5:30 pm"}, 
                // { value: "17:45:00", text: "5:45 pm"}, 
                { value: 18.00, text: "6:00 pm"}, 
                // { value: "18:15:00", text: "6:15 pm"}, 
                { value: 18.50, text: "6:30 pm"}, 
                // { value: "18:45:00", text: "6:45 pm"}, 
                { value: 19.00, text: "7:00 pm"}, 
                // { value: "19:15:00", text: "7:15 pm"}, 
                { value: 19.50, text: "7:30 pm"}, 
                // { value: "19:45:00", text: "7:45 pm"}, 
                { value: 20.00, text: "8:00 pm"}, 
                // { value: "20:15:00", text: "8:15 pm"}, 
                { value: 20.50, text: "8:30 pm"}, 
                { value: 20.75, text: "8:45 pm"}, 
                { value: 21.00, text: "9:00 pm"}, 
                // { value: "21:15:00", text: "9:15 pm"}, 
                { value: 21.50, text: "9:30 pm"}, 
                // { value: "21:45:00", text: "9:45 pm"}, 
                { value: 22.00, text: "10:00 pm"}, 
                // { value: "22:15:00", text: "10:15 pm"}, 
                { value: 22.50, text: "10:30 pm"}, 
                // { value: "22:45:00", text: "10:45 pm"}, 
                { value: 23.00, text: "11:00 pm"}, 
                // { value: "23:15:00", text: "11:15 pm"}, 
                { value: 23.50, text: "11:30 pm"}, 
                // { value: "23:45:00", text: "11:45 pm"}, 
   ];
   
	};
	return{
		 restrict: 'E',
    require: 'ngModel',
    replace: true,
		link: configureTimePicker,
    template: '<select class="form-control" >\
    <option value="{{time.value}}" ng-repeat="time in timings">{{time.text}}</option>\
    </select>'
	};
});
