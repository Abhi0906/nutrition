angular.module('app.core')
	   .directive('configureBlock',function(){
	   		function configureBlock(scope,element,attrs){
	   			// Toggle block's content
	   			//console.log($(element).find('[data-toggle="block-toggle-content"]'));
		        $(element).find('[data-toggle="block-toggle-content"]').on('click', function(){
		            var blockContent = $(this).closest('.block').find('.block-content');

		            if ($(this).hasClass('active')) {
		                blockContent.slideDown();
		            } else {
		                blockContent.slideUp();
		            }

		            $(this).toggleClass('active');
		        });

		        // Toggle block fullscreen
		        $(element).find('[data-toggle="block-toggle-fullscreen"]').on('click', function(){
		            var block = $(this).closest('.block');

		            if ($(this).hasClass('active')) {
		                block.removeClass('block-fullscreen');
		            } else {
		                block.addClass('block-fullscreen');
		            }

		            $(this).toggleClass('active');
		        });

		        // Hide block
		        $(element).find('[data-toggle="block-hide"]').on('click', function(){
		            $(this).closest('.block').fadeOut();
		        });
	   		}
	   		return {
	   			link:configureBlock
	   		};
	   });