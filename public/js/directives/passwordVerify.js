/* Directives */
//http://codepen.io/brunoscopelliti/pen/ECyka
//http://blog.brunoscopelliti.com/angularjs-directive-to-check-that-passwords-match/
angular.module('authLogin')
    .directive('pwCheck', [function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
            var firstPassword = '#' + attrs.pwCheck;
                elem.add(firstPassword).on('keyup', function () {
                scope.$apply(function () {
                   // console.log("element value "+elem.val());
                   // console.log("fristpassword value "+$(firstPassword).val());
                    //console.info(elem.val() === $(firstPassword).val());
                    ctrl.$setValidity('pwmatch', elem.val() === $(firstPassword).val());
                });
            });
        }
    }
}]);