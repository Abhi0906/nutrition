angular.module('app.core')
.directive('inputTypeahead',function($timeout){
	function configureTypeahead(scope,element,attrs){
		var medicines = new Bloodhound({
			datumTokenizer: function (data) {
		 	    return Bloodhound.tokenizers.whitespace(data.name);
		    },
		    queryTokenizer: Bloodhound.tokenizers.whitespace,
		    remote: {
			    url: '/api/v3/patient/medicines/%QUERY',
			    wildcard: '%QUERY',
			    filter: function (medicines) {			     		
		            // Map the remote source JSON array to a JavaScript object array
		            return $.map(medicines.response, function (medicine) {
		                return {
		                    name: medicine.display_medicine_name,
		                    medicine:medicine
		                };
		            });
			    }
		    }

		});

		// Initialize the Bloodhound suggestion engine
		 medicines.initialize();

		$(element).find('.typeahead').typeahead(null, {
			  name: 'medicines',
			  display: 'name',
			  minLength:3,
			  hint: false,
			  highlight: true,
		      source:  medicines.ttAdapter()
		}).on('typeahead:select', function(ev, datum) {
			  	$timeout(function() {
			  		//var d = new Date();
			  		var med = 	{
				  					id:datum.medicine.id,
				  			        display_name:datum.name,
				  			        med_frequency:datum.medicine.frequency,
				  			        frequency_times:datum.medicine.frequency_times,
				  			        name:datum.medicine.name,
				  			        generic_name:datum.medicine.generic_name,
				  			        //dose_strength:datum.medicine.dose_strength,
				  			        //dose_form:datum.medicine.dose_form,
				  			        //dose:datum.medicine.dose,
				  			        //uom:datum.medicine.uom,
				  			        //route:datum.medicine.route
				  			        time: datum.medicine.time,
				  			        display_medicine_direction: datum.medicine.display_medicine_direction,
				  			        //start: d.getFullYear() +'-'+ (d.getMonth()+1) +'-'+ d.getDate()
				  			        start: moment().format('MMMM DD, YYYY')
	  							};
			  			
			  		scope.newMedicine = med;
			  		scope.$watchGroup(['newMedicine.start','newMedicine.days'], function (newVal) {
			  			console.log(newVal);
			  			var endDate;
			  			//scope.newMedicine.end = "12-1-2015";
			  			if(!newVal[1]){
			  			  	return;
			  			}

		  				//var date = new Date(newVal[0]);
						var numberOfDaysToAdd = newVal[1];
					
						//endDate = (date.getDate()-1)+'-'+(date.getMonth() + 1)+'-'+date.getFullYear();
						//endDate = date.setDate(date.getDate() + parseInt(numberOfDaysToAdd));
						//endDate = date.getFullYear()+'-'+(date.getMonth() + 1)+'-'+(date.getDate()-1);
						endDate = moment(newVal[0], "MMMM DD, YYYY").add('days', numberOfDaysToAdd).subtract('day', 1).format('MMMM DD, YYYY');
						console.log(endDate);
					 	scope.newMedicine.end = endDate; 
					});
   					scope.$apply();
   					if(scope.clearTypeahead == "true"){
   						console.log($(element).find('.typeahead'));
   						$(element).find('.typeahead').val('');
   					}
	   			}, 0);

		});

		
	};
	return{
		template:'<input   type="text"  placeholder="{{placeholder}}" class="form-control typeahead" >',
		scope: {
			placeholder: '@',
			newMedicine: '=?',
			clearTypeahead:'@?'
		},
		link: configureTypeahead
	};
});
