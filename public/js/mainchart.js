AmCharts.makeChart("chartdiv1",
                {
                    "type": "serial",
                    "categoryField": "category",
                    "startDuration": 1,
                    "autoMarginOffset": 0,
                    "marginBottom": 10,
                    "marginTop": 0,
                    "marginLeft": 50,
                    "fontSize": 11,
                    "categoryAxis": {
                        "gridPosition": "start",
                        "gridColor": "#FFFFFF",
                    },
                    "trendLines": [],
                    "graphs": [
                        {
                            "balloonText": "[[title]] of [[category]]:[[value]]",
                            "bullet": "round",
                            "id": "AmGraph-1",
                            "title": "graph 1",
                            "type": "smoothedLine",
                            "valueField": "column-1"
                        },
                        {
                            "balloonText": "[[title]] of [[category]]:[[value]]",
                            "bullet": "square",
                            "id": "AmGraph-2",
                            "hidden": true,
                            "title": "graph 2",
                            "type": "smoothedLine",
                            "valueField": "column-2"
                        }
                    ],
                    "guides": [],
                    "valueAxes": [
                        {
                            "id": "ValueAxis-1",
                            "title": "Axis title",
                            "titleFontSize": 0,
                            "gridAlpha": 0.05,
                            "maximum": 300
                        }
                    ],
                    "allLabels": [],
                    "balloon": {},
                    "legend": {
                        "enabled": false,
                        "useGraphSettings": true
                    },
                    "titles": [
                        {
                            "id": "Title-1",
                            "size": 0,
                            "text": "Chart Title"
                        }
                    ],
                    "dataProvider": [
                        {
                            "category": "Jan",
                            "column-1": 200
                        },
                        {
                            "category": "Feb",
                            "column-1": 150
                        },
                        {
                            "category": "Mar",
                            "column-1": 100
                        },
                        {
                            "category": "Apr",
                            "column-1": 250
                        }
                    ]
                }
);


/*This is for second row chart*/
AmCharts.makeChart("chartdiv2",
                {
                    "type": "serial",
                    "categoryField": "category",
                    "startDuration": 1,
                    "autoMarginOffset": 0,
                    "marginBottom": 10,
                    "marginTop": 0,
                    "marginLeft": 50,
                    "fontSize": 11,
                    "categoryAxis": {
                        "gridPosition": "start",
                        "gridColor": "#FFFFFF"
                    },
                    "trendLines": [],
                    "graphs": [
                        {
                            //"balloonText": "[[title]] of [[category]]:[[value]]",

                            "balloonText":"[[category]]:[[realValue]] ([[value]]%)",
                            "bullet": "round",
                            "id": "AmGraph-1",
                            "title": "graph 1",
                            "type": "smoothedLine",
                            "valueField": "column-1"
                        },
                        {
                            "balloonText": "[[title]] of [[category]]:[[value]]",
                            "bullet": "square",
                            "id": "AmGraph-2",
                            "hidden": true,
                            "title": "graph 2",
                            "type": "smoothedLine",
                            "valueField": "column-2"
                        }
                    ],
                    "guides": [],
                    "valueAxes": [
                        {
                            "id": "ValueAxis-1",
                            "title": "Axis title",
                            "titleFontSize": 0,
                            "gridAlpha": 0.05,
                            "maximum": 40,
                            "minimum":0,
                            "recalculateToPercents": true
                        }
                    ],
                    "allLabels": [],
                    "balloon": {},
                    "legend": {
                        "enabled": false,
                        "useGraphSettings": true
                    },
                    "titles": [
                        {
                            "id": "Title-1",
                            "size": 0,
                            "text": "Chart Title"
                        }
                    ],
                    "dataProvider": [
                        {
                            "category": "Jan",
                            "column-1": 20
                        },
                        {
                            "category": "Feb",
                            "column-1": 25
                        },
                        {
                            "category": "Mar",
                            "column-1": 15
                        },
                        {
                            "category": "Apr",
                            "column-1": 25
                        }
                    ]
                }
);
/*End of second row chart*/

/*This is for Third row chart*/
AmCharts.makeChart("chartdiv3",
                {
                    "type": "serial",
                    "categoryField": "category",
                    "startDuration": 1,
                    "autoMarginOffset": 0,
                    "marginBottom": 10,
                    "marginTop": 0,
                    "marginLeft": 50,
                    "fontSize": 11,
                    "categoryAxis": {
                        "gridPosition": "start",
                        "gridColor": "#FFFFFF"
                    },
                    "trendLines": [],
                    "graphs": [
                        {
                            "balloonText": "[[title]] of [[category]]:[[value]]",
                            "bullet": "round",
                            "id": "AmGraph-1",
                            "title": "Neck",
                            "type": "smoothedLine",
                            "valueField": "column-1"
                        },
                        {
                            "balloonText": "[[title]] of [[category]]:[[value]]",
                            "bullet": "square",
                            "id": "AmGraph-2",
                            "title": "Arms",
                            "type": "smoothedLine",
                            "valueField": "column-2"
                        },
                        {
                            "balloonText": "[[title]] of [[category]]:[[value]]",
                            "bullet": "bubble",
                            "id": "AmGraph-3",
                            "minBulletSize": 3,
                            "title": "Waist",
                            "type": "smoothedLine",
                            "valueField": "column-3"
                        },
                        {
                            "balloonText": "[[title]] of [[category]]:[[value]]",
                            "bullet": "diamond",
                            "id": "AmGraph-4",
                            "title": "Hips",
                            "type": "smoothedLine",
                            "valueField": "column-4"
                        },
                        {
                            "balloonText": "[[title]] of [[category]]:[[value]]",
                            "bullet": "diamond",
                            "id": "AmGraph-5",
                            "title": "Legs",
                            "type": "smoothedLine",
                            "valueField": "column-5"
                        },

                    ],
                    "guides": [],
                    "valueAxes": [
                        {
                            "id": "ValueAxis-1",
                            "title": "Axis title",
                            "titleFontSize": 0,
                            "gridAlpha": 0.05,
                            "maximum": 40
                        }
                    ],
                    "allLabels": [],
                    "balloon": {},
                    "legend": {
                        "enabled": true,
                        "useGraphSettings": true
                    },
                    "titles": [
                        {
                            "id": "Title-1",
                            "size": 0,
                            "text": "Chart Title"
                        }
                    ],
                    "dataProvider": [
                        {
                            "category": "Jan",
                            "column-1": 16,
                            "column-2": 17,
                            "column-3": 20,
                            "column-4": 25,
                            "column-5": 28
                        },
                        {
                            "category": "Feb",
                            "column-1": 18,
                            "column-2": 19,
                            "column-3": 22,
                            "column-4": 30,
                            "column-5": 33
                        },
                        {
                            "category": "Mar",
                            "column-1": 15,
                            "column-2": 30,
                            "column-3": 18,
                            "column-4": 23,
                            "column-5": 32
                        },
                        {
                            "category": "Apr",
                            "column-1": 16,
                            "column-2": 18,
                            "column-3": 32,
                            "column-4": 34,
                            "column-5": 28
                        }
                    ]
                }
);
/*End of third row chart*/

/*This is for Third row chart*/
AmCharts.makeChart("chartdiv2-1",
                {
                    "type": "serial",
                    "categoryField": "category",
                    "startDuration": 1,
                    "autoMarginOffset": 0,
                    "marginBottom": 10,
                    "marginTop": 0,
                    "marginLeft": 50,
                    "fontSize": 11,
                    "categoryAxis": {
                        "gridPosition": "start",
                        "gridColor": "#FFFFFF"
                    },
                    "trendLines": [],
                    "graphs": [
                        {
                            "balloonText": "[[title]] of [[category]]:[[value]]",
                            "bullet": "round",
                            "id": "AmGraph-1",
                            "title": "systolic",
                            "type": "smoothedLine",
                            "valueField": "column-1"
                        },
                        {
                            "balloonText": "[[title]] of [[category]]:[[value]]",
                            "bullet": "square",
                            "id": "AmGraph-2",
                            "title": "diastolic",
                            "type": "smoothedLine",
                            "valueField": "column-2"
                        }
                    ],
                    "guides": [],
                    "valueAxes": [
                        {
                            "id": "ValueAxis-1",
                            "title": "Axis title",
                            "titleFontSize": 0,
                            "gridAlpha": 0.05,
                            "maximum": 150
                        }
                    ],
                    "allLabels": [],
                    "balloon": {},
                    "legend": {
                        "enabled": true,
                        "useGraphSettings": true
                    },
                    "titles": [
                        {
                            "id": "Title-1",
                            "size": 0,
                            "text": "Chart Title"
                        }
                    ],
                    "dataProvider": [
                        {
                            "category": "Jan",
                            "column-1": 120,
                            "column-2": 85
                        },
                        {
                            "category": "Feb",
                            "column-1": 110,
                            "column-2": 70
                        },
                        {
                            "category": "Mar",
                            "column-1": 121,
                            "column-2": 82
                        },
                        {
                            "category": "Apr",
                            "column-1": 126,
                            "column-2": 84
                        }
                    ]
                }
);
/*End of third row chart*/

/*This is for Third row chart*/
AmCharts.makeChart("chartdiv2-2",
                {
                    "type": "serial",
                    "categoryField": "category",
                    "startDuration": 1,
                    "autoMarginOffset": 0,
                    "marginBottom": 10,
                    "marginTop": 0,
                    "marginLeft": 50,
                    "fontSize": 11,
                    "categoryAxis": {
                        "gridPosition": "start",
                        "gridColor": "#FFFFFF"
                    },
                    "trendLines": [],
                    "graphs": [
                        {
                            "balloonText": "[[title]] of [[category]]:[[value]]",
                            "bullet": "round",
                            "id": "AmGraph-1",
                            "title": "graph 1",
                            "type": "smoothedLine",
                            "valueField": "column-1"
                        },
                        {
                            "balloonText": "[[title]] of [[category]]:[[value]]",
                            "bullet": "square",
                            "id": "AmGraph-2",
                            "hidden": true,
                            "title": "graph 2",
                            "type": "smoothedLine",
                            "valueField": "column-2"
                        }
                    ],
                    "guides": [],
                    "valueAxes": [
                        {
                            "id": "ValueAxis-1",
                            "title": "Axis title",
                            "titleFontSize": 0,
                            "gridAlpha": 0.05,
                            "maximum": 80
                        }
                    ],
                    "allLabels": [],
                    "balloon": {},
                    "legend": {
                        "enabled": false,
                        "useGraphSettings": true
                    },
                    "titles": [
                        {
                            "id": "Title-1",
                            "size": 0,
                            "text": "Chart Title"
                        }
                    ],
                    "dataProvider": [
                        {
                            "category": "Jan",
                            "column-1": 65
                        },
                        {
                            "category": "Feb",
                            "column-1": 60
                        },
                        {
                            "category": "Mar",
                            "column-1": 70
                        },
                        {
                            "category": "Apr",
                            "column-1": 72
                        }
                    ]
                }
);
/*End of third row chart*/

/*This is for Third row chart*/
AmCharts.makeChart("chartdiv2-3",
                {
                    "type": "serial",
                    "categoryField": "category",
                    "startDuration": 1,
                    "autoMarginOffset": 0,
                    "marginBottom": 10,
                    "marginTop": 0,
                    "marginLeft": 50,
                    "fontSize": 11,
                    "categoryAxis": {
                        "gridPosition": "start",
                        "gridColor": "#FFFFFF"
                    },
                    "trendLines": [],
                    "graphs": [
                        {
                            "balloonText": "[[title]] of [[category]]:[[value]]",
                            "bullet": "round",
                            "id": "AmGraph-1",
                            "title": "graph 1",
                            "type": "smoothedLine",
                            "valueField": "column-1"
                        },
                        {
                            "balloonText": "[[title]] of [[category]]:[[value]]",
                            "bullet": "square",
                            "id": "AmGraph-2",
                            "hidden": true,
                            "title": "graph 2",
                            "type": "smoothedLine",
                            "valueField": "column-2"
                        }
                    ],
                    "guides": [],
                    "valueAxes": [
                        {
                            "id": "ValueAxis-1",
                            "title": "Axis title",
                            "titleFontSize": 0,
                            "gridAlpha": 0.05,
                            "maximum": 500
                        }
                    ],
                    "allLabels": [],
                    "balloon": {},
                    "legend": {
                        "enabled": false,
                        "useGraphSettings": true
                    },
                    "titles": [
                        {
                            "id": "Title-1",
                            "size": 0,
                            "text": "Chart Title"
                        }
                    ],
                    "dataProvider": [
                        {
                            "category": "Jan",
                            "column-1": 440
                        },
                        {
                            "category": "Feb",
                            "column-1": 410
                        },
                        {
                            "category": "Mar",
                            "column-1": 460
                        },
                        {
                            "category": "Apr",
                            "column-1": 480
                        }
                    ]
                }
);
/*End of third row chart*/