$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var HTTPObj = function(data, HTTPMethod, apiurl, SuccessCallback){
    this.postdataobj = data;
    this.HTTPMethod = HTTPMethod;
    this.apiurl = apiurl;
    this.SuccessCallback = SuccessCallback;
};

function PriorityQueueListener(){

    jQuery(document).bind('wakeup_queue',   function() {
        if(!priorityQueue.isQueueRunning){
            sendHTTPRequestFromQueueObj(priorityQueue.dequeue());
            if(priorityQueue.size()> 1){
                sendHTTPRequestFromQueueObj(priorityQueue.dequeue());
            }
        }
    });
    this.wakeup_queue_again = function(){
        sendHTTPRequestFromQueueObj(priorityQueue.dequeue());
        if(priorityQueue.size()> 1){
            sendHTTPRequestFromQueueObj(priorityQueue.dequeue());
            //sendHTTPRequestFromQueueObj(priorityQueue.dequeue());
        }
    };
    jQuery(document).bind('wakeup_queue_again',   function() {
        sendHTTPRequestFromQueueObj(priorityQueue.dequeue());
        if(priorityQueue.size()> 1){
            sendHTTPRequestFromQueueObj(priorityQueue.dequeue());
            //sendHTTPRequestFromQueueObj(priorityQueue.dequeue());
        }
    });
}

var listener = new PriorityQueueListener();

function HTTPRequest(data, HTTPMethod, apiurl, SuccessCallback,priority) {
    var postdataobj = data;
    priority = priority || 5;
    priorityQueue.enqueue(new HTTPObj(postdataobj, HTTPMethod, apiurl, SuccessCallback),priority);
}

function sendHTTPRequestFromQueueObj(httpObj){

    if(httpObj == null){
        return;
    }
    //console.log("********** request ::"+httpObj.element.apiurl+"::" +new Date().getTime());
    $.ajax({
        type: httpObj.element.HTTPMethod,
        url: httpObj.element.apiurl,
        data: JSON.stringify(httpObj.element.postdataobj),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: false,
        success: function(data, status, xhr) {
            //jQuery(document).trigger('wakeup_queue_again');
            //console.log("********** response::"+httpObj.element.apiurl+"::"+ new Date().getTime());
            listener.wakeup_queue_again();
            httpObj.element.SuccessCallback(data, status, xhr);


        },
        error: function(error, errorText) {
           // jQuery(document).trigger('wakeup_queue_again');
            listener.wakeup_queue_again();
            if (error.status == 204) {
                httpObj.element.SuccessCallback(null, errorText, null);
            } else {
                //alert("HTTPRequest Error :: " + error.status + ": " + error.responseText + ":" + errorText);
            }

        }
    });
}

// Flash Messages

function showFlashMessage(title,text,type){
    swal({
        title: title,
        text: text,
        type: type,
        timer: 3000,
        showConfirmButton: false
    });
}

function showFlashMessageOverlay(title,text,type){
    swal({
        title: title,
        text: text,
        type: type,
        confirmButtonText: 'OK'
    });
}

function showFlashMessageConfirmation(title,text,type,callback){
    swal({
            title: title,
            text: text,
            type: type,
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        },
        callback
    );
}