angular.module('app.routes').config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/list_plans');

    $stateProvider

        .state('list_plans', {
            url: '/list_plans',
            templateUrl: '/admin/assign_plans_display',
            controller : 'AssignPlanController',
            controllerAs : 'meal_plan'
        })
         .state('view_complete_plan', {
            url: '/view_complete_plan/:id',
            templateUrl: '/admin/assign_complete_plan_details',
            controller : 'ViewPlanController',
            controllerAs : 'view_plan'
        })
         .state('view_day_plan', {
            url: '/view_day_plan/:id',
            templateUrl: '/admin/assign_day_plan_details',
            controller : 'DayViewPlanController',
            controllerAs : 'view_plan'
        });


});

angular.module('app.core').controller('AssignPlanController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){
    var vm = this;
     vm.gender = $scope.ap.gender;
     vm.user_id=$scope.ap.user_id;
     vm.full_name = $scope.ap.full_name;
     vm.showBtnAssign = [];
     vm.showBtnUnAssign = [];
     vm.assignPlanId = null;

     vm.getMealPlans = function(){
             ShowService.query('api/v2/admin/meal_plan/get_meal_plans?gender='+vm.gender).then(function(results){
                   if(results.error==false){
                     vm.meal_plans = results.response;
                       angular.forEach(results.response, function(value, key) {
                        vm.showBtnAssign[value.id] =true;
                        vm.showBtnUnAssign[value.id] =false;
                      });
                      vm.getUserMealPlan(vm.user_id);
                   }

              });

    }

    vm.assignPlan = function(plan){

        var data = {
                "user_id": vm.user_id,
                "meal_plan_id": plan.id,
                "user_agent": "server"
            };
            var plan_type_name = "Complete Plan";

            var plan_type = plan.plan_type;
            if(plan_type == 'day_plan'){
              plan_type_name = "Day Plan";
            }


         // console.log(data);return false;
        StoreService.save('api/v2/admin/meal_plan/assign_plan_to_user', data).then(function(result) {
            if(result.error == false) {
              showFlashMessage('Assigned', plan_type_name+' Assigned Successfully','success');
              vm.showBtnAssign[plan.id] =false;
              vm.showBtnUnAssign[plan.id] =true;
              vm.showBtnAssign[vm.assignPlanId]= true;
              vm.showBtnUnAssign[vm.assignPlanId]= false;
              vm.assignPlanId = plan.id;
          }
        });
    }

    vm.getUserMealPlan = function(user_id){

        ShowService.get('api/v2/admin/assign_plan/get_user_plan', user_id).then(function(results) {
          var user_meal_plan = results.response.user_meal_plan;
         // console.log(user_meal_plan)
          if(user_meal_plan){
             vm.assignPlanId = user_meal_plan.meal_plan_id;
             vm.showBtnAssign[user_meal_plan.meal_plan_id] =false;
             vm.showBtnUnAssign[user_meal_plan.meal_plan_id] =true;
            // console.log(vm.showBtnAssign);
          }

        });

    }

   vm.unassignPlan = function(plan){

     var data = {
                "user_id": vm.user_id,
                "meal_plan_id": plan.id,
                "user_agent": "server"
            };

            var plan_type_name = "Complete Plan";

            var plan_type = plan.plan_type;
            if(plan_type == 'day_plan'){
              plan_type_name = "Day Plan";
            }
    var msg = "Are you sure want to unassign this meal plan ?";

    showFlashMessageConfirmation("Unassign",msg,"info",function(result) {
          if (result) {
                StoreService.save('api/v2/admin/meal_plan/unassign_plan_to_user', data).then(function(results) {

                   vm.showBtnAssign[plan.id] =true;
                   vm.showBtnUnAssign[plan.id] =false;
                   showFlashMessage('Unassigned', plan_type_name+' Unassigned successfully','success');
                });
            }
    });

   }

}]);