angular.module('app.core').controller('DealFrontController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){
    var vm = this;
    $('.back-top').trigger('click');
    
    vm.getDeals = function(){

        ShowService.query('api/v3/deal/getlist_front').then(function(result){
               
            vm.deals = result.response;
        });
    }

 }]);