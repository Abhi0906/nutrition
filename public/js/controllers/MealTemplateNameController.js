angular.module('app.core').controller('MealTemplateNameController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){
    var vm = this;
    vm.template_data = [];
    vm.template_id = $stateParams.id;
    
    // Filters
    $scope.maleTemplateFilter = function(item){
        return item.template_gender === 'Male';
    }

    $scope.femaleTemplateFilter = function(item){
        return item.template_gender === 'Female';
    }
    // end: Filters

    if(vm.template_id){
        vm.template_data = [];
        ShowService.get('api/v3/recommended_meal/template', vm.template_id).then(function(result){
           
            vm.template_data = result.response.template;
        });   
    }

    vm.getTemplateNames = function(){
        vm.template_names = [];

        ShowService.query('api/v3/recommended_meal/template_names').then(function(result){
           
            vm.template_names = result.response.template_names;
        });
    }
    
    vm.saveTemplateName = function(){
        console.log(vm.template_data);

        var data = {
            "template_name": vm.template_data.template_name,
            "template_gender": vm.template_data.template_gender,
            "user_agent": "server"
        };
        
        StoreService.save('api/v3/recommended_meal/create_template_name', data).then(function(result){
            
            if(result.error==false){

                showFlashMessage('Created','Template name created successfully','success');
                $state.go("template-names");
            }
            else{
                console.log(result.error);
            }
            
        });
    }

    vm.setDefaultTemplate = function(id){
        var url = "api/v3/recommended_meal/set_default_template";

        var msg = "Are you sure you want to set this template as default ?";
        showFlashMessageConfirmation("Confirm",msg,"info",function(result) {
            if(result){
                StoreService.update(url,id,null).then(function(result){
                    if(result.error==false){

                        console.log(result.msg);
                        showFlashMessage('Success','Default template Set successfully','success');
                    }
                    else{
                        showFlashMessage('Error','Default template not Set successfully','error');
                        console.log(result.error);
                    }
                });
            }
                
        });
    }

    vm.updateTemplate = function(){
        var data = {
            "template_name": vm.template_data.template_name,
            "template_gender": vm.template_data.template_gender,
            "user_agent": "server"
        };

        var url = "api/v3/recommended_meal/update_meal_template";
        StoreService.update(url, vm.template_id, data).then(function(result){
            if(result.error==false){
                showFlashMessage('Updated','Template name updated successfully','success');
                $state.go("template-names");
                console.log(result.msg);
            }
            else{
                console.log(result.error);
            }
        });
    }

    vm.deleteTemplate = function(template){

        if(template.is_default == 1){ return false; }
        var id = template.id;
        
        var msg = "Are you sure you want to Delete this template meal? In case this template is assigned to any patient, then default template meal will be attached to those patients automatically. ?";
      
        showFlashMessageConfirmation("Confirm", msg, "info", function(result) {
              if (result) {

                ShowService.remove('api/v3/recommended_meal/delete_meal_template', id).then(function(result){
                    if(result.error==false){
                        var index = vm.template_names.indexOf(template);
                        vm.template_names.splice(index, 1);
                        showFlashMessage('Delete','Template deleted successfully','success');
                    }
                    
                });

              }
        });
        
    }

}]);