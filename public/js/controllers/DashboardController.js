angular.module('app.core').controller('DashboardController',['ShowService','$scope','StoreService', '$rootScope','$timeout','uiCalendarConfig', function(ShowService,$scope,StoreService, $rootScope,$timeout,uiCalendarConfig){
    var vm = this;
    vm.userId = null;
    vm.displayServerError = false;
    vm.displayServerMessage = null;
    vm.ageValidation =false;
    vm.percentage_food = null;
    vm.daily_cal_date = moment().format('MMMM DD, YYYY');
    vm.log_waterloader = false;
    vm.add_log_water_display = true;
    vm.eventSources =[];
    vm.format = 'MM-dd-yy';
    vm.daily_calorie_date = new Date();
    var today = moment().format('MM/DD/YYYY');

     $timeout(function() {
       if($scope.dash_calorie && typeof $scope.dash_calorie.user_birth_date != "undefined"){
        vm.birth_date = new Date($scope.dash_calorie.user_birth_date);
      }
    },2000);

    vm.dateOptions = {
      showWeeks:false
    };

    vm.daily_calorie_date_popup = {
        opened: false
    };

    vm.daily_calorie_date_open = function() {
        vm.daily_calorie_date_popup.opened = true;
    };

    vm.daily_calorie_exercise_date = new Date();

    vm.daily_calorie_exercise_date_popup = {
        opened: false
    };

    vm.daily_calorie_exercise_date_open = function() {
        vm.daily_calorie_exercise_date_popup.opened = true;
    };

    vm.profile_data_birth_date_popup = {
        opened: false
    };

    vm.profile_data_birth_date_open = function() {
        vm.profile_data_birth_date_popup.opened = true;
    };

    vm.graph_range =[{id:'last_week', title:'Last week'},
                        {id:'last_month', title:'Last Month'},
                        {id:'last_four_months', title:'Last Quarter'},
                        {id:'last_six_months', title:'Last 6 months'},
                        {id:'last_year', title:'Last year'}
                      ];

    $rootScope.$on('updatedDate', function (e,args) {

       vm.getCaloriesLog( moment(args).format('DD-MM-YYYY') );
    });

  vm.getCalories = function(){
    ShowService.get('api/v3/patient/calories',vm.userId).then(function(results){
        vm.calories = results.response;
    });
   }

   vm.getCaloriesLog = function(req_date){

      //angular.element(document.querySelector('#cal_summary_datepicker input[type="text"]')).removeClass("form-control");
     // angular.element(document.querySelector('#cal_summary_datepicker input[type="text"]')).addClass("cal_sammary_date_input");

      var param = vm.userId;
      if(req_date){
          req_date = moment(req_date).format('YYYY-MM-DD');
          param = vm.userId+'?req_date='+req_date;
      }

      ShowService.get('api/v3/patient/calories', param).then(function(results){

        var calories = results.response;

        vm.calories_consumed_persent = calories.calories_consumed_persent;
        vm.calories_consumed = calories.calories_consumed;
        vm.calorie_goal = calories.calorie_goal;
        vm.calories_burned_persent = calories.calories_burned_persent;
        vm.calories_burned = calories.calories_burned;
        vm.exercise_goal = calories.exercise_goal;
        vm.total_calories_persent = calories.total_calories_persent;
        vm.total_calories = calories.total_calories;
        vm.total_calorie_goal = calories.total_calorie_goal;


        if(vm.calories_consumed_persent > 100){
            vm.calories_consumed_persent = 100;
        }
        else if(vm.calories_consumed_persent < 0){
          vm.calories_consumed_persent = 0;
        }

        guageMeter('calories-gauge', vm.calories_consumed_persent);

        if(vm.calories_burned_persent > 100){
            vm.calories_burned_persent = 100;
        }
        else if(vm.calories_burned_persent < 0){
          vm.calories_burned_persent = 0;
        }

        guageMeter('calories-burned', vm.calories_burned_persent);

        if(vm.total_calories_persent > 100){
            vm.total_calories_persent = 100;
        }
        else if(vm.total_calories_persent < 0){
          vm.total_calories_persent = 0;
        }

        guageMeter('calories_total', vm.total_calories_persent);

        //display waterintake cups
        var total_waterlog = results.response.total_waterlog;

        vm.water_goal_cup = results.response.water_goal;

        var total_water_to_drink = results.response.remaining_waterToDrink;

        if(total_water_to_drink>0){
          vm.total_water_to_drink = total_water_to_drink;
        }
        else{
          vm.total_water_to_drink = 0;
        }

        vm.cupDisplay = '';
        for(i = 0; i < vm.water_goal_cup; i++){
            var font_change = 'glyphicon glyphicon-glass  glyphicon-glass-grey';
            if(i+1 <= total_waterlog){

              font_change = "glyphicon glyphicon-glass";
            }

            vm.cupDisplay += '<span class="'+font_change+'" ></span> ';
        }

        //end: display waterintake cups
     });
   }

   function calculatePercentage(goal,logged){
      var percentage = 0;
      if(goal){
          percentage = (logged/goal)*100;
      }

      return percentage;
    }


   $scope.update_email = function() {
        $scope.myVar = !$scope.myVar;
        vm.email="";
        vm.emailValidation= false;
        vm.emailSuccessMsg = false;
    };

   vm.update_user_email = function(){
    var data = {'email':vm.email};
    StoreService.update('api/v3/patient/change_email',vm.userId,data).then(function(result) {
    if(result.error == false) {
     vm.emailValidation = false;
     vm.emailSuccessMsg = result.response.message;
     }else{
       vm.emailValidation = true;
       vm.emailSuccessMsg = null;
       vm.emailValidationMesg = result.response.message;
    }
     }, function(error) {
              console.log(error);
    });
   }

   vm.updateUserInfo = function(){
      var data = {};
      data = vm.profile_data;
      data.user_id = vm.userId;
      data.birth_date =  moment(vm.birth_date).format('YYYY-MM-DD');

      var years = moment().diff(data.birth_date, 'years');

      data.activity_id=1
      data.activity_value =1.2

      if(years > 13){

       StoreService.save('api/v3/patient/updateProfile',data,vm.userId).then(function(result) {
        if(result.error == false){
            vm.getCalories();
             $('#profile_modal').modal('hide');
            }
        },function(error) {
       console.log(error);
      });
     }else{

            vm.ageValidation = true;
     }
   }

   vm.resend_email = function(){
      vm.emailValidation_email= false;
      vm.emailSuccessMsg_email = false;
      ShowService.get('api/v3/patient/resend_email',vm.userId).then(function(result) {
      if(result.error == false) {
             vm.emailValidation_email = false;
             vm.emailSuccessMsg_email = result.response.message;
       }else{
             vm.emailValidation_email = true;
             vm.emailSuccessMsg_email = null;
             vm.emailValidation_email = result.response.message;
      }
       }, function(error) {
                console.log(error);
      });
   }

   vm.dateNextPrevious = function(btn, date, type){



      date = new Date(date);
      var today = new Date();

      if(btn=='prev'){

          date.setTime(date.getTime() - (1000*60*60*24))

      }

      if(btn=='next'){

          date.setTime(date.getTime() + (1000*60*60*24))

      }

      if(date.setHours(0,0,0,0) <= today.setHours(0,0,0,0)){


        /*if(date.setHours(0,0,0,0) == today.setHours(0,0,0,0)){

        }
        else{

        }*/

        if(type=="exercise"){
          vm.getUserLoggedExercise(date);
          vm.daily_calorie_exercise_date = date;
        }
        else if(type=="calorie"){
          vm.getCaloriesLog(date);
          vm.daily_calorie_date = date;
        }

      }

   }

  /* vm.CompCalendar_calories = function(req_month, req_year){
    var param = vm.userId;
      if(req_month && req_year){
          param = vm.userId+'?req_month='+req_month+'&req_year='+req_year;
      }

      ShowService.get('api/v3/patient/monthly_calories', param).then(function(results){


        //var monthly_calories = JSON.stringify(results.response);

        var values = results.response;
        var eventdata = [];
        angular.forEach(values, function(value, key) {

          eventdata.push({
                            "calory":value.total_consumed,
                            "start":value.date,
                            "diff_text":value.diff_text,
                            "diff_text_color":value.diff_text_color,
                            "diff":value.diff,
                            "water_intake":value.water_intake
                          });

        });

          vm.monthly_calories = eventdata;
          vm.calory_calendar(vm.monthly_calories);
      });
   }*/

      vm.calory_calendar = function(data) {


        /* Initialize FullCalendar */
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        $('#calendar').fullCalendar({
            header: {
                left:   'title',
                center: '',
                right:  'prev,next'
            },
            height: 760,
            events: vm.monthly_calories,

            eventRender: function(event, element, view) {
                               return $(
                                        '<div class="tabletd-div cal_sammary_data"><p>Calory Total</p> <p><b>' +
                                              event.calory +
                                              '</b></p>'+
                                              '<p style="color:'+event.diff_text_color+';">'+event.diff_text+' = ' + event.diff + '</p>'+

                                          '<p>Water = '+event.water_intake+' Cups</p></div>');
                      },

           /*eventClick: function(calEvent, jsEvent, view) {
            console.log("calEvent");
            console.log(calEvent);

            console.log("jsEvent");
            console.log(jsEvent);
            alert("clicked");
               //window.location = "http://www.domain.com?start=" + calEvent.start;

            }*/
           });

      }

      vm.getProfile= function(){

          ShowService.get('api/v3/user/profile',vm.userId).then(function(results){
           vm.profile_data = results.response;
         });
      }

      vm.openLogWaterForm = function(){
          vm.water_intake = "";
         $scope.LogWaterIntake.$setUntouched();
        $('#log_water_tmplt').modal('show');
      }

      vm.logWaterIntake = function(){
        vm.log_waterloader = true;
        var data = {};
        data.user_id = vm.userId;
        data.log_date = moment().utc().format('YYYY-MM-DD');
        data.guid_server = true;
        data.log_water = vm.water_intake;

        StoreService.save('api/v3/patient/water_intake',data).then(function(result) {
          if(result.error == false){

              var total_water_to_drink = vm.total_cup - result.response.total_waterlog;
              if(total_water_to_drink>0){
                vm.total_water_to_drink = total_water_to_drink;
              }
              else{
                vm.total_water_to_drink = 0;
              }

              vm.cupDisplay = '';
              for(i = 0; i < vm.total_cup; i++){
                  var font_change = 'fa-square-o';
                  if(i+1 <= result.response.total_waterlog){

                    font_change = "fa-square";
                  }

                  vm.cupDisplay += '<small class="fa '+font_change+' fa-2x" ></small> ';
              }

              vm.log_waterloader = false;
              $('#log_water_tmplt').modal('hide');
          }
        },function(error) {
          console.log(error);
        });
      }

      vm.monthlyCalorieSummary = function(calendar_date){

      var param = vm.userId;
      var month = moment(calendar_date).format('MM');
      var year = moment(calendar_date).format('YYYY');
      param = vm.userId+'?req_month='+month+'&req_year='+year;
      var eventdata = [];
      ShowService.get('api/v3/food/monthly_calorie_summary', param).then(function(results){

           // date wise data
           var values = results.response.date_wise;

           angular.forEach(values, function(value, key) {

                      eventdata.push({
                          "calory":value.total_consumed,
                          "start":value.date,
                          "diff_text":value.diff_text,
                          "diff_text_color":value.diff_text_color,
                          "diff":value.diff,
                          "water_intake":value.water_intake
                        });

            });
           // end date wise data

           // weekly wise data
           if(results.response.average.week_wise_avg){
             var weekly_avg_data = results.response.average.week_wise_avg;

              angular.forEach(weekly_avg_data, function(value, key) {
                var html_data_weekly = "";
                $('#week_average .week_'+ key).html(html_data_weekly);
                  if(value.length == undefined){

                      html_data_weekly = "<p>Calorie Average</p><p>"+value.total_consumed_weekly_avg+"/day</p>"+
                                    "<p class="+value.weekly_diff_text_class_avg+">"+
                                    value.weekly_diff_text_avg+" = "+value.weekly_diff_avg+"</p>"+
                                    "<p>Water = "+value.total_weekly_water_intake_avg+" Cups</p>";

                      $('#week_average .week_'+ key).html(html_data_weekly);
                  }
              });
           }
            // end weekly wise data

            // monthly data
            if(results.response.average.monthly_avg){
              var monthly_avg = results.response.average.monthly_avg;

              var monthly_avg_html_data = "<p>Calorie Average</p>"
                          +"<p><strong>"+ monthly_avg.monthly_consumed_avg+"/day</strong></p>"
                          +"<p class="+ monthly_avg.monthly_diff_text_class_avg+">"
                                + monthly_avg.monthly_diff_text_avg+ "="
                                + monthly_avg.monthly_diff_avg+"</p>"
                          +"<p>Water = "+monthly_avg.monthly_water_avg+" Cups</p>";

              $('#month-average-calorie').html(monthly_avg_html_data);
            }
            // end monthly data
      });

      vm.events = eventdata;
     // vm.eventSources = [vm.events];


       $timeout(function() {
            if (uiCalendarConfig.calendars['calorieCalendar']) {
               angular.forEach(eventdata, function(value, key) {
               uiCalendarConfig.calendars['calorieCalendar'].fullCalendar('renderEvent',value,false);
              });
            }
         },2000);

    }

    vm.renderCalorieView = function(view,element){
      // console.log("View Changed: ", view.visStart, view.visEnd, view.start, view.end);
        var date = new Date(view.calendar.getDate());
        vm.monthlyCalorieSummary(date);

    }

    vm.eventRender = function(event, element, view ){

         return $(
                  '<div class="tabletd-div cal_sammary_data"><p>Calorie Total</p> <p><b>' +
                        event.calory +
                        '</b></p>'+
                        '<p style="color:'+event.diff_text_color+';">'+event.diff_text+' = ' + event.diff + '</p>'+

                    '<p>Water = '+event.water_intake+' Cups</p></div>');
    }

    vm.uiConfig = {
        calendar:{
                editable: false,
                height:"100%",
                theme: true,
                header: {
                    right: 'title',
                    left:'prev,next',
                    center:false,

                },
               eventRender: vm.eventRender,
               viewRender: vm.renderCalorieView,

           }
    };

    vm.getUserLoggedExercise = function(req_date){

      var params = vm.userId;

      if(req_date){
        req_date = moment(req_date).format('YYYY-MM-DD');
        params = params+'?req_date='+req_date;
      }


      ShowService.get('api/v3/exercise/user_logged_exercise', params).then(function(results){
        var user_logged_exercise = results.response;
        vm.cardio = user_logged_exercise.Cardio;
        vm.resistance = user_logged_exercise.Resistance;

      });
    }

    vm.monthlyCalorieBurnedSummary = function(calendar_date){

      var param = vm.userId;
      var month = moment(calendar_date).format('MM');
      var year = moment(calendar_date).format('YYYY');


      param = vm.userId+'?req_month='+month+'&req_year='+year;
      var eventdata = [];
      ShowService.get('api/v3/exercise/monthly_calorie_summary', param).then(function(results){

           // date wise data
           var values = results.response.date_wise;

           var week_wise_avg = results.response.week_wise_avg;

           vm.week_wise_avg_html = {};
           angular.forEach(week_wise_avg, function(value, key) {

           var html_data_weekly = "";
            $('#week_average_exercise .week_'+ key).html(html_data_weekly);
             if(value.length == undefined){
                   html_data_weekly = "Calorie Burned = <span class="+value.weekly_calories_class_avg+">" +
                                                                            value.total_calories_weekly_avg +
                                                                            "<span class="+value.weekly_class_avg+"><br>"+
                                                                            value.diff_text+ " = "+value.week_total_deficit+"</span>";



               $('#week_average_exercise .week_'+ key).html(html_data_weekly);
             }
           });

           vm.monthly_avg_calories = results.response.monthly_avg.monthly_avg_calories;
           vm.monthly_calories_class_avg = results.response.monthly_avg.monthly_calories_class_avg;
           vm.monthly_month_calories = results.response.monthly_avg.monthly_month_calories;
           vm.diff_text = results.response.monthly_avg.diff_text;
           vm.monthly_class_avg = results.response.monthly_avg.monthly_class_avg;


           vm.monthlyExerciseCalories  = '<p>Calorie Burned Average = <span class='+vm.monthly_calories_class_avg +'>'
                                                                    + vm.monthly_avg_calories +"<br>"
                                                                    +"<span class="+vm.monthly_class_avg +">"
                                                                    +vm.diff_text+" = "+
                                                                    vm.monthly_month_calories +'</p></span>';

           angular.forEach(values, function(value, key) {

                      eventdata.push({
                          "calorie_burned":value.total_calorie_burned,
                          "start":value.date,
                          "diff_text":value.diff_text,
                          "diff_text_color":value.diff_text_color,
                          "diff":value.diff,

                        });

            });
           // end date wise data
       });
      vm.events = eventdata;
     // vm.eventSourcesExercise = [vm.events];


       $timeout(function() {
            if (uiCalendarConfig.calendars['exerciseCalendar']) {
               angular.forEach(eventdata, function(value, key) {
               uiCalendarConfig.calendars['exerciseCalendar'].fullCalendar('renderEvent',value,false);
              });
            }
         },2000);

    }

    vm.renderExerciseView = function(view,element){
      // console.log("View Changed: ", view.visStart, view.visEnd, view.start, view.end);
        var date = new Date(view.calendar.getDate());
        vm.monthlyCalorieBurnedSummary(date);
    }

   vm.eventRenderExercise = function(event, element, view ){

         return $(
                  '<div class="tabletd-div cal_sammary_data"><p>Calorie Burned Total</p> <p><b>' +
                        event.calorie_burned +
                        '</b></p>'+
                        '<p style="color:'+event.diff_text_color+';">'+event.diff_text+' = ' + event.diff + '</p>'+

                    '</div>');
    }


    vm.uiConfigExercise = {
        calendar:{
                editable: false,
                height:"100%",
                theme: true,
                header: {
                    right: 'title',
                    left:'prev,next',
                    center:false
                },
               eventRender: vm.eventRenderExercise,
               viewRender: vm.renderExerciseView,

           }
    };

    vm.getUserhealthData = function(){

      var params = vm.userId;

      ShowService.get('api/v3/goals', vm.userId).then(function(results){
        var user_health_data = results.response;
        if(results.response.hasOwnProperty('user_health_data')){
        vm.last_weight = user_health_data.last_weight.current_weight;

        vm.weight_goal = user_health_data.weight_goal.weight_g;
        vm.weight_percent = user_health_data.weight_goal.weight_percentage;
        guageMeter("bodyweight-gauge", vm.weight_percent);

        vm.last_fat = user_health_data.last_weight.body_fat;
        vm.fat_goal = user_health_data.weight_goal.body_fat_g;
        vm.fat_percent = user_health_data.weight_goal.body_fat_percentage;
        guageMeter("bodyfat-gauge", vm.fat_percent);

        vm.last_weight_fat_date = user_health_data.last_weight.log_date;

        vm.last_body_measurements = user_health_data.last_body_measurements;
        vm.body_measurements_goal = user_health_data.bm_goal;
        guageMeter("neck-gauge", vm.body_measurements_goal.neck_percentage);
        guageMeter("arms-gauge", vm.body_measurements_goal.arms_percentage);
        guageMeter("waist-gauge", vm.body_measurements_goal.waist_percentage);
        guageMeter("hips-gauge", vm.body_measurements_goal.hips_percentage);
        guageMeter("legs-gauge", vm.body_measurements_goal.legs_percentage);

        vm.last_blood_pressure = user_health_data.last_blood_pressure;
        vm.bp_goal = user_health_data.bp_goal;
        guageMeter("bloodpresure-systolic-gauge", vm.bp_goal.systolic_percentage);
        guageMeter("bloodpresure-diastolic-gauge", vm.bp_goal.diastolic_percentage);

        guageMeter("hartrate-gauge", vm.bp_goal.pulse_percentage);

        vm.last_pulse = user_health_data.last_pulse;

        vm.last_sleep = user_health_data.last_sleep;
        guageMeter("sleep-gauge", vm.last_sleep.last_progress);
       }
      });
    }

    vm.displayCharts = function(date_range, type){
          var params = vm.userId;
            params = params+'?date_range='+date_range;
        ShowService.get('api/v3/reports/display_charts',params).then(function(result){

          var weight_chart_data = result.response.weight_chart;
          var body_fat_chart_data = result.response.body_fat_chart;
          var bp_chart_data = result.response.bp_chart;
          var pulse_chart_data = result.response.pulse_chart;
          var bm_chart_data = result.response.bm_chart;
          var sleep_chart_data =result.response.sleep_chart;

          if(type=='outer_measurements'){
            vm.displayWeightChart(weight_chart_data);
            vm.displayBodyFatChart(body_fat_chart_data);
            vm.displayBmChart(bm_chart_data);

          }
          else if(type=='inner_measurements'){
            vm.displayBpChart(bp_chart_data);
            vm.displayPulseChart(pulse_chart_data);
            vm.displaySleepChart(sleep_chart_data);

          }
          else{
            vm.displayWeightChart(weight_chart_data);
            vm.displayBodyFatChart(body_fat_chart_data);
            vm.displayBpChart(bp_chart_data);
            vm.displayPulseChart(pulse_chart_data);
            vm.displayBmChart(bm_chart_data);
            vm.displaySleepChart(sleep_chart_data);
          }


       });
      }

    vm.saveReferFriend = function(){
      var refer_data = {};
      refer_data.user_id = vm.userId;
      refer_data.first_name = vm.friend_data.firstname;
      refer_data.last_name = vm.friend_data.lastname;
      refer_data.email = vm.friend_data.email;
      refer_data.phone_number = vm.friend_data.phonenumber;
      StoreService.save('api/v3/referfriend/refere_friend', refer_data).then(function(result) {
      if(result.error == false) {

          }
      });
     }

    vm.displayWeightChart = function(data){

        var data_date_format = data.chart_setting.data_date_format;
        var data_provider = data.chart_data;

        AmCharts.makeChart("weight-chart",
            {
              "type": "serial",
              "categoryField": "date",
              "dataDateFormat": data_date_format,
              "theme": "default",
              "categoryAxis": {

                "parseDates": true
              },
              "trendLines": [],
              "graphs": [
                {
                  "bullet": "round",
                  "columnWidth": 0,
                  "fixedColumnWidth": 0,
                  "fontSize": 0,
                  "id": "AmGraph-1",
                  "lineColor": "#1294F2",
                  "lineThickness": 1,
                  "title": "Weight",
                  "valueField": "weight"
                }
              ],
              "valueAxes": [
                {
                  "id": "ValueAxis-1",
                  "title": "lbs"
                }
              ],
              "legend": {
            "enabled": true,
            "useGraphSettings": true
          },
              "allLabels": [],
              "balloon": {},
              "dataProvider": data_provider
            }
        );

    }

    vm.displayBodyFatChart = function(data){

        var data_date_format = data.chart_setting.data_date_format;
        var data_provider = data.chart_data;
        AmCharts.makeChart("body-fat-chart",
            {
              "type": "serial",
              "categoryField": "date",
              "dataDateFormat": data_date_format,
              "theme": "default",
              "categoryAxis": {

                "parseDates": true
              },
              "trendLines": [],
              "graphs": [
                {
                  "bullet": "round",
                  "columnWidth": 0,
                  "fixedColumnWidth": 0,
                  "fontSize": 0,
                  "id": "AmGraph-1",
                  "lineColor": "#1294F2",
                  "lineThickness": 2,
                  "title": "Body Fat",
                  "valueField": "body_fat"
                }
              ],
              "valueAxes": [
                {
                  "id": "ValueAxis-1",
                  "title": "Percent"
                }
              ],
              "legend": {
            "enabled": true,
            "useGraphSettings": true
          },
              "allLabels": [],
              "balloon": {},
              "dataProvider": data_provider
            }
        );

    }

    vm.displayBpChart = function(data){

      var data_date_format = data.chart_setting.data_date_format;
      var data_provider = data.chart_data;

     AmCharts.makeChart("bp-chart",
        {
          "type": "serial",
          "categoryField": "date",
          "dataDateFormat": data_date_format,
          "handDrawScatter": 1,
          "processCount": 998,
          "svgIcons": false,
          "tapToActivate": false,
          "theme": "default",
          "categoryAxis": {

            "parseDates": true
          },
          "trendLines": [],
          "graphs": [
            {
              "bullet": "round",
              "fixedColumnWidth": 0,
              "id": "systolic-1",
              "lineColor": "#61AB00",
              "lineThickness": 2,
              "tabIndex": -5,
              "title": "Systolic",
              "type": "smoothedLine",
              "valueField": "systolic"
            },
            {
              "bullet": "round",
              "color": "#FF0000",
              "fillColors": "#FF0000",
              "fixedColumnWidth": 0,
              "fontSize": 1,
              "id": "AmGraph-2",
              "lineColor": "#FF0000",
              "lineThickness": 2,
              "maxBulletSize": 49,
              "tabIndex": 0,
              "title": "Diastolic",
              "valueField": "diastolic"
            }
          ],
          "valueAxes": [
            {
              "id": "ValueAxis-1",
              "title": "mmHg",
              "titleColor": "#1ADACA"
            }
          ],
            "legend": {
        "enabled": true,
        "useGraphSettings": true
        },
            "allLabels": [],
            "balloon": {},
            "dataProvider": data_provider
          }
      );

    }

    vm.displayPulseChart = function(data){

      var data_date_format = data.chart_setting.data_date_format;
      var data_provider = data.chart_data;

       AmCharts.makeChart("pulse-chart",
            {
              "type": "serial",
              "categoryField": "date",
              "dataDateFormat": data_date_format,
              "handDrawScatter": 1,
              "processCount": 998,
              "svgIcons": false,
              "tapToActivate": false,
              "theme": "default",
              "categoryAxis": {
                "parseDates": true
              },
              "trendLines": [],
              "graphs": [
                {
                  "bullet": "round",
                  "fixedColumnWidth": 0,
                  "id": "pulse-1",
                  "lineColor": "#61AB00",
                  "lineThickness": 2,
                  "tabIndex": -5,
                  "title": "Pulse",
                  "type": "smoothedLine",
                  "valueField": "pulse"
                }

              ],
              "valueAxes": [
                {
                  "id": "ValueAxis-1",
                  "title": "bpm",
                  "titleColor": "#1ADACA"
                }
              ],
                "legend": {
            "enabled": true,
            "useGraphSettings": true
          },
              "allLabels": [],
              "balloon": {},
              "dataProvider": data_provider
            }
        );

    }

    vm.displayBmChart = function(data){

          var data_date_format = data.chart_setting.data_date_format;
          var data_provider = data.chart_data;
            AmCharts.makeChart("all-bm-chart",
              {
                "type": "serial",
                "categoryField": "date",
                "dataDateFormat": data_date_format,
                "theme": "default",
                "categoryAxis": {

                  "parseDates": true
                },
                "trendLines": [],
                "graphs": [
                  {
                    "bullet": "round",
                    "columnWidth": 0,
                    "fixedColumnWidth": 0,
                    "fontSize": 0,
                    "id": "neck-1",
                    "lineColor": "#61AB00",
                    "lineThickness": 2,
                    "title": "neck",
                    "valueField": "neck"
                  },
                  {
                    "bullet": "round",
                    "columnWidth": 0,
                    "fixedColumnWidth": 0,
                    "fontSize": 0,
                    "id": "arms-1",
                    "lineColor": "#FF0000",
                    "lineThickness": 2,
                    "title": "arms",
                    "valueField": "arms"
                  },
                  {
                    "bullet": "round",
                    "columnWidth": 0,
                    "fixedColumnWidth": 0,
                    "fontSize": 0,
                    "id": "waist-1",
                    "lineColor": "#ff8000",
                    "lineThickness": 2,
                    "title": "waist",
                    "valueField": "waist"
                  },
                  {
                    "bullet": "round",
                    "columnWidth": 0,
                    "fixedColumnWidth": 0,
                    "fontSize": 0,
                    "id": "hips-1",
                    "lineColor": "#ffff00",
                    "lineThickness": 2,
                    "title": "hips",
                    "valueField": "hips"
                  },
                  {
                    "bullet": "round",
                    "columnWidth": 0,
                    "fixedColumnWidth": 0,
                    "fontSize": 0,
                    "id": "legs-1",
                    "lineColor": "#9400d3",
                    "lineThickness": 2,
                    "title": "legs",
                    "valueField": "legs"
                  }
                ],

                "valueAxes": [
                  {
                    "id": "ValueAxis-1",
                    "title": "Inches"
                  }
                ],
                 "legend": {
              "enabled": true,
              "useGraphSettings": true
            },
                "allLabels": [],
                "balloon": {},
                "dataProvider":data_provider
              }
          );
    }

    vm.displaySleepChart = function(data) {

          var data_date_format = data.chart_setting.data_date_format;
          var data_provider = data.chart_data;

            AmCharts.makeChart("sleep-chart",
          {
            "type": "serial",
            "categoryField": "date",
            "dataDateFormat": data_date_format,
            "theme": "default",
            "categoryAxis": {

              "parseDates": true
            },
            "trendLines": [],
            "graphs": [
              {
                "columnWidth": 0.05,
                "cornerRadiusTop": 1,
                "dashLength": 3,
                "fillAlphas": 1,
                "id": "sleep-id",
                "lineAlpha": 0,
                "lineColor": "#0000FF",
                "lineThickness": 3,
                "title": "sleep",
                "type": "column",
                "valueField": "sleep"
              }
            ],
            "guides": [],
            "valueAxes": [
              {
                "id": "ValueAxis-1",
                "title": "Hours"
              }
            ],
            "allLabels": [],
            "balloon": {},
            "legend": {
              "enabled": true
            },
            "dataProvider": data_provider
          }
        );

    }

}]);

function guageMeter(elementId,value){

  var gauge = new Gauge($('#'+elementId), {
            values: {
                0 : '0',
                20: '2',
                40: '4',
                60: '6',
                80: '8',
                100: '10'
            },
            colors: {
                /*0 : '#666',
                9 : '#378618',
                60: '#ffa500',
                80: '#f00'*/
                0 : '#003d99',
                40: '#33cc33',
                70: '#ff1a1a',
                100:'#f00'
            },
            angles: [
                180,
                360
            ],
            lineWidth: 10,
            arrowWidth: 5,
            arrowColor: '#ccc',
            inset:true,
            value:value
        });
}