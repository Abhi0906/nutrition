
angular.module('app.routes').config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/complete_plans');

    $stateProvider

        .state('complete_plans', {
            url: '/complete_plans',
            templateUrl: '/admin/complete_plans_display',
            controller : 'MealPlansController',
            controllerAs : 'meal_plan'
        })
         .state('view_plan', {
            url: '/view_plan/:id',
            templateUrl: '/admin/view_complete_plan',
            controller : 'ViewPlanController',
            controllerAs : 'view_plan'
        });


});

angular.module('app.core').controller('MealPlansController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){
    var vm = this;

    vm.meal_plans = [];

    // Filters
    $scope.maleTemplateFilter = function(item){
        return item.gender === 'Male';
    }

    $scope.femaleTemplateFilter = function(item){
        return item.gender === 'Female';
    }
    // end: Filters

    vm.getMealPlans = function(){
             ShowService.query('api/v2/admin/meal_plan/get_meal_plans?plan_type=complete_plan').then(function(result){
             vm.meal_plans = result.response;
             console.log(vm.meal_plans);
        });
    }

    vm.addMealPlanModal = function(){
        vm.plan_name ="";
        vm.gender ='Male';
        vm.action ="Add";
        $("#addMealPlanModal").modal("show");
    }

    vm.updateMealPlanModal = function(template){
        vm.plan_name =template.name;
        vm.gender =template.gender;
        vm.selectMealPlanId = template.id;
        vm.selectedPlan = template;
        vm.action ="Update";
        $("#addMealPlanModal").modal("show");
    }

    vm.createMealPlan = function(){

      var data = {
                "name": vm.plan_name,
                "gender": vm.gender,
                 "plan_type":"complete_plan",
                "user_agent": "server"
            };
         // console.log(data);return false;
         if(vm.action =="Add"){

            StoreService.save('api/v2/admin/meal_plan/create_meal_plan', data).then(function(result) {
                if(result.error == false) {
                  vm.meal_plans.push(result.response.meal_plan);
                  $("#addMealPlanModal").modal("hide");
              }
            });
         }else if(vm.action =="Update"){

            StoreService.update('api/v2/admin/meal_plan/update_plan',vm.selectMealPlanId,data).then(function(result) {
                if(result.error == false){
                   var index = vm.meal_plans.indexOf(vm.selectedPlan);
                  vm.meal_plans[index].name =vm.plan_name;
                  vm.meal_plans[index].gender =vm.gender;
                  $("#addMealPlanModal").modal("hide");
                }
              },function(error) {
                console.log(error);
              });

         }


    }

    vm.deleteMealPlan = function(plan_obj){
        var id = plan_obj.id;
        var msg = "Are you sure want to delete this complete plan ?";

        showFlashMessageConfirmation("Delete",msg,"info",function(result) {
              if (result) {
                  ShowService.remove('api/v2/admin/meal_plan/delete', id).then(function(results){

                        var index = vm.meal_plans.indexOf(plan_obj);

                        vm.meal_plans.splice(index, 1);

                       showFlashMessage('Delete','Complete Plan deleted successfully','success');
                    });
                }
        });
    }



}]);
