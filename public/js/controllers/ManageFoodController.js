angular.module('app.routes').config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/manage_food');

    $stateProvider

        .state('manage_food', {
            url: '/manage_food',
            templateUrl: '/admin/manage_food_content',
            controller : 'ManageFoodController',
            controllerAs : 'manage_food'
        })
        .state('add-food', {
            url: '/add-food',
            templateUrl: '/admin/add_manage_food',
            //params : { recommended_type_id : null,recommended_type_name:null},
            controller : 'AddEditFoodController',
            controllerAs : 'add_edit_food'
        })
        .state('edit-food', {
            url: '/edit-food/:id',
            templateUrl: '/admin/add_manage_food',
            //params: { edit_from: null, recommended_type_id : null, food_data: null, food_for_names: null, recommended_types: null, recommended_food_id: null },
            controller: 'AddEditFoodController',
            controllerAs: 'add_edit_food'
        });

});

angular.module('app.core').controller('ManageFoodController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){
    var vm = this;
    // $('.back-top').trigger('click');
    // vm.add_food_url = "add-food";
    // vm.recommended_type_name = null;

    // vm.recommended_type_id = $stateParams.recommended_type_id;
    // vm.recommended_type_name =  $stateParams.recommended_type_name;

    vm.search_loader = false;

    // vm.displayShortRecords = [
    // { name: 'Name', value: 'food_name' },
    // { name: 'Gender', value: 'food_for' },
    // { name: 'Calories', value: 'calories' }];

    // vm.itemDefault = "food_name";

    vm.pageno = 1; // initialize page no to 1
    vm.current_page = 1;
    vm.total_count = 0;
    vm.displayRecords = [50,100,500,1000];
    //vm.itemsPerPage = 1; //this could be a dynamic value from a drop down
    vm.itemPerPage = 100;

    vm.searchFood = function(pageno) {
        vm.search_loader = true;
        vm.foodSearchdata = {};
        vm.search_result = false;
        var searchString = '';
        searchString = vm.searchKeyFood;

        ShowService.search('search.php?keyword='+searchString+'&page='+pageno+'&itemPerPage='+vm.itemPerPage).then(function(result) {
            if(result.error == false) {
                vm.foodSearchdata = result.response;
                vm.total_count = result.param.total; // total data count.
                vm.current_page = result.param.current_page;
                vm.from = result.param.from;
                vm.to = result.param.to;

                if(result.response.length>0){
                  vm.search_loader = false;
                  vm.search_result = true;
                }
              vm.search_loader = false;
            }
            vm.search_loader = false;
        },function(error) {
            console.log(error);
        });
    }

    vm.pageChanged = function(){
        var pageno =  vm.current_page;
        vm.searchFood(pageno);
    }

    //clear logFood search
    vm.clear_logFood_searchResult = function(){
       vm.searchKeyFood = '';
       vm.foodSearchdata = {};
    }
    //end: clear logFood search

    // vm.getFoodRecommendedTypes = function() {
    //     ShowService.query('api/v3/recommended_food/types').then(function(result){
    //         vm.recommended_types = result.response;
    //     });
    // }


    vm.viewFoodDetail = function(food_obj){
        vm.food_detail = food_obj;
        $('#view_food_detail_tmplt').modal('show');
    }

    vm.deleteFoodDetail = function(food_obj){
        var id = food_obj.id;
        var msg = "Are you sure want to delete this food ?";

        showFlashMessageConfirmation("Delete",msg,"info",function(result) {
            if (result) {
                ShowService.remove('api/v2/admin/manage_food/delete', id).then(function(results) {
                    if(results.error == false) {
                        console.log('hiii');
                        var index = vm.foodSearchdata.indexOf(food_obj);
                        vm.foodSearchdata.splice(index, 1);
                        showFlashMessage('Delete','Food deleted successfully','success');
                    }
                });
            }
        });
    }

    // vm.mdRecommendedFoodsDisplayData = function(){
    //     ShowService.query('api/v3/recommended_food/display').then(function(result){
    //         console.log(result);

    //         if(result.error==false){
    //             vm.recommended_foods = result.response;
    //         }
    //     });
    // }

    // vm.deleteRecommendedFood = function(type_obj, food_obj){

    //   var id = food_obj.recommended_food_id;
    //   var msg = "Are you sure want to delete this food ?";

    //   showFlashMessageConfirmation("Delete",msg,"info",function(result) {
    //           if (result) {
    //               ShowService.remove('api/v3/recommended_food/delete', id).then(function(results){

    //                     var main_index = vm.recommended_foods.indexOf(type_obj);

    //                     var obj = vm.recommended_foods[main_index].recommended_foods;
    //                     var sub_index = obj.indexOf(food_obj);

    //                     vm.recommended_foods[main_index].recommended_foods.splice(sub_index, 1);

    //                    showFlashMessage('Delete','Food deleted successfully','success');
    //                 });
    //             }
    //   });

    // }

    // vm.calculateServingSize = function($index, nutrition_obj){

    //     var serving_size_obj = vm.recommended_food[$index].serving_string;
    //     var no_of_serving =  vm.recommended_food[$index].no_of_servings;
    //     var serving_label = serving_size_obj.label;
    //     var serving_unit = serving_size_obj.unit;
    //     var serving_quantity = nutrition_obj.nutrition_data.serving_quantity;

    //     var serving_value = serving_label.replace(serving_unit,'');
    //     var serving_size = recursiveSearch(serving_value);

    //     if(nutrition_obj.hasOwnProperty("nutrition_data")){
    //         var nutrition_data = nutrition_obj.nutrition_data;
    //     }else{
    //         var nutrition_data = nutrition_obj.nutritions.nutrition_data;
    //     }

    //     var update_calories = calculateCalories(no_of_serving,nutrition_data,serving_size,serving_quantity);

    //     vm.recommended_food[$index].calories =update_calories.toFixed(2);

    // }

    // function recursiveSearch(serving_value){

    //    //  var serving_quantity=0;
    //      if (serving_value.toLowerCase().indexOf("container") >= 0) {

    //         var arr = serving_value.split("(");
    //         if (arr.length > 1) {
    //             var innerString = arr[1].trim();
    //             if (innerString.indexOf("/") >= 0) {
    //                 return recursiveSearch(innerString);
    //             }else{
    //                 var space_arr = innerString.split(" ");
    //                return recursiveSearch(space_arr[0]);
    //             }
    //         }
    //     }else if (serving_value.indexOf("/") >= 0) {

    //         var arr = serving_value.split("/")
    //         if (arr.length > 1) {
    //             var innerString = arr[0].trim();
    //             var arrInnerCompo = innerString.split(" ");
    //             if (arrInnerCompo.length == 2) {
    //                 var firstString = arrInnerCompo[0].trim();
    //                 var secondString = arrInnerCompo[1].trim();
    //                 var arr1FirstComponent= arr[1].trim();
    //                 var thirdString = Number(arr1FirstComponent.split(" ")[0]);
    //                 // console.log("firstString",firstString);
    //                 // console.log("secondString",secondString);
    //                 // console.log("thirdString",thirdString);
    //                 var dividevalue = Number(firstString) + Number(secondString) / Number(thirdString) ;
    //                return recursiveSearch(dividevalue.toString());

    //             }else{

    //                  var arrInner = innerString.split(" ");
    //                  var firstString = Number(arrInner[arrInner.length-1]);
    //                  var secondStringArr= arr[1].trim();
    //                  var secondString = Number(secondStringArr.split(" ")[0]);
    //                  var thirdString = firstString/secondString;
    //                  return recursiveSearch(thirdString.toString());

    //             }

    //         }

    //     }else{

    //         var final_value = serving_value.trim();
    //         var splitString = final_value.split(" ")[0]
    //         return Number(splitString);
    //     }

    // }

    // function calculateCalories(no_of_servings,nutrition_obj,serving_size,serving_quantity){

    //     var default_calories = nutrition_obj.calories;
    //     var serving_size_value = Number(serving_size/serving_quantity);
    //     var update_calories = Number(default_calories)*Number(no_of_servings)*Number(serving_size_value);
    //     return update_calories;

    // }

}]);
