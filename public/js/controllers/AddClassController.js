angular.module('app.core').controller('AddClassController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){
    var vm = this;
    vm.class_data = {};

    $('#to-top').trigger('click');
    var select_date = (angular.isUndefined($stateParams.calendar_date)?new Date():$stateParams.calendar_date);
    console.log(select_date);
    vm.format = 'MM-dd-yyyy';

    vm.class_data.class_start_date = new Date(select_date);
	vm.class_data.class_end_date = new Date(select_date);

	vm.dateOptions = {
	  showWeeks:false
	};


	vm.class_start_date_open = function() {
        vm.class_start_date_popup.opened = true;
    };
    vm.class_start_date_popup = {
        opened: false
    };

    vm.class_end_date_open = function() {
        vm.class_end_date_popup.opened = true;
    };
    vm.class_end_date_popup = {
        opened: false
    };

    vm.class_locations = [
						    { title: 'Daily' }, 
						    { title: 'Weekly' }, 
						    { title: 'Monthly' }
						  ];

	vm.class_days = [
		              {
		                  day_index: "1",
		                  day_title: "Monday",
		                  start_time: "09:00:00",
		                  end_time: "10:00:00"
		              },
		              {
		                  day_index: "2",
		                  day_title: "Tuesday",
		                  start_time: "09:00:00",
		                  end_time: "10:00:00"
		              },
		              {
		                  day_index: "3",
		                  day_title: "Wednesday",
                          start_time: "09:00:00",
                          end_time: "10:00:00"
		              },
		              {
		                  day_index: "4",
		                  day_title: "Thursday",
	                      start_time: "09:00:00",
	                      end_time: "10:00:00"
		              },
		              {
		                  day_index: "5",
		                  day_title: "Friday",
		                  start_time: "09:00:00",
		                  end_time: "10:00:00"
		              },
		              {
		                  day_index: "6",
		                  day_title: "Saturday",
		                  start_time: "",
		                  end_time: ""
		              },
		              {
		                  day_index: "7",
		                  day_title: "Sunday",
		                  start_time: "",
		                  end_time: ""
		              }
		            ];
					  
	

	//vm.class_data.start_time = "9:00 am";

	vm.saveClass = function(){


		console.log("saveClass");
		
		var class_data = {};

		class_data.location_id = vm.class_data.class_location;
        class_data.class_name = vm.class_data.class_name;
        class_data.instructor = vm.class_data.instructor;
        class_data.frequency_type = vm.class_data.frequency;
        class_data.frequency_interval = vm.class_data.frequency_interval;

        class_data.class_start_date = moment(vm.class_data.class_start_date).format('YYYY-MM-DD');
        if(vm.class_data.end_after_occurences){
        	class_data.end_after_occurences = vm.class_data.end_after_occurences;
        }
        else{
        	class_data.end_after_occurences = null;
        }
        class_data.class_end_date = moment(vm.class_data.class_end_date).format('YYYY-MM-DD');


        var days = [];
        var k=1;
        angular.forEach( vm.class_data.class_days, function(class_day, key){
        	var temp = {};
        	temp.day_index = k;
        	temp.class_start_time = null;
        	temp.class_end_time = null;

        	if(class_day.start_time){
        		temp.class_start_time = class_day.start_time;
        	}

        	if(class_day.end_time){
        		temp.class_end_time = class_day.end_time;
        	}        	
        	
        	this.push(temp);
        	k++;
        }, days);

        class_data.class_days = days;
       // console.log(class_data);
       // return false;
		StoreService.save('api/v3/class_schedules/create_class', class_data).then(function(result){
            
            if(result.error==false){
                
            }
            else{

            }
        });

	};


}])
.directive('timePicker', function() {
	return {
	restrict: 'E',
	require: 'ngModel',
	replace: true,
	link: function(scope, element, attrs) {
  
	  scope.timings = [				
				{ value: '', text: 'OFF', },				
				{ value: "00:00:00", text: "12:00 am"}, 
				{ value: "00:15:00", text: "12:15 am"}, 
				{ value: "00:30:00", text: "12:30 am"}, 
				{ value: "00:45:00", text: "12:45 am"}, 
				{ value: "01:00:00", text: "1:00 am"}, 
				{ value: "01:15:00", text: "1:15 am"}, 
				{ value: "01:30:00", text: "1:30 am"}, 
				{ value: "01:45:00", text: "1:45 am"}, 
				{ value: "02:00:00", text: "2:00 am"}, 
				{ value: "02:15:00", text: "2:15 am"}, 
				{ value: "02:30:00", text: "2:30 am"}, 
				{ value: "02:45:00", text: "2:45 am"}, 
				{ value: "03:00:00", text: "3:00 am"}, 
				{ value: "03:15:00", text: "3:15 am"}, 
				{ value: "03:30:00", text: "3:30 am"}, 
				{ value: "03:45:00", text: "3:45 am"}, 
				{ value: "04:00:00", text: "4:00 am"}, 
				{ value: "04:15:00", text: "4:15 am"}, 
				{ value: "04:30:00", text: "4:30 am"}, 
				{ value: "04:45:00", text: "4:45 am"}, 
				{ value: "05:00:00", text: "5:00 am"}, 
				{ value: "05:15:00", text: "5:15 am"}, 
				{ value: "05:30:00", text: "5:30 am"}, 
				{ value: "05:45:00", text: "5:45 am"}, 
				{ value: "06:00:00", text: "6:00 am"}, 
				{ value: "06:15:00", text: "6:15 am"}, 
				{ value: "06:30:00", text: "6:30 am"}, 
				{ value: "06:45:00", text: "6:45 am"}, 
				{ value: "07:00:00", text: "7:00 am"}, 
				{ value: "07:15:00", text: "7:15 am"}, 
				{ value: "07:30:00", text: "7:30 am"}, 
				{ value: "07:45:00", text: "7:45 am"}, 
				{ value: "08:00:00", text: "8:00 am"}, 
				{ value: "08:15:00", text: "8:15 am"}, 
				{ value: "08:30:00", text: "8:30 am"}, 
				{ value: "08:45:00", text: "8:45 am"}, 
				{ value: "09:00:00", text: "9:00 am"}, 
				{ value: "09:15:00", text: "9:15 am"}, 
				{ value: "09:30:00", text: "9:30 am"}, 
				{ value: "09:45:00", text: "9:45 am"}, 
				{ value: "10:00:00", text: "10:00 am"}, 
				{ value: "10:15:00", text: "10:15 am"}, 
				{ value: "10:30:00", text: "10:30 am"}, 
				{ value: "10:45:00", text: "10:45 am"}, 
				{ value: "11:00:00", text: "11:00 am"}, 
				{ value: "11:15:00", text: "11:15 am"}, 
				{ value: "11:30:00", text: "11:30 am"}, 
				{ value: "11:45:00", text: "11:45 am"}, 
				{ value: "12:00:00", text: "12:00 pm"}, 
				{ value: "12:15:00", text: "12:15 pm"}, 
				{ value: "12:30:00", text: "12:30 pm"}, 
				{ value: "12:45:00", text: "12:45 pm"}, 
				{ value: "13:00:00", text: "1:00 pm"}, 
				{ value: "13:15:00", text: "1:15 pm"}, 
				{ value: "13:30:00", text: "1:30 pm"}, 
				{ value: "13:45:00", text: "1:45 pm"}, 
				{ value: "14:00:00", text: "2:00 pm"}, 
				{ value: "14:15:00", text: "2:15 pm"}, 
				{ value: "14:30:00", text: "2:30 pm"}, 
				{ value: "14:45:00", text: "2:45 pm"}, 
				{ value: "15:00:00", text: "3:00 pm"}, 
				{ value: "15:15:00", text: "3:15 pm"}, 
				{ value: "15:30:00", text: "3:30 pm"}, 
				{ value: "15:45:00", text: "3:45 pm"}, 
				{ value: "16:00:00", text: "4:00 pm"}, 
				{ value: "16:15:00", text: "4:15 pm"}, 
				{ value: "16:30:00", text: "4:30 pm"}, 
				{ value: "16:45:00", text: "4:45 pm"}, 
				{ value: "17:00:00", text: "5:00 pm"}, 
				{ value: "17:15:00", text: "5:15 pm"}, 
				{ value: "17:30:00", text: "5:30 pm"}, 
				{ value: "17:45:00", text: "5:45 pm"}, 
				{ value: "18:00:00", text: "6:00 pm"}, 
				{ value: "18:15:00", text: "6:15 pm"}, 
				{ value: "18:30:00", text: "6:30 pm"}, 
				{ value: "18:45:00", text: "6:45 pm"}, 
				{ value: "19:00:00", text: "7:00 pm"}, 
				{ value: "19:15:00", text: "7:15 pm"}, 
				{ value: "19:30:00", text: "7:30 pm"}, 
				{ value: "19:45:00", text: "7:45 pm"}, 
				{ value: "20:00:00", text: "8:00 pm"}, 
				{ value: "20:15:00", text: "8:15 pm"}, 
				{ value: "20:30:00", text: "8:30 pm"}, 
				{ value: "20:45:00", text: "8:45 pm"}, 
				{ value: "21:00:00", text: "9:00 pm"}, 
				{ value: "21:15:00", text: "9:15 pm"}, 
				{ value: "21:30:00", text: "9:30 pm"}, 
				{ value: "21:45:00", text: "9:45 pm"}, 
				{ value: "22:00:00", text: "10:00 pm"}, 
				{ value: "22:15:00", text: "10:15 pm"}, 
				{ value: "22:30:00", text: "10:30 pm"}, 
				{ value: "22:45:00", text: "10:45 pm"}, 
				{ value: "23:00:00", text: "11:00 pm"}, 
				{ value: "23:15:00", text: "11:15 pm"}, 
				{ value: "23:30:00", text: "11:30 pm"}, 
				{ value: "23:45:00", text: "11:45 pm"}, 
			];
	},
	template: '<select class="form-control" >\
	<option value="{{time.value}}" ng-repeat="time in timings">{{time.text}}</option>\
	</select>'
	};
});