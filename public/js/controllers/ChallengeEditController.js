angular.module('app.core').controller('ChallengeEditController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll','utility', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll,utility){
    var allowed_dev = $scope.vm.allowed;
    var vm = this;
    vm.today_datetime = new Date();
    vm.goal_select = function(){
        if(vm.goal_type == 'calories'){
            vm.calories = true;
            vm.start_unit = false;
            vm.end_unit = false;
            vm.first ='Target calories';
            vm.second = 'Daily max credit calories';
            vm.weight_loss=false;
        }
          else if(vm.goal_type == 'steps_run'){
            vm.calories = true;
            vm.start_unit = false;
            vm.end_unit = false;
            vm.first ='Target steps';
            vm.second = 'Daily max steps';
             vm.weight_loss=false;
          }
        else if(vm.goal_type == "weight_loss"){
            vm.calories = true;
            vm.start_unit = true;
            vm.end_unit = true;
            vm.percentage = 'lbs';
            vm.first ='Weight';
            vm.second = 'Weeks';
        }
        else if(vm.goal_type == "body_fat_and_weight"){
             vm.calories = true;
             vm.first ='Body fat';
             vm.second = 'Weeks';
             vm.start_unit = true;
             vm.end_unit = true;
             vm.percentage = '%';
             vm.weight_loss=true;
        }
        else if(vm.goal_type == "body_transformation"){
             vm.calories = false;
             vm.start_unit = false;
             vm.end_unit = false;
        }
        else if(vm.goal_type == "muscle_gain"){
             vm.body_fat = false;
             vm.weight_loss = false;
             vm.steps_run = false;
             vm.calories = false;
             vm.start_unit = false;
             vm.end_unit = false;
        }
        else{
            vm.steps_run = false;
            vm.calories = false;
            vm.weight_loss = false;
            vm.body_fat = false;
            vm.start_unit = false;
            vm.end_unit = false;
        }
    }

    vm.getChallengeData = function(){
        vm.challenge_id = $stateParams.id;
        ShowService.get('api/v2/admin/challenge/item_data', vm.challenge_id).then(function(result){
            if(result.error==false){
                vm.challenge_data = result.response;
                vm.challenge_data.start_date = new Date(vm.challenge_data.start_date);
                vm.challenge_data.end_date = new Date(vm.challenge_data.end_date);
                vm.goal_type = result.response.goal_type;
                vm.allowed_devices =result.response.allowed_devices.split(",");
                if(vm.goal_type == 'calories'){
                    vm.calories = true;
                    vm.first ='Target calories burned';
                    vm.second = 'Daily max credit calories';
                    vm.challenge_data.target_calories = result.response.goal_type_config.target_calories;
                    vm.challenge_data.daily_max_credit_calories = result.response.goal_type_config.daily_max_credit_calories;
                }
                else if(vm.goal_type == 'steps_run'){
                    vm.calories = true;
                    vm.first ='Target steps';
                    vm.second = 'Daily max steps';
                    vm.challenge_data.target_calories = result.response.goal_type_config.target_steps;
                    vm.challenge_data.daily_max_credit_calories = result.response.goal_type_config.daily_max_steps;
                }
                else if(vm.goal_type == 'weight_loss'){
                    vm.calories = true;
                    vm.first ='Weight';
                    vm.second = 'Weeks';
                    vm.start_unit = true;
                    vm.end_unit = true;
                    vm.percentage = 'lbs';
                    vm.challenge_data.target_calories = result.response.goal_type_config.weight_loss_lbs;

                    vm.challenge_data.daily_max_credit_calories = result.response.goal_type_config.weight_loss_weeks;
                }
                else if(vm.goal_type == 'body_fat_and_weight'){
                    vm.calories = true;
                    vm.first ='Body fat';
                    vm.second = 'Weeks';
                    vm.start_unit = true;
                    vm.end_unit = true;
                    vm.weight_loss=true;
                    vm.percentage = '%';
                    vm.challenge_data.target_calories = result.response.goal_type_config.body_fat_percentage;
                    vm.challenge_data.weight_in_lbs = result.response.goal_type_config.weight_in_lbs;
                    vm.challenge_data.daily_max_credit_calories = result.response.goal_type_config.weeks;
                }
                else if(vm.goal_type == 'body_transformation'){
                    vm.calories = false;
                }
                else if(vm.goal_type == 'muscle_gain'){
                    vm.calories = false;
                }
            }
        });
    }
    // date time picker funcions
    vm.isOpen_startDate = false;
    vm.openCalendar_startDate = function(e) {
        e.preventDefault();
        e.stopPropagation();
        vm.isOpen_startDate = true;
    };
    vm.isOpen_endDate = false;
    vm.openCalendar_endDate = function(e) {
        e.preventDefault();
        e.stopPropagation();
        vm.isOpen_endDate = true;
    };
    // End: date time picker funcions
    vm.updateChallenge = function(){
            var challenge = {};
            var id = vm.challenge_data.id;
            vm.challengeId = id;
            challenge.title = vm.challenge_data.title;
            challenge.description = vm.challenge_data.description;
            challenge.start_date = vm.challenge_data.start_date;
            challenge.end_date = vm.challenge_data.end_date;
            challenge.allowed_device = vm.allowed_devices;
            challenge.is_expired = vm.challenge_data.is_expired;
            challenge.image = vm.challenge_data.image;
            challenge.weight_in_lbs = vm.challenge_data.weight_in_lbs;

            if(vm.allowed_devices.indexOf("all") != -1 ){
             var arr = [];
                angular.forEach(allowed_dev, function(value, key) {
                var allowed_device_all =  value.id;
                arr.push(allowed_device_all);
                    });
                challenge.allowed_device = arr;
              }else{
                challenge.allowed_device = vm.allowed_devices;
              }

            challenge.goal_type = vm.goal_type;
            challenge.target_calories = vm.challenge_data.target_calories;
            challenge.daily_max_credit_calories = vm.challenge_data.daily_max_credit_calories;
            challenge.is_expired = vm.challenge_data.is_expired;
            var challenge_data = challenge;
            StoreService.update('api/v2/admin/challenge/update',vm.challengeId,challenge_data).then(function(result) {
              if(result.error == false){
              //  showFlashMessage('Update','Challege Update successfully','success');
                $state.go('list-challenge');
              }
            },function(error) {
              console.log(error);
            });
    }
        vm.deleteChallenge = function(challenge){
          var id = challenge;
          var msg = "Are you sure want to delete this Challenge ?";
          showFlashMessageConfirmation("Delete",msg,"info",function(result) {
                  if (result) {
                     ShowService.remove('api/v2/admin/challenge/delete', id).then(function(results){
                             showFlashMessage('Delete','Challenge deleted successfully','success');
                             $state.go('list-challenge');
                        });
                    }
          });
        }

        vm.errMessage_startdate = '';
        vm.errMessage_enddate = '';
        vm.dateCompareValidation = function(startDate, endDate) {

            vm.errMessage_startdate = '';
            vm.errMessage_enddate = '';

            var result = utility.dateCompareValidation(startDate, endDate);

            if(result=='end_date'){
              vm.errMessage_enddate = 'End Date & Time should be greater than start date.';
              return false;
            }

            if(result=='start_date'){
               vm.errMessage_startdate = 'Start Date & Time should not be before today.';
               return false;
            }
        }
}]);