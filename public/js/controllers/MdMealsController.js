
angular.module('app.routes').config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/meals');

    $stateProvider

        .state('meals', {
            url: '/meals',
            templateUrl: '/admin/md_meals_display',
            controller : 'MdMealsController',
            controllerAs : 'meal'
        })
         .state('create-meal-items', {
            url: '/create-meal-items?new_meal_id',
            templateUrl: '/admin/new_meal_items_data',
            controller : 'RecommendedNewMealItemController',
            controllerAs : 'new_meal_item'
        })
        .state('meal-items', {
            url: '/meal-items',
            templateUrl: '/admin/meal_items_data',
            params : { recommended_meal_id : null, recommended_meal_name: null},
            controller : 'RecommendedMealItemController',
            controllerAs : 'meal_item'
        })

        .state('new-meal-items', {
            url: '/new-meal-items',
            templateUrl: '/admin/new_meal_items_data',
            params : { new_meal_id : null, meal_template_id: null, meal_gender: null},
            controller : 'RecommendedNewMealItemController',
            controllerAs : 'new_meal_item'
        })


        .state('add-meal-food', {
            url: '/add-meal-food',
            templateUrl: '/admin/md_recommended_food_type',
            params : { recommended_meal_id: null, recommended_meal_name: null, item_id: null, item_name:null },
            controller : 'AddRecomMealFoodController',
            controllerAs : 'add_meal_food'
        })
        .state('edit-food', {
            url: '/edit-food',
            templateUrl: '/admin/edit_recommended_food',
            params: {   edit_from: null,
                        recommended_type_id : null,
                        food_data: null,
                        food_for_names: null,
                        recommended_types: null,
                        recommended_meal_id: null,
                        recommended_meal_name: null
                    },
            controller: 'EditRecommendedFoodController',
            controllerAs: 'edit_food'
        });
});

angular.module('app.core').controller('MdMealsController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){
    var vm = this;

    vm.recommended_meals = [];
    vm.meal_item_data = [];
    vm.new_meal_id=null;

    vm.createRecommendedMeal = function(){

        var data = {
            "meal_name": vm.meal_name,
            "meal_template_id": vm.template_id,
            "meal_time": vm.meal_time,
            "user_agent": "server"
        };

        StoreService.save('api/v2/admin/recommended_meal/create_meal', data).then(function(result){
            //$state.go('foods');
            if(result.error==false){
                var last_recommended_meal = result.response.last_recommended_meal;
                last_recommended_meal.calorie_range = 0;
                vm.recommended_meals.push(last_recommended_meal);
                $("#addMealModal").modal("hide");
            }
            else{
                if(result.response.meal_exist==true){
                    vm.meal_exist = true;
                }
            }

        });
    }

    vm.getMdMeals = function(){

        ShowService.query('api/v2/admin/md_meals/meals').then(function(result){

            vm.recommended_meals = result.response.md_meals;
            vm.new_meal_id = result.response.new_meal_id;


        });
    }

    vm.deleteRecommendedMeal = function(meal_obj){

        var id = meal_obj.id;
        var msg = "Are you sure want to delete this meal ?";

        showFlashMessageConfirmation("Delete", msg, "info", function(result) {
              if (result) {

                ShowService.remove('api/v2/admin/recommended_meal/delete', id).then(function(result){
                    if(result.error==false){

                        var index = vm.recommended_meals.indexOf(meal_obj);
                        vm.recommended_meals.splice(index, 1);

                        showFlashMessage('Delete','Meal deleted successfully','success');
                    }

                });

              }
        });

    }

    vm.addMealPlanGenderModal = function(){
        vm.gender ='Male';
        vm.action ="Add";
        $("#addMealGenderModal").modal("show");
    }

    // Filters
    $scope.maleTemplateFilter = function(item){
        return item.gender === 'Male';
    }

    $scope.femaleTemplateFilter = function(item){
        return item.gender === 'Female';
    }
    // end: Filters

}]);
