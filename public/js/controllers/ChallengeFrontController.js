angular.module('app.core').controller('ChallengeFrontController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){
    var vm = this;
    $('.back-top').trigger('click');

    vm.getChallenges = function(){
                 ShowService.query('api/v3/challenge/challenges_front').then(function(results){
                    vm.challenges = results.response;
                });
    }

 }]);