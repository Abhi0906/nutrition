angular.module('app.core').controller('WeightController',
	['ShowService','$scope','StoreService','$timeout', '$location', '$anchorScroll', 
		function(ShowService,$scope,StoreService,$timeout, $location, $anchorScroll){
			var vm = this;
			vm.log_weight = {};
			vm.userId = null;
			//vm.log_weight.log_date = moment().format('MMMM DD, YYYY');
			vm.body_fat_error = '';
			vm.log_weight_error = false;
			vm.log_loader = false;
			vm.user_weights = {};
			var today = moment().format('YYYY-MM-DD');
			var d = new Date();
			var before_six_day = d.setDate(d.getDate()-6);
		
			vm.body_measurements = {};
			vm.body_measurements.log_date = new Date();
			vm.user_body_measurements = {};
			vm.last_log_date = '';
			vm.user_height = '';
			vm.weightGraph = 'weight';
			vm.selected_measurement = 'all';



			vm.format = 'MM-dd-yyyy';
			vm.log_weight.log_date = new Date();

			vm.dateOptions = {
			  showWeeks:false
			};


			vm.weight_log_date_open = function() {
		        vm.weight_log_date_popup.opened = true;
		    };
		    vm.weight_log_date_popup = {
		        opened: false
		    };


		    vm.weight_start_date_open = function() {
		        vm.weight_start_date_popup.opened = true;
		    };
		    vm.weight_start_date_popup = {
		        opened: false
		    };
		    

		    vm.weight_end_date_open = function() {
		        vm.weight_end_date_popup.opened = true;
		    };
		    vm.weight_end_date_popup = {
		        opened: false
		    };



		    vm.body_measurements_date_open = function() {
		        vm.body_measurements_date_popup.opened = true;
		    };
		    vm.body_measurements_date_popup = {
		        opened: false
		    };


		    vm.body_chart_start_date_open = function() {
		        vm.body_chart_start_date_popup.opened = true;
		    };
		    vm.body_chart_start_date_popup = {
		        opened: false
		    };


			vm.body_chart_end_open = function() {
		        vm.body_chart_end_popup.opened = true;
		    };
		    vm.body_chart_end_popup = {
		        opened: false
		    };
	
	    	vm.loggedWeight = function(change){
				
				if(change){ // if Change button clicked
					start_date = vm.weight_start_date;
					end_date = vm.weight_end_date;
				}
				else{

					var d = new Date();
					var before_six= d.setDate(d.getDate()-6);
					start_date = before_six;

					end_date  = new Date();
					
					vm.weight_start_date = start_date;
					vm.weight_end_date = end_date;
					
				}
				

				start_date = moment(start_date).format('YYYY-MM-DD');
				end_date = moment(end_date).format('YYYY-MM-DD');
				

				var params = {"from_server":true, "start_date":start_date, "end_date":end_date};
				
				ShowService.search('api/v3/weight/logged_weight/'+vm.userId, params).then(function(results){
					var result_data = results.response.user_weight;
					var result_weight_goal = results.response.user_weight_goal;
					var result_weight_data = results.response.user_weight_last_log;
					if(change){
						vm.user_weights = result_data;
						
					}
					else{
						vm.user_weights_history = result_weight_data;
						vm.user_weights = result_data;
					}
					vm.weight_goal = '';
					vm.body_fat_goal = '';
					vm.weight_goal_date = '';
					vm.weight_goal_days_left = 0;
					
					if(result_weight_goal){
						vm.weight_goal = result_weight_goal.weight_g;
						vm.body_fat_goal = result_weight_goal.body_fat_g;
						if(result_weight_goal.goal_date){
							vm.weight_goal_date = moment(result_weight_goal.goal_date).format('MM-DD-YY');
					
							vm.weight_goal_days_left = vm.getDateDiffDays(today, result_weight_goal.goal_date);
						}
						
					}

					vm.user_height = results.response.user_height;
					
					if(result_data.length > 0){
						vm.last_log_date = moment( result_data[result_data.length-1].log_date ).format('MMM-DD-YYYY'); // get last element date
					}
					
					$timeout(function() {
		   					vm.weightCharts(vm.weightGraph);		   					
	   				}, 200);

				});
			}

			vm.logWeight = function(){
				vm.log_loader = true;

				var weight_data = {};

				weight_data.current_weight = vm.log_weight.current_weight;
				if(vm.validBodyFat(vm.log_weight.body_fat) == true){
					weight_data.body_fat = vm.log_weight.body_fat;
				}
				weight_data.user_id = vm.userId;
				weight_data.log_date = moment(vm.log_weight.log_date).format('YYYY-MM-DD');
				weight_data.guid_server = true;
				weight_data.source = "MANUAL";
				StoreService.save('api/v3/weight/log_weight', weight_data).then(function(result) {
					vm.user_weights_history.push(weight_data);
					if(moment(weight_data.log_date).format('YYYY-MM-DD') > moment(before_six_day).format("YYYY-MM-DD")) {
						vm.user_weights.push(weight_data);
						vm.weightCharts(vm.weightGraph);
					} 
					

					vm.last_log_date = moment(weight_data.log_date).format('MMM-DD-YYYY');
					vm.log_weight = {};
					vm.log_weight.log_date = new Date();
					vm.log_loader = false;

					if(result.error == false) {
						showFlashMessage('Logged','Weight logged successfully','success');
						vm.loggedWeight();
					}
					
				});
			
			}

			vm.validBodyFat = function(body_fat){
				if( (body_fat>=0 && body_fat<=100) || body_fat==null){
					vm.body_fat_error = "";
					vm.log_weight_error = false;
					return true;					
				}
				else{					
					vm.body_fat_error = "<small>Please enter your Body Fat</small>";
					vm.log_weight_error = true;
					return false;
				}
			}

			// Check date exist in logged weight
			vm.checkDate = function(obj, list) {

			    var i;
			    for (i = 0; i < list.length; i++) {
			    	
			        if (angular.equals(list[i]['log_date'], obj) && list[i]['log_date']==today) {
			        	
			            return true;
			        }
			    }

			    return false;
			};
			// end Check date exist in logged weight


            vm.calculateWeightPercent = function(reading, goal){
					 actual_data = Math.abs(reading-goal);
					 ans = (actual_data*100)/goal;
					 final_data = 100-ans;
					 return final_data;
			}


			vm.weightCharts = function(graph){
				vm.weightGraph = graph;
				var data = vm.user_weights;
				
				if(data.length > 0){
					data.sort(function(a,b){
					  return new Date(a.log_date) - new Date(b.log_date);
					});
				
				
					var dataProvider_weight = [];
					angular.forEach(data, function(value, key) {
						
						var date = moment(value.log_date).format('ddd/DD');
						
						var body_fat = vm.calBodyFat(value.current_weight, value.body_fat);

						var lean = vm.calLean(value.current_weight, body_fat);
						var bmi = vm.calBmi(value.current_weight);

						dataProvider_weight.push({"weight":value.current_weight, "body_fat":body_fat, "lean":lean, "bmi":bmi, "date":date});
					});

					/* graph weight */ 
					
					var graphs_weight = [];
					var weight_chart_name = '';
					var valueAxis_title = '';
					if(graph=="weight"){

						weight_chart_name = 'weight_chart';
						valueAxis_title = 'Pounds';
						graphs_weight = [
					          {
					            "balloonText": "[[title]] of [[category]]:[[value]]",
					            "bullet": "round",
					            "id": "AmGraph-1",
					            "title": "Weight",
					            "type": "smoothedLine",
					            "valueField": "weight"
					          }
					        ];

					        ValueAxis = [
					          {
					            "id": "ValueAxis-1",
					            "title": "Pounds"
					          }
					        ];

					    var weight_goals = vm.weight_goal;				
						var guide_data = [{
				              "boldLabel": true,
				              "color": "#FE6E0E",
				              "dashLength": 6,
				              "fillColor": "#FE6E0E",
				              "fontSize": 12,
				              "id": "Consume-Guide-1",
				              "inside": true,
				              "label": "Weight Goal: "+ weight_goals +"",
				              "lineAlpha": 1,
				              "lineColor": "#FE6E0E",
				              "lineThickness": 2,
				              "tickLength": 5,
				              "value": weight_goals,
				              "valueAxis": "ValueAxis-1"
				            }];

					        // weight change
					        var old_weight = data[0].current_weight;
							var new_weight = data[data.length-1].current_weight;
							var weight_goal_latest = vm.weight_goal;
							
							vm.final_goal_weight = vm.calculateWeightPercent(new_weight, weight_goal_latest);
							if(vm.final_goal_weight > 95 ){
								vm.diff_class = "green";
							}
							else{
								vm.diff_class = "red";
							}

							var weight_diff = new_weight - old_weight;
							vm.weightDiffChange(weight_diff, 'lbs');


					}
					else if(graph=="lean_fat"){
						weight_chart_name = 'lean_fat_chart';
						valueAxis_title = 'Lean Vs Fat';
						graphs_weight = [
					          {
					            "balloonText": "[[title]] of [[category]]:[[value]]",
					            "bullet": "round",
					            "id": "AmGraph-1",
					            "title": "Lean",
					            "type": "smoothedLine",
					            "valueField": "lean"
					          },
					          {
					            "balloonText": "[[title]] of [[category]]:[[value]]",
					            "bullet": "round",
					            "id": "AmGraph-2",
					            "title": "Fat",
					            "type": "smoothedLine",
					            "valueField": "body_fat"
					          }
					        ];

					    var body_dat_goal = vm.body_fat_goal;
				           var guide_data = [{
				              "boldLabel": true,
				              "color": "#FCD202",
				              "dashLength": 6,
				              "fillColor": "#FCD202",
				              "fontSize": 12,
				              "id": "Consume-Guide-1",
				              "inside": true,
				              "label": "Fat Goal: "+ body_dat_goal +"",
				              "lineAlpha": 1,
				              "lineColor": "#FCD202",
				              "lineThickness": 2,
				              "tickLength": 5,
				              "value": body_dat_goal,
				              "valueAxis": "ValueAxis-1"
				            }];

					        // fat change
					        var old_fat = vm.calBodyFat(data[0].current_weight, data[0].body_fat);
							var new_fat = vm.calBodyFat(data[data.length-1].current_weight, data[data.length-1].body_fat);

							var green_red_fat = data[data.length-1].body_fat;
							var fat_goal = vm.body_fat_goal;
							vm.final_goal_fat = vm.calculateWeightPercent(green_red_fat, fat_goal);

							if(vm.final_goal_fat > 95 ){
								vm.diff_class = "green";
							}
							else{
								vm.diff_class = "red";
							}
							
							var fat_diff = new_fat - old_fat;
							if(fat_diff>=0){
								vm.fat_diff_arrow = "up";
							}
							else{
								vm.fat_diff_arrow = "down"; 
							}
							vm.fat_chart_diff = fat_diff + " fat";
							
							// lean change
							/*var old_lean = vm.calLean(data[0].current_weight, old_fat);
							var new_lean = vm.calLean(data[data.length-1].current_weight, new_fat);

							var lean_diff = new_lean - old_lean;
							vm.weightDiffChange(lean_diff, 'lean');*/

					}
					else if(graph=="bmi"){
						weight_chart_name = 'bmi_chart';
						valueAxis_title = 'BMI';
						graphs_weight = [
					          {
					            "balloonText": "[[title]] of [[category]]:[[value]]",
					            "bullet": "round",
					            "id": "AmGraph-1",
					            "title": "BMI",
					            "type": "smoothedLine",
					            "valueField": "bmi"
					          }
					        ];

					        //bmi
					        var old_bmi = vm.calBmi(data[0].current_weight);
							var new_bmi = vm.calBmi(data[data.length-1].current_weight);
							
							var weight_diff = new_bmi - old_bmi;
							vm.weightDiffChange(weight_diff, 'bmi');
					}
					/* end graph weight */
				
					AmCharts.makeChart(weight_chart_name,
						{
							"type": "serial",
					        "categoryField": "date",
					        "startDuration": 0,
					        "categoryAxis": {
					          "gridPosition": "start"
					        },
					        "trendLines": [],
					        "graphs": graphs_weight,
					        "guides": guide_data,
					        "valueAxes": [
					          {
					            "id": "ValueAxis-1",
					            "title": valueAxis_title
					          }
					        ],
					        "allLabels": [],
					        "balloon": {},
					        "legend": {
					          "enabled": true,
					          "useGraphSettings": true
					        },
					        "titles": [
					          {
					            "id": "Title-1",
					            "size": 15,
					            "text": ""
					          }
					        ],
							"dataProvider": dataProvider_weight

						}
					);
				}
			}

			vm.loggedBodyMeasurements = function(change){

				if(change){
					start_date = vm.body_chart_start_date;
					end_date = vm.body_chart_end_date;
				}
				else{
					var d = new Date();
					var before_six_d= d.setDate(d.getDate()-6);
					start_date = before_six_d;

					end_date  = new Date();
					vm.body_chart_start_date = start_date;
					vm.body_chart_end_date = end_date;
					
				}

				start_date = moment(start_date).format('YYYY-MM-DD');
		
				end_date = moment(end_date).format('YYYY-MM-DD');
				
				vm.body_measurement_goal_data = {};
				var params = {"from_server":true, "start_date":start_date, "end_date":end_date};
				
				ShowService.search('api/v3/weight/logged_body_measurements/'+vm.userId, params).then(function(results){

					var result_data = results.response.user_body_measurements;
					var result_data_last_log = results.response.user_body_measurements_last_log;

					// body measurement goal data
					vm.body_measurement_goal_data = results.response.body_measurement_goal;

					vm.body_measurement_goal_date = '';
				
					vm.body_measurement_goal_days_left = 0;

					if(vm.body_measurement_goal_data && vm.body_measurement_goal_data.goal_date){
						vm.body_measurement_goal_date = moment(vm.body_measurement_goal_data.goal_date).format('MM-DD-YY');	
						vm.body_measurement_goal_days_left = vm.getDateDiffDays(today, vm.body_measurement_goal_data.goal_date);
					}

					if(change){
						vm.user_body_measurements = result_data;
						
					}
					else{
						vm.user_measurements_history = result_data_last_log;
						vm.user_body_measurements = result_data;
					}

					// end body measurement goal data 

					$timeout(function() {
		   					vm.bodyMeasurementsChart(vm.selected_measurement);		
		   							
	   					}, 200);
				});
			}


			vm.logBodyMeasurements = function(){
				
				vm.log_loader = true;

				var body_measurements_data = {};

				body_measurements_data.user_id = vm.userId;
				body_measurements_data.log_date = moment(vm.body_measurements.log_date).format('YYYY-MM-DD');
				body_measurements_data.neck = vm.body_measurements.neck;
				body_measurements_data.arms = vm.body_measurements.arms;
				body_measurements_data.waist = vm.body_measurements.waist;
				body_measurements_data.hips = vm.body_measurements.hips;
				body_measurements_data.legs = vm.body_measurements.legs;
				body_measurements_data.guid_server = true;
				body_measurements_data.source = "MANUAL";
				

				StoreService.save('api/v3/weight/log_body_measurements', body_measurements_data).then(function(result) {
					if(moment(body_measurements_data.log_date).format('YYYY-MM-DD') > moment(before_six_day).format("YYYY-MM-DD")) {
						vm.user_body_measurements.push(body_measurements_data);
						vm.bodyMeasurementsChart(vm.selected_measurement);
					}
					vm.body_measurements = {};
					vm.body_measurements.log_date = new Date();
					vm.log_loader = false;

					if(result.error == false) {
						showFlashMessage('Logged','Body Measurements logged successfully','success');
						vm.loggedBodyMeasurements();
					}
					
				});
			
			}

			vm.bodyMeasurementsChart = function(measurement){
				if(measurement != 'all'){
					$scope.ChangeButton='Change';
				}else{
					$scope.ChangeButton='';
				}
				

				



				vm.selected_measurement = measurement;
				var data = vm.user_body_measurements;
				if(data.length > 0){
					data.sort(function(a,b){
					  return new Date(a.log_date) - new Date(b.log_date);
					});
				
					

					/* graph data */ 
					var dataProvider_body = [];

					angular.forEach(data, function(value, key) {

						var date = moment(value.log_date).format('ddd/DD');
						dataProvider_body.push(
							{
								"neck":value.neck,
								"arms":value.arms, 
								"waist":value.waist, 
								"hips":value.hips, 
								"legs":value.legs, 
								"date":date,
							});
					});
					/* end graph data */ 

					/* graph body */ 
					var graphs_body = [];
					var body_chart_name = '';
					if(measurement=="all"){
						body_chart_name = 'measurements_all';
						graphs_body = [
				            {
				              "balloonText": "[[title]] of [[category]]:[[value]]",
				              "bullet": "round",
				              "id": "AmGraph-1",
				              "title": "Neck",
				              "type": "smoothedLine",
				              "valueField": "neck"
				            },
				            {
				              "balloonText": "[[title]] of [[category]]:[[value]]",
				              "bullet": "square",
				              "id": "AmGraph-2",
				              "title": "Arms",
				              "type": "smoothedLine",
				              "valueField": "arms"
				            },
				            {
				              "balloonText": "[[title]] of [[category]]:[[value]]",
				              "bullet": "square",
				              "id": "AmGraph-3",
				              "title": "Waist",
				              "type": "smoothedLine",
				              "valueField": "waist"
				            },
				            {
				              "balloonText": "[[title]] of [[category]]:[[value]]",
				              "bullet": "square",
				              "id": "AmGraph-4",
				              "title": "Hips",
				              "type": "smoothedLine",
				              "valueField": "hips"
				            },
				            {
				              "balloonText": "[[title]] of [[category]]:[[value]]",
				              "bullet": "square",
				              "id": "AmGraph-5",
				              "title": "Legs",
				              "type": "smoothedLine",
				              "valueField": "legs"
				            }
				        ];
					}
					else if(measurement=="neck"){
						body_chart_name = 'measurements_neck';
						var neck_goals = vm.body_measurement_goal_data.neck_g;
						var guide_data = [{
				              "boldLabel": true,
				              "color": "#FE6E0E",
				              "dashLength": 6,
				              "fillColor": "#FE6E0E",
				              "fontSize": 12,
				              "id": "Consume-Guide-1",
				              "inside": true,
				              "label": "Neck Goal: "+ neck_goals +"",
				              "lineAlpha": 1,
				              "lineColor": "#FE6E0E",
				              "lineThickness": 2,
				              "tickLength": 5,
				              "value": neck_goals,
				              "valueAxis": "ValueAxis-1"
				            }];
						graphs_body = [
							{
				              "balloonText": "[[title]] of [[category]]:[[value]]",
				              "bullet": "round",
				              "id": "AmGraph-1",
				              "title": "Neck",
				              "type": "smoothedLine",
				              "valueField": "neck"
				            }
						];
						

					


						// neck changes
						var old_neck = data[0].neck;
						var new_neck = data[data.length-1].neck;
						var neck_goal = vm.body_measurement_goal_data.neck_g;
						vm.final_neck_goal = vm.calculateWeightPercent(new_neck, neck_goal);

						if(vm.final_neck_goal > 95 ){
							vm.diff_class = "green";
						}
						else{
							vm.diff_class = "red";
						}
						var neck_diff = new_neck - old_neck;
						vm.bodyDiffChange(neck_diff);
					}
					else if(measurement=="arms"){
						body_chart_name = 'measurements_arms';
						var arms_goals = vm.body_measurement_goal_data.arms_g;
						var guide_data = [{
				              "boldLabel": true,
				              "color": "#FCD202",
				              "dashLength": 6,
				              "fillColor": "#FCD202",
				              "fontSize": 12,
				              "id": "Consume-Guide-1",
				              "inside": true,
				              "label": "Arms Goal: "+ arms_goals +"",
				              "lineAlpha": 1,
				              "lineColor": "#FCD202",
				              "lineThickness": 2,
				              "tickLength": 5,
				              "value": arms_goals,
				              "valueAxis": "ValueAxis-1"
				            }];


						graphs_body = [
							{
				              "balloonText": "[[title]] of [[category]]:[[value]]",
				              "bullet": "square",
				              "id": "AmGraph-2",
				              "title": "Arms",
				              "type": "smoothedLine",
				              "valueField": "arms",
				              "lineColor": "#FCD202"
				            }
						];

						// neck changes

						var old_arms = data[0].arms;
						
						var new_arms = data[data.length-1].arms;
						
						var arms_goal = vm.body_measurement_goal_data.arms_g;
						vm.final_arms_goal = vm.calculateWeightPercent(new_arms, arms_goal);

						if(vm.final_arms_goal > 95 ){
							vm.diff_class = "green";
						}
						else{
							vm.diff_class = "red";
						}


						var arms_diff = new_arms - old_arms;

						vm.bodyDiffChange(arms_diff);
						
					}
					else if(measurement=="waist"){
						body_chart_name = 'measurements_waist';
						graphs_body = [
							{
				              "balloonText": "[[title]] of [[category]]:[[value]]",
				              "bullet": "square",
				              "id": "AmGraph-3",
				              "title": "Waist",
				              "type": "smoothedLine",
				              "valueField": "waist",
				              "lineColor": "#B0DE09"
				            }
						];
						var waist_goals = vm.body_measurement_goal_data.waist_g;
						var guide_data = [{
				              "boldLabel": true,
				              "color": "#B0DE09",
				              "dashLength": 6,
				              "fillColor": "#B0DE09",
				              "fontSize": 12,
				              "id": "Consume-Guide-1",
				              "inside": true,
				              "label": "Waist Goal: "+ waist_goals +"",
				              "lineAlpha": 1,
				              "lineColor": "#B0DE09",
				              "lineThickness": 2,
				              "tickLength": 5,
				              "value": waist_goals,
				              "valueAxis": "ValueAxis-1"
				            }];


						// waist changes

						var old_waist = data[0].waist;
						
						var new_waist = data[data.length-1].waist;
						
						var waist_goal = vm.body_measurement_goal_data.waist_g;
						vm.final_waist_goal = vm.calculateWeightPercent(new_waist, waist_goal);

						if(vm.final_waist_goal > 95 ){
							vm.diff_class = "green";
						}
						else{
							vm.diff_class = "red";
						}


						var waist_diff = new_waist - old_waist;

						vm.bodyDiffChange(waist_diff);
					}
					else if(measurement=="hips"){
						body_chart_name = 'measurements_hips';
						graphs_body = [
							{
				              "balloonText": "[[title]] of [[category]]:[[value]]",
				              "bullet": "square",
				              "id": "AmGraph-4",
				              "title": "Hips",
				              "type": "smoothedLine",
				              "valueField": "hips",
				              "lineColor": "#0D8ECF"
				            }
						];

						var hips_goals = vm.body_measurement_goal_data.hips_g;
						var guide_data = [{
				              "boldLabel": true,
				              "color": "#0D8ECF",
				              "dashLength": 6,
				              "fillColor": "#0D8ECF",
				              "fontSize": 12,
				              "id": "Consume-Guide-1",
				              "inside": true,
				              "label": "Hips Goal: "+ hips_goals +"",
				              "lineAlpha": 1,
				              "lineColor": "#0D8ECF",
				              "lineThickness": 2,
				              "tickLength": 5,
				              "value": hips_goals,
				              "valueAxis": "ValueAxis-1"
				            }];

						// hips changes

						var old_hips = data[0].hips;
						
						var new_hips = data[data.length-1].hips;
						
						var hips_goal = vm.body_measurement_goal_data.hips_g;
						vm.final_hips_goal = vm.calculateWeightPercent(new_hips, hips_goal);

						if(vm.final_hips_goal > 95 ){
							vm.diff_class = "green";
						}
						else{
							vm.diff_class = "red";
						}



						var hips_diff = new_hips - old_hips;

						vm.bodyDiffChange(hips_diff);
					}
					else if(measurement=="legs"){
						body_chart_name = 'measurements_legs';
						graphs_body = [
							{
				              "balloonText": "[[title]] of [[category]]:[[value]]",
				              "bullet": "square",
				              "id": "AmGraph-5",
				              "title": "Legs",
				              "type": "smoothedLine",
				              "valueField": "legs",
				              "lineColor": "#2A0CD0"
				            }
						];
						var legs_goals = vm.body_measurement_goal_data.legs_g;
						var guide_data = [{
				              "boldLabel": true,
				              "color": "#2A0CD0",
				              "dashLength": 6,
				              "fillColor": "#2A0CD0",
				              "fontSize": 12,
				              "id": "Consume-Guide-1",
				              "inside": true,
				              "label": "Legs Goal: "+ legs_goals +"",
				              "lineAlpha": 1,
				              "lineColor": "#2A0CD0",
				              "lineThickness": 2,
				              "tickLength": 5,
				              "value": legs_goals,
				              "valueAxis": "ValueAxis-1"
				            }];

						// legs changes

						var old_legs = data[0].legs;
						
						var new_legs = data[data.length-1].legs;
						
						var legs_goal = vm.body_measurement_goal_data.legs_g;
						vm.final_legs_goal = vm.calculateWeightPercent(new_legs, legs_goal);

						if(vm.final_legs_goal > 95 ){
							vm.diff_class = "green";
						}
						else{
							vm.diff_class = "red";
						}

						
						var legs_diff = new_legs - old_legs;

						vm.bodyDiffChange(legs_diff);
					}
					/* end graph body */ 
				
					AmCharts.makeChart(body_chart_name, {
			          "type": "serial",
			          "categoryField": "date",
			          "startDuration": 0,
			          "categoryAxis": {
			            "gridPosition": "start"
			          },
			          "trendLines": [],
			          "graphs": graphs_body,
			          "guides": guide_data,
			          "valueAxes": [
			            {
			              "id": "ValueAxis-1",
			              "title": "Inches"
			            }
			          ],
			          "allLabels": [],
			          "balloon": {},
			          "legend": {
			            "enabled": true,
			            "useGraphSettings": true
			          },
			          "titles": [
			            {
			              "id": "Title-1",
			              "size": 15,
			              "text": ""
			            }
			          ],
			          "dataProvider": dataProvider_body
			        });
				}
			}

			vm.getDateDiffDays = function(date1, date2){
				
				var date1 = moment(date1);
					
				var date2 = moment(date2);

				return date2.diff(date1, 'days');

			}

			vm.weightDiffChange = function(diff, unit){

				if(diff>=0){
					vm.weight_diff_arrow = "up";
				}
				else{
					vm.weight_diff_arrow = "down"; 
				}

				diff = diff.toFixed(1);
				vm.weight_chart_diff = diff + " " + unit;
				
			}

			vm.calBmi = function(weight){
				if(vm.user_height){
					return Math.round( (703*weight) / (vm.user_height * vm.user_height) );
				}
				else{
					return 0;
				}
				
			}

			vm.calBodyFat = function(weight, fat_percent){
				return Math.round( ( weight * fat_percent ) / 100 );
			}
			
			vm.calLean = function(weight, fat){
				return Math.round( weight - fat );
			}

			vm.bodyDiffChange = function(diff){
				if(diff>=0){
					vm.measurementChanges_arrow = "up";
				}
				else{
					vm.measurementChanges_arrow = "down"; 
				}
				diff = diff.toFixed(1);
				vm.measurementChanges = diff + " Inches";
			}
		}
	]
);