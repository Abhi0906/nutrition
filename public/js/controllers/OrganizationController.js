angular.module('app.core').controller('OrganizationController',['ShowService','$scope','StoreService','$timeout', function(ShowService,$scope,StoreService,$timeout){
    var vm = this;
	vm.organizations = {};
	vm.displayRecords = [10,20,50,100];
	vm.itemPerPage = 10;
  vm.keyOrg=null;

    ShowService.query('api/v2/admin/organization/list').then(function(result){
            vm.organizations = result.response;
    });

    vm.getDate = function(date){
      if(date==null){
        return '---';
      }
      return moment(date).format('MMMM DD, YYYY');
    }

    vm.deleteOrganization = function(organization){

      var id = organization.id;
      var msg = "Are you sure want to delete this organization ?";
      showFlashMessageConfirmation("Delete",msg,"info",function(result) {
          if (result) {
              ShowService.remove('api/v2/admin/organization/delete', id).then(function(results){

                    var index = vm.organizations.data.indexOf(organization);

                    vm.organizations.data.splice(index, 1);

                   showFlashMessage('Delete','Organization deleted successfully','success');
                });
            }
      });

    }

    vm.searchOrg = function(){
      var params ={'keyword':vm.keyOrg,
                   'itemPerPage':vm.itemPerPage
                   };

      ShowService.search('api/v2/admin/organization/list',params,1).then(function(result){
            vm.organizations = result.response;
      });

    }


}]);