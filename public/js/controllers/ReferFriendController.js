angular.module('app.routes').config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/list-referfriend');

    $stateProvider

        .state('list-referfriend', {
            url: '/list-referfriend',
            templateUrl: '/admin/load_referfriend',
            controller : 'ReferFriendController',
            controllerAs : 'list_referfriend'
        });
});
angular.module('app.core').controller('ReferFriendController',['ShowService','$scope',function(ShowService,$scope){
    var vm = this;
    vm.current_page = 1;
    vm.total_count = 0;
    vm.itemPerPage = 50;


    vm.referFriendList = function(pageno){
                ShowService.search('api/v2/admin/referfriend/getRefered').then(function(results){
                    vm.get_referfriend = results.response;
                    vm.total_count = results.response.total; // total data count.
                    vm.current_page = results.response.current_page;
                    vm.from = results.response.from;
                    vm.to = results.response.to;
                });
    }
    vm.pageChanged = function(){
        var pageno =  vm.current_page;
        vm.referFriendList(pageno);
       }

}]);