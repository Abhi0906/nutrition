angular.module('app.core').controller('ScheduleController',['ShowService','$scope','StoreService','$rootScope', function(ShowService,$scope,StoreService,$rootScope){
    var vm = this;
    vm.userId = $scope.userDetails.userId;
    vm.user_program_start ='';
    vm.user_schedules = [];
    vm.templates = [];
    vm.selectedSchedule = null;
    
    //get user medicines list
    ShowService.get('api/v3/patient/user_schedule',vm.userId).then(function(results){
        vm.user_schedules = results.response.user_schedule; 
        vm.user_program_start=results.response.program_start;      
    });



    $scope.$watch('schedule.keyword', function(newVal) {

        vm.searchTemplate(newVal);
    });

    vm.searchTemplate = function(keyword){

       if(typeof keyword != "undefined"){
         var params ={'keyword':keyword };
         ShowService.search('api/v3/template/list',params,1).then(function(results){
           vm.templates = results.response;
         });
       }
      else{     
        ShowService.query('api/v3/template/list').then(function(results){
          vm.templates = results.response;
        });
      }
    }

    vm.editSchedule = function(){
     
        vm.selectedSchedule = user_schedule;
        $('#user-schedule').modal('show');
    }

    vm.getWorkoutTitle = function(workout){

      if(workout == null){
        return true;
      }
      var class_experience ="text-primary";
      if(workout.experience == 'Beginner'){
        class_experience="text-primary";
      }
      else if(workout.experience == 'Intermediate'){
        class_experience="text-warning";
      }
      if(workout.experience == 'Advanced'){
        class_experience="text-success";
      }

      var start_days = (typeof workout.pivot != 'undefined' && workout.pivot.start !=null)?workout.pivot.start:'';
      var end_days = (typeof workout.pivot != 'undefined' && workout.pivot.end !=null)?workout.pivot.end:'';
      var config =   (typeof workout.week_day != 'undefined' && workout.week_day !=null)?workout.week_day:'';
      var week_days_name = vm.getWeekDayName(config);
      var icon = '<strong><i class="flaticon-stick5 '+ class_experience +'"></i></strong>';
      var atag = '<span class="tillana-font '+class_experience+'">'+workout.title +'</span>';
      var video_count = '<small class="tillana-font">Videos:</small><small><strong> '+workout.video_count +'</strong></small>';
      var body_part = "<small class='tillana-font'>Body part:</small> <span><small>&nbsp;"+workout.body_part+"</small></span>,"+" &nbsp;&nbsp;" +video_count;
      var duration = "<span class='tillana-font'><small>Duration:</small>  </span><small>&nbsp;&nbsp;From: </small><small><strong>"+start_days+"</strong></small><small>&nbsp;&nbsp;To: </small><small><strong>"+end_days+"</strong></small>";
      var week_days = "<span class='tillana-font'><small>Week Day(s):</small></span>&nbsp;<small><strong>"+week_days_name+"</strong></small>";
      return icon + "&nbsp;&nbsp;"+ atag + "<br>"+body_part+"<br>"+duration+",&nbsp;&nbsp;"+week_days; 
    }

    vm.getScheduleTitle = function(schedule){
        if(schedule == null){
             return true;
        }
        var title = '<span class= "tillana-font">'+schedule.name+'</span>';
        var form_date = '<small><a href="javascript:void(0);"><strong>From:</strong></a>&nbsp;<strong>'+schedule.from_date+'</strong> &nbsp;&nbsp;';
        var to_date = '<a href="javascript:void(0);"><strong>To:</strong></a>&nbsp;<strong>'+schedule.to_date+'</strong></small>';
        return  title +"<br>"+form_date+to_date;
     }

    vm.getWeekDayName = function(days){
     
      var week_name = ['SUN','MON','THE','WED','THU','FRI','SAT'];
      var week_names ='';
      if(days !=""){
        angular.forEach(days, function(value, key) {
           week_names+= week_name[value]+",&nbsp;";
          });
        week_names = week_names.slice(0,-7);
      }
      return week_names;
    }

    vm.templateInserted = function(event, index,template_id,workouts_added) {
      
      var workouts = workouts;
      var user_id = vm.userId;
      var data = {'template_id':template_id,'workouts':workouts_added,'user_id':user_id};
     
       StoreService.save('api/v3/user_schedule/attached_template',data).then(function(result) {
            
             
        }, function(error) {
          console.log(error);
        });
    }

    vm.deleteUserWorkout = function(workout){

        var workout_id = workout.id;
        var user_id = vm.userId;
        var msg = "Are you sure want to delete this workout ?";
        showFlashMessageConfirmation("Delete",msg,"info",function(result) {
            if (result) {
                  ShowService.remove('api/v3/user_schedule/delete_workout/'+user_id,workout_id).then(function(results){
                     if(results.error == false){
                            var index = vm.user_schedules.indexOf(workout);
                            vm.user_schedules.splice(index, 1);
                            showFlashMessage('Delete','Workout deleted successfully','success');
                     }
                  });
            }
        }); 
    }

     $rootScope.$on('editSchedule', function (e,args) {
       $('#user-schedule').modal('show');
      
    });

     vm.dropCallback = function(event, index, item, external, type, allowedType) {
        var workouts = angular.copy(item.workouts);
        var template_id = item.id;
        var workouts_added = [];
        for(var i =0; i < workouts.length;i++){
          var pivot_start =workouts[i].pivot.start;
          var pivot_end =workouts[i].pivot.end;
          workouts[i].pivot.start =moment(vm.user_program_start).add(pivot_start, 'day').subtract(1,'day').format("MMMM DD, YYYY");
          workouts[i].pivot.end =moment(vm.user_program_start).add(pivot_end, 'day').subtract(1,'day').format("MMMM DD, YYYY");  
          workouts[i].week_day = workouts[i].pivot.config;
          
          if(!vm.workoutExists(workouts[i],vm.user_schedules)){
            vm.user_schedules.push(workouts[i]);
            workouts_added.push(workouts[i]);
          }
        }
        if(workouts_added.length>0){
          vm.templateInserted(event,index,template_id,workouts_added);
        }
        return  false;
    };
    vm.workoutExists = function(workout, workoutArray){
      for(var i = 0; i < workoutArray.length; i++){
        if(workout.id != workoutArray[i].id){
          continue;
        }
        if(workout.pivot.start == workoutArray[i].pivot.start && workout.pivot.end == workoutArray[i].pivot.end && $(workout.pivot.config.sort()).not(workoutArray[i].pivot.config.sort()).length === 0 && $(workoutArray[i].pivot.config.sort()).not(workout.pivot.config.sort()).length === 0){
          return true;
        }

      }
      return false;
    }
      
}]);

