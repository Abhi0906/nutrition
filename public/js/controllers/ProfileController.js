/**
 * Created by plenar on 9/24/2015.
 */
angular.module('app.core').controller('ProfileController', function(ShowService, StoreService, $scope, utility, $rootScope) {
    var vm = this;
    vm.userId = $scope.userDetails.userId;
    vm.profile = [];
    vm.user_weight = [];
    vm.age = null;
    vm.gender = null;
    vm.userType = 'patient';
    var patient_calories = null;
    vm.user_goal = [];
    //vm.log_date = moment().utc().format('LL');
    
    ShowService.get('api/v3/patient/profile', vm.userId).then(function(results) {
        vm.profile = results.response.profile;
        vm.user_weight = results.response.user_weight;
        vm.user_goal = results.response.user_goal;
        vm.age = results.response.age;
        vm.gender = results.response.gender;
        patient_calories = results.response.profile.calories;
        vm.water_intake = results.response.profile.water_intake;
        vm.user_weight.created_at = results.response.user_weight.created_at;
        if(vm.user_weight.created_at == undefined){
            vm.user_weight.created_at = '';
        }else{
            vm.user_weight.created_at = moment(results.response.user_weight.created_at).format('MMMM DD, YYYY')
        }
        vm.calculateCalories();
        vm.profileOriginal = angular.copy(results);
    });

    ShowService.query('api/v3/' + vm.userType + '/weeklygoals').then(function(results) {
        vm.weeklygoals =[];
          if(results.response != null){
               for(i = 0; i < results.response.length; i++){
                 results.response[i].id = parseInt(results.response[i].id);                 
                } 
          }
          vm.weeklygoals = results.response;
    });
  
    // calculate Water_intake function
    vm.calculateWaterIntake = function() {
        var weight = 0;
        var waterIntake = '';
        try {
            weight = parseInt(vm.current_weight);
        } catch (error) {}
        if(weight) {
            waterIntake = weight * 0.5;
            vm.water = waterIntake;
        }
    }
    // save data same days pop up and kepp_current
   /* vm.saveWeight = function(){
            var data = {};
            data.guid_server = true;
            data.source = 'MANUAL';
            data.user_id = vm.userId;
            data.current_weight = vm.current_weight;
            data.body_fat = vm.bodyfat;
            data.log_date = moment(vm.log_date).format('YYYY-MM-DD');
            StoreService.save('api/v3/weight/log_weight', data).then(function(result) {
                if(result.error == false) {
                    $('#same_day').modal('hide');
                    vm.new_water_intake  = parseInt( vm.current_weight) * 0.5;
                    var weight = vm.current_weight;
                    var height_feet = vm.profile.height_in_feet;
                    var height_inches = vm.profile.height_in_inch;
                    var age = vm.age;
                    var gender = vm.gender;
                    var activity =1;
                    new_calories = utility.calculateUsCalories(gender, weight, height_feet, height_inches, age,activity);
                    vm.new_calories = new_calories;
                    if(vm.user_weight.hasOwnProperty('current_weight')){
                            $('#keep_current').modal('show');
                    }
                vm.user_weight.body_fat = vm.bodyfat;
                vm.user_weight.current_weight = vm.current_weight;
                vm.user_weight.created_at = moment(vm.log_date).format('MMMM DD, YYYY');
                }
            },
            function(error) {
                console.log(error);
            });
    }*/
    
   /* // check which pop-up display
    vm.log_weight = function() {
            var data = {};
            created_date =  moment(vm.user_weight.created_at).format('YYYY-MM-DD');
            data.log_date = moment(vm.log_date).format('YYYY-MM-DD');
            if(created_date == "Invalid date"){
                    vm.saveWeight();
            }
            else if(created_date == data.log_date){
                $('#same_day').modal('show');
                return false;
            }
            else{
              $('#keep_current').modal('show');
              return false;
            }
 
        }*/
    // Goal Weight called
    vm.changeGoalWeight = function() {
        var goal_weight = parseFloat(vm.user_goal.goal_weight);
        var current_weight = parseFloat(vm.user_weight.current_weight);
        var diff_weight = goal_weight - current_weight;

        if(diff_weight == 1  || diff_weight == -1){
          vm.user_goal.weekly_goal_id = 5;
          return true;
        }else if (goal_weight > current_weight && diff_weight > 2) {
            vm.user_goal.weekly_goal_id = 6;
            vm.profile.calories = parseInt(patient_calories) + (500 * parseFloat(0.5));
        } else {
            vm.user_goal.weekly_goal_id = 1;
            vm.profile.calories = parseInt(patient_calories) + (500 * parseFloat(-0.5));
        }
    }
    // Weekly Goal called
    vm.changeCalories = function() {
        var weekly_goal_val = vm.getWeeklyVal(vm.user_goal.weekly_goal_id);
        vm.profile.calories = parseInt(patient_calories) + (500 * parseFloat(weekly_goal_val));
        //console.log(vm.currentSelectedGoal);

    }
    vm.getWeeklyVal = function(weekly_goal_id) {
        var weekly_value = 0;
        angular.forEach(vm.weeklygoals, function(weeklygoal, key) {
            if (weeklygoal['id'] == weekly_goal_id) {
                weekly_value = weeklygoal['value'];
            }
        });
        return weekly_value;
    }
    vm.updateCaloriesAndWater = function(){
        vm.profile.water_intake = vm.new_water_intake;
        vm.profile.calories = vm.new_calories;
        vm.saveProfile('updateCalories');
    }

    vm.saveProfile = function() {
        var data = {};
        data.user_id = vm.userId;
        data.baseline_weight = vm.profile.baseline_weight;
        data.height_in_feet = vm.profile.height_in_feet;
        data.height_in_inch = vm.profile.height_in_inch;
        data.program_start = vm.profile.program_start;
        data.program_duration_days = vm.profile.program_duration_days;
        data.activity_id = (typeof vm.profile.activity_id == 'undefined') ? 1 : vm.profile.activity_id;
        data.goal = vm.profile.goal;
        data.water_intake = vm.profile.water_intake;
        data.goal_weight = vm.user_goal.goal_weight;
        data.weekly_goal_id = vm.user_goal.weekly_goal_id;
        data.calories = vm.profile.calories;
        StoreService.save('api/v3/patient/profile', data).then(function(result) {
                if (result.error == false) {
                    showFlashMessage('Save', 'Profile save successfully', 'success');
                }
            },
            function(error) {
                console.log(error);
            });
    }
    vm.reset = function() {
        vm.profile = angular.copy(vm.profileOriginal.response.profile);
        vm.routine_times = angular.copy(vm.profileOriginal.response.routine_times);
    }
    // $scope.$watchGroup([
    //     'profile_goals.profile.height_in_feet',
    //     'profile_goals.profile.height_in_inch',
    // ], function(newVal) {
    //     var age = 0;
    //     var gender = null;
    //     var height_feet = 0;
    //     var height_inches = 0;
    //     var weight = 0;
    //     var activity = 1;
    //    console.log(newVal);
    //     age = vm.age;
    //     gender = vm.gender;
    //     weight = vm.profile.baseline_weight;

    //     height_feet = (typeof newVal[1] != 'undefined' && newVal[1] != '') ? newVal[1] : 0;
    //     height_inches = (typeof newVal[2] != 'undefined' && newVal[2] != '') ? newVal[2] : 0;
    //     console.log('height_feet'+height_feet);
    //         console.log('height_inches'+height_inches);
    //     var patient_calories = utility.calculateUsCalories(gender, weight, height_feet, height_inches, age, activity);
    //     console.log(patient_calories);
    //     if(patient_calories) {
    //       vm.profile.calories = patient_calories;
    //     }
    // });

    vm.getActivityValue = function(id) {
        var activity_value = 1;
        angular.forEach(vm.activities, function(activity, key) {
            if(activity['id'] == id) {
                activity_value = activity['value'];
            }
        });
        return activity_value;
    }

    // $scope.$watch('profile_goals.profile.activity_id',function(newValue){
    //     if(typeof newValue != 'undefined' && newValue != null){
    //         var activity_id = newValue;
    //         var activity_value = null;
    //         angular.forEach(vm.activities, function(activity, key) {
    //           if(activity_id == activity['id']){
    //             activity_value = activity['value'];
    //           }
    //         });
    //         if(activity_value){
    //           vm.profile.calories = parseInt(vm.profile.calories * activity_value);
    //         }
    //     }
    //     else
    //     {
    //       vm.profile.calories = vm.old_calories;
    //     }
    // });

    vm.calculateCalories = function(){

        var age = 0;
        var gender = null;
        var height_feet = 0;
        var height_inches = 0;
        var weight = 0;
        var activity = 1;
      
        age = vm.age;
        gender = vm.gender;
        weight = vm.profile.baseline_weight;
        height_feet = (vm.profile.height_in_feet != null ) ? vm.profile.height_in_feet : 0;
        height_inches = (vm.profile.height_in_inch != null ) ? vm.profile.height_in_inch : 0;
        var patient_calories = utility.calculateUsCalories(gender, weight, height_feet, height_inches, age, activity);
        //console.log(patient_calories);
        if(patient_calories) {
          vm.profile.calories = patient_calories;
        }

    }

});