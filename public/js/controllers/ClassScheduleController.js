angular.module('app.routes').config(function($stateProvider, $urlRouterProvider) {

    //$urlRouterProvider.otherwise('/event-setting');

    $stateProvider

        .state('events', {
            url: '/events',
            templateUrl: '/admin/load_events',
            controller : 'ShowClassController',
            controllerAs : 'show_class'
        })
        .state('event-setting', {
            url: '/event-setting',
            templateUrl: '/admin/load_event_setting',
            controller : 'EventSettingController',
            controllerAs : 'setting'
        });

});

angular.module('app.core').controller('ClassScheduleController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll','uiCalendarConfig', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll,uiCalendarConfig){
    var vm = this;
    vm.isEventSetting = 0;
    $('.back-top').trigger('click');

    vm.changeRoute = function(){

     if(vm.isEventSetting == 1){
        $state.go('events');
     }else{
         $state.go('event-setting');
     }
    }


}]);
