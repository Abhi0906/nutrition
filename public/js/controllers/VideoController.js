/**
 * Created by Plenar on 10/3/2015.
 */
angular.module('app.core').controller('VideoController',['ShowService','$rootScope',function(ShowService,$rootScope){
    var vm = this;
    vm.listView = true;
    vm.selectedVideo = null;
    vm.experiences = ['Beginner','Intermediate','Advanced'];
    vm.training_types = [];
    vm.equipments =[];
    vm.isloading = false;

    ShowService.query('api/v2/video/body_parts').then(function(results){
            vm.body_parts = results.response;
        });

    ShowService.query('api/v2/video/equipments').then(function(results){
            vm.equipments = results.response;
       });

    $rootScope.$on('editVideo', function (e,args) {

        vm.listView = false;
    });

    // $rootScope.$on('videoEdited', function (e,args) {

    //     vm.listView = true;
    // });


}]);

angular.module('app.core').controller('VideoListingController', ['ShowService','$sce','$rootScope', function(ShowService,$sce,$rootScope){
    var vm = this;

    vm.fetch_videos = function() {
      ShowService.query('api/v2/video/fetch_videos').then(function(results){
            vm.isloading = true;
            vm.videos = results.response.data;
            vm.isloading = false;
       });
    }

    vm.editVideo = function(video) {
        //console.log('calling...',video);
        $rootScope.$broadcast('editVideo',angular.copy(video));
        $rootScope.$broadcast('resetScroll');
    }

    vm.deleteVideo = function(video){

        var id = video.id;
            var msg = "Are you sure want to delete this video ?";
            showFlashMessageConfirmation("Delete",msg,"info",function(result) {
                    if (result) {
                        ShowService.remove('api/v2/video',id).then(function(results){
                           if(results.error == false){
                              var index = vm.videos.indexOf(video);
                              vm.videos.splice(index, 1);
                              showFlashMessage('Delete','Video delete successfully','success');
                           }

                        });

                    }
            });

    }



}]);

/**
 * Created by plenar on 9/24/2015.
 */
angular.module('app.core').controller('VideoSettingController', function(ShowService,StoreService,$rootScope,$sce,$scope){
    var vm = this;
    $rootScope.$on('editVideo', function (e,args) {
        vm.video = args;
    });
    vm.getEmbedCode = function(video){
      if(!video) return;
        vm.embedIFrame = $sce.trustAsHtml(video.embed);
        return vm.embedIFrame;
    }
    $scope.$watch('setting.video.experience', function(newVal) {
            if(typeof newVal != 'undefined' && newVal != null){
               vm.saveSetting();
            }
       });

     $scope.$watch('setting.video.equipments', function(newVal) {
            if(typeof newVal != 'undefined' && newVal != null){
               vm.saveSetting();
            }
       });

      $scope.$watch('setting.video.body_parts', function(newVal) {
            if(typeof newVal != 'undefined' && newVal != null){
               vm.saveSetting();
            }
       });


    vm.saveSetting = function(){
    var video_data = vm.video;
     if(!video_data){
      return;
     }
    delete(video_data.created_at);
    delete(video_data.updated_at);
    delete(video_data.deleted_at);
    delete(video_data.created_by);
    delete(video_data.updated_by);
    delete(video_data.pictures);
    StoreService.update('api/v2/video',video_data.id,video_data).then(function(result) {
               if(result.error == false) {
                 // showFlashMessage('Video Setting','Video Setting Updated successfully','success');
               }
               $rootScope.$broadcast('videoEdited',vm.video);

            }, function(error) {
              console.log(error);
            });
   }
});
