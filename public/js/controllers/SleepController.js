angular.module('app.core').controller('SleepController',
	['ShowService','$scope','StoreService','$timeout', '$location', '$anchorScroll','uibDateParser', 
		function(ShowService,$scope,StoreService,$timeout, $location, $anchorScroll,uibDateParser){
		var vm = this;
		vm.log_sleep = {};
		vm.weekly_sleep= {};
		vm.userId = null;
		vm.format = 'MM-dd-yyyy';
		var yesterday = new Date(new Date().getTime() - 24 * 60 * 60 * 1000);
		vm.log_sleep.went_to_date =yesterday;
	    vm.log_sleep.rise_from_date =new Date();
		vm.log_Sleep_error = false;
		vm.times_awakened_num = true;
		vm.dateOptions = {
		  showWeeks:false
		};
		vm.went_tobed_date_open = function() {
		    vm.went_tobed_date_popup.opened = true;
		};

		vm.went_tobed_date_popup = {
		    opened: false
		};
		vm.rise_tobed_date_open = function() {
		    vm.rise_tobed_date_popup.opened = true;
		};
		vm.rise_tobed_date_popup = {
		    opened: false
		};

		// weekly calendar 
		vm.weekly_sleep.from_date =  getLastWeek();
		vm.weekly_sleep.to_date =  new Date();
		vm.weekly_sleep.to_opened= false;
		vm.weekly_sleep.from_opened= false;
		
		vm.weekly_from_open = function(){
			vm.weekly_sleep.from_opened= true;
		}
		vm.weekly_to_open = function(){
		   vm.weekly_sleep.to_opened= true;
		}

		var went_to = new Date(vm.log_sleep.went_to_date);
		    went_to.setHours( 22 );
		    went_to.setMinutes( 0 );
		   
		vm.log_sleep.went_to = went_to;
	 	
	    var rise_from= new Date(vm.log_sleep.rise_from_date);
		    rise_from.setHours( 6 );
		    rise_from.setMinutes( 0 );
		vm.log_sleep.rise_from = rise_from;

		vm.calculateTotalSleep = function () {
			
			var start_date = moment(vm.log_sleep.went_to);
			var end_date = moment(vm.log_sleep.rise_from);
			var duration = moment.duration(end_date.diff(start_date));
			var hours = duration.hours();
			var mins = parseInt(duration.minutes());

			vm.log_sleep.total_bed = duration.asHours();
			vm.log_sleep.total_sleep = duration.asHours();
			vm.log_sleep.total_sleep_hours = hours; 
			vm.log_sleep.total_sleep_mins = mins; 

			var times_awakened_minutes = vm.log_sleep.times_awakened_minutes;
			var minutes_fall_alseep = vm.log_sleep.minutes_fall_alseep;
			if(typeof times_awakened_minutes !== "undefined") {
				var update_start_date =start_date.add(times_awakened_minutes,'minutes');
				var update_duration = moment.duration(end_date.diff(update_start_date));
				vm.log_sleep.total_sleep_hours = update_duration.hours(); 
				vm.log_sleep.total_sleep_mins = update_duration.minutes();
				vm.log_sleep.total_sleep = update_duration.asHours(); 
			}

			if(typeof minutes_fall_alseep !== "undefined") {
				var update_start_date =start_date.add(minutes_fall_alseep,'minutes');
				var update_duration = moment.duration(end_date.diff(update_start_date));
				vm.log_sleep.total_sleep_hours = update_duration.hours(); 
				vm.log_sleep.total_sleep_mins = update_duration.minutes();
				vm.log_sleep.total_sleep = update_duration.asHours(); 
			}
		}

		vm.changeDate = function(time_type){
			if(time_type == 'went_to_bed'){
			   var went_to_time = vm.log_sleep.went_to;
			   var update_went_to = new Date(vm.log_sleep.went_to_date);
			   update_went_to.setHours( went_to_time.getHours());
			   update_went_to.setMinutes( went_to_time.getMinutes());
			   vm.log_sleep.went_to = update_went_to;
		       vm.calculateTotalSleep();
			}else if(time_type == 'rise_from_bed'){

			   var rise_from_time = vm.log_sleep.rise_from;
			   var update_rise_from = new Date(vm.log_sleep.rise_from_date);
			   update_rise_from.setHours( rise_from_time.getHours());
			   update_rise_from.setMinutes( rise_from_time.getMinutes());
			   vm.log_sleep.rise_from = update_rise_from;
		       vm.calculateTotalSleep();

			}
		}

		vm.enableTimeAwakened = function(){
			vm.times_awakened_num = false;
		}
			
	    vm.logSleep = function(){
			 	var sleep_data = {};
			 	sleep_data.user_id = vm.userId;
				sleep_data.guid_server = true;
				sleep_data.source = "MANUAL";
				var times_awakened_minutes = (typeof vm.log_sleep.times_awakened_minutes != 'undefined')?vm.log_sleep.times_awakened_minutes :0;
				var times_awakened = (typeof vm.log_sleep.times_awakened != 'undefined')?vm.log_sleep.times_awakened :0;
				var minutes_fall_alseep = (typeof vm.log_sleep.minutes_fall_alseep != 'undefined')?vm.log_sleep.minutes_fall_alseep :0;
				
				sleep_data.times_awakened_minutes = times_awakened_minutes;
				sleep_data.times_awakened = times_awakened;
				sleep_data.total_bed = vm.log_sleep.total_bed;
				sleep_data.total_sleep = vm.log_sleep.total_sleep;
				sleep_data.went_to_bed =  moment(vm.log_sleep.went_to).format('YYYY-MM-DD HH:mm:ss');
				sleep_data.rise_from_bed = moment(vm.log_sleep.rise_from).format('YYYY-MM-DD HH:mm:ss');
				sleep_data.minutes_fall_alseep = minutes_fall_alseep;
				var went_to_bed_hours = moment(vm.log_sleep.went_to).hours();
				 if(parseInt(went_to_bed_hours) > 12){
				 	sleep_data.log_date = moment(vm.log_sleep.went_to).format('YYYY-MM-DD');
				 }else{
				 	sleep_data.log_date = moment(vm.log_sleep.went_to).subtract(1, 'day').format('YYYY-MM-DD');
				 }
				  //console.log(sleep_data);
				  //return false;
				StoreService.save('api/v3/sleep/log_sleep', sleep_data).then(function(result) {
               		//vm.user_sleep_history.push(sleep_data);

				    if(result.error == false) {
						showFlashMessage('Logged','Sleep logged successfully','success');
						vm.userSleep();
						vm.getWeeklySleep();
					}
				});
        }

        vm.userSleep = function(){

          ShowService.get('api/v3/sleep/user_sleep', vm.userId).then(function(result){

	          vm.sleep_progess = result.response.sleep_progess;
	          vm.sleep_history = result.response.sleep_history;
	          vm.last_sleep = result.response.last_sleep;
	          vm.toal_sleep = result.response.total;
	          var chart_data = result.response.chart_data;
	          vm.displaySleepChart('last-sleep', JSON.parse(chart_data),'last');
	         // console.log("data",chart_data);
	         
        	});

       }
       vm.getWeeklySleep = function(){

       	  var params = vm.userId;
	      var from_date = moment(vm.weekly_sleep.from_date).format('YYYY-MM-DD');
	      var to_date = moment(vm.weekly_sleep.to_date).format('YYYY-MM-DD');
	      params = params+'?from_date='+from_date+'&to_date='+to_date;
	     
	      ShowService.get('api/v3/sleep/weekly_sleep', params).then(function(result){

	        vm.weekly_history = result.response.weekly_history;
	        
	      });

       }

       vm.displaySleepChart = function(id,data_prvoider,type){
       		 var categoryAxis ={"autoRotateCount": 0,
				      "minPeriod": "hh",
				      "parseDates": true,
				      "startOnAxis": true,
				      "ignoreAxisWidth": true,
				      "minHorizontalGap": 30,
				      "minVerticalGap": 30};	
       		 if(type =="weekly"){

       		 	 categoryAxis ={"autoRotateCount": 0,
				      "minPeriod": "hh",
				      "parseDates": true,
				      "startOnAxis": true,
				      "ignoreAxisWidth": true
				     };
       		 }
	       	AmCharts.makeChart(id,
			   {
				   "type": "serial",
				   "categoryField": "date",
				   "columnSpacing": 4,
				   "columnWidth": 0,
				   "dataDateFormat": "YYYY-MM-DD HH",
				   "maxZoomFactor": 19,
				   "zoomOutButtonPadding": 6,
				   "sequencedAnimation": false,
				   "fontSize": 13,
				   "processTimeout": 1,
				   "svgIcons": false,
				   "categoryAxis": categoryAxis,
				   "chartCursor": {
				      "enabled": true,
				      "categoryBalloonDateFormat": "JJ:NN"
				   },
				   "trendLines": [],
				   "graphs": [
				      {
				         "fillAlphas": 1,
				         "fillColors": "#0000FF",
				         "fixedColumnWidth": -1,
				         "fontSize": -4,
				         "id": "asleepId",
				         "lineAlpha": 0,
				         "markerType": "square",
				         "title": "asleep",
				         "type": "step",
				         "valueField": "column-1"
				      },
				      {
				         "balloonColor": "#FF0000",
				         "columnWidth": 0.11,
				         "fillAlphas": 1,
				         "fillColors": "#FF0000",
				         "fixedColumnWidth": 8,
				         "gapPeriod": 2,
				         "id": "activeId",
				         "lineAlpha": 0,
				         "lineThickness": 0,
				         "showBalloon": false,
				         "tabIndex": 3,
				         "title": "active",
				         "type": "column",
				         "valueField": "column-2"
				      }
				   ],
				   "guides": [],
				   "valueAxes": [
				      {
				       
						"axisAlpha": 0,
						"gridAlpha": 0.1,
						"labelsEnabled": false

				      }
				   ],
				   "allLabels": [],
				   "balloon": {},
				   "legend": {
				      "enabled": true
				   },
				   "titles": [
				      {
				         "id": "Title-1",
				         "size": 15,
				         "text": "Sleep"
				      }
				   ],
				   "dataProvider": data_prvoider
			   });


       }

       vm.displayweeklyChart = function(id,weekly_chart_data){
       	var id = 'last_sleep_'+id;
        	vm.displaySleepChart(id,weekly_chart_data.chart_data,'weekly');
       }

       function getLastWeek(){
		   var today = new Date();
		   var lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);
		   return lastWeek ;
	   }
	
}]);