angular.module('app.core').controller('ChallengeAddController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll','utility', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll, utility){
    var allowed_dev_uniq = $scope.vm.allowed;
    var vm = this;
    vm.today_datetime = new Date();
    vm.challenge_data = {};
    vm.goal_select = function(){
        if(vm.goal_type == 'calories'){
            vm.calories = true;
        		vm.start_unit = false;
            vm.end_unit = false;
            vm.first ='Target calories burned';
            vm.second = 'Daily max credit calories';
            vm.weight_loss =false;
        }
    	  else if(vm.goal_type == 'steps_run'){
        		vm.calories = true;
            vm.start_unit = false;
            vm.end_unit = false;
            vm.first ='Target steps';
            vm.second = 'Daily max steps';
            vm.weight_loss =false;
    	  }
        else if(vm.goal_type == "weight_loss"){
            vm.calories = true;
            vm.start_unit = true;
            vm.end_unit = true;
            vm.percentage = 'lbs';
            vm.first ='Weight';
            vm.second = 'Weeks';
        }
        else if(vm.goal_type == "body_fat_and_weight"){
             vm.calories = true;
             vm.first ='Body fat';
             vm.second = 'Weeks';
             vm.start_unit = true;
             vm.end_unit = true;
             vm.percentage = '%';
             vm.weight_loss =true;
        }
        else if(vm.goal_type == "body_transformation"){
             vm.steps_run = false;
             vm.calories = false;
             vm.start_unit = false;
             vm.end_unit = false;
        }
        else if(vm.goal_type == "muscle_gain"){
             vm.steps_run = false;
             vm.calories = false;
             vm.start_unit = false;
             vm.end_unit = false;
        }
        else{
            vm.calories = false;
            vm.start_unit = false;
            vm.end_unit = false;
        }
    }
    // date time picker funcions
    vm.isOpen_startDate = false;
    vm.openCalendar_startDate = function(e) {
        e.preventDefault();
        e.stopPropagation();
        vm.isOpen_startDate = true;
    };
    vm.isOpen_endDate = false;
    vm.openCalendar_endDate = function(e) {
        e.preventDefault();
        e.stopPropagation();
        vm.isOpen_endDate = true;
    };

    vm.saveChallenge = function(){
	  var challenge_data = {};
      challenge_data.title = vm.title;
      challenge_data.description = vm.description;
      challenge_data.start_date = moment(vm.challenge_data.start_datetime).format('YYYY-MM-DD HH:mm:ss');
      challenge_data.end_date = moment(vm.challenge_data.end_datetime).format('YYYY-MM-DD HH:mm:ss');
      challenge_data.image = vm.challenge_data.image;
      challenge_data.allowed_device = vm.allowed_devices;
      if(vm.allowed_devices.indexOf("all") != -1 ){
             var arr = [];
             angular.forEach(allowed_dev_uniq, function(value, key) {
                var allowed_device_all =  value.id;
                arr.push(allowed_device_all);
             });
        challenge_data.allowed_device = arr;
      }else{
        challenge_data.allowed_device = vm.allowed_devices;
      }
      challenge_data.goal_type = vm.goal_type;
	    challenge_data.target_calories = vm.target_calories;
	    challenge_data.daily_max_credit_calories = vm.daily_max_credit_calories;
      challenge_data.weight_in_lbs = vm.weight_in_lbs;
      StoreService.save('api/v2/admin/challenge/create_challenge', challenge_data).then(function(result) {
			if(result.error == false) {
			           // showFlashMessage('Logged','Challege logged successfully','success');
                  $state.go('list-challenge');
          }
			});
     }

      vm.errMessage_startdate = '';
      vm.errMessage_enddate = '';
      vm.dateCompareValidation = function(startDate, endDate) {

          vm.errMessage_startdate = '';
          vm.errMessage_enddate = '';
          var result = utility.dateCompareValidation(startDate, endDate);
          if(result=='end_date'){
            vm.errMessage_enddate = 'End Date & Time should be greater than start date.';
            return false;
          }
          if(result=='start_date'){
             vm.errMessage_startdate = 'Start Date & Time should not be before today.';
             return false;
          }
      }
}]);
