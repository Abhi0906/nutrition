angular.module('app.core').controller('BodyMeasurementsController',
	['ShowService','$scope','StoreService','$timeout', '$location', '$anchorScroll', 
		function(ShowService,$scope,StoreService,$timeout, $location, $anchorScroll){
			var vm = this;
			vm.log_weight = {};
			vm.userId = null;
			vm.log_weight.log_date = moment().format('MMMM DD, YYYY');
			vm.body_fat_error = '';
			vm.log_weight_error = false;
			vm.log_loader = false;
			vm.user_weights = {};
			var today = moment().format('YYYY-MM-DD');

			vm.loggedWeight = function(requested_days, btn_id){

				var params = '';
				if(requested_days)
				{
					params = {"requested_days":requested_days};

					$(".days-filter").removeClass("active");
            		$("#"+btn_id).addClass("active");
					
				}
				ShowService.search('api/v3/weight/logged_weight/'+vm.userId, params).then(function(results){
					vm.user_weights = results.response;

					$timeout(function() {
		   					vm.amCharts();		   					
	   					}, 200);

				});
			}

			// end Check date exist in logged weight

			vm.amCharts = function(){
				var data = vm.user_weights;
				if(data.length > 0){
					data.sort(function(a,b){
					  return new Date(a.log_date) - new Date(b.log_date);
					});
				}
				
				var dataProvider = [];
				angular.forEach(data, function(value, key) {
					var date = moment(value.log_date).format('MMM DD,<br>YYYY');
					dataProvider.push({"weight":value.current_weight, "date":date, "arms":"100","waist":"130","hips":"160","legs":"190" });
				});

				AmCharts.makeChart("chartdiv",
					{
						"type": "serial",
						"categoryField": "date",
						"startDuration": 0,
						"handDrawScatter": 3,
						"categoryAxis": {
							"gridPosition": "start"
						},
						"trendLines": [],
						"graphs": [
							{
								"balloonText": "[[value]] lbs On [[category]]",
								"bullet": "round",
								"id": "AmGraph-1",
								"title": "Neck graph",
								"type": "smoothedLine",
								"valueField": "weight"
							},
							{
								"balloonText": "[[value]] of [[category]]",
								"bullet": "square",
								"id": "AmGraph-2",
								"title": "Arms graph",
								"type": "smoothedLine",
								"valueField": "arms"
							},
							{
								"balloonText": "[[value]] of [[category]]",
								"bullet": "square",
								"id": "AmGraph-3",
								"title": "Waist graph",
								"type": "smoothedLine",
								"valueField": "waist"
							},
							{
								"balloonText": "[[value]] of [[category]]",
								"bullet": "square",
								"id": "AmGraph-4",
								"title": "Hips graph",
								"type": "smoothedLine",
								"valueField": "hips"
							},
							{
								"balloonText": "[[value]] of [[category]]",
								"bullet": "square",
								"id": "AmGraph-5",
								"title": "Legs graph",
								"type": "smoothedLine",
								"valueField": "legs"
							}
						],
						"guides": [],
						"valueAxes": [
							{
								"id": "ValueAxis-1",
								"title": "Body Measurements Graph"

							}
						],
						"allLabels": [],
						"balloon": {},
						"legend": {
							"enabled": true,
							"useGraphSettings": true
						},
						"titles": [
							{
								"id": "Title-1",
								"size": 15,
								"text": ""
							}
						],
						"dataProvider": dataProvider

					}
				);
			}
		}
	]
);