angular.module('app.core').controller('FoodDiaryController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$location', '$anchorScroll','uibDateParser','uiCalendarConfig', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $location, $anchorScroll,uibDateParser,uiCalendarConfig){
    var vm = this;
    vm.userId = $scope.food.userId;
    vm.log_date = $scope.food.log_date;
    vm.food_notes ="edit notes";
    vm.eventSources =[];
    vm.breakfastFood =[];
    vm.snackFood =[];
    vm.lunchFood =[];
    vm.dinnerFood =[];
    vm.totalLunch=[];
    vm.totalBreakfast=[];
    vm.totalDinner=[];
    vm.totalSnacks=[];
    vm.foodSummary=[];
    vm.food_type = $scope.food.food_type;
    vm.water_intake = 0;
    vm.format = 'MM-dd-yyyy';
    vm.dateOptions = {
      showWeeks:false
    };

  

    if(vm.food_type){
        $anchorScroll.yOffset = 150;
        var old = $location.hash();
        $location.hash(vm.food_type+'_focus_id');
        $anchorScroll();
        $location.hash(old);
    }

    vm.food_date_open = function() {
        vm.food_date_popup.opened = true;
    };
    
    vm.food_date_popup = {
        opened: false
    };
    vm.prevFoodDairy = function(){
       var yesterday = new Date(new Date(vm.log_date).getTime() - 24 * 60 * 60 * 1000);
        vm.log_date = yesterday;
        $scope.food.log_date = yesterday;
        vm.getFoodDiary();
    }
    
    vm.nextFoodDairy = function(){
    
        var tomarrow = new Date(new Date(vm.log_date).getTime() + 24 * 60 * 60 * 1000);
        vm.log_date = tomarrow;
        $scope.food.log_date = tomarrow;
        vm.getFoodDiary();

    }
    
    vm.changeDate = function(time_type){
        vm.log_date = moment(vm.log_date).format('MM/DD/YYYY');
        vm.log_date = new Date(vm.log_date);
        $scope.food.log_date = vm.log_date;
        vm.getFoodDiary();
    }


    vm.getFoodDiary = function(){
      vm.food_notes = null;
      var params = vm.userId;
      var log_date = moment(vm.log_date).format('YYYY-MM-DD');
      params = vm.userId+'?log_date='+log_date;
     
      ShowService.get('api/v3/food/patient_food', params).then(function(result){

         vm.breakfastFood = result.response.todayLoggedFood.Breakfast;
         vm.snackFood = result.response.todayLoggedFood.Snacks;
         vm.lunchFood = result.response.todayLoggedFood.Lunch;
         vm.dinnerFood = result.response.todayLoggedFood.Dinner;

         vm.totalLunch = result.response.todayLoggedFood.lunch_total;
         vm.totalDinner = result.response.todayLoggedFood.dinner_total;
         vm.totalSnacks = result.response.todayLoggedFood.snacks_total;
         vm.totalBreakfast = result.response.todayLoggedFood.breakfast_total;
         vm.foodSummary = result.response.food_summary; 

         // get food note
         var food_note_data = result.response.food_note;
         if(food_note_data!=null){
          vm.food_notes = food_note_data.notes;
         }
         // end get food note  

         // get total water intake
         var  total_water_intake = result.response.total_water_intake;
         if(total_water_intake!=null){
          vm.water_intake = total_water_intake;
         }
        
         // end total water intake


         // get favourite , frequent and recent data
            $scope.food.favourite_nutritions = result.response.favourite_nutritions;
            $scope.food.frequent_nutritions = result.response.frequent_nutritions;
            $scope.food.recent_nutritions = result.response.recent_nutritions;
            $scope.food.custom_nutritions = result.response.custom_nutritions;
            $scope.food.meals = result.response.meals;
         // end get fav, freq, rec data

      });

    }

    
    vm.monthlyCalorieSummary = function(calendar_date){
     
      var param = vm.userId;
      var month = moment(calendar_date).format('MM');
      var year = moment(calendar_date).format('YYYY');
      param = vm.userId+'?req_month='+month+'&req_year='+year;
      var eventdata = [];
      ShowService.get('api/v3/food/monthly_calorie_summary', param).then(function(results){
           
           // date wise data
           var values = results.response.date_wise;

           angular.forEach(values, function(value, key) {

                      eventdata.push({
                          "calory":value.total_consumed, 
                          "start":value.date, 
                          "diff_text":value.diff_text, 
                          "diff_text_color":value.diff_text_color,
                          "diff":value.diff,
                          "water_intake":value.water_intake
                        });
          
            });
           // end date wise data

           // weekly wise data
           var weekly_avg_data = results.response.average.week_wise_avg;

            angular.forEach(weekly_avg_data, function(value, key) {
              var html_data_weekly = "";
               $('#week_average .week_'+ key).html(html_data_weekly);
                if(value.length == undefined){
                
                    html_data_weekly = "<p>Calorie Average</p><p>"+value.total_consumed_weekly_avg+"/day</p>"+
                                  "<p class="+value.weekly_diff_text_class_avg+">"+
                                  value.weekly_diff_text_avg+" = "+value.weekly_diff_avg+"</p>"+
                                  "<p>Water = "+value.total_weekly_water_intake_avg+" Cups</p>";

                    $('#week_average .week_'+ key).html(html_data_weekly);
                }
            });

            // end weekly wise data

            // monthly data
            var monthly_avg = results.response.average.monthly_avg;

            var monthly_avg_html_data = "<p>Calorie Average</p>"
                        +"<p><strong>"+ monthly_avg.monthly_consumed_avg+"/day</strong></p>"
                        +"<p class="+ monthly_avg.monthly_diff_text_class_avg+">"
                              + monthly_avg.monthly_diff_text_avg+ "=" 
                              + monthly_avg.monthly_diff_avg+"</p>"
                        +"<p>Water = "+monthly_avg.monthly_water_avg+" Cups</p>";

            $('#month-average-calorie').html(monthly_avg_html_data);

            // end monthly data

            


      
      });

      vm.events = eventdata;
    //  vm.eventSources = [vm.events];


       $timeout(function() {
            if (uiCalendarConfig.calendars['foodCalendar']) {
               angular.forEach(eventdata, function(value, key) { 
               uiCalendarConfig.calendars['foodCalendar'].fullCalendar('renderEvent',value,false);
              });
            }
         },2000);
    
    }

    vm.renderView =function(view,element){
       // console.log("View Changed: ", view.visStart, view.visEnd, view.start, view.end);
        var date = new Date(view.calendar.getDate());
        vm.monthlyCalorieSummary(date);
    }

    vm.eventRender = function(event, element, view ){
      
         return $(
                  '<div class="tabletd-div cal_sammary_data"><p>Calorie Total</p> <p><b>' + 
                        event.calory + 
                        '</b></p>'+
                        '<p style="color:'+event.diff_text_color+';">'+event.diff_text+' = ' + event.diff + '</p>'+

                    '<p>Water = '+event.water_intake+' Cups</p></div>');
    }


    vm.uiConfig = {
        calendar:{
                editable: false,
                height:"100%",
                theme: true,
                header: {
                    right: 'title',
                    left:'prev,next',
                    center:false
                },
                 
               eventRender: vm.eventRender,
               viewRender: vm.renderView, 

           }
    };

    vm.update_notes = function(){

      var food_note ={};
      food_note.log_date =moment(vm.log_date).format('YYYY-MM-DD');
      food_note.notes =vm.food_notes;
      food_note.guid_server = true;
      StoreService.update('api/v3/user_food_notes/update_note', vm.userId, food_note).then(function(result) {
          if(result.error == false) {

           }  
          }, function(error) {
              console.log(error);
        });
    }

    vm.deleteLoggedFood = function(food_obj){
      
      var id = food_obj.id;
      var food_type = food_obj.food_type;
      var msg = "Are you sure want to delete this food ?";
      
      showFlashMessageConfirmation("Delete",msg,"info",function(result) {
              if (result) {
                  ShowService.remove('api/v3/food/delete_logged_food', id).then(function(results){

                      if(food_type=="Breakfast"){
                          var index = vm.breakfastFood.indexOf(food_obj);
                          vm.breakfastFood.splice(index, 1);

                          //calculation of sub total of food types grand total
                          vm.totalBreakfast.calorie = Number(vm.totalBreakfast.calorie) - Number(food_obj.calories);
                          vm.totalBreakfast.carbs = Number(vm.totalBreakfast.carbs) - Number(food_obj.carb);
                          vm.totalBreakfast.fat = Number(vm.totalBreakfast.fat) - Number(food_obj.fat);
                          vm.totalBreakfast.fiber = Number(vm.totalBreakfast.fiber) - Number(food_obj.fiber);
                          vm.totalBreakfast.protein = Number(vm.totalBreakfast.protein) - Number(food_obj.protein);
                          vm.totalBreakfast.sodium = Number(vm.totalBreakfast.sodium) - Number(food_obj.sodium);

                          // endcalculation of sub total of food types grand total
                      }
                      if(food_type=="Lunch"){
                          var index = vm.lunchFood.indexOf(food_obj);
                          vm.lunchFood.splice(index, 1);

                          //calculation of sub total of food types grand total
                          vm.totalLunch.calorie = Number(vm.totalLunch.calorie) - Number(food_obj.calories);
                          vm.totalLunch.carbs = Number(vm.totalLunch.carbs) - Number(food_obj.carb);
                          vm.totalLunch.fat = Number(vm.totalLunch.fat) - Number(food_obj.fat);
                          vm.totalLunch.fiber = Number(vm.totalLunch.fiber) - Number(food_obj.fiber);
                          vm.totalLunch.protein = Number(vm.totalLunch.protein) - Number(food_obj.protein);
                          vm.totalLunch.sodium = Number(vm.totalLunch.sodium) - Number(food_obj.sodium);

                          
                          // endcalculation of sub total of food types grand total
                      }
                      if(food_type=="Snacks"){
                          var index = vm.snackFood.indexOf(food_obj);
                          vm.snackFood.splice(index, 1);

                          //calculation of sub total of food types grand total
                          vm.totalSnacks.calorie = Number(vm.totalSnacks.calorie) - Number(food_obj.calories);
                          vm.totalSnacks.carbs = Number(vm.totalSnacks.carbs) - Number(food_obj.carb);
                          vm.totalSnacks.fat = Number(vm.totalSnacks.fat) - Number(food_obj.fat);
                          vm.totalSnacks.fiber = Number(vm.totalSnacks.fiber) - Number(food_obj.fiber);
                          vm.totalSnacks.protein = Number(vm.totalSnacks.protein) - Number(food_obj.protein);
                          vm.totalSnacks.sodium = Number(vm.totalSnacks.sodium) - Number(food_obj.sodium);

                          
                          // endcalculation of sub total of food types grand total
                      }
                      if(food_type=="Dinner"){
                          var index = vm.dinnerFood.indexOf(food_obj);
                        vm.dinnerFood.splice(index, 1);

                        //calculation of sub total of food types grand total
                          vm.totalDinner.calorie = Number(vm.totalDinner.calorie) - Number(food_obj.calories);
                          vm.totalDinner.carbs = Number(vm.totalDinner.carbs) - Number(food_obj.carb);
                          vm.totalDinner.fat = Number(vm.totalDinner.fat) - Number(food_obj.fat);
                          vm.totalDinner.fiber = Number(vm.totalDinner.fiber) - Number(food_obj.fiber);
                          vm.totalDinner.protein = Number(vm.totalDinner.protein) - Number(food_obj.protein);
                          vm.totalDinner.sodium = Number(vm.totalDinner.sodium) - Number(food_obj.sodium);

                          
                          // endcalculation of sub total of food types 
                      }

                      // grand total
                      vm.foodSummary.total_consumed = Number(vm.foodSummary.total_consumed) - Number(food_obj.calories);
                      vm.foodSummary.total_remaining = Number(vm.foodSummary.total_remaining) + Number(food_obj.calories);

                      vm.foodSummary.calorie_percentage = Math.round( (Number(vm.foodSummary.total_consumed) / Number(vm.foodSummary.calorie_goal)) * 100 );
                      // end grand total
                      if (uiCalendarConfig.calendars['foodCalendar']) {
                        uiCalendarConfig.calendars['foodCalendar'].fullCalendar('removeEvents');
                        vm.monthlyCalorieSummary();
                      }
                       showFlashMessage('Delete','Food deleted successfully','success');
                    });
                }
      });

    }

    vm.addWaterIntake = function(){
      var water_intake_data = {};
      water_intake_data.user_id = vm.userId;
      var log_date = moment(vm.log_date).format('YYYY-MM-DD');
      water_intake_data.log_date = log_date;
      water_intake_data.log_water = 8;
      water_intake_data.guid_server = 'server';

      StoreService.save('api/v3/water/log_water', water_intake_data).then(function(result) {
          if(result.error == false){
                vm.water_intake = vm.water_intake + 1;
          }
        },function(error) {
          console.log(error);
        });

    }
      
    vm.deleteWaterIntake = function(){

      var log_date = moment(vm.log_date).format('YYYY-MM-DD');

      ShowService.remove('api/v3/water/delete/'+log_date, vm.userId).then(function(result){

          vm.water_intake = vm.water_intake - 1;

      });

    }

     vm.addFoodCompleted = function(){
      var FoodCompleted_data = {};
      FoodCompleted_data.user_id = vm.userId;
      FoodCompleted_data.date = moment(vm.log_date).format('YYYY-MM-DD');
      var msg = "Are you sure Your diary for today is marked ?";

      
      showFlashMessageConfirmation("Food Completed",msg,"info",function(result) {
              if(result) {
                  StoreService.save('api/v3/food/log_food_completed', FoodCompleted_data).then(function(result) {
                    if(result.error == false){
                    }
                  },function(error) {
                    console.log(error);
                  });
                }

                showFlashMessage('Completed','Your today food diary Completed','success');
      });
    }
}]);