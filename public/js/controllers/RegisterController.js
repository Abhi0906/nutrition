
angular.module('app.core').controller('RegisterController', function(ShowService,StoreService,$scope){
    var vm = this;
    vm.activities = [];
    var user_id;
    vm.displayServerError = false;
    vm.displayServerMessage = null;
    vm.ageValidation =false;

    
    vm.loader = false;
    
    vm.registerUser = function() {
      vm.loader = !vm.loader;

      var data = {};
      data = vm.user;
      StoreService.save('api/v3/patient/store',data).then(function(result) {
     if(result.error == false){
          
        }else{
         
        }
      },function(error) {
        console.log(error);
      });
    };

    vm.updateUserInfo = function(){
   
      var data = {};
      data = vm.about_data;
      data.user_id = user_id;
      data.birth_date = moment($("#birth_date").val()).format('YYYY-MM-DD');

      var years = moment().diff(data.birth_date, 'years');
      //console.log(years); return false;
      
      data.activity_id=1
      data.activity_value =1.2

      if(years > 13){
      vm.loder_about = !vm.loder_about;
      StoreService.save('api/v3/patient/updateProfile',data).then(function(result) {
        
        if(result.error == false){
            window.location.href = "/dashboard";
            
        }
        
      },function(error) {
        console.log(error);
      });
    }
    else{  
           vm.ageValidation = true;
         }
    }
  
    vm.getActivityValue = function(id){
      var activity_value = 1;
      angular.forEach(vm.activities, function(activity, key) {
        if(activity['id'] == id){
          activity_value = activity['value'];
        }
     });
     return activity_value;
    }
    vm.resetPassword = function(){
      var data = {};
      data = vm.user;
      StoreService.save('api/v3/password/email',data).then(function(result) {
         if(result.error == false){
            vm.passwordValidation = false;
            vm.passwordSuccessMsg = result.response.message;
            window.location.href = "/sentpassword";
        }else{
          vm.passwordValidation = true;
          vm.passwordSuccessMsg = null;
          vm.passwordValidationMesg = result.response.message;
      }
      },function(error) {
        console.log(error);
      });
    }
});