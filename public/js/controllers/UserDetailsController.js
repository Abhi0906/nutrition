/**
 * Created by plenar on 10/1/2015.
 */

angular.module('app.routes').config(routes);

function routes($routeProvider,$locationProvider){
     $routeProvider
    .when('/demographics', {
        templateUrl: 'demographics',
        controller: 'DemographicsController',
        controllerAs: 'demographics'
      })
      .when('/history', {
        templateUrl: 'history',
        controller: 'DiaryController',
        controllerAs: 'diary'
      })
      .when('/medication', {
        templateUrl: 'medication',
        controller: 'MedicationsController',
        controllerAs: 'medication'
      })
     /* .when('/vitals', {
        templateUrl: 'vitals',
        controller: 'VitalsController',
        controllerAs: 'vitals'
      })*/
      .when('/profile_goals', {
        templateUrl: 'profile_goals',
        controller: 'ProfileController',
        controllerAs: 'profile_goals'
      })
      .when('/schedules', {
        templateUrl: 'schedules',
        controller: 'ScheduleController',
        controllerAs: 'schedule'
      })
      .when('/weight', {
        templateUrl: 'weight',
        controller: 'WeightController',
        controllerAs: 'weight'
      })
      .when('/assign_patients', {
        templateUrl: 'assign_patients',
        controller: 'AssignpatientsController',
        controllerAs: 'assign_patients'
      })
      .otherwise({
        redirectTo: '/'
      });

      // use the HTML5 History API
      $locationProvider.html5Mode(true);

}        
angular.module('app.core').controller('UserDetailsController',['$location','$rootScope', function($location,$rootScope){
    var vm = this;
    vm.userId='';
    vm.userType='';
    vm.menu_content={}; 
    vm.right_block_title = "Expand All";   

    vm.selectedBlockTitle = 'Demographics';
    var path = 'demographics';
   
    vm.patientNav = [{'title':'Demographics',
    					'href':'demographics',
    					'class_name':'fa fa-user fa-fw'
    				   },
    				   {'title':'History',
    					'href':'history',
    					'class_name':'fa fa-history fa-fw'
    				   },
    				   {'title':'Medication',
    					'href':'medication',
    					'class_name':'wellness-icon-medicine'
    				   },
    				 /*  {'title':'Vital Thresholds',
    					'href':'vitals',
    					'class_name':'wellness-icon-vital'
    				   },*/
    				   {'title':'Profile & Goals',
    					'href':'profile_goals',
    					'class_name':'fa fa-bullseye fa-fw'
    				   },
    				   {'title':'Schedule',
    					'href':'schedules',
    					'class_name':'fa fa-calendar fa-fw'
    				   },
		               {'title':'Weight',
		              'href':'weight',
		              'class_name':'fa wellness-icon-weight fa-fw'
		               }
    				];
    vm.doctorNav = 	[{'title':'Demographics',
    					'href':'demographics',
    					'class_name':'fa fa-user fa-fw'
    				   },
               {'title':'Patients',
              'href':'assign_patients',
              'class_name':'fa fa-users'
               }
    			    ];		                   
    
    vm.setMenu = function(){
        if(vm.userType == 'patient'){
            vm.menu_content=vm.patientNav;
        }else {
            vm.menu_content=vm.doctorNav;
        }

        var location_path = $location.path().substring(1, $location.path().length);    

        angular.forEach(vm.menu_content, function(value, key) {
            if(value.href ==  location_path ){
                 vm.selectedBlockTitle = value.title;
                 path = value.href;
                
            }
         });

        $location.url(path);
     }
     

     vm.editSchedule = function(){
       $rootScope.$broadcast('editSchedule');
     }     
    vm.expandAll = function(){

      var blockContent = $('[data-toggle="block-toggle-content"]').closest('.block').find('.block-content');
      
      if (blockContent.css('display')=='block' || vm.right_block_title == "Collapse All") {
           blockContent.slideUp();
           $('[data-toggle="block-toggle-content"]').addClass('active');
           vm.right_block_title = "Expand All";
         
      } else {
          blockContent.slideDown();
          $('[data-toggle="block-toggle-content"]').removeClass('active');
          vm.right_block_title = "Collapse All";
         
      }
    }
  
}]);
