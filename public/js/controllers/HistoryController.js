/**
 * History controller for displaying history for patient.
 */

angular.module('app.core').controller('HistoryController', ['ShowService','StoreService','$scope',function(ShowService,StoreService,$scope){
    var vm = this;
    vm.userId = $scope.userDetails.userId;
    vm.userHealthDiary = [];

    $scope.$watchGroup(['history.diary_from','history.diary_to'],function(new_value){
        vm.diary_from = new_value[0];
        vm.diary_to = new_value[1];
    });    

    vm.getHealthDiaryData = function(){

        var diff = moment(vm.diary_to).diff(vm.diary_from,'days');
           if(diff < 0){
              vm.diary_to = vm.diary_from
           }
       
        var params ={ 
                    'diary_from':moment(vm.diary_from).format('YYYY-MM-DD'),
                    'diary_to':moment(vm.diary_to).format('YYYY-MM-DD'),
                    'user_id':vm.userId
                };

        ShowService.search('api/v3/patient/user_health_diary',params,1).then(function(results){
            vm.userHealthDiary =  results.response
        });
    }

    vm.getDate = function(date){
      return moment(date).format('MMMM DD, YYYY');
    }

}]);



