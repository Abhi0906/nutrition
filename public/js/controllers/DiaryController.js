angular.module('app.core').controller('DiaryController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$location', '$anchorScroll','uibDateParser','uiCalendarConfig', '$rootScope', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $location, $anchorScroll,uibDateParser,uiCalendarConfig, $rootScope){
    var vm = this;
    //$('.back-top').trigger('click');
    vm.diary_summary_loader = $scope.diary_index.diary_summary_loader;
    vm.userId = $scope.diary_index.userId;
    vm.log_date = $scope.diary_index.log_date;
    vm.food_notes ="edit notes";
    vm.eventSources =[];

    vm.meal_one_food =[];
    vm.meal_two_food = [];
    vm.meal_three_food =[];
    vm.meal_four_food =[];
    vm.meal_five_food =[];
    vm.meal_six_food =[];
    vm.pre_workout_food =[];
    vm.post_workout_food =[];



    vm.meal_one_total=[];
    vm.meal_two_total=[];
    vm.meal_three_total=[];
    vm.meal_four_total=[];
    vm.meal_five_total=[];
    vm.meal_six_total=[];
    vm.pre_workout_total=[];
    vm.post_workout_total=[];

    vm.foodSummary=[];
    vm.food_exercise_type = $scope.diary_index.food_exercise_type;


    vm.cardioExercises =[];
    vm.resistanceExercises =[];
    vm.totalCardio=[];
    vm.totalResistance=[];
    vm.exerciseSummary=[];

    vm.water_intake = 0;
    vm.format = 'MM-dd-yyyy';
    vm.dateOptions = {
      showWeeks:false
    };

    if(vm.food_exercise_type){

        $anchorScroll.yOffset = 220;
        var old = $location.hash();
        $location.hash(vm.food_exercise_type);
        /*$anchorScroll();
        $location.hash(old);*/

    }

    vm.food_date_open = function() {
        vm.food_date_popup.opened = true;
    };

    vm.food_date_popup = {
        opened: false
    };

    vm.prevFoodDairy = function(){
        vm.diary_summary_loader = true;
       var yesterday = new Date(new Date(vm.log_date).getTime() - 24 * 60 * 60 * 1000);
        vm.log_date = yesterday;
        $scope.diary_index.log_date = yesterday;
        $scope.diary_index.getFoodDiary();

    }

    vm.nextFoodDairy = function(){
        vm.diary_summary_loader = true;
        var tomarrow = new Date(new Date(vm.log_date).getTime() + 24 * 60 * 60 * 1000);
        vm.log_date = tomarrow;
        $scope.diary_index.log_date = tomarrow;
        $scope.diary_index.getFoodDiary();

        //$scope.diary_index.getExerciseDiary();
    }

    vm.changeDate = function(time_type){
        vm.diary_summary_loader = true;
        vm.log_date = moment(vm.log_date).format('MM/DD/YYYY');
        vm.log_date = new Date(vm.log_date);
        $scope.diary_index.log_date = vm.log_date;
        $scope.diary_index.getFoodDiary();

       // $scope.diary_index.getExerciseDiary();
    }

    vm.setFoodDiary =  function(){

        vm.meal_one_food = $scope.diary_index.meal_one_food;
        vm.meal_two_food = $scope.diary_index.meal_two_food;
        vm.meal_three_food = $scope.diary_index.meal_three_food;
        vm.meal_four_food = $scope.diary_index.meal_four_food;
        vm.meal_five_food = $scope.diary_index.meal_five_food;
        vm.meal_six_food = $scope.diary_index.meal_six_food;
        vm.pre_workout_food = $scope.diary_index.pre_workout_food;
        vm.post_workout_food = $scope.diary_index.post_workout_food;

        vm.meal_one_total = $scope.diary_index.meal_one_total;
        vm.meal_two_total = $scope.diary_index.meal_two_total;
        vm.meal_three_total = $scope.diary_index.meal_three_total;
        vm.meal_four_total = $scope.diary_index.meal_four_total;
        vm.meal_five_total = $scope.diary_index.meal_five_total;
        vm.meal_six_total = $scope.diary_index.meal_six_total;
        vm.pre_workout_total = $scope.diary_index.pre_workout_total;
        vm.post_workout_total = $scope.diary_index.post_workout_total;
        vm.foodSummary = $scope.diary_index.foodSummary;


    }

    vm.setExerciseDiary = function(){
        vm.cardioExercises = $scope.diary_index.cardioExercises;
        vm.resistanceExercises = $scope.diary_index.resistanceExercises;
        vm.totalCardio = $scope.diary_index.totalCardio;
        vm.totalResistance = $scope.diary_index.totalResistance;
        vm.exerciseSummary = $scope.diary_index.exerciseSummary;
        vm.frequent_exercises = $scope.diary_index.frequent_exercises;
        vm.recent_exercises = $scope.diary_index.recent_exercises;
        vm.exercise_notes = $scope.diary_index.exercise_notes;
        vm.appleHealthExercises = $scope.diary_index.appleHealthExercises;
        vm.totalAppleHealth = $scope.diary_index.totalAppleHealth;
    }

    vm.setOtherDiaryData = function(){
        vm.notes = $scope.diary_index.notes;
        vm.water_intake = $scope.diary_index.water_intake;
    }


    vm.callSetFunctions = function(){
        vm.setFoodDiary();
        vm.setExerciseDiary();
        vm.setOtherDiaryData();
    }

    if(vm.food_exercise_type==null){

      $rootScope.$on('onGetFoodExercise', function(event, obj) {

        if(obj.data_type == 'food'){

            $timeout(function() {
                    vm.callSetFunctions();
                    vm.diary_summary_loader = false;
            }, 1000);

        }

      });
    }
    else{
      vm.callSetFunctions();
      vm.diary_summary_loader = false;
    }

    vm.callSetFunctions();
    vm.diary_summary_loader = false;
    //alert($scope.diary_index.backFromDiary);
    vm.monthlyCalorieSummary = function(calendar_date){

      var param = vm.userId;
      var month = moment(calendar_date).format('MM');
      var year = moment(calendar_date).format('YYYY');
      param = vm.userId+'?req_month='+month+'&req_year='+year;
      var eventdata = [];
      ShowService.get('api/v3/diary/monthly_calorie_summary', param).then(function(results){

           // date wise data
           var values = results.response.date_wise;

           angular.forEach(values, function(value, key) {

                      eventdata.push({
                          "calory":value.total_consumed,
                          "exercise":value.total_calorie_burned,
                          "start":value.date,
                          "diff_text":value.diff_text,
                          "diff_text_color":value.diff_text_color,
                          "diff":value.diff,
                          "water_intake":value.water_intake
                        });

            });
           // end date wise data

           // weekly wise data
           var weekly_avg_data = results.response.average.week_wise_avg;

            angular.forEach(weekly_avg_data, function(value, key) {
              var html_data_weekly = "";
               $('#week_average .week_'+ key).html(html_data_weekly);
                if(value.length == undefined){

                    html_data_weekly = "<p>Calorie Average</p><p>"+value.total_consumed_weekly_avg+"/day</p>"+
                                  "<p class="+value.weekly_diff_text_class_avg+">"+
                                  "<p>Exercise Average</p><p>"+value.total_exercise_weekly_avg+"/day</p>"+
                                  "<p class="+ value.weekly_diff_text_class_avg+">"
                                  + value.weekly_diff_text_avg+" = "+value.weekly_diff_avg+"</p>"+
                                  "<p>Water = "+value.total_weekly_water_intake_avg+" Cups</p>";

                    $('#week_average .week_'+ key).html(html_data_weekly);
                }
            });

            // end weekly wise data

            // monthly data
            var monthly_avg = results.response.average.monthly_avg;

            var monthly_avg_html_data = "<p>Calorie Average</p>"
                        +"<p><strong>"+ monthly_avg.monthly_consumed_avg+"/day</strong></p>"
                        +"<p>Exercise Average</p>"
                        +"<p><strong>"+ monthly_avg.monthly_exercise_avg+"/day</strong></p>"
                        +"<p class="+ monthly_avg.monthly_diff_text_class_avg+">"
                              + monthly_avg.monthly_diff_text_avg+ "="
                              + monthly_avg.monthly_diff_avg+"</p>"
                        +"<p>Water = "+monthly_avg.monthly_water_avg+" Cups</p>";

            $('#month-average-calorie').html(monthly_avg_html_data);

            // end monthly data





      });

      vm.events = eventdata;
    //  vm.eventSources = [vm.events];


       $timeout(function() {
            if (uiCalendarConfig.calendars['diaryCalendar']) {
               angular.forEach(eventdata, function(value, key) {
               uiCalendarConfig.calendars['diaryCalendar'].fullCalendar('renderEvent',value,false);
              });
            }
         },2000);

    }

    vm.renderView =function(view,element){
       // console.log("View Changed: ", view.visStart, view.visEnd, view.start, view.end);
        var date = new Date(view.calendar.getDate());
        vm.monthlyCalorieSummary(date);
    }

    vm.eventRender = function(event, element, view ){

         return $(
                  '<div class="tabletd-div cal_sammary_data"><p>Calorie Total</p> <p><b>' +
                        event.calory +
                        '</b></p>'+
                        '<p>Exercise Total</p> <p><b>' +
                        event.exercise +
                        '<p style="color:'+event.diff_text_color+';">'+event.diff_text+' = ' + event.diff + '</p>'+

                    '<p>Water = '+event.water_intake+' Cups</p></div>');
    }


    vm.uiConfig = {
        calendar:{
                editable: false,
                height:"100%",
                theme: true,
                header: {
                    right: 'title',
                    left:'prev,next',
                    center:false
                },

               eventRender: vm.eventRender,
               viewRender: vm.renderView,

           }
    };

    vm.update_notes = function(){

      var diary_note ={};
      diary_note.log_date =moment(vm.log_date).format('YYYY-MM-DD');
      diary_note.notes =vm.notes;
      diary_note.guid_server = true;

      StoreService.update('api/v3/diary/update_diary_note', vm.userId, diary_note).then(function(result) {
          if(result.error == false) {

           }
          }, function(error) {
              console.log(error);
        });
    }

    vm.deleteLoggedFood = function(food_obj){

      var id = food_obj.id;
      var food_type = food_obj.food_type;
      var msg = "Are you sure want to delete this food ?";

      showFlashMessageConfirmation("Delete",msg,"info",function(result) {
              if (result) {
                  ShowService.remove('api/v3/food/delete_logged_food', id).then(function(results){

                      if(food_type=="meal_1"){
                          var index = vm.meal_one_food.indexOf(food_obj);
                          vm.meal_one_food.splice(index, 1);

                          //calculation of sub total of food types grand total
                          vm.meal_one_total.calorie = Number(vm.meal_one_total.calorie) - Number(food_obj.calories);
                          vm.meal_one_total.carbs = Number(vm.meal_one_total.carbs) - Number(food_obj.carb);
                          vm.meal_one_total.fat = Number(vm.meal_one_total.fat) - Number(food_obj.fat);
                          vm.meal_one_total.fiber = Number(vm.meal_one_total.fiber) - Number(food_obj.fiber);
                          vm.meal_one_total.protein = Number(vm.meal_one_total.protein) - Number(food_obj.protein);
                          vm.meal_one_total.sodium = Number(vm.meal_one_total.sodium) - Number(food_obj.sodium);

                          // endcalculation of sub total of food types grand total
                      }
                      if(food_type=="meal_2"){
                          var index = vm.meal_two_food.indexOf(food_obj);
                          vm.meal_two_food.splice(index, 1);

                          //calculation of sub total of food types grand total
                          vm.meal_two_total.calorie = Number(vm.meal_two_total.calorie) - Number(food_obj.calories);
                          vm.meal_two_total.carbs = Number(vm.meal_two_total.carbs) - Number(food_obj.carb);
                          vm.meal_two_total.fat = Number(vm.meal_two_total.fat) - Number(food_obj.fat);
                          vm.meal_two_total.fiber = Number(vm.meal_two_total.fiber) - Number(food_obj.fiber);
                          vm.meal_two_total.protein = Number(vm.meal_two_total.protein) - Number(food_obj.protein);
                          vm.meal_two_total.sodium = Number(vm.meal_two_total.sodium) - Number(food_obj.sodium);


                          // endcalculation of sub total of food types grand total
                      }
                      if(food_type=="meal_3"){
                          var index = vm.meal_three_food.indexOf(food_obj);
                          vm.meal_three_food.splice(index, 1);

                          //calculation of sub total of food types grand total
                          vm.meal_three_total.calorie = Number(vm.meal_three_total.calorie) - Number(food_obj.calories);
                          vm.meal_three_total.carbs = Number(vm.meal_three_total.carbs) - Number(food_obj.carb);
                          vm.meal_three_total.fat = Number(vm.meal_three_total.fat) - Number(food_obj.fat);
                          vm.meal_three_total.fiber = Number(vm.meal_three_total.fiber) - Number(food_obj.fiber);
                          vm.meal_three_total.protein = Number(vm.meal_three_total.protein) - Number(food_obj.protein);
                          vm.meal_three_total.sodium = Number(vm.meal_three_total.sodium) - Number(food_obj.sodium);


                          // endcalculation of sub total of food types grand total
                      }
                      if(food_type=="meal_4"){
                          var index = vm.meal_four_food.indexOf(food_obj);
                        vm.meal_four_food.splice(index, 1);

                        //calculation of sub total of food types grand total
                          vm.meal_four_total.calorie = Number(vm.meal_four_total.calorie) - Number(food_obj.calories);
                          vm.meal_four_total.carbs = Number(vm.meal_four_total.carbs) - Number(food_obj.carb);
                          vm.meal_four_total.fat = Number(vm.meal_four_total.fat) - Number(food_obj.fat);
                          vm.meal_four_total.fiber = Number(vm.meal_four_total.fiber) - Number(food_obj.fiber);
                          vm.meal_four_total.protein = Number(vm.meal_four_total.protein) - Number(food_obj.protein);
                          vm.meal_four_total.sodium = Number(vm.meal_four_total.sodium) - Number(food_obj.sodium);


                          // endcalculation of sub total of food types
                      }

                      if(food_type=="meal_5"){
                          var index = vm.meal_five_food.indexOf(food_obj);
                          vm.meal_five_food.splice(index, 1);

                          //calculation of sub total of food types grand total
                          vm.meal_five_total.calorie = Number(vm.meal_five_total.calorie) - Number(food_obj.calories);
                          vm.meal_five_total.carbs = Number(vm.meal_five_total.carbs) - Number(food_obj.carb);
                          vm.meal_five_total.fat = Number(vm.meal_five_total.fat) - Number(food_obj.fat);
                          vm.meal_five_total.fiber = Number(vm.meal_five_total.fiber) - Number(food_obj.fiber);
                          vm.meal_five_total.protein = Number(vm.meal_five_total.protein) - Number(food_obj.protein);
                          vm.meal_five_total.sodium = Number(vm.meal_five_total.sodium) - Number(food_obj.sodium);


                          // endcalculation of sub total of food types
                      }

                      if(food_type=="meal_6"){
                          var index = vm.meal_six_food.indexOf(food_obj);
                          vm.meal_six_food.splice(index, 1);

                          //calculation of sub total of food types grand total
                          vm.meal_six_total.calorie = Number(vm.meal_six_total.calorie) - Number(food_obj.calories);
                          vm.meal_six_total.carbs = Number(vm.meal_six_total.carbs) - Number(food_obj.carb);
                          vm.meal_six_total.fat = Number(vm.meal_six_total.fat) - Number(food_obj.fat);
                          vm.meal_six_total.fiber = Number(vm.meal_six_total.fiber) - Number(food_obj.fiber);
                          vm.meal_six_total.protein = Number(vm.meal_six_total.protein) - Number(food_obj.protein);
                          vm.meal_six_total.sodium = Number(vm.meal_six_total.sodium) - Number(food_obj.sodium);


                          // endcalculation of sub total of food types
                      }

                      if(food_type=="pre_workout_meal"){
                          var index = vm.pre_workout_food.indexOf(food_obj);
                          vm.pre_workout_food.splice(index, 1);

                          //calculation of sub total of food types grand total
                          vm.pre_workout_total.calorie = Number(vm.pre_workout_total.calorie) - Number(food_obj.calories);
                          vm.pre_workout_total.carbs = Number(vm.pre_workout_total.carbs) - Number(food_obj.carb);
                          vm.pre_workout_total.fat = Number(vm.pre_workout_total.fat) - Number(food_obj.fat);
                          vm.pre_workout_total.fiber = Number(vm.pre_workout_total.fiber) - Number(food_obj.fiber);
                          vm.pre_workout_total.protein = Number(vm.pre_workout_total.protein) - Number(food_obj.protein);
                          vm.pre_workout_total.sodium = Number(vm.pre_workout_total.sodium) - Number(food_obj.sodium);


                          // endcalculation of sub total of food types
                      }

                      if(food_type=="post_workout_meal"){
                          var index = vm.post_workout_food.indexOf(food_obj);
                          vm.post_workout_food.splice(index, 1);

                          //calculation of sub total of food types grand total
                          vm.post_workout_total.calorie = Number(vm.post_workout_total.calorie) - Number(food_obj.calories);
                          vm.post_workout_total.carbs = Number(vm.post_workout_total.carbs) - Number(food_obj.carb);
                          vm.post_workout_total.fat = Number(vm.post_workout_total.fat) - Number(food_obj.fat);
                          vm.post_workout_total.fiber = Number(vm.post_workout_total.fiber) - Number(food_obj.fiber);
                          vm.post_workout_total.protein = Number(vm.post_workout_total.protein) - Number(food_obj.protein);
                          vm.post_workout_total.sodium = Number(vm.post_workout_total.sodium) - Number(food_obj.sodium);


                          // endcalculation of sub total of food types
                      }

                      // grand total
                      vm.foodSummary.total_consumed = Number(vm.foodSummary.total_consumed) - Number(food_obj.calories);
                      vm.foodSummary.total_remaining = Number(vm.foodSummary.total_remaining) + Number(food_obj.calories);

                      vm.foodSummary.calorie_percentage = Math.round( (Number(vm.foodSummary.total_consumed) / Number(vm.foodSummary.calorie_goal)) * 100 );
                      // end grand total
                      if (uiCalendarConfig.calendars['diaryCalendar']) {
                        uiCalendarConfig.calendars['diaryCalendar'].fullCalendar('removeEvents');
                        vm.monthlyCalorieSummary();
                      }
                       showFlashMessage('Delete','Food deleted successfully','success');
                    });
                }
      });

    }

    vm.addWaterIntake = function(){
      var water_intake_data = {};
      water_intake_data.user_id = vm.userId;
      var log_date = moment(vm.log_date).format('YYYY-MM-DD');
      water_intake_data.log_date = log_date;
      water_intake_data.log_water = 8;
      water_intake_data.guid_server = 'server';

      StoreService.save('api/v3/water/log_water', water_intake_data).then(function(result) {
          if(result.error == false){
                vm.water_intake = vm.water_intake + 1;
          }
        },function(error) {
          console.log(error);
        });

    }

    vm.deleteWaterIntake = function(){

      var log_date = moment(vm.log_date).format('YYYY-MM-DD');

      ShowService.remove('api/v3/water/delete/'+log_date, vm.userId).then(function(result){

          vm.water_intake = vm.water_intake - 1;

      });

    }

    vm.addFoodCompleted = function(){
      var FoodCompleted_data = {};
      FoodCompleted_data.user_id = vm.userId;
      FoodCompleted_data.date = moment(vm.log_date).format('YYYY-MM-DD');
      var msg = "Are you sure Your diary for today is marked ?";


      showFlashMessageConfirmation("Food Completed",msg,"info",function(result) {
              if(result) {
                  StoreService.save('api/v3/food/log_food_completed', FoodCompleted_data).then(function(result) {
                    if(result.error == false){
                    }
                  },function(error) {
                    console.log(error);
                  });
                }

                showFlashMessage('Completed','Your today food diary Completed','success');
      });
    }

    vm.deleteExercise = function(exercise_obj){

      var id = exercise_obj.id;
      var exercise_type = exercise_obj.exercise_type;
      var msg = "Are you sure want to delete this exercise ?";

      showFlashMessageConfirmation("Delete",msg,"info",function(result) {
              if (result) {
                  ShowService.remove('api/v3/exercise/delete_exercise', id).then(function(results){

                      if(exercise_type=="Cardio"){
                          var index = vm.cardioExercises.indexOf(exercise_obj);
                          vm.cardioExercises.splice(index, 1);
                          //calculation of sub total of exercise types grand total
                          vm.totalCardio.calories_burned = Number(vm.totalCardio.calories_burned) - Number(exercise_obj.calories_burned);
                          vm.totalCardio.time = Number(vm.totalCardio.time) - Number(exercise_obj.time);
                         // endcalculation of sub total of exercise types grand total
                      }
                      if(exercise_type=="Resistance"){
                          var index = vm.resistanceExercises.indexOf(exercise_obj);
                          vm.resistanceExercises.splice(index, 1);

                          //calculation of sub total of food types grand total
                          vm.totalResistance.calories_burned = Number(vm.totalResistance.calories_burned) - Number(exercise_obj.calories_burned);
                          vm.totalResistance.time = Number(vm.totalResistance.time) - Number(exercise_obj.time);
                          vm.totalResistance.sets = Number(vm.totalResistance.sets) - Number(exercise_obj.sets);
                          vm.totalResistance.reps = Number(vm.totalResistance.reps) - Number(exercise_obj.reps);
                          vm.totalResistance.weight_lbs = Number(vm.totalResistance.weight_lbs) - Number(exercise_obj.weight_lbs);


                          // endcalculation of sub total of food types grand total
                      }

                        // grand total
                        vm.exerciseSummary.total_burned = Number(vm.exerciseSummary.total_burned) - Number(exercise_obj.calories_burned);
                        vm.exerciseSummary.total_remaining = Number(vm.exerciseSummary.total_remaining) + Number(exercise_obj.calories_burned);
                        vm.exerciseSummary.calorie_burned_percentage = Math.round( (Number(vm.exerciseSummary.total_burned) / Number(vm.exerciseSummary.calorie_burned_goal)) * 100 );
                        // end grand total


                      if (uiCalendarConfig.calendars['exerciseCalendar']) {
                        uiCalendarConfig.calendars['exerciseCalendar'].fullCalendar('removeEvents');
                        vm.monthlyCalorieSummary();
                      }

                       showFlashMessage('Delete','Exercise deleted successfully','success');
                    });
                }
      });

    }
}]);