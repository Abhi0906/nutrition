angular.module('app.core').controller('AssignpatientsController',['ShowService','$scope','StoreService','$rootScope', function(ShowService,$scope,StoreService,$rootScope){
    var vm = this;
    vm.userId = $scope.userDetails.userId;
    vm.assign_patients = [];
    vm.nonassign_patients = [];

    //get assign patient to doctor
    ShowService.get('api/v3/doctor/assign_patients',vm.userId).then(function(results){
        vm.assign_patients = results.response;     
    });
  	
  	// //get non assign patient to doctor
   //  ShowService.get('api/v3/doctor/nonassign_patients',vm.userId).then(function(results){
   //      vm.nonassign_patients = results.response;     
   //  });

   vm.sort = function(keyname){
        vm.sortKey = keyname;   //set the sortKey to the param passed
        vm.reverse = !vm.reverse; //if true make it false and vice versa
    }

    //assign patient to doctor
    vm.assignPatientToDoctor = function(assigned_patient){	
    	var data = {};
    	data.doctor_id =  vm.userId;
    	data.patient_id = assigned_patient.id;
      var index = vm.nonassign_patients.indexOf(assigned_patient);

    	StoreService.save('api/v3/doctor/patients',data).then(function(result) {
        if(result.error == false){
          vm.assign_patients.push(result.response.user); 
          showFlashMessage('Assign','Patient assign successfully','success');
          vm.nonassign_patients.splice(index, 1);
        }
    	},function(error) {
        console.log(error);
      });
    }

    //unassign patient from doctor
    vm.unassignPatientFromDoctor = function(unassigned_patient) {

        var doctor_id =  vm.userId;
        var patient_id = unassigned_patient.id;
        var index = vm.assign_patients.indexOf(unassigned_patient);

        var msg = "Are you sure want to unassign "+ unassigned_patient.full_name;
        showFlashMessageConfirmation("Unassign",msg,"info",function(result) {
            if (result) {
                ShowService.remove('api/v3/doctor/remove_assign_patient/'+doctor_id,patient_id).then(function(results){
                   if(results.error == false){
                      vm.assign_patients.splice(index, 1);
                      showFlashMessage('Unassign','Patient Unassign successfully','success');
                       
                   }
                });
            }
        }); 
    }

     vm.getDate = function(date){
      return moment(date).format('MMMM DD, YYYY');
    }
     
}]);


