angular.module('app.core').controller('ExerciseDiaryController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$location', '$anchorScroll','uibDateParser','uiCalendarConfig', 
  function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $location, $anchorScroll,uibDateParser,uiCalendarConfig){
    var vm = this;
    vm.userId = $scope.exercise.userId;
    vm.log_date = $scope.exercise.log_date;
    vm.eventSources =[];
    vm.exercise_type = $scope.exercise.exercise_type;
    vm.cardioExercises =[];
    vm.resistanceExercises =[];
    vm.totalCardio=[];
    vm.totalResistance=[];
    vm.exerciseSummary=[];
    vm.format = 'MM-dd-yyyy';
    vm.events=[]
  
    vm.dateOptions = {
      showWeeks:false
    };
    if(vm.exercise_type){
        $anchorScroll.yOffset = 150;
        var old = $location.hash();
        $location.hash(vm.exercise_type+'_focus_id');
        $anchorScroll();
        $location.hash(old);
    }

    vm.exercise_date_open = function() {
        vm.exercise_date_popup.opened = true;
    };
    
    vm.exercise_date_popup = {
        opened: false
    };


    vm.prevExerciseDairy = function(){
      
      var yesterday = new Date(new Date(vm.log_date).getTime() - 24 * 60 * 60 * 1000);
      vm.log_date = yesterday;
      $scope.exercise.log_date = yesterday;
      vm.getExerciseDiary();

      
    }
    vm.nextExerciseDairy = function(){
        var tomarrow = new Date(new Date(vm.log_date).getTime() + 24 * 60 * 60 * 1000);
        vm.log_date = tomarrow;
        $scope.exercise.log_date = tomarrow;
        vm.getExerciseDiary();
       
    }
    vm.changeDate = function(time_type){
      vm.log_date = moment(vm.log_date).format('MM/DD/YYYY');
      vm.log_date = new Date(vm.log_date);
      $scope.exercise.log_date = vm.log_date;
      vm.getExerciseDiary();
    }

    vm.getExerciseDiary = function(){

      var params = vm.userId;
      var log_date = moment(vm.log_date).format('YYYY-MM-DD');
      params = vm.userId+'?log_date='+log_date;
     
      ShowService.get('api/v3/exercise/patient_exercise', params).then(function(result){

         vm.cardioExercises = result.response.loggedExercise.Cardio;
         vm.resistanceExercises = result.response.loggedExercise.Resistance;
         vm.totalCardio = result.response.loggedExercise.cardio_total;
         vm.totalResistance = result.response.loggedExercise.resistance_total;
         vm.exerciseSummary = result.response.exercise_summary;
         $scope.exercise.frequent_exercises = result.response.frequent_exercises;
         $scope.exercise.recent_exercises = result.response.recent_exercises;   
         vm.exercise_notes = result.response.exercise_notes;
         vm.appleHealthExercises = result.response.apple_health_exercises.apple_health;
         vm.totalAppleHealth = result.response.apple_health_exercises.apple_health_total;
      });

    }

    vm.monthlyCalorieSummary = function(calendar_date){
     
      var param = vm.userId;
      var month = moment(calendar_date).format('MM');
      var year = moment(calendar_date).format('YYYY');
    

      param = vm.userId+'?req_month='+month+'&req_year='+year;
      var eventdata = [];
      ShowService.get('api/v3/exercise/monthly_calorie_summary', param).then(function(results){
           
           // date wise data
           var values = results.response.date_wise;
              
           var week_wise_avg = results.response.week_wise_avg;
           
           vm.week_wise_avg_html = {}; 
           angular.forEach(week_wise_avg, function(value, key) {

           var html_data_weekly = "";
            $('#week_average .week_'+ key).html(html_data_weekly);
             if(value.length == undefined){
                   html_data_weekly = "Calorie Burned = <span class="+value.weekly_calories_class_avg+">" +  
                                                                            value.total_calories_weekly_avg +  
                                                                            "<span class="+value.weekly_class_avg+"><br>"+
                                                                            value.diff_text+ " = "+value.week_total_deficit+"</span>";



                            $('#week_average .week_'+ key).html(html_data_weekly);
             }
           });

           vm.monthly_avg_calories = results.response.monthly_avg.monthly_avg_calories;
           vm.monthly_calories_class_avg = results.response.monthly_avg.monthly_calories_class_avg;
           vm.monthly_month_calories = results.response.monthly_avg.monthly_month_calories;
           vm.diff_text = results.response.monthly_avg.diff_text;
           vm.monthly_class_avg = results.response.monthly_avg.monthly_class_avg;
         
           
           vm.calories  = '<p>Calorie Burned Average = <span class='+vm.monthly_calories_class_avg +'>' 
                                                                    + vm.monthly_avg_calories +"<br>"
                                                                    +"<span class="+vm.monthly_class_avg +">"
                                                                    +vm.diff_text+" = "+
                                                                    vm.monthly_month_calories +'</p></span>';

           angular.forEach(values, function(value, key) {

                      eventdata.push({
                          "title":value.total_calorie_burned, 
                          "start":value.date, 
                          "diff_text":value.diff_text,  
                          "diff_text_color":value.diff_text_color,
                          "diff":value.diff,
                          "calorie_burned":value.total_calorie_burned,
                          
                        });
          
            });
           // end date wise data


       });
        vm.events = eventdata;
       // vm.eventSources = [vm.events];

        $timeout(function() {
            if (uiCalendarConfig.calendars['exerciseCalendar']) {
               angular.forEach(eventdata, function(value, key) { 
               uiCalendarConfig.calendars['exerciseCalendar'].fullCalendar('renderEvent',value,false);
              });
            }
         },2000);
     
    }

   


    vm.eventRender = function(event, element, view ){
     return $(
          '<div class="tabletd-div cal_sammary_data"><p>Calorie Burned Total</p> <p><b>' + 
                event.calorie_burned + 
                '</b></p>'+
                '<p style="color:'+event.diff_text_color+';">'+event.diff_text+' = ' + event.diff + '</p>'+

            '</div>');
    }

   vm.renderView = function(view,element){
      // console.log("View Changed: ", view.visStart, view.visEnd, view.start, view.end);
        var date = new Date(view.calendar.getDate());
        vm.monthlyCalorieSummary(date);
       
    }

                  

    vm.uiConfig = {
        calendar:{
                editable: false,
                height:"100%",
                theme: true,
               header: {
                    right: 'title',
                    left:'prev,next',
                    center:false
                },
               // lazyFetching:false,
               eventRender: vm.eventRender,
               viewRender: vm.renderView, 
               

           }
    };
 
    vm.update_notes = function(){
       
       var exercise_note ={};
       exercise_note.log_date =moment(vm.log_date).format('YYYY-MM-DD');
       exercise_note.notes =vm.exercise_notes;
       exercise_note.guid_server = true;
       StoreService.update('api/v3/user_exercise_notes/update_note',vm.userId,exercise_note).then(function(result) {
            if(result.error == false) {

             }  
            }, function(error) {
                console.log(error);
          });


    }


    vm.deleteExercise = function(exercise_obj){
      
      var id = exercise_obj.id;
      var exercise_type = exercise_obj.exercise_type;
      var msg = "Are you sure want to delete this exercise ?";
      
      showFlashMessageConfirmation("Delete",msg,"info",function(result) {
              if (result) {
                  ShowService.remove('api/v3/exercise/delete_exercise', id).then(function(results){

                      if(exercise_type=="Cardio"){
                          var index = vm.cardioExercises.indexOf(exercise_obj);
                          vm.cardioExercises.splice(index, 1);
                          //calculation of sub total of exercise types grand total
                          vm.totalCardio.calories_burned = Number(vm.totalCardio.calories_burned) - Number(exercise_obj.calories_burned);
                          vm.totalCardio.time = Number(vm.totalCardio.time) - Number(exercise_obj.time);
                         // endcalculation of sub total of exercise types grand total
                      }
                      if(exercise_type=="Resistance"){
                          var index = vm.resistanceExercises.indexOf(exercise_obj);
                          vm.resistanceExercises.splice(index, 1);

                          //calculation of sub total of food types grand total
                          vm.totalResistance.calories_burned = Number(vm.totalResistance.calories_burned) - Number(exercise_obj.calories_burned);
                          vm.totalResistance.time = Number(vm.totalResistance.time) - Number(exercise_obj.time);
                          vm.totalResistance.sets = Number(vm.totalResistance.sets) - Number(exercise_obj.sets);
                          vm.totalResistance.reps = Number(vm.totalResistance.reps) - Number(exercise_obj.reps);
                          vm.totalResistance.weight_lbs = Number(vm.totalResistance.weight_lbs) - Number(exercise_obj.weight_lbs);

                          
                          // endcalculation of sub total of food types grand total
                      }
                      
                        // grand total
                        vm.exerciseSummary.total_burned = Number(vm.exerciseSummary.total_burned) - Number(exercise_obj.calories_burned);
                        vm.exerciseSummary.total_remaining = Number(vm.exerciseSummary.total_remaining) + Number(exercise_obj.calories_burned);
                        vm.exerciseSummary.calorie_burned_percentage = Math.round( (Number(vm.exerciseSummary.total_burned) / Number(vm.exerciseSummary.calorie_burned_goal)) * 100 );
                        // end grand total
                      
                       
                      if (uiCalendarConfig.calendars['exerciseCalendar']) {
                        uiCalendarConfig.calendars['exerciseCalendar'].fullCalendar('removeEvents');
                        vm.monthlyCalorieSummary();
                      } 

                       showFlashMessage('Delete','Exercise deleted successfully','success');
                    });
                }
      });

    }

  
    
}]);