
angular.module('app.core').controller('MedicationsController', ['ShowService','StoreService','$scope', function(ShowService,StoreService,$scope){
    var vm = this;   
    vm.userId = $scope.userDetails.userId;
    vm.routine_times = [];
    vm.user_medicine_list = [];
    vm.new_medicine=null;

       
       ShowService.get('api/v3/patient/routine_times',vm.userId).then(function(results){
        vm.routine_times =  results.response
       });

    //getting routine time for Take column
    //get user medicines list
    /*ShowService.get('api/v3/patient/user_medicine_list',vm.userId).then(function(results){
       vm.user_medicine_list = results.response;        
    });*/

    //save medicine into database
    vm.saveMedicine = function(){	
    	var data = {};
    	data.new_medicine = vm.new_medicine;
    	data.new_medicine.user_id = vm.userId;
    	data.new_medicine.medicine_id = data.new_medicine.id;

    	StoreService.save('api/v3/patient/medicines',data).then(function(result) {
        if(result.error == false){
          vm.new_medicine.display_medicine_name = vm.new_medicine.display_name;
          vm.user_medicine_list.push(vm.new_medicine);
          showFlashMessage('Save','Medicine saved successfully','success');
          vm.new_medicine = null;
        }
      },function(error) {
          console.log(error);
        });
    }

    //delete medicine for user
    vm.deleteMedicine = function(medicine) {

        var user_id = medicine.user_id;
        var medicine_id = medicine.medicine_id;
        var msg = "Are you sure want to delete "+ medicine.display_medicine_name;
        showFlashMessageConfirmation("Delete",msg,"info",function(result) {
            if (result) {
                ShowService.remove('api/v3/patient/user_medicine_delete/'+user_id,medicine_id).then(function(results){
                   if(results.error == false){
                      var index = vm.user_medicine_list.indexOf(medicine);
                      vm.user_medicine_list.splice(index, 1);
                      showFlashMessage('Delete','Medicine deleted successfully','success');
                   }
                });
            }
        }); 
    }

    //calculate days from dispense and refill
    vm.calculateDays = function(){

      if(vm.new_medicine){
     
        var days = 0;
        var frequency_time = 0;
        var dispense = 0;
        var refill = 0;
        try{
              frequency_time = parseInt(vm.new_medicine.frequency_times);
           }catch(error){}
        try{
            dispense = parseInt(vm.new_medicine.config.dispense);
           }catch(error){}          
        try{
             refill =    parseInt(vm.new_medicine.config.refills); 
           }  catch(error){}
         
        if(frequency_time){
          if(dispense){
              days = parseInt(dispense/frequency_time);
              if(refill){
                  days = (refill+1)*dispense/frequency_time;
              }
          }
        }
        vm.new_medicine.days = days ;
      }
    }

    //get medicine fullName with genericName
    vm.getMedicationFullHtmlName = function(medicine){
      var genericName = '';
      if(!!medicine.generic_name){
        genericName = '['+ medicine.generic_name + ']';
      }

      return '<b class=&quot;text-primary&quot;>'+medicine.name + genericName +'</b>'
    }

}]);



