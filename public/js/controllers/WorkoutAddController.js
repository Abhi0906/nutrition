angular.module('app.core').controller('WorkoutAddController',['ShowService','StoreService','$scope','$rootScope','$stateParams','$state',function(ShowService,StoreService,$scope,$rootScope,$stateParams,$state){
	  var vm = this;
	  vm.workoutId =  $stateParams.workoutId;
	  vm.selectedWorkout = null;
	  vm.showAddWorkout =false;

	  vm.saveWorkout = function(isValid){
			if (isValid) {
		        var data = vm.selectedWorkout;
		        if(data != null && typeof data.id != "undefined") {
		          delete(data.created_at);
		          delete(data.updated_at);
		          delete(data.deleted_at);
		          StoreService.update('api/v2/admin/workout',data.id,data).then(function(results){
		               if(results.error == false){
		                 showFlashMessage('Update','Workout updated successfully!','success');
		                 $state.go('list-workouts');
		               }
		           },
		           function(error) {
		              console.log(error);
		           });
		        }else{
		          StoreService.save('api/v2/admin/workout',data).then(function(results){
		            if(results.error == false){
		                vm.selectedWorkout = results.response;
		                vm.selectedWorkout.videos =[];
		                vm.selectedWorkout.video_count = 0;
		                //vm.workouts.push(vm.selectedWorkout);
		                showFlashMessage('Save','Workout added successfully!','success');
		                $state.go('list-workouts');
		            }
		          },
		          function(error) {
		            console.log(error);
		          });
		        }
	     	}
	  }
}]);