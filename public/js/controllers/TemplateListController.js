angular.module('app.core').controller('TemplateListController',
  ['ShowService','StoreService','$scope','$rootScope','$state',
  function(ShowService,StoreService,$scope,$rootScope,$state){
	  var vm = this;
    vm.showAddTemplate =false;
    $scope.keyword = "";
    vm.isloading = true;

    //fire on click event of 'Add template'
    vm.addTemplate = function(){
      vm.selectedTemplate = null;
    }

    // Save/Edit template into database.
    vm.saveTemplate = function(){
      var data = vm.selectedTemplate;
      vm.keyword = "";

      if(typeof data.id != 'undefined'){
        //edit template
        StoreService.update('api/v2/admin/template',data.id,data).then(function(results){console.log(results);
            if(results.error == false){
               showFlashMessage('Update','Template updated successfully','success');
            }
        },
        function(error) {
            console.log(error);
        });
      }
      else{

        StoreService.save('api/v2/admin/template',data).then(function(results){
          if(results.error == false){
              vm.selectedTemplate = results.response;
              vm.selectedTemplate.workouts =[];
              vm.selectedTemplate.workout_count=0;
              vm.selectedTemplate.video_count=0;
              vm.selectedTemplate.exercise_count=0;
              vm.templates.push(vm.selectedTemplate);
              showFlashMessage('Save','Template added successfully','success');
          }
        },
        function(error) {
            console.log(error);
        });
      }
    }

    //clear find template fields.
    vm.clearFiler = function(){
      $scope.keyword = "";
    }

    vm.showTemplate = function(template){
      vm.selectedTemplate = template;
      vm.showAddTemplate = false;
    }


     //delete template
    vm.deleteTemplate = function(template){

      var template_id = template.id;
      var msg = "Are you sure want to delete this template ?";
      showFlashMessageConfirmation("Delete",msg,"info",function(result) {
        if (result) {
          ShowService.remove('api/v2/admin/template',template_id).then(function(results){
             if(results.error == false){
                var index = vm.templates.indexOf(template);
                vm.templates.splice(index, 1);
                showFlashMessage('Delete','Template deleted successfully','success');
             }
          });
        }
      });
    }

    vm.searchTemplate = function(){

      if($scope.keyword != "" && typeof $scope.keyword != "undefined"){
        var params ={'keyword':$scope.keyword };
        ShowService.search('api/v2/admin/template/list',params,1).then(function(results){
          vm.templates = results.response;
          vm.isloading = false;
        });
      }
      else{
        ShowService.query('api/v2/admin/template/list').then(function(results){
          vm.templates = results.response;
          vm.isloading = false;
        });
      }
    }

    var watchArray = ['keyword'];
    $scope.$watchGroup(watchArray, function(newVal, oldVal) {
        vm.searchTemplate();
    });


    //edit template.desplaying template with details and workouts.
    vm.editTemplate = function(template, type='detail'){

      vm.selectedTemplate = template;
      var result = { templateId:template.id, type: type};
      $state.go('edit-workout-plans', result);
    }




}]);