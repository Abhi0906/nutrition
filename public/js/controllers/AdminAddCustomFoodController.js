angular.module('app.core').controller('AdminAddCustomFoodController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){
    var vm = this;
    
    vm.custom_food = {};
    
    vm.log_newfoodloader = false;    

    // set custom food object values
    vm.custom_food.sodium = 0;
    vm.custom_food.total_fat = 0;
    vm.custom_food.potassium = 0;
    vm.custom_food.saturated_fat = 0;
    vm.custom_food.total_carbs = 0;
    vm.custom_food.polyunsaturated = 0;
    vm.custom_food.dietary_fiber = 0;
    vm.custom_food.monounsaturated = 0;
    vm.custom_food.sugar = 0;
    vm.custom_food.protein = 0;
    vm.custom_food.trans_fat = 0;
    vm.custom_food.cholesterol = 0;
    vm.custom_food.vitamin_a = 0;
    vm.custom_food.calcium = 0;
    vm.custom_food.vitamin_c = 0;
    vm.custom_food.vitamin_c = 0;
    vm.custom_food.iron = 0;
    vm.custom_food.serving_quantity = 0;
    vm.custom_food.serving_size_unit = '';
    vm.custom_food.source_name="custom";
    // end set custom food object values
  
    vm.add_new_food = function(){
       vm.log_newfoodloader = true;

       var custom_food_data = {

            "user_agent" : 'server',
            "brand_name" : vm.custom_food.brand_name,
            "name" : vm.custom_food.food_name,
            "brand_with_name" : vm.custom_food.brand_name + " - " + vm.custom_food.food_name,
            "source_name" : vm.custom_food.source_name,
            "nutrition_data" : {
                "calories": vm.custom_food.calories,
                "sodium": vm.custom_food.sodium + " mg",
                "total_fat": vm.custom_food.total_fat + " g",
                "potassium": vm.custom_food.potassium + " mg",
                "saturated_fat": vm.custom_food.saturated_fat + " g",
                "total_carb": vm.custom_food.total_carbs + " g",
                "polyunsaturated": vm.custom_food.polyunsaturated + " g",
                "dietary_fiber": vm.custom_food.dietary_fiber + " g",
                "monounsaturated": vm.custom_food.monounsaturated + " g",
                "sugars": vm.custom_food.sugar + " g",
                "protein": vm.custom_food.protein + " g",
                "trans_fat": vm.custom_food.trans_fat + " g",
                "cholesterol": vm.custom_food.cholesterol + " mg",
                "vitamin_a": vm.custom_food.vitamin_a + "%",
                "calcium_dv": vm.custom_food.calcium + "%",
                "vitamin_c": vm.custom_food.vitamin_c + "%",
                "iron_dv": vm.custom_food.iron + "%",
                "serving_quantity": vm.custom_food.serving_quantity,
                "serving_size_unit": vm.custom_food.serving_size_unit,
            },
            "serving_data" : [{
                "label": vm.custom_food.serving_quantity + ' ' + vm.custom_food.serving_size_unit,
                "unit": vm.custom_food.serving_size_unit
            }]
       };
        
        StoreService.save('api/v2/admin/add_custom_food', custom_food_data).then(function(result) {
        
            if(result.error == false) {
                 vm.custom_food = {};
                showFlashMessage('Added', 'Custom Food Added successfully', 'success');
                $state.go('food-diary');
            }
            vm.log_newfoodloader = false;

        },function(error) {
            console.log(error);
          });

        return false;
    }
}]);