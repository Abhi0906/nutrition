angular.module('app.core').controller('UserRoutineDetailController',['ShowService','StoreService','$scope','$rootScope','$state','$stateParams' ,function(ShowService,StoreService,$scope,$rootScope,$state,$stateParams){
    var vm = this;
    $('.back-top').trigger('click');
    vm.templateId =  $stateParams.id;
    
    vm.getTemplateWorkouts = function(){
    	ShowService.get('api/v3/template', vm.templateId).then(function(results){
          
	      	vm.selectedTemplate = results.response.template;
	      	vm.assignWorkouts = results.response.workouts;
	        
	    });
    }
    
}]);