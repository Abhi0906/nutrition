angular.module('app.core').controller('ExerciseImageEditController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){
    var vm = this;
    vm.exerciseId =  $stateParams.id;
    $('.back-top').trigger('click');

	vm.steps = [];
    ShowService.get('api/v2/admin/exercise_image',vm.exerciseId).then(function(results){
            vm.selected = results.response;
            var exercise_steps = results.response.steps;
            angular.forEach(exercise_steps,function(value){
            	var temp ={};
            	 temp.desc = value;
            	 vm.steps.push(temp);
            });
            console.log(vm.steps);
    });
	vm.addNewStep = function() {
		var newItemNo = vm.steps.length+1;
		vm.steps.push({'id':'step-'+newItemNo});
	};

	vm.removeStep = function() {
		var lastItem = vm.steps.length-1;
		vm.steps.splice(lastItem);
	};

	vm.updateExerciseImage = function(){

		var exercise_data ={};
		exercise_data.name =vm.selected.name;
        exercise_data.exercise_type =vm.selected.exercise_type;
		exercise_data.exercise_time =vm.selected.exercise_time;
		exercise_data.calorie_burn =vm.selected.calorie_burn;
		exercise_data.experience =vm.selected.experience;
		exercise_data.body_part =vm.selected.body_part;
		exercise_data.equipment =vm.selected.equipment;
		exercise_data.description =vm.selected.description;
		exercise_data.exercise_steps =vm.steps;
		exercise_data.image_name = vm.selected.image_name;
	//	console.log(exercise_data); return false;
        StoreService.update('api/v2/admin/exercise_image',vm.exerciseId,exercise_data).then(function(result) {
          if(result.error == false){
            $state.go('list-exercise-images');

          }
        },function(error) {
          console.log(error);
        });
	}
	vm.deleteExercise = function(exercise){
		var id = exercise;
        var msg = "Are you sure want to delete this exercise image ?";
        showFlashMessageConfirmation("Delete",msg,"info",function(result) {
                if (result) {
                    ShowService.remove('api/v2/admin/exercise_image',id).then(function(results){
                       if(results.error == false){
                        $state.go('list-exercise-images');
                        showFlashMessage('Delete','Exercise Image delete successfully','success');
                       }

                    });

                }
        });

    }
}]);
