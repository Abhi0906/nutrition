angular.module('app.core').controller('DealController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){
    var vm = this;
	vm.deallist = {};

    vm.getDealList = function(){

        ShowService.query('api/v2/admin/deal/getlist').then(function(result){
            vm.deallist = result.response;
        });
    }

    vm.deleteDeal = function(deal_obj){
		var id = deal_obj.id;
		var msg = "Are you sure want to delete this deal ?";

		showFlashMessageConfirmation("Delete",msg,"info",function(result) {
		      if (result) {
		          ShowService.remove('api/v2/admin/deal/delete', id).then(function(results){

		                var index = vm.deallist.indexOf(deal_obj);

		                vm.deallist.splice(index, 1);

		               showFlashMessage('Delete','Deal deleted successfully','success');
		            });
		        }
		});
    }

}]);