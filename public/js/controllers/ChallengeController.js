angular.module('app.routes').config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/list-challenge');

    $stateProvider

        .state('list-challenge', {
            url: '/list-challenge',
            templateUrl: '/admin/load_challenge',
            controller : 'ChallengeController',
            controllerAs : 'list_challenge'
        })
        .state('add-challenge', {
            url: '/add-challenge',
            templateUrl: '/admin/load_add_challenge',
            controller : 'ChallengeAddController',
            controllerAs : 'add_challenge'
        })
        .state('edit-challenge', {
            url: '/edit-challenge/:id',
            templateUrl: '/admin/load_edit_challenge',
            controller : 'ChallengeEditController',
            controllerAs : 'edit_challenge'

        });
});
angular.module('app.core').controller('ChallengeController',['ShowService','$scope',function(ShowService,$scope){
    var vm = this;
    vm.allowedDeviceList = function(){
        ShowService.search('api/v2/admin/allowed_device/allowedevicelist').then(function(results){
             vm.allowed = results.response;
        });
    }

    vm.challengeList = function(){
                 ShowService.search('api/v2/admin/challenge/challengelist').then(function(results){
                    vm.get_challenge = results.response;
                });
    }

    vm.deleteChallenged = function(list_obj){
        var id = list_obj.id;
        var msg = "Are you sure want to delete this Challenge ?";
        showFlashMessageConfirmation("Delete", msg, "info", function(result) {
              if (result) {
                ShowService.remove('api/v2/admin/challenge/delete', id).then(function(result){
                    if(result.error==false){
                   var index = vm.get_challenge.indexOf(list_obj);
                   vm.get_challenge.splice(index, 1);
                    showFlashMessage('Delete','Challenge deleted successfully','success');
                  }
                });
            }
        });
    }
}]);