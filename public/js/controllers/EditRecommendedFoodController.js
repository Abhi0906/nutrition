angular.module('app.core').controller('EditRecommendedFoodController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', 'utility', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll, utility){
    var vm = this;
    $('#to-top').trigger('click');

    vm.edit_from = $stateParams.edit_from; // md_meal or md_food

    vm.recommended_type_id = $stateParams.recommended_type_id;

    vm.recommended_food_id = $stateParams.recommended_food_id;


    vm.food_data =  $stateParams.food_data;
    if(vm.edit_from == 'md_meal'){
        var nutrition_id = vm.food_data.recommended_food_id;
        var update_id = vm.food_data.recommended_meal_food_prim_id;
        vm.recommended_meal_id = $stateParams.recommended_meal_id;
        vm.meal_name = $stateParams.recommended_meal_name;
        vm.del_btn_display = false;

    }
    else{
        var nutrition_id = vm.food_data.nutrition_id;
        var update_id = vm.food_data.recommended_food_id;
        vm.del_btn_display = true;
    }

    vm.backToItemPage = function(){
        if(vm.edit_from == 'md_meal'){
            $state.go('meal-items', {recommended_meal_id: vm.recommended_meal_id, recommended_meal_name: vm.meal_name});
        }
        else{
            $state.go('foods');
        }

    }

    vm.deleteRecommendedFood = function(){
    var id = vm.recommended_food_id;
      var msg = "Are you sure want to delete this food ?";
      showFlashMessageConfirmation("Delete",msg,"info",function(result) {
              if (result) {
                  ShowService.remove('api/v2/admin/recommended_food/delete', id).then(function(results){
                         showFlashMessage('Delete','Food deleted successfully','success');
                          $state.go('foods');
                    });
                }
      });

    }


    vm.food_for_names = [ "Any", "Male", "Female"];

    vm.nutrition_food = [];

    vm.getRecommendedFood = function(){
        vm.edit_food_loader = true;
        if(vm.food_data){

	    	ShowService.get('api/v2/admin/recommended_food/edit', nutrition_id).then(function(results){

	    		vm.nutrition_food = results.response;

                vm.edit_food_loader = false;
	    	});

    	}


    }

    vm.getFoodRecommendedTypes = function(){

        vm.serving_ranges = utility.getServingRange(1,10,0.5);
        console.log(vm.serving_ranges);

        if(vm.edit_from == 'md_meal'){
            ShowService.query('api/v2/admin/recommended_meal/items').then(function(result){
                console.log("result");
                console.log(result);
                vm.recommended_types = result.response;
            });
        }
        else{
            ShowService.query('api/v2/admin/recommended_food/types').then(function(result){
                vm.recommended_types = result.response;
            });
        }
    }

    vm.calculateServingSize = function(nutrition_obj){

        var no_of_serving =  vm.recommended_food.no_of_servings;
        var serving_range =  vm.recommended_food.serving_range;
        var serving_label = vm.recommended_food.serving_string;
        var serving_unit = nutrition_obj.nutrition_data.serving_size_unit;
        var serving_quantity = nutrition_obj.nutrition_data.serving_quantity;

        var serving_value = serving_label.replace(serving_unit,'');
        var serving_size = utility.recursiveSearch(serving_value);

        if(nutrition_obj.hasOwnProperty("nutrition_data")){
            var nutrition_data = nutrition_obj.nutrition_data;
        }else{
            var nutrition_data = nutrition_obj.nutritions.nutrition_data;
        }

        var update_calories = utility.calculateFoodCalories(no_of_serving,nutrition_data,serving_size,serving_quantity);

        vm.recommended_food.calories =serving_range * update_calories.toFixed(2);

    }

    vm.updateMdRecommendedFood = function(nutrition_obj){

        var calories = vm.recommended_food.calories;

        var serving_string = vm.recommended_food.serving_string;

        var serving_quantity = utility.recursiveSearch(serving_string);

        var serving_size_unit = nutrition_obj.nutrition_data.serving_size_unit;

        var no_of_servings = vm.recommended_food.no_of_servings;
        var recommended_type_id = vm.recommended_food.food_recommendation_type_id;
        var serving_range = vm.recommended_food.serving_range;

        if(vm.edit_from == 'md_meal'){

            var params = {

                "item_id": recommended_type_id,
                "calories": calories,
                "serving_quantity": serving_quantity,
                "serving_size_unit": serving_size_unit,
                "no_of_servings": no_of_servings,
                "serving_string": serving_string,
                "serving_range": serving_range,

            }

            StoreService.update('api/v2/admin/recommended_meal/update_meal_food', update_id, params).then(function(result){
                vm.backToItemPage();
            });
        }
        else{

            var food_for = vm.recommended_food.food_for;
            var params = {

                "food_recommendation_type_id": recommended_type_id,
                "calories": calories,
                "serving_quantity": serving_quantity,
                "serving_size_unit": serving_size_unit,
                "no_of_servings": no_of_servings,
                "serving_string": serving_string,
                "food_for": food_for
            }

            StoreService.update('api/v2/admin/recommended_food/update', update_id, params).then(function(result){
                vm.backToItemPage();
            });
        }

    }

    vm.updateCaloriesRange = function(nutrition_obj){

        var no_of_serving =  vm.recommended_food.no_of_servings;
        var serving_range =  vm.recommended_food.serving_range;
        var default_calories = nutrition_obj.nutrition_data.calories;
        var update_calories = Number(default_calories)*Number(no_of_serving)*Number(serving_range);

        vm.recommended_food.calories =update_calories.toFixed(2);


    }

    vm.updateDefaultServing = function(nutrition_obj){
        // console.log(nutrition_obj);

        var food_for =  vm.recommended_food.food_for;

        switch (food_for) {
            case 'Any':
                vm.recommended_food.no_of_servings=1;
                break;
            case 'Male':
                vm.recommended_food.no_of_servings=5;
                break;
            case 'Female':
                vm.recommended_food.no_of_servings=3;
                break;

        }
        var no_of_serving =  vm.recommended_food.no_of_servings;
        var serving_range =  vm.recommended_food.serving_range;
        var default_calories = nutrition_obj.nutrition_data.calories;
        //console.log(no_of_serving);
        //console.log(serving_range);
        // console.log(default_calories);


        var update_calories = Number(default_calories)*Number(no_of_serving)*Number(serving_range);

        vm.recommended_food.calories =update_calories.toFixed(2);




    }

    vm.viewFoodDetail = function(){

        vm.food_detail =vm.updateNutritionFacts(vm.nutrition_food); vm.nutrition_food;
    	$('#view_food_detail_tmplt').modal('show');
    }

    vm.updateNutritionFacts = function(nutrition_obj){

        var copy_nutrition_obj = angular.copy(nutrition_obj);

        var no_of_serving =  vm.recommended_food.no_of_servings;
        var serving_range =  vm.recommended_food.serving_range;


        var default_protein = copy_nutrition_obj.nutrition_data.protein;
        var default_calories = copy_nutrition_obj.nutrition_data.calories;
        var default_calcium_dv = copy_nutrition_obj.nutrition_data.calcium_dv;
        var default_cholesterol = copy_nutrition_obj.nutrition_data.cholesterol;
        var default_dietary_fiber = copy_nutrition_obj.nutrition_data.dietary_fiber;
        var default_iron_dv = copy_nutrition_obj.nutrition_data.iron_dv;
        var default_monounsaturated = copy_nutrition_obj.nutrition_data.monounsaturated;
        var default_polyunsaturated = copy_nutrition_obj.nutrition_data.polyunsaturated;
        var default_potassium = copy_nutrition_obj.nutrition_data.potassium;
        var default_saturated_fat = copy_nutrition_obj.nutrition_data.saturated_fat;
        var default_sodium = copy_nutrition_obj.nutrition_data.sodium;
        var default_sugars = copy_nutrition_obj.nutrition_data.sugars;
        var default_total_carb = copy_nutrition_obj.nutrition_data.total_carb;
        var default_total_fat = copy_nutrition_obj.nutrition_data.total_fat;
        var default_trans_fat = copy_nutrition_obj.nutrition_data.trans_fat;
        var default_vitamin_a = copy_nutrition_obj.nutrition_data.vitamin_a;
        var default_vitamin_c = copy_nutrition_obj.nutrition_data.vitamin_c;


        var update_protine = parseFloat(default_protein)*Number(no_of_serving)*Number(serving_range);
        var update_calories = Number(default_calories)*Number(no_of_serving)*Number(serving_range);
        var update_calcium_dv = parseFloat(default_calcium_dv)*Number(no_of_serving)*Number(serving_range);
        var update_cholesterol = parseFloat(default_cholesterol)*Number(no_of_serving)*Number(serving_range);
        var update_dietary_fiber = parseFloat(default_dietary_fiber)*Number(no_of_serving)*Number(serving_range);
        var update_iron_dv = parseFloat(default_iron_dv)*Number(no_of_serving)*Number(serving_range);
        var update_monounsaturated = parseFloat(default_monounsaturated)*Number(no_of_serving)*Number(serving_range);
        var update_polyunsaturated = parseFloat(default_polyunsaturated)*Number(no_of_serving)*Number(serving_range);
        var update_potassium = parseFloat(default_potassium)*Number(no_of_serving)*Number(serving_range);
        var update_saturated_fat = parseFloat(default_saturated_fat)*Number(no_of_serving)*Number(serving_range);
        var update_sodium = parseFloat(default_sodium)*Number(no_of_serving)*Number(serving_range);
        var update_sugars = parseFloat(default_sugars)*Number(no_of_serving)*Number(serving_range);
        var update_total_carb = parseFloat(default_total_carb)*Number(no_of_serving)*Number(serving_range);
        var update_total_fat = parseFloat(default_total_fat)*Number(no_of_serving)*Number(serving_range);
        var update_trans_fat = parseFloat(default_trans_fat)*Number(no_of_serving)*Number(serving_range);
        var update_vitamin_a = parseFloat(default_vitamin_a)*Number(no_of_serving)*Number(serving_range);
        var update_vitamin_c = parseFloat(default_vitamin_c)*Number(no_of_serving)*Number(serving_range);




        copy_nutrition_obj.nutrition_data.calories = update_calories.toFixed(2);
        copy_nutrition_obj.nutrition_data.protein = update_protine +" g";
        copy_nutrition_obj.nutrition_data.calcium_dv = update_calcium_dv +"%";
        copy_nutrition_obj.nutrition_data.cholesterol = update_cholesterol +" mg";
        copy_nutrition_obj.nutrition_data.dietary_fiber = update_dietary_fiber +" g";
        copy_nutrition_obj.nutrition_data.iron_dv = update_iron_dv +"%";
        copy_nutrition_obj.nutrition_data.monounsaturated = update_monounsaturated +" g";
        copy_nutrition_obj.nutrition_data.polyunsaturated = update_polyunsaturated +" g";
        copy_nutrition_obj.nutrition_data.potassium = update_potassium +" mg";
        copy_nutrition_obj.nutrition_data.saturated_fat = update_saturated_fat +" g";
        copy_nutrition_obj.nutrition_data.sodium = update_sodium +" mg";
        copy_nutrition_obj.nutrition_data.sugars = update_sugars +" g";
        copy_nutrition_obj.nutrition_data.total_carb = update_total_carb +" g";
        copy_nutrition_obj.nutrition_data.total_fat = update_total_fat +" g";
        copy_nutrition_obj.nutrition_data.trans_fat = update_trans_fat +" g";
        copy_nutrition_obj.nutrition_data.vitamin_a = update_vitamin_a +"%";
        copy_nutrition_obj.nutrition_data.vitamin_c = update_vitamin_c +"%";


        return copy_nutrition_obj;

    }

}]);