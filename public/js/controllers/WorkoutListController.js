angular.module('app.core').controller('WorkoutListController',['ShowService','StoreService','$scope','$rootScope','$state',function(ShowService,StoreService,$scope,$rootScope,$state){
	  	var vm = this;
		vm.selectedWorkout = null;
		vm.showAddWorkout =false;
	    vm.isloading = true;
		vm.blockTitleHeading = 'FIND WORKOUTS';
		var watchArray = ['keyword','experience','body_part'];

        $scope.$watchGroup(watchArray, function(newVal, oldVal) {
            vm.searchWorkout();
        });

	    vm.searchWorkout = function(){

	      //getting all master workouts with associate videos.

	      if(typeof $scope.keyword != "undefined" ||
	         typeof $scope.experience != "undefined" ||
	         typeof $scope.body_part != "undefined"
	        ){
	              var params ={'keyword':$scope.keyword,
	                          'experience':$scope.experience,
	                          'body_part':$scope.body_part
	                        };
	             ShowService.search('api/v2/admin/workout/list',params,1).then(function(results){
	                         vm.workouts = results.response;
	                          vm.isloading = false;
	                  });

	          }else{

	              ShowService.query('api/v2/admin/workout/list').then(function(results){
	                    vm.workouts = results.response;
	                     vm.isloading = false;
	               });
	          }

	    }

/*
		vm.saveWorkout = function(isValid){
			if (isValid) {
		        var data = vm.selectedWorkout;
		        if(data != null && typeof data.id != "undefined") {
		          delete(data.created_at);
		          delete(data.updated_at);
		          delete(data.deleted_at);
		          StoreService.update('api/v3/workout',data.id,data).then(function(results){
		               if(results.error == false){
		                 showFlashMessage('Update','Workout updated successfully!','success');
		               }
		           },
		           function(error) {
		              console.log(error);
		           });
		        }else{
		          StoreService.save('api/v3/workout',data).then(function(results){
		            if(results.error == false){
		                vm.selectedWorkout = results.response;
		                vm.selectedWorkout.videos =[];
		                vm.selectedWorkout.video_count = 0;
		                vm.workouts.push(vm.selectedWorkout);
		                showFlashMessage('Save','Workout added successfully!','success');
		            }
		          },
		          function(error) {
		            console.log(error);
		          });
		        }
	     	}
	    } */

	    vm.showWorkout = function(workout){

	        vm.selectedWorkout = workout;
	        vm.showAddWorkout = false;
	    }

	    vm.deleteWorkout = function(workout){


	        var workout_id = workout.id;
	        var msg = "Are you sure want to delete this workout ?";
	        showFlashMessageConfirmation("Delete",msg,"info",function(result) {
	            if (result) {
	                ShowService.remove('api/v2/admin/workout',workout_id).then(function(results){
	                   if(results.error == false){
	                      var index = vm.workouts.indexOf(workout);
	                      vm.workouts.splice(index, 1);
	                      showFlashMessage('Delete','Workout deleted successfully!','success');
	                   }
	                });
	            }
	        });
	    }

	    vm.clearFiler = function(){

	      $scope.keyword = "";
	      $scope.experience = "undefined";
	      $scope.body_part = "undefined";
	    }

	    vm.deleteVideo = function(video,workout_id){

	       var video_id = video.id;

	        var msg = "Are you sure want to delete this video ?";
	        showFlashMessageConfirmation("Delete",msg,"info",function(result) {
	            if (result) {
	                ShowService.remove('api/v2/admin/workout/delete_video/'+workout_id,video_id).then(function(results){
	                   if(results.error == false){
	                      var index =  vm.selectedWorkout.videos.indexOf(video);
	                      vm.selectedWorkout.videos.splice(index, 1);
	                       vm.selectedWorkout.video_count = parseInt(vm.selectedWorkout.video_count)-1;
	                      showFlashMessage('Delete','Video deleted successfully!','success');
	                   }
	                });
	            }
	        });
	    }



	    //desplaying workout with details and videos.
    	vm.editWorkout = function(workout, type='detail'){


			vm.selectedWorkout = workout;

			var result = { workoutId:workout.id, type: type};
			$state.go('edit-workout', result);

		}

		vm.deleteExercise = function(workout,exercise,workout_id){


           if(exercise.workout_type == 'exercise_images'){
             var msg = "Are you sure want to delete this Exercise Image ?";
           }else if(exercise.workout_type == 'videos'){
             var msg = "Are you sure want to delete this video ?";
           }

	          showFlashMessageConfirmation("Delete",msg,"info",function(result) {
	                    if (result) {

	                    	var index =  workout.exercise_list.indexOf(exercise);
                            workout.exercise_list.splice(index, 1);

	                        var data = {'workout_id':workout_id,'exrecise_list':workout.exercise_list};

	                         StoreService.save('api/v2/admin/workout/attached_exercise',data).then(function(result) {
	                           if(exercise.workout_type == 'exercise_images'){
	                             var exercise_time = exercise.exercise_time;
	                             workout.exercise_image_count = parseInt(workout.exercise_image_count)-1;
	                             workout.total_mins = parseInt(workout.total_mins)-parseInt(exercise_time);
	                           }

	                           if(exercise.workout_type == 'videos'){
	                            var videos_time = exercise.workout_time;
	                            workout.video_count = parseInt(workout.video_count)-1;
	                            workout.total_mins = parseInt(workout.total_mins)-parseInt(videos_time);
	                           }
	                            showFlashMessage('Delete','Exercise deleted successfully!','success');

	                        }, function(error) {
	                          console.log(error);
	                        });
	                    }
	          });

        }


}]);