angular.module('app.core').controller('UserWorkoutController',['ShowService','StoreService','$scope','$rootScope','$state',function(ShowService,StoreService,$scope,$rootScope,$state){
    var vm = this;
    vm.userId = $scope.user_workout_index.userId;
    vm.myRoutine = {};

    vm.week = [
      {dayNumber: '0', dayName: 'SUN'},
      {dayNumber: '1', dayName: 'MON'},
      {dayNumber: '2', dayName: 'TUE'},
      {dayNumber: '3', dayName: 'WED'},
      {dayNumber: '4', dayName: 'THU'},
      {dayNumber: '5', dayName: 'FRI'},
      {dayNumber: '6', dayName: 'SAT'},
    ];

    vm.sel_day_counter = 0;
    vm.checkWeekSelected = function(){
      vm.sel_day_counter = 0;
      angular.forEach(vm.myRoutine.days, function(values, key){
        if(values==true){
          vm.sel_day_counter++;
        }
      });
    }
      
    var d = new Date();
    vm.format = 'MM-dd-yyyy';
    vm.start_date = new Date();

    vm.start_date_open = function() {
        vm.start_date_popup.opened = true;
    };
    vm.start_date_popup = {
      opened: false
    };

    vm.end_date = new Date();

    vm.end_date_open = function() {
        vm.end_date_popup.opened = true;
    };

    vm.end_date_popup = {
      opened: false
    };

    var time= new Date();
    time.setHours(10);
    time.setMinutes(0);
    vm.time_total = time; 

    // Routine start/end date
    vm.routine_start_date_open = function() {
        vm.routine_start_date_popup.opened = true;
    };
    vm.routine_start_date_popup = {
      opened: false
    };

    // end: Routine start/end date

    vm.getMdRecommendedWorkoutDetails = function(workout){

      vm.selectedWorkout = workout;
    }

    vm.setMdRecommendedRoutineDetails = function(routine){

      vm.my_routine_start_date = new Date();
      vm.my_routine_end_date = moment(vm.my_routine_start_date).add(30, 'days').calendar();

      vm.selectedRoutine = routine;
    }

    vm.setEndDate = function(){

      var end_date = moment(vm.my_routine_start_date).add(parseInt(vm.selectedRoutine.days), 'days').calendar();

      vm.my_routine_end_date = end_date;
    }
    
    vm.addToMyRoutine = function(){

      var myRoutineData = {};

      myRoutineData.user_id = vm.userId;
      myRoutineData.title = vm.selectedWorkout.title;
      myRoutineData.description = vm.selectedWorkout.description;
      myRoutineData.experience = vm.selectedWorkout.experience;
      myRoutineData.body_part = vm.selectedWorkout.body_part;
      myRoutineData.start = moment(vm.myRoutine.start_date).format('YYYY-MM-DD');
      myRoutineData.end = moment(vm.myRoutine.end_date).format('YYYY-MM-DD');

      var time_in_decimal = moment.duration(vm.myRoutine.time).asHours().toFixed(2);
  
      var myRoutineConfig = [];
      angular.forEach(vm.myRoutine.days, function(value, key){
        myRoutineConfig.push({"week_day": parseInt(key), "time": time_in_decimal});
      });
      myRoutineData.config = myRoutineConfig;

      var workoutExercise = [];
      angular.forEach(vm.selectedWorkout.exercise_list, function(value, key){
        workoutExercise.push({"exercise_image_id": value.id, 
                              "video_id": "",
                              "exercise_id" : "",
                              "sort_order" : parseInt(key) + 1
                              });
      });
      myRoutineData.workout_exercise = workoutExercise;

      myRoutineData.user_agent = "server";

      StoreService.save('api/v3/workout/save_user_workout_routine', myRoutineData).then(function(result) {
          if(result.error == false){ 
            $scope.user_workout_index.getUserWorkoutRoutines();            

            $('#AddWorkoutModal').modal('hide');
            vm.selectedWorkout = {};
            vm.myRoutine = {};
            showFlashMessage('Added','Workout added successfully','success'); 
          
          }
      },function(error) {
        console.log(error);
      });

    }
    
    vm.addRoutineToMyWorkout = function(){
      var myRoutineDataObj = {};

      myRoutineDataObj.user_id = vm.userId;
      myRoutineDataObj.routine_id = vm.selectedRoutine.id;
      myRoutineDataObj.start_date = moment(vm.my_routine_start_date).format('YYYY-MM-DD');
      myRoutineDataObj.end_date = moment(vm.my_routine_end_date).format('YYYY-MM-DD');

      var myRoutineData = [];
      angular.forEach(vm.selectedRoutine.workouts, function(workout, key){
          var myWorkoutData = {};

          myWorkoutData.user_id = vm.userId;
          myWorkoutData.title = workout.title;
          myWorkoutData.description = workout.description;
          myWorkoutData.experience = workout.experience;
          myWorkoutData.body_part = workout.body_part;
          myWorkoutData.start = moment(vm.my_routine_start_date).format('YYYY-MM-DD');
          myWorkoutData.end = moment(vm.my_routine_end_date).format('YYYY-MM-DD');
      
          // form data 
          //ng-model="user_workouts.myRoutineWorkout[w_key].days[key]"
          var is_continue = false;
          angular.forEach(vm.myRoutineWorkout, function(user_workout, u_key){
            
              if(user_workout.workout_id == workout.id && is_continue == false){

                  var time_in_decimal = moment.duration(user_workout.time).asHours().toFixed(2);
      
                  var myRoutineConfig = [];
                  angular.forEach(workout.pivot.config, function(value, key){
                    myRoutineConfig.push({"week_day": parseInt(value), "time": time_in_decimal});
                  });
                  myWorkoutData.config = myRoutineConfig;
                  is_continue = true;
              }
          });
          
          //end: form data 

          var workoutExercise = [];
          angular.forEach(workout.workout_exercise, function(value, key){
            workoutExercise.push({"exercise_image_id": value.exercise_image_id, 
                                  "video_id": "",
                                  "exercise_id" : "",
                                  "sort_order" : parseInt(key) + 1
                                  });
          });
          myWorkoutData.workout_exercise = workoutExercise;
          myRoutineData.user_agent = "server";
          myRoutineData.push(myWorkoutData);
      });
      
      myRoutineDataObj.myRoutineData = myRoutineData;
      StoreService.save('api/v3/workout/save_user_routine', myRoutineDataObj).then(function(result) {
          if(result.error == false){ 
            $scope.user_workout_index.getUserWorkoutRoutines();
            $('#AddRoutineWorkoutModal').modal('hide');
            vm.selectedRoutine = {};
            vm.myRoutineDataObj = {};
            vm.myRoutineWorkout = {};
            showFlashMessage('Added','Routine added successfully','success'); 
          }
      },function(error) {
        console.log(error);
      });
      
    } 

    vm.clearAllMyWorkouts = function(){

      var msg = "Are you sure you want to clear all my workouts?";
      showFlashMessageConfirmation("Clear", msg, "info", function(result) {
        if (result) {

            var url = "api/v3/workout/clear_all_user_workouts";
            var id  = vm.userId;
            ShowService.remove(url, id).then(function(result){
              if(result.error == false){ 
                  $scope.user_workout_index.getUserWorkoutRoutines();
                  
                  showFlashMessage('Success','All workouts are cleared successfully','success'); 
                }
            });
        }
      });
    }

    vm.removeMyWorkout = function(workout_id, req_dates){

        if(req_dates == "curr_week"){
          var delete_from = "week";
        }
        else{
          var delete_from = "date";
        }
      
        var msg = "Do you want to delete by";
        
        var inviteDevInput = '<div>'+ 
                                msg +
                                '<br><br>'+
                                '<div class="radio">'+
                                  '<label><input style="display: block; margin-top: -11px; width: 5%;" type="radio" checked="checked" name="radioSelect" id="radio1"  value="update" > &nbsp;&nbsp;&nbsp;Option 1: one workout for this '+delete_from+'</label>'+
                                '</div>'+
                                '<div class="radio">'+
                                  '<label><input type="radio" style="display: block; margin-top: -12px; margin-left: -33px; width: 5%;" name="radioSelect" id="radio2" value="delete"> Option 2: all workouts of this type</label>'+
                                '</div>'+
                              '</div>';
        var selectedOption;
              swal({
                    title: "Delete Workout",
                    text: inviteDevInput,
                    html: true,
                    showCancelButton: true,
                    confirmButtonColor: "#006DCC",
                    confirmButtonText: "Ok",
                    cancelButtonText: "Close",
                    closeOnConfirm: false,
                    closeOnCancel: true
                  }, // Function that validates email address through a regular expression.
                  function () {
                    selectedOption = $("input[name=radioSelect]:checked").val();
                        if(selectedOption){
                               var dates = [];
                                if(req_dates == "curr_week"){
                                  var currentDate = new Date($scope.user_workout_index.week_start);
                                  var endDate = new Date($scope.user_workout_index.week_end);
                                  
                                  while(currentDate <= endDate) {

                                      dates.push(moment(currentDate).format('YYYY-MM-DD'));

                                      currentDate = new Date(currentDate.setDate(currentDate.getDate()+1));            
                                      
                                  }
                                }    
                                else{
                                  dates.push(moment(req_dates).format('YYYY-MM-DD'));
                                }    

                               
                                var url = "api/v3/workout/remove_myworkout";
                                var id  = vm.userId;
                                var data = { "w_id": workout_id,
                                              "opt": selectedOption,
                                              "dates": dates
                                           };

                               
                                StoreService.update(url, id, data).then(function(result){
                                  if(result.error == false){ 
                                    $scope.user_workout_index.getUserWorkoutRoutines();
                                      
                                      showFlashMessage('Success','Workouts deleted successfully','success'); 
                                    }
                                });
                        }
                        else{
                          alert("Please select any one option");
                          return false;
                        }
                    }
              );

    }

}]);