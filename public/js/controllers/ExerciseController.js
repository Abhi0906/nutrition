angular.module('app.routes').config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/exercise-diary');
    
    $stateProvider
        
        .state('exercise-diary', {
            url: '/exercise-diary',
            templateUrl: '/load_exercise_diary',
            controller : 'ExerciseDiaryController',
            controllerAs : 'exercise_diary'
        })
        .state('log_exercise', {
            url: '/log_exercise',
            templateUrl: '/log_exercise',
            params : { exercise_type : null, selected_from: null,selected_exercise : null },
            controller : 'LogExerciseController',
            controllerAs : 'add_exercise'
        });
        
});

angular.module('app.core').controller('ExerciseController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){
    var vm = this;
    vm.userId = null;
    vm.exercise_type = null;
    vm.log_date = new Date();
    vm.frequent_exercises = null;
    vm.recent_exercises = null;
  

}]);
