/**
 * Created by Plenar on 9/24/2015.
 */

angular.module('app.core').controller('UserController', ['ShowService','StoreService','$http','$cookieStore','$scope', '$location', '$anchorScroll',function(ShowService,StoreService,$http,$cookieStore,$scope, $location, $anchorScroll){
    var vm = this;
    vm.isloading = true;
    vm.cardLayout = false;
    vm.listLayout = true;
    var CookieVal = $cookieStore.get('cardLayout');
    if(CookieVal != undefined){
      if(CookieVal === 'false'){
        vm.cardLayout = false;
      }
    }

    vm.unassign_doctors = [];
    vm.assign_doctors = [];

    vm.sort = function(keyname){
        vm.sortKey = keyname;   //set the sortKey to the param passed
        vm.reverse = !vm.reverse; //if true make it false and vice versa
    }

    /*vm.getUsers = function() {
       ShowService.query('api/v3/'+vm.type+'/list').then(function(results){
            vm.users = results.response.data;
            vm.isloading = false;
        });
    }  */

    // get user with pagination
    vm.users = []; //declare an empty array
    vm.pageno = 1; // initialize page no to 1
    vm.current_page = 1;
    vm.total_count = 0;
    vm.displayRecords = [10,20,50,100];
    //vm.itemsPerPage = 1; //this could be a dynamic value from a drop down
    vm.itemPerPage = 50;

    vm.getUsers = function(pageno){ // This would fetch the data on page change.

        var old = $location.hash();
        $location.hash('innerpage');
        $anchorScroll();
        $location.hash(old);

        var search_query = "";
        if(vm.searchkey){
          var search_query = "&search_keyword="+vm.searchkey;
        }

        //In practice this should be in a factory.
        vm.users = [];
        vm.isloading = true;

        ShowService.query('api/v2/admin/'+vm.type+'/list?page='+pageno+'&itemPerPage='+vm.itemPerPage+search_query).then(function(results){

          if(results.error==false){
            //ajax request to fetch data into vm.data
            vm.searchkey = '';


            vm.users = results.response.data;  // data to be displayed on current page.
            vm.total_count = results.response.total; // total data count.
            vm.current_page = results.response.current_page;
            vm.from = results.response.from;
            vm.to = results.response.to;
          }
          vm.isloading = false;
        });
    };

    vm.pageChanged = function(){
        var pageno =  vm.current_page;
        vm.getUsers(pageno);
       }


    // end : get user with pagination

    vm.deleteUser = function(user) {

            var id = user.id;
            var msg = "Are you sure want to delete "+ vm.type;
            showFlashMessageConfirmation("Delete",msg,"info",function(result) {
                    if (result) {
                        ShowService.remove('api/v2/admin/'+vm.type,id).then(function(results){
                           if(results.error == false){
                              var index = vm.users.indexOf(user);
                              vm.users.splice(index, 1);
                              showFlashMessage('Delete',vm.type.charAt(0).toUpperCase() + vm.type.slice(1) +' deleted successfully!','success');
                           }

                        });

                    }
            });
    }

    vm.getHTMLTitle = function(user){
           if(vm.type == 'patient'){
             if(user.full_name.trim().length == 0){
                user.full_name = '&nbsp;';
             }
             return "<a href='"+vm.type+"/"+user.id+"'>"+user.full_name+"</a> ";
           }else{
             return "<a href='"+vm.type+"/"+user.id+"'>"+user.name_with_title+"</a>";

           }
    }

    vm.switchLayout = function(layout){

      if(layout === 'cardLayout'){
        vm.cardLayout = true;
        $cookieStore.put('cardLayout', 'true');
      }
      else if(layout === 'listLayout'){
        vm.cardLayout = false;
        $cookieStore.put('cardLayout', 'false');
      }
    }

    vm.loadDoctorsList = function(user) {

      return $http.get('api/v2/admin/patient/unassign_doctors/'+ user.id, { cache: false}).then(function(response) {
        vm.unassign_doctors = response.data.response;
        return vm.unassign_doctors;
      });
    };

    vm.assignedDoctor = function(doctor,patient){
      var data = {};
      data.doctor_id =  doctor.id;
      data.patient_id = patient.id;

      StoreService.save('api/v2/admin/patient/assigned_doctor',data).then(function(result) {
        if(result.error == false){
          //showFlashMessage('Assign','Doctor assigned successfully!','success');
        }
      },function(error) {
        console.log(error);
      });
    }

    vm.removeDoctor = function(doctor,patient){
      var doctor_id =  doctor.id;
      var patient_id = patient.id;

      var msg = "Are you sure want to unassign "+ doctor.name_with_title;
      showFlashMessageConfirmation("Delete",msg,"info",function(result) {
          if (result) {
              ShowService.remove('api/v2/admin/doctor/remove_assign_patient/'+doctor_id,patient_id).then(function(results){
                 if(results.error == false){
                    showFlashMessage('Unassign','Doctor Unassign successfully!','success');
                    var index = patient.assign_doctor.indexOf(doctor);
                    patient.assign_doctor.splice(index, 1);

                 }
              });
          }
      });
      return false;
    }

    vm.getDate = function(date){
      if(date==null){
        return '---';
      }
      return moment(date).format('MMMM DD, YYYY');
    }


    /*$scope.getFilter = function(){
         switch ($scope.col_field_name) {
            case 'name':
                return {full_name: $scope.search};
            case 'doctor':
                return {assign_doctor: {full_name: $scope.search}};
            case 'all':
                return {$: $scope.search};
            default:
                return {$: $scope.search};
        }
    }*/

}]);
