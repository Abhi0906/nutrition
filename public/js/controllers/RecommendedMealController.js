angular.module('app.routes').config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/template-names');
    
    $stateProvider
        
        .state('meals', {
            url: '/meals/:id',
            templateUrl: '/recommended_meals_display',
            controller : 'RecommendedMealController',
            controllerAs : 'meal'
        })
        .state('meal-items', {
            url: '/meal-items',
            templateUrl: '/meal_items_data',
            params : { recommended_meal_id : null, recommended_meal_name: null},
            controller : 'RecommendedMealItemController',
            controllerAs : 'meal_item'
        })

        .state('new-meal-items', {
            url: '/new-meal-items',
            templateUrl: '/new_meal_items_data',
            params : { new_meal_id : null, meal_template_id: null},
            controller : 'RecommendedNewMealItemController',
            controllerAs : 'new_meal_item'
        })
        .state('add-meal-food', {
            url: '/add-meal-food',
            templateUrl: '/add_recommended_meal_food',
            params : { recommended_meal_id: null, recommended_meal_name: null, item_id: null, item_name:null },
            controller : 'AddRecomMealFoodController',
            controllerAs : 'add_meal_food'
        })
        .state('edit-food', {
            url: '/edit-food',
            templateUrl: '/edit_recommended_food',
            params: {   edit_from: null, 
                        recommended_type_id : null, 
                        food_data: null, 
                        food_for_names: null, 
                        recommended_types: null,
                        recommended_meal_id: null, 
                        recommended_meal_name: null
                    },
            controller: 'EditRecommendedFoodController',
            controllerAs: 'edit_food'
        })
        .state('creat-templatename', {
            url: '/create-templatename',
            templateUrl: '/create_template_name',
            controller: 'MealTemplateNameController',
            controllerAs: 'create_templatename'
        })
        .state('template-names', {
            url: '/template-names',
            templateUrl: '/template_names',
            controller: 'MealTemplateNameController',
            controllerAs: 'template_name'
        })
        .state('edit-template', {
            url: '/edit-template/:id',
            templateUrl: '/edit_template_names',
            controller: 'MealTemplateNameController',
            controllerAs: 'create_templatename'
        });
});

angular.module('app.core').controller('RecommendedMealController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){
    var vm = this;

    vm.recommended_meals = [];
    vm.meal_item_data = [];
    vm.meal_exist = false;
    vm.new_meal_id=null;

    vm.template_id = $stateParams.id;
    vm.meal_for_names = [ "Any", "Male", "Female"];
    
    $('.back-top').trigger('click');

    

    vm.createRecommendedMeal = function(){
      
        var data = {
            "meal_name": vm.meal_name,
            "meal_template_id": vm.template_id,
            "meal_time": vm.meal_time,
            "user_agent": "server"
        };

        StoreService.save('api/v3/recommended_meal/create_meal', data).then(function(result){
            //$state.go('foods');
            if(result.error==false){
                var last_recommended_meal = result.response.last_recommended_meal;
                last_recommended_meal.calorie_range = 0;
                vm.recommended_meals.push(last_recommended_meal);
                $("#addMealModal").modal("hide");
            }
            else{
                if(result.response.meal_exist==true){
                    vm.meal_exist = true;
                }
            }
            
        });
    }

    vm.getRecommendedMeals = function(){

        ShowService.get('api/v3/recommended_meal/meals', vm.template_id).then(function(result){
               
            vm.recommended_meals = result.response;

            vm.getTemplate();
        });
    }

    vm.deleteRecommendedMeal = function(meal_obj){
        
        var id = meal_obj.id;
        var msg = "Are you sure want to delete this meal ?";
      
        showFlashMessageConfirmation("Delete", msg, "info", function(result) {
              if (result) {

                ShowService.remove('api/v3/recommended_meal/delete', id).then(function(result){
                    if(result.error==false){

                        var index = vm.recommended_meals.indexOf(meal_obj);
                        vm.recommended_meals.splice(index, 1);

                        showFlashMessage('Delete','Meal deleted successfully','success');
                    }
                    
                });

              }
        });
        
    }

    vm.getTemplateNames = function(){
        vm.template_names = [];

        ShowService.query('api/v3/recommended_meal/template_names').then(function(result){
            if(result.error == false){
                vm.template_names = result.response.template_names;
            }
            
        });
    }

    vm.getTemplate = function(){
        vm.template = [];

        ShowService.get('api/v3/recommended_meal/template', vm.template_id).then(function(result){
           
            vm.meal_template = result.response.template;
            vm.new_meal_id=result.response.new_meal_id;
            console.log(vm.new_meal_id);
        });
    }

    vm.goToCreateNewTemplateName = function(){
        $("#addMealModal").modal("hide");

        $timeout(function () {
            $state.go('creat-templatename');
        },200);
    }   
    
}])
.directive('timePicker', function() {
    return {
    restrict: 'E',
    require: 'ngModel',
    replace: true,
    link: function(scope, element, attrs) {
  
      scope.timings = [             
                { value: '', text: 'Select' },             
                { value: "00:00:00", text: "12:00 am"}, 
                // { value: "00:15:00", text: "12:15 am"}, 
                { value: "00:30:00", text: "12:30 am"}, 
                // { value: "00:45:00", text: "12:45 am"}, 
                { value: "01:00:00", text: "1:00 am"}, 
                // { value: "01:15:00", text: "1:15 am"}, 
                { value: "01:30:00", text: "1:30 am"}, 
                // { value: "01:45:00", text: "1:45 am"}, 
                { value: "02:00:00", text: "2:00 am"}, 
                // { value: "02:15:00", text: "2:15 am"}, 
                { value: "02:30:00", text: "2:30 am"}, 
                // { value: "02:45:00", text: "2:45 am"}, 
                { value: "03:00:00", text: "3:00 am"}, 
                // { value: "03:15:00", text: "3:15 am"}, 
                { value: "03:30:00", text: "3:30 am"}, 
                // { value: "03:45:00", text: "3:45 am"}, 
                { value: "04:00:00", text: "4:00 am"}, 
                // { value: "04:15:00", text: "4:15 am"}, 
                { value: "04:30:00", text: "4:30 am"}, 
                // { value: "04:45:00", text: "4:45 am"}, 
                { value: "05:00:00", text: "5:00 am"}, 
                // { value: "05:15:00", text: "5:15 am"}, 
                { value: "05:30:00", text: "5:30 am"}, 
                // { value: "05:45:00", text: "5:45 am"}, 
                { value: "06:00:00", text: "6:00 am"}, 
                // { value: "06:15:00", text: "6:15 am"}, 
                { value: "06:30:00", text: "6:30 am"}, 
                // { value: "06:45:00", text: "6:45 am"}, 
                { value: "07:00:00", text: "7:00 am"}, 
                // { value: "07:15:00", text: "7:15 am"}, 
                { value: "07:30:00", text: "7:30 am"}, 
                // { value: "07:45:00", text: "7:45 am"}, 
                { value: "08:00:00", text: "8:00 am"}, 
                // { value: "08:15:00", text: "8:15 am"}, 
                { value: "08:30:00", text: "8:30 am"}, 
                // { value: "08:45:00", text: "8:45 am"}, 
                { value: "09:00:00", text: "9:00 am"}, 
                // { value: "09:15:00", text: "9:15 am"}, 
                { value: "09:30:00", text: "9:30 am"}, 
                // { value: "09:45:00", text: "9:45 am"}, 
                { value: "10:00:00", text: "10:00 am"}, 
                // { value: "10:15:00", text: "10:15 am"}, 
                { value: "10:30:00", text: "10:30 am"}, 
                // { value: "10:45:00", text: "10:45 am"}, 
                { value: "11:00:00", text: "11:00 am"}, 
                // { value: "11:15:00", text: "11:15 am"}, 
                { value: "11:30:00", text: "11:30 am"}, 
                // { value: "11:45:00", text: "11:45 am"}, 
                { value: "12:00:00", text: "12:00 pm"}, 
                // { value: "12:15:00", text: "12:15 pm"}, 
                { value: "12:30:00", text: "12:30 pm"}, 
                // { value: "12:45:00", text: "12:45 pm"}, 
                { value: "13:00:00", text: "1:00 pm"}, 
                // { value: "13:15:00", text: "1:15 pm"}, 
                { value: "13:30:00", text: "1:30 pm"}, 
                // { value: "13:45:00", text: "1:45 pm"}, 
                { value: "14:00:00", text: "2:00 pm"}, 
                // { value: "14:15:00", text: "2:15 pm"}, 
                { value: "14:30:00", text: "2:30 pm"}, 
                // { value: "14:45:00", text: "2:45 pm"}, 
                { value: "15:00:00", text: "3:00 pm"}, 
                // { value: "15:15:00", text: "3:15 pm"}, 
                { value: "15:30:00", text: "3:30 pm"}, 
                // { value: "15:45:00", text: "3:45 pm"}, 
                { value: "16:00:00", text: "4:00 pm"}, 
                // { value: "16:15:00", text: "4:15 pm"}, 
                { value: "16:30:00", text: "4:30 pm"}, 
                // { value: "16:45:00", text: "4:45 pm"}, 
                { value: "17:00:00", text: "5:00 pm"}, 
                // { value: "17:15:00", text: "5:15 pm"}, 
                { value: "17:30:00", text: "5:30 pm"}, 
                // { value: "17:45:00", text: "5:45 pm"}, 
                { value: "18:00:00", text: "6:00 pm"}, 
                // { value: "18:15:00", text: "6:15 pm"}, 
                { value: "18:30:00", text: "6:30 pm"}, 
                // { value: "18:45:00", text: "6:45 pm"}, 
                { value: "19:00:00", text: "7:00 pm"}, 
                // { value: "19:15:00", text: "7:15 pm"}, 
                { value: "19:30:00", text: "7:30 pm"}, 
                // { value: "19:45:00", text: "7:45 pm"}, 
                { value: "20:00:00", text: "8:00 pm"}, 
                // { value: "20:15:00", text: "8:15 pm"}, 
                { value: "20:30:00", text: "8:30 pm"}, 
                // { value: "20:45:00", text: "8:45 pm"}, 
                { value: "21:00:00", text: "9:00 pm"}, 
                // { value: "21:15:00", text: "9:15 pm"}, 
                { value: "21:30:00", text: "9:30 pm"}, 
                // { value: "21:45:00", text: "9:45 pm"}, 
                { value: "22:00:00", text: "10:00 pm"}, 
                // { value: "22:15:00", text: "10:15 pm"}, 
                { value: "22:30:00", text: "10:30 pm"}, 
                // { value: "22:45:00", text: "10:45 pm"}, 
                { value: "23:00:00", text: "11:00 pm"}, 
                // { value: "23:15:00", text: "11:15 pm"}, 
                { value: "23:30:00", text: "11:30 pm"}, 
                // { value: "23:45:00", text: "11:45 pm"}, 
            ];
    },
    template: '<select class="form-control" >\
    <option value="{{time.value}}" ng-repeat="time in timings">{{time.text}}</option>\
    </select>'
    };
});