angular.module('app.core').controller('EditDealController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', 'utility', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll, utility){
    var vm = this;
    vm.dealId =  $stateParams.deal_id;

    // date time picker funcions
    vm.isOpen_startDate = false;

    vm.openCalendar_startDate = function(e) {
        e.preventDefault();
        e.stopPropagation();

        vm.isOpen_startDate = true;
    };

    vm.isOpen_endDate = false;

    vm.openCalendar_endDate = function(e) {
        e.preventDefault();
        e.stopPropagation();

        vm.isOpen_endDate = true;
    };
    // End: date time picker funcions

    vm.getDealData = function(){
    	ShowService.get('api/v2/admin/deal/get_deal', vm.dealId).then(function(result){

    		vm.deal_data = result.response;

    		vm.deal_data.start_datetime = new Date(vm.deal_data.start);

    		vm.deal_data.end_datetime = new Date(vm.deal_data.end);
    	});
    }

    vm.updateDealData = function(){
    	var deal_data = {};
        deal_data.title = vm.deal_data.title;
        deal_data.description = vm.deal_data.description;
        deal_data.start = moment(vm.deal_data.start_datetime).format('YYYY-MM-DD HH:mm:ss');
        deal_data.end = moment(vm.deal_data.end_datetime).format('YYYY-MM-DD HH:mm:ss');
        deal_data.coupon_code = vm.deal_data.coupon_code;
        deal_data.image = vm.deal_data.image;
        deal_data.is_expired = vm.deal_data.is_expired;

        StoreService.update('api/v2/admin/deal/update_deal', vm.dealId, deal_data).then(function(result){

            vm.gotoDealListPage();

        });
    }

    vm.gotoDealListPage = function(){
        $state.go('deal-list');
    }

    vm.deleteDeal = function(id){

        var msg = "Are you sure want to delete this deal ?";

        showFlashMessageConfirmation("Delete",msg,"info",function(result) {
              if (result) {
                  ShowService.remove('api/v2/admin/deal/delete', id).then(function(results){

                       showFlashMessage('Delete','Deal deleted successfully','success');
                       vm.gotoDealListPage();
                    });
                }
        });
    }

    // date compare validation
    vm.errMessage_startdate = '';
    vm.errMessage_enddate = '';
    vm.date_error = false;
    vm.dateCompareValidation = function(startDate, endDate) {

        vm.errMessage_startdate = '';
        vm.errMessage_enddate = '';
        vm.date_error = false;

        var result = utility.dateCompareValidation(startDate, endDate);

        if(result=='end_date'){
          vm.errMessage_enddate = 'End Date & Time should be greater than start date.';
          vm.date_error = true;
          return false;
        }

        if(result=='start_date'){
           vm.errMessage_startdate = 'Start Date & Time should not be before today.';
           vm.date_error = true;
           return false;
        }
    }
    // end: date compare validation

}]);