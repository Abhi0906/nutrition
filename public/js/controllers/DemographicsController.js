/**
 * Created by Plenar on 9/24/2015.
 */
/**
 * Created by Plenar on 9/24/2015.
 */
angular.module('app.core').controller('DemographicsController', function(ShowService,StoreService,$scope){
    var vm = this;
    vm.user = [];
    vm.doctor_titles = [];
    vm.userId = $scope.userDetails.userId;
    vm.userType = $scope.userDetails.userType;
    vm.users_status = [{id:'Active', name:'Active'},
                       {id:'Inactive', name:'Inactive'},
                       {id:'Guest', name:'Guest'},
                       {id:'Incomplete', name:'Incomplete'},
                      ];

    if(vm.userType == 'doctor') {
        ShowService.query('api/v3/doctor_titles').then(function(results){
          vm.doctor_titles = results.response;
        });
    }
    ShowService.get('api/v3/'+vm.userType+'/demographics',vm.userId).then(function(results){
        var result = results.response;
       
        vm.user = result;
         if(result.user_type == 'patient'){
          vm.user.news_letter = parseInt(result.profile.news_letter);
        }
        vm.orig =angular.copy(vm.user);
    });

    //update user (patient/doctor)
    vm.updateUser = function(isValid) {
      // console.log(vm.user);return;
      if (isValid) {                   
          StoreService.update('api/v3/'+vm.userType,vm.userId,vm.user).then(function(result) {
              if(result.error == false) {
                showFlashMessage('Update','User Updated successfully','success');
              }  
          }, function(error) {
              console.log(error);
          }); 
      }  
    }
     vm.reset = function() {
      vm.user =  angular.copy(vm.orig);
    }
});