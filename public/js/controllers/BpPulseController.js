angular.module('app.core').controller('BpPulseController',
	['ShowService','$scope','StoreService','$timeout', '$location', '$anchorScroll','uiCalendarConfig', 
		function(ShowService,$scope,StoreService,$timeout, $location, $anchorScroll,uiCalendarConfig){
			var vm = this;
			vm.userId = null;
			//vm.log_date = moment().format('MMMM DD, YYYY');
            vm.log_BpPulse_error = false;
			vm.log_loader = false;
			vm.user_bppulse = {};
			vm.bp_pulse_Graph = 'all';	
			vm.last_log_date = '';
			vm.eventSources =[];
			var today = moment().format('YYYY-MM-DD');
			var before_six_day = moment().subtract(6,'d').format('YYYY-MM-DD');
			
			vm.format = 'MM-dd-yyyy';
			vm.log_date = new Date();

			vm.dateOptions = {
			  showWeeks:false
			};


			vm.bp_pulse_date_open = function() {
		        vm.bp_pulse_date_popup.opened = true;
		    };
		    vm.bp_pulse_date_popup = {
		        opened: false
		    };


			vm.bp_pulse_s_date_open = function() {
		        vm.bp_pulse_s_date_popup.opened = true;
		    };
		    vm.bp_pulse_s_date_popup = {
		        opened: false
		    };


			vm.bp_pulse_e_date_open = function() {
		        vm.bp_pulse_e_date_popup.opened = true;
		    };
		    vm.bp_pulse_e_date_popup = {
		        opened: false
		    };

		    vm.show_hide = false;
			vm.loggedBpPulse = function(change){
				  	if(change){
				    	start_date = vm.bp_sys_dys_start_date;
						end_date = vm.date_log_diastolic;
					}
					else{
						var d = new Date();
 						var before_six_day_angular = d.setDate(d.getDate()-6);
						start_date = before_six_day_angular;
						end_date  = new Date();
						vm.bp_sys_dys_start_date = start_date;
						vm.date_log_diastolic = end_date;
					}

				 start_date = moment(start_date).format('YYYY-MM-DD');
				 end_date = moment(end_date).format('YYYY-MM-DD');

				 var params = {"from_server":true, "start_date":start_date, "end_date":end_date};

				 ShowService.search('api/v3/bppulse/logged_bppulse/'+vm.userId, params).then(function(results){
				 	
				 	vm.user_bppulse = results.response.user_bp_pulse_history_data;
				 	if(results.response.hasOwnProperty('user_bp_pulse_goals')){
						vm.show_hide = true; 
						var systolic_goal = results.response.user_bp_pulse_goals.systolic;
						var diastolic_goal = results.response.user_bp_pulse_goals.diastolic;
						var heart_rate_goal = results.response.user_bp_pulse_goals.heart_rate;
						var goal_last_thirty_days = results.response.goal_last_thirty_days;

						vm.systolic_last_thirty_days = parseInt(results.response.bppulse_progress.last_month_count_systolic);
						vm.systolic_last_seven_day = parseInt(results.response.bppulse_progress.last_week_count_systolic);
						vm.systolic_last_reading = parseInt(results.response.bppulse_progress.last_progress_systolic);


						vm.diastolic_last_thirty_days = parseInt(results.response.bppulse_progress.last_month_count_diastolic);
						vm.diastolic_last_seven_day = parseInt(results.response.bppulse_progress.last_week_count_diastolic);
						vm.diastolic_last_reading = parseInt(results.response.bppulse_progress.last_progress_diastolic);


						vm.pulse_last_thirty_days = parseInt(results.response.bppulse_progress.last_month_count_pulse);
						vm.pulse_last_seven_day = parseInt(results.response.bppulse_progress.last_week_count_pulse);
						vm.pulse_last_reading = parseInt(results.response.bppulse_progress.last_progress_pulse);		
						
						}else{
				    			vm.show_hide = false; 
						} 
					if(results.response.hasOwnProperty('user_bp_pulse_goals'))
					{	
						var bp_pulse_goal_res = results.response.user_bp_pulse_goals;
						if(bp_pulse_goal_res.goal_date == undefined){
							vm.bp_pulse_goal = "-";
						}else{
						    vm.bp_pulse_goal = bp_pulse_goal_res.goal_date;
						}
						if(bp_pulse_goal_res.systolic == undefined){
							vm.systolic_goal = "-";
						}else{
							vm.systolic_goal = parseInt(bp_pulse_goal_res.systolic);
						}
						if(bp_pulse_goal_res.diastolic == undefined){
							vm.diastolic_goal = "-";
						}else{
							vm.diastolic_goal = parseInt(bp_pulse_goal_res.diastolic);
						}
						if(bp_pulse_goal_res.heart_rate == undefined){
							vm.heart_rate = "-";
						}else{
							vm.heart_rate = parseInt(bp_pulse_goal_res.heart_rate);
						}
						var leftdays = bp_pulse_goal_res.goal_date;
					}
					vm.bp_pulse_goal_days_left = vm.getDateDiffDays(today, leftdays);
					if(change){
						vm.user_bppulse = results.response.user_bp_pulse_history_data;
					}else{
						vm.user_bppulse_history = results.response.user_bp_pulse_last_log;
						vm.user_bppulse = results.response.user_bp_pulse_history_data;
					}
					if(vm.user_bppulse.length > 0){
						vm.last_log_date = moment(vm.user_bppulse[vm.user_bppulse.length-1].log_date ).format('MMM-DD-YYYY'); // get last element date
					}
					$timeout(function() {
		   					vm.systolicDiastolicCharts(vm.bp_pulse_Graph);	  
		   			}, 200);
	   			});
			}
			vm.getDateDiffDays = function(today, leftdays){
					var today = moment(today);
					var leftdays = moment(leftdays);
					return parseInt(leftdays.diff(today, 'days'));
			}
			vm.logBpPulse = function(){
				vm.log_loader = true;
				var bp_pulse_data = {};
				var pulse = {};
				bp_pulse_data.systolic = vm.systolic;
				bp_pulse_data.diastolic = vm.diastolic;
				bp_pulse_data.pulse = vm.pulse;
				bp_pulse_data.user_id = vm.userId;
				bp_pulse_data.log_date = moment(vm.log_date).format('YYYY-MM-DD');
				bp_pulse_data.guid_server = true;
				bp_pulse_data.source = "MANUAL";
				vm.bp_pulse_data = bp_pulse_data;
				
				StoreService.save('api/v3/bppulse/log_bppulse', bp_pulse_data).then(function(result) {
				    var pulse = {};
					pulse['pulse'] = {"pulse": vm.pulse};
					$.extend(true, bp_pulse_data, pulse);
					vm.user_bppulse_history.push(bp_pulse_data);
					vm.systolic = null;
					vm.diastolic = null;
					vm.pulse = null;
					vm.systolicDiastolicCharts(vm.bp_pulse_Graph);	
					vm.last_log_date = moment(bp_pulse_data.log_date).format('MMM-DD-YYYY');
					vm.log_loader = false;
					if(result.error == false) {
						showFlashMessage('Logged','Bp & Pulse logged successfully','success');
						vm.loggedBpPulse();

						if (uiCalendarConfig.calendars['bpCalendar']) {
		                    uiCalendarConfig.calendars['bpCalendar'].fullCalendar('removeEvents');
		                    vm.monthlyBpPulseSummary();
		                }

						
					}	
				});
            }

            vm.calculateBpPercent = function(reading, goal){
					 actual_data = Math.abs(reading-goal);
					 ans = (actual_data*100)/goal;
					 final_data = 100-ans;
					 return final_data;
			}



       		vm.monthlyBpPulseSummary = function(calendar_date){
              var param = vm.userId;
		      var month = moment(calendar_date).format('MM');
		      var year = moment(calendar_date).format('YYYY');
		
		      param = vm.userId+'?req_month='+month+'&req_year='+year;
		      var eventdata = [];

		      ShowService.get('api/v3/patient/monthly_bp_pulse_summary', param).then(function(results){
		      	     var values = results.response.bp_pulse_data;
					 var systolic_user_goal = 0;
					 var diastolic_user_goal = 0;
					 var pulse_user_goal = 0;
					 var text_class_systolic_uniq = "";
					 var text_class_diastolic_uniq = "";
					 var text_class_pulse_uniq = "";
					
					 // weekly data start
					vm.monthly_avg_systolic = results.response.avg_data.monthly_avg.monthly_avg_systolic;
		      		vm.monthly_avg_dystolic = results.response.avg_data.monthly_avg.monthly_avg_dystolic;
		      		vm.monthly_avg_pulse = results.response.avg_data.monthly_avg.monthly_avg_pulse;
		      		
		      		var monthly_systolic_class_avg = results.response.avg_data.monthly_avg.monthly_systolic_class_avg;
		      		var monthly_dystolic_class_avg = results.response.avg_data.monthly_avg.monthly_dystolic_class_avg;
		      		var monthly_pulse_class_avg = results.response.avg_data.monthly_avg.monthly_pulse_class_avg;
		      		 	vm.sys  = '<p>Systolic = <span class='+monthly_systolic_class_avg+'>' + vm.monthly_avg_systolic +'</p></span>';
                     	vm.dys  = '<p>Diastolic = <span class='+monthly_dystolic_class_avg+'>' + vm.monthly_avg_dystolic +'</p></span>';
						vm.pulse_month  = '<p>Pulse = <span class='+monthly_pulse_class_avg+'>' + vm.monthly_avg_pulse +'</p></span>';
					
					var week_wise_avg = results.response.avg_data.week_wise_avg;
					vm.week_wise_avg_html = {}; 
					angular.forEach(week_wise_avg, function(value, key) {
						var html_data_weekly = "";
						 $('#week_average .week_'+ key).html(html_data_weekly);
					 	 	if(value.length == undefined){
					 	 
	                        	html_data_weekly = "<p><b>Systolic = <span class="+value.weekly_systolic_class_avg+">" +  value.total_systolic_weekly_avg +
	                              					 "<p><b>Diastolic = <span class="+value.weekly_diastolic_class_avg+">" +  value.total_diastolic_weekly_avg +
													 "<p>Pulse = <span class="+value.weekly_diastolic_class_avg+">" +value.total_pulse_weekly_avg+"</p>";

	                        	$('#week_average .week_'+ key).html(html_data_weekly);
							}
						});
					// weekly data end


					if(results.response.goals_data!=null){
					 	systolic_user_goal = results.response.goals_data.systolic;
						diastolic_user_goal = results.response.goals_data.diastolic;
						pulse_user_goal = results.response.goals_data.heart_rate;	
					 }
					 
					 angular.forEach(values, function(value, key) {
					 	 	// systolic css class set
					 	 	if(systolic_user_goal!=0){
					 	 		vm.final_goal_systolic = vm.calculateBpPercent(value.systolic, systolic_user_goal);
					 	 		if(vm.final_goal_systolic > 95){
					 	 			 var text_class_systolic_uniq = "text-deficit";
					 	 		}else if(value.systolic > 139){
					 	 			var text_class_systolic_uniq = "text-excess";
					 	 		}
					 	 		else{
	                                 var text_class_systolic_uniq = "text-neutral";
	                            }
							}
					 		
                            // diastolic css class set
                            if(diastolic_user_goal!=0){
                            	vm.final_goal_diastolic = vm.calculateBpPercent(value.diastolic, diastolic_user_goal);
                            	if(vm.final_goal_diastolic > 95){
                                     var text_class_diastolic_uniq = "text-deficit";
	                            }else if(value.diastolic > 89){
	                                 var text_class_diastolic_uniq = "text-excess";
	                            }else{
	                                 var text_class_diastolic_uniq = "text-neutral";
	                            }	
                            }
                            
                            // pulse css class set
                            if(pulse_user_goal!=0){
                            	vm.final_goal_pulse = vm.calculateBpPercent(value.pulse, pulse_user_goal);
                            	if(vm.final_goal_pulse > 95){
                                     var text_class_pulse_uniq = "text-deficit";
	                            }else if(value.pulse > 149){
	                                 var text_class_pulse_uniq = "text-excess";
	                            }else{
	                                 var text_class_pulse_uniq = "text-neutral";
	                            }	
                            }
                            	eventdata.push({
		                            "systolic":value.systolic, 
		                            "diastolic":value.diastolic,
		                            "pulse":value.pulse, 
		                            "start":value.log_date,
		                            "text_class_systolic":text_class_systolic_uniq,
		                            "text_class_diastolic":text_class_diastolic_uniq,
		                            "text_class_pulse":text_class_pulse_uniq
		                          });
						});
		       });

				
				  $timeout(function() {
		            if (uiCalendarConfig.calendars['bpCalendar']) {
		               angular.forEach(eventdata, function(value, key) { 
		               uiCalendarConfig.calendars['bpCalendar'].fullCalendar('renderEvent',value,false);
		              });
		            }
		         },2000);
			  // vm.events = eventdata;
		      // vm.eventSources = [vm.events];
			}
			 vm.renderView = function(view,element){
			 	 // console.log("View Changed: ", view.visStart, view.visEnd, view.start, view.end);
		        var date = new Date(view.calendar.getDate());
		        vm.monthlyBpPulseSummary(date);
			 }
             vm.eventRender = function(event, element, view ){
		      	return $(
							 '<p><b>Systolic = <span class='+event.text_class_systolic+'>' +  event.systolic +  
                                '</span><br>Diastolic = <span class='+event.text_class_diastolic+'> ' +  event.diastolic + 
                                '</span><br>Pulse = <span class='+event.text_class_pulse+'>' + event.pulse+
                             '</div>'
		                );
			}
		    
		    vm.uiConfig = {
		        calendar:{
		                editable: false,
		                height:"100%",
		                theme: true,
		                header: {
		                    right: 'title',
		                    left:'prev,next',
		                    center:false
		                },
		               eventRender: vm.eventRender,
		               viewRender: vm.renderView,

		           }
		    };
			// calendar end / 
	        vm.systolicDiastolicCharts = function(systolic_diastolic_pulse){
				var data = vm.user_bppulse;

				var systolic_goals = vm.systolic_goal;

				
				var systolic_guide = [{
		              "boldLabel": true,
		              "color": "#FE6E0E",
		              "dashLength": 6,
		              "fillColor": "#FE6E0E",
		              "fontSize": 12,
		              "id": "Consume-Guide-1",
		              "inside": true,
		              "label": "Systolic Goal: "+ systolic_goals +"",
		              "lineAlpha": 1,
		              "lineColor": "#FE6E0E",
		              "lineThickness": 2,
		              "tickLength": 5,
		              "value": systolic_goals,
		              "valueAxis": "ValueAxis-1"
		            }];



				if(data.length > 0){
					data.sort(function(a,b){
					  return new Date(a.log_date) - new Date(b.log_date);
					});
				}
				var dataProvider_body = [];
				angular.forEach(data, function(value, key) {
				var date = moment(value.log_date).format('ddd/DD');
					dataProvider_body.push(
						{	"systolic":value.systolic,
							"diastolic":value.diastolic, 
							"pulse":value.pulse.pulse, 
							"date":date,
						});
				});
                /* graph body */ 
                vm.bp_pulse_Graph = systolic_diastolic_pulse;
           		var graphs_body = [];
				var date ='';
				var body_chart_name = '';
				if(systolic_diastolic_pulse=="all"){
					body_chart_name = 'Systolic_Chart';
					title = 'mmHg ';

					graphs_body = [
			            {
			              "balloonText": "[[title]] of [[category]]:[[value]]",
			              "bullet": "round",
			              "id": "AmGraph-1",
			              "title": "Systolic",
			              "type": "smoothedLine",
			              "valueField": "systolic"
			            },
			            {
			              "balloonText": "[[title]] of [[category]]:[[value]]",
			              "bullet": "square",
			              "id": "AmGraph-2",
			              "title": "Diastolic",
			              "type": "smoothedLine",
			              "valueField": "diastolic"
			            }
			        ];

			        var systolic_goals = vm.systolic_goal;
			        var diastolic_goals = vm.diastolic_goal;
						var guide_data = [
							{
				              "boldLabel": true,
				              "color": "#FE6E0E",
				              "dashLength": 6,
				              "fillColor": "#FE6E0E",
				              "fontSize": 12,
				              "id": "Consume-Guide-1",
				              "inside": true,
				              "label": "Systolic Goal: "+ systolic_goals +"",
				              "lineAlpha": 1,
				              "lineColor": "#FE6E0E",
				              "lineThickness": 2,
				              "tickLength": 5,
				              "value": systolic_goals,
				              "valueAxis": "ValueAxis-1"
				            },
				            {
				              "boldLabel": true,
				              "color": "#FCD202",
				              "dashLength": 6,
				              "fillColor": "#FCD202",
				              "fontSize": 12,
				              "id": "Consume-Guide-1",
				              "inside": true,
				              "label": "Diastolic Goal: "+ diastolic_goals +"",
				              "lineAlpha": 1,
				              "lineColor": "#FCD202",
				              "lineThickness": 2,
				              "tickLength": 5,
				              "value": diastolic_goals,
				              "valueAxis": "ValueAxis-1"
				            }];




					if(data.length != 0){
                		var old_systolic = data[0].systolic;
						var new_systolic = data[data.length-1].systolic;
						var sys_goal = vm.systolic_goal;
						
						vm.final_goal_systolic = vm.calculateBpPercent(new_systolic, sys_goal);
						if(vm.final_goal_systolic > 95 ){
							vm.diff_class = "green";
						}
						else{
							vm.diff_class = "red";
						}
							
						
						var systolic_diff = new_systolic - old_systolic;
						

						var old_diastolic = data[0].diastolic;
						var new_diastolic = data[data.length-1].diastolic;
						var diastoli_goal = vm.diastolic_goal;
						vm.final_goal_diastoli = vm.calculateBpPercent(new_diastolic, diastoli_goal);
						if(vm.final_goal_diastoli > 95 ){
							vm.diff_class = "green";
						}else{
							vm.diff_class = "red";
						} 

						var diastolic_diff = new_diastolic - old_diastolic;
						
						if(systolic_diff>=0){
							vm.Systolic_diff_arrow = "up";
						}
						else{
							vm.Systolic_diff_arrow = "down"; 
						}
							
						vm.systolic_chart_diff = systolic_diff;
						if(diastolic_diff>=0){
							vm.Diastolic_diff_arrow = "up";
						}
						else{
							vm.Diastolic_diff_arrow = "down"; 
						}
							vm.diastolic_chart_diff = diastolic_diff;
						}
                }
				else if(systolic_diastolic_pulse=="pulse"){
					body_chart_name = 'Pulse_Chart';
					title = 'bpm';
					graphs_body = [
						{
			              "balloonText": "[[title]] of [[category]]:[[value]]",
			              "bullet": "round",
			              "id": "AmGraph-1",
			              "title": "Pulse",
			              "type": "smoothedLine",
			              "valueField": "pulse"
			            }
					];

					var heart_goals = vm.heart_rate;
						var guide_data = [{
				              "boldLabel": true,
				              "color": "#FE6E0E",
				              "dashLength": 6,
				              "fillColor": "#FE6E0E",
				              "fontSize": 12,
				              "id": "Consume-Guide-1",
				              "inside": true,
				              "label": "Pulse Goal: "+ heart_goals +"",
				              "lineAlpha": 1,
				              "lineColor": "#FE6E0E",
				              "lineThickness": 2,
				              "tickLength": 5,
				              "value": heart_goals,
				              "valueAxis": "ValueAxis-1"
				            }];

					if(data.length != 0){
						var old_pulse = data[0].pulse.pulse;
						var new_pulse = data[data.length-1].pulse.pulse;
						var heart_rate_goal_label = vm.heart_rate;	
						vm.final_goal_heart_rate = vm.calculateBpPercent(new_pulse, heart_rate_goal_label);
						if(vm.final_goal_heart_rate > 95 ){
							vm.diff_class = "green";
						}else{
							vm.diff_class = "red";
						} 
						
						var pulse_diff = new_pulse - old_pulse;
					 	if(pulse_diff>=0){
							vm.pulse_diff_arrow = "up";
						}
						else{
								vm.pulse_diff_arrow = "down"; 
							}
							vm.pulse_chart_diff = pulse_diff;
						}
				}
 				/* end graph body */ 
				AmCharts.makeChart(body_chart_name,
					{
						"type": "serial",
				        "categoryField": "date",
				        "startDuration": 0,
				        "categoryAxis": {
				          "gridPosition": "start"
				        },
				        "trendLines": [],
				        "graphs": graphs_body,
		          		"guides": guide_data,
				        "valueAxes": [
				          {
				            "id": "ValueAxis-1",
				            "title": title
				          }
				        ],
				        "allLabels": [],
				        "balloon": {},
				        "legend": {
				          "enabled": true,
				          "useGraphSettings": true
				        },
				        "titles": [
				          {
				            "id": "Title-1",
				            "size": 15,
				            "text": ""
				          }
				        ],
						"dataProvider": dataProvider_body
					}
				);
			}
		}
	]
);