angular.module('app.core').controller('LogExerciseController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$location', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $location, $anchorScroll){
    var vm = this;
    vm.userId = $scope.diary_index.userId;
    vm.exercise_type = $stateParams.exercise_type;
    vm.searchKeyExercise = '';
    vm.exercise_tab_display = true;
    vm.log_date = $scope.diary_index.log_date;
    
    vm.search_log = {};
    vm.recent_log = {};
    vm.freq_log =  {};
    vm.select_log={};
    vm.is_selected = false;
    vm.selected_from = $stateParams.selected_from;
    vm.selectedExrcise = {};

    if(vm.exercise_type){
      // focus of selected tab
      $anchorScroll.yOffset = -1;
      var old = $location.hash();
      $location.hash('');
      $anchorScroll();
      $location.hash(old);
      // end: focus of selected tab
    }

    vm.exerciseTabToggel = function(tab_id, tab_content_id){
        
        if(tab_id){
            $(".logfood_tab_continer #log_food_tab ul li").removeClass("active");
            $("#"+tab_id).addClass("active");
        }

        if(tab_content_id){
            $(".logfood_tab_details .tab-pane").removeClass("active");
            $("#"+tab_content_id).addClass("active");
        }
        
    }

    if(vm.selected_from){

      // focus of selected tab
      $anchorScroll.yOffset = 300;
      var old = $location.hash();
      $location.hash('log_food_tab');
      $anchorScroll();
      $location.hash(old);
      // end: focus of selected tab

      vm.is_selected = true;
      vm.selectedExrcise = $stateParams.selected_exercise;
      vm.exerciseTabToggel('selected_tab', 'selected_log_tab');
      console.log(vm.selectedExrcise);
    }


    vm.searchExercise = function(){

      vm.exercise_tab_display = false;
      vm.exerciseTabToggel(null, 'searched_exercise_tab');
      vm.search_result = false;
      vm.search_loader = true;
      vm.searchNodata = false;
      var searchString = '';
      searchString = vm.searchKeyExercise;
      ShowService.search('api/v3/exercise/search_exercise?keyword='+searchString+'&type='+vm.exercise_type+'&limit=100').then(function(result) {
         
            if(result.error == false){

                vm.exerciseSearchdata = result.response;
                 if(result.response.length > 0){
                    vm.search_loader = false;
                    vm.search_result = true;
                  }
                  else{

                    vm.searchNodata = true;
                  }              

                vm.search_loader = false;
                vm.searchMenu = true;
              }

        },function(error) {
            console.log(error);
        });

    }
   
    vm.clearSearchResult = function(){
        vm.searchKeyExercise = '';
        vm.exercise_tab_display = true;
        vm.exerciseTabToggel("feq_tab", 'fre_log_tab');     
    }

  

    vm.calculateCaloriesBurned = function(index,exercise_obj,tab_type){
      if(tab_type == "search"){
        var default_min = exercise_obj.time;
        var default_calories_burn =exercise_obj.calories_burned;
        var updated_min =vm.search_log[index].time;
        var updated_calories_burned = parseInt(updated_min) * parseInt(exercise_obj.calories_burned)/parseInt(default_min);
        vm.search_log[index].calories_burned = updated_calories_burned.toFixed(2);
      }else if(tab_type == "frequent"){

        var default_min = exercise_obj.time;
        var default_calories_burn =exercise_obj.calories_burned;
        var updated_min =vm.freq_log[index].time;
        var updated_calories_burned = parseInt(updated_min) * parseInt(exercise_obj.calories_burned)/parseInt(default_min);
        vm.freq_log[index].calories_burned = updated_calories_burned.toFixed(2);
      }else if(tab_type == "recent"){

        var default_min = exercise_obj.time;
        var default_calories_burn =exercise_obj.calories_burned;
        var updated_min =vm.recent_log[index].time;
        var updated_calories_burned = parseInt(updated_min) * parseInt(exercise_obj.calories_burned)/parseInt(default_min);
        vm.recent_log[index].calories_burned = updated_calories_burned.toFixed(2);
      }else if(tab_type == "selected"){

        var default_min = exercise_obj.time;
        var default_calories_burn =exercise_obj.calories_burned;
        var updated_min =vm.select_log.time;
        var updated_calories_burned = parseInt(updated_min) * parseInt(exercise_obj.calories_burned)/parseInt(default_min);
        vm.select_log.calories_burned = updated_calories_burned.toFixed(2);

      }
      
    }

    vm.logExercise = function(exercise_obj,$index,tab_type){
               
        var log_exercise_data ={};
        var log_date = moment(vm.log_date).format('YYYY-MM-DD');
        log_exercise_data.user_id= vm.userId;
        log_exercise_data.exercise_date= log_date;
        log_exercise_data.guid_server = 'server';

        if(vm.exercise_type == 'cardio'){
          log_exercise_data.exercise_type ='Cardio';
          log_exercise_data.exercise_sub_type ='Cardio';
        }else if(vm.exercise_type == 'resistance'){
          log_exercise_data.exercise_type ='Resistance';
          log_exercise_data.exercise_sub_type ='Resistance';
        }

        if(tab_type == 'search'){
           log_exercise_data.exercise_id= exercise_obj.id;
           log_exercise_data.calories_burned=  vm.search_log[$index].calories_burned; 
           log_exercise_data.time = vm.search_log[$index].time;
           if(vm.exercise_type == 'resistance'){
            log_exercise_data.sets= vm.search_log[$index].sets; 
            log_exercise_data.reps=  vm.search_log[$index].reps; 
            log_exercise_data.weight_lbs = vm.search_log[$index].weight_lbs;
           }

        }else if(tab_type == 'recent'){

           log_exercise_data.exercise_id= exercise_obj.exercise_id;
           log_exercise_data.calories_burned=  vm.recent_log[$index].calories_burned; 
           log_exercise_data.time = vm.recent_log[$index].time;
           if(vm.exercise_type == 'resistance'){
            log_exercise_data.sets= vm.recent_log[$index].sets; 
            log_exercise_data.reps=  vm.recent_log[$index].reps; 
            log_exercise_data.weight_lbs = vm.recent_log[$index].weight_lbs;
           }


        }else if(tab_type == 'frequent'){

           log_exercise_data.exercise_id= exercise_obj.exercise_id;
           log_exercise_data.calories_burned=  vm.freq_log[$index].calories_burned; 
           log_exercise_data.time = vm.freq_log[$index].time;
           if(vm.exercise_type == 'resistance'){
            log_exercise_data.sets= vm.freq_log[$index].sets; 
            log_exercise_data.reps=  vm.freq_log[$index].reps; 
            log_exercise_data.weight_lbs = vm.freq_log[$index].weight_lbs;
           }

        }else if(tab_type == 'selected'){

           log_exercise_data.exercise_id= exercise_obj.exercise_id;
           log_exercise_data.exercise_type =exercise_obj.exercise_type;
           log_exercise_data.calories_burned=  vm.select_log.calories_burned; 
           log_exercise_data.time = vm.select_log.time;
           if(exercise_obj.exercise_type == 'Resistance'){
            log_exercise_data.sets= vm.select_log.sets; 
            log_exercise_data.reps=  vm.select_log.reps; 
            log_exercise_data.weight_lbs = vm.select_log.weight_lbs;
           }

        }
       
        StoreService.save('api/v3/exercise/log_exercise',log_exercise_data).then(function(result) {
          if(result.error == false){


            var logExerciseToBePusshed = {};

            logExerciseToBePusshed.calories_burned = log_exercise_data.calories_burned;
            logExerciseToBePusshed.exercise_id = log_exercise_data.exercise_id;
            logExerciseToBePusshed.exercise_type = log_exercise_data.exercise_type;
            logExerciseToBePusshed.time = log_exercise_data.time;
            logExerciseToBePusshed.user_id = log_exercise_data.user_id;
            logExerciseToBePusshed.name = exercise_obj.name;
            logExerciseToBePusshed.id = result.response.log_exercise.id;


            if(vm.exercise_type == 'resistance'){
              logExerciseToBePusshed.reps = log_exercise_data.reps;
              logExerciseToBePusshed.sets = log_exercise_data.sets;
              logExerciseToBePusshed.weight_lbs = log_exercise_data.weight_lbs;

              $scope.diary_index.resistanceExercises.push(logExerciseToBePusshed);
              $scope.diary_index.totalResistance.calories_burned = Number($scope.diary_index.totalResistance.calories_burned) + Number(logExerciseToBePusshed.calories_burned);
              $scope.diary_index.exerciseSummary.total_burned = Number($scope.diary_index.exerciseSummary.total_burned) + Number(logExerciseToBePusshed.calories_burned);
            }
            else if (vm.exercise_type == 'cardio'){
              $scope.diary_index.cardioExercises.push(logExerciseToBePusshed);
              $scope.diary_index.totalCardio.calories_burned = Number($scope.diary_index.totalCardio.calories_burned) + Number(logExerciseToBePusshed.calories_burned);
              $scope.diary_index.exerciseSummary.total_burned = Number($scope.diary_index.exerciseSummary.total_burned) + Number(logExerciseToBePusshed.calories_burned);
            }

            $state.go('diary');
            $scope.diary_index.food_exercise_type = vm.exercise_type;
          }
        },function(error) {
          console.log(error);
        });

    }

     vm.allLoggedExercise = function(){

        ShowService.get('api/v3/exercise/all_logged_exercise/'+vm.userId,vm.exercise_type).then(function(results){
            vm.frequent_exercises = results.response.frequent_exercises;
            vm.recent_exercises = results.response.recent_exercises;
            
        });

    }
  
}]);