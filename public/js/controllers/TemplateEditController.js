angular.module('app.core').controller('TemplateEditController',['ShowService','StoreService','$scope','$rootScope','$stateParams','$state',function(ShowService,StoreService,$scope,$rootScope,$stateParams,$state){
	  var vm = this;
	  vm.templateId =  $stateParams.templateId;
    vm.type =  $stateParams.type;

    vm.workouts = [];
      $scope.workout_keyword = "";
	  vm.week = [
      {dayNumber: '0', dayName: 'SUN'},
      {dayNumber: '1', dayName: 'MON'},
      {dayNumber: '2', dayName: 'TUE'},
      {dayNumber: '3', dayName: 'WED'},
      {dayNumber: '4', dayName: 'THU'},
      {dayNumber: '5', dayName: 'FRI'},
      {dayNumber: '6', dayName: 'SAT'},
    ];

    vm.Edit_Form_Details =true;
    vm.editformShowHide = function(){
          vm.Edit_Form = true;
          vm.Edit_Form_Details = false;
    }
     ShowService.get('api/v2/admin/template',vm.templateId).then(function(results){
      	vm.selectedTemplate = results.response.template;
      	vm.assignWorkouts = results.response.workouts;

        if (vm.type == 'edit') {
          vm.Edit_Form = true;
          vm.Edit_Form_Details = false;
        }
     });



    var watchArray = ['workout_keyword','experience','body_part'];

     $scope.$watchGroup(watchArray, function(newVal, oldVal) {
            vm.searchWorkout();
     });

     vm.searchWorkout = function(){

      //getting all master workouts with associate videos.
      if($scope.workout_keyword != "" ||
         typeof $scope.experience != "undefined" ||
         typeof $scope.body_part != "undefined"
        ){
              var params ={'keyword':$scope.workout_keyword,
                          'experience':$scope.experience,
                          'body_part':$scope.body_part
                        };
             ShowService.search('api/v2/admin/workout/list',params,1).then(function(results){
                         vm.workouts = results.response;
                  });

          }else{

              ShowService.query('api/v2/admin/workout/list').then(function(results){
                    vm.workouts = results.response;
               });
          }

    }


     vm.workoutInserted = function(event, index, item,  templateId) {
       var data = {'template_id':templateId,'workout_id':item.id};

       StoreService.save('api/v2/admin/template/attached_workout',data).then(function(result) {
              vm.selectedTemplate.workout_count = parseInt(vm.selectedTemplate.workout_count)+1;
            //  vm.selectedTemplate.video_count = parseInt(vm.selectedTemplate.video_count)+parseInt(item.videos.length);
              //vm.updateSelectedTemplateDuration();
            }, function(error) {
              console.log(error);
            });
    }

    vm.deleteWorkout = function(workout,template_id){

      var workout_id = workout.id;

        var msg = "Are you sure want to delete this workout ?";
        showFlashMessageConfirmation("Delete",msg,"info",function(result) {
            if (result) {
                ShowService.remove('api/v2/admin/template/delete_workout/'+template_id,workout_id).then(function(results){
                   if(results.error == false){
                      var index =  vm.assignWorkouts.indexOf(workout);
                      vm.assignWorkouts.splice(index, 1);
                      vm.selectedTemplate.workout_count = parseInt(vm.selectedTemplate.workout_count)-1;
                      // vm.selectedTemplate.video_count = parseInt(vm.selectedTemplate.video_count)-parseInt(workout.videos.length);
                        vm.updateSelectedTemplateDuration();
                      showFlashMessage('Delete','Workout deleted successfully','success');
                   }
                });
            }
        });
    }

    vm.updateSetting = function(workout,isValid)
    {
      if(isValid){
        var workout_data = {};
         workout_data.template_id = vm.selectedTemplate.id;
         workout_data.workout_id = workout.id;
         workout_data.start = workout.pivot.start;
         workout_data.end = workout.pivot.end;
         workout_data.config = workout.pivot.config;
         vm.updateSelectedTemplateDuration();
         workout_data.template_days = vm.selectedTemplate.days;
         //console.log(workout_data);return false;
        // return;
         StoreService.save('api/v2/admin/template/updateWorkout',workout_data).then(function(result) {
             if(result.error == false) {

            }
         }, function(error) {
            console.log(error);
          });
      }
    }

    vm.clearWorkoutFilter = function(){

		$scope.workout_keyword = "";
		$scope.experience = "undefined";
		$scope.body_part = "undefined";
    }

    vm.getWeekDayName = function(days){

      var week_name = ['SUN','MON','TUE','WED','THU','FRI','SAT'];
      var week_names ='';
      if(days !=""){
         days.sort();
        angular.forEach(days, function(value, key) {
           week_names+= week_name[value]+",&nbsp;";
          });
        week_names = week_names.slice(0,-7);
      }
      return week_names;
    }

    vm.updateSelectedTemplateDuration = function(){
      if(vm.assignWorkouts && vm.assignWorkouts.length > 0){
        var temp = new Array();
        for(var i = 0; i < vm.assignWorkouts.length;i++){
          if(typeof vm.assignWorkouts[i].pivot != 'undefined'){
            temp.push(vm.assignWorkouts[i].pivot.end);
          }

        }
      }
      var max_of_array = Math.max.apply(Math, temp);
      if(vm.selectedTemplate.days != max_of_array){
        vm.selectedTemplate.days = max_of_array;
        //vm.saveTemplate();
      }

    }

    vm.getWorkoutTitle = function(workout){

      //return '<a style="cursor:pointer" href="javascript:void(0)">'+workout.title + '</a> ['+workout.body_part+']';
      if(workout == null){
        return true;
      }
      var class_experience ="text-primary";
      if(workout.experience == 'Beginner'){
        class_experience="text-primary";
      }
      else if(workout.experience == 'Intermediate'){
        class_experience="text-warning";
      }
      if(workout.experience == 'Advanced'){
        class_experience="text-success";
      }
      var start_days = (typeof workout.pivot != 'undefined' && workout.pivot.start !=null)?workout.pivot.start:'';
      var end_days = (typeof workout.pivot != 'undefined' && workout.pivot.end !=null)?workout.pivot.end:'';

      var config =   (typeof workout.pivot != 'undefined' && workout.pivot.config !=null)?workout.pivot.config:'';
      var week_days_name = vm.getWeekDayName(config);
      var icon = '<strong><i class="flaticon-stick5 '+ class_experience +'"></i></strong>';
      var atag = '<span class="tillana-font '+class_experience+'">'+workout.title +'</span>';
      var video_count = '<small class="tillana-font">Videos:</small><small><strong> '+workout.video_count +'</strong></small>';

      var exercise_count = "<small class='tillana-font'>Exercise:</small>&nbsp;<span><small>"+workout.exercise_count+"</small></span>";

        var total_mins = "<small class='tillana-font'>Total Minutes:</small>&nbsp;<span><small>"+workout.total_mins+"</small></span>";

      var body_part = "<small class='tillana-font'>Body part:</small> <span><small>&nbsp;"+workout.body_part+"</small></span>,"+" &nbsp;&nbsp;" +video_count+"&nbsp;&nbsp;" +exercise_count+"&nbsp;&nbsp;" +total_mins;

      var duration = "<span class='tillana-font'><small>Duration:</small>  </span><small>&nbsp;&nbsp;From: </small><small><strong>"+start_days+"</strong></small><small>&nbsp;&nbsp;To: </small><small><strong>"+end_days+"</strong></small>";

      var week_days = "<span class='tillana-font'><small>Week Day(s):</small></span>&nbsp;<small><strong>"+week_days_name+"</strong></small>";

       return icon + "&nbsp;&nbsp;"+ atag + "<br>"+body_part+"<br>"+duration+",&nbsp;&nbsp;"+week_days;
    }

    vm.back = function(){
    	$state.go('list-workout-plans');
    }

    vm.updateTemplate = function(){
      var data = vm.selectedTemplate;
      vm.keyword = "";

      if(typeof data.id != 'undefined'){
        //edit template
        StoreService.update('api/v2/admin/template',data.id,data).then(function(results){
            if(results.error == false){
               showFlashMessage('Update','Template updated successfully','success');
               $state.go('list-workout-plans');
            }
        },
        function(error) {
            console.log(error);
        });
      }
    }

}]);