angular.module('app.routes').config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/list-workout-plans');

    $stateProvider

        .state('list-workout-plans', {
            url: '/list-workout-plans',
            templateUrl: '/admin/load_list_workout_plans',
            controller : 'TemplateListController',
            controllerAs : 'list_template'
        })

        .state('edit-workout-plans', {
            url: '/edit-workout-plans/:templateId/:type',
            templateUrl: '/admin/load_edit_wokout_plans',
            controller : 'TemplateEditController',
            controllerAs : 'edit_template'
        })

        .state('add-workout-plans', {
            url: '/add-workout-plans',
            templateUrl: '/admin/load_add_workouts_plans',
            controller : 'TemplateAddController',
            controllerAs : 'add_template'
        });
});



angular.module('app.core').controller('TemplateController',['ShowService','StoreService','$scope','$rootScope',function(ShowService,StoreService,$scope,$rootScope){
	  var vm = this;
    vm.experiences = ['Beginner','Intermediate','Advanced'];
    ShowService.query('api/v2/video/body_parts').then(function(results){
            vm.body_parts = results.response;
    });

}]);
