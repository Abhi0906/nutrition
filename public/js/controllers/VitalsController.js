/**
 * Created by Plenar on 9/24/2015.
 */
angular.module('app.core').controller('VitalsController', function(ShowService,StoreService,$scope){
    var vm = this;
    vm.userId = $scope.userDetails.userId;
    vm.weight_parameters = [];
    vm.blood_pressure_parameters = [];
    vm.blood_sugar_parameters = [];
    vm.pulse_ox_parameters = [];
    vm.vitalOriginal=[];
   
    ShowService.get('api/v3/patient/vitals',vm.userId).then(function(results){
        var result = results.response;
        vm.weight_parameters = result.weight_parameters;
        vm.blood_pressure_parameters = result.blood_pressure_parameters;
        vm.blood_sugar_parameters = result.blood_sugar_parameters;
        vm.pulse_ox_parameters = result.pulse_ox_parameters;
        vm.vitalOriginal = angular.copy(result);
           
    });
    $scope.$watch('vitals.weight_parameters.baseline_weight',function(value){
      var baseline_weight = 0;
      try{
        baseline_weight = parseInt(value);
     }catch(error){}

      if(baseline_weight){
        vm.weight_parameters.max_weight = baseline_weight+5 ;
        vm.weight_parameters.min_weight = baseline_weight-5 ;
      }

    });

     vm.updateVital = function(vital,vital_type) {
         
          vital.vital_type =vital_type;
          delete(vital.created_at);
          delete(vital.updated_at);
          delete(vital.deleted_at);
          delete(vital.created_by);
          delete(vital.updated_by);
          StoreService.save('api/v3/patient/vitalParameters',vital).then(function(result) {
               console.log(result)
               showFlashMessage('Save','Vital Parameters save successfully','success');
            }, function(error) {
              console.log(error);
            });
           
     }

     vm.reset = function(type){
        if(type == 'weight'){
           vm.weight_parameters = angular.copy(vm.vitalOriginal.weight_parameters);
        }else if(type == 'blood_pressure'){
             vm.blood_pressure_parameters = angular.copy(vm.vitalOriginal.blood_pressure_parameters);
        }else if(type == 'pulse_ox'){
             vm.pulse_ox_parameters = angular.copy(vm.vitalOriginal.pulse_ox_parameters);
        }else if(type == 'blood_sugar'){
             vm.blood_sugar_parameters = angular.copy(vm.vitalOriginal.blood_sugar_parameters);
        }
     }
    
});

