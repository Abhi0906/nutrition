angular.module('app.routes').config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/list-workouts');

    $stateProvider

        .state('list-workouts', {
            url: '/list-workouts',
            templateUrl: '/admin/load_list_workouts',
            controller : 'WorkoutListController',
            controllerAs : 'list_workout'
        })

        .state('edit-workout', {
            url: '/edit-workout/:workoutId/:type',
            templateUrl: '/admin/load_edit_workout',
            controller : 'WorkoutEditController',
            controllerAs : 'edit_workout'
        })
        .state('add-workout', {
            url: '/add-workout',
            templateUrl: '/admin/load_add_workouts',
            controller : 'WorkoutAddController',
            controllerAs : 'add_workout'
        });

});





/**
 * Workout controller is for listing, add, update, delete master workouts.
 */
angular.module('app.core').controller('WorkoutController',['ShowService','StoreService','$scope','$rootScope',function(ShowService,StoreService,$scope,$rootScope){
	  var vm = this;
    vm.experiences = ['Beginner','Intermediate','Advanced'];
    vm.exercise_categories = ['Resistance','Cardio'];


    ShowService.query('api/v2/video/body_parts').then(function(results){
            vm.body_parts = results.response;
    });

    //desplaying workout with details and videos.
    vm.workoutDetails = function(workout){
    	vm.selectedWorkout = workout;
    }



    vm.addWorkout =function(){

      vm.selectedWorkout = null;

    }
    vm.cancelWorkout = function(){

    //vm.blockTitleHeading = 'FIND WORKOUTS';

    }

     vm.getWorkoutTitle = function(workout){
            if(workout == null){
              return true;
            }
            var class_experience ="text-primary";
            if(workout.experience == 'Beginner'){
              class_experience="text-primary";
            }
            else if(workout.experience == 'Intermediate'){
              class_experience="text-warning";
            }
            if(workout.experience == 'Advanced'){
              class_experience="text-success";
            }
            var icon = '<strong><i class="flaticon-stick5 '+ class_experience +'"></i></strong>';
            var atag = '<span class="tillana-font '+class_experience+'">'+workout.title +'</span>';
            var body_part = "<small>Body part</small>: <span><small>"+workout.body_part+"</small></span>";
            var video_count = "<small>Videos:</small>&nbsp;<span><small>"+workout.video_count+"</small></span>";
            var exercise_count = "<small>Exercise:</small>&nbsp;<span><small>"+workout.exercise_image_count+"</small></span>";

            var total_mins = "<small>Total Minutes:</small>&nbsp;<span><small>"+workout.total_mins+"</small></span>";

            return icon + "&nbsp;&nbsp;"+ atag + " &nbsp;&nbsp;&nbsp;&nbsp;"+body_part+",&nbsp;&nbsp;"+video_count+",&nbsp;&nbsp;"+exercise_count+",&nbsp;&nbsp;"+total_mins;
     }




    vm.viewExercise = function(video_excise){

        if(video_excise.workout_type == 'exercise_images'){

            var parseSteps =  angular.fromJson(video_excise.steps);
            console.log(parseSteps);
            vm.selectedExercise = video_excise;
            vm.selectedExercise.steps = parseSteps;
            $("#workout_exercise_image").modal('show');

        }

    }


}]);
