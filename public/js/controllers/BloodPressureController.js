angular.module('app.core').controller('BloodPressureController',
	['ShowService','$scope','StoreService','$timeout', '$location', '$anchorScroll', 
		function(ShowService,$scope,StoreService,$timeout, $location, $anchorScroll){
			var vm = this;
			vm.log_weight = {};
			vm.userId = null;
			vm.log_weight.log_date = moment().format('MMMM DD, YYYY');
			vm.body_fat_error = '';
			vm.log_weight_error = false;
			vm.log_loader = false;
			vm.user_weights = {};
			var today = moment().format('YYYY-MM-DD');

			vm.loggedWeight = function(requested_days, btn_id){

				var params = '';
				if(requested_days)
				{
					params = {"requested_days":requested_days};

					$(".days-filter").removeClass("active");
            		$("#"+btn_id).addClass("active");
					
				}
				ShowService.search('api/v3/weight/logged_weight/'+vm.userId, params).then(function(results){
					vm.user_weights = results.response;

					$timeout(function() {
		   					vm.amCharts();		   					
	   					}, 200);

				});
			}
			// end Check date exist in logged weight

			vm.amCharts = function(){
				
				var data = vm.user_weights;
				if(data.length > 0){
					data.sort(function(a,b){
					  return new Date(a.log_date) - new Date(b.log_date);
					});
				}
				
				var dataProvider = [];
				angular.forEach(data, function(value, key) {
					var date = moment(value.log_date).format('MMM DD,<br>YYYY');
					//var date = '2016-04-27';
					dataProvider.push({"weight":value.current_weight, "date":date, "systolic":"100", "diastolic":"150" });
				});

				AmCharts.makeChart("chartdiv",
					{
						"type": "serial",
						"categoryField": "date",
						"startDuration": 0,
						"handDrawScatter": 3,
						"categoryAxis": {
							"gridPosition": "start"
						},
						"trendLines": [],
						"graphs": [
							{
								"balloonText": "[[value]] lbs On [[category]]",
								"bullet": "round",
								"id": "AmGraph-1",
								"title": "Systolic graph",
								"type": "smoothedLine",
								"valueField": "weight"
							},
							{
								"balloonText": "[[value]] of [[category]]",
								"bullet": "square",
								"id": "AmGraph-2",
								"title": "Diastolic graph",
								"type": "smoothedLine",
								"valueField": "systolic"
							},
							{
								"balloonText": "[[value]] of [[category]]",
								"bullet": "square",
								"id": "AmGraph-3",
								"title": "Heart Rate graph",
								"type": "smoothedLine",
								"valueField": "diastolic"
							}
						],
						"guides": [],
						"valueAxes": [
							{
								"id": "ValueAxis-1",
								"title": "Systolic,Diastolic"

							}
						],
						"allLabels": [],
						"balloon": {},
						"legend": {
							"enabled": true,
							"useGraphSettings": true
						},
						"titles": [
							{
								"id": "Title-1",
								"size": 15,
								"text": ""
							}
						],
						"dataProvider": dataProvider

					}
				);
			}
		}
	]
);