angular.module('app.routes').config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/food-diary');
    
    $stateProvider
        .state('diary', {
            url: '/diary',
            templateUrl: '/load_diary_page',
            controller : 'FoodDiaryController',
            controllerAs : 'food_diary'
        })
        .state('food-diary', {
            url: '/food-diary',
            templateUrl: '/load_food_diary',
            controller : 'FoodDiaryController',
            controllerAs : 'food_diary'
        })
        .state('log_food', {
            url: '/log_food',
            templateUrl: '/log_food',
            params : { food_type : null, selected_from: null, selected_food : null },
            controller : 'LogFoodController',
            controllerAs : 'add_food'
        })
        .state('add_custom_food', {
            url: '/add_custom_food',
            templateUrl: '/add_custom_food',
            controller : 'FoodController',
            controllerAs : 'food'
        })
        .state('custom_foods', {
            url: '/custom_foods',
            templateUrl: '/custom_foods',
            controller : 'CustomFoodController',
            controllerAs : 'custom_food'
        })
        .state('create_meal', {
            url: '/create_meal',
            templateUrl: '/create_meal',
            params : { food_type: null, meal_food: null, meal_food_total: null },
            controller : 'MealController',
            controllerAs : 'meal'
        })
        .state('meals', {
            url: '/meals',
            templateUrl: '/meals',
            controller : 'MealController',
            controllerAs : 'meal'
        });
        
});

angular.module('app.core').controller('FoodController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){
    var vm = this;
    vm.userId = null;
    vm.food_type = null;
    vm.log_date = new Date();
    vm.favourite_nutritions = null;
    vm.frequent_nutritions = null;
    vm.recent_nutritions = null;
    vm.custom_food = {};
    vm.meals = {};
    vm.log_newfoodloader = false;
    $('.back-top').trigger('click');

    // set custom food object values
    vm.custom_food.sodium = 0;

    vm.custom_food.total_fat = 0;

    vm.custom_food.potassium = 0;
    vm.custom_food.saturated_fat = 0;

    vm.custom_food.total_carbs = 0;

    vm.custom_food.polyunsaturated = 0;

    vm.custom_food.dietary_fiber = 0;

    vm.custom_food.monounsaturated = 0;

    vm.custom_food.sugar = 0;

    vm.custom_food.protein = 0;

    vm.custom_food.trans_fat = 0;
    vm.custom_food.cholesterol = 0;
    vm.custom_food.vitamin_a = 0;

    vm.custom_food.calcium = 0;

    vm.custom_food.vitamin_c = 0;

    vm.custom_food.vitamin_c = 0;

    vm.custom_food.iron = 0;
    vm.custom_food.serving_quantity = 0;
    vm.custom_food.serving_size_unit = '';
    // end set custom food object values
  
    vm.add_new_food = function(){
       vm.log_newfoodloader = true;

       var custom_food_data = {

            "user_id" : vm.userId,
            "user_agent" : 'server',
            "brand_name" : vm.custom_food.brand_name,
            "name" : vm.custom_food.food_name,
            "nutrition_data": {
                "calories": vm.custom_food.calories,
                "sodium": vm.custom_food.sodium + " mg",
                "total_fat": vm.custom_food.total_fat + " g",
                "potassium": vm.custom_food.potassium + " mg",
                "saturated_fat": vm.custom_food.saturated_fat + " g",
                "total_carb": vm.custom_food.total_carbs + " g",
                "polyunsaturated": vm.custom_food.polyunsaturated + " g",
                "dietary_fiber": vm.custom_food.dietary_fiber + " g",
                "monounsaturated": vm.custom_food.monounsaturated + " g",
                "sugars": vm.custom_food.sugar + " g",
                "protein": vm.custom_food.protein + " g",
                "trans_fat": vm.custom_food.trans_fat + " g",
                "cholesterol": vm.custom_food.cholesterol + " mg",
                "vitamin_a": vm.custom_food.vitamin_a + "%",
                "calcium_dv": vm.custom_food.calcium + "%",
                "vitamin_c": vm.custom_food.vitamin_c + "%",
                "iron_dv": vm.custom_food.iron + "%",
                "serving_quantity": vm.custom_food.serving_quantity,
                "serving_size_unit": vm.custom_food.serving_size_unit,
                }
       };
        
        /*console.log("nutrition_data");
        console.log(nutrition_data); return false;*/

        StoreService.save('api/v3/food/add_custom_food',custom_food_data).then(function(result) {
            if(result.error == false){

                if(result.error == false) {
                    showFlashMessage('Added','Custom Food Added successfully','success');
                    $state.go('food-diary');
                }

                vm.custom_food = {};
                
            }
            vm.log_newfoodloader = false;

        },function(error) {
            console.log(error);
          });

        return false;
    }
}]);
