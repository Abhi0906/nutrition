angular.module('app.core').controller('WorkoutEditController',['ShowService','StoreService','$scope','$rootScope','$stateParams','$state',function(ShowService,StoreService,$scope,$rootScope,$stateParams,$state){
	  var vm = this;
	  vm.workoutId =  $stateParams.workoutId;
    vm.type =  $stateParams.type;
    vm.selectedWorkout = null;
	  vm.exercise_list = [];
	  vm.filterData =[];
    vm.workout_type ="exercise_images";
    vm.ResultTitle ="Exercise Images";
    vm.filterLoading =true;
    vm.showAddWorkout =false;
    vm.selecedExercise=null;

    vm.Edit_Form_Details =true;
    vm.editformShowHide = function(){
          vm.Edit_Form = true;
          vm.Edit_Form_Details = false;
    }

      $scope.keyword="";
      $scope.minimun_exercise_time = "";
      $scope.maximum_exercise_time = "" ;
      $scope.minimun_calorie_burn = "";
      $scope.maximum_calorie_burn = "";

    ShowService.get('api/v2/admin/workout',vm.workoutId).then(function(results){
      	vm.selectedWorkout = results.response;
        vm.exercise_list = results.response.exercise_lists;
        vm.workoutname_title =  results.response.title;

        if (vm.type == 'edit') {
          vm.Edit_Form = true;
          vm.Edit_Form_Details = false;
        }

     });




    vm.updateWorkout = function(isValid){
      if (isValid) {
            var data = vm.selectedWorkout;
            if(data != null && typeof data.id != "undefined") {
              delete(data.created_at);
              delete(data.updated_at);
              delete(data.deleted_at);
              StoreService.update('api/v2/admin/workout',data.id,data).then(function(results){
                   if(results.error == false){
                     showFlashMessage('Update','Workout updated successfully!','success');
                     $state.go('list-workouts');
                   }
               },
               function(error) {
                  console.log(error);
               });
            }
        }
    }
    var watchArray = ['keyword',
                      'minimun_exercise_time',
                      'maximum_exercise_time',
                      'minimun_calorie_burn',
                      'maximum_calorie_burn',
                      'body_part',
                      'experience',
                      'equipment',
                      'exercise_type'
                      ];

    $scope.$watchGroup(watchArray, function(newVal, oldVal) {

      if(vm.workout_type == 'exercise_images'){

        vm.fetchExercise();
        vm.ResultTitle ="Exercise Images";

      }else if(vm.workout_type == 'videos'){
        vm.fetchVideo();
        vm.ResultTitle ="Videos";

      }
    });


    vm.fetchExercise = function(){

      if( $scope.keyword != "" ||
           $scope.minimun_exercise_time != "" ||
           $scope.maximum_exercise_time != "" ||
           $scope.minimun_calorie_burn != "" ||
           $scope.maximum_calorie_burn != "" ||
          typeof $scope.experience != "undefined" ||
          typeof $scope.body_part != "undefined" ||
          typeof $scope.equipment != "undefined" ||
          typeof $scope.exercise_type != "undefined"
       ){

        var params ={'keyword':$scope.keyword,
                      'minimun_exercise_time':$scope.minimun_exercise_time,
                      'maximum_exercise_time':$scope.maximum_exercise_time,
                      'minimun_calorie_burn':$scope.minimun_calorie_burn,
                      'maximum_calorie_burn':$scope.maximum_calorie_burn,
                      'experience':$scope.experience,
                      'body_part':$scope.body_part,
                      'equipment':$scope.equipment,
                      'exercise_type':$scope.exercise_type,
                      'itemPerPage':20
                          };
             ShowService.search('api/v2/admin/exercise_image/list',params,1).then(function(results){
             var exercise_images = results.response.data;

             angular.forEach(exercise_images, function(value,key){
                 value.workout_type ='exercise_images';
               });
              vm.filterData =exercise_images;

              vm.filterLoading = false;
        });


      }else{

        ShowService.query('api/v2/admin/exercise_image/list?page=1&itemPerPage=20').then(function(results){
           var exercise_images = results.response.data;
           angular.forEach(exercise_images, function(value,key){
            value.workout_type ='exercise_images';
           });
              vm.filterData =exercise_images;
              vm.filterLoading =false;
       });

      }

    }

    vm.fetchVideo = function(){

      if( $scope.keyword != "" ||
               $scope.minimun_exercise_time != "" ||
               $scope.maximum_exercise_time != "" ||
               $scope.minimun_calorie_burn != "" ||
               $scope.maximum_calorie_burn != "" ||
              typeof $scope.experience != "undefined" ||
              typeof $scope.body_part != "undefined" ||
              typeof $scope.equipment != "undefined"
       ){

        var params ={'keyword':$scope.keyword,
                      'minimun_workout_time':$scope.minimun_exercise_time,
                      'maximum_workout_time':$scope.maximum_exercise_time,
                      'minimun_calorie_burn':$scope.minimun_calorie_burn,
                      'maximum_calorie_burn':$scope.maximum_calorie_burn,
                      'experience':$scope.experience,
                      'body_part':$scope.body_part,
                      'equipment':$scope.equipment,
                      'itemPerPage':20
                          };
             ShowService.search('api/v2/video/list',params,1).then(function(results){

               var videos = results.response.data;
               angular.forEach(videos, function(value,key){
                value.workout_type ='videos';
               });
                vm.filterData =videos;
                vm.filterLoading =false;

             });


      }else{

           ShowService.query('api/v2/video/list?page=1&itemPerPage=20').then(function(results){
             var videos = results.response.data;
             angular.forEach(videos, function(value,key){
              value.workout_type ='videos';
             });
                vm.filterData =videos;
                vm.filterLoading =false;
          });

      }

    }


    vm.exerciseInserted = function(event, index, item,  workoutId) {

      var data = {'workout_id':workoutId,'exrecise_list':vm.exercise_list};

        StoreService.save('api/v2/admin/workout/attached_exercise',data).then(function(result) {
           if(item.workout_type == 'exercise_images'){
             var exercise_time = item.exercise_time;
             vm.selectedWorkout.exercise_image_count = parseInt(result.response.exercise_image_count);
             vm.selectedWorkout.total_mins = parseInt(result.response.total_mins);
           }

           if(item.workout_type == 'videos'){
            var videos_time = item.workout_time;
            vm.selectedWorkout.video_count = parseInt(result.response.video_count);
            vm.selectedWorkout.total_mins = parseInt(result.response.total_mins);
           }

        }, function(error) {
          console.log(error);
        });


    }


     vm.deleteExercise = function(exercise,workout_id){

           if(exercise.workout_type == 'exercise_images'){
             var msg = "Are you sure want to delete this Exercise Image ?";
           }else if(exercise.workout_type == 'videos'){
             var msg = "Are you sure want to delete this video ?";
           }

          showFlashMessageConfirmation("Delete",msg,"info",function(result) {
                    if (result) {
                        var index =  vm.exercise_list.indexOf(exercise);
                        vm.exercise_list.splice(index, 1);
                        var data = {'workout_id':workout_id,'exrecise_list':vm.exercise_list};

                         StoreService.save('api/v2/admin/workout/attached_exercise',data).then(function(result) {
                           if(exercise.workout_type == 'exercise_images'){
                             var exercise_time = exercise.exercise_time;
                             vm.selectedWorkout.exercise_image_count = parseInt(vm.selectedWorkout.exercise_image_count)-1;
                             vm.selectedWorkout.total_mins = parseInt(vm.selectedWorkout.total_mins)-parseInt(exercise_time);
                           }

                           if(exercise.workout_type == 'videos'){
                            var videos_time = exercise.workout_time;
                            vm.selectedWorkout.video_count = parseInt(vm.selectedWorkout.video_count)-1;
                            vm.selectedWorkout.total_mins = parseInt(vm.selectedWorkout.total_mins)-parseInt(videos_time);
                           }
                            showFlashMessage('Delete','Exercise deleted successfully!','success');

                        }, function(error) {
                          console.log(error);
                        });
                    }
          });

    }

    vm.changeFilter = function(){
     vm.filterLoading =true;
     vm.filterData =[];
     if(vm.workout_type == 'exercise_images'){

        vm.fetchExercise();
        vm.ResultTitle ="Exercise Images";

      }else if(vm.workout_type == 'videos'){
        vm.fetchVideo();
        vm.ResultTitle ="Videos";

      }
    }

    vm.clearFilter = function(){

      $scope.keyword="";
      $scope.minimun_exercise_time = "";
      $scope.maximum_exercise_time = "" ;
      $scope.minimun_calorie_burn = "";
      $scope.maximum_calorie_burn = "";
      $scope.experience = "undefined";
      $scope.body_part = "undefined";
      $scope.equipment = "undefined";
      $scope.exercise_type ="undefined";
    }




}]);