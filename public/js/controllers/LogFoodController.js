angular.module('app.core').controller('LogFoodController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$location', '$anchorScroll','utility', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $location, $anchorScroll,utility){
    var vm = this;
    vm.userId = $scope.diary_index.userId;
    vm.user_gender = $scope.diary_index.user_gender;
    vm.food_type = $stateParams.food_type;


    vm.default_serving = utility.getDefaultServingRange(vm.user_gender);
    $scope.diary_index.food_exercise_type = null;  

    vm.searchKeyFood = '';
    vm.food_tab_display = true;
    vm.log_date = $scope.diary_index.log_date;
    $('#log_water_tmplt').modal('hide');

    vm.recommendedFood_loader = false;
    vm.recommendedMeal_loader = false;
    
    vm.search_log = {};
    vm.recent_log = {};
    vm.freq_log =  {};
    vm.fav_log =   {};
    var favourite_food_ids = [];

    // initialize recommended food type object
    var recommended_Pobj_counter = 0;
    var recommended_Cobj_counter = 0;
    var recommended_Vobj_counter = 0;
    var recommended_Fobj_counter = 0;
    vm.prote_object =  [];
    vm.carb_object =  [];
    vm.veg_object =  [];
    vm.fat_object =  [];
    // end: initialize recommended food type object

    vm.is_selected = false;
    vm.selected_from = $stateParams.selected_from;
    
    vm.selectedfood = {};

    vm.selectedFoodTypeName = null;

    vm.selectLogFoodTab = function(id){
        
        vm.is_selected = false;
        
        switch(id){
        
          case "recent":
            vm.selectedFoodTypeName = "Recent";
            break;

            case "proteins_foods":
            vm.selectedFoodTypeName = "MD Protein";
            break;

            case "carb_foods":
            vm.selectedFoodTypeName = "MD Carbs";
            break;

            case "vegetable_foods":
            vm.selectedFoodTypeName = "MD Vegetable";
            break;

            case "frequent":
            vm.selectedFoodTypeName = "Most Logged";
            break;

            case "cust_tab":
            vm.foodTabToggel('cust_tab', 'cust_log_tab');
            break;

        }

        
      }

    if(vm.food_type){
      // focus of selected tab
      $anchorScroll.yOffset = -1;
      var old = $location.hash();
      $location.hash('');
      $anchorScroll();
      $location.hash(old);
      // end: focus of selected tab
    }
    if(vm.selected_from){
      //alert(vm.selected_from);
    
      // focus of selected tab
      $anchorScroll.yOffset = 300;
      var old = $location.hash();
      $location.hash('log_food_tab');
      $anchorScroll();
      $location.hash(old);
      // end: focus of selected tab

      vm.from_custom = false;
      var selectedfooddata = $stateParams.selected_food;
      
      if(vm.selected_from=="md_meals"){
        //vm.is_selected = false;
        //vm.is_tabs_selected = true;
        //console.log("vm.is_selected");
        //console.log(vm.is_selected);
        //alert("md meal");
        //$timeout(vm.foodTabToggel('md_meal_tab','md_recmeal_tab'), 3000);
        //angular.element('#md_meal_tab').trigger('click');

        //vm.recommended_meal_id = 9;
        
      }
      else if(vm.selected_from=="custom"){
          vm.is_food_selected = true;
          vm.is_selected = true;
          vm.from_custom = true;
          vm.selectedFoodTypeName = "Custom";
          vm.selectedfood.id = selectedfooddata.id;
          vm.selectedfood.foodname = selectedfooddata.name;
          vm.selectedfood.brand_name = selectedfooddata.brand_name;
          vm.selectedfood.serving_quantity = selectedfooddata.nutrition_data.serving_quantity;
          vm.selectedfood.serving_size_unit = selectedfooddata.nutrition_data.serving_size_unit;
          vm.selectedfood.calories = selectedfooddata.nutrition_data.calories;
          vm.selectedfood.nutrition_data = selectedfooddata.nutrition_data;
          vm.selectedfood.no_of_servings = 1;

          
      }
      else if(vm.selected_from=="favourite"){
        vm.is_food_selected = true;
          vm.is_selected = true;
          vm.selectedFoodTypeName = "Favourite";
          vm.selectedfood.id = selectedfooddata.nutrition_id;
          vm.selectedfood.foodname = selectedfooddata.nutritions.name;
          vm.selectedfood.brand_name = selectedfooddata.nutritions.brand_name;
          vm.selectedfood.serving_data = selectedfooddata.nutritions.serving_data;
          vm.selectedfood.serving_quantity = selectedfooddata.nutritions.nutrition_data.serving_quantity;
          vm.selectedfood.serving_size_unit = selectedfooddata.nutritions.nutrition_data.serving_size_unit;
          vm.selectedfood.nutrition_data = selectedfooddata.nutritions.nutrition_data;
          vm.selectedfood.calories = selectedfooddata.nutritions.nutrition_data.calories;
          vm.selectedfood.no_of_servings = 1;
      }
      else if(vm.selected_from=="meal"){
        vm.is_selected = true;
        vm.is_meal_selected = true;
        vm.selectedFoodTypeName = "My Meal";
        vm.mealFoodData = selectedfooddata;
      }
      else{
          vm.is_food_selected = true;
          vm.is_selected = true;
          
          switch(vm.selected_from){
            case "recent":
            vm.selectedFoodTypeName = "Recent";
            break;

            case "proteins_foods":
            vm.selectedFoodTypeName = "MD Protein";
            break;

            case "carb_foods":
            vm.selectedFoodTypeName = "MD Carbs";
            break;

            case "vegetable_foods":
            vm.selectedFoodTypeName = "MD Vegetable";
            break;

            case "frequent":
            vm.selectedFoodTypeName = "Most Logged";
            break;

            case "custom":
            vm.selectedFoodTypeName = "Custom";
            break;

          }

          vm.selectedfood.id = selectedfooddata.nutrition_id;
          vm.selectedfood.foodname = selectedfooddata.nutritions.name;
          vm.selectedfood.brand_name = selectedfooddata.nutritions.brand_name;
          vm.selectedfood.serving_data = selectedfooddata.nutritions.serving_data;
          vm.selectedfood.serving_quantity = selectedfooddata.serving_quantity;

          vm.selectedfood.serving_string = selectedfooddata.serving_string;
          vm.selectedfood.no_of_servings = selectedfooddata.no_of_servings;
          vm.selectedfood.schedule_time = selectedfooddata.schedule_time;
          vm.selectedfood.serving_size_unit = selectedfooddata.serving_size_unit;
          vm.selectedfood.nutrition_data = selectedfooddata.nutritions.nutrition_data;
          vm.selectedfood.calories = selectedfooddata.calories;
          
      }
    }

    vm.schedule_times =[{id:'meal_1', name:'Meal 1 (Breakfast)'},
                        {id:'meal_2', name:'Meal 2 (Snack)'},
                        {id:'meal_3', name:'Meal 3 (Lunch)'},
                        {id:'meal_4', name:'Meal 4 (Snack)'},
                        {id:'meal_5', name:'Meal 5 (Dinner)'},
                        {id:'meal_6', name:'Meal 6 (Snack - Optional)'},
                        {id:'pre_workout_meal', name:'Pre Workout Meal'},
                        {id:'post_workout_meal', name:'Post Workout Meal'},
                      ];
    vm.serving_ranges = utility.getServingRange(1,10,0.5);

    vm.searchFood = function(){
        vm.foodSearchdata = {};
        vm.is_selected=false;
    		vm.food_tab_display = false;
    		vm.foodTabToggel(null, 'searched_food_tab');
     		vm.search_result = false;
    		vm.search_loader = true;
    		var searchString = '';
    		searchString = vm.searchKeyFood;
        ShowService.search('search.php?keyword='+searchString+'&limit=100').then(function(result) {
        
           
            if(result.error == false){

               vm.foodSearchdata = result.response;
                if(result.response.length>0){

                  vm.search_loader = false;
                  vm.search_result = true;

                  for(var i=0; i<vm.foodSearchdata.length; i++){

                      var is_favourite = favourite_food_ids.indexOf(Number(vm.foodSearchdata[i]['id']));
                      if(is_favourite != -1){
                        vm.foodSearchdata[i]['is_favourite'] = 1;
                      }
                      else{
                        vm.foodSearchdata[i]['is_favourite'] = 0;
                      }
                  }


                }          

              vm.search_loader = false;
              vm.searchMenu = true;
            }
        },function(error) {
            console.log(error);
        });
    }

    //clear logFood search
    vm.clear_logFood_searchResult = function(){
        vm.searchKeyFood = '';
        vm.is_selected=false;
        vm.food_tab_display = true;
        vm.foodTabToggel("md_food_tab", 'md_recfood_tab');     
    }
    
    //end: clear logFood search
    
    // get favourite food
    vm.favouriteFood = function(){
        

        ShowService.get('api/v3/food/favourite_food',vm.userId).then(function(results){
            vm.favourite_nutritions = results.response.favourite_nutritions;

            for (var i=0; i<vm.favourite_nutritions.length; i++) {
             
              for (var k in vm.favourite_nutritions[i]) {
                  
                     var id_exist = favourite_food_ids.indexOf(vm.favourite_nutritions[i]['nutrition_id']);
                     if(id_exist == -1){
                        favourite_food_ids.push(Number(vm.favourite_nutritions[i]['nutrition_id']));
                     }
                     
              }
            }
        });
        
    }

    vm.allLoggedFood = function(){

        ShowService.get('api/v3/food/all_logged_food',vm.userId).then(function(results){
            vm.frequent_nutritions = results.response.frequent_nutritions;
            vm.recent_nutritions = results.response.recent_nutritions;
            vm.custom_nutritions = results.response.custom_nutritions;
            vm.meal_foods = results.response.meal_foods;
        });

    }
   
    // end: get favourite food

    vm.foodTabToggel = function(tab_id, tab_content_id){
          
        if(tab_id){
            $(".logfood_tab_continer #log_food_tab ul li").removeClass("active");
            $("#"+tab_id).addClass("active");
        }

        if(tab_content_id){
            $(".logfood_tab_details .tab-pane").removeClass("active");
            $("#"+tab_content_id).addClass("in active");
        }
        
    }

    vm.viewFoodDetail = function(food_obj, from,no_of_servings){
      if(food_obj){
        if(from == 'custom' || from == 'search' || from == 'selected')
        {
          vm.food_detail = vm.updateNutritionFacts(food_obj,no_of_servings);
          if(from == 'selected'){
              vm.food_detail.name = food_obj.foodname;
          }
        }
        else{
          vm.food_detail =  vm.updateNutritionFacts(food_obj.nutritions,no_of_servings);
        }

        $('#log_water_tmplt').modal('show');
      }
      else{
        alert("Select Food Properly");
      }        
    }

    vm.updateNutritionFacts = function(nutrtion_obj,no_of_serving){

        var copy_nutrition_obj = angular.copy(nutrtion_obj);

        var serving_range =  1;


        var default_protein = copy_nutrition_obj.nutrition_data.protein;
        var default_calories = copy_nutrition_obj.nutrition_data.calories;
        var default_calcium_dv = copy_nutrition_obj.nutrition_data.calcium_dv;
        var default_cholesterol = copy_nutrition_obj.nutrition_data.cholesterol;
        var default_dietary_fiber = copy_nutrition_obj.nutrition_data.dietary_fiber;
        var default_iron_dv = copy_nutrition_obj.nutrition_data.iron_dv;
        var default_monounsaturated = copy_nutrition_obj.nutrition_data.monounsaturated;
        var default_polyunsaturated = copy_nutrition_obj.nutrition_data.polyunsaturated;
        var default_potassium = copy_nutrition_obj.nutrition_data.potassium;
        var default_saturated_fat = copy_nutrition_obj.nutrition_data.saturated_fat;
        var default_sodium = copy_nutrition_obj.nutrition_data.sodium;
        var default_sugars = copy_nutrition_obj.nutrition_data.sugars;
        var default_total_carb = copy_nutrition_obj.nutrition_data.total_carb;
        var default_total_fat = copy_nutrition_obj.nutrition_data.total_fat;
        var default_trans_fat = copy_nutrition_obj.nutrition_data.trans_fat;
        var default_vitamin_a = copy_nutrition_obj.nutrition_data.vitamin_a;
        var default_vitamin_c = copy_nutrition_obj.nutrition_data.vitamin_c;


        var update_protine = parseFloat(default_protein)*Number(no_of_serving)*Number(serving_range);
        var update_calories = Number(default_calories)*Number(no_of_serving)*Number(serving_range);
        var update_calcium_dv = parseFloat(default_calcium_dv)*Number(no_of_serving)*Number(serving_range);
        var update_cholesterol = parseFloat(default_cholesterol)*Number(no_of_serving)*Number(serving_range);
        var update_dietary_fiber = parseFloat(default_dietary_fiber)*Number(no_of_serving)*Number(serving_range);
        var update_iron_dv = parseFloat(default_iron_dv)*Number(no_of_serving)*Number(serving_range);
        var update_monounsaturated = parseFloat(default_monounsaturated)*Number(no_of_serving)*Number(serving_range);
        var update_polyunsaturated = parseFloat(default_polyunsaturated)*Number(no_of_serving)*Number(serving_range);
        var update_potassium = parseFloat(default_potassium)*Number(no_of_serving)*Number(serving_range);
        var update_saturated_fat = parseFloat(default_saturated_fat)*Number(no_of_serving)*Number(serving_range);
        var update_sodium = parseFloat(default_sodium)*Number(no_of_serving)*Number(serving_range);
        var update_sugars = parseFloat(default_sugars)*Number(no_of_serving)*Number(serving_range);
        var update_total_carb = parseFloat(default_total_carb)*Number(no_of_serving)*Number(serving_range);
        var update_total_fat = parseFloat(default_total_fat)*Number(no_of_serving)*Number(serving_range);
        var update_trans_fat = parseFloat(default_trans_fat)*Number(no_of_serving)*Number(serving_range);
        var update_vitamin_a = parseFloat(default_vitamin_a)*Number(no_of_serving)*Number(serving_range);
        var update_vitamin_c = parseFloat(default_vitamin_c)*Number(no_of_serving)*Number(serving_range);




        copy_nutrition_obj.nutrition_data.calories = update_calories.toFixed(2);
        copy_nutrition_obj.nutrition_data.protein = update_protine +" g";
        copy_nutrition_obj.nutrition_data.calcium_dv = update_calcium_dv +"%";
        copy_nutrition_obj.nutrition_data.cholesterol = update_cholesterol +" mg";
        copy_nutrition_obj.nutrition_data.dietary_fiber = update_dietary_fiber +" g";
        copy_nutrition_obj.nutrition_data.iron_dv = update_iron_dv +"%";
        copy_nutrition_obj.nutrition_data.monounsaturated = update_monounsaturated +" g";
        copy_nutrition_obj.nutrition_data.polyunsaturated = update_polyunsaturated +" g";
        copy_nutrition_obj.nutrition_data.potassium = update_potassium +" mg";
        copy_nutrition_obj.nutrition_data.saturated_fat = update_saturated_fat +" g";
        copy_nutrition_obj.nutrition_data.sodium = update_sodium +" mg";
        copy_nutrition_obj.nutrition_data.sugars = update_sugars +" g";
        copy_nutrition_obj.nutrition_data.total_carb = update_total_carb +" g";
        copy_nutrition_obj.nutrition_data.total_fat = update_total_fat +" g";
        copy_nutrition_obj.nutrition_data.trans_fat = update_trans_fat +" g";
        copy_nutrition_obj.nutrition_data.vitamin_a = update_vitamin_a +"%";
        copy_nutrition_obj.nutrition_data.vitamin_c = update_vitamin_c +"%";

        // console.log("default_calories",default_calories);
        // console.log("update_calories",update_calories);
        // console.log("copy_nutrition_obj.nutrition_data.calories",copy_nutrition_obj.nutrition_data.calories);
        // console.log("no_of_servings",no_of_serving);
        return copy_nutrition_obj;
    }

    
    vm.logFood = function(log_food_obj,$index,tab_type){
        /*console.log(log_food_obj); 
        console.log(tab_type); 
        return false;*/
        var log_food_data ={};
        var logFoodTobePushed = {};
        var log_date = moment(vm.log_date).format('YYYY-MM-DD');
        log_food_data.user_id= vm.userId;
        log_food_data.serving_date= log_date;
        log_food_data.guid_server = 'server';

        if(tab_type == 'search'){
           log_food_data.nutrition_id= log_food_obj.id;
           log_food_data.calories=  vm.search_log[$index].calories; 
           log_food_data.serving_string = vm.search_log[$index].serving_string.label; 
           log_food_data.no_of_servings= vm.search_log[$index].no_of_servings;
           log_food_data.nutrition_id= log_food_obj.id;
           log_food_data.serving_size_unit = log_food_obj.nutrition_data.serving_size_unit;
           var serving_value = log_food_data.serving_string.replace(log_food_data.serving_size_unit,'');
           var serving_quantity =recursiveSearch(serving_value);
           log_food_data.serving_quantity = serving_quantity.toFixed(2);
           log_food_data.schedule_time =vm.search_log[$index].schedule_time;


           // set values to push to diary data page
           logFoodTobePushed.total_fat = log_food_obj.nutrition_data.total_fat;
           logFoodTobePushed.dietary_fiber = log_food_obj.nutrition_data.dietary_fiber;
           logFoodTobePushed.total_carb = log_food_obj.nutrition_data.total_carb;
           logFoodTobePushed.sodium = log_food_obj.nutrition_data.sodium;
           logFoodTobePushed.protein = log_food_obj.nutrition_data.protein;
           logFoodTobePushed.name = log_food_obj.name;
           
        }else if(tab_type == 'favourite'){
            log_food_data.nutrition_id= log_food_obj.nutrition_id;
            log_food_data.calories=  vm.fav_log[$index].calories; 
            log_food_data.serving_string = vm.fav_log[$index].serving_string.label; 
            log_food_data.no_of_servings= vm.fav_log[$index].no_of_servings;
            log_food_data.serving_size_unit = log_food_obj.nutritions.nutrition_data.serving_size_unit;
            var serving_value = log_food_data.serving_string.replace(log_food_data.serving_size_unit,'');
            var serving_quantity =recursiveSearch(serving_value);
            log_food_data.serving_quantity = serving_quantity.toFixed(2);
            log_food_data.schedule_time =vm.fav_log[$index].schedule_time;

            // set values to push to diary data page
           logFoodTobePushed.total_fat = log_food_obj.nutritions.nutrition_data.total_fat;
           logFoodTobePushed.dietary_fiber = log_food_obj.nutritions.nutrition_data.dietary_fiber;
           logFoodTobePushed.total_carb = log_food_obj.nutritions.nutrition_data.total_carb;
           logFoodTobePushed.sodium = log_food_obj.nutritions.nutrition_data.sodium;
           logFoodTobePushed.protein = log_food_obj.nutritions.nutrition_data.protein;
           logFoodTobePushed.name = log_food_obj.nutritions.name;


        }else if(tab_type == 'recent'){

           log_food_data.nutrition_id= log_food_obj.nutrition_id;
           log_food_data.calories=  vm.recent_log[$index].calories; 
           log_food_data.serving_string = vm.recent_log[$index].serving_string; 
           log_food_data.no_of_servings= vm.recent_log[$index].no_of_servings;
           log_food_data.serving_size_unit = log_food_obj.nutritions.nutrition_data.serving_size_unit;
           var serving_value = log_food_data.serving_string.replace(log_food_data.serving_size_unit,'');
           var serving_quantity =recursiveSearch(serving_value);
           log_food_data.serving_quantity = serving_quantity.toFixed(2);
            log_food_data.schedule_time =vm.recent_log[$index].schedule_time;


            // set values to push to diary data page
           logFoodTobePushed.total_fat = log_food_obj.nutritions.nutrition_data.total_fat;
           logFoodTobePushed.dietary_fiber = log_food_obj.nutritions.nutrition_data.dietary_fiber;
           logFoodTobePushed.total_carb = log_food_obj.nutritions.nutrition_data.total_carb;
           logFoodTobePushed.sodium = log_food_obj.nutritions.nutrition_data.sodium;
           logFoodTobePushed.protein = log_food_obj.nutritions.nutrition_data.protein;
           logFoodTobePushed.name = log_food_obj.nutritions.name;


        }else if(tab_type == 'frequent'){

           log_food_data.nutrition_id= log_food_obj.nutrition_id;
           log_food_data.calories=  vm.freq_log[$index].calories; 
           log_food_data.serving_string = vm.freq_log[$index].serving_string; 
           log_food_data.no_of_servings= vm.freq_log[$index].no_of_servings;
           log_food_data.serving_size_unit = log_food_obj.nutritions.nutrition_data.serving_size_unit;
           var serving_value = log_food_data.serving_string.replace(log_food_data.serving_size_unit,'');
           var serving_quantity =recursiveSearch(serving_value);
           log_food_data.serving_quantity = serving_quantity.toFixed(2);
            log_food_data.schedule_time =vm.freq_log[$index].schedule_time;

            // set values to push to diary data page
           logFoodTobePushed.total_fat = log_food_obj.nutritions.nutrition_data.total_fat;
           logFoodTobePushed.dietary_fiber = log_food_obj.nutritions.nutrition_data.dietary_fiber;
           logFoodTobePushed.total_carb = log_food_obj.nutritions.nutrition_data.total_carb;
           logFoodTobePushed.sodium = log_food_obj.nutritions.nutrition_data.sodium;
           logFoodTobePushed.protein = log_food_obj.nutritions.nutrition_data.protein;
           logFoodTobePushed.name = log_food_obj.nutritions.name;

        }else if(tab_type == 'custom'){

           var num_of_serving = vm.custom_log[$index].no_of_servings;
           log_food_data.nutrition_id= log_food_obj.id;
           log_food_data.calories=  Number(log_food_obj.nutrition_data.calories) * Number(num_of_serving); 
           
           var serving_quantity = log_food_obj.nutrition_data.serving_quantity;
           
           log_food_data.serving_quantity = serving_quantity;

           var serving_size_unit = log_food_obj.nutrition_data.serving_size_unit;
           log_food_data.serving_size_unit = serving_size_unit;
           log_food_data.no_of_servings= num_of_serving;
           log_food_data.serving_string = serving_quantity+" "+serving_size_unit; 

           log_food_data.schedule_time =vm.custom_log[$index].schedule_time;
           log_food_data.is_custom = 1;

           // set values to push to diary data page
           logFoodTobePushed.total_fat = log_food_obj.nutrition_data.total_fat;
           logFoodTobePushed.dietary_fiber = log_food_obj.nutrition_data.dietary_fiber;
           logFoodTobePushed.total_carb = log_food_obj.nutrition_data.total_carb;
           logFoodTobePushed.sodium = log_food_obj.nutrition_data.sodium;
           logFoodTobePushed.protein = log_food_obj.nutrition_data.protein;
           logFoodTobePushed.name = log_food_obj.name;

        }else if(tab_type == 'selected'){

            log_food_data.nutrition_id= log_food_obj.id;
            if(vm.selected_from =="custom"){
              log_food_data.calories=  Number(log_food_obj.calories) * Number(vm.selected_log.no_of_servings); 
              log_food_data.is_custom = 1;
               var serving_quantity = log_food_obj.nutrition_data.serving_quantity;
               var serving_size_unit = log_food_obj.nutrition_data.serving_size_unit;
               log_food_data.serving_string = serving_quantity+" "+serving_size_unit; 
             }else{
               log_food_data.calories= vm.selected_log.calories; 
             }

             if(vm.selected_from =="recent" || vm.selected_from =="frequent" ||

                vm.selected_from =="proteins_foods" || vm.selected_from =="carb_foods"
                || vm.selected_from =="healthy_fats_food" || vm.selected_from =="vegetable_foods" ){

              log_food_data.serving_string = vm.selected_log.serving_string;
              var serving_size_unit = log_food_obj.serving_size_unit;
              var serving_value = log_food_data.serving_string.replace(log_food_obj.serving_size_unit,'');
              var serving_quantity =recursiveSearch(serving_value);
              serving_quantity = serving_quantity.toFixed(2);
             }

             if(vm.selected_from =="favourite" ){
              log_food_data.serving_string = vm.selected_log.serving_string.label;
              var serving_size_unit = log_food_obj.nutrition_data.serving_size_unit;
              var serving_value = log_food_data.serving_string.replace(serving_size_unit,'');
              var serving_quantity =recursiveSearch(serving_value);
              serving_quantity = serving_quantity.toFixed(2);
            }
 
                     
            log_food_data.serving_quantity = serving_quantity;
            log_food_data.serving_size_unit = serving_size_unit;
            log_food_data.no_of_servings=  vm.selected_log.no_of_servings;
           
            log_food_data.schedule_time =  vm.selected_log.schedule_time;

            // set values to push to diary data page
            logFoodTobePushed.total_fat = log_food_obj.nutrition_data.total_fat;
            logFoodTobePushed.dietary_fiber = log_food_obj.nutrition_data.dietary_fiber;
            logFoodTobePushed.total_carb = log_food_obj.nutrition_data.total_carb;
            logFoodTobePushed.sodium = log_food_obj.nutrition_data.sodium;
            logFoodTobePushed.protein = log_food_obj.nutrition_data.protein;
            logFoodTobePushed.name = log_food_obj.foodname;
            
        }
        else if(tab_type == 'recommended_food'){
          

           log_food_data.calories=  vm.recommended_food[$index].calories; 
          
           log_food_data.serving_string = vm.recommended_food[$index].serving_string; 
           log_food_data.no_of_servings= vm.recommended_food[$index].no_of_servings;
           log_food_data.nutrition_id= log_food_obj.nutrition_id;
           log_food_data.serving_size_unit = log_food_obj.nutritions.nutrition_data.serving_size_unit;
           var serving_value = log_food_data.serving_string.replace(log_food_data.serving_size_unit,'');
           var serving_quantity =recursiveSearch(serving_value);
           log_food_data.serving_quantity = serving_quantity.toFixed(2);
           log_food_data.schedule_time =vm.recommended_food[$index].schedule_time;

        }

        if(log_food_data.is_custom){
          var is_custom = 1;
        }
        else{
          var is_custom = 0;
        }
     
        StoreService.save('api/v3/food/log_food',log_food_data).then(function(result) {
          if(result.error == false){  
                var foodObjTemp = vm.makeFoodObjTobePushed(logFoodTobePushed, log_food_data, is_custom);

                foodObjTemp.id = result.response.log_food.id;
                
                switch(log_food_data.schedule_time){
                  case "meal_1":
                  $scope.diary_index.meal_one_food.push(foodObjTemp);
                  $scope.diary_index.meal_one_total.calorie = Number($scope.diary_index.meal_one_total.calorie) + Number(foodObjTemp.calories);
                  $scope.diary_index.foodSummary.total_consumed = Number($scope.diary_index.foodSummary.total_consumed) + Number(foodObjTemp.calories);
                  break;

                  case "meal_2":
                  $scope.diary_index.meal_two_food.push(foodObjTemp);
                  $scope.diary_index.meal_two_total.calorie = Number($scope.diary_index.meal_two_total.calorie) + Number(foodObjTemp.calories);
                  $scope.diary_index.foodSummary.total_consumed = Number($scope.diary_index.foodSummary.total_consumed) + Number(foodObjTemp.calories);
                  break;

                  case "meal_3":
                  $scope.diary_index.meal_three_food.push(foodObjTemp);
                  $scope.diary_index.meal_three_total.calorie = Number($scope.diary_index.meal_three_total.calorie) + Number(foodObjTemp.calories);
                  $scope.diary_index.foodSummary.total_consumed = Number($scope.diary_index.foodSummary.total_consumed) + Number(foodObjTemp.calories);
                  break;

                  case "meal_4":
                  $scope.diary_index.meal_four_food.push(foodObjTemp);
                  $scope.diary_index.meal_four_total.calorie = Number($scope.diary_index.meal_four_total.calorie) + Number(foodObjTemp.calories);
                  $scope.diary_index.foodSummary.total_consumed = Number($scope.diary_index.foodSummary.total_consumed) + Number(foodObjTemp.calories);
                  break;

                  case "meal_5":
                  $scope.diary_index.meal_five_food.push(foodObjTemp);
                  $scope.diary_index.meal_five_total.calorie = Number($scope.diary_index.meal_five_total.calorie) + Number(foodObjTemp.calories);
                  $scope.diary_index.foodSummary.total_consumed = Number($scope.diary_index.foodSummary.total_consumed) + Number(foodObjTemp.calories);
                  break;

                  case "meal_6":
                  $scope.diary_index.meal_six_food.push(foodObjTemp);
                  $scope.diary_index.meal_six_total.calorie = Number($scope.diary_index.meal_six_total.calorie) + Number(foodObjTemp.calories);
                  $scope.diary_index.foodSummary.total_consumed = Number($scope.diary_index.foodSummary.total_consumed) + Number(foodObjTemp.calories);
                  break;

                  case "pre_workout_meal":
                  $scope.diary_index.pre_workout_food.push(foodObjTemp);
                  $scope.diary_index.pre_workout_total.calorie = Number($scope.diary_index.pre_workout_total.calorie) + Number(foodObjTemp.calories);
                  $scope.diary_index.foodSummary.total_consumed = Number($scope.diary_index.foodSummary.total_consumed) + Number(foodObjTemp.calories);
                  break;

                  case "post_workout_meal":
                    $scope.diary_index.post_workout_food.push(foodObjTemp);
                    $scope.diary_index.post_workout_total.calorie = Number($scope.diary_index.post_workout_total.calorie) + Number(foodObjTemp.calories);
                    $scope.diary_index.foodSummary.total_consumed = Number($scope.diary_index.foodSummary.total_consumed) + Number(foodObjTemp.calories);
                    break;

                }             
                
                $scope.diary_index.food_exercise_type = vm.food_type;
                $state.go('diary');
          }
        },function(error) {
          console.log(error);
        });

    }

    vm.makeFoodObjTobePushed = function(logFoodTobePushed, logged_food_data, is_custom){
      
      var fat = logFoodTobePushed.total_fat;
      var fat_arr = fat.split(' ');
      fat = Number(fat_arr[0].replace(',',''));

      var fiber = logFoodTobePushed.dietary_fiber;
      var fiber_arr = fiber.split(' ');
      fiber = Number(fiber_arr[0].replace(',',''));

      var carb = logFoodTobePushed.total_carb;
      var carb_arr = carb.split(' ');
      carb = Number(carb_arr[0].replace(',',''));

      var sodium = logFoodTobePushed.sodium;
      var sodium_arr = sodium.split(' ');
      sodium = Number(sodium_arr[0].replace(',',''));

      var protein = logFoodTobePushed.protein;
      var protein_arr = protein.split(' ');
      protein = Number(protein_arr[0].replace(',',''));

      var foodObj = {};

      foodObj.is_custom = is_custom;
      foodObj.name = logFoodTobePushed.name;


      foodObj.calories = logged_food_data.calories;
      foodObj.carb = carb * Number(logged_food_data.no_of_servings);
      foodObj.fat = fat * Number(logged_food_data.no_of_servings);
      foodObj.fiber = fiber * Number(logged_food_data.no_of_servings);
      foodObj.protein = protein * Number(logged_food_data.no_of_servings);
      foodObj.sodium = sodium * Number(logged_food_data.no_of_servings);
      foodObj.food_type = logged_food_data.schedule_time;      
      foodObj.no_of_servings = logged_food_data.no_of_servings;
      foodObj.nutrition_id = logged_food_data.nutrition_id;
      foodObj.serving_quantity = logged_food_data.serving_quantity;
      foodObj.serving_size_unit = logged_food_data.serving_size_unit;
      foodObj.user_id = logged_food_data.user_id;

      return foodObj;
    }

    vm.calculateServingSize = function($index,nutrition_obj,tab_type){
        
        if(tab_type == 'search'){
          var serving_size_obj = vm.search_log[$index].serving_string;
          var no_of_serving =  vm.search_log[$index].no_of_servings;
           var serving_label = serving_size_obj.label;
           var serving_unit = serving_size_obj.unit;
           var serving_quantity = nutrition_obj.nutrition_data.serving_quantity;
           var calories = nutrition_obj.nutrition_data.calories; 
           var no_of_serving_master = 1;   
        }else if(tab_type == 'favorite'){
            var serving_size_obj = vm.fav_log[$index].serving_string;
            var no_of_serving =  vm.fav_log[$index].no_of_servings;
            var serving_label = serving_size_obj.label;
            var serving_unit = serving_size_obj.unit;
            var serving_quantity = nutrition_obj.nutritions.nutrition_data.serving_quantity;  
            var calories = nutrition_obj.nutritions.nutrition_data.calories; 
            var no_of_serving_master = 1; 
        }else if(tab_type == 'recent'){

            var no_of_serving =  vm.recent_log[$index].no_of_servings;
            var serving_label = vm.recent_log[$index].serving_string;
            var serving_unit =  nutrition_obj.nutritions.nutrition_data.serving_size_unit;
            var serving_quantity = nutrition_obj.serving_quantity;
            var calories = nutrition_obj.calories; 
            var no_of_serving_master = nutrition_obj.no_of_servings; 
        }else if(tab_type == 'ferquent'){

            var no_of_serving =  vm.freq_log[$index].no_of_servings;
            var serving_label = vm.freq_log[$index].serving_string;
            var serving_unit =  nutrition_obj.nutritions.nutrition_data.serving_size_unit;
            var serving_quantity = nutrition_obj.serving_quantity;
            var calories = nutrition_obj.calories; 
            var no_of_serving_master = nutrition_obj.no_of_servings;
        }else if(tab_type == 'selected_favorite'){
            
            var no_of_serving =  vm.selected_log.no_of_servings;
            var serving_label = vm.selected_log.serving_string.label;
            var serving_unit =  nutrition_obj.serving_size_unit;
            var serving_quantity = nutrition_obj.serving_quantity;
            var calories = nutrition_obj.calories; 
            var no_of_serving_master = 1;
        }else if(tab_type == 'selected_rec_freq'){
         
            var no_of_serving =  vm.selected_log.no_of_servings;
            var serving_label = vm.selected_log.serving_string;
            var serving_unit =  nutrition_obj.serving_size_unit;
            var serving_quantity = nutrition_obj.serving_quantity;
            var calories = nutrition_obj.calories; 
            var no_of_serving_master = nutrition_obj.no_of_servings;
        }else if(tab_type == 'recommended_food'){
            
            var serving_label = vm.recommended_food[$index].serving_string;
          
            var no_of_serving =  vm.recommended_food[$index].no_of_servings;

            var serving_quantity = nutrition_obj.serving_quantity;
            var serving_unit =  nutrition_obj.nutritions.nutrition_data.serving_size_unit;

            var calories = nutrition_obj.calories; 
            var no_of_serving_master = nutrition_obj.no_of_servings;
        }
       
       
        var serving_value = serving_label.replace(serving_unit,'');
         
        var serving_size = recursiveSearch(serving_value);
         
         if(nutrition_obj.hasOwnProperty("nutrition_data")){
            var nutrition_data = nutrition_obj.nutrition_data;
         }else{
             var nutrition_data = nutrition_obj.nutritions.nutrition_data;
         }
        var update_calories = calculateCalories(no_of_serving,calories,serving_size,serving_quantity, no_of_serving_master);
        if(tab_type == 'search'){
           vm.search_log[$index].calories =update_calories.toFixed(2);
        }
        if(tab_type == 'favorite'){
           vm.fav_log[$index].calories =update_calories.toFixed(2);
        }

        if(tab_type == 'recent'){
           vm.recent_log[$index].calories =update_calories.toFixed(2);
        }

        if(tab_type == 'ferquent'){
           vm.freq_log[$index].calories =update_calories.toFixed(2);
        }  

        if(tab_type == 'selected_favorite' || tab_type == 'selected_rec_freq'){
           vm.selected_log.calories =update_calories.toFixed(2);
        } 

        if(tab_type == 'recommended_food'){
           vm.recommended_food[$index].calories =update_calories.toFixed(2);
        }
    }



    function recursiveSearch(serving_value){

       //  var serving_quantity=0;
         if (serving_value.toLowerCase().indexOf("container") >= 0) {
            
            var arr = serving_value.split("(");
            if (arr.length > 1) {
                var innerString = arr[1].trim();
                if (innerString.indexOf("/") >= 0) {
                    return recursiveSearch(innerString);
                }else{
                    var space_arr = innerString.split(" ");
                   return recursiveSearch(space_arr[0]);
                }
            }
        }else if (serving_value.indexOf("/") >= 0) {
            
            var arr = serving_value.split("/")
            if (arr.length > 1) {
                var innerString = arr[0].trim();
                var arrInnerCompo = innerString.split(" ");
                if (arrInnerCompo.length == 2) {
                    var firstString = arrInnerCompo[0].trim();
                    var secondString = arrInnerCompo[1].trim();
                    var arr1FirstComponent= arr[1].trim();
                    var thirdString = Number(arr1FirstComponent.split(" ")[0]);
                    //console.log("firstString",firstString);
                    // console.log("secondString",secondString);
                    // console.log("thirdString",thirdString);
                    var dividevalue = Number(firstString) + Number(secondString) / Number(thirdString) ; 
                     //console.log("dividevalue",dividevalue);
                   return recursiveSearch(dividevalue.toString());

                }else{

                     var arrInner = innerString.split(" ");
                     var firstString = Number(arrInner[arrInner.length-1]);
                     var secondStringArr= arr[1].trim();
                     var secondString = Number(secondStringArr.split(" ")[0]);
                     var thirdString = firstString/secondString;
                     return recursiveSearch(thirdString.toString());

                }
                
            }
            
        }else{

            var final_value = serving_value.trim();
            var splitString = final_value.split(" ")[0]
            return Number(splitString);
        }
      
    }

    function calculateCalories(no_of_servings,calorie,serving_size,serving_quantity, no_of_serving_master){
      
        var num_of_serving = Number(no_of_servings/no_of_serving_master);
        var default_calories = calorie;
        var serving_size_value = Number(serving_size/serving_quantity);
        var update_calories = Number(default_calories)*Number(num_of_serving)*Number(serving_size_value);
        return update_calories;

    }

    if(vm.selected_from){

      if(vm.selected_from == "meal"){

        vm.is_meal_selected = true;
        vm.foodTabToggel('sel_meal_tab', 'selected_meal_tab');
      }
      else{
        vm.is_selected = true;
        vm.foodTabToggel('sel_tab', 'selected_food_tab');
      }
      
    }

    vm.addToFavourite = function(food_obj, added_from){
      var log_food_data = {};
      log_food_data.user_id = vm.userId;
      
      log_food_data.assigned_by = "patient";
      if(added_from == 'custom'){
        log_food_data.is_custom = 1;
        log_food_data.nutrition_id = food_obj.id;
      }
      else if(added_from == 'search'){
        log_food_data.is_custom = 0;
        log_food_data.nutrition_id = food_obj.id;
      }
      else if(added_from == 'recent' || added_from == 'frequent'){
        log_food_data.is_custom = food_obj.is_custom;
        log_food_data.nutrition_id = food_obj.nutrition_id;
      }
      else if(added_from == 'recommended_food'){
        log_food_data.is_custom = 0;
        log_food_data.nutrition_id = food_obj.nutrition_id;
      }
      
      log_food_data.guid_server = true;

      StoreService.save('api/v3/food/log_fav_food',log_food_data).then(function(result) {
          if(result.error == false){

              if(added_from == 'custom'){
                  var index = vm.custom_nutritions.indexOf(food_obj);
                  vm.custom_nutritions[index]['is_favourite'] = 1;

                  var id_exist = favourite_food_ids.indexOf(food_obj.id);
                  
              }
              else if(added_from == 'search'){
                  var index = vm.foodSearchdata.indexOf(food_obj);
                  vm.foodSearchdata[index]['is_favourite'] = 1;

                  var id_exist = favourite_food_ids.indexOf(food_obj.id);
              }
              else if(added_from == 'recent'){
                var index = vm.recent_nutritions.indexOf(food_obj);
                vm.recent_nutritions[index]['is_favourite'] = 1;

                var id_exist = favourite_food_ids.indexOf(food_obj.nutrition_id);
              }
              else if(added_from == 'frequent'){
                var index = vm.frequent_nutritions.indexOf(food_obj);
                vm.frequent_nutritions[index]['is_favourite'] = 1;

                var id_exist = favourite_food_ids.indexOf(food_obj.nutrition_id);
              }
              else if(added_from == 'recommended_food'){
                var index = vm.recommended_foods.indexOf(food_obj);
                vm.recommended_foods[index]['is_favourite'] = 1;

                var id_exist = favourite_food_ids.indexOf(food_obj.nutrition_id);
              }

              
              if(id_exist == -1){
                favourite_food_ids.push(food_obj.id);
              }

              vm.favouriteFood();
                  
          }
        },function(error) {
          console.log(error);
      });
    }

    vm.removeFromFavourite = function(food_obj, from){
      
      if(from == 'favourite' || from == 'recent' || from == 'frequent' || from=='recommended_food'){
          var nutrition_id = food_obj.nutrition_id;
      }
      else if(from == 'search' || from == 'custom'){
          var nutrition_id = food_obj.id;
      }
      
      if(nutrition_id){
        ShowService.remove('api/v3/food/delete_favourite_food_by_foodid', nutrition_id+'?user_id='+vm.userId).then(function(results){

            if(results.error == false){

                  if(from == 'search'){
                    var index = vm.foodSearchdata.indexOf(food_obj);
                    vm.foodSearchdata[index]['is_favourite'] = 0;
                    var id_exist = favourite_food_ids.indexOf(food_obj.id);

                  } 
                  else if(from == 'custom'){
                    var index = vm.custom_nutritions.indexOf(food_obj);
                    vm.custom_nutritions[index]['is_favourite'] = 0;
                    var id_exist = favourite_food_ids.indexOf(food_obj.id);

                  }
                  else if(from == 'recent'){
                      var index = vm.recent_nutritions.indexOf(food_obj);
                      vm.recent_nutritions[index]['is_favourite'] = 0;
                      var id_exist = favourite_food_ids.indexOf(food_obj.nutrition_id);
                  }     
                  else if(from == 'frequent'){
                      var index = vm.frequent_nutritions.indexOf(food_obj);
                      vm.frequent_nutritions[index]['is_favourite'] = 0;
                      var id_exist = favourite_food_ids.indexOf(food_obj.nutrition_id);
                  }               
                  else if(from == 'recommended_food'){
                      var index = vm.recommended_foods.indexOf(food_obj);
                      vm.recommended_foods[index]['is_favourite'] = 0;
                      var id_exist = favourite_food_ids.indexOf(food_obj.nutrition_id);
                  }
                  
                  if(id_exist != -1){
                    favourite_food_ids.splice(id_exist, 1);
                  }

                  vm.favouriteFood();

            }
            
        });
      }
      
    }

    vm.logMeal = function(log_meal_obj,$index){

        var meal_data ={};
        var log_date = moment(vm.log_date).format('YYYY-MM-DD');
        meal_data.user_id= vm.userId;
        meal_data.serving_date= log_date;
        meal_data.guid_server = 'server';
        meal_data.schedule_time = vm.meal_log[$index].schedule_time;

        var foods = [];
        
        angular.forEach(log_meal_obj.meal_foods, function(value, key) {

                  foods.push({
                      "id":value.id
                    });
      
        });
        
        meal_data.user_meal_foods = foods;

        StoreService.save('api/v3/food/log_meal', meal_data).then(function(result) {
          if(result.error == false){
                
                vm.pushRecommendedLoggedFoods(result.response.disply_inserted_foods);
                $state.go('diary');
                $scope.diary_index.food_exercise_type = vm.food_type;


          }
        },function(error) {
          console.log(error);
        });
    }

    vm.mealDetail = function(meal_obj){

        vm.meal_name = meal_obj.meal_name;
        vm.total_calories = meal_obj.total_calories;
        var total_fat = 0;
        var total_fiber = 0;
        var total_carb = 0;
        var total_sodium = 0;
        var total_protein = 0;

        vm.meal_food = [];

        angular.forEach(meal_obj.meal_foods, function(value, key){

            var name = value.nutritions.name;
            var calories = value.nutritions.nutrition_data.calories;

            var fat = value.nutritions.nutrition_data.total_fat;
            var fat_arr = fat.split(' ');
            fat = Number(fat_arr[0].replace(',',''));
            total_fat = total_fat + fat;

            var fiber = value.nutritions.nutrition_data.dietary_fiber;
            var fiber_arr = fiber.split(' ');
            fiber = Number(fiber_arr[0].replace(',',''));
            total_fiber = total_fiber + fiber;

            var carb = value.nutritions.nutrition_data.total_carb;
            var carb_arr = carb.split(' ');
            carb = Number(carb_arr[0].replace(',',''));
            total_carb = total_carb + carb;

            var sodium = value.nutritions.nutrition_data.sodium;
            var sodium_arr = sodium.split(' ');
            sodium = Number(sodium_arr[0].replace(',',''));
            total_sodium = total_sodium + sodium;

            var protein = value.nutritions.nutrition_data.protein;
            var protein_arr = protein.split(' ');
            protein = Number(protein_arr[0].replace(',',''));
            total_protein = total_protein + protein;

            vm.meal_food.push(
              {
                  "name": name,
                  "calories": calories,
                  "fat": fat,
                  "fiber": fiber,
                  "carb": carb,
                  "sodium": sodium,
                  "protein": protein
              }
            );
            
            vm.total_fat = total_fat;
            vm.total_fiber = total_fiber;
            vm.total_carb = total_carb;
            vm.total_sodium = total_sodium;
            vm.total_protein = total_protein;
        });

        $("#meal_detail_tmplt").modal('show');

    }

    vm.getFoodRecommendedTypes = function(){

        ShowService.query('api/v3/recommended_food/types').then(function(result){
            vm.recommended_types = result.response;
        });
    }


    vm.getMdRecommendedFoods = function(){
        vm.recommended_foods = [];
        vm.recommendedFood_loader = true;
        ShowService.query('api/v3/recommended_food/all_types_foods/'+vm.userId).then(function(result){

            if(result.error==false){
              vm.recommended_foods = result.response;

              angular.forEach(vm.recommended_foods, function(food_types, key){
        
                  if(key==0 && food_types.recommended_foods.length > 0){
                      vm.prote_object.push(
                          {
                            name: "test " + recommended_Pobj_counter++
                          }
                        );
                  }


                  if(key==1  && food_types.recommended_foods.length > 0){
                      vm.carb_object.push(
                        {
                          name: "test " + recommended_Cobj_counter++
                        }
                      );
                  }

                  if(key==2  && food_types.recommended_foods.length > 0){
                    vm.veg_object.push(
                      {
                        name: "test " + recommended_Vobj_counter++
                      }
                    );
                  }

                  if(key==3  && food_types.recommended_foods.length > 0){
                      vm.fat_object.push(
                        {
                          name: "test " + recommended_Fobj_counter++
                        }
                      ); 
                  }               
              });
            }
            
            vm.recommendedFood_loader = false;
        });
    }

    // Add food row to md recommended food tab section
    

    vm.addField = function(key) {
      switch(key){
        case 0:
          vm.prote_object.push({
            name: "test " + recommended_Pobj_counter++
          });
        break;

        case 1:
          vm.carb_object.push({
            name: "test " + recommended_Cobj_counter++
          });
        break;

        case 2:
          vm.veg_object.push({
            name: "test " + recommended_Vobj_counter++
          });
        break;

        case 3:
          vm.fat_object.push({
            name: "test " + recommended_Fobj_counter++
          });
        break;
      }

    }

    vm.removeField = function(key1, key2, obj){
     
        if(vm.recommended_food[key1]){
          vm.recommended_food[key1][key2] = {};
        } 
        
        switch(key1){
          case 0:          
          var index = vm.prote_object.indexOf(obj);
          vm.prote_object.splice(index, 1);
          break;

          case 1:
          var index = vm.carb_object.indexOf(obj);
          vm.carb_object.splice(index, 1);
          break;

          case 2:
          var index = vm.veg_object.indexOf(obj);
          vm.veg_object.splice(index, 1);
          break;

          case 3:
          var index = vm.fat_object.indexOf(obj);
          vm.fat_object.splice(index, 1);
          break;
        }
        
    }
    // Add food row to md recommended food tab section

    vm.selectServingSize = function(recom_food_id, key1, key2){

      if(recom_food_id){
          var recommended_foods = vm.recommended_foods[key1].recommended_foods;

          var keepGoing = true;
          angular.forEach(recommended_foods, function(values, key){
              if(keepGoing == true && values.id == recom_food_id){
                  vm.recommended_food[key1][key2].serving_size_obj = values.nutritions.serving_data;
                  vm.recommended_food[key1][key2].serving_string = values.serving_string;
                  vm.recommended_food[key1][key2].no_of_servings = values.no_of_servings;
                  vm.recommended_food[key1][key2].calories = values.calories;
                  vm.recommended_food[key1][key2].fooddata = values;
                  keepGoing = false;
              }
          });
      }    
      else{
          vm.recommended_food[key1][key2].serving_size_obj = {};
          vm.recommended_food[key1][key2].no_of_servings = null;
          vm.recommended_food[key1][key2].calories = null;
      } 
     
    }

    vm.getMealsByUser = function(){
      vm.recommended_meals = [];
      ShowService.get('api/v3/recommended_meal/meals_user', vm.userId).then(function(result){

            if(result.error==false){
                if(result.response.length > 0){
                    vm.recommended_meals = result.response;
                    vm.recommended_meal_id = vm.recommended_meals[0].id;
                    vm.getRecommendedMealFoods();
                }

            }
            
        });
    }

    vm.getRecommendedMealFoods = function(){
      vm.recommendedMeal_loader = true;
      //vm.recommended_meal_foods = [];
      vm.recommended_meal_foods_types = [];

      ShowService.get('api/v3/recommended_meal/foods', vm.recommended_meal_id+'?user_id='+vm.userId).then(function(result){

            if(result.error==false){
              vm.recommended_meal_foods_types = result.response;
           
              var selectedfoods = [];
              var selectedfoods_custom_index = 0;
              angular.forEach(vm.recommended_meal_foods_types, function(recommended_meal_foods, key1){
                  
                  angular.forEach(recommended_meal_foods.item_foods, function(recommended_meal_food, key2){
                    
                    var no_of_servings = Number(recommended_meal_food.no_of_servings);
                    var food_object = vm.MealFoodObjectFilter( recommended_meal_food, no_of_servings );
                    food_object.calories = recommended_meal_food.calories;
                    food_object.no_of_servings = no_of_servings;
                    food_object.nutrition_id = recommended_meal_food.nutrition_id;
                    food_object.serving_quantity = recommended_meal_food.serving_quantity;
                    food_object.serving_size_unit = recommended_meal_food.serving_size_unit;
                    food_object.serving_string = recommended_meal_food.serving_string;
                    selectedfoods.push(food_object);

                    vm.recommended_meal_foods_types[key1].item_foods[key2].custom_index = selectedfoods_custom_index;
                    selectedfoods_custom_index++;
                  });
                  
              });
  
              vm.selectedfoods = selectedfoods;

              vm.calculationRecomMealFoods();
            }

            vm.recommendedMeal_loader = false;
            
        });
    }

    vm.toggleMealFoodSelection = function(food_obj, key1, key2, custom_index, food_checked){

        if(food_checked==false){
          vm.selectedfoods[custom_index] = null;
        }
        else{
          var no_of_servings = Number(vm.recommended_meal_food[key1][key2].no_of_servings);
              
          var food_object = vm.MealFoodObjectFilter( food_obj, no_of_servings );

          food_object.calories = Number(vm.recommended_meal_food[key1][key2].calories);

          food_object.no_of_servings = no_of_servings;
          food_object.nutrition_id = food_obj.nutrition_id;
          
          var serving_string = vm.recommended_meal_food[key1][key2].serving_string;
          food_object.serving_string = serving_string;

          var serving_string_arr  = serving_string.split(" ");
          food_object.serving_quantity = recursiveSearch(serving_string_arr[0]);
          food_object.serving_size_unit = food_obj.serving_size_unit;

          vm.selectedfoods[custom_index] = food_object;  
      
        }

        vm.calculationRecomMealFoods();
    }

    vm.calculationRecomMealFoods = function(){

      var total_calories = 0;
      var total_fat = 0;
      var total_fibar = 0;
      var total_carbs = 0;
      var total_sodium = 0;
      var total_protein = 0;

      angular.forEach(vm.selectedfoods, function(value, key){

        if(value!=null){

            total_calories = total_calories + Number(value.calories);
            total_fat = total_fat + Number(value.fat);
            total_fibar = total_fibar + Number(value.fiber);
            total_carbs = total_carbs + Number(value.carb);
            total_sodium = total_sodium + Number(value.sodium);
            total_protein = total_protein + Number(value.protein);         
          }
      });

      vm.total_meal_food_calorie = total_calories;
      vm.total_meal_food_fat = total_fat;
      vm.total_meal_food_fibar = total_fibar;
      vm.total_meal_food_carbs = total_carbs;
      vm.total_meal_food_sodium = total_sodium;
      vm.total_meal_food_protein = total_protein;
    }

    vm.logRecommendedMeal = function(){
      
      var schedule_time = vm.recommended_meal.schedule_time;

      var log_date = moment(vm.log_date).format('YYYY-MM-DD');
 
      var selected_food = [];
      selected_food['user_agent'] = "server";
      
      angular.forEach(vm.selectedfoods, function(value, key){
        if(value!=null){

            var food_data = {};
            food_data.user_id = vm.userId;
            food_data.nutrition_id = value.nutrition_id;
            food_data.calories = value.calories;
            food_data.serving_quantity = value.serving_quantity;
            food_data.serving_size_unit = value.serving_size_unit;
            food_data.no_of_servings = value.no_of_servings;
            food_data.serving_string = value.serving_string;
            food_data.is_custom = 0;
            food_data.schedule_time = schedule_time;
            food_data.serving_date = log_date;
            food_data.user_agent = "server";

            selected_food.push(food_data);
        }
      });
      
      StoreService.save('api/v3/food/log_recommended_foods', selected_food).then(function(result) {
          if(result.error == false){
                 
                vm.pushRecommendedLoggedFoods(result.response.disply_inserted_foods);
                $state.go('diary');
                $scope.diary_index.food_exercise_type = vm.food_type;

          }
        },function(error) {
          console.log(error);
        });
    }

    
    vm.logRecommendedFoods = function(){
      
      var schedule_time = vm.recommended_food.schedule_time;
      
      var log_date = moment(vm.log_date).format('YYYY-MM-DD');

      var selected_recommended_food = [];
      angular.forEach(vm.recommended_food, function(food_object, key){

            if(food_object && angular.isObject(food_object)){
               
              angular.forEach(food_object, function(food, key){

                if(food.hasOwnProperty('fooddata') && food.calories!=null){
                  var food_data = {};

                  food_data.user_id = vm.userId;
                  food_data.nutrition_id = food.fooddata.nutrition_id;
                  food_data.calories = food.calories;

                  var serving_string = food.serving_string;
                  var serving_size_unit =  food.fooddata.serving_size_unit;

                  food_data.serving_string = serving_string;
                  food_data.serving_size_unit = serving_size_unit;                

                  var serving_food = serving_string.replace(serving_size_unit,'');
                  var serving_quantity = recursiveSearch(serving_food);

                  food_data.serving_quantity = serving_quantity.toFixed(2);                
                  food_data.no_of_servings = food.no_of_servings;
                  
                  food_data.is_custom = 0;
                  food_data.schedule_time = schedule_time;
                  food_data.serving_date = log_date;
                  food_data.user_agent = "server";

                  selected_recommended_food.push(food_data);
                }
                
              });
            }

      });
      
      if(selected_recommended_food.length == 0){
        vm.log_recommended_food_err = true;
        return false;
      }
      else{
        vm.log_recommended_food_err = false;
      }
      
      StoreService.save('api/v3/food/log_recommended_foods', selected_recommended_food).then(function(result) {
          if(result.error == false){

                vm.pushRecommendedLoggedFoods(result.response.disply_inserted_foods);
                $state.go('diary');
                $scope.diary_index.food_exercise_type = vm.food_type;

          }
        },function(error) {
          console.log(error);
        });
    }

    vm.pushRecommendedLoggedFoods = function(last_inserted_foods){
            console.log(last_inserted_foods[0].food_type);
        switch(last_inserted_foods[0].food_type){
          case "meal_1":
          
          angular.forEach(last_inserted_foods, function(values, key){
            $scope.diary_index.meal_one_food.push(values);
            $scope.diary_index.meal_one_total.calorie = Number($scope.diary_index.meal_one_total.calorie) + Number(values.calories);
            $scope.diary_index.foodSummary.total_consumed = Number($scope.diary_index.foodSummary.total_consumed) + Number(values.calories);
          });
          
          break;

          case "meal_2":
          
          angular.forEach(last_inserted_foods, function(values, key){
            $scope.diary_index.meal_two_food.push(values);
            $scope.diary_index.meal_two_food.calorie = Number($scope.diary_index.meal_two_food.calorie) + Number(values.calories);
            $scope.diary_index.foodSummary.total_consumed = Number($scope.diary_index.foodSummary.total_consumed) + Number(values.calories);
          });
          
          break;

          case "meal_3":
          
          angular.forEach(last_inserted_foods, function(values, key){
            $scope.diary_index.meal_three_food.push(values);
            $scope.diary_index.meal_three_food.calorie = Number($scope.diary_index.meal_three_food.calorie) + Number(values.calories);
            $scope.diary_index.foodSummary.total_consumed = Number($scope.diary_index.foodSummary.total_consumed) + Number(values.calories);
          });
        
          break;

          case "meal_4":
          
          angular.forEach(last_inserted_foods, function(values, key){
            $scope.diary_index.meal_four_food.push(values);
            $scope.diary_index.meal_four_food.calorie = Number($scope.diary_index.meal_four_food.calorie) + Number(values.calories);
            $scope.diary_index.foodSummary.total_consumed = Number($scope.diary_index.foodSummary.total_consumed) + Number(values.calories);
          });
          
          break;

             case "meal_5":

                angular.forEach(last_inserted_foods, function(values, key){
                    $scope.diary_index.meal_five_food.push(values);
                    $scope.diary_index.meal_five_food.calorie = Number($scope.diary_index.meal_five_food.calorie) + Number(values.calories);
                    $scope.diary_index.foodSummary.total_consumed = Number($scope.diary_index.foodSummary.total_consumed) + Number(values.calories);
                });

              break;

            case "meal_6":

                angular.forEach(last_inserted_foods, function(values, key){
                    $scope.diary_index.meal_six_food.push(values);
                    $scope.diary_index.meal_six_food.calorie = Number($scope.diary_index.meal_six_food.calorie) + Number(values.calories);
                    $scope.diary_index.foodSummary.total_consumed = Number($scope.diary_index.foodSummary.total_consumed) + Number(values.calories);
                });

                break;

            case "pre_workout_meal":

            angular.forEach(last_inserted_foods, function(values, key){
                $scope.diary_index.pre_workout_food.push(values);
                $scope.diary_index.pre_workout_food.calorie = Number($scope.diary_index.pre_workout_food.calorie) + Number(values.calories);
                $scope.diary_index.foodSummary.total_consumed = Number($scope.diary_index.foodSummary.total_consumed) + Number(values.calories);
            });

            break;

            case "post_workout_meal":

                angular.forEach(last_inserted_foods, function(values, key){
                    $scope.diary_index.post_workout_food.push(values);
                    $scope.diary_index.post_workout_food.calorie = Number($scope.diary_index.post_workout_food.calorie) + Number(values.calories);
                    $scope.diary_index.foodSummary.total_consumed = Number($scope.diary_index.foodSummary.total_consumed) + Number(values.calories);
                });

                break;
        }
    }

    vm.calculateRecomMealServingSize = function(key1, key2, nutrition_obj, from){
        
        if(from=="recommended_food"){
          var no_of_serving =  vm.recommended_food[key1][key2].no_of_servings;
          var serving_label = vm.recommended_food[key1][key2].serving_string;
          if(!serving_label){
            vm.recommended_food[key1][key2].calories = null;
            return false;
          }
        }
        else if(from=="recommended_meal"){
          var no_of_serving =  vm.recommended_meal_food[key1][key2].no_of_servings;
          var serving_label = vm.recommended_meal_food[key1][key2].serving_string;
        }

        var serving_quantity = nutrition_obj.serving_quantity;
        var serving_unit =  nutrition_obj.nutritions.nutrition_data.serving_size_unit;

        var calories = nutrition_obj.calories; 
        var no_of_serving_master = nutrition_obj.no_of_servings;
  
        if(serving_label){
          var serving_value = serving_label.replace(serving_unit,'');
        }
        else{
          var serving_value = 0;
        }

        var serving_size = recursiveSearch(serving_value);        

        if(nutrition_obj.hasOwnProperty("nutrition_data")){
            var nutrition_data = nutrition_obj.nutrition_data;
        }else{
            var nutrition_data = nutrition_obj.nutritions.nutrition_data;
        }

        var update_calories = calculateCalories(no_of_serving,calories,serving_size,serving_quantity,no_of_serving_master);
        
        var updated_calories = update_calories.toFixed(2);

        if(from=="recommended_food"){
          vm.recommended_food[key1][key2].calories = updated_calories;
        }
        else if(from=="recommended_meal"){
          vm.recommended_meal_food[key1][key2].calories = updated_calories;
        }
        
        
    }

    vm.MealFoodObjectFilter = function(food_object, no_of_servings){
      var new_food_obj = {};
      
      var fat = food_object.nutritions.nutrition_data.total_fat;
      var fat_arr = fat.split(' ');
      fat = Number(fat_arr[0].replace(',',''));
      new_food_obj.fat = fat * no_of_servings;

      var fiber = food_object.nutritions.nutrition_data.dietary_fiber;
      var fiber_arr = fiber.split(' ');
      fiber = Number(fiber_arr[0].replace(',',''));
      new_food_obj.fiber = fiber * no_of_servings;

      var carb = food_object.nutritions.nutrition_data.total_carb;
      var carb_arr = carb.split(' ');
      carb = Number(carb_arr[0].replace(',',''));
      new_food_obj.carb = carb * no_of_servings;

      var sodium = food_object.nutritions.nutrition_data.sodium;
      var sodium_arr = sodium.split(' ');
      sodium = Number(sodium_arr[0].replace(',',''));
      new_food_obj.sodium = sodium * no_of_servings;

      var protein = food_object.nutritions.nutrition_data.protein;
      var protein_arr = protein.split(' ');
      protein = Number(protein_arr[0].replace(',',''));
      new_food_obj.protein = protein * no_of_servings;

      return new_food_obj;

    }

    vm.getScheduleTime = function(food_type){

        var scheduleTime='';
        switch(food_type){
            case "meal_1":
                scheduleTime = "Meal 1(Breakfast)";
                break;

            case "meal_2":
                scheduleTime = "Meal 2(Snack)";
                break;

            case "meal_3":
                scheduleTime = "Meal 3(Lunch)";
                break;

            case "meal_4":
                scheduleTime = "Meal 4(Snack)";
                break;

            case "meal_5":
                scheduleTime = "Meal 5(Dinner)";
                break;
            case "meal_6":
                scheduleTime = "Meal 6(Snack - Optional)";
                break;
            case "pre_workout_meal":
                scheduleTime = "Pre Workout Meal";
                break;
            case "post_workout_meal":
                scheduleTime = "Post Workout Meal";
                break;
        }
        return scheduleTime;
    }

}]);

angular.module('app.core').directive('customSelect', function($timeout) {
  return function(scope, element, attrs) {
     $timeout(function() {
      $(element).selectric({
            maxHeight: 200,

        });}, 3000);

  };    
});

