angular.module('app.core').controller('ReportsController',
	['ShowService','$scope','StoreService','$timeout', '$location',
		function(ShowService,$scope,StoreService,$timeout, $location){
		var vm = this;
		vm.userId = null;
		vm.go_to = null;

		vm.go_to_workout = function(){
			$timeout(function() {
				if(vm.go_to=='workout'){
					angular.element('#workout_report').trigger('click');					  
				}
			});
		}

		vm.initChartLoder = function(){
			  vm.chart_loader= true;
              vm.calorie_chart_display = true;
              vm.bm_chart_display = true;
              vm.weight_chart_display = true;
              vm.bp_chart_display = true;
              vm.sleep_chart_display = true;
              vm.water_chart_display = true;
              vm.workout_chart_display = true;
		}

	    vm.displayCharts = function(date_range){

	    	vm.initChartLoder();
            var params = vm.userId;
        
	          params = params+'?date_range='+date_range;
			  ShowService.get('api/v3/reports/display_charts',params).then(function(result){
			  	var calorie_chart_data = result.response.calorie_chart;
			  	var weight_chart_data = result.response.weight_chart;
			  	var body_fat_chart_data = result.response.body_fat_chart;
			  	var water_chart_data =result.response.water_chart;
			  	var bp_chart_data = result.response.bp_chart;
			  	var pulse_chart_data = result.response.pulse_chart;
			  	var bm_chart_data = result.response.bm_chart;
			  	var sleep_chart_data =result.response.sleep_chart;
			  	var workout_chart_data =result.response.workout_chart;

	             vm.displayCalorieChart(calorie_chart_data);
	             vm.displayWeightChart(weight_chart_data);
	             vm.displayBodyFatChart(body_fat_chart_data);
	             vm.displayWaterChart(water_chart_data);
	             vm.displayBpChart(bp_chart_data);
	             vm.displayPulseChart(pulse_chart_data);
	             vm.displayBmChart(bm_chart_data);
	             vm.displayBmawnlh(bm_chart_data,'neck-chart','neck');
	             vm.displayBmawnlh(bm_chart_data,'arms-chart','arms');
	             vm.displayBmawnlh(bm_chart_data,'waist-chart','waist');
	             vm.displayBmawnlh(bm_chart_data,'hips-chart','hips');
	             vm.displayBmawnlh(bm_chart_data,'legs-chart','legs');
	             vm.displaySleepChart(sleep_chart_data);
	             vm.displayWorkoutChart(workout_chart_data);

			 });
	    }

	    vm.displayCalorieChart = function(data){

	    	//console.log(data);
	    	var data_date_format = data.chart_setting.data_date_format;
	    	var data_provider = data.chart_data;
	    	
	    	var calorie_guide = getCalorieGuide(data.goal);
	    	//console.log(calorie_guide);
	    	AmCharts.makeChart("calorie-chart",

		      {
		          "type": "serial",
		          "categoryField": "date",
		          "dataDateFormat": data_date_format,
		          "handDrawScatter": 1,
		          "processCount": 998,
		          "svgIcons": false,
		          "tapToActivate": false,
		          "theme": "default",
		          "categoryAxis": {
		            "parseDates": true
		          },
		          "trendLines": [],
		          "graphs": [
		            {
		              "bullet": "round",
		              "fixedColumnWidth": 0,
		              "id": "AmGraph-1",
		              "lineColor": "#61AB00",
		              "lineThickness": 2,
		              "tabIndex": -5,
		              "title": "Calories Consume",
		              "type": "smoothedLine",
		              "valueField": "calorie_consume"
		            },
		            {
		              "bullet": "round",
		              "color": "#FF0000",
		              "fillColors": "#FF0000",
		              "fixedColumnWidth": 0,
		              "fontSize": 1,
		              "id": "AmGraph-2",
		              "lineColor": "#FF0000",
		              "lineThickness": 2,
		              "maxBulletSize": 49,
		              "tabIndex": 0,
		              "title": "Calories Burned",
		              "valueField": "calorie_burned"
		            }
		          ],
		          "guides": calorie_guide,
		          "valueAxes": [
		            {
		              "id": "ValueAxis-1",
		              "title": "Calories"
		            }
		          ],
		          "legend": {
						"enabled": true,
						"useGraphSettings": true
					},
		          "allLabels": [],
		          "balloon": {},
		          "titles": [
		            {
		              "id": "Title-1",
		              "size": 15,
		              "text": "Calories"
		            }
		          ],
		          "dataProvider": data_provider
		      }

		    );

	   	 	vm.chart_loader = false;
	    	vm.calorie_chart_display = true; 
	    }

	    vm.displayWeightChart = function(data){

	    	var data_date_format = data.chart_setting.data_date_format;
	    	var data_provider = data.chart_data;
	    	var weight_goal = data.goal.weight_goal;
	    		
	    	AmCharts.makeChart("weight-chart",
		        {
		          "type": "serial",
		          "categoryField": "date",
		          "dataDateFormat": data_date_format,
		          "theme": "default",
		          "categoryAxis": {
		            
		            "parseDates": true
		          },
		          "trendLines": [],
		          "graphs": [
		            {
		              "bullet": "round",
		              "columnWidth": 0,
		              "fixedColumnWidth": 0,
		              "fontSize": 0,
		              "id": "AmGraph-1",
		              "lineColor": "#1294F2",
		              "lineThickness": 2,
		              "title": "Weight",
		              "valueField": "weight"
		            }
		          ],
		          "guides": [
		            {
		              "boldLabel": true,
		              "color": "#FF0000",
		              "dashLength": 6,
		              "fillAlpha": 0,
		              "fillColor": "#FF0000",
		              "fontSize": 12,
		              "id": "weight-1",
		              "inside": true,
		              "label": "Weight Goal: "+weight_goal+" lbs",
		              "lineAlpha": 1,
		              "lineColor": "#FF0000",
		              "lineThickness": 2,
		              "value": weight_goal,
		              "valueAxis": "ValueAxis-1"
		            }
		          ],
		          "valueAxes": [
		            {
		              "id": "ValueAxis-1",
		              "title": "lbs"
		            }
		          ],
		          "legend": {
						"enabled": true,
						"useGraphSettings": true
					},
		          "allLabels": [],
		          "balloon": {},
		          "titles": [
		            {
		              "id": "Title-1",
		              "size": 15,
		              "text": "Weight"
		            }
		          ],
		          "dataProvider": data_provider
		        }
		    );

	   		 vm.chart_loader = false;
	    	 vm.weight_chart_display = true; 
	    }

	    vm.displayBodyFatChart = function(data){

	    	var data_date_format = data.chart_setting.data_date_format;
	    	var data_provider = data.chart_data;
	    	var body_fat_goal = data.goal.body_fat_goal;
	    	AmCharts.makeChart("body-fat-chart",
		        {
		          "type": "serial",
		          "categoryField": "date",
		          "dataDateFormat": data_date_format,
		          "theme": "default",
		          "categoryAxis": {
		            
		            "parseDates": true
		          },
		          "trendLines": [],
		          "graphs": [
		            {
		              "bullet": "round",
		              "columnWidth": 0,
		              "fixedColumnWidth": 0,
		              "fontSize": 0,
		              "id": "AmGraph-1",
		              "lineColor": "#1294F2",
		              "lineThickness": 2,
		              "title": "Body Fat",
		              "valueField": "body_fat"
		            }
		          ],
		          "guides": [
		            {
		              "boldLabel": true,
		              "color": "#FF0000",
		              "dashLength": 6,
		              "fillAlpha": 0,
		              "fillColor": "#FF0000",
		              "fontSize": 12,
		              "id": "body_fat-1",
		              "inside": true,
		              "label": "Body Fat Goal: "+body_fat_goal+"%",
		              "lineAlpha": 1,
		              "lineColor": "#FF0000",
		              "lineThickness": 2,
		              "value": body_fat_goal,
		              "valueAxis": "ValueAxis-1"
		            }
		          ],
		          "valueAxes": [
		            {
		              "id": "ValueAxis-1",
		              "title": "Percent"
		            }
		          ],
		          "legend": {
						"enabled": true,
						"useGraphSettings": true
					},
		          "allLabels": [],
		          "balloon": {},
		          "titles": [
		            {
		              "id": "Title-1",
		              "size": 15,
		              "text": "Body Fat"
		            }
		          ],
		          "dataProvider": data_provider
		        }
		    );
		
	    }

	    vm.displayWaterChart = function(data){

	    	var data_date_format = data.chart_setting.data_date_format;
	    	var data_provider = data.chart_data;
	    	var water_goal = data.goal.water_goal;

	    	AmCharts.makeChart("waterintake-chart",
		        {
		          "type": "serial",
		          "categoryField": "date",
		          "dataDateFormat": data_date_format,
		          "theme": "default",
		          "categoryAxis": {
		            "parseDates": true
		          },
		          "trendLines": [],
		          "graphs": [
		            {
		              "bullet": "round",
		              "bulletBorderThickness": 1,
		              "fillColors": "#1ADACA",
		              "fontSize": 12,
		              "id": "AmGraph-1",
		              "lineColor": "#1ADACA",
		              "lineThickness": 2,
		              "tabIndex": -1,
		              "title": "Water Intake",
		              "type": "smoothedLine",
		              "valueField": "water"
		            }
		          ],
		          "guides": [
		            {
		              

		              "boldLabel": true,
		              "color": "#FF0000",
		              "dashLength": 6,
		              "fillAlpha": 0,
		              "fillColor": "#FF0000",
		              "fontSize": 12,
		              "id": "body_fat-1",
		              "inside": true,
		              "label": "WaterIntake Goal: "+water_goal+"Oz",
		              "lineAlpha": 1,
		              "lineColor": "#FF0000",
		              "lineThickness": 2,
		              "value": water_goal,
		              "valueAxis": "ValueAxis-1" 	


		            }
		          ],
		          "valueAxes": [
		            {
		              "id": "ValueAxis-1",
		              "title": "Ounces",
		              //"titleColor": "#1ADACA"
		            }
		          ],
		          "legend": {
						"enabled": true,
						"useGraphSettings": true
					},
		          "allLabels": [],
		          "balloon": {},
		          "titles": [
		            {
		              
		              "id": "Title-1",
		              "size": 15,
		              "text": "Water Intake"
		            }
		          ],
		          "dataProvider":data_provider 
		        }
		    );

	  		 vm.chart_loader = false;
	    	 vm.water_chart_display = true;  
	    }

	    vm.displayBpChart = function(data){

	    	var data_date_format = data.chart_setting.data_date_format;
	    	var data_provider = data.chart_data;
	    	var systolic_goal = data.goal.systolic_goal;
	    	var diastolic_goal = data.goal.diastolic_goal;

	    	 AmCharts.makeChart("bp-chart",
		        {
		          "type": "serial",
		          "categoryField": "date",
		          "dataDateFormat": data_date_format,
		          "handDrawScatter": 1,
		          "processCount": 998,
		          "svgIcons": false,
		          "tapToActivate": false,
		          "theme": "default",
		          "categoryAxis": {
		            
		            "parseDates": true
		          },
		          "trendLines": [],
		          "graphs": [
		            {
		              "bullet": "round",
		              "fixedColumnWidth": 0,
		              "id": "systolic-1",
		              "lineColor": "#61AB00",
		              "lineThickness": 2,
		              "tabIndex": -5,
		              "title": "Systolic",
		              "type": "smoothedLine",
		              "valueField": "systolic"
		            },
		            {
		              "bullet": "round",
		              "color": "#FF0000",
		              "fillColors": "#FF0000",
		              "fixedColumnWidth": 0,
		              "fontSize": 1,
		              "id": "AmGraph-2",
		              "lineColor": "#FF0000",
		              "lineThickness": 2,
		              "maxBulletSize": 49,
		              "tabIndex": 0,
		              "title": "Diastolic",
		              "valueField": "diastolic"
		            }
		          ],
		          "guides": [
		            {
		              "boldLabel": true,
		              "color": "#61AB00",
		              "dashLength": 6,
		              "fillColor": "#61AB00",
		              "fontSize": 12,
		              "id": "Systolic-Guide-1",
		              "inside": true,
		              "label": "Systolic Goal: "+systolic_goal,
		              "lineAlpha": 1,
		              "lineColor": "#61AB00",
		              "lineThickness": 2,
		              "tickLength": 5,
		              "value": systolic_goal,
		              "valueAxis": "ValueAxis-1"
		            },
		            {
		              "boldLabel": true,
		              "color": "#FF0000",
		              "dashLength": 6,
		              "fillColor": "#FF0000",
		              "fontSize": 12,
		              "id": "Diastolic-Guide-2",
		              "inside": true,
		              "label": "Diastolic Goal: "+diastolic_goal,
		              "lineAlpha": 1,
		              "lineColor": "#FF0000",
		              "lineThickness": 2,
		              "tickLength": 5,
		              "value": diastolic_goal,
		              "valueAxis": "ValueAxis-1"
		            }
		          ],
		          "valueAxes": [
		            {
		              "id": "ValueAxis-1",
		              "title": "mmHg",
		              //"titleColor": "#1ADACA"
		            }
		          ],
  		          "legend": {
						"enabled": true,
						"useGraphSettings": true
					},
		          "allLabels": [],
		          "balloon": {},
		          "titles": [
		            {
		              "id": "Title-1",
		              "size": 15,
		              "text": "Blood pressure"
		            }
		          ],
		          "dataProvider": data_provider
		        }
		    );
	    	
       	
       		 vm.chart_loader = false;
       		 vm.bp_chart_display = true;
        }

        vm.displayPulseChart = function(data){

	    	var data_date_format = data.chart_setting.data_date_format;
	    	var data_provider = data.chart_data;
	    	var pulse_goal = data.goal.pulse_goal;

	    	 AmCharts.makeChart("pulse-chart",
		        {
		          "type": "serial",
		          "categoryField": "date",
		          "dataDateFormat": data_date_format,
		          "handDrawScatter": 1,
		          "processCount": 998,
		          "svgIcons": false,
		          "tapToActivate": false,
		          "theme": "default",
		          "categoryAxis": {
		            "parseDates": true
		          },
		          "trendLines": [],
		          "graphs": [
		            {
		              "bullet": "round",
		              "fixedColumnWidth": 0,
		              "id": "pulse-1",
		              "lineColor": "#61AB00",
		              "lineThickness": 2,
		              "tabIndex": -5,
		              "title": "Pulse",
		              "type": "smoothedLine",
		              "valueField": "pulse"
		            }
		            
		          ],
		          "guides": [
		            {
		              "boldLabel": true,
		              "color": "#61AB00",
		              "dashLength": 6,
		              "fillColor": "#61AB00",
		              "fontSize": 12,
		              "id": "Pulse-Guide-1",
		              "inside": true,
		              "label": "Pulse Goal: "+pulse_goal,
		              "lineAlpha": 1,
		              "lineColor": "#61AB00",
		              "lineThickness": 2,
		              "tickLength": 5,
		              "value": pulse_goal,
		              "valueAxis": "ValueAxis-1"
		            }
		            
		          ],
		          "valueAxes": [
		            {
		              "id": "ValueAxis-1",
		              "title": "bpm",
		              //"titleColor": "#1ADACA"
		            }
		          ],
  		          "legend": {
						"enabled": true,
						"useGraphSettings": true
					},
		          "allLabels": [],
		          "balloon": {},
		          "titles": [
		            {
		              "id": "Title-1",
		              "size": 15,
		              "text": "Pulse"
		            }
		          ],
		          "dataProvider": data_provider
		        }
		    );
	    	
        }
        vm.displayBmChart = function(data){

        	var data_date_format = data.chart_setting.data_date_format;
	    	var data_provider = data.chart_data;
	    	var neck_goal = data.goal.neck_goal;
	    	var arms_goal = data.goal.arms_goal;
	    	var waits_goal = data.goal.waits_goal;
	    	var hips_goal = data.goal.hips_goal;
	    	var legs_goal = data.goal.legs_goal;
        	AmCharts.makeChart("all-bm-chart",
		        {
		          "type": "serial",
		          "categoryField": "date",
		          "dataDateFormat": data_date_format,
		          "theme": "default",
		          "categoryAxis": {
		           
		            "parseDates": true
		          },
		          "trendLines": [],
		          "graphs": [
		            {
		              "bullet": "round",
		              "columnWidth": 0,
		              "fixedColumnWidth": 0,
		              "fontSize": 0,
		              "id": "neck-1",
		              "lineColor": "#61AB00",
		              "lineThickness": 2,
		              "title": "neck",
		              "valueField": "neck"
		            },
		            {
		              "bullet": "round",
		              "columnWidth": 0,
		              "fixedColumnWidth": 0,
		              "fontSize": 0,
		              "id": "arms-1",
		              "lineColor": "#FF0000",
		              "lineThickness": 2,
		              "title": "arms",
		              "valueField": "arms"
		            },
		            {
		              "bullet": "round",
		              "columnWidth": 0,
		              "fixedColumnWidth": 0,
		              "fontSize": 0,
		              "id": "waist-1",
		              "lineColor": "#ff8000",
		              "lineThickness": 2,
		              "title": "waist",
		              "valueField": "waist"
		            },
		            {
		              "bullet": "round",
		              "columnWidth": 0,
		              "fixedColumnWidth": 0,
		              "fontSize": 0,
		              "id": "hips-1",
		              "lineColor": "#0c5662",
		              "lineThickness": 2,
		              "title": "hips",
		              "valueField": "hips"
		            },
		            {
		              "bullet": "round",
		              "columnWidth": 0,
		              "fixedColumnWidth": 0,
		              "fontSize": 0,
		              "id": "legs-1",
		              "lineColor": "#9400d3",
		              "lineThickness": 2,
		              "title": "legs",
		              "valueField": "legs"
		            }
		          ],

		          "guides": [
		            {
		              "boldLabel": true,
		              "color": "#61AB00",
		              "dashLength": 6,
		              "fillColor": "#61AB00",
		              "fontSize": 12,
		              "id": "Neck-Guide-1",
		              "inside": true,
		              "label": "Neck Goal: "+neck_goal,
		              "lineAlpha": 1,
		              "lineColor": "#61AB00",
		              "lineThickness": 2,
		              "tickLength": 5,
		              "value": neck_goal,
		              "valueAxis": "ValueAxis-1"
		            },
		            {
		              "boldLabel": true,
		              "color": "#FF0000",
		              "dashLength": 6,
		              "fillColor": "#FF0000",
		              "fontSize": 12,
		              "id": "Arms-Guide-2",
		              "inside": true,
		              "label": "Arms Goal: "+arms_goal,
		              "lineAlpha": 1,
		              "lineColor": "#FF0000",
		              "lineThickness": 2,
		              "tickLength": 5,
		              "value": arms_goal,
		              "valueAxis": "ValueAxis-1"
		            },

		            {
		              "boldLabel": true,
		              "color": "#ff8000",
		              "dashLength": 6,
		              "fillColor": "#ff8000",
		              "fontSize": 12,
		              "id": "Waist-Guide-2",
		              "inside": true,
		              "label": "Waist Goal: "+waits_goal,
		              "lineAlpha": 1,
		              "lineColor": "#ff8000",
		              "lineThickness": 2,
		              "tickLength": 5,
		              "value": waits_goal,
		              "valueAxis": "ValueAxis-1"
		            },
		            {
		              "boldLabel": true,
		              "color": "#0c5662",
		              "dashLength": 6,
		              "fillColor": "#0c5662",
		              "fontSize": 12,
		              "id": "Waist-Guide-2",
		              "inside": true,
		              "label": "Hips Goal: "+hips_goal,
		              "lineAlpha": 1,
		              "lineColor": "#0c5662",
		              "lineThickness": 2,
		              "tickLength": 5,
		              "value": hips_goal,
		              "valueAxis": "ValueAxis-1"
		            },
		            {
		              "boldLabel": true,
		              "color": "#9400d3",
		              "dashLength": 6,
		              "fillColor": "#9400d3",
		              "fontSize": 12,
		              "id": "Waist-Guide-2",
		              "inside": true,
		              "label": "Legs Goal: "+legs_goal,
		              "lineAlpha": 1,
		              "lineColor": "#9400d3",
		              "lineThickness": 2,
		              "tickLength": 5,
		              "value": legs_goal,
		              "valueAxis": "ValueAxis-1"
		            },
		          ],
		          
		          "valueAxes": [
		            {
		              "id": "ValueAxis-1",
		              "title": "Inches"
		            }
		          ],
		           "legend": {
						"enabled": true,
						"useGraphSettings": true
					},
		          "allLabels": [],
		          "balloon": {},
		          "titles": [
		            {
		              "id": "Title-1",
		              "size": 15,
		              "text": "Body Measurement"
		            }
		          ],
		          "dataProvider":data_provider 
		        }
		    );

		     vm.chart_loader = false;
        	 vm.bm_chart_display = true;
        }
        vm.displayBmawnlh = function(data,chart_id,bm_type){

        	var data_date_format = data.chart_setting.data_date_format;
	    	var data_provider = data.chart_data;
	    	var display_type = capitalizeFirstLetter(bm_type);
	    	if(bm_type == 'neck'){
	    		var line_color = '#61AB00';
	    		var bm_goal = data.goal.neck_goal;
	    	}else if(bm_type == 'arms'){
	    		var line_color = '#FF0000';
	    		var bm_goal = data.goal.arms_goal;
	    	}else if(bm_type == 'waist'){
	    		var line_color = '#ff8000';
	    		var bm_goal = data.goal.waits_goal;
	    	}else if(bm_type == 'hips'){
	    		var line_color = '#0c5662';
	    		var bm_goal = data.goal.hips_goal;
	    	}else if(bm_type == 'legs'){
	    		var line_color = '#9400d3';
	    		var bm_goal = data.goal.neck_goal;
	    	}
        	AmCharts.makeChart(chart_id,
		        {
		          "type": "serial",
		          "categoryField": "date",
		          "dataDateFormat": data_date_format,
		          "theme": "default",
		          "categoryAxis": {
		           
		            "parseDates": true
		          },
		          "trendLines": [],
		          "graphs": [
		            {
		              "bullet": "round",
		              "columnWidth": 0,
		              "fixedColumnWidth": 0,
		              "fontSize": 0,
		              "id": "neck-1",
		              "lineColor": line_color,
		              "lineThickness": 2,
		              "title": bm_type,
		              "valueField": bm_type
		            }
		            
		          ],

		          "guides": [

						{
			              "boldLabel": true,
			              "color": line_color,
			              "dashLength": 6,
			              "fillColor": line_color,
			              "fontSize": 12,
			              "id": "BM-Guide-1",
			              "inside": true,
			              "label": bm_type+": "+bm_goal,
			              "lineAlpha": 1,
			              "lineColor": line_color,
			              "lineThickness": 2,
			              "tickLength": 5,
			              "value": bm_goal,
			              "valueAxis": "ValueAxis-1"
		            }

					],
		          
		          "valueAxes": [
		            {
		              "id": "ValueAxis-1",
		              "title": "Inches"
		            }
		          ],
		           "legend": {
						"enabled": true,
						"useGraphSettings": true
					},
		          "allLabels": [],
		          "balloon": {},
		          "titles": [
		            {
		              "id": "Title-1",
		              "size": 15,
		              "text": display_type
		            }
		          ],
		          "dataProvider":data_provider 
		        }
		    );
        }
        vm.displaySleepChart = function(data) {

        	var data_date_format = data.chart_setting.data_date_format;
	    	var data_provider = data.chart_data;
	    	var sleep_goal = data.goal.sleep_goal;
	    	var display_goal = sleep_goal.hours +" HRS ";
	    	  if(parseInt(sleep_goal.min) > 0){
	    	  	display_goal+= sleep_goal.min+" MINS";
	    	  }

        	AmCharts.makeChart("sleep-chart",
				{
					"type": "serial",
					"categoryField": "date",
					"dataDateFormat": data_date_format,
					"theme": "default",
					"categoryAxis": {
						
						"parseDates": true
					},
					"trendLines": [],
					"graphs": [
						{
							"columnWidth": 0.05,
							"cornerRadiusTop": 1,
							"dashLength": 3,
							"fillAlphas": 1,
							"id": "sleep-id",
							"lineAlpha": 0,
							"lineColor": "#0000FF",
							"lineThickness": 3,
							"title": "sleep",
							"type": "column",
							"valueField": "sleep"
						}
					],
					"guides": [

						{
			              "boldLabel": true,
			              "color": "#61AB00",
			              "dashLength": 6,
			              "fillColor": "#61AB00",
			              "fontSize": 12,
			              "id": "Pulse-Guide-1",
			              "inside": true,
			              "label": "Sleep Goal: "+display_goal,
			              "lineAlpha": 1,
			              "lineColor": "#61AB00",
			              "lineThickness": 2,
			              "tickLength": 5,
			              "value": sleep_goal.hours,
			              "valueAxis": "ValueAxis-1"
		            }

					],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "Hours"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": "Sleep"
						}
					],
					"dataProvider": data_provider
				}
			);

       		 vm.chart_loader = false;
	    	 vm.sleep_chart_display = true; 
        }

        vm.displayWorkoutChart = function(data) {

        	var data_date_format = data.chart_setting.data_date_format;
	    	var data_provider = data.chart_data;
	   

        	AmCharts.makeChart("workout-chart",
				{
					"type": "serial",
					"categoryField": "date",
					"dataDateFormat": data_date_format,
					"theme": "default",
					"categoryAxis": {
						
						"parseDates": true
					},
					"trendLines": [],
					"graphs": [
						{
							"columnWidth": 0.05,
							"cornerRadiusTop": 1,
							"dashLength": 3,
							"fillAlphas": 1,
							"id": "workout-id",
							"lineAlpha": 0,
							"lineColor": "#0000FF",
							"lineThickness": 3,
							"title": "workout",
							"type": "column",
							"valueField": "workout_calories"
						}
					],
					"guides": [

						{
			              "boldLabel": true,
			              "color": "#61AB00",
			              "dashLength": 6,
			              "fillColor": "#61AB00",
			              "fontSize": 12,
			              "id": "Pulse-Guide-1",
			              "inside": true,
			              "label": "Workout Goal: No Goal",
			              "lineAlpha": 1,
			              "lineColor": "#61AB00",
			              "lineThickness": 2,
			              "tickLength": 5,
			              "value": 0,
			              "valueAxis": "ValueAxis-1"
		            }

					],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "Calories burnt"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": "Workout"
						}
					],
					"dataProvider": data_provider
				}
			);

       		 vm.chart_loader = false;
	    	 vm.workout_chart_display = true;
        }

        function capitalizeFirstLetter(string) {
			    return string.charAt(0).toUpperCase() + string.slice(1);
		}

		function getCalorieGuide(goal){

			var calorie_goal = goal.calorie_intake_g;
	    	var calorie_burned_goal = goal.calorie_burned_g;
	    	var guide = new Array();
	    	var intake_guide ={
		              "boldLabel": true,
		              "color": "#61AB00",
		              "dashLength": 6,
		              "fillColor": "#61AB00",
		              "fontSize": 12,
		              "id": "Consume-Guide-1",
		              "inside": true,
		              "label": "Calorie Consume Goal: "+calorie_goal+" Cal",
		              "lineAlpha": 1,
		              "lineColor": "#61AB00",
		              "lineThickness": 2,
		              "tickLength": 5,
		              "value": calorie_goal,
		              "valueAxis": "ValueAxis-1"
		            };
		   var burn_guide= {
		              "boldLabel": true,
		              "color": "#FF0000",
		              "dashLength": 6,
		              "fillColor": "#FF0000",
		              "fontSize": 12,
		              "id": "Burned-Guide-2",
		              "inside": true,
		              "label": "Calorie Burned Goal: "+calorie_burned_goal+" Cal",
		              "lineAlpha": 1,
		              "lineColor": "#FF0000",
		              "lineThickness": 2,
		              "tickLength": 5,
		              "value": calorie_burned_goal,
		              "valueAxis": "ValueAxis-1"
		            };
		          	guide.push(intake_guide);
		            if(parseInt(calorie_burned_goal) > 0){
		            	guide.push(burn_guide);	
		            }
		   return guide;
		}
		
}]);