angular.module('app.routes').config(function($stateProvider, $urlRouterProvider) {
$urlRouterProvider.otherwise('/list-workout');
    $stateProvider
        .state('list-workout', {
            url: '/list-workout',
            templateUrl: '/user_workout',
            controller : 'UserWorkoutController',
            controllerAs : 'user_workouts'
        })
        .state('workout-details', {
            url: '/workout-details/:id',            
            templateUrl: '/load_workout_details',
            controller : 'UserWorkoutDetailController',
            controllerAs : 'workout_details'
        })
        .state('routine-details', {
            url: '/routine-details/:id',            
            templateUrl: '/load_routine_details',
            controller : 'UserRoutineDetailController',
            controllerAs : 'routine_detail'
        });
});
angular.module('app.core').controller('UserWorkoutIndexController',['ShowService','StoreService','$scope','$rootScope','$state',function(ShowService,StoreService,$scope,$rootScope,$state){
    var vm = this;
    vm.userId = null;
    
    vm.getLoggedUserWorkoutRoutines = function(){
     
      ShowService.get('api/v3/workout/logged_user_workout_routines', vm.userId).then(function(results) {

           vm.loggedUserWorkoutRoutines = results.response.user_workout_routines;
      });
    }

    vm.getUserWorkoutRoutines = function(week){
      
      if(week=="next"){
        vm.week_start = moment(vm.week_start).add(1, 'weeks').format('YYYY-MM-DD');
        vm.week_end = moment(vm.week_end).add(1, 'weeks').format('YYYY-MM-DD');
      }
      else if (week=="previous"){
        vm.week_start = moment(vm.week_start).subtract(1, 'weeks').format('YYYY-MM-DD');
        vm.week_end = moment(vm.week_end).subtract(1, 'weeks').format('YYYY-MM-DD');
      }
      else{
        vm.week_start = moment().startOf('isoWeek').format('YYYY-MM-DD');
        vm.week_end = moment().endOf('isoWeek').format('YYYY-MM-DD');
      }

      vm.all_workouts = [];
      vm.sunday_routine = [];
      vm.monday_routine = [];
      vm.tuesday_routine = [];
      vm.wednesday_routine = [];
      vm.thursday_routine = [];
      vm.friday_routine = [];
      vm.saturday_routine = [];

      ShowService.get('api/v3/workout/display_user_workout_routines', vm.userId+"?start="+vm.week_start+"&end="+vm.week_end).then(function(results){
        
        if(results.error==false){

            var user_all_workouts = results.response.user_all_workouts;
            // Convert Object to Array
            var output = [];
            for (var key in user_all_workouts) {
              // must create a temp object to set the key using a variable
              var tempObj = {};
              tempObj = user_all_workouts[key];
              output.push(tempObj);
            }   
            vm.all_workouts = output; 
            // end: Convert Object to Array    
            
           // vm.all_workouts_count = vm.countObjLength(vm.all_workouts);

            vm.sunday_routine = results.response.user_current_week_workouts.Sunday;
            vm.monday_routine = results.response.user_current_week_workouts.Monday;
            vm.tuesday_routine = results.response.user_current_week_workouts.Tuesday;
            vm.wednesday_routine = results.response.user_current_week_workouts.Wednesday;
            vm.thursday_routine = results.response.user_current_week_workouts.Thursday;
            vm.friday_routine = results.response.user_current_week_workouts.Friday;
            vm.saturday_routine = results.response.user_current_week_workouts.Saturday;
          
        }
      }); 
    }

    vm.getMdRecommendedWorkout = function(){
      ShowService.search('api/v3/workout/list').then(function(results){
           vm.MdRecommendedWorkout = results.response;
      });   
    }

    vm.getMdRecommendedRoutines = function(){
      
      vm.mdRecommendedRoutines = [];

      ShowService.search('api/v3/template/list').then(function(results){
           
           if(results.error == false){
              vm.mdRecommendedRoutines = results.response;
              angular.forEach(vm.mdRecommendedRoutines, function(values, key){
               
                angular.forEach(vm.user_templates_info, function(u_teplate, key2){
                  if(values.id == u_teplate.template_id)
                    {
                      values.info = u_teplate;

                    }
                });
              });

           }
           else{
              console.log(results.error);
           }
           
      });   
    }   
    
}]);