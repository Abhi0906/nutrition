angular.module('app.core').controller('SocialMediaController',['ShowService','StoreService','$http','$cookieStore','$scope','$location', '$anchorScroll', function(ShowService,StoreService,$http,$cookieStore,$scope, $location, $anchorScroll){
    var vm = this;
    vm.getAllSocialMedia = function(){
                 ShowService.search('api/v2/admin/social_media/get_social_media').then(function(results){
                    vm.socialMedia = results.response;
                });
    }
    vm.updateSocialMedia = function(){
        var social_media_data={'media_data':vm.socialMedia};
        StoreService.save('api/v2/admin/social_media/update_social_media',social_media_data).then(function(result) {
            if(result.error == false) {
            showFlashMessage('Update','Social Media Update successfully','success');
           }
        });
    }
}]);