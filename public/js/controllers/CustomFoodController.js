angular.module('app.core').controller('CustomFoodController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', 
	function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){

		var vm = this;
    	vm.userId = $scope.diary_index.userId;
    	vm.custom_nutritions = {};

    	vm.getCustomFoods = function(){
    		$('.back-top').trigger('click');
    		vm.custom_food_loader = true;
	    	vm.show_custom_foods = false;

			var params = vm.userId;
			ShowService.get('api/v3/food/custom_food', params).then(function(result){

				if(result.error == false){
					vm.custom_nutritions = result.response.custom_nutritions;

					if(vm.custom_nutritions.length>0){
						vm.noData = false;
			    		vm.show_custom_foods = true;
					}	
					else{
						vm.noData = true;
						vm.show_custom_foods = false;
					}				
				}

				vm.custom_food_loader = false;
				
			});

		}

		vm.deleteCustomFood = function(food_obj){
      
			var id = food_obj.id;
			var msg = "Are you sure want to delete this food ?";

			showFlashMessageConfirmation("Delete",msg,"info",function(result) {
				if(result) {
			          ShowService.remove('api/v3/food/delete_custom_food', id).then(function(results){

			               	var index = vm.custom_nutritions.indexOf(food_obj);
                          	vm.custom_nutritions.splice(index, 1);

                          	var index2 = $scope.diary_index.custom_nutritions.indexOf(food_obj);
                          	$scope.diary_index.custom_nutritions.splice(index, 1);

                          	showFlashMessage('Delete','Food deleted successfully','success');
			            });
			    }
			});

		}
	}
]);