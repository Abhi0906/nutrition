angular.module('app.core').controller('ExerciseImageAddController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){
    var vm = this;
    $('.back-top').trigger('click');

	vm.steps = [{id: 'step-1','desc':null}];

	vm.addNewStep = function() {
		var newItemNo = vm.steps.length+1;
		vm.steps.push({'id':'step-'+newItemNo});
	};

	vm.removeStep = function() {
		var lastItem = vm.steps.length-1;
		vm.steps.splice(lastItem);
	};

	vm.saveExerciseImage = function(){

		var exercise_data ={};
		exercise_data.name =vm.name;
		exercise_data.exercise_time =vm.exercise_time;
		exercise_data.calorie_burn =vm.calorie_burn;
		exercise_data.experience =vm.experience;
		exercise_data.body_part =vm.body_part;
		exercise_data.equipment =vm.equipment;
		exercise_data.description =vm.description;
		exercise_data.exercise_steps =vm.steps;
		exercise_data.image_name = vm.image;
		exercise_data.exercise_type = vm.exercise_type;
		//console.log(exercise_data);return false;
        StoreService.save('api/v2/admin/exercise_image',exercise_data).then(function(result) {
          if(result.error == false){
            $state.go('list-exercise-images');

          }
        },function(error) {
          console.log(error);
        });


	}

}]);
