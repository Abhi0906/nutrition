  /**
 * Created by Plenar on 9/24/2015.
 */

angular.module('app.core').controller('ProfileEditController', function(ShowService,StoreService,$scope){
    var vm = this;
    vm.user = [];
    vm.userId = null;
    vm.userType ='patient';
    vm.current_weight = "141";
    vm.carbohydrates = "50%";
    vm.protein = "30%";
    vm.fat = "20%";
    vm.baseline_weight = "147.3"
    vm.calories = "1,500";
    
    vm.getDemographics = function() {
    ShowService.get('api/v3/'+vm.userType+'/demographics',vm.userId).then(function(results){
        var result = results.response;
        vm.age = moment().diff(results.response.birth_date, 'years');
        vm.user = result;
          if(result.user_type == 'patient'){
          vm.user.news_letter = parseInt(result.profile.news_letter);
        }
        vm.orig =angular.copy(vm.user);
    });

   }   

    //update user (patient/doctor)
    vm.updateUser = function(isValid) {
       var date = vm.user.birth_date;
       if (isValid) {                   
          StoreService.update('api/v3/'+vm.userType,vm.userId,vm.user).then(function(result) {
              if(result.error == false) {
                vm.age = moment().diff(date, 'years');
                showFlashMessage('Update','User Updated successfully','success');
              }  
          }, function(error) {
              console.log(error);
          }); 
      }  
    }
  
  vm.reset = function() {
      vm.user =  angular.copy(vm.orig);
    }
  });