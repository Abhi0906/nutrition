angular.module('app.routes').config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/diary');

    $stateProvider
        .state('diary', {
            url: '/diary',
            templateUrl: '/load_diary_page',
            controller : 'DiaryController',
            controllerAs : 'diary'
        })
        .state('food-diary', {
            url: '/food-diary',
            templateUrl: '/load_food_diary',
            controller : 'FoodDiaryController',
            controllerAs : 'food_diary'
        })
        .state('log_food', {
            url: '/log_food',
            templateUrl: '/log_food',
            params : { food_type : null, selected_from: null, selected_food : null },
            controller : 'LogFoodController',
            controllerAs : 'add_food'
        })
        .state('add_custom_food', {
            url: '/add_custom_food',
            templateUrl: '/add_custom_food',
            controller : 'DiaryIndexController',
            controllerAs : 'diary_index'
        })
        .state('custom_foods', {
            url: '/custom_foods',
            templateUrl: '/custom_foods',
            controller : 'CustomFoodController',
            controllerAs : 'custom_food'
        })
        .state('create_meal', {
            url: '/create_meal',
            templateUrl: '/create_meal',
            params : { food_type: null, meal_food: null, meal_food_total: null },
            controller : 'MealController',
            controllerAs : 'meal'
        })
        .state('meals', {
            url: '/meals',
            templateUrl: '/meals',
            controller : 'MealController',
            controllerAs : 'meal'
        })
        .state('log_exercise', {
            url: '/log_exercise',
            templateUrl: '/log_exercise',
            params : { exercise_type : null, selected_from: null,selected_exercise : null },
            controller : 'LogExerciseController',
            controllerAs : 'add_exercise'
        });

});

angular.module('app.core').controller('DiaryIndexController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', '$rootScope', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll, $rootScope){
    var vm = this;
    vm.userId = null;
    vm.food_type = null;
    vm.exercise_type = null;
    vm.user_gender = null;


    vm.log_date = new Date();
    vm.favourite_nutritions = null;
    vm.frequent_nutritions = null;
    vm.recent_nutritions = null;
    vm.custom_food = {};
    vm.meals = {};
    vm.log_newfoodloader = false;

    vm.frequent_exercises = null;
    vm.recent_exercises = null;

    $('.back-top').trigger('click');





    vm.getFoodDiary = function(){
      vm.diary_summary_loader = true;
      vm.meal_one_food =[];
      vm.meal_two_food = [];
      vm.meal_three_food =[];
      vm.meal_four_food =[];
      vm.meal_five_food =[];
      vm.meal_six_food =[];
      vm.pre_workout_food =[];
      vm.post_workout_food =[];

      vm.meal_one_total=[];
      vm.meal_two_total=[];
      vm.meal_three_total=[];
      vm.meal_four_total=[];
      vm.meal_five_total=[];
      vm.meal_six_total=[];
      vm.pre_workout_total=[];
      vm.post_workout_total=[];

      vm.totalDinner=[];
      vm.totalSnacks=[];

      vm.foodSummary=[];
      vm.cardioExercises = [];
      vm.totalCardio = [];
      vm.resistanceExercises = [];
      vm.totalResistance = [];
      vm.exerciseSummary = [];

      vm.food_notes ="edit notes";
      vm.water_intake = 0;
      var params = vm.userId;
      var log_date = moment(vm.log_date).format('YYYY-MM-DD');
      params = vm.userId+'?log_date='+log_date;


      ShowService.get('api/v3/food/patient_food', params).then(function(result){

        if(result.response.todayLoggedFood !== 'undefined' && result.response.todayLoggedFood !== null){

            if(typeof result.response.todayLoggedFood.meal_1 != 'undefined'){

              vm.meal_one_food = result.response.todayLoggedFood.meal_1;

            }

            if(typeof result.response.todayLoggedFood.meal_2 != 'undefined'){

              vm.meal_two_food = result.response.todayLoggedFood.meal_2;
            }

            if(typeof result.response.todayLoggedFood.meal_3 != 'undefined'){
              vm.meal_three_food = result.response.todayLoggedFood.meal_3;
            }

            if(typeof result.response.todayLoggedFood.meal_4 != 'undefined'){
              vm.meal_four_food = result.response.todayLoggedFood.meal_4;
            }

            if(typeof result.response.todayLoggedFood.meal_5 != 'undefined'){
                vm.meal_five_food = result.response.todayLoggedFood.meal_5;
            }

            if(typeof result.response.todayLoggedFood.meal_6 != 'undefined'){
                vm.meal_six_food = result.response.todayLoggedFood.meal_6;
            }

            if(typeof result.response.todayLoggedFood.pre_workout_meal != 'undefined'){
                vm.pre_workout_food = result.response.todayLoggedFood.pre_workout_meal;
            }

            if(typeof result.response.todayLoggedFood.post_workout_meal != 'undefined'){
                vm.post_workout_food = result.response.todayLoggedFood.post_workout_meal;
            }



            if(typeof result.response.todayLoggedFood.meal_one_total != 'undefined'){
              vm.meal_one_total = result.response.todayLoggedFood.meal_one_total;
            }
            else{
              vm.meal_one_total.calorie = 0;
            }

            if(typeof result.response.todayLoggedFood.meal_two_total != 'undefined'){
              vm.meal_two_total = result.response.todayLoggedFood.meal_two_total;
            }
            else{
              vm.meal_two_total.calorie = 0;
            }

            if(typeof result.response.todayLoggedFood.meal_three_total != 'undefined'){
              vm.meal_three_total = result.response.todayLoggedFood.meal_three_total;
            }
            else{
              vm.meal_three_total.calorie = 0;
            }

            if(typeof result.response.todayLoggedFood.meal_four_total != 'undefined'){
              vm.meal_four_total = result.response.todayLoggedFood.meal_four_total;
            }
            else{
              vm.meal_four_total.calorie = 0;
            }

            if(typeof result.response.todayLoggedFood.meal_five_total != 'undefined'){
                vm.meal_five_total = result.response.todayLoggedFood.meal_five_total;
            }
            else{
                vm.meal_five_total.calorie = 0;
            }

            if(typeof result.response.todayLoggedFood.meal_six_total != 'undefined'){
                vm.meal_six_total = result.response.todayLoggedFood.meal_six_total;
            }
            else{
                vm.meal_six_total.calorie = 0;
            }

            if(typeof result.response.todayLoggedFood.pre_workout_total != 'undefined'){
                vm.pre_workout_total = result.response.todayLoggedFood.pre_workout_total;
            }
            else{
                vm.pre_workout_total.calorie = 0;
            }

            if(typeof result.response.todayLoggedFood.post_workout_total != 'undefined'){
                vm.post_workout_total = result.response.todayLoggedFood.post_workout_total;
            }
            else{
                vm.post_workout_total.calorie = 0;
            }

        }

         vm.foodSummary = result.response.food_summary;

         // Get Exercise Data
      if(result.response.patientExercise !== 'undefined' && result.response.patientExercise !== null){
         if(typeof result.response.patientExercise.Cardio != 'undefined'){
            vm.cardioExercises = result.response.patientExercise.loggedExercise.Cardio;
          }

          if(typeof result.response.patientExercise.loggedExercise.cardio_total != 'undefined'){
            vm.totalCardio = result.response.patientExercise.loggedExercise.cardio_total;
          }
          else{
            vm.totalCardio.calories_burned = 0;
          }

          if(typeof result.response.patientExercise.loggedExercise.Resistance != 'undefined'){
            vm.resistanceExercises = result.response.patientExercise.loggedExercise.Resistance;
          }

          if(typeof result.response.patientExercise.loggedExercise.resistance_total != 'undefined'){
            vm.totalResistance = result.response.patientExercise.loggedExercise.resistance_total;
          }
          else{
            vm.totalResistance.calories_burned = 0;
          }


          vm.exerciseSummary = result.response.patientExercise.exercise_summary;

          vm.frequent_exercises = result.response.patientExercise.frequent_exercises;
          vm.recent_exercises = result.response.patientExercise.recent_exercises;

          vm.appleHealthExercises = result.response.patientExercise.apple_health_exercises.apple_health;
          vm.totalAppleHealth = result.response.patientExercise.apple_health_exercises.apple_health_total;
      }
          // End: Get Exercise Data

         // get food note
         var food_note_data = result.response.food_note;
         if(food_note_data!=null){
          vm.notes = food_note_data.notes;
         }
         // end get food note

         // get total water intake
         var  total_water_intake = result.response.total_water_intake;
         if(total_water_intake!=null){
          vm.water_intake = total_water_intake;
         }

         // end total water intake


         // get favourite , frequent and recent data
            vm.favourite_nutritions = result.response.favourite_nutritions;
            vm.frequent_nutritions = result.response.frequent_nutritions;
            vm.recent_nutritions = result.response.recent_nutritions;
            vm.custom_nutritions = result.response.custom_nutritions;
            vm.meals = result.response.meals;
            vm.md_meals = result.response.md_meals;
            vm.md_meal_foods = result.response.md_meal_foods;
            angular.forEach(vm.md_meal_foods, function(values, key){
              var value_foods = values.recommended_foods;
              switch(values.name){
                case "Proteins":
                vm.proteins_foods = value_foods;
                break;

                case "Carbohydrates":
                vm.carb_foods = value_foods;
                break;

                case "Vegetables":
                vm.vegetable_foods = value_foods;
                break;

                case "Healthy Fats":
                vm.healthyfat_foods = value_foods;
                break;
              }
            });
         // end get fav, freq, rec data

         $rootScope.$broadcast('onGetFoodExercise', {data_type: 'food'});
      });

    }

    /*vm.getExerciseDiary = function(){

      vm.diary_summary_loader = true;
      var params = vm.userId;
      vm.cardioExercises = [];
      vm.totalCardio = [];
      vm.resistanceExercises = [];
      vm.totalResistance = [];
      vm.exerciseSummary = [];

      var log_date = moment(vm.log_date).format('YYYY-MM-DD');
      params = vm.userId+'?log_date='+log_date;

      ShowService.get('api/v3/exercise/patient_exercise', params).then(function(result){

          if(typeof result.response.loggedExercise.Cardio != 'undefined'){
            vm.cardioExercises = result.response.loggedExercise.Cardio;
          }

          if(typeof result.response.loggedExercise.cardio_total != 'undefined'){
            vm.totalCardio = result.response.loggedExercise.cardio_total;
          }
          else{
            vm.totalCardio.calories_burned = 0;
          }

          if(typeof result.response.loggedExercise.Resistance != 'undefined'){
            vm.resistanceExercises = result.response.loggedExercise.Resistance;
          }

          if(typeof result.response.loggedExercise.resistance_total != 'undefined'){
            vm.totalResistance = result.response.loggedExercise.resistance_total;
          }
          else{
            vm.totalResistance.calories_burned = 0;
          }


          vm.exerciseSummary = result.response.exercise_summary;

          vm.frequent_exercises = result.response.frequent_exercises;
          vm.recent_exercises = result.response.recent_exercises;
          vm.exercise_notes = result.response.exercise_notes;
          vm.appleHealthExercises = result.response.apple_health_exercises.apple_health;
          vm.totalAppleHealth = result.response.apple_health_exercises.apple_health_total;
          console.log("get");
          $rootScope.$broadcast('onGetFoodExercise', {data_type: 'exercise'});

      });



    }*/

/********************* Custom food add ********************************/
    // set custom food object values
    vm.custom_food.sodium = 0;

    vm.custom_food.total_fat = 0;

    vm.custom_food.potassium = 0;
    vm.custom_food.saturated_fat = 0;

    vm.custom_food.total_carbs = 0;

    vm.custom_food.polyunsaturated = 0;

    vm.custom_food.dietary_fiber = 0;

    vm.custom_food.monounsaturated = 0;

    vm.custom_food.sugar = 0;

    vm.custom_food.protein = 0;

    vm.custom_food.trans_fat = 0;
    vm.custom_food.cholesterol = 0;
    vm.custom_food.vitamin_a = 0;

    vm.custom_food.calcium = 0;

    vm.custom_food.vitamin_c = 0;

    vm.custom_food.vitamin_c = 0;

    vm.custom_food.iron = 0;
    vm.custom_food.serving_quantity = 0;
    vm.custom_food.serving_size_unit = '';
    // end set custom food object values

    vm.add_new_food = function(){
       vm.log_newfoodloader = true;

       var custom_food_data = {

            "user_id" : vm.userId,
            "user_agent" : 'server',
            "brand_name" : vm.custom_food.brand_name,
            "name" : vm.custom_food.food_name,
            "nutrition_data": {
                "calories": vm.custom_food.calories,
                "sodium": vm.custom_food.sodium + " mg",
                "total_fat": vm.custom_food.total_fat + " g",
                "potassium": vm.custom_food.potassium + " mg",
                "saturated_fat": vm.custom_food.saturated_fat + " g",
                "total_carb": vm.custom_food.total_carbs + " g",
                "polyunsaturated": vm.custom_food.polyunsaturated + " g",
                "dietary_fiber": vm.custom_food.dietary_fiber + " g",
                "monounsaturated": vm.custom_food.monounsaturated + " g",
                "sugars": vm.custom_food.sugar + " g",
                "protein": vm.custom_food.protein + " g",
                "trans_fat": vm.custom_food.trans_fat + " g",
                "cholesterol": vm.custom_food.cholesterol + " mg",
                "vitamin_a": vm.custom_food.vitamin_a + "%",
                "calcium_dv": vm.custom_food.calcium + "%",
                "vitamin_c": vm.custom_food.vitamin_c + "%",
                "iron_dv": vm.custom_food.iron + "%",
                "serving_quantity": vm.custom_food.serving_quantity,
                "serving_size_unit": vm.custom_food.serving_size_unit,
                }
       };

        StoreService.save('api/v3/food/add_custom_food',custom_food_data).then(function(result) {
            if(result.error == false){

                if(result.error == false) {
                    showFlashMessage('Added','Custom Food Added successfully','success');
                    $state.go('diary');
                }

                vm.custom_food = {};

            }
            vm.log_newfoodloader = false;

        },function(error) {
            console.log(error);
          });

        return false;
    }
/********************* End: Custom food add ********************************/


}]);
