angular.module('app.core').controller('ShowClassController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll','$sce', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll,$sce){
    var vm = this;
    $('#to-top').trigger('click');
    vm.settingTitle = 'Google Calendar';

    vm.getActiveEventSetting = function(){

        ShowService.query('api/v2/admin/events/active_settings').then(function(results){
           var event_setting = results.response;
           vm.event_type  = event_setting.type;

           if(vm.event_type == "google_calendar"){
             vm.settingTitle = 'Google Calendar';
             vm.google_api_key = event_setting.config.api_key;
             vm.google_calendarId = event_setting.config.calendarId;
            }else if(vm.event_type == "events_pdf"){
             vm.settingTitle = 'Events PDF';
             vm.event_pdf_url = event_setting.config.pdf_url;
            }else if(vm.event_type == "events_url"){
               vm.settingTitle = 'Events URL';
               vm.event_url =  $sce.trustAsResourceUrl(event_setting.config.url);
            }

        });

    }



}]);

angular.module('app.core').directive('googleCalendar', function($timeout) {
  return function(scope, element, attrs) {

    if('apiKey' in attrs && 'calendarId' in attrs){
            var google_api_key = attrs['apiKey'];
            var google_calendarId = attrs['calendarId'];

        }
    $timeout(function() {
        intiCalendar();
     },0);

    function intiCalendar(){
        $(element).fullCalendar({

            header: {
                      left: 'prev,next today',
                      center: 'title',
                      right: 'month,agendaWeek,agendaDay'
                   },

                // THIS KEY WON'T WORK IN PRODUCTION!!!
                // To make your own Google API key, follow the directions here:
                // http://fullcalendar.io/docs/google_calendar/
                googleCalendarApiKey: google_api_key,

                // US Holidays
                events: {
                googleCalendarId: google_calendarId,
                className: 'gcal-event' // an option!
                },

                eventClick: function(event) {
                    // opens events in a popup window
                    window.open(event.url, 'gcalevent', 'width=700,height=600');
                    return false;
                },

                // loading: function(bool) {
                //     $('#loading').toggle(bool);
                // }

        });
    }

     };
});