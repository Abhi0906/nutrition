 /**
 * Created by Plenar on 29/1/2016.
 */
angular.module('app.core').controller('GoalsController', function(ShowService,StoreService,$scope,utility){
    var vm = this;
    vm.user_goal = [];
    vm.calorie_goal = [];
    vm.water_goal = [];
    vm.bm_goal = [];
    vm.weight_goal = [];
    vm.bp_goal = [];
    vm.sleep_goal=[];
    vm.userId = null;
    vm.userType ='patient';
    vm.today_date = moment().format('MM/DD/YYYY');
    var sleep_goal_date= new Date();
        sleep_goal_date.setHours(8);
        sleep_goal_date.setMinutes(0);
    vm.sleep_goal.goal = sleep_goal_date;


    vm.format = 'MM-dd-yyyy';
    vm.bm_goal.goal_date = new Date();
    vm.bp_goal.goal_date = new Date();
    vm.sleep_goal.goal_date = new Date();

    vm.dateOptions = {
      showWeeks:false
    };

    vm.body_date_open = function() {
            vm.body_date_popup.opened = true;
        };
        vm.body_date_popup = {
            opened: false
        };

    vm.bp_pulse_date_open = function() {
            vm.bp_pulse_date_popup.opened = true;
        };
        vm.bp_pulse_date_popup = {
            opened: false
        };

    vm.sleep_open = function() {
        vm.sleep_popup.opened = true;
    };
    vm.sleep_popup = {
        opened: false
    };


  ShowService.query('api/v3/'+vm.userType+'/weeklygoals').then(function(results){
          vm.weeklygoals =[];
          if(results.response != null){
               for(i = 0; i < results.response.length; i++){
                 results.response[i].id = parseInt(results.response[i].id);
              }
          }
          vm.weeklygoals = results.response;
    });

    vm.updateCalorieGoal = function(){
       var calorieGoal ={};
       calorieGoal.calorie_intake_g =vm.calorie_goal.calorie_intake_g;
       calorieGoal.calorie_burned_g =vm.calorie_goal.calorie_burned_g;
       calorieGoal.guid_server = true;
       StoreService.update('api/v3/goals/update_calorie_goal',vm.userId,calorieGoal).then(function(result) {
            if(result.error == false) {
                showFlashMessage('Update','Calorie Goal Updated successfully','success');
              }
            }, function(error) {
                console.log(error);
          });
    }

    vm.updateBodyMeasurement = function(){
      var body_measurement ={};
      body_measurement.goal_date = moment(vm.bm_goal.goal_date).format('YYYY-MM-DD');
      body_measurement.neck_g =vm.bm_goal.neck_g;
      body_measurement.arms_g =vm.bm_goal.arms_g;
      body_measurement.waist_g =vm.bm_goal.waist_g;
      body_measurement.hips_g =vm.bm_goal.hips_g;
      body_measurement.legs_g =vm.bm_goal.legs_g;
      body_measurement.guid_server = true;
      StoreService.update('api/v3/goals/update_bm_goal',vm.userId,body_measurement).then(function(result) {
        if(result.error == false) {
            showFlashMessage('Update','Body Measurement Goal Updated successfully','success');
          }
        }, function(error) {
            console.log(error);
      });
    }

    vm.updateWeightGoal = function(){

      var weight_goal_data ={};

      weight_goal_data.weekly_goal_id =vm.weight_goal.weekly_goal_id;
      weight_goal_data.weight_g =vm.weight_goal.weight_g;
      weight_goal_data.body_fat_g =vm.weight_goal.body_fat_g;
      weight_goal_data.current_body_fat = vm.weight_goal.current_body_fat;
      weight_goal_data.baseline_weight =vm.weight_goal.baseline_weight;
      weight_goal_data.height_in_feet =vm.weight_goal.height_in_feet;
      weight_goal_data.height_in_inch =vm.weight_goal.height_in_inch;
      weight_goal_data.guid_server = true;
      var weight_diff = parseFloat(weight_goal_data.weight_g) - parseFloat(weight_goal_data.baseline_weight)
      var weekly_goal_value = vm.getWeeklyVal(weight_goal_data.weekly_goal_id);
      var number_of_days = (Math.abs(weight_diff)/Math.abs(weekly_goal_value))*7;
      var weight_goal_date = moment().add(parseInt(number_of_days), 'days').format('YYYY-MM-DD');
      weight_goal_data.goal_date = weight_goal_date;

      StoreService.update('api/v3/goals/update_weight_goal',vm.userId,weight_goal_data).then(function(result) {
        if(result.error == false) {
            showFlashMessage('Update',' Weight Goal Updated successfully','success');
          }
        }, function(error) {
            console.log(error);
      });

    }

    vm.updateBpPulse = function(){


      var bp_pulse ={};
      bp_pulse.goal_date = moment(vm.bp_goal.goal_date).format('YYYY-MM-DD');
      bp_pulse.systolic_g =vm.bp_goal.systolic_g;
      bp_pulse.diastolic_g =vm.bp_goal.diastolic_g;
      bp_pulse.heart_rate_g =vm.bp_goal.heart_rate_g;
      bp_pulse.guid_server = true;
      StoreService.update('api/v3/goals/update_bp_pulse_goal',vm.userId,bp_pulse).then(function(result) {
        if(result.error == false) {
            showFlashMessage('Update','Bp and Pulse Goal Updated successfully','success');
          }
        }, function(error) {
            console.log(error);
      });
    }

    vm.updateSleepGoal = function(){
      var sleepGoal ={};
      sleepGoal.goal_date =moment(vm.sleep_goal.goal_date).format('YYYY-MM-DD');
      sleepGoal.goal = moment.duration(vm.sleep_goal.goal).asHours();
      sleepGoal.guid_server = true;
      StoreService.update('api/v3/goals/update_sleep_goal',vm.userId,sleepGoal).then(function(result) {
          if(result.error == false) {
              showFlashMessage('Update','Sleep Goal Updated successfully','success');
            }
          }, function(error) {
              console.log(error);
        });
    }
    vm.updateWaterGoal = function() {
      var waterGoal ={};
      waterGoal.water_g =vm.water_goal.water_g;
      waterGoal.guid_server = true;
      StoreService.update('api/v3/goals/update_water_goal',vm.userId,waterGoal).then(function(result) {
          if(result.error == false) {
              showFlashMessage('Update','Water Goal Updated successfully','success');
            }
          }, function(error) {
              console.log(error);
        });
    }

    vm.getPatientGoals = function() {

      ShowService.get('api/v3/goals',vm.userId).then(function(results){

          vm.calorie_goal =  results.response.calorie_goal;
          vm.bm_goal =  results.response.bm_goal;
          vm.weight_goal =  results.response.weight_goal;
          vm.bp_goal =  results.response.bp_goal;
          vm.water_goal =results.response.water_goal;
          if(results.response.sleep_goal.hours > 0){
                  vm.sleep_goal=results.response.sleep_goal;
                  var sleep_goal_date= new Date();
                  sleep_goal_date.setHours(results.response.sleep_goal.hours);
                  sleep_goal_date.setMinutes(results.response.sleep_goal.min);
                  vm.sleep_goal.goal = sleep_goal_date;
          }

          vm.weight_goal.weekly_goal_id = parseInt(results.response.weight_goal.weekly_goal_id);

            if(results.response.bm_goal.goal_date){
              vm.bm_goal.goal_date = new Date(results.response.bm_goal.goal_date);
            }else{
              vm.bm_goal.goal_date = new Date(vm.today_date);
            }

            if(results.response.bp_goal.goal_date){
              vm.bp_goal.goal_date = new Date(results.response.bp_goal.goal_date);
            }else{
              vm.bp_goal.goal_date = new Date(vm.today_date);
            }

           if(results.response.sleep_goal.goal_date){
              vm.sleep_goal.goal_date = new Date(results.response.sleep_goal.goal_date);
           }else{
              vm.sleep_goal.goal_date = new Date(vm.today_date);
           }


          //  vm.orig = angular.copy(vm.user_goal);
       });
     }


    // Goal Weight called
    vm.changeGoalWeight = function() {
     var goal_weight = parseFloat(vm.goal_weight);
     var baseline_weight =  parseFloat(vm.baseline_weight);

     if(goal_weight > baseline_weight){
       vm.weekly_goal_id = 6;
       vm.calories = parseInt(patient_calories)+(500*parseFloat(0.5));
     }else{
       vm.weekly_goal_id = 1;
       vm.calories = parseInt(patient_calories)+(500*parseFloat(-0.5));
     }
    }


    // Weekly Goal called
    vm.changeCalories = function() {
        var weekly_goal_val = vm.getWeeklyVal(vm.weight_goal.weekly_goal_id);
        vm.weight_goal.recommended_daily_calorie = parseInt(vm.calorie_goal.calorie_intake_g)+(500*parseFloat(weekly_goal_val));
        //console.log(vm.weight_goal.recommended_daily_calorie);
    }

    vm.getWeeklyVal = function(weekly_goal_id){

      var weekly_value = 0;
      angular.forEach(vm.weeklygoals, function(weeklygoal, key) {
              if(weeklygoal['id'] == weekly_goal_id){
                weekly_value = weeklygoal['value'];
              }
            });
       return weekly_value;

    }

    vm.reset = function(){
      vm.user_goal = angular.copy(vm.orig);
    }

     vm.changeRecommendedCalorie = function(){
        var activity = 1;
        var age = vm.weight_goal.age;
        var  gender = vm.weight_goal.gender;
        var weight = vm.weight_goal.baseline_weight;
        var height_feet = vm.weight_goal.height_in_feet;
        var height_inches = vm.weight_goal.height_in_inch;
        var patient_calories = utility.calculateUsCalories(gender, weight, height_feet, height_inches, age, activity);
        //console.log(patient_calories);
        if(patient_calories) {
          vm.weight_goal.recommended_daily_calorie = patient_calories;
        }

    }

});