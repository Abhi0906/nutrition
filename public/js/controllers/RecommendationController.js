angular.module('app.routes').config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/foods');

    $stateProvider

        .state('foods', {
            url: '/foods',
            templateUrl: '/admin/recommended_food_content',
            controller : 'RecommendationController',
            controllerAs : 'recommend_food'
        })
        .state('add-food', {
            url: '/add-food',
            templateUrl: '/admin/add_recommended_food',
            params : { recommended_type_id : null,recommended_type_name:null},
            controller : 'RecommendationController',
            controllerAs : 'add_food'
        })
        .state('edit-food', {
            url: '/edit-food',
            templateUrl: '/admin/edit_recommended_food',
            params: { edit_from: null, recommended_type_id : null, food_data: null, food_for_names: null, recommended_types: null, recommended_food_id: null },
            controller: 'EditRecommendedFoodController',
            controllerAs: 'edit_food'
        });

});

angular.module('app.core').controller('RecommendationController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll','utility', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll,utility){
    var vm = this;
    $('.back-top').trigger('click');
    vm.add_food_url = "add-food";
    vm.recommended_type_name = null;

    vm.recommended_type_id = $stateParams.recommended_type_id;
    vm.recommended_type_name =  $stateParams.recommended_type_name;

    vm.search_loader = false;

    vm.displayShortRecords = [
    { name: 'Name', value: 'food_name' },
    { name: 'Gender', value: 'food_for' },
    { name: 'Calories', value: 'calories' }];

    vm.itemDefault = "food_name";

    /*if(vm.recommended_type_name){
        console.log(vm.recommended_type_name);
        $anchorScroll.yOffset = 150;
        var old = $location.hash();
        $location.hash(vm.food_type+'_focus_id');
        $anchorScroll();
        $location.hash(old);
    }*/

    vm.searchFood = function(){
            vm.search_loader = true;
            vm.foodSearchdata = {};
            //vm.food_tab_display = false;
            //vm.foodTabToggel(null, 'searched_food_tab');
            vm.search_result = false;
            vm.search_loader = true;
            var searchString = '';
            searchString = vm.searchKeyFood;
            ShowService.search('search.php?keyword='+searchString+'&limit=100').then(function(result) {


                if(result.error == false){

                   vm.foodSearchdata = result.response;
                    if(result.response.length>0){

                      vm.search_loader = false;
                      vm.search_result = true;

                      /*for(var i=0; i<vm.foodSearchdata.length; i++){

                          var is_favourite = favourite_food_ids.indexOf(Number(vm.foodSearchdata[i]['id']));
                          if(is_favourite != -1){
                            vm.foodSearchdata[i]['is_favourite'] = 1;
                          }
                          else{
                            vm.foodSearchdata[i]['is_favourite'] = 0;
                          }
                      }*/


                    }

                  vm.search_loader = false;
                  //vm.searchMenu = true;
                }

                vm.search_loader = false;
            },function(error) {
                console.log(error);
            });
    }

    //clear logFood search
    vm.clear_logFood_searchResult = function(){
        vm.searchKeyFood = '';

        vm.foodSearchdata = {};
    }

    //end: clear logFood search

    vm.getFoodRecommendedTypes = function(){

        vm.serving_ranges = utility.getServingRange(1,10,0.5);


        ShowService.query('api/v2/admin/recommended_food/types').then(function(result){
            vm.recommended_types = result.response;
        });
    }

    vm.createMdRecommendedFood = function($index, food_obj){


        var nutrition_id = food_obj.id;
        var calories = vm.recommended_food[$index].calories;

        var serving_string = vm.recommended_food[$index].serving_string.label;

        var serving_quantity =recursiveSearch(serving_string);

        var serving_size_unit = vm.recommended_food[$index].serving_string.unit;

        var no_of_servings = vm.recommended_food[$index].no_of_servings;
        var recommended_type_id = vm.recommended_food[$index].food_recommendation_type_id;
        var food_for = vm.recommended_food[$index].food_for;
        var serving_range = vm.recommended_food[$index].serving_range;

        var params = {
            "nutrition_id": nutrition_id,
            "food_recommendation_type_id": recommended_type_id,
            "calories": calories,
            "serving_quantity": serving_quantity,
            "serving_size_unit": serving_size_unit,
            "no_of_servings": no_of_servings,
            "serving_string": serving_string,
            "serving_range": serving_range,
            "food_for": food_for
        }

        StoreService.save('api/v2/admin/recommended_food/create', params).then(function(result){
            $state.go('foods');
        });
    }

    vm.viewFoodDetail = function($index,food_obj){


        vm.food_detail =  vm.updateNutritionFacts($index,food_obj);
        $('#view_food_detail_tmplt').modal('show');
    }

    vm.mdRecommendedFoodsDisplayData = function(){
        ShowService.query('api/v2/admin/recommended_food/display').then(function(result){
            if(result.error==false){
                vm.recommended_foods = result.response;
            }
        });
    }

    vm.deleteRecommendedFood = function(type_obj, food_obj){

      var id = food_obj.recommended_food_id;
      var msg = "Are you sure want to delete this food ?";

      showFlashMessageConfirmation("Delete",msg,"info",function(result) {
              if (result) {
                  ShowService.remove('api/v2/admin/recommended_food/delete', id).then(function(results){

                        var main_index = vm.recommended_foods.indexOf(type_obj);

                        var obj = vm.recommended_foods[main_index].recommended_foods;
                        var sub_index = obj.indexOf(food_obj);

                        vm.recommended_foods[main_index].recommended_foods.splice(sub_index, 1);

                       showFlashMessage('Delete','Food deleted successfully','success');
                    });
                }
      });

    }

    vm.calculateServingSize = function($index, nutrition_obj){

        var serving_size_obj = vm.recommended_food[$index].serving_string;
        var serving_range = vm.recommended_food[$index].serving_range;
        var no_of_serving =  vm.recommended_food[$index].no_of_servings;
        var serving_label = serving_size_obj.label;
        var serving_unit = serving_size_obj.unit;
        var serving_quantity = nutrition_obj.nutrition_data.serving_quantity;

        var serving_value = serving_label.replace(serving_unit,'');
        var serving_size = recursiveSearch(serving_value);

        if(nutrition_obj.hasOwnProperty("nutrition_data")){
            var nutrition_data = nutrition_obj.nutrition_data;
        }else{
            var nutrition_data = nutrition_obj.nutritions.nutrition_data;
        }

        var update_calories = calculateCalories(no_of_serving,nutrition_data,serving_size,serving_quantity,serving_range);

        vm.recommended_food[$index].calories =update_calories.toFixed(2);


    }

    vm.updateCaloriesRange = function($index, nutrition_obj){

        var no_of_serving =  vm.recommended_food[$index].no_of_servings;
        var serving_range =  vm.recommended_food[$index].serving_range;
        var default_calories = nutrition_obj.nutrition_data.calories;
        var update_calories = Number(default_calories)*Number(no_of_serving)*Number(serving_range);

        vm.recommended_food[$index].calories =update_calories.toFixed(2);


    }

    vm.updateDefaultServing = function($index, nutrition_obj){
       // console.log(nutrition_obj);

       var food_for =  vm.recommended_food[$index].food_for;

        switch (food_for) {
            case 'Any':
                vm.recommended_food[$index].no_of_servings=1;
                break;
            case 'Male':
                vm.recommended_food[$index].no_of_servings=5;
                break;
            case 'Female':
                vm.recommended_food[$index].no_of_servings=3;
                break;

        }
        var no_of_serving =  vm.recommended_food[$index].no_of_servings;
        var serving_range =  vm.recommended_food[$index].serving_range;
        var default_calories = nutrition_obj.nutrition_data.calories;
        //console.log(no_of_serving);
        //console.log(serving_range);
       // console.log(default_calories);


        var update_calories = Number(default_calories)*Number(no_of_serving)*Number(serving_range);

        vm.recommended_food[$index].calories =update_calories.toFixed(2);




    }

    vm.updateNutritionFacts = function($index,nutrition_obj){

        var copy_nutrition_obj = angular.copy(nutrition_obj);

        var no_of_serving =  vm.recommended_food[$index].no_of_servings;
        var serving_range =  vm.recommended_food[$index].serving_range;


        var default_protein = copy_nutrition_obj.nutrition_data.protein;
        var default_calories = copy_nutrition_obj.nutrition_data.calories;
        var default_calcium_dv = copy_nutrition_obj.nutrition_data.calcium_dv;
        var default_cholesterol = copy_nutrition_obj.nutrition_data.cholesterol;
        var default_dietary_fiber = copy_nutrition_obj.nutrition_data.dietary_fiber;
        var default_iron_dv = copy_nutrition_obj.nutrition_data.iron_dv;
        var default_monounsaturated = copy_nutrition_obj.nutrition_data.monounsaturated;
        var default_polyunsaturated = copy_nutrition_obj.nutrition_data.polyunsaturated;
        var default_potassium = copy_nutrition_obj.nutrition_data.potassium;
        var default_saturated_fat = copy_nutrition_obj.nutrition_data.saturated_fat;
        var default_sodium = copy_nutrition_obj.nutrition_data.sodium;
        var default_sugars = copy_nutrition_obj.nutrition_data.sugars;
        var default_total_carb = copy_nutrition_obj.nutrition_data.total_carb;
        var default_total_fat = copy_nutrition_obj.nutrition_data.total_fat;
        var default_trans_fat = copy_nutrition_obj.nutrition_data.trans_fat;
        var default_vitamin_a = copy_nutrition_obj.nutrition_data.vitamin_a;
        var default_vitamin_c = copy_nutrition_obj.nutrition_data.vitamin_c;


        var update_protine = parseFloat(default_protein)*Number(no_of_serving)*Number(serving_range);
        var update_calories = Number(default_calories)*Number(no_of_serving)*Number(serving_range);
        var update_calcium_dv = parseFloat(default_calcium_dv)*Number(no_of_serving)*Number(serving_range);
        var update_cholesterol = parseFloat(default_cholesterol)*Number(no_of_serving)*Number(serving_range);
        var update_dietary_fiber = parseFloat(default_dietary_fiber)*Number(no_of_serving)*Number(serving_range);
        var update_iron_dv = parseFloat(default_iron_dv)*Number(no_of_serving)*Number(serving_range);
        var update_monounsaturated = parseFloat(default_monounsaturated)*Number(no_of_serving)*Number(serving_range);
        var update_polyunsaturated = parseFloat(default_polyunsaturated)*Number(no_of_serving)*Number(serving_range);
        var update_potassium = parseFloat(default_potassium)*Number(no_of_serving)*Number(serving_range);
        var update_saturated_fat = parseFloat(default_saturated_fat)*Number(no_of_serving)*Number(serving_range);
        var update_sodium = parseFloat(default_sodium)*Number(no_of_serving)*Number(serving_range);
        var update_sugars = parseFloat(default_sugars)*Number(no_of_serving)*Number(serving_range);
        var update_total_carb = parseFloat(default_total_carb)*Number(no_of_serving)*Number(serving_range);
        var update_total_fat = parseFloat(default_total_fat)*Number(no_of_serving)*Number(serving_range);
        var update_trans_fat = parseFloat(default_trans_fat)*Number(no_of_serving)*Number(serving_range);
        var update_vitamin_a = parseFloat(default_vitamin_a)*Number(no_of_serving)*Number(serving_range);
        var update_vitamin_c = parseFloat(default_vitamin_c)*Number(no_of_serving)*Number(serving_range);




        copy_nutrition_obj.nutrition_data.calories = update_calories.toFixed(2);
        copy_nutrition_obj.nutrition_data.protein = update_protine +" g";
        copy_nutrition_obj.nutrition_data.calcium_dv = update_calcium_dv +"%";
        copy_nutrition_obj.nutrition_data.cholesterol = update_cholesterol +" mg";
        copy_nutrition_obj.nutrition_data.dietary_fiber = update_dietary_fiber +" g";
        copy_nutrition_obj.nutrition_data.iron_dv = update_iron_dv +"%";
        copy_nutrition_obj.nutrition_data.monounsaturated = update_monounsaturated +" g";
        copy_nutrition_obj.nutrition_data.polyunsaturated = update_polyunsaturated +" g";
        copy_nutrition_obj.nutrition_data.potassium = update_potassium +" mg";
        copy_nutrition_obj.nutrition_data.saturated_fat = update_saturated_fat +" g";
        copy_nutrition_obj.nutrition_data.sodium = update_sodium +" mg";
        copy_nutrition_obj.nutrition_data.sugars = update_sugars +" g";
        copy_nutrition_obj.nutrition_data.total_carb = update_total_carb +" g";
        copy_nutrition_obj.nutrition_data.total_fat = update_total_fat +" g";
        copy_nutrition_obj.nutrition_data.trans_fat = update_trans_fat +" g";
        copy_nutrition_obj.nutrition_data.vitamin_a = update_vitamin_a +"%";
        copy_nutrition_obj.nutrition_data.vitamin_c = update_vitamin_c +"%";


        return copy_nutrition_obj;

    }

    function recursiveSearch(serving_value){

       //  var serving_quantity=0;
         if (serving_value.toLowerCase().indexOf("container") >= 0) {

            var arr = serving_value.split("(");
            if (arr.length > 1) {
                var innerString = arr[1].trim();
                if (innerString.indexOf("/") >= 0) {
                    return recursiveSearch(innerString);
                }else{
                    var space_arr = innerString.split(" ");
                   return recursiveSearch(space_arr[0]);
                }
            }
        }else if (serving_value.indexOf("/") >= 0) {

            var arr = serving_value.split("/")
            if (arr.length > 1) {
                var innerString = arr[0].trim();
                var arrInnerCompo = innerString.split(" ");
                if (arrInnerCompo.length == 2) {
                    var firstString = arrInnerCompo[0].trim();
                    var secondString = arrInnerCompo[1].trim();
                    var arr1FirstComponent= arr[1].trim();
                    var thirdString = Number(arr1FirstComponent.split(" ")[0]);
                    // console.log("firstString",firstString);
                    // console.log("secondString",secondString);
                    // console.log("thirdString",thirdString);
                    var dividevalue = Number(firstString) + Number(secondString) / Number(thirdString) ;
                   return recursiveSearch(dividevalue.toString());

                }else{

                     var arrInner = innerString.split(" ");
                     var firstString = Number(arrInner[arrInner.length-1]);
                     var secondStringArr= arr[1].trim();
                     var secondString = Number(secondStringArr.split(" ")[0]);
                     var thirdString = firstString/secondString;
                     return recursiveSearch(thirdString.toString());

                }

            }

        }else{

            var final_value = serving_value.trim();
            var splitString = final_value.split(" ")[0]
            return Number(splitString);
        }

    }

    function calculateCalories(no_of_servings,nutrition_obj,serving_size,serving_quantity,serving_range){

        var default_calories = nutrition_obj.calories;
        var serving_size_value = Number(serving_size/serving_quantity);
        var update_calories = Number(default_calories)*Number(no_of_servings)*Number(serving_size_value)*Number(serving_range);
        return update_calories;

    }



}]);
