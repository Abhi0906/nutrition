angular.module('app.core').controller('CreateDealController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', 'utility', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll, utility){
    var vm = this;

    vm.deal_data = {};

    vm.today_datetime = new Date();

    // date time picker funcions
    vm.isOpen_startDate = false;

    vm.openCalendar_startDate = function(e) {
        e.preventDefault();
        e.stopPropagation();

        vm.isOpen_startDate = true;
    };

    vm.isOpen_endDate = false;

    vm.openCalendar_endDate = function(e) {
        e.preventDefault();
        e.stopPropagation();

        vm.isOpen_endDate = true;
    };
    // End: date time picker funcions

    vm.saveDeal = function(){

        var deal_data = {};
        deal_data.title = vm.deal_data.title;
        deal_data.description = vm.deal_data.description;
        deal_data.start = moment(vm.deal_data.start_datetime).format('YYYY-MM-DD HH:mm:ss');
        deal_data.end = moment(vm.deal_data.end_datetime).format('YYYY-MM-DD HH:mm:ss');
        deal_data.coupon_code = vm.deal_data.coupon_code;
        deal_data.image = vm.deal_data.image;

        StoreService.save('api/v2/admin/deal/create_deal', deal_data).then(function(result){

            vm.gotoDealListPage();

        });
    }

    vm.gotoDealListPage = function(){
        $state.go('deal-list');
    }

    // date compare validation
    vm.errMessage_startdate = '';
    vm.errMessage_enddate = '';
    vm.date_error = false;
    vm.dateCompareValidation = function(startDate, endDate) {

        vm.errMessage_startdate = '';
        vm.errMessage_enddate = '';
        vm.date_error = false;

        var result = utility.dateCompareValidation(startDate, endDate);

        if(result=='end_date'){
          vm.errMessage_enddate = 'End Date & Time should be greater than start date.';
          vm.date_error = true;
          return false;
        }

        if(result=='start_date'){
           vm.errMessage_startdate = 'Start Date & Time should not be before today.';
           vm.date_error = true;
           return false;
        }
    }
    // end: date compare validation

}]);