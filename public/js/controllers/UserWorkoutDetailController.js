angular.module('app.core').controller('UserWorkoutDetailController',['ShowService','$stateParams','StoreService','$scope','$rootScope','$state',function(ShowService,$stateParams,StoreService,$scope,$rootScope,$state){
	var vm = this;
	vm.id = $stateParams.id;
	$('.back-top').trigger('click');
	
	vm.getWorkoutdetails = function(){
		vm.workout = {};
		ShowService.search('api/v3/workout/'+ vm.id).then(function(results){
			if(results.error == false){
				var workout = results.response;

				angular.forEach(workout.exercise_lists, function(values, key){

					values.steps = JSON.parse(values.steps);
				});
				
				vm.workout = workout;
			}			
			
		});
    }
}]);