angular.module('app.core').controller('RecommendedMealItemController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){
    var vm = this;

    vm.recommended_meal_id = $stateParams.recommended_meal_id;
    vm.meal_name = $stateParams.recommended_meal_name;
    vm.item_id = $stateParams.item_id;
    vm.meal_item_data = {};

    vm.getMealItemData = function(){

        ShowService.get('api/v2/admin/recommended_meal/item_data', vm.recommended_meal_id).then(function(result){
            if(result.error==false){
                vm.meal_item_data = result.response;
                vm.getTotalMealItemData();
            }
        });

        vm.getdata_RecommendedMeal();
    }

    vm.deleteRecommendedMealFood = function(item_obj, food_obj){

        var id = food_obj.recommended_meal_food_prim_id;
        var msg = "Are you sure want to delete this food ?";

        showFlashMessageConfirmation("Delete", msg, "info", function(result) {
              if (result) {

                ShowService.remove('api/v2/admin/recommended_meal/food_delete', id).then(function(result){
                    if(result.error==false){

                        var item_index = vm.meal_item_data.indexOf(item_obj);
                        var food_index = vm.meal_item_data[item_index].meal_item_foods.indexOf(food_obj);
                        vm.meal_item_data[item_index].meal_item_foods.splice(food_index, 1);

                        showFlashMessage('Delete','Food deleted successfully','success');
                        vm.getTotalMealItemData();
                    }



                });

              }
        });
    }

    vm.getTotalMealItemData = function(){

        vm.totalMealItemData = {};

        var total_calories = 0;
        var total_fat = 0;
        var total_fiber = 0;
        var total_carb = 0;
        var total_sodium = 0;
        var total_protein = 0;

        angular.forEach( vm.meal_item_data, function(values, key){

            sub_total_calories = 0;
            sub_total_fat = 0;
            sub_total_fiber = 0;
            sub_total_carb = 0;
            sub_total_sodium = 0;
            sub_total_protein = 0;

            angular.forEach( values.meal_item_foods, function(food, key2){
                sub_total_calories = sub_total_calories + Number(food.calories);
                sub_total_fat = sub_total_fat + Number(food.total_fat);
                sub_total_fiber = sub_total_fiber + Number(food.dietary_fiber);
                sub_total_carb = sub_total_carb + Number(food.total_carb);
                sub_total_sodium = sub_total_sodium + Number(food.sodium);
                sub_total_protein = sub_total_protein + Number(food.protein);
            });

            total_calories = total_calories + sub_total_calories;
            total_fat = total_fat + sub_total_fat;
            total_fiber = total_fiber + sub_total_fiber;
            total_carb = total_carb + sub_total_carb;
            total_sodium = total_sodium + sub_total_sodium;
            total_protein = total_protein + sub_total_protein;
        });

        vm.totalMealItemData.total_calories = total_calories;
        vm.totalMealItemData.total_fat = total_fat;
        vm.totalMealItemData.total_fiber = total_fiber;
        vm.totalMealItemData.total_carb = total_carb;
        vm.totalMealItemData.total_sodium = total_sodium;
        vm.totalMealItemData.total_protein = total_protein;
    }

    vm.getdata_RecommendedMeal = function(){

        if(vm.recommended_meal_id){
            ShowService.get('api/v2/admin/food/meal_id', vm.recommended_meal_id).then(function(result){
                if(result.error==false){
                    vm.new_meal_name = result.response.meal_name;
                    vm.meal_time = result.response.meal_time;
                    vm.meal_template_id = result.response.meal_template_id;


                }
            });
        }

    }

    vm.editRecommendedMeal = function(){
        $("#updateMealModal").modal("show");
    }

    vm.updateRecommendedMeal = function(){
      if(vm.recommended_meal_id){
            var data = {
                "meal_name": vm.new_meal_name,
                "meal_time": vm.meal_time,
                "meal_template_id": vm.meal_template_id,
                "user_agent": "server"
            };

            StoreService.update('api/v2/admin/meal/update_meal', vm.recommended_meal_id, data).then(function(result) {
                if(result.error == false) {
                  var updated_obj = result.response;
                  vm.recommended_meal_id = updated_obj.id;
                  vm.meal_name = updated_obj.meal_name;
                  $("#updateMealModal").modal("hide");
                  showFlashMessage('Success','Meal name updated successfully','success');
                }
                else{
                    if(result.response.meal_exist==true){
                        vm.meal_exist = true;
                    }
                }

            });
        }
    }
}]);