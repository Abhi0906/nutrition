angular.module('app.routes').config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/');
    
    $stateProvider
        
        .state('/', {
            url: '/',
            templateUrl: '/load_user_plan',
            controller : 'MyPlanController',
            controllerAs : 'my_plan'
        })
        .state('create-meal', {
            url: '/create-meal',
            templateUrl: '/create_user_meal',
            params : { master_template_id : null,master_template_name:null},
            controller : 'UserMealController',
            controllerAs : 'user_meal'
        })
        .state('create-workout', {
            url: '/create-workout',
            templateUrl: '/create_user_workout',
            params: { edit_from: null, recommended_type_id : null, food_data: null, food_for_names: null, recommended_types: null, recommended_food_id: null },
            controller: 'UserWorkoutController',
            controllerAs: 'user_workout'
        });
        
});

angular.module('app.core').controller('MyPlanController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll','uiCalendarConfig', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll,uiCalendarConfig){
    var vm = this;
     vm.gender = 'male';
     vm.user_id=null;
      vm.meal_time="10:00:00";
     vm.week = [
      {dayNumber: '1', dayName: 'MON'},
      {dayNumber: '2', dayName: 'TUE'},
      {dayNumber: '3', dayName: 'WED'},
      {dayNumber: '4', dayName: 'THU'},
      {dayNumber: '5', dayName: 'FRI'},
      {dayNumber: '6', dayName: 'SAT'},
      {dayNumber: '7', dayName: 'SUN'},
    ];
    vm.select_days = ['1','2','3','4','5','6','7'];

    vm.eventSources = [];

    vm.uiConfig = {
      calendar:{
        height:'auto',
        dayNamesShort:['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat','Sun'],
        editable: true,
        header:{
          left: '',
          center: '',
          right: ''
        },


       
          // aspectRatio: 2,
        allDaySlot:false,
        defaultView: 'agendaWeek',
        handleWindowResize:true,
        eventClick: $scope.alertEventOnClick,
        eventDrop: $scope.alertOnDrop,
        eventResize: $scope.alertOnResize
      }
    };

     

     vm.getMDTemplate = function(){
      
      vm.getNutritionTemplate();

      vm.getMasterTemplate();

      vm.getWorkoutsTemplate();

     }

     vm.getNutritionTemplate =function(){

       ShowService.query('api/v3/my_plan/get_nutrition_template?gender='+vm.gender).then(function(results){
                   if(results.error==false){
                      vm.nutrition_templates = results.response.nutrition_templates;
                      vm.nutrition_template_id="0";
                    
                   }
              
       });

     }

     vm.getWorkoutsTemplate = function(){

      ShowService.query('api/v3/my_plan/get_workouts_template').then(function(results){
                   if(results.error==false){
                    
                     vm.workouts_templates = results.response.workout_templates;
                    
                   }
              
      });

     }
     vm.getMasterTemplate = function(){

        ShowService.query('api/v3/my_plan/get_master_templates').then(function(results){
                     if(results.error==false){
                       vm.master_templates = results.response.master_templates;

                     }
                
        });

     }

     vm.getMealTemplate = function(){
        if(parseInt(vm.nutrition_template_id) > 0){

          ShowService.query('api/v3/recommended_meal/meals/'+vm.nutrition_template_id).then(function(results){
                     if(results.error==false){
                    

                      var meal_data = [];
                        angular.forEach(results.response, function(value, key) {
                          this.push({
                            "id":value.id, 
                            "template_name":vm.getMealName(value.meal_foods_names)
                          });

                        }, meal_data);
                     }
                       vm.meal_templates = meal_data;
                       console.log(meal_data);
                
                });
        }
       }

       vm.getMealName = function(foodNames){

         var food_names = "";
          angular.forEach(foodNames, function(value, key) {
            food_names = food_names +", "+value;

          });
          food_names = food_names.slice(1);
          return "["+ food_names +"]";
       }
       vm.openDayTimeModel = function(template){
          vm.selectTemplate = template;
        $('#days_time_modal').modal('show');
       }
}]).directive('timePicker', function() {
    return {
    restrict: 'E',
    require: 'ngModel',
    replace: true,
    link: function(scope, element, attrs) {
    scope.timings = [
                { value: '', text: 'Select' },             
                { value: "00:00:00", text: "12:00 am"}, 
                // { value: "00:15:00", text: "12:15 am"}, 
                { value: "00:30:00", text: "12:30 am"}, 
                // { value: "00:45:00", text: "12:45 am"}, 
                { value: "01:00:00", text: "1:00 am"}, 
                // { value: "01:15:00", text: "1:15 am"}, 
                { value: "01:30:00", text: "1:30 am"}, 
                // { value: "01:45:00", text: "1:45 am"}, 
                { value: "02:00:00", text: "2:00 am"}, 
                // { value: "02:15:00", text: "2:15 am"}, 
                { value: "02:30:00", text: "2:30 am"}, 
                // { value: "02:45:00", text: "2:45 am"}, 
                { value: "03:00:00", text: "3:00 am"}, 
                // { value: "03:15:00", text: "3:15 am"}, 
                { value: "03:30:00", text: "3:30 am"}, 
                // { value: "03:45:00", text: "3:45 am"}, 
                { value: "04:00:00", text: "4:00 am"}, 
                // { value: "04:15:00", text: "4:15 am"}, 
                { value: "04:30:00", text: "4:30 am"}, 
                // { value: "04:45:00", text: "4:45 am"}, 
                { value: "05:00:00", text: "5:00 am"}, 
                // { value: "05:15:00", text: "5:15 am"}, 
                { value: "05:30:00", text: "5:30 am"}, 
                // { value: "05:45:00", text: "5:45 am"}, 
                { value: "06:00:00", text: "6:00 am"}, 
                // { value: "06:15:00", text: "6:15 am"}, 
                { value: "06:30:00", text: "6:30 am"}, 
                // { value: "06:45:00", text: "6:45 am"}, 
                { value: "07:00:00", text: "7:00 am"}, 
                // { value: "07:15:00", text: "7:15 am"}, 
                { value: "07:30:00", text: "7:30 am"}, 
                // { value: "07:45:00", text: "7:45 am"}, 
                { value: "08:00:00", text: "8:00 am"}, 
                // { value: "08:15:00", text: "8:15 am"}, 
                { value: "08:30:00", text: "8:30 am"}, 
                // { value: "08:45:00", text: "8:45 am"}, 
                { value: "09:00:00", text: "9:00 am"}, 
                // { value: "09:15:00", text: "9:15 am"}, 
                { value: "09:30:00", text: "9:30 am"}, 
                // { value: "09:45:00", text: "9:45 am"}, 
                { value: "10:00:00", text: "10:00 am"}, 
                // { value: "10:15:00", text: "10:15 am"}, 
                { value: "10:30:00", text: "10:30 am"}, 
                // { value: "10:45:00", text: "10:45 am"}, 
                { value: "11:00:00", text: "11:00 am"}, 
                // { value: "11:15:00", text: "11:15 am"}, 
                { value: "11:30:00", text: "11:30 am"}, 
                // { value: "11:45:00", text: "11:45 am"}, 
                { value: "12:00:00", text: "12:00 pm"}, 
                // { value: "12:15:00", text: "12:15 pm"}, 
                { value: "12:30:00", text: "12:30 pm"}, 
                // { value: "12:45:00", text: "12:45 pm"}, 
                { value: "13:00:00", text: "1:00 pm"}, 
                // { value: "13:15:00", text: "1:15 pm"}, 
                { value: "13:30:00", text: "1:30 pm"}, 
                // { value: "13:45:00", text: "1:45 pm"}, 
                { value: "14:00:00", text: "2:00 pm"}, 
                // { value: "14:15:00", text: "2:15 pm"}, 
                { value: "14:30:00", text: "2:30 pm"}, 
                // { value: "14:45:00", text: "2:45 pm"}, 
                { value: "15:00:00", text: "3:00 pm"}, 
                // { value: "15:15:00", text: "3:15 pm"}, 
                { value: "15:30:00", text: "3:30 pm"}, 
                // { value: "15:45:00", text: "3:45 pm"}, 
                { value: "16:00:00", text: "4:00 pm"}, 
                // { value: "16:15:00", text: "4:15 pm"}, 
                { value: "16:30:00", text: "4:30 pm"}, 
                // { value: "16:45:00", text: "4:45 pm"}, 
                { value: "17:00:00", text: "5:00 pm"}, 
                // { value: "17:15:00", text: "5:15 pm"}, 
                { value: "17:30:00", text: "5:30 pm"}, 
                // { value: "17:45:00", text: "5:45 pm"}, 
                { value: "18:00:00", text: "6:00 pm"}, 
                // { value: "18:15:00", text: "6:15 pm"}, 
                { value: "18:30:00", text: "6:30 pm"}, 
                // { value: "18:45:00", text: "6:45 pm"}, 
                { value: "19:00:00", text: "7:00 pm"}, 
                // { value: "19:15:00", text: "7:15 pm"}, 
                { value: "19:30:00", text: "7:30 pm"}, 
                // { value: "19:45:00", text: "7:45 pm"}, 
                { value: "20:00:00", text: "8:00 pm"}, 
                // { value: "20:15:00", text: "8:15 pm"}, 
                { value: "20:30:00", text: "8:30 pm"}, 
                // { value: "20:45:00", text: "8:45 pm"}, 
                { value: "21:00:00", text: "9:00 pm"}, 
                // { value: "21:15:00", text: "9:15 pm"}, 
                { value: "21:30:00", text: "9:30 pm"}, 
                // { value: "21:45:00", text: "9:45 pm"}, 
                { value: "22:00:00", text: "10:00 pm"}, 
                // { value: "22:15:00", text: "10:15 pm"}, 
                { value: "22:30:00", text: "10:30 pm"}, 
                // { value: "22:45:00", text: "10:45 pm"}, 
                { value: "23:00:00", text: "11:00 pm"}, 
                // { value: "23:15:00", text: "11:15 pm"}, 
                { value: "23:30:00", text: "11:30 pm"}, 
                // { value: "23:45:00", text: "11:45 pm"}, 
            ];
    },
    template: '<select class="form-control" >\
    <option value="{{time.value}}" ng-repeat="time in timings">{{time.text}}</option>\
    </select>'
    };
});