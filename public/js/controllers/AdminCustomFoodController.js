angular.module('app.routes').config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/create-custom-foods');
    
    $stateProvider
        
        .state('create-custom-foods', {
            url: '/create-custom-foods',
            templateUrl: '/admin_custom_foods_create',
            controller: 'AdminAddCustomFoodController',
            controllerAs: 'add_custom_food'
        });
});

angular.module('app.core').controller('AdminCustomFoodController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){
    var vm = this;
    
    
}]);