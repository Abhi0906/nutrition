angular.module('app.core').controller('DayViewPlanController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll','uiCalendarConfig', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll,uiCalendarConfig){

    var vm = this;
    var is_editable = ($scope.ap && $scope.ap.calendar_editable !="undefined" )?$scope.ap.calendar_editable:true;
    vm.user_id = ($scope.ap && $scope.ap.user_id !="undefined" )?$scope.ap.user_id:null;
    vm.meal_plan_id = $stateParams.id;
    vm.meal_plan_name = "";
    vm.nutritionExerciseData ={};
    vm.meal_time=[];
    vm.mas_temp ={};
    vm.template=[];

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    vm.events=[];
    vm.eventSources = [vm.events];

    vm.getEventDetail = function( data, jsEvent, view){

      var template_type = data.type;
      vm.template_name =data.title;
      vm.template_time = moment(data.start).format('h:mm: A');
        if(template_type == 'Meal'){
          ShowService.get('api/v2/admin/recommended_meal/item_data', data.meal_id).then(function(result){
                  vm.meal_item_data = result.response;
                  vm.getTotalMealItemData();
                  $('#mealDetailsModal').modal('show');
          });
        }

        if(template_type == 'Workout'){

           ShowService.get('api/v2/admin/workout', data.workout_id).then(function(result){
                vm.workout = result.response;
                $('#workoutDetailsModal').modal('show');

             });
        }

    };

    vm.updateTemplatePlan = function(event, delta, revertFunc, jsEvent, ui, view){

        var time = moment.duration(event.start).asHours();
        vm.mas_temp[event.id].meal_time=time.toString();
     };

    vm.uiConfig = {
      calendar:{
        height:'auto',

        editable: is_editable,
        header:{
          left: '',
          center: '',
          right: ''
        },
        // aspectRatio: 2,
        allDaySlot:false,
        defaultView: 'agendaDay',
        handleWindowResize:true,
        eventClick: vm.getEventDetail,
        eventDrop: vm.updateTemplatePlan,

      }
    };



    vm.getMDTemplate = function(){

      vm.getMasterTemplate();
      vm.getMealsTemplate();
      vm.getWorkoutsTemplate();
      $timeout(function() {
            vm.getMealPlanDetails(vm.meal_plan_id);
      }, 2000);

    }




    vm.getMealsTemplate =function(){


          ShowService.query('api/v2/admin/md_meals/meals').then(function(results){
                     if(results.error==false){


                      var meal_data = [];
                        angular.forEach(results.response.md_meals, function(value, key) {
                          this.push({
                            "id":value.id,
                            "template_name":vm.getMealName(value.meal_foods_names)
                          });

                        }, meal_data);
                     }
                       vm.meal_templates = meal_data;
                       vm.meal_templates[0].id=0;
                       vm.meal_templates[0].template_name='Select Meal Template';
                       var last_data = meal_data[meal_data.length-1];
                       vm.new_meal_id = parseInt(last_data.id)+1;

          });

    }

    vm.getWorkoutsTemplate = function(){

      ShowService.query('api/v2/admin/my_plan/get_workouts_template').then(function(results){
                   if(results.error==false){

                     vm.workouts_templates = results.response.workout_templates;
                      vm.workouts_templates[0].id=0;
                      vm.workouts_templates[0].template_name='Select Workout Template';

                   }

      });

     }
    vm.getMasterTemplate = function(){

        ShowService.query('api/v2/admin/my_plan/get_master_templates').then(function(results){
                     if(results.error==false){
                       vm.master_templates = results.response.master_templates;


                     }

        });

    }

    vm.getMealName = function(foodNames){

         var food_names = "";
          angular.forEach(foodNames, function(value, key) {
            food_names = food_names +", "+value;

          });
          food_names = food_names.slice(1);
          return "["+ food_names +"]";
    }

    vm.getMealPlanDetails =function(id){

      ShowService.get('api/v2/admin/meal_plan/get_mplan_details', id).then(function(result){
                vm.meal_plan_details = result.response.meal_plan_details;
               // console.log(vm.meal_plan_details.name);
                vm.meal_plan_name = vm.meal_plan_details.name;
                vm.selected_plan_id =parseInt(vm.meal_plan_details.id);
               //  vm.nutritionExerciseData =[];
                 vm.event_data=[];
                 if(result.response.meal_plan_details.nutrition_exercise_data.length > 0){
                     angular.forEach(result.response.meal_plan_details.nutrition_exercise_data, function(value, key) {
                             var planConfig = value.config;

                              if(value.master_template.type == 'Meal'){
                                 vm.mas_temp[value.master_template_id].meal_id = value.meal_id;
                              }
                              if(value.master_template.type == 'Workout'){
                                 vm.mas_temp[value.master_template_id].workout_id = value.workout_id;
                              }
                              var templateMealDay = [];
                             angular.forEach(planConfig, function (pc, key) {

                                 vm.mas_temp[value.master_template_id].meal_time = pc.meal_time.toString();
                                  console.log(pc);
                                  var meal_time =pc.meal_time;
                                  var timeObj =  vm.decimalToHours(meal_time);
                                  var hour = timeObj.hours;
                                  var min = timeObj.min;
                                  var day_number = pc.dayNumber;

                                  var eventsObj = {
                                        "id": value.master_template.id,
                                        "type": value.master_template.type,
                                        "meal_id" : value.meal_id,
                                        "workout_id" :value.workout_id,
                                        "title":value.master_template.template_name,
                                        "start": new Date(y, m, d, hour,min),
                                        "className": 'font-calendar'

                                   };

                                 vm.events.push(eventsObj);


                             });


                      });
                  }



      });
    }
    vm.decimalToHours = function(decimalNumber){

        var hours =0;
        var min=0;
        var time ={};
        hours = Math.floor(decimalNumber);
        min = Math.round(60*(decimalNumber- hours));
        time.hours = hours;
        time.min = min;
        console.log(time);
        return time;
    }

    vm.saveDayPlan = function(){


         var meal_plan_obj ={
            "nutritionExerciseData":vm.mas_temp
          };
         // console.log(vm.nutritionExerciseData); return false;
           StoreService.update('api/v2/admin/meal_plan/update_day_plan',vm.meal_plan_id,meal_plan_obj).then(function(result) {
            if(result.error == false){
              showFlashMessage('Update','Day Plan Update successfully','success');
              $state.go('day_plans');
            }
          },function(error) {
            console.log(error);
          });

    }

    vm.changedMeal = function(mtemplateObj){

      var meal_id = vm.mas_temp[mtemplateObj.id].meal_id;


      if(vm.events.length > 0){


            var updateEventArr=[];
            for(var i = vm.events.length - 1; i >= 0; i--){
                if(vm.events[i].id == mtemplateObj.id){

                    vm.events[i].meal_id = meal_id;
                    var updateEvent = angular.copy(vm.events[i]);
                    updateEventArr.push(updateEvent);
                    vm.events.splice(i,1);
                }
            }
            if(updateEventArr.length > 0){
                $timeout(function() {
                    angular.forEach(updateEventArr, function(newEvent, key){
                        vm.events.push(newEvent);
                    });

                },0);
            }

        }

    }

    vm.timeDisplayOnCal = function(template){

      var meal_time =vm.mas_temp[template.id].meal_time;
      var timeObj =  vm.decimalToHours(meal_time);
      var hour = timeObj.hours;
      var min = timeObj.min;
      var eventsObj = {
            "id": template.id,
            "type": template.type,
            "meal_id" : vm.mas_temp[template.id].meal_id,
            "workout_id" :vm.mas_temp[template.id].workout_id,
            "title":template.template_name,
            "start": new Date(y, m, d, hour,min),

       };
      if(vm.events.length > 0){

         var filtered = vm.events.filter(function (el, index, arr) {
             if(el.id == template.id){
                vm.events[index].start = new Date(y, m, d, hour,min);
                return el;
             }
          });
         if(filtered.length == 0) {
            vm.events.push(eventsObj);

         }

      }else{
          vm.events.push(eventsObj);
      }


    }



    vm.changedWorkout = function(mtemplateObj){

      var workout_id = vm.mas_temp[mtemplateObj.id].workout_id;

      if(vm.events.length > 0){


            var updateEventArr=[];
            for(var i = vm.events.length - 1; i >= 0; i--){
                if(vm.events[i].id == mtemplateObj.id){

                    vm.events[i].workout_id = workout_id;
                    var updateEvent = angular.copy(vm.events[i]);
                    updateEventArr.push(updateEvent);
                    vm.events.splice(i,1);
                }
            }
            if(updateEventArr.length > 0){
                $timeout(function() {
                    angular.forEach(updateEventArr, function(newEvent, key){
                        vm.events.push(newEvent);
                    });

                },0);
            }

        }

    }

    vm.getTotalMealItemData = function(){

        vm.totalMealItemData = {};

        var total_calories = 0;
        var total_fat = 0;
        var total_fiber = 0;
        var total_carb = 0;
        var total_sodium = 0;
        var total_protein = 0;

        angular.forEach( vm.meal_item_data, function(values, key){

            sub_total_calories = 0;
            sub_total_fat = 0;
            sub_total_fiber = 0;
            sub_total_carb = 0;
            sub_total_sodium = 0;
            sub_total_protein = 0;

            angular.forEach( values.meal_item_foods, function(food, key2){
                sub_total_calories = sub_total_calories + Number(food.calories);
                sub_total_fat = sub_total_fat + Number(food.total_fat);
                sub_total_fiber = sub_total_fiber + Number(food.dietary_fiber);
                sub_total_carb = sub_total_carb + Number(food.total_carb);
                sub_total_sodium = sub_total_sodium + Number(food.sodium);
                sub_total_protein = sub_total_protein + Number(food.protein);
            });

            total_calories = total_calories + sub_total_calories;
            total_fat = total_fat + sub_total_fat;
            total_fiber = total_fiber + sub_total_fiber;
            total_carb = total_carb + sub_total_carb;
            total_sodium = total_sodium + sub_total_sodium;
            total_protein = total_protein + sub_total_protein;
        });

        vm.totalMealItemData.total_calories = total_calories;
        vm.totalMealItemData.total_fat = total_fat;
        vm.totalMealItemData.total_fiber = total_fiber;
        vm.totalMealItemData.total_carb = total_carb;
        vm.totalMealItemData.total_sodium = total_sodium;
        vm.totalMealItemData.total_protein = total_protein;
    }

    vm.getWorkoutTitle = function(workout){
            if(workout == null){
              return true;
            }
            var class_experience ="text-primary";
            if(workout.experience == 'Beginner'){
              class_experience="text-primary";
            }
            else if(workout.experience == 'Intermediate'){
              class_experience="text-warning";
            }
            if(workout.experience == 'Advanced'){
              class_experience="text-success";
            }
            var icon = '<strong><i class="flaticon-stick5 '+ class_experience +'"></i></strong>';
            var atag = '<span class="tillana-font '+class_experience+'">'+workout.title +'</span>';
            var body_part = "<small>Body part</small>: <span><small>"+workout.body_part+"</small></span>";
            var video_count = "<small>Videos:</small>&nbsp;<span><small>"+workout.video_count+"</small></span>";
            var exercise_count = "<small>Exercise:</small>&nbsp;<span><small>"+workout.exercise_image_count+"</small></span>";

            var total_mins = "<small>Total Minutes:</small>&nbsp;<span><small>"+workout.total_mins+"</small></span>";

            return icon + "&nbsp;&nbsp;"+ atag + " &nbsp;&nbsp;&nbsp;&nbsp;"+body_part+",&nbsp;&nbsp;"+video_count+",&nbsp;&nbsp;"+exercise_count+",&nbsp;&nbsp;"+total_mins;
    }
    vm.getUserPlan = function(){

       ShowService.get('api/v2/admin/assign_plan/get_user_plan', vm.user_id).then(function(results) {
          var user_meal_plan = results.response.user_meal_plan;
          if(user_meal_plan){
              if(user_meal_plan.meal_plan_id == vm.meal_plan_id){
                 vm.showBtnAssign =false;
                 vm.showBtnUnAssign =true;
              }else{
                vm.showBtnAssign =true;
                vm.showBtnUnAssign =false;
              }

            // console.log(vm.showBtnAssign);
          }else{
             vm.showBtnAssign =true;
             vm.showBtnUnAssign =false;
          }

        });
    }

    vm.assignPlan = function(plan){

        var data = {
                "user_id": vm.user_id,
                "meal_plan_id": plan.id,
                "user_agent": "server"
            };
            var plan_type_name = "Complete Plan";

            var plan_type = plan.plan_type;
            if(plan_type == 'day_plan'){
              plan_type_name = "Day Plan";
            }


         // console.log(data);return false;
        StoreService.save('api/v2/admin/meal_plan/assign_plan_to_user', data).then(function(result) {
            if(result.error == false) {
              showFlashMessage('Assigned', plan_type_name+' Assigned Successfully','success');
              vm.showBtnAssign =false;
              vm.showBtnUnAssign =true;
          }
        });
    }

   vm.unassignPlan = function(plan){

    var data = {
                "user_id": vm.user_id,
                "meal_plan_id": plan.id,
                "user_agent": "server"
            };

    var plan_type_name = "Complete Plan";

    var plan_type = plan.plan_type;
    if(plan_type == 'day_plan'){
      plan_type_name = "Day Plan";
    }
     var msg = "Are you sure want to unassign this "+ plan_type_name+ " ?";

     showFlashMessageConfirmation("Unassign",msg,"info",function(result) {
          if (result) {
                StoreService.save('api/v2/admin/meal_plan/unassign_plan_to_user', data).then(function(results) {

                   vm.showBtnAssign = true;
                   vm.showBtnUnAssign=false;
                   showFlashMessage('Unassigned', plan_type_name+' Unassigned successfully','success');
                });
            }
    });

   }




}]);