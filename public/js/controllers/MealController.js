angular.module('app.core').controller('MealController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){
    var vm = this;
    vm.userId = null;
    vm.food_type = $stateParams.food_type;
   
    $('.back-top').trigger('click');
    vm.log_date = $scope.diary_index.log_date;
    vm.userId = $scope.diary_index.userId;
    vm.meal_food = $stateParams.meal_food;
    //console.log(vm.meal_food); return false;
    vm.meal_food_total = $stateParams.meal_food_total;

    vm.create_meal = function(){
       //vm.log_newfoodloader = true;
        var loggedNutritions = [];
        angular.forEach(vm.meal_food, function(value, key) {

                  loggedNutritions.push({
                      "nutrition_id":value.nutrition_id, 
                      "calories":value.calories, 
                      "serving_quantity":value.serving_quantity, 
                      "serving_size_unit":value.serving_size_unit,
                      "no_of_servings":value.no_of_servings,
                      "is_custom":value.is_custom
                    });
      
        });

       var log_date = moment(vm.log_date).format('YYYY-MM-DD');
       var meal_data = {

            "user_id" : vm.userId,
            "guid_server" : true,
            "food_type" : vm.food_type,
            "logged_date" : log_date,
            "meal_name" : vm.meal_name,
            "logged_nutritions": loggedNutritions
            
       };
        

        StoreService.save('api/v3/food/create_meal', meal_data).then(function(result) {
            if(result.error == false){

                if(result.error == false) {
                    showFlashMessage('Created','Meal created successfully','success');
                    $state.go('diary');
                }
                
            }
            //vm.log_newfoodloader = false;

        },function(error) {
            console.log(error);
          });

        return false;
    }

    vm.getMeals = function(){
        $('.back-top').trigger('click');
        vm.meal_loader = true;
        

        var params = vm.userId;
        ShowService.get('api/v3/food/meals', params).then(function(result){
          console.log(result);
          if(result.error == false){
            vm.meals = result.response;

            
          }

         vm.meal_loader = false;
          
        });

      }

    vm.deleteMeal = function(mealData){

      var id = mealData.id;
      var msg = "Are you sure want to delete this meal ?";

      showFlashMessageConfirmation("Delete",msg,"info",function(result) {
      if(result) {
              ShowService.remove('api/v3/food/delete_meal', id).then(function(results){

                    var index = vm.meals.indexOf(mealData);
                    vm.meals.splice(index, 1);

                    var index2 = $scope.diary_index.meals.indexOf(mealData);
                    $scope.diary_index.meals.splice(index, 1);

                    showFlashMessage('Delete','Meal deleted successfully','success');
                });
        }
      });

    }

}]);