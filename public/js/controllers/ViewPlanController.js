angular.module('app.core').controller('ViewPlanController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll','uiCalendarConfig', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll,uiCalendarConfig){

    var vm = this;
    var isFake = false;
    var isNotification=false;
    var is_editable = ($scope.ap && $scope.ap.calendar_editable !="undefined" )?$scope.ap.calendar_editable:true;
    vm.user_id = ($scope.ap && $scope.ap.user_id !="undefined" )?$scope.ap.user_id:null;
    vm.meal_plan_id = $stateParams.id;
    vm.meal_plan_name = "";
    vm.completePlanData ={};
    vm.daysTemplateArr = [];
    var workout_meal_time =5.00;
    var workout_interval =1;
    var meal_default_time =6.00;
    var meal_interval = 3;
    vm.atuo_timer_pre_workout=1;
    vm.atuo_timer_meal_1=1;
    vm.default_time ="";
    vm.select_day_plans = [{id: 'day-plan-1','day_plan_id':0,"day_plan_days":['1','2','3','4','5','6','7']}];
    vm.showBtnUnAssign =false;
    vm.addDayPlan = function() {
      var newItemNo = vm.select_day_plans.length+1;
      vm.select_day_plans.push({id: 'day-plan-'+newItemNo,'day_plan_id':0,"day_plan_days":['1','2','3','4','5','6','7']});
    };

   vm.removeDayPlan = function(sdp) {
     var index = vm.select_day_plans.indexOf(sdp);
     var day_plan_id = sdp.day_plan_id;
      vm.select_day_plans.splice(index, 1);

      if(day_plan_id >0 && vm.events.length > 0){

         for(var i = vm.events.length - 1; i >= 0; i--){
                  if(vm.events[i].day_plan_id == day_plan_id && vm.events[i].meal_template_type =="complete_day_template"){
                      vm.events.splice(i,1);
                  }
          }

      }
  };

    vm.week = [
      {dayNumber: '1', dayName: 'MON'},
      {dayNumber: '2', dayName: 'TUE'},
      {dayNumber: '3', dayName: 'WED'},
      {dayNumber: '4', dayName: 'THU'},
      {dayNumber: '5', dayName: 'FRI'},
      {dayNumber: '6', dayName: 'SAT'},
      {dayNumber: '7', dayName: 'SUN'},
    ];
    vm.select_days = ['1','2','3','4','5','6','7'];
    vm.meal_days =['1','2','3','4','5','6','7'];
    vm.meal_time=[];
    vm.notification=[];
    vm.mas_temp =[];
    vm.template=[];
    vm.selectNotifications=[];

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();


    vm.events=[];
    vm.eventSources = [vm.events];

    vm.getEventDetail = function( data, jsEvent, view){

      var template_type = data.type;
      console.log(data);

      vm.template_name =data.title;
      vm.template_time = moment(data.start).format('h:mm: A');
        if(template_type == 'Meal'){
          ShowService.get('api/v2/admin/recommended_meal/item_data', data.meal_id).then(function(result){
                  vm.meal_item_data = result.response;
                  vm.getTotalMealItemData();
                  $('#mealDetailsModal').modal('show');
          });
        }

        if(template_type == 'Workout'){

           ShowService.get('api/v2/admin/workout', data.workout_id).then(function(result){
                vm.workout = result.response;
                $('#workoutDetailsModal').modal('show');

             });
        }

    };

    vm.updateTemplatePlan = function(event, delta, revertFunc, jsEvent, ui, view){

      var time = moment.duration(event.start).asHours();
      var dayNumber = moment(event.start).day();
        if(dayNumber == 0){
          dayNumber =7;
        }
      var update_data = {
              "master_template_id": event.id,
              "time":  time.toFixed(1),
              "dayNumber": dayNumber,
              "configIndex":event.config_index

        };
         vm.meal_time[event.id][dayNumber]=time.toString();
         var dayArr = vm.meal_days[event.id];
         dayArr[event.config_index] = dayNumber.toString();
         vm.meal_days[event.id] = dayArr;

    };

    vm.uiConfig = {
      calendar:{
        height:'auto',
        editable: true,
        header:{
          left: '',
          center: '',
          right: ''
        },

        allDaySlot:false,
        defaultView: 'agendaWeek',
        handleWindowResize:true,
        eventClick: vm.getEventDetail,
        eventDrop: vm.updateTemplatePlan,

      }
    };



    vm.getMDTemplate = function(){

      vm.getMealPlans();
      vm.getMasterTemplate();
      vm.getMealsTemplate();
      vm.getWorkoutsTemplate();
      vm.getDayPlanTemplate();
      $timeout(function() {
          if(vm.showBtnUnAssign){
              vm.getUserMealPlanDetails(vm.meal_plan_id);
          }else{
              vm.getMealPlanDetails(vm.meal_plan_id);
          }


      }, 2000);



     }

    vm.getMealPlans = function(){
             ShowService.query('api/v2/admin/meal_plan/get_meal_plans?plan_type=complete_plan').then(function(result){
             vm.meal_plans = result.response;
            // console.log(vm.meal_plans);
            vm.selected_plan_id = vm.meal_plan_id;
            //console.log("select plan id",vm.selected_plan_id);
        });
    }


    vm.getMealsTemplate =function(){


          ShowService.query('api/v2/admin/md_meals/meals').then(function(results){
                     if(results.error==false){


                      var meal_data = [];
                        angular.forEach(results.response.md_meals, function(value, key) {
                          this.push({
                            "id":value.id,
                            "template_name":vm.getMealName(value.meal_foods_names)
                          });

                        }, meal_data);
                     }
                       vm.meal_templates = meal_data;
                       vm.meal_templates[0].id=0;
                       vm.meal_templates[0].template_name='Select Meal Template';
                       var last_data = meal_data[meal_data.length-1];
                       vm.new_meal_id = parseInt(last_data.id)+1;

                });

    }

    vm.getWorkoutsTemplate = function(){

      ShowService.query('api/v2/admin/my_plan/get_workouts_template').then(function(results){
                   if(results.error==false){

                     vm.workouts_templates = results.response.workout_templates;
                      vm.workouts_templates[0].id=0;
                      vm.workouts_templates[0].template_name='Select Workout Template';

                   }

      });

     }
    vm.getMasterTemplate = function(){

        ShowService.query('api/v2/admin/my_plan/get_master_templates').then(function(results){
                     if(results.error==false){
                       vm.master_templates = results.response.master_templates;

                       angular.forEach(results.response.master_templates, function(value, key) {

                           vm.meal_days[value.id] = ['0','1','2','3','4','5','6','7'];
                           vm.selectNotifications[value.id]=['0','1','2','3','4','5','6','7'];
                           vm.meal_time[value.id] = [];
                          // vm.notification[value.id]=[];

                           vm.dynamicWeekDays(value);
                           var default_time = vm.getMealDefaultTime(value);
                          // console.log(default_time);
                           angular.forEach(vm.masterTemplateWeeks, function (weekDays, key) {
                               vm.meal_time[value.id][weekDays.dayNumber] = default_time.toString();
                              // vm.notification[value.id][weekDays.dayNumber]=1;
                           });


                          });

                     }

        });

    }
    vm.getMealDefaultTime = function(templateObj){
      var template_tech_name = templateObj.tech_name;

      if(template_tech_name == 'pre_workout_meal'){
       vm.default_time = workout_meal_time;
      }else if(template_tech_name == 'resistance_trainning' ||
               template_tech_name == 'cardio_trainning'
               ){
        vm.default_time= parseFloat(vm.default_time)+parseInt(workout_interval);
      }else if(template_tech_name == 'meal_1'){
         vm.default_time=meal_default_time;
      }else if(template_tech_name == 'meal_2' ||
               template_tech_name == 'meal_3' ||
               template_tech_name == 'meal_4' ||
               template_tech_name == 'meal_5' ||
               template_tech_name == 'meal_6' ){
        vm.default_time= parseFloat(vm.default_time)+parseInt(meal_interval);
      }
      return vm.default_time;
    }
    vm.getDayPlanTemplate = function(){
      vm.day_plan_templates=[];
         ShowService.query('api/v2/admin/meal_plan/get_meal_plans?plan_type=day_plan&details=1').then(function(result){

             result.response.push({
              id: 0,
              name: 'Select Day Plan Template'
            });
            vm.day_plan_templates = result.response;


           // console.log(vm.day_plan_templates);

        });

    }

    vm.getMealName = function(foodNames){

         var food_names = "";
          angular.forEach(foodNames, function(value, key) {
            food_names = food_names +", "+value;

          });
          food_names = food_names.slice(1);
          return "["+ food_names +"]";
    }

    vm.getMealPlanDetails =function(id){

      ShowService.get('api/v2/admin/meal_plan/get_mplan_details', id).then(function(result){
                vm.meal_plan_details = result.response.meal_plan_details;
                vm.setDayPlanDetails(result.response.day_plan_details);

                vm.meal_plan_name = vm.meal_plan_details.name;
                vm.selected_plan_id =parseInt(vm.meal_plan_details.id);
                 vm.event_data=[];
                 if(result.response.meal_plan_details.nutrition_exercise_data.length > 0){
                     vm.atuo_timer_pre_workout=0;
                     vm.atuo_timer_meal_1=0;
                     angular.forEach(result.response.meal_plan_details.nutrition_exercise_data, function(value, key) {
                             var planConfig = value.config;
                              if(value.master_template.type == 'Meal' && value.meal_template_type == 'complete_meal_template'){
                                 vm.mas_temp[value.master_template_id].meal_id = value.meal_id;
                              }
                              if(value.master_template.type == 'Workout' && value.meal_template_type == 'complete_meal_template'){
                                 vm.mas_temp[value.master_template_id].workout_id = value.workout_id;
                              }
                              var templateMealDay = [];
                              var notification_arr = [];
                              angular.forEach(planConfig, function (pc, key) {
                                if(value.meal_template_type == 'complete_meal_template'){
                                   vm.meal_time[value.master_template_id][pc.dayNumber] = pc.meal_time.toString();
                                   //vm.notification[value.master_template_id][pc.dayNumber] = pc.notification;
                                    if(pc.notification){
                                        notification_arr.push(pc.dayNumber.toString());
                                    }
                                   templateMealDay.push(pc.dayNumber.toString());
                                    vm.daysTemplateArr.push(value.master_template.id + "-" + pc.dayNumber);

                                }

                                  var meal_time =pc.meal_time;
                                  var timeObj =  vm.decimalToHours(meal_time);
                                  var hour = timeObj.hours;
                                  var min = timeObj.min;
                                  var day_number = pc.dayNumber;
                                  if(pc.dayNumber == 7){
                                    day_number =0;
                                  }
                                  var eventsObj = {
                                        "id": value.master_template.id,
                                        "type": value.master_template.type,
                                        "meal_id" : value.meal_id,
                                        "workout_id" :value.workout_id,
                                        "day_plan_id":value.day_plan_id,
                                        "title":value.master_template.template_name,
                                        "start": new Date(y, m, d, hour,min),
                                        "dow":[day_number],
                                        "meal_template_type":value.meal_template_type,
                                        "config_index":key
                                   };

                                  vm.events.push(eventsObj);


                             });
                            // console.log(vm.events);

                            if(value.meal_template_type == 'complete_meal_template'){
                              vm.meal_days[value.master_template_id] = templateMealDay;
                                vm.selectNotifications[value.master_template_id] = notification_arr;
                              vm.resetNutritionExerciseObj(value,templateMealDay,vm.meal_time[value.master_template_id],notification_arr);
                            }

                      });
                  }



      });
    }


    vm.getUserMealPlanDetails =function(id){

        var params = id;

        params = params+'?user_id='+vm.user_id;

        ShowService.get('api/v2/admin/meal_plan/get_user_mplan_details', params).then(function(result){
            vm.meal_plan_details = result.response.meal_plan_details.new_meal_plan;
            vm.setDayPlanDetails(result.response.day_plan_details);

            vm.meal_plan_name = vm.meal_plan_details.name;
            vm.selected_plan_id =parseInt(vm.meal_plan_details.id);
            vm.event_data=[];
             if(result.response.meal_plan_details.nutrition_exercise_data.length > 0){
                 vm.atuo_timer_pre_workout=0;
                 vm.atuo_timer_meal_1=0;
                 angular.forEach(result.response.meal_plan_details.nutrition_exercise_data, function(value, key) {
                     var planConfig = value.config;
                     if(value.master_template.type == 'Meal' && value.meal_template_type == 'complete_meal_template'){
                         vm.mas_temp[value.master_template_id].meal_id = value.meal_id;
                    }
                     if(value.master_template.type == 'Workout' && value.meal_template_type == 'complete_meal_template'){
                         vm.mas_temp[value.master_template_id].workout_id = value.workout_id;
                     }
                     var templateMealDay = [];
                     var notification_arr = [];
                     angular.forEach(planConfig, function (pc, key) {
                         if(value.meal_template_type == 'complete_meal_template'){
                             vm.meal_time[value.master_template_id][pc.dayNumber] = pc.meal_time.toString();
                             //vm.notification[value.master_template_id][pc.dayNumber] = pc.notification;
                             if(pc.notification){
                                 notification_arr.push(pc.dayNumber.toString());
                             }
                            templateMealDay.push(pc.dayNumber.toString());
                             vm.daysTemplateArr.push(value.master_template.id + "-" + pc.dayNumber);

                        }

                         var meal_time =pc.meal_time;
                         var timeObj =  vm.decimalToHours(meal_time);
                         var hour = timeObj.hours;
                         var min = timeObj.min;
                         var day_number = pc.dayNumber;
                         if(pc.dayNumber == 7){
                             day_number =0;
                         }
                         var eventsObj = {
                             "id": value.master_template.id,
                             "type": value.master_template.type,
                             "meal_id" : value.meal_id,
                             "workout_id" :value.workout_id,
                             "day_plan_id":value.day_plan_id,
                             "title":value.master_template.template_name,
                             "start": new Date(y, m, d, hour,min),
                             "dow":[day_number],
                             "meal_template_type":value.meal_template_type,
                            "config_index":key
                         };

                         vm.events.push(eventsObj);


                     });
                     // console.log(vm.events);

                    if(value.meal_template_type == 'complete_meal_template'){
                         vm.meal_days[value.master_template_id] = templateMealDay;
                         vm.selectNotifications[value.master_template_id] = notification_arr;
                         vm.resetNutritionExerciseObj(value,templateMealDay,vm.meal_time[value.master_template_id],notification_arr);
                     }

                 });
             }



        });
    }

    vm.setDayPlanDetails =function(day_plan_details){

       if(day_plan_details.length >0){
         vm.select_day_plans = [];
           angular.forEach(day_plan_details, function(value, key) {
           //console.log("day_plan_value",value);
              var dayConfig = value.config;
              var meal_days = [];
                angular.forEach(dayConfig, function (pc, key) {
                meal_days.push(pc.dayNumber.toString());
                });

             var day_plan_obj = {
                id:"day-plan-"+key,
                day_plan_id:value.day_plan_id,
                day_plan_days:meal_days
             };
              vm.select_day_plans.push(day_plan_obj);
           });

          // console.log("day_plan_obj",vm.select_day_plans);
        }

    }

    vm.openDayTimeModel = function(template,index){
        vm.selectTemplate = template;
        vm.selectedAll = true;
        vm.selectIndex =index;
        vm.dynamicWeekDays(template);
        $('#daysTimeModal').modal('show');
    }

    vm.openDayModel = function(sdpObj){
      vm.selectDayPlanObj =sdpObj;
      $('#daysModal').modal('show');
    }

    vm.checkAll = function (dayNumber) {

        if(dayNumber == 0){
            if (isFake) {
              isFake = false;
              return;

            }

          //  console.log(vm.template[vm.selectTemplate.id].meal_day.indexOf(dayNumber));

            if(vm.template[vm.selectTemplate.id].meal_day.indexOf(dayNumber) !== -1)
            {
              vm.template[vm.selectTemplate.id].meal_day=['0','1','2','3','4','5','6','7'];
            }else{
              vm.template[vm.selectTemplate.id].meal_day=[];
          }

        }else{

          if(vm.template[vm.selectTemplate.id].meal_day.indexOf('0') !== -1)
          {
              vm.template[vm.selectTemplate.id].meal_day = vm.template[vm.selectTemplate.id].meal_day.slice(1);
              isFake = true;
          }
        }
    };

    vm.checkAllNotification = function (dayNumber) {
        if(dayNumber == 0) {
            if (isNotification) {
              isNotification = false;
              return;
            }

            if(vm.template[vm.selectTemplate.id].notification.indexOf(dayNumber) !== -1)
            {
              vm.template[vm.selectTemplate.id].notification=['0','1','2','3','4','5','6','7'];
            } else {
              vm.template[vm.selectTemplate.id].notification=[];
            }
        } else {
          if(vm.template[vm.selectTemplate.id].notification.indexOf('0') !== -1)
          {
            vm.template[vm.selectTemplate.id].notification = vm.template[vm.selectTemplate.id].notification.slice(1);
            isNotification = true;
          }
        }
    };

    vm.changeAllTime = function(dayNumber){
      if(dayNumber == 0){
       // console.log("changeTime");
        var all_meal_time = vm.template[vm.selectTemplate.id][dayNumber].meal_time;

        angular.forEach(vm.masterTemplateWeeks, function(value, key) {
            vm.template[value.master_template_id][value.dayNumber].meal_time = all_meal_time;
        });

      }
    }

    vm.creatTemplateData = function(){
    	var meal_id=null;
    	var workout_id =null;
      console.log(vm.mas_temp[vm.selectTemplate.id].meal_id);
      if((typeof vm.mas_temp[vm.selectTemplate.id].meal_id != 'undefined' &&  vm.mas_temp[vm.selectTemplate.id].meal_id > 0) ||
         (typeof vm.mas_temp[vm.selectTemplate.id].workout_id != 'undefined' && vm.mas_temp[vm.selectTemplate.id].workout_id > 0)
        ){

           // console.log(vm.atuo_timer_pre_workout);
            vm.completePlanData[vm.selectTemplate.id] ={};
          	if(typeof vm.mas_temp[vm.selectTemplate.id].meal_id != 'undefined'){
          		meal_id = vm.mas_temp[vm.selectTemplate.id].meal_id;
          	}

          	if(typeof vm.mas_temp[vm.selectTemplate.id].workout_id != 'undefined'){
          		workout_id = vm.mas_temp[vm.selectTemplate.id].workout_id;
          	}

            vm.meal_days[vm.selectTemplate.id] = angular.copy(vm.template[vm.selectTemplate.id].meal_day);
          	var item_plan_data = {
               "master_template_id" : vm.selectTemplate.id,
               "meal_id" : meal_id,
               "workout_id" :workout_id,
               "template_days":vm.template[vm.selectTemplate.id].meal_day,
               "template_time":vm.template[vm.selectTemplate.id],
               "notification":vm.template[vm.selectTemplate.id].notification,

               "action":"add"

          	};
          //	console.log(item_plan_data);return false;
          	 vm.completePlanData[vm.selectTemplate.id]= item_plan_data;



              if(vm.events.length > 0){

                 for(var i = vm.events.length - 1; i >= 0; i--){
                        if(vm.events[i].id == vm.selectTemplate.id && vm.events[i].meal_template_type =="complete_meal_template"){
                            vm.events.splice(i,1);
                        }
                 }
              }


             angular.forEach(vm.template[vm.selectTemplate.id].meal_day,function(dayNumber,key){
                var meal_time = vm.template[vm.selectTemplate.id][dayNumber].meal_time;
                var timeObj =  vm.decimalToHours(meal_time);
                var hour = timeObj.hours;
                var min = timeObj.min;
                var day_number = dayNumber;


                vm.daysTemplateArr.push(vm.selectTemplate.id + "-" + dayNumber);


                if(dayNumber == 7){
                    day_number =0;
                 }
                var eventsObj = {
                    "id": vm.selectTemplate.id,
                    "type": vm.selectTemplate.type,
                    "meal_id" : meal_id,
                    "workout_id" :workout_id,
                    "day_plan_id":null,
                    "meal_template_type":"complete_meal_template",
                    "title":vm.selectTemplate.template_name,
                    "start": new Date(y, m, d, hour,min),
                    "dow":[day_number],
                    "config_index":key
                 };
                vm.meal_time[vm.selectTemplate.id][dayNumber] = angular.copy(vm.template[vm.selectTemplate.id][dayNumber].meal_time);
                vm.events.push(eventsObj);

             });
             if(vm.selectTemplate.tech_name == "pre_workout_meal" && vm.atuo_timer_pre_workout){
              vm.setAutoTimerPreWorkout(vm.template[vm.selectTemplate.id]);
              vm.setAutoTimerMeals(vm.template[vm.selectTemplate.id],1);
             }

            if(vm.selectTemplate.tech_name == "meal_1" && vm.atuo_timer_meal_1){
              vm.setAutoTimerMeals(vm.template[vm.selectTemplate.id],0);
            }
          }
           $('#daysTimeModal').modal('hide');
    }

    vm.setAutoTimerPreWorkout = function(selectedDayTimes){
     // console.log("Call Workout");
      var masterTemplates = vm.master_templates;

       angular.forEach(masterTemplates, function(value, key) {


         if(value.tech_name == 'resistance_trainning' || value.tech_name == 'cardio_trainning' || value.tech_name == 'post_workout_meal')
          {

            angular.forEach(selectedDayTimes.meal_day, function (dayNumber, key) {
              var p_default_time = selectedDayTimes[dayNumber].meal_time;
            //    console.log(p_default_time);
              if(value.tech_name == 'resistance_trainning'){
                   p_default_time = parseFloat(p_default_time)+1;
              }
              if(value.tech_name == 'cardio_trainning'){
               p_default_time = parseFloat(p_default_time)+2;
              }

              if(value.tech_name == 'post_workout_meal'){
               p_default_time = parseFloat(p_default_time)+2;
              }

              vm.meal_time[value.id][dayNumber] = p_default_time.toString();


            });
          }

        });


    }

    vm.setAutoTimerMeals = function(selectedDayTimes,ismeal1inclue){

       var masterTemplates = vm.master_templates;
       var select_temp_time1 = {};
       angular.forEach(masterTemplates, function(templateObj, key) {

           if(
            (templateObj.tech_name == 'meal_1' && ismeal1inclue) ||
            templateObj.tech_name == 'meal_2' ||
            templateObj.tech_name == 'meal_3' ||
            templateObj.tech_name == 'meal_4' ||
            templateObj.tech_name == 'meal_5' ||
            templateObj.tech_name == 'meal_6')
            {

              angular.forEach(selectedDayTimes.meal_day, function (dayNumber, key) {

                var m_default_time = parseFloat(angular.copy(vm.meal_time[templateObj.id][dayNumber]));
               // console.log("\n\tDN :" + dayNumber + " - ML : "+ templateObj.tech_name);
                var select_temp_time = parseFloat(selectedDayTimes[dayNumber].meal_time);
                //console.log("m_default_time",m_default_time);
                //console.log("select_temp_time : ",select_temp_time);

                 if(m_default_time >= select_temp_time){

                    if (typeof select_temp_time1[dayNumber] == "undefined"){
                         //console.log("setting default ",templateObj.tech_name);
                        select_temp_time1[dayNumber] = parseFloat(select_temp_time);
                    }
                     //console.log("select_temp_time 1 : ",select_temp_time1[dayNumber]);

                    select_temp_time1[dayNumber] += 3;
                   // console.log("DN :" + dayNumber + " - ML : "+ templateObj.tech_name +" - Ti: "+select_temp_time1[dayNumber]);
                     if(parseFloat(select_temp_time1[dayNumber])+3 >= 24){
                        select_temp_time1[dayNumber] = parseFloat(parseFloat(select_temp_time1[dayNumber])%24);
                     }
                    vm.meal_time[templateObj.id][dayNumber] = select_temp_time1[dayNumber].toString();


                 }

              });


            }

      });


    }

    vm.decimalToHours = function(decimalNumber){

        var hours =0;
        var min=0;
        var time ={};
        hours = Math.floor(decimalNumber);
        min = Math.round(60*(decimalNumber- hours));
        time.hours = hours;
        time.min = min;
      //  console.log(time);
        return time;
    }

    vm.savePlan = function(){
       // console.log(vm.select_day_plans); return false;
         var meal_plan_obj ={
            "completePlanData":vm.completePlanData,
            "dayPlanData":vm.select_day_plans,
            "daysTemplateArr" : vm.daysTemplateArr
          };
         // console.log(vm.completePlanData); return false;
           StoreService.update('api/v2/admin/meal_plan/update_meal_plan',vm.meal_plan_id,meal_plan_obj).then(function(result) {
            if(result.error == false){
              showFlashMessage('Update','Complete Plan Update successfully','success');
              $state.go('complete_plans');
            }
          },function(error) {
            console.log(error);
          });

    }

    vm.updateUserMealPlan = function(){
        var user_meal_plan_obj ={
            "user_id":vm.user_id,
            "completePlanData":vm.completePlanData,
            "dayPlanData":vm.select_day_plans,
            "daysTemplateArr" : vm.daysTemplateArr
        };

        
        StoreService.update('api/v2/admin/meal_plan/update_user_meal_plan',vm.meal_plan_id,user_meal_plan_obj).then(function(result) {
            if(result.error == false){
                showFlashMessage('Update','Complete Plan Update successfully','success');
                $state.go('list_plans');
            }
        },function(error) {
            console.log(error);
        });
    }

    vm.resetNutritionExerciseObj = function(neData,template_days,template_time,notification){
      vm.completePlanData[neData.master_template_id] ={};
      var item_plan_data = {
         "master_template_id" : neData.master_template_id,
         "meal_id" : neData.meal_id,
         "workout_id" :neData.workout_id,
         "template_days":template_days,
         "template_time":template_time,
         "notification":notification,
         "action":"update"

         };
         vm.completePlanData[neData.master_template_id]= item_plan_data;
        // console.log("reset nutriton exercise obj",vm.completePlanData);


    }
    vm.dynamicWeekDays = function(template){
      vm.masterTemplateWeeks =[
        {dayNumber: '0', dayName: 'ALL',master_template_id:template.id},
        {dayNumber: '1', dayName: 'MON',master_template_id:template.id},
        {dayNumber: '2', dayName: 'TUE',master_template_id:template.id},
        {dayNumber: '3', dayName: 'WED',master_template_id:template.id},
        {dayNumber: '4', dayName: 'THU',master_template_id:template.id},
        {dayNumber: '5', dayName: 'FRI',master_template_id:template.id},
        {dayNumber: '6', dayName: 'SAT',master_template_id:template.id},
        {dayNumber: '7', dayName: 'SUN',master_template_id:template.id}
      ];
        //console.log(vm.masterTemplateWeeks);
    }

    vm.changedPlan = function(){

       vm.getMealPlanDetails(vm.selected_plan_id);
    }

    vm.changedMeal = function(mtemplateObj){

      var meal_id = vm.mas_temp[mtemplateObj.id].meal_id;
       if(vm.events.length > 0){

        var nutEXObj = vm.completePlanData[mtemplateObj.id];
        vm.completePlanData[mtemplateObj.id].meal_id =meal_id;
        var updateEventArr=[];
        for(var i = vm.events.length - 1; i >= 0; i--){
            if(vm.events[i].id == mtemplateObj.id && vm.events[i].meal_template_type =="complete_meal_template"){

                vm.events[i].meal_id = meal_id;
                var updateEvent = angular.copy(vm.events[i]);
                updateEventArr.push(updateEvent);
                vm.events.splice(i,1);
            }
        }
         if(updateEventArr.length > 0){
             $timeout(function() {
                 angular.forEach(updateEventArr, function(newEvent, key){
                     vm.events.push(newEvent);
                 });

             },0);
         }

      }

    }



    vm.changedWorkout = function(mtemplateObj){

      var workout_id = vm.mas_temp[mtemplateObj.id].workout_id;

      if(vm.completePlanData[mtemplateObj.id] && typeof vm.completePlanData[mtemplateObj.id].workout_id != "undefined"){
         vm.completePlanData[mtemplateObj.id].workout_id =workout_id;

      }

      if(vm.events.length > 0){


            var updateEventArr=[];
            for(var i = vm.events.length - 1; i >= 0; i--){
                if(vm.events[i].id == mtemplateObj.id && vm.events[i].meal_template_type =="complete_meal_template"){

                    vm.events[i].workout_id = workout_id;
                    var updateEvent = angular.copy(vm.events[i]);
                    updateEventArr.push(updateEvent);
                    vm.events.splice(i,1);
                }
            }
            if(updateEventArr.length > 0){
                $timeout(function() {
                    angular.forEach(updateEventArr, function(newEvent, key){
                        vm.events.push(newEvent);
                    });

                },0);
            }

        }

    }

    vm.getTotalMealItemData = function(){

        vm.totalMealItemData = {};

        var total_calories = 0;
        var total_fat = 0;
        var total_fiber = 0;
        var total_carb = 0;
        var total_sodium = 0;
        var total_protein = 0;

        angular.forEach( vm.meal_item_data, function(values, key){

            sub_total_calories = 0;
            sub_total_fat = 0;
            sub_total_fiber = 0;
            sub_total_carb = 0;
            sub_total_sodium = 0;
            sub_total_protein = 0;

            angular.forEach( values.meal_item_foods, function(food, key2){
                sub_total_calories = sub_total_calories + Number(food.calories);
                sub_total_fat = sub_total_fat + Number(food.total_fat);
                sub_total_fiber = sub_total_fiber + Number(food.dietary_fiber);
                sub_total_carb = sub_total_carb + Number(food.total_carb);
                sub_total_sodium = sub_total_sodium + Number(food.sodium);
                sub_total_protein = sub_total_protein + Number(food.protein);
            });

            total_calories = total_calories + sub_total_calories;
            total_fat = total_fat + sub_total_fat;
            total_fiber = total_fiber + sub_total_fiber;
            total_carb = total_carb + sub_total_carb;
            total_sodium = total_sodium + sub_total_sodium;
            total_protein = total_protein + sub_total_protein;
        });

        vm.totalMealItemData.total_calories = total_calories;
        vm.totalMealItemData.total_fat = total_fat;
        vm.totalMealItemData.total_fiber = total_fiber;
        vm.totalMealItemData.total_carb = total_carb;
        vm.totalMealItemData.total_sodium = total_sodium;
        vm.totalMealItemData.total_protein = total_protein;
    }

    vm.getWorkoutTitle = function(workout){
            if(workout == null){
              return true;
            }
            var class_experience ="text-primary";
            if(workout.experience == 'Beginner'){
              class_experience="text-primary";
            }
            else if(workout.experience == 'Intermediate'){
              class_experience="text-warning";
            }
            if(workout.experience == 'Advanced'){
              class_experience="text-success";
            }
            var icon = '<strong><i class="flaticon-stick5 '+ class_experience +'"></i></strong>';
            var atag = '<span class="tillana-font '+class_experience+'">'+workout.title +'</span>';
            var body_part = "<small>Body part</small>: <span><small>"+workout.body_part+"</small></span>";
            var video_count = "<small>Videos:</small>&nbsp;<span><small>"+workout.video_count+"</small></span>";
            var exercise_count = "<small>Exercise:</small>&nbsp;<span><small>"+workout.exercise_image_count+"</small></span>";

            var total_mins = "<small>Total Minutes:</small>&nbsp;<span><small>"+workout.total_mins+"</small></span>";

            return icon + "&nbsp;&nbsp;"+ atag + " &nbsp;&nbsp;&nbsp;&nbsp;"+body_part+",&nbsp;&nbsp;"+video_count+",&nbsp;&nbsp;"+exercise_count+",&nbsp;&nbsp;"+total_mins;
    }
    vm.saveNewPlan  = function(){

      var msg = "Are you sure want to save this new meal plan ?";

        showFlashMessageConfirmation("New Plan",msg,"info",function(result) {
              if (result) {

                }
        });

    }

    vm.getUserPlan = function(){

       ShowService.get('api/v2/admin/assign_plan/get_user_plan', vm.user_id).then(function(results) {
          var user_meal_plan = results.response.user_meal_plan;
          if(user_meal_plan){
              if(user_meal_plan.meal_plan_id == vm.meal_plan_id){
                 vm.showBtnAssign =false;
                 vm.showBtnUnAssign =true;
              }else{
                vm.showBtnAssign =true;
                vm.showBtnUnAssign =false;
              }

            // console.log(vm.showBtnAssign);
          }else{
             vm.showBtnAssign =true;
             vm.showBtnUnAssign =false;
          }

        });
    }

    vm.assignPlan = function(plan){

        var data = {
                "user_id": vm.user_id,
                "meal_plan_id": plan.id,
                "user_agent": "server"
            };
            var plan_type_name = "Complete Plan";

            var plan_type = plan.plan_type;
            if(plan_type == 'day_plan'){
              plan_type_name = "Day Plan";
            }


         // console.log(data);return false;
        StoreService.save('api/v2/admin/meal_plan/assign_plan_to_user', data).then(function(result) {
            if(result.error == false) {
              showFlashMessage('Assigned', plan_type_name+' Assigned Successfully','success');
              vm.showBtnAssign =false;
              vm.showBtnUnAssign =true;
          }
        });
    }

    vm.unassignPlan = function(plan){

     var data = {
                "user_id": vm.user_id,
                "meal_plan_id": plan.id,
                "user_agent": "server"
            };

            var plan_type_name = "Complete Plan";

            var plan_type = plan.plan_type;
            if(plan_type == 'day_plan'){
              plan_type_name = "Day Plan";
            }
     var msg = "Are you sure want to unassign this "+ plan_type_name+ " ?";

     showFlashMessageConfirmation("Unassign",msg,"info",function(result) {
          if (result) {
                StoreService.save('api/v2/admin/meal_plan/unassign_plan_to_user', data).then(function(results) {

                   vm.showBtnAssign = true;
                   vm.showBtnUnAssign=false;
                   showFlashMessage('Unassigned', plan_type_name+' Unassigned successfully','success');
                });
            }
    });

   }

  vm.displayDayPlanCal = function(selectDayPlan){
   // console.log(selectDayPlan);
    var day_plan_id =selectDayPlan.day_plan_id;
    var day_number_arr = selectDayPlan.day_plan_days;
    var day_numbers = day_number_arr.map(function (x) {
          return parseInt(x, 10);
      });
     var Windex = day_numbers.indexOf(7);
      if (Windex !== -1) {
          day_numbers[Windex] =0;
      }
    //  console.log(day_plan_id);

       $('#daysModal').modal('hide');


        if(day_plan_id >0){
            var day_plan_details = vm.day_plan_templates.filter(function (el, index, arr) {
                                   if(el.id == day_plan_id ){
                                    return el;
                                   }
                                });
         //   console.log(day_plan_details);
           if(vm.events.length == 0){
              vm.pushDataOnCal(day_plan_id,day_plan_details[0],day_numbers)
           }else{
              var selectedPlanIndex =vm.select_day_plans.indexOf(selectDayPlan);
             for(var i = vm.select_day_plans.length - 1; i >= 0; i--){
              var dayPlanData = vm.select_day_plans[i];


              angular.forEach(day_numbers, function(dayNumber, key){
                  var dayIndex = dayPlanData.day_plan_days.indexOf(dayNumber.toString());
                      //console.log(dayNumber + "-- dayIndex :: " + dayIndex);
                  if(dayIndex != -1 && selectedPlanIndex != i){
                    vm.select_day_plans[i].day_plan_days.splice(dayIndex,1);
                  }
              });

            }
            //  console.log(vm.select_day_plans);
              for(var i = vm.events.length - 1; i >= 0; i--){
                  if(vm.events[i].day_plan_id == day_plan_id && vm.events[i].meal_template_type =="complete_day_template"){
                      vm.events.splice(i,1);
                  }
              }


              vm.pushDataOnCal(day_plan_id,day_plan_details[0],day_numbers);

           }

        }
  }

  vm.pushDataOnCal = function(day_plan_id,day_plan_details,day_number){
     angular.forEach(day_plan_details.nutrition_exercise_data, function(value, key) {
                      var dayConfig = value.config;

                      var day_meal_time = dayConfig[0].meal_time;
                      var timeObj =  vm.decimalToHours(day_meal_time);
                      var hour = timeObj.hours;
                      var min = timeObj.min;
                      var eventsObj = {
                          "id": value.master_template.id,
                          "day_plan_id":day_plan_id,
                          "type": value.master_template.type,
                          "meal_id" : value.meal_id,
                          "workout_id" :value.workout_id,
                          "meal_template_type":"complete_day_template",
                          "title":value.master_template.template_name,
                          "start": new Date(y, m, d, hour,min),
                          "dow":day_number,
                          "config_index":key
                      };
                      vm.events.push(eventsObj);

            });
         // console.log("events",vm.events);

  }


}]);