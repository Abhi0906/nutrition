/**
 * Diary controller for displaying Diary for patient.
 */
angular.module('app.core').controller('DiaryController', ['ShowService','StoreService','$scope',function(ShowService,StoreService,$scope){
    var vm = this;
    vm.userId = null;
    vm.userHealthDiary = [];
    vm.filter =null;
    vm.diary_loader = false;

    vm.all_log_filter = true; // by default all log filter should remain checked
    vm.getHealthDiaryData = function(value, requested_days, btn_id){

        vm.all_filter_display = false;
        vm.food_filter_display = false;
        vm.exercise_filter_display = false;
        vm.weight_filter_display = false;

        // set filter
        var filter = [];
        if(vm.all_log_filter == true){

            filter.push("all_filter");
            vm.all_filter_display = true;
        }
        else{
            angular.forEach(vm.diaryFilters, function (values) {
                if(values.selected == true){
                  filter.push(values.id);
                  if(values.id == "food_filter")
                  {
                    vm.food_filter_display = true;
                  }
                  if(values.id == "exercise_filter")
                  {
                    vm.exercise_filter_display = true;
                  }
                  if(values.id == "weight_filter")
                  {
                    vm.weight_filter_display = true;
                  }

                }
            });
        }
        
        if(filter.length == 0){
            filter.push("all_filter");
        }
        // end: set filter

        
        var diary_from, diary_to;
        if(requested_days!=null){
            diary_from = moment().subtract(requested_days, 'days' ).format('YYYY-MM-DD');
            diary_to = moment().format('YYYY-MM-DD');
            vm.diary_from = vm.getDate(diary_from);
            vm.diary_to = vm.getDate(diary_to);

            $(".days-filter").removeClass("active");
            $("#"+btn_id).addClass("active");
        }
        else{

            var diff = moment(vm.diary_to).diff(vm.diary_from,'days');
            if(diff < 0){
              vm.diary_to = vm.diary_from
            }

            diary_from = moment(vm.diary_from).format('YYYY-MM-DD');
            diary_to = moment(vm.diary_to).format('YYYY-MM-DD');

            $(".days-filter").removeClass("active");
        }
        
        var params ={ 
                'diary_from':diary_from,
                'diary_to':diary_to,
                'user_id':vm.userId,
                'filter':filter,
                'btn_value' : value
        };
        
        if(value=='download'){
          
          window.open("/diary/download_diary?diary_from="+diary_from+"&diary_to="+diary_to+"&user_id="+vm.userId+"&filter="+filter+"&btn_value="+value, "_blank");
          vm.diary_loader = false;
        }else{
          vm.display_diary_data = false;
          vm.diary_loader = true;
          ShowService.search('api/v3/patient/user_health_diary',params,1).then(function(results){
              vm.userHealthDiary =  results.response;
              vm.userHealthDiaryLength =  results.result_count;
              
              vm.display_diary_data = true;
              vm.diary_loader = false;
          });
        }
        
    };
          
    vm.diaryFilters = [
        {
          id: "food_filter",
          title: "Food",
          selected:true
        }, 
        {
          id: "exercise_filter",
          title: "Exercise",
          selected:true
        }, 
        {
          id: "weight_filter",
          title: "Weight",
          selected:true
        }
    ];

    // check all uncheck all functions
    vm.checkAllFilter = function () { 
        
        if (vm.all_log_filter) {
            vm.all_log_filter = true;
        } else {
            vm.all_log_filter = false;
        }

        angular.forEach(vm.diaryFilters, function (filter) {
            filter.selected = vm.all_log_filter;
        });
    }

    vm.singleCheckbox = function(){
      var keepGoing = true;
        angular.forEach(vm.diaryFilters, function (filter) {
          if(keepGoing) {
            if(filter.selected == false){
              vm.all_log_filter = false;
              keepGoing = false;
            }
            else{
              vm.all_log_filter = true;
            }
          }
        });
    }
    // End: check all uncheck all functions

    vm.getDate = function(date){
      return moment(date).format('MMMM DD, YYYY');
    }
}]);