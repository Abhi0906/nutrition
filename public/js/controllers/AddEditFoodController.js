angular.module('app.core').controller('AddEditFoodController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){

    var vm = this;
    vm.custom_food = {};
    vm.log_newfoodloader = false;

    vm.addNewServing = function() {
        var newItemNo = vm.serving_size.length+1;
        vm.serving_size.push({id: newItemNo});
    };

    vm.removeServing = function(index) {
        vm.serving_size.splice(index,1);
    };

    if($stateParams.id && $stateParams.id != null) {
        vm.btnLabel = 'Update';
        vm.FormLabel = 'EDIT FOOD';
        vm.serving_size = [];
    } else {
        vm.btnLabel = 'Submit';
        vm.FormLabel = 'CREATE FOOD';
        vm.serving_size = [{'id': 1 }];
    }

    if($stateParams.id && $stateParams.id != null) {
        vm.food_id = $stateParams.id;
        ShowService.get('api/v2/admin/manage_food/edit_data', vm.food_id).then(function(result){
            if(result.error==false) {
                vm.custom_food = result.response;
                if(result.response.source_name =='mfp'){
                    result.response.source_name='food';
                }
                vm.custom_food.source_name = result.response.source_name || "food";
                vm.custom_food.food_name = result.response.name || null;
                vm.custom_food.brand_name = result.response.brand_name || null;
                vm.custom_food.calories = parseFloat(result.response.nutrition_data.calories) || 0;
                vm.custom_food.sodium = parseFloat(result.response.nutrition_data.sodium) || 0;
                vm.custom_food.total_fat = parseFloat(result.response.nutrition_data.total_fat) || 0;
                vm.custom_food.potassium = parseFloat(result.response.nutrition_data.potassium) || 0;
                vm.custom_food.saturated_fat = parseFloat(result.response.nutrition_data.saturated_fat) || 0;
                vm.custom_food.total_carbs = parseFloat(result.response.nutrition_data.total_carb) || 0;
                vm.custom_food.polyunsaturated = parseFloat(result.response.nutrition_data.polyunsaturated) || 0;
                vm.custom_food.dietary_fiber = parseFloat(result.response.nutrition_data.dietary_fiber) || 0;
                vm.custom_food.monounsaturated = parseFloat(result.response.nutrition_data.monounsaturated) || 0;
                vm.custom_food.sugar = parseFloat(result.response.nutrition_data.sugars) || 0;
                vm.custom_food.protein = parseFloat(result.response.nutrition_data.protein) || 0;
                vm.custom_food.trans_fat = parseFloat(result.response.nutrition_data.trans_fat) || 0;
                vm.custom_food.cholesterol = parseFloat(result.response.nutrition_data.cholesterol) || 0;
                vm.custom_food.vitamin_a = parseFloat(result.response.nutrition_data.vitamin_a) || 0;
                vm.custom_food.calcium = parseFloat(result.response.nutrition_data.calcium_dv) || 0;
                vm.custom_food.vitamin_c = parseFloat(result.response.nutrition_data.vitamin_c) || 0;
                vm.custom_food.iron = parseFloat(result.response.nutrition_data.iron_dv) || 0;
                angular.forEach(result.response.serving_data, function(value) {
                    var temp ={};
                    temp.serving_quantity = value.label;
                    temp.serving_size_unit = value.unit;
                    vm.serving_size.push(temp);
                });
                vm.custom_food.serving_quantity = result.response.nutrition_data.serving_quantity || '';
                vm.custom_food.serving_size_unit = result.response.nutrition_data.serving_size_unit || '';
            }
        });
    } else {
        vm.custom_food.sodium = 0;
        vm.custom_food.total_fat = 0;
        vm.custom_food.potassium = 0;
        vm.custom_food.saturated_fat = 0;
        vm.custom_food.total_carbs = 0;
        vm.custom_food.polyunsaturated = 0;
        vm.custom_food.dietary_fiber = 0;
        vm.custom_food.monounsaturated = 0;
        vm.custom_food.sugar = 0;
        vm.custom_food.protein = 0;
        vm.custom_food.trans_fat = 0;
        vm.custom_food.cholesterol = 0;
        vm.custom_food.vitamin_a = 0;
        vm.custom_food.calcium = 0;
        vm.custom_food.vitamin_c = 0;
        vm.custom_food.vitamin_c = 0;
        vm.custom_food.iron = 0;
        vm.custom_food.serving_quantity = 0;
        vm.custom_food.serving_size_unit = '';
        vm.custom_food.source_name="food";
    }

    vm.gotoManageFoodPage = function(){
        $state.go('manage_food');
    }

    vm.add_new_food = function(){

        var serving_size_log = [];
        angular.forEach(vm.serving_size, function(value, key) {
            this.push({
                "label": value.serving_quantity + ' ' + value.serving_size_unit,
                "unit": value.serving_size_unit
            });
        }, serving_size_log);

        vm.log_newfoodloader = true;
        var custom_food_data = {
            "user_agent" : 'server',
            "brand_name" : vm.custom_food.brand_name,
            "name" : vm.custom_food.food_name,
            "brand_with_name" : vm.custom_food.brand_name + " - " + vm.custom_food.food_name,
            "source_name" : vm.custom_food.source_name,
            "nutrition_data" : {
                "calories": vm.custom_food.calories,
                "sodium": vm.custom_food.sodium + " mg",
                "total_fat": vm.custom_food.total_fat + " g",
                "potassium": vm.custom_food.potassium + " mg",
                "saturated_fat": vm.custom_food.saturated_fat + " g",
                "total_carb": vm.custom_food.total_carbs + " g",
                "polyunsaturated": vm.custom_food.polyunsaturated + " g",
                "dietary_fiber": vm.custom_food.dietary_fiber + " g",
                "monounsaturated": vm.custom_food.monounsaturated + " g",
                "sugars": vm.custom_food.sugar + " g",
                "protein": vm.custom_food.protein + " g",
                "trans_fat": vm.custom_food.trans_fat + " g",
                "cholesterol": vm.custom_food.cholesterol + " mg",
                "vitamin_a": vm.custom_food.vitamin_a + "%",
                "calcium_dv": vm.custom_food.calcium + "%",
                "vitamin_c": vm.custom_food.vitamin_c + "%",
                "iron_dv": vm.custom_food.iron + "%",
                "serving_quantity": vm.serving_size[0].serving_quantity,
                "serving_size_unit": vm.serving_size[0].serving_size_unit,
            },
            "serving_data" : serving_size_log
        };

        if($stateParams.id && $stateParams.id != null) {
            var manage_food_id = $stateParams.id;
            StoreService.update('api/v2/admin/manage_food/update', manage_food_id, custom_food_data).then(function(result) {
                if(result.error == false) {
                    vm.custom_food = {};
                    showFlashMessage('Updated', 'Custom Food Updated successfully', 'success');
                    $state.go('manage_food');
                }
                vm.log_newfoodloader = false;
            },function(error) {
                console.log(error);
            });
        } else {
            StoreService.save('api/v2/admin/add_custom_food', custom_food_data).then(function(result) {
                if(result.error == false) {
                    vm.custom_food = {};
                    showFlashMessage('Added', 'Custom Food Added successfully', 'success');
                    $state.go('manage_food');
                }
                vm.log_newfoodloader = false;
            },function(error) {
                console.log(error);
            });
        }
        return false;
    }
}]);