angular.module('app.routes').config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/list');

    $stateProvider

        .state('deal-list', {
            url: '/list',
            templateUrl: '/admin/deal_list',
            controller : 'DealController',
            controllerAs : 'deal'
        })
        .state('create-deal', {
            url: '/create',
            templateUrl: '/admin/create_deal',
            controller : 'CreateDealController',
            controllerAs : 'create_deal'
        })
        .state('edit-deal', {
            url: '/edit/:deal_id',
            templateUrl: '/admin/edit_deal',
            controller : 'EditDealController',
            controllerAs : 'edit_deal'
        });
});

angular.module('app.core').controller('DealIndexController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){
    var vm = this;

}]);