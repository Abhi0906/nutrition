angular.module('app.core').controller('EventSettingController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){
    var vm = this;
    $('#to-top').trigger('click');

    // vm.settingTitle = 'Google Calendar';
    // vm.class_setting ="google_calendar";

    vm.changeSetting = function(){
       if(vm.class_setting == "google_calendar"){
         vm.settingTitle = 'Google Calendar';
        }else if(vm.class_setting == "events_pdf"){
         vm.settingTitle = 'Events PDF';
        }else if(vm.class_setting == "events_url"){
           vm.settingTitle = 'Events URL';
        }

    }

    vm.saveEventSetting = function(){


    	var setting_data ={};
		setting_data.type =vm.class_setting;
		setting_data.api_key =vm.api_key;
		setting_data.calendarId =vm.calendar_id;
		setting_data.url =vm.url;
		setting_data.pdf_name =vm.pdf_name;

		console.log(setting_data);
        StoreService.save('api/v2/admin/events/update_settings',setting_data).then(function(result) {
          if(result.error == false){
            $state.go('events');

          }
        },function(error) {
          console.log(error);
        });

    }

    vm.getEventSettings = function(){

		ShowService.query('api/v2/admin/events/settings').then(function(results){
	       var calendar_settings = results.response.google_calendar;
	       var event_url_settings = results.response.events_url;
	       var active_event = results.response.active_event;
	        vm.api_key= calendar_settings.api_key;
	        vm.calendar_id= calendar_settings.calendarId;
	        vm.url= event_url_settings.url;
	        vm.class_setting =active_event.event_type;
	        vm.pdf_name= results.response.events_pdf.file_name;
	        vm.event_pdf_url= results.response.events_pdf.pdf_url;
	        if(active_event.event_type == "google_calendar"){
	         vm.settingTitle = 'Google Calendar';
	        }else if(active_event.event_type == "events_pdf"){
	         vm.settingTitle = 'Events PDF';
	        }else if(active_event.event_type == "events_url"){
	           vm.settingTitle = 'Events URL';
	        }

	    });
    }



}]);