angular.module('app.core').controller('ExerciseImageListController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){
    var vm = this;
    $('.back-top').trigger('click');

	 	vm.exercise_images =[];
	    vm.current_page = 1; // initialize page no to 1
	    vm.total_count = 0;
	    vm.displayRecords = [20,50,100,150,200];
	    vm.itemPerPage = 50; //this could be a dynamic value from a drop down
	    vm.isloading = true;
      vm.S_parameter=null;
        vm.fetchExercise = function(pageno){

         	ShowService.query('api/v2/admin/exercise_image/list?page='+pageno+'&itemPerPage='+vm.itemPerPage).then(function(results){
             vm.exercise_images = results.response.data;
              vm.total_count = results.response.total;
    	        vm.current_page = results.response.current_page;
             // vm.all_exercises_count = results.response.all_exercises_count;
              vm.from = results.response.from;
              vm.to = results.response.to;
              vm.isloading = false;
    	    });

       }

       vm.pageChanged = function(){
       	var pageno =  vm.current_page;
       	//vm.fetchExercise(pageno);
        var pa= vm.S_parameter
        var r_url = 'api/v2/admin/exercise_image/list?page='+pageno;
          console.log(pa);
         angular.forEach(pa, function(value, key){
            r_url = r_url + '&' + key + '=' + value;
        });
        ShowService.query(r_url).then(function(results){
               vm.exercise_images = results.response.data;
                vm.total_count = results.response.total;
                vm.from = results.response.from;
                vm.to = results.response.to;
                vm.isloading = false;
            });

       }

        var watchArray = ['keyword',
                      'minimun_exercise_time',
                      'maximum_exercise_time',
                      'minimun_calorie_burn',
                      'maximum_calorie_burn',
                      'body_part',
                      'experience',
                      'equipment',
                      'exercise_type'
                      ];

          $scope.$watchGroup(watchArray, function(newVal, oldVal) {
           //console.log("scope",$scope.keyword);

           if(typeof $scope.keyword != "undefined" ||
              typeof $scope.minimun_exercise_time != "undefined" ||
              typeof $scope.maximum_exercise_time != "undefined" ||
              typeof $scope.minimun_calorie_burn != "undefined" ||
              typeof $scope.maximum_calorie_burn != "undefined" ||
              typeof $scope.experience != "undefined" ||
              typeof $scope.body_part != "undefined" ||
              typeof $scope.equipment != "undefined" ||
              typeof $scope.exercise_type != "undefined"


            ){
           	 vm.isloading = true;
            var params ={'keyword':$scope.keyword,
                            'minimun_exercise_time':$scope.minimun_exercise_time,
                            'maximum_exercise_time':$scope.maximum_exercise_time,
                            'minimun_calorie_burn':$scope.minimun_calorie_burn,
                            'maximum_calorie_burn':$scope.maximum_calorie_burn,
                            'experience':$scope.experience,
                            'body_part':$scope.body_part,
                            'equipment':$scope.equipment,
                            'exercise_type':$scope.exercise_type,
                            'is_filter':false
                          };
             vm.S_parameter = params;
             ShowService.search('api/v2/admin/exercise_image/list',params,1).then(function(results){
  		         vm.exercise_images = results.response.data;
                vm.total_count = results.response.total;
                vm.from = results.response.from;
                vm.to = results.response.to;
  		          vm.isloading = false;
		        });

           }
        });


     vm.clearFilter = function(){

	      $scope.keyword = "";
	      $scope.minimun_exercise_time = "";
	      $scope.maximum_exercise_time = "";
	      $scope.minimun_calorie_burn = "";
	      $scope.maximum_calorie_burn = "";
	      $scope.experience = "undefined";
	      $scope.body_part = "undefined";
	      $scope.equipment = "undefined";
        $scope.exercise_type = "undefined";
	      var params = {'is_filter':true};

     }


    vm.deleteExercise = function(exercise){

        var id = exercise.id;
        var msg = "Are you sure want to delete this exercise image ?";
        showFlashMessageConfirmation("Delete",msg,"info",function(result) {
                if (result) {
                    ShowService.remove('api/v2/admin/exercise_image',id).then(function(results){
                       if(results.error == false){
                          var index = vm.exercise_images.indexOf(exercise);
                          vm.exercise_images.splice(index, 1);
                          showFlashMessage('Delete','Exercise Image delete successfully','success');
                       }

                    });

                }
        });

    }

    // $scope.$on('onFilterExercise', function(event, obj) {
    //   console.log("obj",obj);
    //  	if(!obj.is_filter){

	   //   	ShowService.search('api/v2/admin/exercise_image/list',obj,1).then(function(results){
		  //        vm.exercise_images = results.response.data;
		  //   });

	   //   }else{

	   //   	ShowService.query('api/v2/admin/exercise_image/list').then(function(results){
	   //      vm.exercise_images = results.response.data;
	   //     });
	   //   }

    // });

    vm.viewExercise = function(exercise){
      //console.log(exercise);
    	var parseSteps =  angular.fromJson(exercise.steps);
    	//console.log(parseSteps);
    	vm.selectedExercise = exercise;
    	vm.selectedExercise.steps = parseSteps;

    	$("#view_exercise_image").modal('show');
    }

}]);
