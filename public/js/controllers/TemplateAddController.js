  angular.module('app.core').controller('TemplateAddController',
  ['ShowService','StoreService','$scope','$rootScope','$state',
  function(ShowService,StoreService,$scope,$rootScope,$state){
	  var vm = this;
    vm.saveTemplate = function(){
      var data = vm.selectedTemplate;
      vm.keyword = "";
      StoreService.save('api/v2/admin/template',data).then(function(results){
          if(results.error == false){
              vm.selectedTemplate = results.response;
              vm.selectedTemplate.workouts =[];
              vm.selectedTemplate.workout_count=0;
              vm.selectedTemplate.video_count=0;
              vm.selectedTemplate.exercise_count=0;
              //vm.templates.push(vm.selectedTemplate);
              showFlashMessage('Save','Template added successfully','success');
              $state.go('list-workout-plans');
          }
        },
        function(error) {
            console.log(error);
        });
    }

}]);