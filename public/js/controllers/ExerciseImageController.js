angular.module('app.routes').config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/list-exercise-images');

    $stateProvider

        .state('list-exercise-images', {
            url: '/list-exercise-images',
            templateUrl: '/admin/load_exrcise_images',
            controller : 'ExerciseImageListController',
            controllerAs : 'list_exercise'
        })
        .state('add-exercise-image', {
            url: '/add-exercise-image',
            templateUrl: '/admin/load_add_exercise_img',
            controller : 'ExerciseImageAddController',
            controllerAs : 'add_exercise'
        })
        .state('edit-exercise-image', {
            url: '/edit-exercise-image/:id',
            templateUrl: '/admin/load_edit_exercise_img',

            controller : 'ExerciseImageEditController',
            controllerAs : 'edit_exercise'
        });

});





angular.module('app.core').controller('ExerciseImageController',['ShowService','$scope',function(ShowService,$scope){
    var vm = this;
    vm.experiences = ['Beginner','Intermediate','Advanced'];
    vm.exercise_categories = ['Resistance','Cardio'];
    vm.training_types = [];
    vm.equipments =[];
    vm.clear_filter = false;
    ShowService.query('api/v2/video/body_parts').then(function(results){
            vm.body_parts = results.response;
        });

    ShowService.query('api/v2/video/equipments').then(function(results){
            vm.equipments = results.response;
       });



}]);
