angular.module('app.core').controller('AddRecomMealFoodController',['$stateParams', 'ShowService','$scope','StoreService','$timeout', '$state', '$anchorScroll', function($stateParams, ShowService,$scope,StoreService,$timeout, $state, $anchorScroll){
    var vm = this;

    vm.recommended_meal_id = $stateParams.recommended_meal_id;
    vm.meal_name = $stateParams.recommended_meal_name;

    vm.item_id = $stateParams.item_id;
    vm.item_name = $stateParams.item_name;
    vm.selectedFoods = [];

    vm.searchFood = function(){
            vm.search_loader = true;
            vm.foodSearchdata = {};
            //vm.food_tab_display = false;
            //vm.foodTabToggel(null, 'searched_food_tab');
            vm.search_result = false;
            vm.search_loader = true;
            var searchString = '';
            searchString = vm.searchKeyFood;
            ShowService.search('search.php?keyword='+searchString+'&limit=100').then(function(result) {


                if(result.error == false){

                   vm.foodSearchdata = result.response;
                    if(result.response.length>0){

                      vm.search_loader = false;
                      vm.search_result = true;


                    }

                  vm.search_loader = false;
                  //vm.searchMenu = true;
                }

                vm.search_loader = false;
            },function(error) {
                console.log(error);
            });
    }

    //clear logFood search
    vm.clear_logFood_searchResult = function(){
        vm.searchKeyFood = '';

        vm.foodSearchdata = {};
    }

    vm.getMDFoodTypes = function(){

        var params ={
            'recommended_meal_id':vm.recommended_meal_id,
            'item_id':vm.item_id
        };
        ShowService.search('api/v2/admin/recommended_meal/food_type',params,1).then(function(results){
            vm.md_food_type =  results.response.recommended_foods
        });

    }

    vm.checkAllFood = function() {
        console.log(vm.chk_all_type);
        if(vm.chk_all_type){
            vm.selectedFoods.splice(0, vm.selectedFoods.length);
            for (var i in vm.md_food_type) {
                vm.selectedFoods.push(vm.md_food_type[i]);
            }

        }else{
            vm.selectedFoods.splice(0, vm.selectedFoods.length);
        }

    };

    //end: clear logFood search

    vm.viewFoodDetail = function(food_obj){

        vm.food_detail = food_obj;
        $('#view_food_detail_tmplt').modal('show');
    }

    vm.createRecommendedMeal = function($index, food_obj){

        var nutrition_id = food_obj.id;
        var calories = vm.recommended_food[$index].calories;

        var serving_string = vm.recommended_food[$index].serving_string.label;

        var serving_size_unit = vm.recommended_food[$index].serving_string.unit;

        var serving_value = serving_string.replace(serving_size_unit,'');

        var serving_quantity =recursiveSearch(serving_value);

        var no_of_servings = vm.recommended_food[$index].no_of_servings;

        var item_id = vm.recommended_meal_item[$index].recommended_meal_item_id;

        var data = {
            "meal_id": vm.recommended_meal_id,
            "item_id": item_id,
            "calories": calories,
            "serving_quantity": serving_quantity,
            "serving_size_unit": serving_size_unit,
            "no_of_servings": no_of_servings,
            "serving_string": serving_string,
            "nutrition_id": nutrition_id,
            "user_agent": "server"
        };

        StoreService.save('api/v2/admin/recommended_meal/create_meal_food', data).then(function(result){

            if(result.error==false){

            }
            else{

            }

            vm.backToItemPage();

        });
    }

    vm.getRecommendedMealItems = function(){

        ShowService.query('api/v2/admin/recommended_meal/items').then(function(result){

            vm.recommended_meal_items = result.response;
        });
    }

    vm.backToItemPage = function(){
        $state.go('meal-items', {recommended_meal_id: vm.recommended_meal_id, recommended_meal_name: vm.meal_name});
    }

    vm.addFoodToItem = function(){
       //console.log(vm.selectedFoods);

        var data = {
            "meal_id": vm.recommended_meal_id,
            "user_agent": "server",
            "selectedFoods": vm.selectedFoods
        };

        StoreService.save('api/v2/admin/recommended_meal/save_meal_foods', data).then(function(result){

            if(result.error==false){
                vm.backToItemPage();
            }
            else{

            }



        });
    }

    vm.calculateServingSize = function($index, nutrition_obj){

        var serving_size_obj = vm.recommended_food[$index].serving_string;
        var no_of_serving =  vm.recommended_food[$index].no_of_servings;
        var serving_label = serving_size_obj.label;
        var serving_unit = serving_size_obj.unit;
        var serving_quantity = nutrition_obj.nutrition_data.serving_quantity;

        var calories = nutrition_obj.nutrition_data.calories;
        var no_of_serving_master = 1;

        var serving_value = serving_label.replace(serving_unit,'');
        var serving_size = recursiveSearch(serving_value);

        if(nutrition_obj.hasOwnProperty("nutrition_data")){
            var nutrition_data = nutrition_obj.nutrition_data;
        }else{
            var nutrition_data = nutrition_obj.nutritions.nutrition_data;
        }

        var update_calories = calculateCalories(no_of_serving,calories,serving_size,serving_quantity,no_of_serving_master);

        vm.recommended_food[$index].calories =update_calories.toFixed(2);

    }

    function recursiveSearch(serving_value){

       //  var serving_quantity=0;
         if (serving_value.toLowerCase().indexOf("container") >= 0) {

            var arr = serving_value.split("(");
            if (arr.length > 1) {
                var innerString = arr[1].trim();
                if (innerString.indexOf("/") >= 0) {
                    return recursiveSearch(innerString);
                }else{
                    var space_arr = innerString.split(" ");
                   return recursiveSearch(space_arr[0]);
                }
            }
        }else if (serving_value.indexOf("/") >= 0) {

            var arr = serving_value.split("/")
            if (arr.length > 1) {
                var innerString = arr[0].trim();
                var arrInnerCompo = innerString.split(" ");
                if (arrInnerCompo.length == 2) {
                    var firstString = arrInnerCompo[0].trim();
                    var secondString = arrInnerCompo[1].trim();
                    var arr1FirstComponent= arr[1].trim();
                    var thirdString = Number(arr1FirstComponent.split(" ")[0]);
                    // console.log("firstString",firstString);
                    // console.log("secondString",secondString);
                    // console.log("thirdString",thirdString);
                    var dividevalue = Number(firstString) + Number(secondString) / Number(thirdString) ;
                   return recursiveSearch(dividevalue.toString());

                }else{

                     var arrInner = innerString.split(" ");
                     var firstString = Number(arrInner[arrInner.length-1]);
                     var secondStringArr= arr[1].trim();
                     var secondString = Number(secondStringArr.split(" ")[0]);
                     var thirdString = firstString/secondString;
                     return recursiveSearch(thirdString.toString());

                }

            }

        }else{

            var final_value = serving_value.trim();
            var splitString = final_value.split(" ")[0]
            return Number(splitString);
        }

    }


    function calculateCalories(no_of_servings,calorie,serving_size,serving_quantity, no_of_serving_master){

        var num_of_serving = Number(no_of_servings/no_of_serving_master);
        var default_calories = calorie;
        var serving_size_value = Number(serving_size/serving_quantity);
        var update_calories = Number(default_calories)*Number(num_of_serving)*Number(serving_size_value);
        return update_calories;

    }


}]);