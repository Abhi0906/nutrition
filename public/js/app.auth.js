angular.module('app.auth',['angular-storage','angular-jwt','auth0','ngCookies'])
.config(function(tagsInputConfigProvider) {
    tagsInputConfigProvider.setDefaults('tagsInput', {
        placeholder: ''
    });
});
