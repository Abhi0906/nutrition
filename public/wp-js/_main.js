$(document).ready(function(e) {

         
$('.back-top').on('click', function(event){
    $("html, body").animate({scrollTop: 0}, "slow");
    return false;
  });

        $(window).bind("scroll", function () {
      if ($(this).scrollTop() > 320) {
          $(".back-top").fadeIn();
      } else {
          $(".back-top").stop().fadeOut();
      }
  });
  
       

	$('#respond input').on('click',function(){
		$('#respond input + .error').css('display','none');
	});

	$(' #respond textarea').on('click',function(){
		$('#respond textarea + .error').css('display','none');
	});

	$("#filter select").select2();
	jsCode();

	$('#frm_field_25_container').append('<a class="leavecomment">Leave a Comment<i class="fa fa-angle-up"></i></a>');



	$('.nav-down').on('click',function(){
		$('html,body').animate({
        scrollTop: $('#about').offset().top - 129},
        1400),
        $(this).hide();
	});

	


	$('#floating-form .exit').on('click', function() {
		/* $('#floating-form').hide('fade'); */
		$('#floating-form').addClass('close-me');
		$('.back-top').css('bottom','1%');
	});


	$('#formBtn').on('click',function(){
		$('#banner-form').stop().animate({
		  		left: "-15px"
		  	}, 400 );
		$(this).hide();
		$('#formActive').show();
		$('#banner-form .fa-close').show();
	});
	$('#formActive, #exitBtn').on('click',function(){
	
		if (Modernizr.mq("screen and (min-width:2600px)")) {
			$('#banner-form').stop().animate({
		  		left: "-570px"
		  	}, 400 );
		  	
		
  
		}else if (Modernizr.mq("screen and (min-width:2400px)")) {
		
		
			$('#banner-form').stop().animate({
		  		left: "-500px"
		  	}, 400 );
		  	
		  	
		}else if (Modernizr.mq("screen and (min-width:1200px)")) {
		
		
			$('#banner-form').stop().animate({
		  		left: "-320px"
		  	}, 400 );
		  	
		}else if (Modernizr.mq("screen and (min-width:992px)")) {
		
		
			$('#banner-form').stop().animate({
		  		left: "-530px"
		  	}, 400 );
		  	console.log('992');
		}else{
			
			$('#banner-form').stop().animate({
		  		left: "-610px"
		  	}, 400 );
		  	
		}
	
		$(this).hide();
		$('#banner-form .fa-close').hide();
		$('#formBtn').show();
	});

       

	// $('#formBtn').toggle(function () {
	// 	$("#banner-form").css({right: "0"});
	// }, function () {
	// 	$("#banner-form").css({right: "-365px"});
	// });



	$( "#header .main-menu" ).clone().appendTo( "#sidepanel .menu" );
	/* SUBMENU TOGGLE */
	$('.menu li.menu-item-has-children').prepend('<span class="submenu-toggle fa fa-plus-square-o"></span>');
	// $('.submenu-toggle').on('click', function(){
	// $('.custom-submenu').toggle();
	// });
	$('.submenu-toggle').on('click', function(){
		if($(this).hasClass('fa-plus-square-o')){
			$(this).removeClass('fa-plus-square-o').addClass('fa-minus-square-o');
			$(this).siblings('.sub-menu').css('display','block');
		}else{
			$(this).removeClass(' fa-minus-square-o').addClass('fa-plus-square-o');
			$(this).siblings('.sub-menu').css('display','none');
		}
	});
	$('.mobile-menu').click(function(){
		var target = '#'+ $(this).data('target');
		if($(target).hasClass('active')){
			$(target).stop().animate({
		  		left: "-280px"
		  	}, 400 );
			$('#mainpanel').stop().animate({
		  		paddingLeft: "0",
				marginRight: "0"
		  	}, 400 );
			$('#header.scroll, #header').stop().animate({
		  		left: "0px"
		  	}, 400 );
		 	$(target).removeClass('active');
		}else{
			$(target).stop().animate({
		  		left: "0px"
		  	}, 400 );
			$('#mainpanel').stop().animate({
		  		paddingLeft: "280px",
				marginRight: "-280px"
		  	}, 400 );
			
			$('#header.scroll, #header').stop().animate({
		  		left: "280px"
		  	}, 400 );
		 	$(target).addClass('active');
		} 
	});
	$('#sidepanel .exit').click(function(){
		$('#sidepanel').stop().animate({
			left: "-280px"
		}, 400 );
		$('#mainpanel').stop().animate({
			paddingLeft: "0",
			marginRight: "0"
		}, 400 );
		$('#header.scroll, #header').stop().animate({
		  		left: "0px"
		  	}, 400 );
		$('#sidepanel').removeClass('active');
	});
	
	$('#sidepanel .menu').on('click','a',function(){
		$('#sidepanel').stop().animate({
			left: "-280px"
		}, 400 );
		$('#mainpanel').stop().animate({
			paddingLeft: "0",
			marginRight: "0"
		}, 400 );
		$('#header.scroll, #header').stop().animate({
		  		left: "0px"
		  	}, 400 );
		$('#sidepanel').removeClass('active');		
	});
	$('#mainpanel .left').addClass('wow fadeInLeft');
	$('#mainpanel .right').addClass('wow fadeInRight');
	// $('#vmap').bind('labelShow.jqvmap',
 //    	function(event, label, code)
	// );
    $('#vmap').vectorMap({
      map: 'usa_en',
      enableZoom: false,
      showTooltip: false,
      selectedRegions: ['OR', 'CA', 'AZ', 'TX', 'GA', 'FL', 'IL','MI'],
    });
	$(window).scroll(function(){

		if ($(this).scrollTop()) {
		  $('#header').addClass('scroll');
		  /* $('#floating-form').hide(); */
		   $('.nav-down').hide();

		} else {
		  $('#header').removeClass('scroll');
		  $('.nav-down').show();
		}

	});
	if (Modernizr.mq("screen and (min-width:1200px)")) {
		// SCROLL BOTTOM
	$(window).scroll(function() {
  if ($(window).scrollTop() == $(document).height() - $(window).height()) {
	   $('#floating-form').hide('fade');
  }
  
  if ($(window).scrollTop() != $(document).height() - $(window).height()) {
	   $('#floating-form').show();
  }
  
});
}

     $(window).on('resize', function(){
     	jsCode(); 
     	
     	if ( $("body").hasClass( "home" ) ) {
 if (Modernizr.mq("screen and (min-width:1200px)")) {
		  	
		  	window.setTimeout(function(){ $('#formBtn').trigger('click')} , 8000);

		}
 
    } 
     	 
     }); 

    $(window).load(function(){
         $(".frm_form_field.user").append('<i class="fa fa-user"></i>');
         $(".frm_form_field.mail").append('<i class="fa fa-envelope"></i>');
         $(".frm_form_field.message").append('<i class="fa fa-weixin"></i>');
         $(".frm_form_field.phone").append('<i class="fa fa-phone"></i>');
         $(".frm_form_field.subject").append('<i class="fa fa-tag"></i>');
      
         $('#floating-panel #submit').trigger('click');
         jsCode(); 
 
	$('.leavecomment').on('click', function(){
		$('#field_cx2776').toggle();
	});

        $(window).resize();
    });
   sidebar();
});
function jsCode() {

	wow = new WOW(
		{
			boxClass:     'wow',      // default
			animateClass: 'animated', // default
			offset:       0,          // default
			mobile:       true,       // default
			live:         true        // default
		}
	)
	wow.init();
	

	$('.flexslider').each(function(){
		$(this).flexslider({
		    animation: $(this).data('flexeffect'),
		    video: $(this).data('flex-video'),
		    slideshow: $(this).data('flexslideshow'),
		    controlNav: true,
		    directionNav: $(this).data('flexdirection')
		});
	});

	// var windowHeight = $(window).height();
	
	// //HOME TOP

	// var topContainer = $('#top .container').outerHeight()+300;
	
	
	// console.log(topContainer);
	// console.log(windowHeight);
	
	// if(topContainer >= windowHeight){
	// 	var headerHeight = $('#header').outerHeight()+30;
	// 	$('#top').css('padding-top',headerHeight+'px').css('padding-bottom','100px');
	// }else{	
	// //	var headerHeight = $('#header').outerHeight();
	// 	var paddings = ( windowHeight - $('#top .container').outerHeight() );
		
	// 	$('#top').css('padding-top',(paddings*0.4)+'px').css('padding-bottom',(paddings*0.6)+'px');
	// }


	// $('#top').css('height',windowHeight);

	var headerHeight = $('#header').outerHeight();
	$('.error404 #mainpanel').css('padding-top', headerHeight);
	//console.log(headerHeight);

	// EQUALIZE 
	if(Modernizr.mq('(min-width: 768px)')){
		var maxHeight = 0;
		$('#stories .content').each(function(){
			maxHeight = $(this).outerHeight() > maxHeight ? $(this).outerHeight() : maxHeight;
		}).css('height', maxHeight + 'px');
	}else{
		$('#stories .content').css('height', 'auto');	
	}
	if(Modernizr.mq('(min-width: 768px)')){
		var maxHeight = 0;
		$('#recent .post h3').each(function(){
			maxHeight = $(this).outerHeight() > maxHeight ? $(this).outerHeight() : maxHeight;
		}).css('height', maxHeight + 'px');
	}else{
		$('#recent .post h3').css('height', 'auto');	
	}


    $(".btnControl").on('click',function() {
		
		if($(this).find('i').hasClass('fa-pause')){
			$('.slider-results-men').trigger('pause',true);
			$(this).find('i').removeClass('fa-pause');
			$(this).find('i').addClass('fa-play');

		}else{
			$('.slider-results-men').trigger('resume',true);
			$(this).find('i').addClass('fa-pause');
			$(this).find('i').removeClass('fa-play');

		}

    });

	$(".slider-results-men").carouFredSel({
		auto: true,
		responsive: true,
		circular: true,
		pagination: '.LsliderNav',
		infinite    : true,
		pause: "#btnPauseLeft",
		items: {
			visible: {
				max: 1
			},
			height: 'auto',
			width: 300,
		},
		scroll: {
			items: 1,
			fx: 'crossfade'
		},
		auto: {
			timeoutDuration:999999999,
			pauseOnHover: true			
		},
		next: '.lNavNext',
		prev: '.lNavPrev'
	});	
	$(".slider-results-women").carouFredSel({
		auto: true,
		responsive: true,
		circular: true,
		pagination: '.RsliderNav',
		infinite    : true,
		items: {
			visible: {
				max: 1
			},
			height: 'auto',
			width: 1165,
		},
		scroll: {
			items: 1,
			fx: 'crossfade'
		},
		auto: {
			timeoutDuration:999999999,
			pauseOnHover: true			
		},
		next: '.rNavNext',
		prev: '.rNavPrev'
	});	
	
	
	$('.freeconsultbtn').on('click', function(){
	   $('#formBtn').trigger('click');
	});
  
  
  
    $('.featured-content .carousel-phones ul').carouFredSel({
        responsive: true,
        circular: true,
        auto: true,
        prev: '.prev',
        next: '.next',
        pagination: '.paginate',
        items: {
            visible: {
                min: 1,
                max: 1
            },
            height: 'auto'
        },
        scroll: {
            items: 1,
            fx: 'crossfade'
        }

    });

}


function sidebar() {
  
  var $sidebar   = $("#sidebar"), 
	$window    = $(window),
	offset     = $sidebar.offset(),
	topPadding = $('.header').outerHeight()+ 15;
		$(window).on('resize',function(){
			offset     = $sidebar.offset();
            if(Modernizr.mq('(min-width: 2400px)')){
              topPadding = $('.header').outerHeight() + 250;
            }else{
              topPadding = $('.header').outerHeight() + 150;
            }
			
		});
	$window.scroll(function() {
		if(Modernizr.mq('(min-width: 768px)')){
			var v1 = $window.scrollTop()+$('#sidebar').outerHeight();
			
				var v2 =  $('#innerpage').outerHeight()+$('section.main-content').outerHeight();
            
			if(v1 < v2){
				if ($window.scrollTop() > offset.top) {
					$sidebar.stop().animate({
						marginTop: $window.scrollTop() - offset.top + topPadding 
					});
				} else {
					$sidebar.stop().animate({
						marginTop: 0
					});
				}
			}
		}else{
			
			$sidebar.stop().animate({
				marginTop: 0
			});
		}
	});
  
}

