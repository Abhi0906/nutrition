$(document).ready(function(e) {
    setbannerheight();
    $(".back-top").on("click", function(event) {
        $("html, body").animate({
            scrollTop: 0
        }, "slow");
        return false
    });
    $(window).bind("scroll", function() {
        if ($(this).scrollTop() > 320) $(".back-top").fadeIn();
        else $(".back-top").stop().fadeOut()
    });
    $("#respond input").on("click", function() {
        $("#respond input + .error").css("display", "none")
    });
    $(" #respond textarea").on("click", function() {
        $("#respond textarea + .error").css("display", "none")
    });
    $("#filter select").select2();
    jsCode();
    $("#frm_field_25_container").append('<a class="leavecomment">Leave a Comment<i class="fa fa-angle-up"></i></a>');
    $(".nav-down").on("click", function() {
        $("html,body").animate({
            scrollTop: $("#about").offset().top - 129
        }, 1400), $(this).hide()
    });
    $("#floating-form .exit").on("click", function() {
        $("#floating-form").addClass("close-me");
        $(".back-top").css("bottom", "1%")
    });
    $("#formBtn").on("click", function() {
        $("#banner-form").stop().animate({
            left: "-15px"
        }, 400);
        $(this).hide();
        $("#formActive").show();
        $("#banner-form .fa-close").show()
    });
    $("#formActive, #exitBtn").on("click", function() {
        if (Modernizr.mq("screen and (min-width:2600px)")) $("#banner-form").stop().animate({
            left: "-350px"
        }, 400);
        else if (Modernizr.mq("screen and (min-width:2400px)")) $("#banner-form").stop().animate({
            left: "-350px"
        }, 400);
        else if (Modernizr.mq("screen and (min-width:1200px)")) $("#banner-form").stop().animate({
            left: "-281px"
        }, 400);
        else if (Modernizr.mq("screen and (min-width:992px)")) {
            $("#banner-form").stop().animate({
                left: "-530px"
            }, 400);
            console.log("992")
        } else $("#banner-form").stop().animate({
                left: "-610px"
            },
            400);
        $(this).hide();
        $("#banner-form .fa-close").hide();
        $("#formBtn").show()
    });
    
    if($('#header .main-menu > li ').hasClass('current-menu-item')){
    	$("#header .main-menu .current-menu-item > .sub-menu").clone().appendTo("#side-nav");
    }
    else {
    	if($('#header .main-menu > li > ul > li').hasClass('current_page_item') || $('#header .main-menu > li > ul > li').hasClass('menu-item-object-services')){
            if($('#header .main-menu .current-menu-parent').hasClass('current-menu-item')){
                $("#header .main-menu .current-menu-parent.current-menu-item > .sub-menu").clone().appendTo("#side-nav");
            }else{
                $("#header .main-menu .current-menu-parent > .sub-menu").clone().appendTo("#side-nav");
            }
    		
            //$("#header .main-menu .current-menu-ancestor .current-menu-ancestor .current-menu-parent > .sub-menu").clone().appendTo("#side-nav");
            //$("#header .main-menu > .current-menu-ancestor > ul > .current_page_parent > .sub-menu").clone().appendTo("#side-nav");
            //$("#header .main-menu .menu-item-object-services.current-menu-parent > .sub-menu").clone().appendTo("#side-nav");
    	}else{
			$("#header .main-menu .current-menu-ancestor .current-menu-parent > .sub-menu").clone().appendTo("#side-nav");
    	}
    }

    $("#header .main-menu").clone().appendTo("#sidepanel .menu");
    $(".menu li.menu-item-has-children").prepend('<span class="submenu-toggle fa fa-plus-square-o"></span>');
    $(".submenu-toggle").on("click", function() {
        if ($(this).hasClass("fa-plus-square-o")) {
            $(this).removeClass("fa-plus-square-o").addClass("fa-minus-square-o");
            $(this).siblings(".sub-menu").css("display", "block")
        } else {
            $(this).removeClass(" fa-minus-square-o").addClass("fa-plus-square-o");
            $(this).siblings(".sub-menu").css("display", "none")
        }
    });
    $(".mobile-menu").click(function() {
        var target = "#" + $(this).data("target");
        if ($(target).hasClass("active")) {
            $(target).stop().animate({
                left: "-280px"
            }, 400);
            $("#mainpanel").stop().animate({
                paddingLeft: "0",
                marginRight: "0"
            }, 400);
            $("#header.scroll, #header").stop().animate({
                left: "0px"
            }, 400);
            $(target).removeClass("active")
        } else {
            $(target).stop().animate({
                    left: "0px"
                },
                400);
            $("#mainpanel").stop().animate({
                paddingLeft: "280px",
                marginRight: "-280px"
            }, 400);
            $("#header.scroll, #header").stop().animate({
                left: "280px"
            }, 400);
            $(target).addClass("active")
        }
    });
    $("#sidepanel .exit").click(function() {
        $("#sidepanel").stop().animate({
            left: "-280px"
        }, 400);
        $("#mainpanel").stop().animate({
            paddingLeft: "0",
            marginRight: "0"
        }, 400);
        $("#header.scroll, #header").stop().animate({
            left: "0px"
        }, 400);
        $("#sidepanel").removeClass("active")
    });
    $("#sidepanel .menu").on("click", "a", function() {
        $("#sidepanel").stop().animate({
                left: "-280px"
            },
            400);
        $("#mainpanel").stop().animate({
            paddingLeft: "0",
            marginRight: "0"
        }, 400);
        $("#header.scroll, #header").stop().animate({
            left: "0px"
        }, 400);
        $("#sidepanel").removeClass("active")
    });
    $("#mainpanel .left").addClass("wow fadeInLeft");
    $("#mainpanel .right").addClass("wow fadeInRight");
    // $("#vmap").vectorMap({
    //     map: "usa_en",
    //     enableZoom: false,
    //     showTooltip: false,
    //     selectedRegions: ["OR", "CA", "AZ", "TX", "GA", "FL", "IL", "MI"]
    // });
    $(window).scroll(function() {
        if ($(this).scrollTop()) {
            $("#header").addClass("scroll");
            $(".nav-down").hide()
        } else {
            $("#header").removeClass("scroll");
            $(".nav-down").show()
        }
    });
    if (Modernizr.mq("screen and (min-width:1200px)")) $(window).scroll(function() {
        if ($(window).scrollTop() == $(document).height() - $(window).height()) $("#floating-form").hide("fade");
        if ($(window).scrollTop() != $(document).height() - $(window).height()) $("#floating-form").show()
    });
    $(window).on("resize", function() {
        jsCode();
        if ($("body").hasClass("home")){
            if (Modernizr.mq("screen and (min-width:1200px)")){
                /*window.setTimeout(function() {
                $("#formBtn").trigger("click")
                }, 11E3);*/
                var countme = 1;
                var loadScrollTop = ($(document).scrollTop() > 0 ? $(document).scrollTop() : null);
                    $(document).scroll(function (e) {
                        if ( $(document).scrollTop() !== loadScrollTop && countme == 1) {
                            $("#formBtn").trigger("click")
                            countme++;
                        }
                        loadScrollTop = null;
                    });
            } 
        }
            
        setbannerheight()
    });
    $(window).load(function() {
        if ($("body").hasClass("page") || $("body").hasClass("single-services")) $("body").on("click", ".scroll-to", function(event) {
            event.preventDefault();
            var target = $(this).data("target");
            if (Modernizr.mq('(min-width: 2400px)')) {
            	var headerHeight = $("#header").outerHeight() + 120;
            }else{
            	var headerHeight = $("#header").outerHeight() + 70;
            }
            
            var scrollHeight = $("#" + target).offset().top - headerHeight;
            $("html, body").animate({
                scrollTop: scrollHeight
            }, 1E3)
        });
        $(".frm_form_field.user").append('<i class="fa fa-user"></i>');
        $(".frm_form_field.mail").append('<i class="fa fa-envelope"></i>');
        $(".frm_form_field.message").append('<i class="fa fa-weixin"></i>');
        $(".frm_form_field.phone").append('<i class="fa fa-phone"></i>');
        $(".frm_form_field.subject").append('<i class="fa fa-tag"></i>');
        $("#floating-panel #submit").trigger("click");
        jsCode();
        $(".leavecomment").on("click", function() {
            $("#field_cx2776").toggle()
        });
        if ($("body").hasClass("page-template-template-success-stories"))
            if (window.location.hash) {
                var hash = window.location.hash.substring(1);
                $("#" + hash).click();
                var target = hash;
                var headerHeight = $("#header").outerHeight() + 66;
                if (Modernizr.mq("(min-width: 2400px)")) var headerHeight =
                    $("#header").outerHeight() + 95;
                var scrollHeight = $("#" + target).offset().top - headerHeight;
                $("html, body").animate({
                    scrollTop: scrollHeight
                }, 1E3)
            }
        /* SIDENAV ACCORDION */
        $("#side-nav > .sub-menu .sub-menu").slideUp();
        $('#side-nav > .sub-menu .menu-item-has-children').append('<i class="fa fa-chevron-right"></i>');
        $('#side-nav > .sub-menu .menu-item-has-children').append('<i class="fa fa-chevron-down"></i>');
        $('#side-nav > .sub-menu > .menu-item-has-children .fa-chevron-down').hide();
        $('#side-nav > .sub-menu > .menu-item-has-children > .fa-chevron-right').on('click', function(){
        	$("#side-nav > .sub-menu .sub-menu").slideUp();
        	$('#side-nav > .sub-menu > li').removeClass('active');
	        $('#side-nav > .sub-menu > li > .fa-chevron-down').hide();
	        $('#side-nav > .sub-menu > li > .fa-chevron-right').show();
        	$(this).parents('li').addClass('active');
        	$(this).siblings('.sub-menu').slideDown();
	        $('#side-nav > .sub-menu > li.active > .fa-chevron-down').show();
	        $('#side-nav > .sub-menu > li.active > .fa-chevron-right').hide();
        });
        $('#side-nav > .sub-menu > .menu-item-has-children > .fa-chevron-down').on('click', function(){
	        $('#side-nav > .sub-menu > li.active > .fa-chevron-right').show();
	        $('#side-nav > .sub-menu > li.active > .fa-chevron-down').hide();
        	$("#side-nav > .sub-menu .sub-menu").slideUp();
        });
        $('#side-nav > .sub-menu > .menu-item-has-children ul .fa-chevron-right').on('click', function(){
        	var chil = $(this).parents('li').addClass('active');
        	$(this).siblings('.sub-menu').slideDown();
        	$('#side-nav > .sub-menu > .active .menu-item-has-children .fa-chevron-right').hide();
        	$('#side-nav > .sub-menu > .active .menu-item-has-children .fa-chevron-down').show();
        });
        $('#side-nav > .sub-menu > .menu-item-has-children ul .fa-chevron-down').on('click', function(){
        	$(this).siblings('.sub-menu').slideUp();
        	$('#side-nav > .sub-menu > .active .menu-item-has-children .fa-chevron-right').show();
        	$('#side-nav > .sub-menu > .active .menu-item-has-children .fa-chevron-down').hide();
        	$('#side-nav > .sub-menu > li .sub-menu > li').removeClass('active');
        });
        $(window).resize()
    });
    sidebar()
});

function jsCode() {
    wow = new WOW({
        boxClass: "wow",
        animateClass: "animated",
        offset: 0,
        mobile: true,
        live: true
    });
    wow.init();
    $(".flexslider").each(function() {
        $(this).flexslider({
            animation: $(this).data("flexeffect"),
            video: $(this).data("flex-video"),
            slideshow: $(this).data("flexslideshow"),
            controlNav: true,
            directionNav: $(this).data("flexdirection")
        })
    });
    var headerHeight = $("#header").outerHeight();
    $(".error404 #mainpanel").css("padding-top", headerHeight);
    /*if (Modernizr.mq("(min-width: 768px)")) {
        var maxHeight = 0;
        $("#story .content").each(function() {
            maxHeight =
                $(this).outerHeight() > maxHeight ? $(this).outerHeight() : maxHeight
        }).css("height", maxHeight + "px")
    } else $("#story .content").css("height", "auto");*/
    if (Modernizr.mq("(min-width: 768px)")) {
        var maxHeight = 0;
        $("#recent .post h3").each(function() {
            maxHeight = $(this).outerHeight() > maxHeight ? $(this).outerHeight() : maxHeight
        }).css("height", maxHeight + "px")
    } else $("#recent .post h3").css("height", "auto");
    $(".btnControl").on("click", function() {
        if ($(this).find("i").hasClass("fa-pause")) {
            $(".slider-results-men").trigger("pause",
                true);
            $(this).find("i").removeClass("fa-pause");
            $(this).find("i").addClass("fa-play")
        } else {
            $(".slider-results-men").trigger("resume", true);
            $(this).find("i").addClass("fa-pause");
            $(this).find("i").removeClass("fa-play")
        }
    });
    /*
    $(".slider-results-men").carouFredSel({
        auto: true,
        responsive: true,
        circular: true,
        pagination: ".LsliderNav",
        infinite: true,
        pause: "#btnPauseLeft",
        items: {
            visible: {
                max: 1
            },
            height: "auto",
            width: 300
        },
        scroll: {
            items: 1,
            fx: "crossfade"
        },
        auto: {
            timeoutDuration: 999999999,
            pauseOnHover: true
        },
        next: ".lNavNext",
        prev: ".lNavPrev"
    });
    $(".slider-results-women").carouFredSel({
        auto: true,
        responsive: true,
        circular: true,
        pagination: ".RsliderNav",
        infinite: true,
        items: {
            visible: {
                max: 1
            },
            height: "auto",
            width: 1165
        },
        scroll: {
            items: 1,
            fx: "crossfade"
        },
        auto: {
            timeoutDuration: 999999999,
            pauseOnHover: true
        },
        next: ".rNavNext",
        prev: ".rNavPrev"
    });
    $(".freeconsultbtn").on("click", function() {
        $("#formBtn").trigger("click")
    });
    $(".featured-content .carousel-phones ul").carouFredSel({
        responsive: true,
        circular: true,
        auto: true,
        prev: ".prev",
        next: ".next",
        pagination: ".paginate",
        items: {
            visible: {
                min: 1,
                max: 1
            },
            height: "auto"
        },
        scroll: {
            items: 1,
            fx: "crossfade"
        }
    })*/
}

function sidebar() {
    var $sidebar = $("#sidebar"),
        $window = $(window),
        offset = $sidebar.offset(),
        topPadding = $(".header").outerHeight() + 15;
        if( typeof offset !="undefined"){

                $(window).on("resize", function() {
                    offset = $sidebar.offset();
                    if (Modernizr.mq("(min-width: 2400px)")) topPadding = $(".header").outerHeight() + 250;
                    else topPadding = $(".header").outerHeight() + 150
                });
                $window.scroll(function() {
                    if (Modernizr.mq("(min-width: 768px)")) {
                        var v1 = $window.scrollTop() + $("#sidebar").outerHeight();
                        var v2 = $("#innerpage").outerHeight() + $("section.main-content").outerHeight();
                        if (v1 < v2)
                            if ($window.scrollTop() > offset.top) $sidebar.stop().animate({
                                marginTop: $window.scrollTop() - offset.top + topPadding
                            });
                            else $sidebar.stop().animate({
                                marginTop: 0
                            })
                    } else $sidebar.stop().animate({
                        marginTop: 0
                    })
                });
                $window.scroll(function() {
                    if (Modernizr.mq("(min-width: 768px)")) {
                        var v1 = $window.scrollTop() + $("#sidebar").outerHeight();
                        var v2 = $("#innerpage").outerHeight() + $(".category-content").outerHeight();
                        if (v1 < v2)
                            if ($window.scrollTop() > offset.top) $sidebar.stop().animate({
                                marginTop: $window.scrollTop() -
                                    offset.top + topPadding
                            });
                            else $sidebar.stop().animate({
                                marginTop: 0
                            })
                    } else $sidebar.stop().animate({
                        marginTop: 0
                    })
                });
                $window.scroll(function() {
                    if (Modernizr.mq("(min-width: 768px)")) {
                        var v1 = $window.scrollTop() + $("#sidebar").outerHeight();
                        var v2 = $("#innerpage").outerHeight() + $(".page-main-content").outerHeight();
                        if (v1 < v2)
                            if ($window.scrollTop() > offset.top) $sidebar.stop().animate({
                                marginTop: $window.scrollTop() -
                                    offset.top + topPadding + 40
                            });
                            else $sidebar.stop().animate({
                                marginTop: 0
                            })
                    } else $sidebar.stop().animate({
                        marginTop: 0
                    })
                });
        }
       
}

function setbannerheight() {
    if (Modernizr.mq("(min-width: 1200px)")) {
        if ($("body").hasClass("home")) {
            var screenHeight = $(window).height();
            var contentHeight = $(".home #top > ul.slides > li .container-fluid").outerHeight();
            var bannerHeight = screenHeight - contentHeight;
            var adjustment = bannerHeight * .4;
            var adjustment2 = bannerHeight * .6;
            var originalHeight = $(".home #top > ul.slides > li").outerHeight();
            if (screenHeight > originalHeight) {
                $(".home #top > ul.slides > li").css("padding-top", adjustment + "px");
                $(".home #top > ul.slides > li").css("padding-bottom", adjustment2 + "px")
            }
        }
    }
};