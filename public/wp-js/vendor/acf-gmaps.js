(function ($) {
  function render_map($el) {
    var $markers = $el.find(".marker");
    var args = {
      zoom: 16,
      center: new google.maps.LatLng(0, 0),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map($el[0], args);
    map.markers = [];
    $markers.each(function () {
      add_marker($(this), map)
    });
    center_map(map)
  }

  function add_marker($marker, map) {
    var latlng = new google.maps.LatLng($marker.attr("data-lat"), $marker.attr("data-lng"));
    var marker = new google.maps.Marker({
      position: latlng,
      map: map
    });
    map.markers.push(marker);
    if ($marker.html()) {
      var infowindow =
        new google.maps.InfoWindow({
          content: $marker.html()
        });
      google.maps.event.addListener(marker, "click", function () {
        infowindow.open(map, marker)
      })
    }
  }

  function center_map(map) {
    var bounds = new google.maps.LatLngBounds;
    $.each(map.markers, function (i, marker) {
      var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
      bounds.extend(latlng)
    });
    if (map.markers.length == 1) {
      map.setCenter(bounds.getCenter());
      map.setZoom(20)
    } else map.fitBounds(bounds)
    
    
  var geocoder = new google.maps.Geocoder();

  document.getElementById('submit').addEventListener('click', function() {
    geocodeAddress(geocoder, map);
  });
  }
  
  function geocodeAddress(geocoder, resultsMap) {
    console.log(resultsMap);
    
    var address = document.getElementById('address').value;
    geocoder.geocode({'address': address}, function(results, status) {
      if (status === google.maps.GeocoderStatus.OK) {
        resultsMap.setCenter(results[0].geometry.location);
//        var marker = new google.maps.Marker({
//          map: resultsMap,
//          position: results[0].geometry.location,
//          zoom: 10
//        });
        console.log(results);
     if (results[0].geometry.viewport) 
          resultsMap.fitBounds(results[0].geometry.viewport);
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
  }
  
  $(document).ready(function () {
    $(".acf-map").each(function () {
      render_map($(this))
    })
  })
})(jQuery);