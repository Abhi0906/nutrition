<?php
use Illuminate\Database\Seeder;
class TrainingTypesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		
		\DB::table('training_types')->truncate();
        
		\DB::table('training_types')->insert(array (
			
			0 => 
			array (
				'id' => 1,
				'display_name' => 'Balance / Agility',
				'name' => 'balance_agility',
				'sort_order' => 1,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
			),
			1 => 
			array (
				'id' => 2,
				'display_name' => 'Barre',
				'name' => 'barre',
				'sort_order' => 2,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),
			2 => 
			array (
				'id' => 3,
				'display_name' => 'Cardiovascular',
				'name' => 'cardiovascular',
				'sort_order' => 3,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),
			3 => 
			array (
				'id' => 4,
				'display_name' => 'HIIT',
				'name' => 'hiit',
				'sort_order' => 4,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),
			4 => 
			array (
				'id' => 5,
				'display_name' => 'Kettlebell',
				'name' => 'kettlebell',
				'sort_order' => 5,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),
			5 => 
			array (
				'id' => 6,
				'display_name' => 'Low Impact',
				'name' => 'low_impact',
				'sort_order' => 6,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),
			6 => 
			array (
				'id' => 7,
				'display_name' => 'Pilates',
				'name' => 'pilates',
				'sort_order' => 7,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),
			7 => 
			array (
				'id' => 8,
				'display_name' => 'Plyometric',
				'name' => 'plyometric',
				'sort_order' => 8,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),
			8 => 
			array (
				'id' => 9,
				'display_name' => 'Strength Training',
				'name' => 'strength_training',
				'sort_order' => 9,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),
			9 => 
			array (
				'id' => 10,
				'display_name' => 'Toning',
				'name' => 'toning',
				'sort_order' => 10,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),
			10 => 
			array (
				'id' => 11,
				'display_name' => 'Warm Up / Cool Down',
				'name' => 'warm_up_cool_down',
				'sort_order' => 11,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),
			11 => 
			array (
				'id' => 12,
				'display_name' => 'Yoga / Stretching / Flexibility',
				'name' => 'yoga_stretching_flexibility',
				'sort_order' => 12,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			)
		));
		
	}

}
