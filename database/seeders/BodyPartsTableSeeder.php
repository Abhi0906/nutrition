<?php
use Illuminate\Database\Seeder;
class BodyPartsTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('body_parts')->truncate();
        
		\DB::table('body_parts')->insert(array (
			0 => 
			array (
				'id' => 1,
				'name' => 'Abductors',
				'created_at' => '2015-10-15 11:30:34',
				'updated_at' => '2015-10-15 11:30:51',
				'deleted_at' => NULL,
			),
			1 => 
			array (
				'id' => 2,
				'name' => 'Abs',
				'created_at' => '2015-10-15 11:30:34',
				'updated_at' => '2015-10-15 11:30:51',
				'deleted_at' => NULL,
			),
			2 => 
			array (
				'id' => 3,
			    'name' => 'Biceps (Arms)',
				'created_at' => '2015-10-15 11:30:34',
				'updated_at' => '2015-10-15 11:30:51',
				'deleted_at' => NULL,
			),
			3 => 
			array (
				'id' => 4,
				'name' => 'Calves',
				'created_at' => '2015-10-15 11:30:34',
				'updated_at' => '2015-10-15 11:30:51',
				'deleted_at' => NULL,
			),
			4 => 
			array (
				'id' => 5,
				'name' => 'Chest',
				'created_at' => '2015-10-15 11:30:34',
				'updated_at' => '2015-10-15 11:30:51',
				'deleted_at' => NULL,
			),
			5 => 
			array (
				'id' => 6,
				'name' => 'Forearms',
				'created_at' => '2015-10-15 11:30:34',
				'updated_at' => '2015-10-15 11:30:51',
				'deleted_at' => NULL,
			),
			6 => 
			array (
				'id' => 7,
				'name' => 'Glutes',
				'created_at' => '2015-10-15 11:30:34',
				'updated_at' => '2015-10-15 11:30:51',
				'deleted_at' => NULL,
			),
			7 => 
			array (
				'id' => 8,
				'name' => 'Lats',
				'created_at' => '2015-10-15 11:30:34',
				'updated_at' => '2015-10-15 11:30:51',
				'deleted_at' => NULL,
			),
			8 => 
			array (
				'id' => 9,
				'name' => 'Neck',
				'created_at' => '2015-10-15 11:30:34',
				'updated_at' => '2015-10-15 11:30:51',
				'deleted_at' => NULL,
			),
			9 => 
			array (
				'id' => 10,
				'name' => 'Quads',
				'created_at' => '2015-10-15 11:30:34',
				'updated_at' => '2015-10-15 11:30:51',
				'deleted_at' => NULL,
			),
			10 => 
			array (
				'id' => 11,
				'name' => 'Shoulders',
				'created_at' => '2015-10-15 11:30:34',
				'updated_at' => '2015-10-15 11:30:51',
				'deleted_at' => NULL,
			),
			11 => 
			array (
				'id' => 12,
				'name' => 'Traps',
				'created_at' => '2015-10-15 11:30:34',
				'updated_at' => '2015-10-15 11:30:51',
				'deleted_at' => NULL,
			),
			12 => 
			array (
				'id' => 13,
				'name' => 'Back',
				'created_at' => '2015-10-15 11:30:34',
				'updated_at' => '2015-10-15 11:30:51',
				'deleted_at' => NULL,
			),
			13 => 
			array (
				'id' => 14,
				'name' => 'Triceps (Arms)',
				'created_at' => '2015-10-15 11:30:34',
				'updated_at' => '2015-10-15 11:30:51',
				'deleted_at' => NULL,
			),
			14 => 
			array (
				'id' => 15,
				'name' => 'Hamstrings (Legs)',
				'created_at' => '2015-10-15 11:30:34',
				'updated_at' => '2015-10-15 11:30:51',
				'deleted_at' => NULL,
			),
			15 => 
			array (
				'id' => 16,
				'name' => 'Delts (Side)',
				'created_at' => '2015-10-15 11:30:34',
				'updated_at' => '2015-10-15 11:30:51',
				'deleted_at' => NULL,
			),
			16 => 
			array (
				'id' => 17,
				'name' => 'Delts (Rear)',
				'created_at' => '2015-10-15 11:30:34',
				'updated_at' => '2015-10-15 11:30:51',
				'deleted_at' => NULL,
			),
			17 => 
			array (
				'id' => 18,
				'name' => 'Delts (Front)',
				'created_at' => '2015-10-15 11:30:34',
				'updated_at' => '2015-10-15 11:30:51',
				'deleted_at' => NULL,
			),
		));
	}

}