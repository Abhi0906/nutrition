<?php

use Illuminate\Database\Seeder;

class FoodRecommendationTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::statement("SET FOREIGN_KEY_CHECKS=0;");
		\DB::table('food_recommendation_types')->truncate();
        
		
		\DB::table('food_recommendation_types')->insert(array (
			0 => 
			array (
				'id' => 1,
				'name' => 'Proteins',
				'tech_name' => 'proteins',
				'sub_type' => 'food',
				'sort_order'=>1,
				'created_at' => '2016-07-19 10:19:17',
				'updated_at' => '2016-07-19 10:19:17',
				'deleted_at' => NULL,
			),
			1 => 
			array (
				'id' => 2,
				'name' => 'Carbohydrates',
				'tech_name' => 'carbohydrates',
				'sub_type' => 'food',
				
				'sort_order'=>2,
				'created_at' => '2016-07-19 10:19:17',
				'updated_at' => '2016-07-19 10:19:17',
				'deleted_at' => NULL,
			),
			2 => 
			array (
				'id' => 3,
				'name' => 'Vegetables',
				'tech_name' => 'vegetables',
				'sub_type' => 'food',
				
				'sort_order'=>3,
				'created_at' => '2016-07-19 10:19:17',
				'updated_at' => '2016-07-19 10:19:17',
				'deleted_at' => NULL,
			),
			3 => 
			array (
				'id' => 4,
				'name' => 'Healthy Fats',
				'tech_name' => 'fealthy_fats',
				'sub_type' => 'food',
				
				'sort_order'=>4,
				'created_at' => '2016-07-19 10:19:17',
				'updated_at' => '2016-07-19 10:19:17',
				'deleted_at' => NULL,
			),
			4 => 
			array (
				'id' => 5,
				'name' => 'Supplements',
				'tech_name' => 'supplements',
				'sub_type' => 'supplement',
				
				'sort_order'=>5,
				'created_at' => '2016-07-19 10:19:17',
				'updated_at' => '2016-07-19 10:19:17',
				'deleted_at' => NULL,
			)
		));
		 \DB::statement("SET FOREIGN_KEY_CHECKS=1;");
	}

}
