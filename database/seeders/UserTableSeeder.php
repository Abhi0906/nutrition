<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		//\DB::table('users')->truncate();

		\DB::table('users')->insert(array (
			0 =>
			array (
				'id' => 1,
				'username' => 'george',
				'auth0_user_id'=>'auth0|57909e366ca6a5f732b6e624',
				'password' => '$2y$10$W04G5faD090xeZubAqx6j.4tAmjxFrd1YT2vJzIuOpIVaV.KpJq3q',
				'user_type' => 'super_admin',
				'first_name' => 'George',
				'middle_name' => NULL,
				'last_name' => 'Shanlikian',
				'gender' => 'male',
				'address' => NULL,
				'state' => NULL,
				'city' => NULL,
				'country' => NULL,
				'post_code' => NULL,
				'email' => 'admin@genemedics.com',
				'phone' => '800-277-4041',
				'photo' => 'george.png',
				'status' => 'Active',
				'doctor_title'=>'MD',
				'created_by' => NULL,
				'updated_by' => NULL,
				'birth_date' => NULL,
				'remember_token' => NULL,
				'created_at' => '2015-09-07 18:11:32',
				'updated_at' => '2015-09-07 18:11:32',
				'deleted_at' => NULL,
			),
			1 =>
			array (
				'id' => 2,
				'username' => 'system',
				'auth0_user_id'=>'auth0|57909e366ca6a5f732b6e624',
				'password' => '$2y$10$Cmld6bEhJPXRj/APF56EhOcQ0WUV1Cn5MnEQ59J4FeAq1tJn4.D92',
				'user_type' => 'system',
				'first_name' => 'system',
				'middle_name' => NULL,
				'last_name' => 'user',
				'gender' => 'male',
				'address' => NULL,
				'state' => NULL,
				'city' => NULL,
				'country' => NULL,
				'post_code' => NULL,
				'email' => 'systemuser@gmail.com',
				'phone' => '800-277-4041',
				'photo' => 'male.jpg',
				'status' => 'Active',
				'doctor_title'=>'MD',
				'created_by' => NULL,
				'updated_by' => NULL,
				'birth_date' => NULL,
				'remember_token' => NULL,
				'created_at' => '2015-09-07 18:11:32',
				'updated_at' => '2015-09-07 18:11:32',
				'deleted_at' => NULL,
			),
		));
	}

}
