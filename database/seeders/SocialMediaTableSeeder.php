<?php

use Illuminate\Database\Seeder;

class SocialMediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('social_media')->truncate();
        
		\DB::table('social_media')->insert(array (
			0 => 
			array (
				'id' => 1,
				'title' => 'Facebook',
				'url' => 'www.facebook.com',
				'slug' => 'fb',
				'sort_order' => '1',
				'is_default' => '1',
				'created_at' => '2015-09-14 12:21:32',
				'updated_at' => '2015-09-14 12:21:32',
				'deleted_at' => NULL,
			),
			1 => 
			array (
				'id' => 2,
				'title' => 'Twitter',
				'url' => 'www.twitter.com',
				'slug' => 'Tw',
				'sort_order' => '2',
				'is_default' => '1',
				'created_at' => '2015-09-14 12:21:32',
				'updated_at' => '2015-09-14 12:21:32',
				'deleted_at' => NULL,
			),
			2 => 
			array (
				'id' => 3,
				'title' => 'GooglePlus',
				'url' => 'www.googleplus.com',
				'slug' => 'Gp',
				'sort_order' => '3',
				'is_default' => '1',
				'created_at' => '2015-09-14 12:21:32',
				'updated_at' => '2015-09-14 12:21:32',
				'deleted_at' => NULL,
				
			),
			3 => 
			array (
				'id' => 4,
				'title' => 'YouTube',
				'url' => 'www.youtube.com',
				'slug' => 'Yt',
				'sort_order' =>'4',
				'is_default' => '1',
				'created_at' => '2015-09-14 12:21:32',
				'updated_at' => '2015-09-14 12:21:32',
				'deleted_at' => NULL,
			),	
		));
    }
}
