<?php
use Illuminate\Database\Seeder;
class RecommendedMealItemTypesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::statement("SET FOREIGN_KEY_CHECKS=0;");
		\DB::table('recommended_meal_item_types')->truncate();
        
		\DB::table('recommended_meal_item_types')->insert(array (
			0 => 
			array (
				'id' => 1,
				'item_name' => 'Proteins',
				'tech_name' => 'proteins',
				'sub_type' => 'food',
				'guid' => '{CEA7FEA0-F552-42B9-9AF9-84DD50FEEB44}',
				'timestamp' => '1465277461',
				'sort_order'=>1,
				'created_at' => '2016-07-19 10:19:17',
				'updated_at' => '2016-07-19 10:19:17',
				'deleted_at' => NULL,
			),
			1 => 
			array (
				'id' => 2,
				'item_name' => 'Carbohydrates',
				'tech_name' => 'carbohydrates',
				'sub_type' => 'food',
				'guid' => '{B86D59C4-E574-47CA-AD69-459269270A6B}',
				'timestamp' => '1467882285',
				'sort_order'=>2,
				'created_at' => '2016-07-19 10:19:17',
				'updated_at' => '2016-07-19 10:19:17',
				'deleted_at' => NULL,
			),
			2 => 
			array (
				'id' => 3,
				'item_name' => 'Vegetables',
				'tech_name' => 'vegetables',
				'sub_type' => 'food',
				'guid' => '{5FBD917C-7FAD-4AB6-8FCE-542A3B2BC0C7}',
				'timestamp' => '1466053953',
				'sort_order'=>3,
				'created_at' => '2016-07-19 10:19:17',
				'updated_at' => '2016-07-19 10:19:17',
				'deleted_at' => NULL,
			),
			3 => 
			array (
				'id' => 4,
				'item_name' => 'Healthy Fats',
				'tech_name' => 'fealthy_fats',
				'sub_type' => 'food',
				'guid' => '{0C085AD3-452F-42A6-BC06-E6B8D362C070}',
				'timestamp' => '1467890180',
				'sort_order'=>4,
				'created_at' => '2016-07-19 10:19:17',
				'updated_at' => '2016-07-19 10:19:17',
				'deleted_at' => NULL,
			),
			4 => 
			array (
				'id' => 5,
				'item_name' => 'Supplements',
				'tech_name' => 'supplements',
				'sub_type' => 'supplement',
				'guid' => '{0C085AD3-452F-42A6-BC06-E6B8D362C070}',
				'timestamp' => '1467890180',
				'sort_order'=>5,
				'created_at' => '2016-07-19 10:19:17',
				'updated_at' => '2016-07-19 10:19:17',
				'deleted_at' => NULL,
			)
		));

		\DB::statement("SET FOREIGN_KEY_CHECKS=1;");
	}

}
