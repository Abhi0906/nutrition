<?php
use Illuminate\Database\Seeder;
class DoctorTitlesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('doctor_titles')->truncate();
        
		\DB::table('doctor_titles')->insert(array (
			0 => 
			array (
				'id' => 1,
				'name' => 'MD',
				'sort_order' => 5,
				'created_at' => '2015-09-14 12:21:32',
				'updated_at' => '2015-09-14 12:21:32',
				'deleted_at' => NULL,
			),
			1 => 
			array (
				'id' => 2,
				'name' => 'RN',
				'sort_order' => 1,
				'created_at' => '2015-09-14 12:21:32',
				'updated_at' => '2015-09-14 12:21:32',
				'deleted_at' => NULL,
			),
			2 => 
			array (
				'id' => 3,
				'name' => 'Tech',
				'sort_order' => 3,
				'created_at' => '2015-09-14 12:21:32',
				'updated_at' => '2015-09-14 12:21:32',
				'deleted_at' => NULL,
				
			),
			3 => 
			array (
				'id' => 4,
				'name' => 'LPN',
				'sort_order' => 2,
				'created_at' => '2015-09-14 12:21:32',
				'updated_at' => '2015-09-14 12:21:32',
				'deleted_at' => NULL,
				
			),
			4 => 
			array (
				'id' => 5,
				'name' => 'LP',
				'sort_order' => 4,
				'created_at' => '2015-09-14 12:21:32',
				'updated_at' => '2015-09-14 12:21:32',
				'deleted_at' => NULL,
				
			),
			5 => 
			array (
				'id' => 6,
				'name' => 'DO',
				'sort_order' => 6,
				'created_at' => '2015-09-14 12:21:32',
				'updated_at' => '2015-09-14 12:21:32',
				'deleted_at' => NULL,
				
			),
		));
	}

}
