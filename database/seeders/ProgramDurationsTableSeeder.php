<?php
use Illuminate\Database\Seeder;
class ProgramDurationsTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::statement("SET FOREIGN_KEY_CHECKS=0;");
		\DB::table('program_durations')->truncate();
        
		\DB::table('program_durations')->insert(array (
			0 => 
			array (
				'id' => 1,
				'name' => '1 week',
				'days' => 7,
				'created_at' => '2015-11-02 11:21:58',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			1 => 
			array (
				'id' => 2,
				'name' => '2 week',
				'days' => 14,
				'created_at' => '2015-11-02 11:21:58',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),			
			2 => 
			array (
				'id' => 3,
				'name' => '30 days',
				'days' => 30,
				'created_at' => '2015-11-02 11:21:58',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			3 => 
			array (
				'id' => 4,
				'name' => '60 days',
				'days' => 60,
				'created_at' => '2015-11-02 11:21:58',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			4 => 
			array (
				'id' => 5,
				'name' => '90 days',
				'days' => 90,
				'created_at' => '2015-11-02 11:21:58',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			5 => 
			array (
				'id' => 6,
				'name' => '12 weeks',
				'days' => 84,
				'created_at' => '2015-11-02 11:21:58',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			6 => 
			array (
				'id' => 7,
				'name' => '16 weeks',
				'days' => 112,
				'created_at' => '2015-11-02 11:21:58',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			)
		));
       \DB::statement("SET FOREIGN_KEY_CHECKS=1;");
	}
}
