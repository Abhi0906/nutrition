<?php
use Illuminate\Database\Seeder;
class RoutineTimesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::statement("SET FOREIGN_KEY_CHECKS=0;");
		\DB::table('routine_times')->truncate();
        
		\DB::table('routine_times')->insert(array (
			0 => 
			array (
				'id' => 1,
				'event_type' => 'wakeup',
				'event_name' => 'Wakeup',
				'time' => '07:00:00',
				'is_primary' => '1',
				'sort_order' => 1,
				'created_at' => '2015-07-02 12:15:29',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			1 => 
			array (
				'id' => 2,
				'event_type' => 'breakfast',
				'event_name' => 'Breakfast',
				'time' => '08:00:00',
				'is_primary' => '1',
				'sort_order' => 2,
				'created_at' => '2015-07-02 12:29:14',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),			
			2 => 
			array (
				'id' => 3,
				'event_type' => 'lunch',
				'event_name' => 'Lunch',
				'time' => '12:30:00',
				'is_primary' => '1',
				'sort_order' => 3,
				'created_at' => '2015-07-02 12:29:14',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			3 => 
			array (
				'id' => 4,
				'event_type' => 'dinner',
				'event_name' => 'Dinner',
				'time' => '21:00:00',
				'is_primary' => '1',
				'sort_order' => 4,
				'created_at' => '2015-07-02 12:29:14',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			4 => 
			array (
				'id' => 5,
				'event_type' => 'sleep',
				'event_name' => 'Sleep',
				'time' => '23:00:00',
				'is_primary' => '1',
				'sort_order' => 5,
				'created_at' => '2015-07-02 12:29:14',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			)
		));
       \DB::statement("SET FOREIGN_KEY_CHECKS=1;");
	}
}
