<?php

use Illuminate\Database\Seeder;

class AllowedDeviceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('allowed_devices')->truncate();
        
		\DB::table('allowed_devices')->insert(array (

			0 => 
			array (
				
				'allowed_device' => 'Apple Health',
				'created_at' => '2015-09-14 12:21:32',
				'updated_at' => '2015-09-14 12:21:32',
				'deleted_at' => NULL,
			),

			// 1 => 
			// array (
				
			// 	'allowed_device' => 'MapMyFitness',
			// 	'created_at' => '2015-09-14 12:21:32',
			// 	'updated_at' => '2015-09-14 12:21:32',
			// 	'deleted_at' => NULL,
			// ),
			// 2 => 
			// array (
				
			// 	'allowed_device' => 'FitBit',				
			// 	'created_at' => '2015-09-14 12:21:32',
			// 	'updated_at' => '2015-09-14 12:21:32',
			// 	'deleted_at' => NULL,
			// ),
			// 3 => 
			// array (
				
			// 	'allowed_device' => 'BodyMedia FIT',
			// 	'created_at' => '2015-09-14 12:21:32',
			// 	'updated_at' => '2015-09-14 12:21:32',
			// 	'deleted_at' => NULL,
				
			// ),
			// 4 => 
			// array (
				
			// 	'allowed_device' => 'RunKeeper',
			// 	'created_at' => '2015-09-14 12:21:32',
			// 	'updated_at' => '2015-09-14 12:21:32',
			// 	'deleted_at' => NULL,
				
			// ),
			
			// 5 => 
			// array (
				
			// 	'allowed_device' => 'Withings Pulse',
			// 	'created_at' => '2015-09-14 12:21:32',
			// 	'updated_at' => '2015-09-14 12:21:32',
			// 	'deleted_at' => NULL,
				
			// ),
			// 6 => 
			// array (
				
			// 	'allowed_device' => 'Up',
			// 	'created_at' => '2015-09-14 12:21:32',
			// 	'updated_at' => '2015-09-14 12:21:32',
			// 	'deleted_at' => NULL,
				
			// ),
			// 7 => 
			// array (
				
			// 	'allowed_device' => 'Nike+',
			// 	'created_at' => '2015-09-14 12:21:32',
			// 	'updated_at' => '2015-09-14 12:21:32',
			// 	'deleted_at' => NULL,
				
			// ),
			// 8 => 
			// array (
				
			// 	'allowed_device' => 'OTbeat',
			// 	'created_at' => '2015-09-14 12:21:32',
			// 	'updated_at' => '2015-09-14 12:21:32',
			// 	'deleted_at' => NULL,
				
			// ),
			// 9 => 
			// array (
				
			// 	'allowed_device' => 'Microsoft Band',
			// 	'created_at' => '2015-09-14 12:21:32',
			// 	'updated_at' => '2015-09-14 12:21:32',
			// 	'deleted_at' => NULL,
				
			// ),
			
		));
    }
}
