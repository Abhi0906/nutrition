<?php

use Illuminate\Database\Seeder;

class MasterTemplatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        \DB::table('master_templates')->truncate();
        
		\DB::table('master_templates')->insert(array (
			
			0 => 
			array (
				'id' => 1,
				'parent' => 0,
				'template_name' => 'Nutrition & Exercise',
				'type' => NULL,
				'subtype' => NULL,
				'tech_name'=>'nutrition_exercise',
				'created_at' => '2017-08-31 13:32:03',
				'updated_at' => '2017-08-31 13:32:03',
				'deleted_at' => NULL,
			),
			1 => 
			array (
				'id' => 2,
				'parent' => 1,
				'template_name' => 'Pre-Workout Meal',
				'type' => 'Meal',
				'subtype' => 'breakfast',
				'tech_name'=>'pre_workout_meal',
				'created_at' => '2017-08-31 13:32:03',
				'updated_at' => '2017-08-31 13:32:03',
				'deleted_at' => NULL,
				
			),
			2 => 
			array (
				'id' => 3,
				'parent' => 1,
				'template_name' => 'Resistance Trainning',
				'type' => 'Workout',
				'subtype' => 'resistance',
				'tech_name'=>'resistance_trainning',
				'created_at' => '2017-08-31 13:32:03',
				'updated_at' => '2017-08-31 13:32:03',
				'deleted_at' => NULL,
				
			),
			3 => 
			array (
				'id' => 4,
				'parent' => 1,
				'template_name' => 'Cardio Trainning',
				'type' => 'Workout',
				'subtype' => 'cardio',
				'tech_name'=>'cardio_trainning',
				'created_at' => '2017-08-31 13:32:03',
				'updated_at' => '2017-08-31 13:32:03',
				'deleted_at' => NULL,
				
			),
			4 => 
			array (
				'id' => 5,
				'parent' => 1,
				'template_name' => 'Post-Workout Meal',
				'type' => 'Meal',
				'subtype' => 'breakfast',
				'tech_name'=>'post_workout_meal',
				'created_at' => '2017-08-31 13:32:03',
				'updated_at' => '2017-08-31 13:32:03',
				'deleted_at' => NULL,
				
			),
			5 => 
			array (
				'id' => 6,
				'parent' => 1,
				'template_name' => 'Meal 1 (Breakfast)',
				'type' => 'Meal',
				'subtype' => 'breakfast',
				'tech_name'=>'meal_1',
				'created_at' => '2017-08-31 13:32:03',
				'updated_at' => '2017-08-31 13:32:03',
				'deleted_at' => NULL,
				
			),
			6 => 
			array (
				'id' => 7,
				'parent' => 1,
				'template_name' => 'Meal 2 (Snacks)',
				'type' => 'Meal',
				'subtype' => 'snacks',
				'tech_name'=>'meal_2',
				'created_at' => '2017-08-31 13:32:03',
				'updated_at' => '2017-08-31 13:32:03',
				'deleted_at' => NULL,
				
			),
			7 => 
			array (
				'id' => 8,
				'parent' => 1,
				'template_name' => 'Meal 3 (Lunch)',
				'type' => 'Meal',
				'subtype' => 'lunch',
				'tech_name'=>'meal_3',
				'created_at' => '2017-08-31 13:32:03',
				'updated_at' => '2017-08-31 13:32:03',
				'deleted_at' => NULL,
				
			),
			8 => 
			array (
				'id' => 9,
				'parent' => 1,
				'template_name' => 'Meal 4 (Snacks)',
				'type' => 'Meal',
				'subtype' => 'snacks',
				'tech_name'=>'meal_4',
				'created_at' => '2017-08-31 13:32:03',
				'updated_at' => '2017-08-31 13:32:03',
				'deleted_at' => NULL,
				
			),
			9 => 
			array (
				'id' => 10,
				'parent' => 1,
				'template_name' => 'Meal 5 (Dinner)',
				'type' => 'Meal',
				'subtype' => 'dinner',
				'tech_name'=>'meal_5',
				'created_at' => '2017-08-31 13:32:03',
				'updated_at' => '2017-08-31 13:32:03',
				'deleted_at' => NULL,
				
			),
			10 => 
			array (
				'id' => 11,
				'parent' => 1,
				'template_name' => 'Meal 6 (Snacks)',
				'type' => 'Meal',
				'subtype' => 'snacks',
				'tech_name'=>'meal_6',
				'created_at' => '2017-08-31 13:32:03',
				'updated_at' => '2017-08-31 13:32:03',
				'deleted_at' => NULL,
				
			),
			11 => 
			array (
				'id' => 12,
				'parent' => 0,
				'template_name' => 'Supplement Templates',
				'type' => Null,
				'subtype' => Null,
				'tech_name'=>'supplement_templates',
				'created_at' => '2017-08-31 13:32:03',
				'updated_at' => '2017-08-31 13:32:03',
				'deleted_at' => NULL,
				
			),
			12 => 
			array (
				'id' => 13,
				'parent' => 0,
				'template_name' => 'Medication',
				'type' => Null,
				'subtype' => Null,
				'tech_name'=>'medication',
				'created_at' => '2017-08-31 13:32:03',
				'updated_at' => '2017-08-31 13:32:03',
				'deleted_at' => NULL,
				
			),
			

		));
    }
}
