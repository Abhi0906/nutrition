<?php

use Illuminate\Database\Seeder;

class TruncateUserDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement("SET FOREIGN_KEY_CHECKS=0;");
		\DB::table('assign_patients')->truncate();
		\DB::table('blood_pressure_parameters')->truncate();
		\DB::table('blood_sugar_parameters')->truncate();
		\DB::table('exercise_user')->truncate();
		\DB::table('notifications')->truncate();
		\DB::table('nutrition_user')->truncate();
		\DB::table('pulse_ox_parameters')->truncate();
		\DB::table('routine_time_user')->truncate();
		\DB::table('template_user')->truncate();
		\DB::table('user_goals')->truncate();
		\DB::table('user_profiles')->truncate();
		\DB::table('user_weight')->truncate();
		\DB::table('user_workout')->truncate();
		\DB::table('weight_parameters')->truncate();
		
		\DB::statement("SET FOREIGN_KEY_CHECKS=1;");
    }
}
