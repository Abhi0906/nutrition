<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Max Mustermann",
            'email' => "max.mustermann@contiva.com",
            'email_verified_at' => now(),
            'password' => bcrypt("ComeHome2020"), // password
            'remember_token' => Str::random(10),
            'superadmin' => 1,
        ]);

        DB::table('customers')->insert([
            'name' => "Contiva",
            'mail_domain' => "contiva.com",
            'globalmail' => "max.mustermann@contiva.com",
            'active' => 1,
        ]);

        DB::table('customers_user')->insert([
            'user_id' => 1,
            'customers_id' => 1,
            'admin' => 1
        ]);
    }
}
