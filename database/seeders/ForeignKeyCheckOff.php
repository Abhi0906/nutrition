<?php
use Illuminate\Database\Seeder;
class ForeignKeyCheckOff extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::statement("SET FOREIGN_KEY_CHECKS=0;");
	}

}
