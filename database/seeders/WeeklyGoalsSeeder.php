<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class WeeklyGoalsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('weekly_goals')->truncate();
        \DB::table('weekly_goals')->insert(array (
			
			0 => 
			array (
				'id' => 1,
				'name' => 'Lose 0.5 lbs per week',
				'value' => -0.5,
				'short_name' => 'Lose 0.5 lbs',
				'sort_order' => 1,
				'created_at' => '2016-02-03 03:43:32',
				'updated_at' => '2016-02-03 03:43:32'
			),
			1 => 
			array (
				'id' => 2,
			    'name' => 'Lose 1 lbs per week',
				'value' => -1,
				'short_name' => 'Lose 1 lbs',
				'sort_order' => 2,
				'created_at' => '2016-02-03 03:43:32',
				'updated_at' => '2016-02-03 03:43:32'
				
				
			),
			2 => 
			array (
				'id' => 3,
				'name' => 'Lose 1.5 lbs per week',
				'value' => -1.5,
				'short_name' => 'Lose 1.5 lbs',
				'sort_order' => 3,
				'created_at' => '2016-02-03 03:43:32',
				'updated_at' => '2016-02-03 03:43:32'
				
			),
			3 => 
			array (
				'id' => 4,
				'name' => 'Lose 2 lbs per week',
				'value' => -2,
				'short_name' => 'Lose 2 lbs',
				'sort_order' => 4,
				'created_at' => '2016-02-03 03:43:32',
				'updated_at' => '2016-02-03 03:43:32'
				
			),
			4 => 
			array (
				'id' => 5,
				'name' => 'Maintain current weight',
				'value' => 0,
				'short_name' => 'Maintain Weight',
				'sort_order' => 5,
				'created_at' => '2016-02-03 03:43:32',
				'updated_at' => '2016-02-03 03:43:32'
				
			),
			5 => 
			array (
				'id' => 6,
				'name' => 'Gain 0.5 lbs per week',
				'value' => 0.5,
				'short_name' => 'Gain 0.5 lbs',
				'sort_order' => 6,
				'created_at' => '2016-02-03 03:43:32',
				'updated_at' => '2016-02-03 03:43:32'
				
			),
			6 => 
			array (
				'id' => 7,
			    'name' => 'Gain 1 lbs per week',
				'value' => 1,
				'short_name' => 'Gain 1 lbs',
				'sort_order' => 7,
				'created_at' => '2016-02-03 03:43:32',
				'updated_at' => '2016-02-03 03:43:32'
				)
		));
    }
}