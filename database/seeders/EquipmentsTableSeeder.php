<?php
use Illuminate\Database\Seeder;
class EquipmentsTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		
		\DB::table('equipments')->truncate();
        
		\DB::table('equipments')->insert(array (
			
			0 => 
			array (
				'id' => 1,
				'display_name' => 'Aerobics Step',
				'name' => 'aerobics_step',
				'sort_order' => 1,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
			),
			1 => 
			array (
				'id' => 2,
				'display_name' => 'Barbell',
				'name' => 'barbell',
				'sort_order' => 2,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),
			2 => 
			array (
				'id' => 3,
				'display_name' => 'Bench',
				'name' => 'bench',
				'sort_order' => 3,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),
			3 => 
			array (
				'id' => 4,
				'display_name' => 'Dumbbell',
				'name' => 'dumbbell',
				'sort_order' => 4,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),
			4 => 
			array (
				'id' => 5,
				'display_name' => 'Exercise Band',
				'name' => 'exercise_band',
				'sort_order' => 5,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),
			5 => 
			array (
				'id' => 6,
				'display_name' => 'Jump Rope',
				'name' => 'jump_rope',
				'sort_order' => 6,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),
			6 => 
			array (
				'id' => 7,
				'display_name' => 'Kettlebell',
				'name' => 'kettlebell',
				'sort_order' => 7,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),
			7 => 
			array (
				'id' => 8,
				'display_name' => 'Mat',
				'name' => 'mat',
				'sort_order' => 8,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),
			8 => 
			array (
				'id' => 9,
				'display_name' => 'Medicine Ball',
				'name' => 'medicine_ball',
				'sort_order' => 9,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),
			9 => 
			array (
				'id' => 10,
				'display_name' => 'No Equipment',
				'name' => 'no_equipment',
				'sort_order' => 10,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),
			10 => 
			array (
				'id' => 11,
				'display_name' => 'Physio-Ball',
				'name' => 'physio_ball',
				'sort_order' => 11,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),
			11 => 
			array (
				'id' => 12,
				'display_name' => 'Sandbag',
				'name' => 'sandbag',
				'sort_order' => 12,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),
			12 => 
			array (
				'id' => 13,
				'display_name' => 'Slosh Tube',
				'name' => 'slosh_tube',
				'sort_order' => 13,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),
			13 => 
			array (
				'id' => 14,
				'display_name' => 'Stationary Bike',
				'name' => 'stationary_bike',
				'sort_order' => 14,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),

			14 => 
			array (
				'id' => 15,
				'display_name' => 'Yoga Block',
				'name' => 'yoga_block',
				'sort_order' => 15,
				'created_at' => '2015-10-03 12:33:32',
				'updated_at' => '2015-10-03 12:33:32',
				'deleted_at' => NULL,
				
			),

		));
	}

}
