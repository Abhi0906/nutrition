<?php
namespace Database\Seeders;
class ActivitiesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::statement("SET FOREIGN_KEY_CHECKS=0;");
		\DB::table('activities')->truncate();

		\DB::table('activities')->insert(array (

			0 =>
			array (
				'id' => 1,
				'name' => 'Not Very Active - Spend most of the day sitting. (i.e desk job).',
				'value'=>'1.2',
				'sort_order' => 1,
				'created_at' => '2015-09-21 4:11:32',
				'updated_at' => '2015-09-21 4:11:32',
				'deleted_at' => NULL,
			),
			1 =>
			array (
				'id' => 2,
				'name' => 'Lightly Active - Spend a good part of the day on your feet. (i.e teacher)',
				'value'=>'1.375',
				'sort_order' => 2,
				'created_at' => '2015-09-21 4:11:32',
				'updated_at' => '2015-09-21 4:11:32',
				'deleted_at' => NULL,

			),
			2 =>
			array (
				'id' => 3,
				'name' => 'Active - Spend time doing light physical activity. (i.e postal carrier)',
				'value'=>'1.55',
				'sort_order' => 3,
				'created_at' => '2015-09-21 4:11:32',
				'updated_at' => '2015-09-21 4:11:32',
				'deleted_at' => NULL,

			),
			3 =>
			array (
				'id' => 4,
				'name' => 'Very Active - Spend a lot of time doing heavy physical activity. (i.e carpenter)',
				'value'=>'1.725',
				'sort_order' => 4,
				'created_at' => '2015-09-21 4:11:32',
				'updated_at' => '2015-09-21 4:11:32',
				'deleted_at' => NULL,

			)

		));
		\DB::statement("SET FOREIGN_KEY_CHECKS=1;");

	}

}
