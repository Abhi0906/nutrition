<?php
use Illuminate\Database\Seeder;
class ConfigurationsTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('configurations')->truncate();
        
		\DB::table('configurations')->insert(array (
			0 => 
			array (
				'id' => 1,
				'title' => 'Weight',
				'description' => 'To set default baseline weight value',
				'param' => 'baseline_weight',
				'value' => '0',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			1 => 
			array (
				'id' => 2,
				'title' => 'Weight',
				'description' => 'To set default max weight value',
				'param' => 'max_weight',
				'value' => '0',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			2 => 
			array (
				'id' => 3,
				'title' => 'Weight',
				'description' => 'To set default min weight value',
				'param' => 'min_weight',
				'value' => '0',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			3 => 
			array (
				'id' => 4,
				'title' => 'Weight',
				'description' => 'To set default value to calculate high alert for daily gain/loss of weight',
				'param' => 'high_alert_lbs',
				'value' => '4',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			4 => 
			array (
				'id' => 5,
				'title' => 'Weight',
				'description' => 'To set default value to calculate high alert in % for daily gain/loss of weight',
				'param' => 'high_alert_per',
				'value' => '3',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			5 => 
			array (
				'id' => 6,
				'title' => 'Weight',
				'description' => 'To set default value to calculate medium alert for daily gain/loss of weight',
				'param' => 'med_alert_lbs',
				'value' => '3',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			6 => 
			array (
				'id' => 7,
				'title' => 'Weight',
				'description' => 'To set default value to calculate medium alert in % for daily gain/loss of weight',
				'param' => 'med_alert_per',
				'value' => '2',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			7 => 
			array (
				'id' => 8,
				'title' => 'Weight',
				'description' => 'To set default value to calculate medium alert for weekly gain/loss of weight',
				'param' => 'med_alert_lbs_weekly',
				'value' => '4',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			8 => 
			array (
				'id' => 9,
				'title' => 'Weight',
				'description' => 'To set default value to calculate medium alert in % for weekly gain/loss of weight',
				'param' => 'med_alert_per_weekly',
				'value' => '3',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			9 => 
			array (
				'id' => 10,
				'title' => 'Weight',
				'description' => 'To calculate medium alert on previous reading',
				'param' => 'med_alert_day',
				'value' => '7',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			10 => 
			array (
				'id' => 11,
				'title' => 'PulseOx',
				'description' => 'To set default value to calculate high alert of pulse',
				'param' => 'high_alert_pulse',
				'value' => '115',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			11 => 
			array (
				'id' => 12,
				'title' => 'PulseOx',
				'description' => 'To set default value to calculate medium alert of pulse',
				'param' => 'med_alert_pulse',
				'value' => '45',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			12 => 
			array (
				'id' => 13,
				'title' => 'PulseOx',
				'description' => 'To set default value to calculate high alert of oxygen',
				'param' => 'high_alert',
				'value' => '90',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			13 => 
			array (
				'id' => 14,
				'title' => 'PulseOx',
				'description' => 'To set default value to calculate medium alert of oxygen',
				'param' => 'med_alert',
				'value' => '94',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			14 => 
			array (
				'id' => 15,
				'title' => 'BloodPressure',
				'description' => 'To set default value to calculate Systolic upper boundry high alert',
				'param' => 'systolic_high_ul',
				'value' => '180',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			15 => 
			array (
				'id' => 16,
				'title' => 'BloodPressure',
				'description' => 'To set default value to calculate Systolic upper boundry medium alert',
				'param' => 'systolic_med_ul',
				'value' => '150',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			16 => 
			array (
				'id' => 17,
				'title' => 'BloodPressure',
				'description' => 'To set default value to calculate Systolic lower boundry high alert',
				'param' => 'systolic_high_ll',
				'value' => '90',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			17 => 
			array (
				'id' => 18,
				'title' => 'BloodPressure',
				'description' => 'To set default value to calculate Systolic lower boundry medium alert',
				'param' => 'systolic_med_ll',
				'value' => '100',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			18 => 
			array (
				'id' => 19,
				'title' => 'BloodPressure',
				'description' => 'To set default value to calculate Diastolic upper boundry high alert',
				'param' => 'diastolic_high_ul',
				'value' => '120',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			19 => 
			array (
				'id' => 20,
				'title' => 'BloodPressure',
				'description' => 'To set default value to calculate Diastolic upper boundry medium alert',
				'param' => 'diastolic_med_ul',
				'value' => '90',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			20 => 
			array (
				'id' => 21,
				'title' => 'BloodPressure',
				'description' => 'To set default value to calculate Diastolic lower boundry medium alert',
				'param' => 'diastolic_med_ll',
				'value' => '60',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			21 => 
			array (
				'id' => 22,
				'title' => 'BloodSugar',
				'description' => 'To set lower boundry high alert limit for all types of readings',
				'param' => 'lb_high',
				'value' => '55',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			22 => 
			array (
				'id' => 23,
				'title' => 'BloodSugar',
				'description' => 'To set lower boundry medium alert limit for all types of reading',
				'param' => 'lb_med',
				'value' => '70',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			23 => 
			array (
				'id' => 24,
				'title' => 'BloodSugar',
				'description' => 'To set upper boundry medium alert limit for fasting reading',
				'param' => 'ub_fasting_med',
				'value' => '100',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			24 => 
			array (
				'id' => 25,
				'title' => 'BloodSugar',
				'description' => 'To set follow up delay for reading in Hours',
				'param' => 'ub_pp_hours',
				'value' => '2.5',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			25 => 
			array (
				'id' => 26,
				'title' => 'BloodSugar',
				'description' => 'To set upper boundry high alert limit for random reading',
				'param' => 'ub_random_chk_high',
				'value' => '150',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			26 => 
			array (
				'id' => 27,
				'title' => 'BloodSugar',
				'description' => 'To set upper boundry medium alert limit for random reading',
				'param' => 'ub_random_chk_med',
				'value' => '120',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			27 => 
			array (
				'id' => 28,
				'title' => 'BloodSugar',
				'description' => 'To set upper boundry medium alert limit for post-prandial reading',
				'param' => 'ub_pp_med',
				'value' => '180',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			28 => 
			array (
				'id' => 29,
				'title' => 'BloodSugar',
				'description' => 'To set post-prandial reading increase limit to generate high alert',
				'param' => 'is_single_reading',
				'value' => 'False',
				'created_at' => '2015-09-21 09:48:12',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			29 => 
			array (
				'id' => 30,
				'title' => 'VitalUOM',
		    	'description' => 'Unit of measurement(weight)',
				'param' => 'weight',
				'value' => 'kg',
				'created_at' => '2015-04-06 11:14:26',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			30 => 
			array (
				'id' => 31,
				'title' => 'VitalUOM',
				'description' => 'Unit of measurement(Blood Sugar)',
				'param' => 'blood_sugar',
				'value' => 'mg/dL',
				'created_at' => '2015-04-06 11:14:35',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			31 => 
			array (
				'id' => 32,
				'title' => 'VitalUOM',
				'description' => 'Unit of measurement(Pulse)',
				'param' => 'pulse',
				'value' => 'bpm',
				'created_at' => '2015-04-06 11:17:23',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			32 => 
			array (
				'id' => 33,
				'title' => 'VitalUOM',
				'description' => 'Unit of measurement(Oxygen)',
				'param' => 'oxygen',
				'value' => '%',
				'created_at' => '2015-04-06 11:18:16',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			33 => 
			array (
				'id' => 34,
				'title' => 'VitalUOM',
				'description' => 'Unit of measurement(Blood Pressure)',
				'param' => 'blood_presure',
				'value' => 'mm Hg',
				'created_at' => '2015-04-06 11:19:39',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			34 => 
			array (
				'id' => 35,
				'title' => 'randomvital',
				'description' => 'Display weight in Random group',
				'param' => 'weight',
				'value' => 'true',
				'created_at' => '2015-04-07 18:02:03',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			35 => 
			array (
				'id' => 36,
				'title' => 'randomvital',
				'description' => 'Display pulse-ox in Random group',
				'param' => 'pulse-ox',
				'value' => 'true',
				'created_at' => '2015-04-07 12:52:43',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			36 => 
			array (
				'id' => 37,
				'title' => 'randomvital',
				'description' => 'Display bp in Random group',
				'param' => 'bp',
				'value' => 'true',
				'created_at' => '2015-04-07 12:52:51',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			),
			37 => 
			array (
				'id' => 38,
				'title' => 'randomvital',
				'description' => 'Display blood sugar in Random group',
				'param' => 'blood_sugar',
				'value' => 'true',
				'created_at' => '2015-04-07 12:53:00',
				'updated_at' => NULL,
				'deleted_at' => NULL,
			)
		));
	}

}
