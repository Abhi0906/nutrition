<?php
use Illuminate\Database\Seeder;
class WorkoutTypesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('workout_types')->truncate();
        
		\DB::table('workout_types')->insert(array (
			0 => 
			array (
				'id' => 1,
				'title' => 'Daily',
				'type'=>'daily',
				'no_of_times'=>1,
				'created_at' => '2015-10-19 11:08:37',
				'updated_at' => '2015-10-19 11:08:48',
				'deleted_at' => NULL,
			),
			1 => 
			array (
				'id' => 2,
				'title' => 'Weekly',
				'type'=>'weekly',
				'no_of_times'=>1,
				'created_at' => '2015-10-19 11:08:40',
				'updated_at' => '2015-10-19 11:08:51',
				'deleted_at' => NULL,
			),
			2 => 
			array (
				'id' => 3,
				'title' => 'Twice in a week',
				'type'=>'twice_in_a_week',
				'no_of_times'=>2,
				'created_at' => '2015-10-19 11:08:42',
				'updated_at' => '2015-10-19 11:08:53',
				'deleted_at' => NULL,
			),
			3 => 
			array (
				'id' => 4,
				'title' => 'Three times in a week',
				'type'=>'three_times_in_a_week',
				'no_of_times'=>3,
				'created_at' => '2015-10-19 11:08:44',
				'updated_at' => '2015-10-19 11:08:55',
				'deleted_at' => NULL,
			),
		));
	}

}
