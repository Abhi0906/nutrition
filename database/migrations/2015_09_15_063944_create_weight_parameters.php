<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeightParameters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weight_parameters', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->decimal('baseline_weight', 18, 1)->nullable();
            $table->decimal('max_weight', 18, 1)->nullable();
            $table->decimal('min_weight', 18, 1)->nullable();
            $table->decimal('high_alert_lbs', 18, 1)->nullable();
            $table->integer('high_alert_per')->nullable();
            $table->decimal('med_alert_lbs', 18, 1)->nullable();
            $table->integer('med_alert_per')->nullable();
            $table->integer('med_alert_day')->nullable();
            $table->decimal('med_alert_lbs_weekly', 18, 1)->nullable();
            $table->integer('med_alert_per_weekly')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('weight_parameters');
    }
}
