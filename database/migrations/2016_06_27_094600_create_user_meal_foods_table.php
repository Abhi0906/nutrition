<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMealFoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_meal_foods', function (Blueprint $table){
            $table->increments('id');
            $table->integer('meal_id')->unsigned()->index();
            $table->integer('nutrition_id')->unsigned()->index();
            $table->double('calories', 8, 2)->nullable();
            $table->double('serving_quantity', 8, 2)->nullable();
            $table->string('serving_size_unit',255)->nullable();
            $table->double('no_of_servings', 8, 2)->nullable();
            $table->tinyInteger('is_custom')->default("0");

            $table->timestamps();
            $table->softDeletes();
            $table->foreign('meal_id')->references('id')->on('user_meals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_meal_foods');
    }
}
