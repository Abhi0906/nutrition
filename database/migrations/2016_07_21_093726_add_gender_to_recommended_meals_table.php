<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGenderToRecommendedMealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recommended_meals', function (Blueprint $table) {
            $table->enum('meal_for', ['Male', 'Female', 'Any'])->default('Any')->after('meal_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recommended_meals', function (Blueprint $table) {
            $table->dropColumn('meal_for');
        });
    }
}
