<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meal_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();
            $table->enum('gender', ['Male', 'Female'])->default('Male');
            $table->enum('plan_type', ['complete_plan', 'day_plan'])->nullable()->default('complete_plan');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('meal_plans');
    }
}
