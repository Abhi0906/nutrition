<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserGoalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_goals', function (Blueprint $table) {

            $table->float('calorie_intake_g')->nullable()->after('goal_body_fat');
            $table->float('calorie_burned_g')->nullable()->after('calorie_intake_g');
            $table->date('weight_goal_date')->nullable()->after('calorie_burned_g');
            $table->date('body_measurement_goal_date')->nullable()->after('weight_goal_date');
            $table->float('neck_g')->nullable()->after('body_measurement_goal_date');
            $table->float('arms_g')->nullable()->after('neck_g');
            $table->float('waist_g')->nullable()->after('arms_g');
            $table->float('hips_g')->nullable()->after('waist_g');
            $table->float('legs_g')->nullable()->after('hips_g');
            $table->date('bp_pulse_goal_date')->nullable()->after('legs_g');
            $table->float('systolic_from')->nullable()->after('bp_pulse_goal_date');
            $table->float('systolic_to')->nullable()->after('systolic_from');
            $table->float('diastolic_from')->nullable()->after('systolic_to');
            $table->float('diastolic_to')->nullable()->after('diastolic_from');
            $table->float('heart_rate_from')->nullable()->after('diastolic_to');
            $table->float('heart_rate_to')->nullable()->after('heart_rate_from');
            $table->date('sleep_goal_date')->nullable()->after('heart_rate_to');
            $table->float('sleep_g')->nullable()->after('sleep_goal_date');
            $table->string('guid')->nullable()->after('sleep_g');
            $table->string('timestamp', 100)->nullable()->after('guid');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_goals', function($table)
        {
            $table->dropColumn('calorie_intake_g');
            $table->dropColumn('calorie_burned_g');
            $table->dropColumn('weight_goal_date');
            $table->dropColumn('body_measurement_goal_date');
            $table->dropColumn('neck_g');
            $table->dropColumn('arms_g');
            $table->dropColumn('waist_g');
            $table->dropColumn('hips_g');
            $table->dropColumn('legs_g');
            $table->dropColumn('bp_pulse_goal_date');
            $table->dropColumn('systolic_from');
            $table->dropColumn('systolic_to');
            $table->dropColumn('diastolic_from');
            $table->dropColumn('diastolic_to');
            $table->dropColumn('heart_rate_from');
            $table->dropColumn('heart_rate_to');
            $table->dropColumn('sleep_goal_date');
            $table->dropColumn('sleep_g');
            $table->dropColumn('guid');
            $table->dropColumn('timestamp');
        });
    }
}
