<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsDefaultColumnToExercises extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exercises', function($table)
        {  
            $table->string('tech_name', 200)->nullable()->after('name'); 
            $table->boolean('trackable', 100)->default("0")->after('calories_burned');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exercises', function($table){
            $table->dropColumn('tech_name');
            $table->dropColumn('trackable');
        });
    }
}
