<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNutritionExercisePlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nutrition_exercise_plan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('meal_plan_id')->unsigned()->index();
            $table->integer('master_template_id')->unsigned()->index();
            $table->integer('meal_id')->unsigned()->index()->nullable();
            $table->integer('workout_id')->unsigned()->index()->nullable();
            $table->integer('day_plan_id')->nullable();
            $table->enum('meal_template_type', ['day_meal_template', 'complete_meal_template','complete_day_template'])->nullable();
          
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nutrition_exercise_plan');
    }
}
