<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNutritionExerciseDayTimes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nutrition_exercise_day_times', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nutrition_exercise_plan_id')->unsigned()->index();
            $table->integer('meal_plan_id')->unsigned()->index();
            $table->string('dayName',10)->nullable();
            $table->integer('dayNumber')->nullable();
            $table->float('meal_time',4,2)->nullable();
            $table->boolean('notification')->nullable()->default(1);
            $table->string('guid',255)->nullable();
            $table->foreign('nutrition_exercise_plan_id')->references('id')->on('nutrition_exercise_plan')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nutrition_exercise_day_times');
    }
}
