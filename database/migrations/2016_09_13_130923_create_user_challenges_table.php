<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserChallengesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('user_challenges', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->text('description')->nullable(); 
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->text('allowed_devices')->nullable();
            $table->string('goal_type')->nullable();
            $table->float('target_calories')->nullable();
            $table->float('daily_max_credit_calories')->nullable();
            $table->integer('target_steps')->nullable();
            $table->integer('daily_max_steps')->nullable();
            $table->boolean('is_expired')->default("0");
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('user_challenges');
    }
}
