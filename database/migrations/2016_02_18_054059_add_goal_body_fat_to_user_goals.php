<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGoalBodyFatToUserGoals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('user_goals', function (Blueprint $table) {

            $table->float('goal_body_fat')->nullable()->after('goal_weight');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_goals', function (Blueprint $table) {
            $table->dropColumn('goal_body_fat');
        });
    }
}
