<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAndAddColumnBpPulse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_bp_pulse', function (Blueprint $table) {

             $table->dropColumn('pulse');
             $table->integer('user_pulse_id')->unsigned()->index()->after('user_id');
           //  $table->foreign('user_pulse_id')->references('id')->on('user_pulse');
        });
    }   

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_bp_pulse', function (Blueprint $table) {
            $table->float('pulse')->nullable();
            //$table->dropForeign('user_bp_pulse_user_pulse_id_foreign');
            $table->dropColumn('user_pulse_id');
           
        });
        
    }
}
