<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSleep extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_sleep', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('minutes_fall_alseep')->nullable();
            $table->integer('rise_bed_time')->nullable();
            $table->integer('went_bed_time')->nullable();
            $table->integer('times_awakened')->nullable();
            $table->float('times_awakened_minutes')->nullable();
            $table->date('rise_bed_date')->nullable();
            $table->date('went_bed_date')->nullable();
            $table->float('total_sleep')->nullable();
            $table->string('guid')->nullable();
            $table->string('timestamp')->nullable();
            $table->string('source')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_sleep');
    }
}
