<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserWorkoutRoutineExecisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('user_workout_routine_exercises', function (Blueprint $table){
            $table->increments('id');
            $table->integer('user_workout_routine_id')->unsigned()->index();
            $table->integer('exercise_image_id')->unsigned()->index();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->integer('spend_time_in_second')->nullable();
            $table->string('exercise_type')->nullable();
            $table->integer('sets')->nullable();
            $table->integer('reps')->nullable();
            $table->float('weights')->nullable();
            $table->integer('cardio_duration')->nullable();
            $table->float('cardio_calories')->nullable();
            $table->float('cardio_distance')->nullable();
            $table->float('cardio_avg_speed')->nullable();
            $table->float('cardio_avg_heartrate')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('user_workout_routine_exercises');
    }
}
