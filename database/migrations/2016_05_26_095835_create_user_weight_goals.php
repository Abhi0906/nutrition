<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserWeightGoals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_weight_goals', function(Blueprint $table) {
          
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('weekly_goal_id')->unsigned()->index();
            $table->float('weight_g')->nullable();
            $table->float('body_fat_g')->nullable();
            $table->float('start_weight')->nullable();
            $table->float('current_weight')->nullable();
            $table->float('start_body_fat')->nullable();
            $table->float('current_body_fat')->nullable();
            $table->date('goal_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_weight_goals');
    }
}
