<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveParentFromFoodRecommendationTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('food_recommendation_types', function (Blueprint $table) {
            $table->dropColumn('parent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('food_recommendation_types', function (Blueprint $table) {
            $table->integer('parent')->nullable()->after('id');
        });
    }
}
