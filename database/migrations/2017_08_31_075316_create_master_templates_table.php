<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent');
            $table->string('template_name');
            $table->string('type')->nullable();
            $table->string('subtype',100)->nullable();
            $table->string('tech_name',100)->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_templates');
    }
}
