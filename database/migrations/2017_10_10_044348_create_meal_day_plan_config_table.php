<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealDayPlanConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meal_day_plan_config', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('meal_day_plan_id')->unsigned()->index();
            $table->integer('complete_plan_id')->unsigned()->index();
            $table->string('dayName',10)->nullable();
            $table->integer('dayNumber')->nullable();
            $table->string('guid',255)->nullable();
            $table->foreign('meal_day_plan_id')->references('id')->on('meal_day_plan')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('meal_day_plan_config');
    }
}
