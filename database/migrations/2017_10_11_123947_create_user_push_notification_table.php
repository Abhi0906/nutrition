<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPushNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('user_pushnotification', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('content_id')->unsigned()->index();
            $table->string('title',255)->nullable();
            $table->string('type',100)->nullable();
            $table->string('desc',255)->nullable();
            $table->string('guid',255)->nullable();
            $table->text('config')->nullable();
            $table->boolean('is_taken')->default("0")->nullable();
            $table->boolean('is_read')->default("0")->nullable();
            $table->timestamps();
            $table->softDeletes();
           

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_pushnotification');
    }
}
