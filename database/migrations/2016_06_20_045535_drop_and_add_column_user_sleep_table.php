<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAndAddColumnUserSleepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_sleep', function (Blueprint $table) {
             $table->dropColumn('rise_bed_time');
             $table->dropColumn('went_bed_time');
             $table->dropColumn('rise_bed_date');
             $table->dropColumn('went_bed_date');
             $table->dateTime('rise_from_bed')->nullable()->after('times_awakened');
             $table->dateTime('went_to_bed')->nullable()->after('times_awakened');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_sleep', function (Blueprint $table) {
           $table->dropColumn('rise_from_bed');
           $table->dropColumn('went_to_bed');
        });
    }
}