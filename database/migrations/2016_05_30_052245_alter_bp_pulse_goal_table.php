<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBpPulseGoalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('user_bp_pulse_goals', function (Blueprint $table) {

             $table->date('start_date')->nullable()->after('goal_date');
             $table->float('goal')->nullable()->after('bp_type');
             $table->dropColumn('from_goal');
             $table->dropColumn('to_goal');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('user_bp_pulse_goals', function (Blueprint $table) {

               $table->dropColumn('start_date');
               $table->dropColumn('goal');
               $table->float('from_goal')->nullable();
               $table->float('to_goal')->nullable();
            
        });
    }
}
