<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecommendedMealFoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recommended_meal_foods', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('meal_id')->unsigned()->index();
            $table->integer('item_id')->unsigned()->index();
            $table->integer('nutrition_id')->unsigned()->index();
            $table->double('calories', 8, 2)->nullable();
            $table->string('guid')->nullable();
            $table->string('timestamp')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('meal_id')->references('id')->on('recommended_meals');
            $table->foreign('item_id')->references('id')->on('recommended_meal_item_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('recommended_meal_foods');
    }
}
