<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGuidTimestampToUserWaterintakeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_waterintake', function (Blueprint $table) {
             $table->string('guid')->nullable()->after('log_water');
             $table->string('timestamp', 100)->nullable()->after('log_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_waterintake', function (Blueprint $table) {
              $table->dropColumn('guid');
              $table->dropColumn('timestamp');
            //
        });
    }
}
