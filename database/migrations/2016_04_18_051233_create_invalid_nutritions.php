<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvalidNutritions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invalid_nutritions', function(Blueprint $table) {
          
            $table->increments('id');
            $table->string('row_id')->nullable();    
            $table->text('barnd_with_name')->nullable();        
            $table->text('nutrition_data')->nullable();
            $table->text('serving_data')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('invalid_nutritions');
    }
}
