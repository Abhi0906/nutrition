<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomNutritionUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_nutrition_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('nutrition_id')->unsigned()->index();
            $table->string('guid')->nullable();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('nutrition_id')->references('id')->on('nutritions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('custom_nutrition_user');
    }
}
