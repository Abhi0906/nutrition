<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCustomNutritionUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('custom_nutrition_user', function (Blueprint $table) {
             $table->dropForeign(['nutrition_id']);
             $table->dropIndex(['nutrition_id']);
             $table->dropColumn('nutrition_id');
             $table->string('name')->index()->nullable()->after('user_id'); 
             $table->string('brand_name')->index()->nullable()->after('name');   
             $table->string('brand_with_name',500)->index()->nullable()->after('brand_name');        
             $table->text('nutrition_data')->nullable()->after('brand_with_name');
             $table->string('timestamp', 100)->nullable()->after('guid');
            
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('custom_nutrition_user', function($table) {

             $table->integer('nutrition_id')->unsigned();
             $table->dropColumn('name');
             $table->dropColumn('brand_name');
             $table->dropColumn('brand_with_name');
             $table->dropColumn('nutrition_data');
             $table->dropColumn('timestamp');
        });
    }
}
