<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterWeightGoalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_weight_goals', function (Blueprint $table) {

             $table->date('weight_start_date')->nullable()->after('goal_date');
             $table->date('body_fat_start_date')->nullable()->after('weight_start_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_weight_goals', function (Blueprint $table) {

            $table->dropColumn('weight_start_date');
            $table->dropColumn('body_fat_start_date');
             
        });
    }
}
