<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealDayPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meal_day_plan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('complete_plan_id')->unsigned()->index();
            $table->integer('day_plan_id')->unsigned()->index();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('meal_day_plan');
    }
}
