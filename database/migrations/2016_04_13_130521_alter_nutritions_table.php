<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNutritionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nutritions', function (Blueprint $table) {
             $table->string('name_with_brand',500)->index()->nullable()->after('name');
             $table->string('sodium_unit', 10)->nullable()->after('sodium');
             $table->string('total_fat_unit', 10)->nullable()->after('total_fat');
             $table->string('potassium_unit', 10)->nullable()->after('potassium');
             $table->string('saturated_fat_unit', 10)->nullable()->after('saturated_fat');
             $table->string('total_carb_unit', 10)->nullable()->after('total_carb');
             $table->string('dietary_fiber_unit', 10)->nullable()->after('dietary_fiber');
             $table->string('sugars_unit', 10)->nullable()->after('sugars');
             $table->string('trans_fat_unit', 10)->nullable()->after('trans_fat');
             $table->string('protein_unit', 10)->nullable()->after('protein');
             $table->string('cholesterol_unit', 10)->nullable()->after('cholesterol');
             $table->string('vitamin_a_unit', 10)->nullable()->after('vitamin_a');
             $table->string('calcium_dv_unit', 10)->nullable()->after('calcium_dv');
             $table->string('vitamin_c_unit', 10)->nullable()->after('vitamin_c');
             $table->string('iron_dv_unit', 10)->nullable()->after('iron_dv');
             $table->float('polyunsaturated')->nullable()->after('iron_dv_unit');
             $table->string('polyunsaturated_unit', 10)->nullable()->after('polyunsaturated');
             $table->float('monounsaturated')->nullable()->after('polyunsaturated_unit');
             $table->string('monounsaturated_unit', 10)->nullable()->after('monounsaturated');
             $table->string('source_name',100)->nullable()->after('ingredient_statement');
             $table->text('servings_json')->nullable()->after('servings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nutritions', function (Blueprint $table) {
              $table->dropColumn('name_with_brand');
              $table->dropColumn('source_name');
              $table->dropColumn('servings_json');
              $table->dropColumn('sodium_unit');
              $table->dropColumn('total_fat_unit');
              $table->dropColumn('potassium_unit');
              $table->dropColumn('saturated_fat_unit');
              $table->dropColumn('total_carb_unit');
              $table->dropColumn('dietary_fiber_unit');
              $table->dropColumn('sugars_unit');
              $table->dropColumn('trans_fat_unit');
              $table->dropColumn('protein_unit');
              $table->dropColumn('cholesterol_unit');
              $table->dropColumn('vitamin_a_unit');
              $table->dropColumn('calcium_dv_unit');
              $table->dropColumn('vitamin_c_unit');
              $table->dropColumn('iron_dv_unit');
              $table->dropColumn('polyunsaturated');
              $table->dropColumn('polyunsaturated_unit');
              $table->dropColumn('monounsaturated');
              $table->dropColumn('monounsaturated_unit');

            
        });
    }
}
