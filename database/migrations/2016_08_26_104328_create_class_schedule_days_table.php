<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassScheduleDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_schedule_days', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('class_schedule_id')->unsigned()->nullable()->index();
            $table->integer('day_index')->nullable();
            $table->string('class_start_time',10)->nullable();
            $table->string('class_end_time',10)->nullable();
            $table->timestamps();

            $table->foreign('class_schedule_id')->references('id')->on('class_schedules');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('class_schedule_days');
    }
}
