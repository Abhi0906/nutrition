<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreatedByAndUpdatedByUsrebppulseUserpulseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('user_bp_pulse', function (Blueprint $table) {
              $table->integer('created_by')->nullable()->after('updated_at');
              $table->integer('updated_by')->nullable()->after('created_by');
        });
        Schema::table('user_pulse', function (Blueprint $table) {
              $table->integer('created_by')->nullable()->after('updated_at');
              $table->integer('updated_by')->nullable()->after('created_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_bp_pulse', function (Blueprint $table) {
            $table->dropColumn('created_by');
            $table->dropColumn('updated_by');
        });

        Schema::table('user_pulse', function (Blueprint $table) {
            $table->dropColumn('created_by');
            $table->dropColumn('updated_by');
        });
    }
}
