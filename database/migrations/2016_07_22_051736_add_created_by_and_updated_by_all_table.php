<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreatedByAndUpdatedByAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_calories_goals', function (Blueprint $table) {
              $table->integer('created_by')->nullable()->after('updated_at');
              $table->integer('updated_by')->nullable()->after('created_by');
        });
        Schema::table('user_body_measurement_goals', function (Blueprint $table) {
              $table->integer('created_by')->nullable()->after('updated_at');
              $table->integer('updated_by')->nullable()->after('created_by');
        });
        
        Schema::table('user_weight_goals', function (Blueprint $table) {
              $table->integer('created_by')->nullable()->after('updated_at');
              $table->integer('updated_by')->nullable()->after('created_by');
        });
        Schema::table('user_bp_pulse_goals', function (Blueprint $table) {
              $table->integer('created_by')->nullable()->after('updated_at');
              $table->integer('updated_by')->nullable()->after('created_by');
        });
        
        Schema::table('user_sleep_goals', function (Blueprint $table) {
              $table->integer('created_by')->nullable()->after('updated_at');
              $table->integer('updated_by')->nullable()->after('created_by');
        });
        Schema::table('user_water_goals', function (Blueprint $table) {
              $table->integer('created_by')->nullable()->after('updated_at');
              $table->integer('updated_by')->nullable()->after('created_by');
        });
        
        Schema::table('nutrition_user', function (Blueprint $table) {
              $table->integer('created_by')->nullable()->after('updated_at');
              $table->integer('updated_by')->nullable()->after('created_by');
        });
        Schema::table('custom_nutrition_user', function (Blueprint $table) {
              $table->integer('created_by')->nullable()->after('updated_at');
              $table->integer('updated_by')->nullable()->after('created_by');
        });

        Schema::table('exercise_user', function (Blueprint $table) {
              $table->integer('created_by')->nullable()->after('updated_at');
              $table->integer('updated_by')->nullable()->after('created_by');
        });
       
        Schema::table('user_sleep', function (Blueprint $table) {
              $table->integer('created_by')->nullable()->after('updated_at');
              $table->integer('updated_by')->nullable()->after('created_by');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_calories_goals', function (Blueprint $table) {
             $table->dropColumn('created_by');
             $table->dropColumn('updated_by');
        });

        Schema::table('user_body_measurement_goals', function (Blueprint $table) {
             $table->dropColumn('created_by');
             $table->dropColumn('updated_by');
        });
        
        Schema::table('user_weight_goals', function (Blueprint $table) {
             $table->dropColumn('created_by');
             $table->dropColumn('updated_by');
        });

        Schema::table('user_bp_pulse_goals', function (Blueprint $table) {
             $table->dropColumn('created_by');
             $table->dropColumn('updated_by');
        });


        Schema::table('user_sleep_goals', function (Blueprint $table) {
             $table->dropColumn('created_by');
             $table->dropColumn('updated_by');
        });

        Schema::table('user_water_goals', function (Blueprint $table) {
             $table->dropColumn('created_by');
             $table->dropColumn('updated_by');
        });
        
        Schema::table('nutrition_user', function (Blueprint $table) {
             $table->dropColumn('created_by');
             $table->dropColumn('updated_by');
        });

        Schema::table('custom_nutrition_user', function (Blueprint $table) {
             $table->dropColumn('created_by');
             $table->dropColumn('updated_by');
        });
        
        Schema::table('exercise_user', function (Blueprint $table) {
             $table->dropColumn('created_by');
             $table->dropColumn('updated_by');
        });
        
        Schema::table('user_sleep', function (Blueprint $table) {
             $table->dropColumn('created_by');
             $table->dropColumn('updated_by');
        });
    }
}