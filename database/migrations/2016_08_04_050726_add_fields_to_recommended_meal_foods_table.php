<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToRecommendedMealFoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recommended_meal_foods', function (Blueprint $table) {
            $table->double('serving_quantity', 8, 2)->nullable()->after('calories');
            $table->string('serving_size_unit')->nullable()->after('serving_quantity');
            $table->double('no_of_servings', 8, 2)->nullable()->after('serving_size_unit');
            $table->string('serving_string')->nullable()->after('no_of_servings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recommended_meal_foods', function (Blueprint $table) {
            $table->dropColumn('serving_quantity');
            $table->dropColumn('serving_size_unit');
            $table->dropColumn('no_of_servings');
            $table->dropColumn('serving_string');
        });
    }
}
