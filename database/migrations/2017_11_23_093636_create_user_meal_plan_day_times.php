<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMealPlanDayTimes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_meal_plan_day_time', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_nutrition_exercise_plan_id')->unsigned()->index();
            $table->integer('meal_plan_id')->unsigned()->index();
            $table->string('dayName',10)->nullable();
            $table->integer('dayNumber')->nullable();
            $table->float('meal_time',4,2)->nullable();
            $table->boolean('notification')->nullable()->default(1);
            $table->string('guid',255)->nullable();
            $table->foreign('user_nutrition_exercise_plan_id')->references('id')->on('user_nutrition_exercise_plan')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_meal_plan_day_time');
    }
}
