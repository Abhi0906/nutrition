<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApnsDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apns_devices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->string('device_uuid',64);
            $table->string('device_token',255);
            $table->enum('platform_type', ['ios', 'android','other'])->default('other');
            $table->string('app_name',50)->nullable();
            $table->string('app_version',10)->nullable();
            $table->string('device_name',50)->nullable();
            $table->string('device_model',50)->nullable();
            $table->string('device_os_version',50)->nullable();
            $table->enum('push_badge', ['disable', 'enable'])->nullable();
            $table->enum('push_alert', ['disable', 'enable'])->nullable();
            $table->enum('push_sound', ['disable', 'enable'])->nullable();
            $table->enum('environment', ['development', 'production'])->default('production');
            $table->enum('status', ['active', 'uninstalled'])->default('active');
            $table->timestamps();
            $table->softDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apns_devices');
    }
}
