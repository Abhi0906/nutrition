<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicineUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicine_user', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->index();
            $table->integer('medicine_id')->unsigned()->index();
            $table->string('med_frequency',255)->nullable();
            $table->text('time')->nullable();
            $table->text('config')->nullable();
            $table->date('start')->nullable();
            $table->date('end')->nullable();
            $table->integer('days')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('medicine_id')->references('id')->on('medicines');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('medicine_user');
    }
}
