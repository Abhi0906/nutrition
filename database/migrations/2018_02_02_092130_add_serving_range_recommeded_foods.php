<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddServingRangeRecommededFoods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recommended_foods', function (Blueprint $table) {
            $table->double('serving_range', 8, 2)->nullable()->after('no_of_servings')->default(1);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recommended_foods', function (Blueprint $table) {
            $table->dropColumn('serving_range');

        });
    }
}
