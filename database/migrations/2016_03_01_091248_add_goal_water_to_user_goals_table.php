<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGoalWaterToUserGoalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_goals', function (Blueprint $table) {
            $table->integer('goal_water')->nullable()->after('goal_weight');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_goals', function (Blueprint $table) {
            $table->dropColumn('goal_water');
        });
    }
}
