<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('organizations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->nullable();
            $table->string('contact_first_name', 100)->nullable();
            $table->string('contact_last_name', 100)->nullable();
            $table->string('email')->nullable();
            $table->string('mobile',50)->nullable();
            $table->string('telephone',50)->nullable();
            $table->string('postcode',50)->nullable();
            $table->string('logo')->nullable();
            $table->string('theme_color',20)->nullable();
            $table->string('domain')->nullable(); 
            $table->string('sub_domain')->nullable();        
            $table->text('address')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('organizations');
    }
}
