<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateExerciseImageWorkout extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('exercise_image_workout', function (Blueprint $table) {
            $table->integer('workout_id')->unsigned()->index();
            $table->integer('exercise_image_id')->unsigned()->index();
            $table->foreign('workout_id')->references('id')->on('workouts');
            $table->foreign('exercise_image_id')->references('id')->on('exercise_images');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('exercise_image_workout');
    }
}
