<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersNutritionUpc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      
         Schema::create('user_nutrition_upc', function(Blueprint $table) {
          
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('nutrition_id')->unsigned()->index();
            $table->string('upc')->nullable();
            $table->string('guid')->nullable();
            $table->string('timestamp')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_nutrition_upc');
    }
}
