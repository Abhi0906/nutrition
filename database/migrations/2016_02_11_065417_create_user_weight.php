<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserWeight extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_weight', function(Blueprint $table) {
          
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();            
            $table->float('current_weight')->nullable();            
            $table->float('body_fat')->nullable();
            $table->date('log_date')->nullable();
            $table->string('guid')->nullable();
            
            $table->foreign('user_id')->references('id')->on('users');            

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_weight');
    }
}
