<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewNutritionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_nutritions', function(Blueprint $table) {
          
            $table->bigIncrements('id');
            $table->string('row_id')->nullable();
            $table->string('name')->index()->nullable(); 
            $table->string('brand_name')->index()->nullable();   
            $table->string('brand_with_name',500)->index()->nullable();        
            $table->text('nutrition_data')->nullable();
            $table->text('serving_data')->nullable();
            $table->string('source_name',100)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('new_nutritions');
    }
}
