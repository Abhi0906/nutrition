<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDatatypeWentBedTimeAtUserSleepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_sleep', function (Blueprint $table) {
        
            $table->string('rise_bed_time_new')->nullable()->after('minutes_fall_alseep');
            $table->string('went_bed_time_new')->nullable()->after('rise_bed_time_new');

        });

        Schema::table('user_sleep', function(Blueprint $table)
        {
            $table->dropColumn('rise_bed_time');
            $table->dropColumn('went_bed_time');
        });

        Schema::table('user_sleep', function(Blueprint $table)
        {
            $table->renameColumn('rise_bed_time_new', 'rise_bed_time');
            $table->renameColumn('went_bed_time_new', 'went_bed_time');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_sleep', function (Blueprint $table) {
             
             $table->renameColumn('rise_bed_time', 'rise_bed_time_old');
             $table->renameColumn('went_bed_time', 'went_bed_time_old');
        });

        Schema::table('user_sleep', function (Blueprint $table) {
             
               $table->integer('rise_bed_time')->nullable()->after('rise_bed_time_old');
               $table->integer('went_bed_time')->nullable()->after('went_bed_time_old');
        });

        Schema::table('user_sleep', function (Blueprint $table) {
             
             $table->dropColumn('rise_bed_time_old');
             $table->dropColumn('went_bed_time_old');
        });

       
    }
}
