<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNutritionUserServingString extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nutrition_user', function (Blueprint $table) {

            $table->string('serving_string')->nullable()->after('no_of_servings');
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nutrition_user', function (Blueprint $table) {

              $table->dropColumn('serving_string');
          
        });
    }
}
