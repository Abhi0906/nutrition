<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkoutExercise extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('workout_exercise', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('workout_id')->unsigned()->index();
            $table->integer('exercise_image_id')->nullable();
            $table->integer('video_id')->nullable();
            $table->integer('exercise_id')->nullable();
            $table->integer('sort_order')->nullable();
            $table->foreign('workout_id')->references('id')->on('workouts');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('workout_exercise');
    }
}
