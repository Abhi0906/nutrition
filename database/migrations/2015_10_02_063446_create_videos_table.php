<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uri',255)->nullable();
            $table->integer('external_video_id')->nullable();
            $table->string('name',255)->nullable();
            $table->text('description')->nullable();
            $table->string('link',255)->nullable();
            $table->integer('duration')->nullable();
            $table->integer('width')->nullable();
            $table->string('language',100)->nullable();
            $table->integer('height')->nullable();
            $table->string('status',60)->nullable();
            $table->longtext('embed')->nullable();
            $table->longtext('pictures')->nullable();
            $table->integer('workout_time')->nullable();
            $table->integer('minimun_calorie_burn')->nullable();
            $table->integer('maximum_calorie_burn')->nullable();
            $table->string('experience', 50)->nullable();
            $table->string('body_parts',100)->nullable();
            $table->string('training_types',255)->nullable();
            $table->string('equipments',255)->nullable();
            $table->boolean('assigned_properties')->default("0");
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('videos');
    }
}
