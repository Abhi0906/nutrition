<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNutritionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('nutritions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nutrition_brand_id')->unsigned()->nullable()->index();
            $table->string('name',2048)->index()->nullable();
            $table->string('item_id', 255);
            $table->float('calories')->nullable();
            $table->string('brand_id',255)->nullable();
            $table->float('serving_quantity')->nullable();
            $table->string('serving_size_unit',255)->nullable();
            $table->float('serving_size_weight_grams')->nullable();
            $table->float('servings')->nullable();
            $table->float('calcium_dv')->nullable();
            $table->float('calories_from_fat')->nullable();
            $table->float('cholesterol')->nullable();
            $table->text('data_source')->nullable();
            $table->float('dietary_fiber')->nullable();
            $table->float('iron_dv')->nullable();
            $table->string('metric_unit',100)->nullable();
            $table->float('potassium')->nullable();
            $table->float('protein')->nullable();
            $table->float('saturated_fat')->nullable();
            $table->float('sodium')->nullable();
            $table->float('sugars')->nullable();
            $table->float('total_carb')->nullable();
            $table->float('total_fat')->nullable();
            $table->float('trans_fat')->nullable();
            $table->string('upc',100)->nullable();
            $table->float('vitamin_a')->nullable();
            $table->float('vitamin_c')->nullable();
            $table->string('image_name',255)->nullable();
            $table->text('ingredient_statement')->nullable();
            $table->timestamps();
            $table->softDeletes();
            // $table->unique(array('item_id','brand_id'));
            $table->foreign('nutrition_brand_id')->references('id')->on('nutrition_brands');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nutritions');
    }
}
