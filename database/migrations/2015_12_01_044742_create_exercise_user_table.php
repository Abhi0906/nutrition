<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExerciseUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercise_user', function(Blueprint $table) {
          
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('exercise_id')->unsigned()->index();
            $table->float('weight')->nullable();
            $table->string('time',50)->nullable();
            $table->float('calories_burned')->nullable();
            $table->string('exercise_type',100)->nullable();
            $table->text('config')->nullable();
            $table->date('exercise_date')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('exercise_id')->references('id')->on('exercises');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('exercise_user');
    }
}
