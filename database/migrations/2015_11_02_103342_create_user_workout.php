<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserWorkout extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_workout', function(Blueprint $table) {
          
            $table->integer('user_id')->unsigned()->index();
            $table->integer('workout_id')->unsigned()->index();
            $table->text('config')->nullable();
            $table->date('start')->nullable();
            $table->date('end')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('workout_id')->references('id')->on('workouts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('user_workout');
    }
}
