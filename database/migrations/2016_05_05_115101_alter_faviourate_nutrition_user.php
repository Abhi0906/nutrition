<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFaviourateNutritionUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('favourite_nutrition_users', function (Blueprint $table) {

             $table->string('timestamp', 100)->nullable()->after('guid');
             $table->boolean('is_custom')->default("0")->after('timestamp');
         });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('favourite_nutrition_users', function($table) {

             $table->dropColumn('timestamp');
             $table->dropColumn('is_custom');
        });
    }
}
