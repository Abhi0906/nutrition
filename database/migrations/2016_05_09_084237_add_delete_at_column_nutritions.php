<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeleteAtColumnNutritions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('custom_nutrition_user', function (Blueprint $table) {

             $table->softDeletes();
        });

       Schema::table('nutrition_user', function (Blueprint $table) {

           $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('custom_nutrition_user', function($table)
        {
              $table->dropColumn('deleted_at');
        });

        Schema::table('nutrition_user', function($table)
        {
              $table->dropColumn('deleted_at');
        });
    }
}
