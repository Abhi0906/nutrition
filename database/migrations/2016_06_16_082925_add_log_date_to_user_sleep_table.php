<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLogDateToUserSleepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_sleep', function (Blueprint $table) {
        $table->date('log_date')->nullable()->after('log_date')->after('source');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_sleep', function (Blueprint $table) {
               $table->dropColumn('log_date');
        });
    }
}
