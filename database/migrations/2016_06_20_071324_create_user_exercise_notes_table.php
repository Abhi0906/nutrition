<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserExerciseNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('user_exercise_notes', function(Blueprint $table) {
          
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->date('log_date')->nullable();
            $table->string('guid')->nullable();
            $table->string('timestamp')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('user_exercise_notes');
    }
}
