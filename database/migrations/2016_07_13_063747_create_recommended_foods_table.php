<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecommendedFoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recommended_foods', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('food_recommendation_type_id')->unsigned()->index();
            $table->integer('nutrition_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('food_recommendation_type_id')->references('id')->on('food_recommendation_types');
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('recommended_foods');
    }
}
