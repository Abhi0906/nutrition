<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsFavouriteToNutritionUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nutrition_user', function (Blueprint $table) {
            $table->boolean('is_favourite')->default("0")->after('serving_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nutrition_user', function (Blueprint $table) {
            $table->dropColumn('is_favourite');
        });
    }
}
