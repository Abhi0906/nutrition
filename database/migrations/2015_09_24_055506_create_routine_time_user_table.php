<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutineTimeUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routine_time_user', function(Blueprint $table) {
            $table->integer('user_id')->unsigned()->index();
            $table->integer('routine_time_id')->unsigned()->index();
            $table->time('time')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('routine_time_id')->references('id')->on('routine_times');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('routine_time_user');
    }
}
