<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBloodSugarParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blood_sugar_parameters', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('lb_high')->nullable();
            $table->integer('lb_med')->nullable();
            $table->integer('ub_fasting_med')->nullable();
            $table->decimal('ub_pp_hours', 5, 2)->nullable();
            $table->integer('ub_pp_med')->nullable();
            $table->integer('ub_random_chk_med')->nullable();
            $table->integer('is_single_reading')->nullable();
            $table->integer('ub_random_chk_high')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blood_sugar_parameters');
    }
}
