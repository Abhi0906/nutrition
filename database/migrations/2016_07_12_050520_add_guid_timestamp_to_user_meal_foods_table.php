<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGuidTimestampToUserMealFoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_meal_foods', function (Blueprint $table) {
            $table->string('guid')->nullable()->after('is_custom');
            $table->string('timestamp')->nullable()->after('guid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_meal_foods', function (Blueprint $table) {
            $table->dropColumn('guid');
            $table->dropColumn('timestamp');
        });
    }
}
