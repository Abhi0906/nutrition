<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMealDayPlanConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_meal_day_plan_config', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_meal_day_plan_id')->unsigned()->index();
            $table->integer('complete_plan_id')->unsigned()->index();
            $table->string('dayName',10)->nullable();
            $table->integer('dayNumber')->nullable();
            $table->string('guid',255)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_meal_day_plan_config');
    }
}
