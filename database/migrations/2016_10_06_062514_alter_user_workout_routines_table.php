<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserWorkoutRoutinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_workout_routines', function (Blueprint $table) {
            $table->float('total_calories')->nullable()->after('log_date');
            $table->float('total_miles')->nullable()->after('total_calories');
            $table->float('total_weight_lifted')->nullable()->after('total_miles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_workout_routines', function (Blueprint $table) {
           $table->dropColumn('total_calories');
           $table->dropColumn('total_miles');
           $table->dropColumn('total_weight_lifted');
        });
    }
}
