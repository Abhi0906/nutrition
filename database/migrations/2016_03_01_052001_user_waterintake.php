<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserWaterintake extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_waterintake', function(Blueprint $table) {
          
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();    
            $table->float('log_water')->nullable();        
            $table->date('log_date');      
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_waterintake');
    }
}
