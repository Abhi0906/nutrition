<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropClassTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("SET FOREIGN_KEY_CHECKS=0;");
        Schema::drop('classes');
        Schema::drop('class_days');
        \DB::statement("SET FOREIGN_KEY_CHECKS=1;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('classes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('location_id')->unsigned()->nullable()->index();
            $table->string('class_name', 255)->nullable();
            $table->string('instructor', 255)->nullable();
            $table->string('activity_type')->nullable();
            $table->integer('class_max_size')->nullable();
            $table->enum('frequency_type', ['Daily', 'Weekly', 'Monthly'])->default('Daily');
            $table->integer('frequency_interval')->nullable();
            $table->date('class_start_date')->nullable();
            $table->integer('end_after_occurences')->nullable();
            $table->date('class_end_date')->nullable();
            $table->timestamps();

            $table->foreign('location_id')->references('id')->on('class_locations');
        });

        Schema::create('class_days', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('class_id')->unsigned()->nullable()->index();
            $table->integer('day_index')->nullable();
            $table->date('class_start_time')->nullable();
            $table->date('class_end_time')->nullable();
            $table->timestamps();

            $table->foreign('class_id')->references('id')->on('classes');
        });
    }
}
