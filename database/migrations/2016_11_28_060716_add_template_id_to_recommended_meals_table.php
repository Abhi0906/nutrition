<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTemplateIdToRecommendedMealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recommended_meals', function (Blueprint $table) {
            $table->dropColumn('meal_for');
            $table->integer('meal_template_id')->nullable()->after('meal_name');
            $table->string('meal_time', 8)->nullable()->after('meal_template_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recommended_meals', function (Blueprint $table) {
            $table->enum('meal_for', ['Male', 'Female', 'Any'])->default('Any')->after('meal_name');
            $table->dropColumn('meal_template_id');
            $table->dropColumn('meal_time');
        });
    }
}
