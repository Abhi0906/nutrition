<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropWorkoutVideoExerciseImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("SET FOREIGN_KEY_CHECKS=0;");
         if (Schema::hasTable('video_workout')){
            Schema::drop('video_workout');
          }
           if (Schema::hasTable('exercise_image_workout')){
             Schema::drop('exercise_image_workout');
           }
        \DB::statement("SET FOREIGN_KEY_CHECKS=1;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
