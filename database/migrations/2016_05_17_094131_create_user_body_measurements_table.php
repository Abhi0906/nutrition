<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBodyMeasurementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_body_measurements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->float('neck')->nullable();
            $table->float('arms')->nullable();
            $table->float('waist')->nullable();
            $table->float('hips')->nullable();
            $table->float('legs')->nullable();
            $table->date('log_date')->nullable();
            $table->string('guid')->nullable();
            $table->string('timestamp')->nullable();
            $table->string('source')->nullable();

            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_body_measurements');
    }
}
