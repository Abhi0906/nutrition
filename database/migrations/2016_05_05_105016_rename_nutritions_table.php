<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameNutritionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::rename('nutritions','old_nutritions');
       Schema::rename('new_nutritions','nutritions');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('nutritions','new_nutritions');
        Schema::rename('old_nutritions','nutritions');
      
    }
}
