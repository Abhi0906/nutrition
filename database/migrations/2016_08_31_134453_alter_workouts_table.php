<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterWorkoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('workouts', function (Blueprint $table) {
            $table->string('guid')->nullable()->after('body_part');
            $table->string('timestamp')->nullable()->after('guid');
            $table->enum('is_admin', [0,1])->default(0)->after('timestamp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workouts', function (Blueprint $table) {
            $table->dropColumn('is_admin');
            $table->dropColumn('guid');
            $table->dropColumn('timestamp');
        });
    }
}
