<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('class_location_id')->unsigned()->nullable()->index();
            $table->string('class_name', 255)->nullable();
            $table->string('instructor', 255)->nullable();
            $table->string('activity_type')->nullable();
            $table->integer('class_max_size')->nullable();
            $table->date('class_start_date')->nullable();
            $table->date('class_end_date')->nullable();
            $table->timestamps();

            $table->foreign('class_location_id')->references('id')->on('class_locations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('class_schedules');
    }
}
