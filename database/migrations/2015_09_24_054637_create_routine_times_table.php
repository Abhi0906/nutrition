<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutineTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routine_times', function(Blueprint $table) {
            $table->increments('id');
            $table->string('event_type',255)->nullable();
            $table->string('event_name',255)->nullable();
            $table->time('time')->nullable();
            $table->tinyInteger('is_primary')->nullable();
            $table->integer('sort_order')->nullable();
            $table->timestamps();
            $table->softDeletes();           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('routine_times');
    }
}
