<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('activity_id')->unsigned()->index();
            $table->date('program_start')->nullable();
            $table->integer('program_duration_days')->nullable();
            $table->integer('height_in_feet')->nullable();
            $table->integer('height_in_inch')->nullable();
            $table->decimal('baseline_weight', 18, 1)->nullable();
            $table->string('goal', 255)->nullable();
            $table->decimal('weight_measurement_goal', 18, 1)->nullable();
            $table->integer('calories')->nullable();
            $table->float('water_intake')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('activity_id')->references('id')->on('activities');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('user_profiles');
    }
}
