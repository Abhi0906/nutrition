<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBloodPressureParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blood_pressure_parameters', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('systolic_high_ul')->nullable();
            $table->integer('systolic_high_ll')->nullable();
            $table->integer('systolic_med_ul')->nullable();
            $table->integer('systolic_med_ll')->nullable();
            $table->integer('diastolic_high_ul')->nullable();
            $table->integer('diastolic_med_ul')->nullable();
            $table->integer('diastolic_med_ll')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blood_pressure_parameters');
    }
}
