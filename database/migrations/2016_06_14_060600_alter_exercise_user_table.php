<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterExerciseUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exercise_user', function (Blueprint $table) {

            $table->integer('sets')->nullable();
            $table->integer('reps')->nullable();
            $table->float('weight_lbs')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->string('guid')->nullable();
            $table->string('timestamp')->nullable();
            $table->softDeletes();
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('exercise_user', function (Blueprint $table) {

            $table->dropColumn('sets');
            $table->dropColumn('reps');
            $table->dropColumn('weight_lbs');
            $table->dropColumn('updated_at');
            $table->dropColumn('deleted_at');
            $table->dropColumn('guid');
            $table->dropColumn('timestamp');
          
        });
    }
}
