<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoWorkoutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('video_workout', function (Blueprint $table) {
            $table->integer('workout_id')->unsigned()->index();
            $table->integer('video_id')->unsigned()->index();
            $table->foreign('workout_id')->references('id')->on('workouts');
            $table->foreign('video_id')->references('id')->on('videos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('video_workout');
    }
}
