<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterExerciseTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('exercises', function (Blueprint $table) {

             $table->string('type',100)->nullable()->after('trackable');
             $table->string('main_muscle_worked',255)->nullable()->after('type');
             $table->string('other_muscles',255)->nullable()->after('main_muscle_worked');
             $table->string('equipment',255)->nullable()->after('other_muscles');
             $table->string('mechanics_type',100)->nullable()->after('equipment');
             $table->string('level',100)->nullable()->after('mechanics_type');
             $table->string('sport',50)->nullable()->after('level');
             $table->string('force',50)->nullable()->after('sport');
             $table->text('steps')->nullable()->after('force');
             $table->string('source_name',100)->nullable()->after('steps');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('exercises', function (Blueprint $table) {

            $table->dropColumn('type');
            $table->dropColumn('main_muscle_worked');
            $table->dropColumn('other_muscles');
            $table->dropColumn('equipment');
            $table->dropColumn('mechanics_type');
            $table->dropColumn('level');
            $table->dropColumn('sport');
            $table->dropColumn('force');
            $table->dropColumn('steps');
            $table->dropColumn('source_name');
             
        });
    }
}
