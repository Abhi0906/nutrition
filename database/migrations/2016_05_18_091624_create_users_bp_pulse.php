<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersBpPulse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_bp_pulse', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->float('systolic')->nullable();
            $table->float('diastolic')->nullable();
            $table->float('pulse')->nullable();
            $table->date('log_date')->nullable();
            $table->string('guid')->nullable();
            $table->string('timestamp')->nullable();
            $table->string('source')->nullable();

            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_bp_pulse');
    }
}
