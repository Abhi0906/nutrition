<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBodyMeasurementGoalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_body_measurement_goals', function (Blueprint $table) {

             $table->date('start_date')->nullable()->after('goal_date');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('user_body_measurement_goals', function (Blueprint $table) {
          
           $table->dropColumn('start_date');
           
        });
    }
}
