<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalBedUserSleepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_sleep', function (Blueprint $table) {
            $table->float('total_bed')->nullable()->after('total_sleep');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_sleep', function (Blueprint $table) {
             $table->dropColumn('total_bed');
        });
    }
}
