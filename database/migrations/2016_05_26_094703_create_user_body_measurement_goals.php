<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBodyMeasurementGoals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_body_measurement_goals', function(Blueprint $table) {
          
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->enum('body_measurement_type', ['neck', 'arms','waist','hips','legs']);
            $table->float('goal')->nullable();
            $table->float('start_measurement')->nullable();
            $table->float('current_measurement')->nullable();
            $table->date('goal_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('user_body_measurement_goals');
    }
}
