<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserChallengesData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_challenges_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('challenge_id')->unsigned()->index();
            $table->date('log_date')->nullable();
            $table->string('device')->nullable();
            $table->string('data')->nullable();
            $table->string('guid')->nullable();
            $table->string('timestamp')->nullable();
            $table->text('config')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('user_challenges_data');
    }
}
