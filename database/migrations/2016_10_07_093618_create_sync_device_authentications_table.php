<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSyncDeviceAuthenticationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sync_device_authentications', function (Blueprint $table) {
            $table->increments('id');        
            $table->integer('user_id')->unsigned()->index();  
            $table->integer('device_user_id')->nullable();    
            $table->string('device_type', 50)->nullable();
            $table->text('auth_config')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sync_device_authentications');
    }
}
