<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExerciseImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercise_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255)->nullable();
            $table->integer('exercise_time')->nullable();
            $table->integer('calorie_burn')->nullable();
            $table->string('experience', 50)->nullable();
            $table->string('body_part',100)->nullable();
            $table->string('equipment',255)->nullable();
            $table->string('image_name',255)->nullable();
            $table->text('description')->nullable();
            $table->longtext('steps')->nullable();
            $table->timestamps();
            $table->softDeletes(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('exercise_images');
    }
}
