<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicines', function($table) {
            $table->increments('id');
            $table->integer('rx_norm')->nullable();
            $table->string('name', 100)->nullable();
            $table->string('generic_name', 100)->nullable();
            $table->string('dose', 255)->nullable();
            $table->string('uom',100)->nullable();
            $table->string('dose_strength',100)->nullable();
            $table->string('route',100)->nullable();
            $table->string('dose_form',100)->nullable();
            $table->string('frequency',100)->nullable();
            $table->integer('frequency_times')->nullable();
            $table->string('span',100)->nullable();
            $table->boolean('beers_criteria')->nullable();
            $table->boolean('boxed_warning')->nullable();
            $table->string('ahfs_class',100)->nullable();
            $table->string('med_condition', 200)->nullable();
            $table->string('reason', 200)->nullable();
            $table->string('med_comment', 800)->nullable();
            $table->boolean('most_consumed');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('medicines');
    }
}
