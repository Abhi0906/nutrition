<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAndAddColumnNutritionUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nutrition_user', function (Blueprint $table) {

            $table->dropColumn('serving_size_weight_grams');
            $table->dropColumn('servings');
            $table->dropColumn('calcium_dv');
            $table->dropColumn('calories_from_fat');
            $table->dropColumn('cholesterol');
            $table->dropColumn('dietary_fiber');
            $table->dropColumn('iron_dv');
            $table->dropColumn('metric_unit');
            $table->dropColumn('potassium');
            $table->dropColumn('protein');
            $table->dropColumn('saturated_fat');
            $table->dropColumn('sodium');
            $table->dropColumn('sugars');
            $table->dropColumn('total_carb');
            $table->dropColumn('total_fat');
            $table->dropColumn('trans_fat');
            $table->dropColumn('upc');
            $table->dropColumn('vitamin_a');
            $table->dropColumn('vitamin_c');
            $table->dropColumn('is_favourite');
            $table->string('guid')->nullable()->after('schedule_time');
            $table->string('timestamp', 100)->nullable()->after('guid');
            $table->boolean('is_custom')->default("0")->after('timestamp');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nutrition_user', function($table)
        {
            $table->float('serving_size_weight_grams')->nullable();
            $table->float('servings')->nullable();
            $table->float('calcium_dv')->nullable();
            $table->float('calories_from_fat')->nullable();
            $table->float('cholesterol')->nullable();
            $table->float('dietary_fiber')->nullable();
            $table->float('iron_dv')->nullable();
            $table->string('metric_unit',100)->nullable();
            $table->float('potassium')->nullable();
            $table->float('protein')->nullable();
            $table->float('saturated_fat')->nullable();
            $table->float('sodium')->nullable();
            $table->float('sugars')->nullable();
            $table->float('total_carb')->nullable();
            $table->float('total_fat')->nullable();
            $table->float('trans_fat')->nullable();
            $table->string('upc',100)->nullable();
            $table->float('vitamin_a')->nullable();
            $table->float('vitamin_c')->nullable();
            $table->boolean('is_favourite')->default("0");

            $table->dropColumn('guid');
            $table->dropColumn('timestamp');
            $table->dropColumn('is_custom');
        });
    }
}
