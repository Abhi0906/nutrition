<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplateWorkoutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('template_workout', function (Blueprint $table) {

            $table->integer('template_id')->unsigned()->index();
            $table->integer('workout_id')->unsigned()->index();
            $table->foreign('template_id')->references('id')->on('templates');
            $table->foreign('workout_id')->references('id')->on('workouts');
            $table->text('config')->nullable();
            $table->integer('start')->nullable();
            $table->integer('end')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('template_workout');
    }
}
