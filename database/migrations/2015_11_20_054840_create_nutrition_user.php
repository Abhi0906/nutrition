<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNutritionUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nutrition_user', function(Blueprint $table) {
          
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('nutrition_id')->unsigned()->index();
            $table->float('calories')->nullable();
            $table->float('serving_quantity')->nullable();
            $table->string('serving_size_unit',255)->nullable();
            $table->float('serving_size_weight_grams')->nullable();
            $table->float('servings')->nullable();
            $table->float('calcium_dv')->nullable();
            $table->float('calories_from_fat')->nullable();
            $table->float('cholesterol')->nullable();
            $table->float('dietary_fiber')->nullable();
            $table->float('iron_dv')->nullable();
            $table->string('metric_unit',100)->nullable();
            $table->float('potassium')->nullable();
            $table->float('protein')->nullable();
            $table->float('saturated_fat')->nullable();
            $table->float('sodium')->nullable();
            $table->float('sugars')->nullable();
            $table->float('total_carb')->nullable();
            $table->float('total_fat')->nullable();
            $table->float('trans_fat')->nullable();
            $table->string('upc',100)->nullable();
            $table->float('vitamin_a')->nullable();
            $table->float('vitamin_c')->nullable();
            $table->string('schedule_time',255)->nullable();
            $table->date('serving_date')->nullable();
            $table->text('config')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nutrition_user');
    }
}
