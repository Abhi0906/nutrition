<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApnsMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apns_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('apns_device_id')->unsigned()->index();
            $table->string('message',500)->nullable();
            $table->enum('type', ['deal', 'business','general'])->nullable();
            $table->text('json_data')->nullable();
            $table->timestamp('delivered_at');
            $table->enum('status', ['queued', 'inprogress','delivered','failed'])->default('queued');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('meal_day_plan');
    }
}
