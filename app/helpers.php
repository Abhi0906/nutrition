<?php


/**
 * @param $message
 * @param $title
 */
function flash($title = null, $message = null){
    $flash = app('App\Http\Flash');

    if(func_num_args() == 0){
        return $flash;
    }

    return $flash->info($title, $message);
}

function calculateMetricCalories($gender, $weight, $height, $age, $activity){
	$bmr = 0;
  if($gender != ""){
    if($gender == 'male'){
        $bmr = 10 * $weight +6.25 * $height - 5 * $age + 5;
      }else if($gender == 'female'){
        $bmr = 10 * $weight + 6.25 * $height - 5 * $age - 161;
      }
   }

	if(!!$activity){
		$bmr = $bmr * $activity;
	}
	return (int)$bmr;
}

function calculateUsCalories($data){
   $age=0;
   $activity = 0;
	 $gender = isset($data['gender'])?$data['gender']:'';
	 $weight = isset($data['baseline_weight'])?$data['baseline_weight']:0;
	 $height_feet =isset($data['height_in_feet'])?$data['height_in_feet']:0;
	 $height_inches =isset($data['height_in_inch'])?$data['height_in_inch']:0;

   if(isset($data['birth_date'])){
    $birth_date =  \Carbon\Carbon::createFromFormat('Y-m-d',$data['birth_date']);
    $age= $birth_date->age;
   }
	  if(isset($data['activity_value'])){
	    $activity = $data['activity_value'];
    }
	 $weight_kg = (float)($weight / 2.20462);
	 $height_cm =($height_feet * 12 + $height_inches) * 2.54;
	 return calculateMetricCalories(strtolower($gender), $weight_kg, $height_cm, $age, $activity);
}

function sendEmail($template_name,$data){

    $email_template_name = 'emails.'.$template_name;
    $email_data = array();
    $email_data['to_email'] =$data['toemail'];
    $email_data['subject'] =isset($data['subject'])?$data['subject']:'Genemedicshealth & Wellness';
    //$email_data['from_email'] =isset($data['fromEmail'])?$data['fromEmail']:'';
    /*$email_data['name'] = isset($data['name'])?$data['name']:'';
    $email_data['username'] = isset($data['username'])?$data['username']:'';
    $email_data['password'] = isset($data['password'])?$data['password']:'';
    $email_data['token'] = isset($data['token'])?$data['token']:'';
    $email_data['confirmation_code'] = isset($data['confirmation_code'])?$data['confirmation_code']:'';
   */
    \Mail::send($email_template_name, $data, function($message) use ($email_data)
    {
        $message->to($email_data['to_email'])->subject($email_data['subject']);
    });
}

function sendNotification($data){

  $user_id = $data['user_id'];
  $notification_text = $data['notification_text'];

  $notification = \App\Models\Notification:: where('user_id',$user_id)
  											 ->where('notification_text',$notification_text)
  											 ->first();

   if($notification != null){

   	 $notification->update($data);

   }else{

   		\App\Models\Notification::create($data);
   }
}

function guid( $opt = true ){       //  Set to true/false as your default way to do this.

    if( function_exists('com_create_guid') ){
        if( $opt ){ return com_create_guid(); }
            else { return trim( com_create_guid(), '{}' ); }
        }
        else {
            mt_srand( (double)microtime() * 10000 );    // optional for php 4.2.0 and up.
            $charid = strtoupper( md5(uniqid(rand(), true)) );
            $hyphen = chr( 45 );    // "-"
            $left_curly = $opt ? chr(123) : "";     //  "{"
            $right_curly = $opt ? chr(125) : "";    //  "}"
            $uuid = $left_curly
                . substr( $charid, 0, 8 ) . $hyphen
                . substr( $charid, 8, 4 ) . $hyphen
                . substr( $charid, 12, 4 ) . $hyphen
                . substr( $charid, 16, 4 ) . $hyphen
                . substr( $charid, 20, 12 )
                . $right_curly;
            return $uuid;
            }
}

function append_array_values($array1, $array2){

    if(!empty($array2)){
      foreach ($array2 as $key => $value) {
          $array1->push($value);
      }
    }

    return $array1;
}

function auth0Signup($data){


   $client  = new GuzzleHttp\Client();
   $signup_url = 'https://'.config('auth0.domain').'/dbconnections/signup';
   $email = $data['email'];
   $password = $data['password'];
   $response = $client->request('POST',$signup_url, [
        'form_params' => [
            'client_id' => config('auth0.clientID'),
            'email' => $email,
            'password' => $password,
            'connection' => config('auth0.connection')

        ]
    ]);
      $code = $response->getStatusCode(); // 200
      $reason = $response->getReasonPhrase(); // OK
    return $code;

}

function calculatePercent($start_measurement,$current_measurement,$goal){
    // echo $start_measurement;
    // echo $current_measurement;
    // echo $goal;
    $final_goal =0;
    $destination_goal = abs($start_measurement-$goal);
   // echo $destination_goal;
    $progress_goal =  abs($start_measurement-$current_measurement);
   // echo $progress_goal;exit;
    if($destination_goal > 0){
       $final_goal = ($progress_goal/$destination_goal)*100;
    }

    return floor($final_goal);
}

function convertStringToArray($string){

  if(is_string($string)){
    return (array)  json_decode($string);
  }

  return $string;
}

function calculateBpPercent($reading,$goal){
  $final_percent = 0;
  if($goal>0){
    $actual_data = abs($reading-$goal);
    $ans = ($actual_data*100)/$goal;
    $final_percent = 100-$ans;
  }

  return $final_percent;

}

function decimalToHours($decimal){

  $hours =0;
  $min=0;
  $time =[];
  $hour = floor($decimal);
  $min = round(60*($decimal- $hour));
  $time['hours'] = $hour;
  $time['min'] = $min;
  return $time;
}

function displayTimeFormat($hour,$min,$caps){
   $string ="";
   if($caps == 'small'){
     $string = $hour." Hrs.";
     if($min > 0){
       $string = $string.$min." Mins";
     }
   }else if($caps == 'capital'){

    $string = $hour." HRS ";
     if($min > 0){
       $string = $string." ".$min." MIN";
     }
   }
   return $string;
}

function calculateEfficiency($target,$achived){

   $calculate = ($achived*100)/$target;
   return  number_format((float)$calculate, 2, '.', '');
}


function addDayswithdate($date,$days){

    $date = strtotime("+".$days." days", strtotime($date));
    return  date("Y-m-d", $date);

}


function GetDayTime($sStartDate, $sEndDate){


    $start_timestamp =strtotime($sStartDate);
    $end_timestamp = strtotime($sEndDate);
    // dd($start_timestamp);

     $aDays[] =date('Y-m-d H:i',$start_timestamp);
    while($start_timestamp < $end_timestamp){
      // Add a day to the current date
      $start_timestamp = $start_timestamp+60*60;

      // Add this new day to the aDays array
      $aDays[] = date('Y-m-d H:i',$start_timestamp);

    }

  return $aDays;
}

function addHourToTimeStamp($date,$num){

   $start_timestamp= strtotime($date);
   $new_timestamp = $start_timestamp+(60*60+$num);

   return date('Y-m-d',$new_timestamp);
}

function getCustomDates($sStartDate, $sEndDate,$range){
    $dates = [];
    if($range == 'month'){
      $durtion = "month";
    }else if($range == 'day'){
       $durtion = "day";
    }

    $begin = new DateTime( $sStartDate );
    $end = new DateTime( $sEndDate );
    $end = $end->modify( '+1 '.$durtion );

    $interval = DateInterval::createFromDateString('1 '.$durtion);

    $period = new DatePeriod($begin, $interval, $end);
    foreach($period as $dt) {
     $dates[] = $dt->format("Y-m-d");
    }
   return $dates;
}

function clearFoodDataValue($value){
  if($value && strpos($value, 'undefined')===false){
    $data = explode(' ', $value);
    $clear_value = str_replace(',', '', $data[0]);
    return $clear_value;
  }else{
    return '0';
  }

  //return $value;
}