<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Nutrition_brand extends Model {
    

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nutrition_brands';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','brand_id', 'is_custom'];

    

    protected $dates = ['deleted_at'];


     public function nutritions()
        {
            return $this->hasMany('App\Models\Nutrition');
        }

   
}
