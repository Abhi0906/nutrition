<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Invalid_nutrition extends Model
{
     use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'invalid_nutritions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['row_id','barnd_with_name','nutrition_data','serving_data'];

    

    protected $dates = ['deleted_at'];
}
