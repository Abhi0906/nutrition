<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User_pulse extends Model
{
   use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */

    protected $table = 'user_pulse';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['pulse', 'guid','timestamp','log_date','source','created_by','updated_by'];

    protected $dates = ['deleted_at'];

    public function bp_pulse()
    {
        return $this->hasMany('App\Models\Blood_pressure_pulse', 'user_pulse_id');
    }
    public function pulse()
    {
        return $this->hasMany('App\Models\User_pulse');
    }

    
}
