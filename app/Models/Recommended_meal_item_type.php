<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recommended_meal_item_type extends Model
{
    use SoftDeletes;

    protected $table = "recommended_meal_item_types";

    protected $fillable = ['item_name', 'guid', 'timestamp'];

    public function item_foods(){
    	return $this->hasMany('App\Models\Recommended_meal_food', 'item_id')
    				->with('nutritions');
    }

    /*public function meals(){
    	return $this->belongsToMany('App\Models\Recommended_meal', 'recommended_meal_foods','item_id', 'meal_id');
    }*/
}
