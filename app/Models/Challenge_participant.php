<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Challenge_participant extends Model
{
     use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'challenge_participants';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','challenge_id','join_date','total_calories_burned','total_steps','rank'];

    

    protected $dates = ['deleted_at'];


    public function user()
    {
        return $this->belongsTo('App\Models\User')->select('id','first_name','last_name','photo','doctor_title');
    }
}
