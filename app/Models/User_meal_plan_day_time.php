<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class User_meal_plan_day_time extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */

    protected $table = 'user_meal_plan_day_time';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_nutrition_exercise_plan_id', 'meal_plan_id', 'dayName', 'dayNumber', 'meal_time', 'notification','guid'];

    public function user_nutrition_exercise_plan(){

        return $this->belongsTo('App\Models\User_nutrition_exercise_plan', 'user_nutrition_exercise_plan_id','id')->with('master_template');
    }
}
