<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User_sleep extends Model
{
  use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_sleep';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['minutes_fall_alseep','times_awakened', 'times_awakened_minutes','went_to_bed','rise_from_bed','total_sleep', 'guid', 'timestamp','source','total_bed','log_date','created_by','updated_by'];


    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}
