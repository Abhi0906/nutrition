<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WaterIntake extends Model
{
    use SoftDeletes;


    protected $table = 'user_waterintake';

    protected $dates = ['deleted_at'];

    protected $fillable = ['user_id','log_water','log_date','guid','timestamp'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    
}
