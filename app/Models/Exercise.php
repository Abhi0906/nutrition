<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exercise extends Model {
    

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'exercises';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','weight','time','calories_burned','config'];

    

    protected $dates = ['deleted_at'];


    /**
     * Scope a query to search exercises.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSerachExercise($query,$ex_name = null,$exercise_type=null)
    {
       
        if($exercise_type == 'cardio'){

            return $query->where('exercises.name', 'LIKE', "%{$ex_name}%")
                     ->where('exercises.type', 'Cardio')   
                     ->where('trackable','0');
        }else{

             return $query->where('exercises.name', 'LIKE', "%{$ex_name}%")
                     ->where('exercises.type', '<>', 'Cardio')   
                     ->where('trackable','0');
        }
        
    }

   
}
