<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Nutrition_user extends Model {
       
     use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nutrition_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','nutrition_id','calories',
                           'serving_quantity','serving_size_unit',
                           'no_of_servings','serving_string','schedule_time','serving_date','guid','timestamp','is_custom','created_at','updated_at','created_by','updated_by'];

    public $timestamps = false;

    protected $appends = ['execute_date'];

    protected $dates = ['deleted_at'];

    public function getExecuteDateAttribute()
    {
      
        return  $this->attributes['serving_date'];
    }

    public function nutritions()
    {
        return $this->belongsTo('App\Models\Nutrition','nutrition_id');
    }

    public function custom_nutritions()
    {
        return $this->belongsTo('App\Models\Custom_nutrition_user','nutrition_id','id');
    }

  
}
