<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Food_Completed extends Model
{
   use SoftDeletes;


    protected $table = 'user_food_completed';
    
    protected $fillable = ['user_id','date'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
