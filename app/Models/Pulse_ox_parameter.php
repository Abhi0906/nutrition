<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Pulse_ox_parameter extends Model
{
    
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pulse_ox_parameters';

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'high_alert',
    					  'med_alert','high_alert_pulse',
    					  'med_alert_pulse',
    					  'created_by','updated_by'];

    // Relationship
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }	

}
