<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Challenge extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */

    protected $table = 'challenges';

    public function getTable() {
      if(\Session::has("organization_id")){
        $org_id = \Session::get("organization_id");
        return $org_id . '_challenges';
      }
      return 'challenges';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'title','description','start_date','end_date','allowed_devices','goal_type','goal_type_config','is_expired','image'];

    

    public function challenge_participants()
    {
        return $this->hasMany('App\Models\Challenge_participant')->orderBy('rank','asc')->with('user');
    }

    public function only_participants(){

        return $this->hasMany('App\Models\Challenge_participant'); 
    }   

    public function participantOfChallenge(){

        return $this->hasOne('App\Models\Challenge_participant'); 
    } 

}
