<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favourite_nutrition_user extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'favourite_nutrition_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','nutrition_id','guid','assigned_by','is_custom','timestamp'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function nutritions()
    {
        return $this->belongsTo('App\Models\Nutrition', 'nutrition_id');
    }

    public function custom_nutritions()
    {
        return $this->belongsTo('App\Models\Custom_nutrition_user', 'nutrition_id','id');
    }
}
