<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class User_meal_foods extends Model
{
    protected $table = "user_meal_foods";

    protected $fillable = [ 'meal_id', 'nutrition_id', 'calories', 'serving_quantity', 'serving_size_unit', 'no_of_servings', 'is_custom'];

    public function meals()
    {
        return $this->belongsTo('App\Models\User_meals');
    }

    public function nutritions()
    {
        return $this->belongsTo('App\Models\Nutrition', 'nutrition_id');
    }
}
