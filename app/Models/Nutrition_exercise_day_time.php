<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Nutrition_exercise_day_time extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nutrition_exercise_day_times';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nutrition_exercise_plan_id','meal_plan_id','dayName','dayNumber','meal_time','notification','guid'];

    public function nutrition_exercise_plan(){

        return $this->belongsTo('App\Models\Nutrition_exercise_plan', 'nutrition_exercise_plan_id','id')->with('master_template');
    }

     
}
