<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MyOwnResetPassword as ResetPasswordNotification;


class User extends Authenticatable
{
   // use Authenticatable, Authorizable, CanResetPassword;

    use SoftDeletes;
    use Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    // public function __construct() {
    //     parent::__construct();
    //     $this->table = 'users';
    //     if(\Session::has("organization_id")){
    //         $org_id = \Session::get("organization_id");
    //         $this->table = $org_id.'_users';
    //     }

    // }

    public function getTable() {
      if(\Session::has("organization_id")){
        $org_id = \Session::get("organization_id");
        return $org_id . '_users';
      }
      return 'users';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['auth0_user_id','username', 'email', 'password',
                            'user_type','first_name','middle_name',
                            'last_name','gender','address',
                            'state','city','country',
                            'timezone_id','timezone_name',
                            'post_code','phone','photo',
                            'birth_date','status','doctor_title',
                            'created_by','updated_by','isGuest',
                            'email_varify','confirmation_code', 'membership_code'
                            ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $appends = ['user_id', 'full_name','name_with_title'];

    protected $dates = ['deleted_at'];

    public function getFullNameAttribute()
    {

        return  ucfirst($this->attributes['first_name'])." ".ucfirst($this->attributes['last_name']);
    }

    public function getUserIdAttribute()
    {
        return "{$this->auth0_user_id}";
    }

    public function getNameWithTitleAttribute()
    {

        return  $this->attributes['doctor_title']." ".ucfirst($this->attributes['first_name'])." ".ucfirst($this->attributes['last_name']);
    }

    // Query Scope

     /**
     * Scope a query to only include patient users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePatient($query)
    {
        return $query->where('user_type', '=', 'patient');
    }

     /**
     * Scope a query to only include doctor users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDoctor($query)
    {
        return $query->where('user_type', '=', 'doctor');
    }

    // Relationship


    /**
     * Get the weight_vital_parameters record associated with the user.
     */
    public function weight_parameters()
    {
        return $this->hasOne('App\Models\Weight_parameter');
    }

    public function blood_pressure_parameters()
    {
        return $this->hasOne('App\Models\Blood_pressure_parameter');
    }

    public function blood_sugar_parameters()
    {
        return $this->hasOne('App\Models\Blood_sugar_parameter');
    }

    public function pulse_ox_parameters()
    {
        return $this->hasOne('App\Models\Pulse_ox_parameter');
    }

    public function profile()
    {
        return $this->hasOne('App\Models\User_profile');
    }

    public function routine_times()
    {
        return $this->belongsToMany('App\Models\Routine_time')->withPivot('time','routine_time_id');
    }

    public function medicines(){
        return $this->belongsToMany('App\Models\Medicine')->withPivot('med_frequency','time','config' ,'start','end','days');
    }

    public function templates()
    {
        return $this->belongsToMany('App\Models\Template')->with('workouts')
                       ->withPivot('template_id','user_id');
    }

    public function user_template(){
        return $this->belongsToMany('App\Models\Template')
                    ->withPivot('template_id','user_id', 'start_date', 'end_date', 'created_at', 'updated_at')
                    ->orderBy('updated_at', 'desc');
    }

    public function workouts()
    {
        return $this->belongsToMany('App\Models\Workout')->with('workout_exercise')->withPivot('user_id','workout_id','start','end','config', 'deleted_dates');
    }

    public function assign_patient(){
        return $this->belongsToMany('App\Models\User','assign_patients','doctor_id','patient_id')->withPivot('doctor_id','patient_id');
    }

    public function assign_doctor(){
        return $this->belongsToMany('App\Models\User','assign_patients','patient_id','doctor_id')->withPivot('doctor_id','patient_id');
    }

    public function nutritions()
    {
        return $this->belongsToMany('App\Models\Nutrition')->with('nutrition_brand')
                    ->select('nutritions.id','nutrition_brand_id','name','data_source','ingredient_statement')
                    ->withPivot('id as nutrition_user_id','user_id','nutrition_id','calories',
                                'serving_quantity','serving_size_unit','serving_size_weight_grams',
                                'servings','no_of_servings','calcium_dv','calories_from_fat',
                                'cholesterol','dietary_fiber','iron_dv',
                                'metric_unit','potassium','protein',
                                'saturated_fat','sodium','sugars',
                                'total_carb','total_fat','trans_fat',
                                'upc','vitamin_a','vitamin_c',
                                'schedule_time','serving_date','config','created_at')
                    ->orderBy('pivot_created_at','desc');
    }

    public function exercises(){

        return $this->belongsToMany('App\Models\Exercise')
                    ->select('exercises.id','name')
                    ->withPivot('id as exercise_user_id','user_id','exercise_id','weight',
                                'time','calories_burned','exercise_type',
                                'config','exercise_date','created_at')
                    ->orderBy('pivot_created_at','desc');
    }

    public function weight()
    {
        return $this->hasMany('App\Models\User_weight');
    }
    public function user_goal()
    {
        return $this->hasOne('App\Models\User_goal');
    }

    public function WaterIntake()
    {
        return $this->hasMany('App\Models\WaterIntake');
    }


    public function favourite_nutritions()
    {
        return $this->hasMany('App\Models\Favourite_nutrition_user')->with('nutritions');
    }

    // public function custom_nutritions()
    // {
    //     return $this->belongsToMany('App\Models\Nutrition', 'custom_nutrition_user')->with('nutrition_brand');
    // }

    public function User_custom_water()
    {
        return $this->hasMany('App\Models\User_custom_water');
    }

    public function body_measurements(){
        return $this->hasMany('App\Models\User_body_measurements');
    }


    public function blood_pressure_pulse(){
        return $this->hasMany('App\Models\Blood_pressure_pulse');
    }

     public function user_pulse(){
        return $this->hasMany('App\Models\User_pulse');
    }


    public function user_weight_goal()
    {
        return $this->hasOne('App\Models\User_weight_goal');
    }

    public function user_body_measurements_goal()
    {
        return $this->hasOne('App\Models\User_body_measurement_goal');
    }

    public function User_bp_pulse_goal()
    {
        return $this->hasOne('App\Models\User_bp_pulse_goal');
    }
    public function nutrition_upc()
    {
        return $this->hasOne('App\Models\Nutrition_upc');
    }

    public function User_calorie_goal()
    {
        return $this->hasOne('App\Models\User_calories_goal');
    }

    public function User_sleep()
    {
        return $this->hasOne('App\Models\User_sleep');
    }

    public function User_sleep_goal(){
        return $this->hasOne('App\Models\User_sleep_goal');
    }
    public function Food_Completed()
    {
        return $this->hasMany('App\Models\Food_Completed');
    }

    public function synch_device_auth()
    {
        return $this->hasMany('App\Models\Sync_device_authentication');
    }

    public function meal_template(){
        return $this->belongsToMany('App\Models\Meal_template');
    }

    public function meal_template_with_meals(){
        return $this->belongsToMany('App\Models\Meal_template')->with('meals');
    }

    public function device_token()
    {
        return $this->hasMany('App\Models\Apns_device')
                                ->groupBy('device_name')
                                ->orderBy('updated_at','desc');
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
}
