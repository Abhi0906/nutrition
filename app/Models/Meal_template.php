<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Meal_template extends Model
{
    use SoftDeletes;

    protected $table = "meal_templates";

    protected $fillable = ['template_name', 'template_gender', 'is_default'];

    public function user(){
        return $this->belongsToMany('App\Models\User');
    }

    public function meals(){
        return $this->hasMany('App\Models\Recommended_meal');
    }

     public function recommended_meals(){
        return $this->hasMany('App\Models\Recommended_meal')->with('meal_foods');
    }
}
