<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User_goal extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_goals';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['user_id','weekly_goal_id','goal_weight','goal_water','goal_body_fat','calorie_intake_g',
                            'calorie_burned_g','weight_goal_date','body_measurement_goal_date',
                            'neck_g','arms_g','waist_g','hips_g','legs_g','bp_pulse_goal_date',
                            'systolic_from','systolic_to','diastolic_from','diastolic_to',
                            'heart_rate_from','heart_rate_to','sleep_goal_date','sleep_g',
                            'guid','timestamp'                            
                            ];

    protected $dates = ['deleted_at'];

    // Relationship
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

   
}
