<?php
namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Refered_friend extends Model
{
    use SoftDeletes;

    protected $table = "refered_friends";

    protected $fillable = ['user_id','first_name', 'last_name', 'email','phone_number'];

     public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}
