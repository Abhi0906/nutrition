<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User_profile extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_profiles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['user_id','activity_id','height_in_feet',
                            'program_start','program_duration_days',
    					   'height_in_inch','baseline_weight','goal',
    					   'weight_measurement_goal','calories','water_intake',
                           'news_letter','created_by','updated_by'
    						];

    

    protected $dates = ['deleted_at'];

    // Relationship
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function activity()
    {
        return $this->belongsTo('App\Models\Activity');
    }    
}
