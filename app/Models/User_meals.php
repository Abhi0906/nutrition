<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User_meals extends Model
{
    protected $table = "user_meals";

    protected $dates = ['deleted_at'];

    protected $fillable = [ 'user_id', 'meal_name', 'guid', 'timestamp'];

    public function meal_foods()
    {
        return $this->hasMany('App\Models\User_meal_foods', 'meal_id')->with('nutritions');
    }
}
