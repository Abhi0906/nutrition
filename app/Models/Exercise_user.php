<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exercise_user extends Model {
       
 use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'exercise_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','exercise_id','weight',
                           'time','calories_burned','exercise_type','exercise_sub_type',
                           'exercise_date','created_at','updated_at','sets','reps','weight_lbs','guid','timestamp','created_by','updated_by','config'];

    public $timestamps = false;

    protected $appends = ['execute_date'];
    protected $dates = ['deleted_at'];

    public function getExecuteDateAttribute()
    {
      
        return  $this->attributes['exercise_date'];
    }

    public function exercise()
    {
        return $this->belongsTo('App\Models\Exercise','exercise_id');
    }
  
}
