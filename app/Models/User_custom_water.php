<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User_custom_water extends Model
{
    use SoftDeletes;


    protected $table = 'user_custom_water';


    protected $fillable = ['user_id','cup_size','guid'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
