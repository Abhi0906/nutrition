<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Nutrition_exercise_plan extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nutrition_exercise_plan';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['meal_plan_id','master_template_id','meal_id','workout_id','day_plan_id','meal_template_type'];

    public function meal()
    {
        return $this->belongsTo('App\Models\Recommended_meal', 'meal_id','id')->with('meal_foods');
    } 

    public function workout()
    {
        return $this->belongsTo('App\Models\Workout', 'workout_id','id')->with('workout_exercise');
    }

    public function master_template()
    {
        return $this->belongsTo('App\Models\Master_template', 'master_template_id','id');
    }

    public function meal_plan()
    {
        return $this->belongsTo('App\Models\Meal_plan', 'meal_plan_id','id');
    }

    public function config() {
         return $this->hasMany('App\Models\Nutrition_exercise_day_time','nutrition_exercise_plan_id');
    } 

   
}
