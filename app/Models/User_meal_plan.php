<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_meal_plan extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_meal_plan';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['meal_plan_id','user_id'];


    public function meal_plan()
    {
        return $this->belongsTo('App\Models\Meal_plan', 'meal_plan_id','id')->with('nutrition_exercise_app');
    }

    public function new_meal_plan()
    {
        return $this->belongsTo('App\Models\Meal_plan', 'meal_plan_id','id');
    }

    public function user_with_token()
    {
        return $this->belongsTo('App\Models\User', 'user_id','id')->with('device_token');
    }

    public function user_nutrition_exercise_plan(){

        return $this->hasMany('App\Models\User_nutrition_exercise_plan','user_meal_plan_id')->with(['config','master_template','workout','meal']);
    }

    public function nutrition_exercise_data(){

        return $this->hasMany('App\Models\User_nutrition_exercise_plan','user_meal_plan_id')->with(['config','master_template']);
    }

    

   
}
