<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class User_workout_routine_exercise extends Model
{
   
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_workout_routine_exercises';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_workout_routine_id','exercise_image_id','start_date','end_date','spend_time_in_second','exercise_type','sets','reps','weights',
        'cardio_duration','cardio_calories','cardio_distance','cardio_avg_speed','cardio_avg_heartrate'];


    public function exercise_image()
    {
        return $this->belongsTo('App\Models\Exercise_image','exercise_image_id');
    }

  
}
