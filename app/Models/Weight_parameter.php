<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Weight_parameter extends Model
{
    
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'weight_parameters';

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','baseline_weight',
    					  'max_weight','min_weight',
    					  'high_alert_lbs','high_alert_per',
    					  'med_alert_lbs','med_alert_per',
    					  'med_alert_day','med_alert_lbs_weekly','med_alert_per_weekly',
    					  'created_by','updated_by'];

    // Relationship
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }					  

}
