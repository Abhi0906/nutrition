<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exercise_image extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'exercise_images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','exercise_type','description','exercise_time','image_name',
                           'calorie_burn','experience','body_part','equipment','steps' 
                           ];
    
    protected $dates = ['deleted_at'];

    // Query Scope

     

    public function scopeSearch($query,$data){
       
       //dd($data)
       return $query->where(function($q) use($data){

             $keyword = (isset($data['keyword']) && $data['keyword'] != "undefined")?$data['keyword']:'';
             $minimun_exercise_time = (isset($data['minimun_exercise_time']) && $data['minimun_exercise_time'] != 'undefined')?$data['minimun_exercise_time']:'';
             $maximum_exercise_time = (isset($data['maximum_exercise_time']) && $data['maximum_exercise_time'] != 'undefined')?$data['maximum_exercise_time']:'';
             $minimun_calorie_burn = (isset($data['minimun_calorie_burn']) && $data['minimun_calorie_burn'] != "undefined")?$data['minimun_calorie_burn']:'';
             $maximum_calorie_burn = (isset($data['maximum_calorie_burn']) && $data['maximum_calorie_burn'] != "undefined")?$data['maximum_calorie_burn']:'';
                     // dd($data);
             if($keyword != ""){
               $q->where('name','LIKE',"%$keyword%");
             }if($minimun_exercise_time != "" && $maximum_exercise_time != "") {
                $q->whereBetween('exercise_time',[$minimun_exercise_time,$maximum_exercise_time]);
             }if($minimun_calorie_burn != "") {
                  $q->where('calorie_burn','>=', $minimun_calorie_burn);
             }if($maximum_calorie_burn != "") {
                  $q->where('calorie_burn','<=', $maximum_calorie_burn);
             }if(isset($data['experience']) && ($data['experience'] != "undefined" && $data['experience'] !="" )) {
                $experience = $data['experience'];
                $q->where('experience', $experience);
             }if(isset($data['body_part']) && ($data['body_part'] != "undefined" && $data['body_part'] !="" )) {
                $body_part = $data['body_part'];
                $q->where('body_part', $body_part);
             }if(isset($data['equipment']) && ($data['equipment'] != "undefined") && $data['equipment'] !="") {
                $equipment = $data['equipment'];
                $q->where('equipment', $equipment);
             }if(isset($data['exercise_type']) && ($data['exercise_type'] != "undefined") && $data['equipment'] !="") {
                $exercise_type = $data['exercise_type'];
                $q->where('exercise_type', $exercise_type);
             }


           
       });
    }
   
   
}
