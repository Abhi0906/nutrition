<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Blood_pressure_parameter extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'blood_pressure_parameters';

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'systolic_high_ul',
    					  'systolic_high_ll','systolic_med_ul',
    					  'systolic_med_ll','diastolic_high_ul',
    					  'diastolic_med_ul','diastolic_med_ll',
    					  'created_by','updated_by'];

    // Relationship
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }   
}
