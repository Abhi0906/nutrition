<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Meal_plan extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'meal_plans';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','gender','plan_type'];

    public function nutrition_exercise_data(){

        return $this->hasMany('App\Models\Nutrition_exercise_plan','meal_plan_id')->with(['master_template','config']); 
    }

    public function nutrition_exercise_app(){

        return $this->hasMany('App\Models\Nutrition_exercise_plan','meal_plan_id')->with(['config','master_template','workout','meal','meal_plan']); 
    }

   

   
}
