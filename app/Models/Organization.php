<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Organization extends Model
{
     use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'organizations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','contact_first_name','contact_last_name',
                            'email','mobile','telephone','postcode',
                            'logo','theme_color','sub_domain','domain','address'];

    

    protected $dates = ['deleted_at'];


     public function scopeSearch($query,$data){ 
        return $query->where(function($q) use($data){
            $keyword = $data['keyword'];
            if($keyword != ""){
               $q->where('name','LIKE',"%$keyword%");
            } 
        });
    }
}
