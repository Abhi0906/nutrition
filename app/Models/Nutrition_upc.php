<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Nutrition_upc extends Model
{
   use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */

    protected $table = 'user_nutrition_upc';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nutrition_id', 'upc','guid','timestamp'];

 
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function nutrition()
    {
        return $this->belongsTo('App\Models\Nutrition');
    }
}
