<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Class_schedule_day extends Model
{
    protected $table = "class_schedule_days";

    protected $fillable = ['class_schedule_id', 'day_index', 'class_start_time', 'class_end_time'];

    public function class_schedule(){
    	return $this->belongsTo('App\Models\Class_schedule');
    }
}
