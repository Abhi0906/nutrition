<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Meal_template_user extends Model
{
    protected $table = "meal_template_user";

    protected $fillable = ['user_id', 'meal_template_id'];
}
