<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event_setting extends Model {
    

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'event_settings';

    public function getTable() {
      if(\Session::has("organization_id")){
        $org_id = \Session::get("organization_id");
        return $org_id . '_event_settings';
      }
      return 'event_settings';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type','config','is_active'];

    

    protected $dates = ['deleted_at'];

   
}
