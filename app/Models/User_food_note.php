<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User_food_note extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_food_notes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['user_id','log_date','guid','timestamp','notes'];

    protected $dates = ['deleted_at'];

    // Relationship
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

   
}
