<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class FoodUpdateLogEntry extends Model
{
     use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'usda_food_update_log_entry ';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
}
