<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Blood_sugar_parameter extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'blood_sugar_parameters';

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'lb_high',
    					  'lb_med','ub_fasting_med',
    					  'ub_pp_hours','ub_pp_med',
    					  'ub_random_chk_med','is_single_reading','ub_random_chk_high',
    					  'created_by','updated_by'];

    // Relationship
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }   
}
