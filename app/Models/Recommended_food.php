<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recommended_food extends Model
{
    use SoftDeletes;

    protected $table = 'recommended_foods';

    protected $fillable = ['food_recommendation_type_id', 'nutrition_id', "calories", "serving_quantity", 
    						"serving_size_unit", "no_of_servings","serving_range", "serving_string", "food_for"];

    public function nutritions(){
    	return $this->belongsTo('App\Models\Nutrition','nutrition_id');
    }
}
