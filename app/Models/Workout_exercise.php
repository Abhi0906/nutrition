<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Workout_exercise extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'workout_exercise';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['workout_id','exercise_image_id','video_id','exercise_id','sort_order'];

    public function exercise_images()
    {
        return $this->belongsTo('App\Models\Exercise_image','exercise_image_id');
    }

    public function videos()
    {
        return $this->belongsTo('App\Models\Video','video_id');
    }

}
