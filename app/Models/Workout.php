<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Workout extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'workouts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title','description','experience','body_part','guid','timestamp','is_admin','image'];

    
    // Query Scope

     /**
     * Scope a query to only include patient users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAdmin($query)
    {
        return $query->where('is_admin','=',"1");
    }

    // public function videos()
    // {
    //     return $this->belongsToMany('App\Models\Video');
    // }

    // public function exercise_images()
    // {
    //     return $this->belongsToMany('App\Models\Exercise_image')->withPivot('workout_id','exercise_image_id','sort_order')->orderBy('sort_order','asc');
    // }


    public function workout_exercise()
    {
        return $this->hasMany('App\Models\Workout_exercise')->with(['exercise_images','videos'])->orderBy('sort_order','asc');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }


    public function scopeSearch($query,$data){
       
       return $query->where(function($q) use($data){

             $keyword = (isset($data['keyword'])?$data['keyword']:'');
             $experience = (isset($data['experience']) && $data['experience'] != 'undefined')?$data['experience']:'';
             $body_part = (isset($data['body_part']) && $data['body_part'] != 'undefined')?$data['body_part']:'';
             if($keyword != ""){
               $q->where('title','LIKE',"%$keyword%");
             }if($experience != "") {
                $q->where('experience', $experience);
             }if($body_part != "") {
                $q->where('body_part', $body_part);
            }
          
       });
    }
    
    public function templates()
    {
        return $this->belongsToMany('App\Models\Template');
    }
}
