<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class SocialMedia extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'social_media';

     public function getTable() {
      if(\Session::has("organization_id")){
        $org_id = \Session::get("organization_id");
        return $org_id . '_social_media';
      }
      return 'social_media';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title','url','slug','sort_order','is_default'];

}
