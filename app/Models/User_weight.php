<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User_weight extends Model {
    

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_weight';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['current_weight', 'body_fat', 'log_date', 'guid', 'source','timestamp','created_by','updated_by'];

    protected $dates = ['deleted_at'];

    protected $appends = ['execute_date'];

    public function getExecuteDateAttribute()
    {
      
        return  $this->attributes['log_date'];
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}
