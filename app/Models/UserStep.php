<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserStep extends Model
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_steps';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'total_steps', 'total_distance','log_date', 'guid', 'timestamp'];
}
