<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Medicine extends Model {
    

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'medicines';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */  
    protected $dates = ['deleted_at'];

    protected $appends = ['display_medicine_name','display_medicine_direction'];

    public function getDisplayMedicineNameAttribute()
    {
        $medicine_name = $this->attributes['name'];

        if($this->attributes['generic_name'] != null){

           $medicine_name.= $this->attributes['generic_name']." ";  
        }

        if($this->attributes['dose'] != null){

           $medicine_name.= $this->attributes['dose']." ";  
        }

        if($this->attributes['dose_form'] != null){

           $medicine_name.= $this->attributes['dose_form']." ";  
        }

        if($this->attributes['dose_strength'] != null){

           $medicine_name.= $this->attributes['dose_strength']." ";  
        }

        if($this->attributes['frequency'] != null) {

           $medicine_name.= $this->attributes['frequency']." ";  
        }

        if($this->attributes['uom'] != null){

           $medicine_name.= $this->attributes['uom']." ";  
        }

        if($this->attributes['route'] != null){

           $medicine_name.= $this->attributes['route'];  
        }

        return $medicine_name;       
    }

    // Query Scope

     /**
     * Scope a query to only include most consumed medicine.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMostConsumed($query)
    {
        return $query->where('most_consumed', '=', '1');
    }

    /**
     * Scope a query to search medicine.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSerachMedicines($query,$med_name = null)
    {
        return $query->where('medicines.name', 'LIKE', "%{$med_name}%")
                    ->orWhere('medicines.generic_name', 'LIKE', "%{$med_name}%")
                    ->orWhere('medicines.dose', 'LIKE', "%{$med_name}%")
                    ->orWhere('medicines.dose_form', 'LIKE', "%{$med_name}%")
                    ->orWhere('medicines.dose_strength', 'LIKE', "%{$med_name}%")
                    ->orWhere('medicines.uom', 'LIKE', "%{$med_name}%")
                    ->orWhere('medicines.frequency', 'LIKE', "%{$med_name}%")
                    ->orWhere('medicines.route', 'LIKE', "%{$med_name}%");
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }

    public function getDisplayMedicineDirectionAttribute(){

        $medicine_direction = '';

        if($this->attributes['dose_strength'] != null){

           $medicine_direction.= $this->attributes['dose_strength']." ";  
        }

        if($this->attributes['dose_form'] != null){

           $medicine_direction.= $this->attributes['dose_form']." ";  
        }
        $medicine_direction.= " (";
        if($this->attributes['dose'] != null){

           $medicine_direction.= $this->attributes['dose']."";  
        }

        if($this->attributes['uom'] != null){

           $medicine_direction.= $this->attributes['uom']." ";  
        }
        $medicine_direction.= ") by/with ";
        if($this->attributes['route'] != null) {

           $medicine_direction.= $this->attributes['route']." ";  
        }

        if($this->attributes['frequency'] != null){

           $medicine_direction.= $this->attributes['frequency']." ";  
        }

        return $medicine_direction;       
    }


   
}
