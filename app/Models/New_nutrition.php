<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class New_nutrition extends Model
{
     use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'new_nutritions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['row_id','name','brand_name','brand_with_name','nutrition_data','serving_data','source_name'];

    

    protected $dates = ['deleted_at'];
}
