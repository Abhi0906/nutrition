<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Recommended_meal_food extends Model
{

    protected $table = "recommended_meal_foods";

    protected $fillable = ['meal_id', 'item_id', 'nutrition_id', 'calories', 'guid', 'timestamp',
    						'serving_quantity', 
    						'serving_size_unit', 'no_of_servings', 'serving_string'];

    public function meal(){
    	return $this->belongsTo('App\Models\Recommended_meal', 'meal_id');
    }

    public function item(){
    	return $this->belongsTo('App\Models\Recommended_meal_item_type', 'item_id');
    }

    public function nutritions()
    {
        return $this->belongsTo('App\Models\Nutrition','nutrition_id');
    }
}
