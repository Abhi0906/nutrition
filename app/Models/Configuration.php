<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Configuration extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'configurations';

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title','description','param','value'];


    // Query Scope

     /**
     * Scope a query to only include patient users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVitalParameters($query)
    {
        return $query->whereIn('title', ['Weight','PulseOx','BloodPressure','BloodSugar']);
                   
    }

}
