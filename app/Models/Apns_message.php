<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Apns_message extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
     protected $table = 'apns_messages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','apns_device_id','message',
                           'type','json_data','delivered_at','status'];

    

 
}
