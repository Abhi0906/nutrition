<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Nutrition extends Model {
    

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nutritions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

   /*  protected $fillable = ['nutrition_brand_id','name','item_id',
                            'calories','brand_id','serving_quantity',
                            'serving_size_unit','serving_size_weight_grams','servings',
                            'calcium_dv','calories_from_fat','cholesterol',
                            'data_source','dietary_fiber','iron_dv',
                            'metric_unit','potassium','protein',
                            'saturated_fat','sodium','sugars',
                            'total_carb','total_fat','trans_fat',
                            'upc','vitamin_a','vitamin_c',
                            'image_name','ingredient_statement','is_custom'];*/

    protected $fillable = ['row_id','name','brand_name','brand_with_name','nutrition_data','serving_data',
                            'source_name'
                            ];

    protected $dates = ['deleted_at'];



    /**
     * Scope a query to search nutritions.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSerachNutrition($query,$food_name = null)
    {
        $food_name = trim($food_name);
        $query->where('nutritions.name', 'LIKE', "%{$food_name}%")
                    ->orWhereHas('nutrition_brand',function($nu) use($food_name){
                             $nu->where('nutrition_brands.name', 'LIKE', "%{$food_name}%");
                    });
        // $foodSearchTextArray = explode(" ", $food_name);
        
        // if(sizeof($foodSearchTextArray) > 1) {
        //     foreach ($foodSearchTextArray as  $value) {
        //         $value = trim($value);
        //         $query->orWhere('nutritions.name', 'LIKE', "%{$value}%")
        //                ->orWhereHas('nutrition_brand',function($nu) use($value){
        //                      $nu->where('nutrition_brands.name', 'LIKE', "%{$value}%");
        //             });
        //     }
        // }

        // $query->where('nutritions.name_with_brand', 'LIKE', "%{$food_name}%");
              // ->orWhere('nutritions.name', 'LIKE', "%{$food_name}%");           
      
        return $query;

    }


    // Relationship
    
    public function nutrition_brand()
    {
        return $this->belongsTo('App\Models\Nutrition_brand','nutrition_brand_id');
    }

    // public function user(){
    //     return $this->belongsToMany('App\Models\User', 'custom_nutrition_user');
    // }

   
}
