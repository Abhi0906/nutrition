<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Food_recommendation_type extends Model
{
    //use SoftDeletes;

    protected $table = 'food_recommendation_types';

    protected $fillable = ['parent', 'name'];

    public function recommended_foods(){
    	return $this->hasMany('App\Models\Recommended_food')->with('nutritions');
    }
}
