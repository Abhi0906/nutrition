<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Class_schedule extends Model
{
    protected $table = "class_schedules";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['class_location_id', 'class_name', 'instructor', 'activity_type', 
    						'class_max_size', 'class_start_date', 'class_end_date'];


    public function class_schedule_days(){
    	return $this->hasMany('App\Models\Class_schedule_day');
    }
}
