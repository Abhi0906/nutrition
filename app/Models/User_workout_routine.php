<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class User_workout_routine extends Model
{
     use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_workout_routines';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','workout_id','log_date','guid','timestamp','total_calories','total_miles','total_weight_lifted'];

    

    protected $dates = ['deleted_at'];


    public function workout_routine()
    {
        return $this->belongsTo('App\Models\Workout','workout_id');
    }

    public function user_workout_rouitne_exercises()
    {
        return $this->hasMany('App\Models\User_workout_routine_exercise')->with('exercise_image');
    }
}
