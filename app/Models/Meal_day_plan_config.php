<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Meal_day_plan_config extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'meal_day_plan_config';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['meal_day_plan_id','complete_plan_id','dayName','dayNumber','guid'];

     
}
