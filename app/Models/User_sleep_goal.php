<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User_sleep_goal extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_sleep_goals';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['user_id','goal','goal_date','start_date','created_by','updated_by'];

    protected $dates = ['deleted_at'];

    // Relationship
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

   
}
