<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;


class Apns_device extends Model
{
     //use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'apns_devices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','device_uuid','device_token',
                           'platform_type','app_name','app_version',
                           'device_name','device_model','device_os_version',
                           'push_badge','push_alert','push_sound','environment',
                           'status'
                          ];

    

   // protected $dates = ['deleted_at'];
}
