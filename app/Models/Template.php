<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Template extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'templates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title','description','template_type','days','image'];

    public function workouts()
    {
        return $this->belongsToMany('App\Models\Workout')->with('workout_exercise')->withPivot('config','start','end');
    }

    public function scopeSearch($query,$data){ 
        return $query->where(function($q) use($data){
            $keyword = $data['keyword'];
            
            if($keyword != ""){
               $q->where('title','LIKE',"%$keyword%");
            } 
        });
    }
   

}
