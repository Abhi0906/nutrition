<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Weeklygoals extends Model
{
   // use SoftDeletes;



     protected $table = 'weekly_goals';


     protected $fillable = ['name','value','short_name','sort_order'];

    

    // protected $dates = ['deleted_at'];
}
