<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class NutrientAnalysisDetails extends Model
{
     use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nutrient_analysis_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
}
