<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Recommended_meal extends Model
{

    protected $table = "recommended_meals";

    protected $fillable = ['meal_name', 'meal_template_id', 'meal_time', 'guid', 'timestamp', 'gender'];

    public function meal_foods(){
    	return $this->hasMany('App\Models\Recommended_meal_food', 'meal_id')->with('nutritions','item');
    }

    public function meal_items(){
    	return $this->belongsToMany('App\Models\Recommended_meal_item_type', 'recommended_meal_foods', 'meal_id', 'item_id', 'nutrition_id')
    				->withPivot('nutrition_id');
    }

    public function template(){
    	return $this->belongsTo('App\Models\Meal_template', 'meal_template_id');
    }

}
