<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sync_device_authentication extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sync_device_authentications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'device_user_id', 'device_type', 'auth_config'];
}
