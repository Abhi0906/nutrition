<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Routine_time extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'routine_times';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['event_type','event_name','time'];

    protected $appends = ['display_name'];

    protected $dates = ['deleted_at'];

    public function getDisplayNameAttribute()
    {
      
        return  ucfirst($this->attributes['event_name'])." [".ucfirst($this->pivot->attributes['time']."]");
    }

    // Relationship
    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    } 
}
