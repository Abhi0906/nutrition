<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Deal extends Model
{
	use SoftDeletes;
    protected $table = 'deals';

    public function getTable() {
      if(\Session::has("organization_id")){
        $org_id = \Session::get("organization_id");
        return $org_id . '_deals';
      }
      return 'deals';
    }

    protected $fillable = ['title', 'description', 'start','end','coupon_code','image', 'is_expired'];
}
