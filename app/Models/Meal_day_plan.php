<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Meal_day_plan extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'meal_day_plan';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['complete_plan_id','day_plan_id'];

    public function config() {
         return $this->hasMany('App\Models\Meal_day_plan_config','meal_day_plan_id');
    } 

     
}
