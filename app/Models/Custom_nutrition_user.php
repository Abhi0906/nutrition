<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Custom_nutrition_user extends Model
{
    
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'custom_nutrition_user';

    
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'name', 'brand_name','brand_with_name','nutrition_data','timestamp', 'guid','created_by','updated_by'];

   /* public function nutritions()
    {
        return $this->belongsTo('App\Models\Nutrition', 'nutrition_id')->with('nutrition_brand');
    }
   */
}
