<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class Blood_pressure_pulse extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */

    protected $table = 'user_bp_pulse';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_pulse_id','systolic', 'diastolic','log_date', 'guid', 'source','timestamp','created_by','updated_by'];
   // protected $hidden = ['pulse'];
   // protected $appends = ['pulse_rate'];
    protected $dates = ['deleted_at'];

    public function pulse()
    {
        return $this->belongsTo('App\Models\User_pulse', 'user_pulse_id','id');
    } 

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    /*public function getPulseRateAttribute()
    {
        return $this->pulse->pulse;
    }*/
}
