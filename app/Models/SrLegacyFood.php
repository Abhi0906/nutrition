<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class SrLegacyFood extends Model
{
     use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'usda_sr_legacy_food';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
}
