<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Video extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'videos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['uri','name','description',
                           'link','duration','width','language',
                           'height','status','embed','pictures',
                           'external_video_id',
                           'created_by','updated_by',
                           'workout_time',
                           'minimun_calorie_burn','maximum_calorie_burn',
                           'experience','body_parts',
                           'equipments',
                           'assigned_properties' 
                           ];
    
    protected $dates = ['deleted_at'];

    // Query Scope

     /**
     * Scope a query to only include available videos.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAvailable($query)
    {
        return $query->where('status', '=', 'available');
    }

    public function scopeSearch($query,$data){
       
       
       return $query->where(function($q) use($data){

             $keyword = $data['keyword'];
             $minimun_workout_time = $data['minimun_workout_time'];
             $maximum_workout_time = $data['maximum_workout_time'];
             $minimun_calorie_burn = $data['minimun_calorie_burn'];
             $maximum_calorie_burn = $data['maximum_calorie_burn'];
             $assigned_properties = isset($data['assigned_properties']) ? $data['assigned_properties']:''; 
            
             if($keyword != ""){
               $q->where('name','LIKE',"%$keyword%");
             }if($minimun_workout_time != "" && $maximum_workout_time != "") {
                $q->whereBetween('workout_time',[$minimun_workout_time,$maximum_workout_time]);
             }if($minimun_calorie_burn != "") {
                  $q->where('minimun_calorie_burn','>=', $minimun_calorie_burn);
             }if($maximum_calorie_burn != "") {
                  $q->where('maximum_calorie_burn','<=', $maximum_calorie_burn);
             }if($data['experience'] != "undefined") {
                $experience = $data['experience'];
                $q->where('experience', $experience);
             }if($data['body_part'] != "undefined") {
                $body_part = $data['body_part'];
                $q->where('body_parts', $body_part);
             }if($data['equipment'] != "undefined") {
                $equipment = $data['equipment'];
                $q->where('equipments', $equipment);
             }if($assigned_properties != ""){
                $assigned_properties = ($assigned_properties == 'true')?0:1;
               $q->where('assigned_properties','=',$assigned_properties);
             }

           
       });
    }
   
    public function workouts()
    {
        return $this->belongsToMany('App\Models\Workout');
    }

}
