<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_meal_day_plan extends Model
{


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_meal_day_plan';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','complete_plan_id','day_plan_id'];

    public function config() {
        return $this->hasMany('App\Models\User_meal_day_plan_config','user_meal_day_plan_id');
    }


}
