<?php

namespace App\Http\Middleware;
//namespace App\Models;
use App\Models\Organization;
use Closure;

class CheckSubdomain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        $url = $_SERVER['HTTP_HOST'];
        $parsedUrl  =  parse_url($url);

        $host = explode('.', $parsedUrl['path']);
        $domain = $parsedUrl['path'];
        $subdomain  = $domain;

        $subdomains = array_slice($host, 0, count($host) - 2 );
        if(count($subdomains) > 0){
           $subdomain  = $subdomains[0];
        }
       
        $organization = \App\Models\Organization::where('sub_domain',$subdomain)
                                                  ->orWhere('domain', $domain)
                                                  ->first();
         if($organization != null){
             \Session::put('organization_id', $organization->id);
         }

      return $next($request);
    }
}
