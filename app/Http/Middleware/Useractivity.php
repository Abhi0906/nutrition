<?php

namespace App\Http\Middleware;
//namespace App\Models;
use App\Models\User;
use Closure;

class Useractivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if(\Auth::check()){
             $user_id = \Auth::User()->id;
             if($user_id != null){
                   $user = \App\Models\User::where('id',$user_id)->first();
                   if($user != null){
                       $date = date('Y-m-d h:i:s', time());
                       $user->last_active = $date;           
                       $user->save();
                   }
                }
         }
         $response = $next($request);
         // Perform action
         return $response;
    }
}
