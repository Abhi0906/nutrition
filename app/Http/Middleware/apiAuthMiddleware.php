<?php

namespace App\Http\Middleware;

use Closure;

class apiAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if (\Auth::check()){
            return $next($request);
        } else{

            if(\Request::header('php-auth-user') == 'george'){

                return \Auth::onceBasic('username') ?: $next($request);
                
            }else{
                
                return \Auth::onceBasic('username') ?: $next($request);
                
             }
        }
    }
}
