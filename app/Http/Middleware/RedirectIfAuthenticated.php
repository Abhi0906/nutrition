<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        switch ($guard) {
          case 'admin' :
              if (Auth::guard($guard)->check()) {
                if((Auth::guard($guard)->user()->getAttributes()['user_type']=='super_admin')||(Auth::guard($guard)->user()->getAttributes()['user_type']=='doctor')){
                  return redirect('/admin/patient');
                }
              }
              break;
          default:
              if (Auth::guard($guard)->check()) {
                  return redirect('/dashboard');
              }
              break;
        }
        /*if (Auth::guard($guard)->check()) {
          if((Auth::guard($guard)->user()->getAttributes()['user_type']=='super_admin')||(Auth::guard($guard)->user()->getAttributes()['user_type']=='doctor')){
            return redirect('/admin/patient');
          }
        }*/
        return $next($request);
    }
}
