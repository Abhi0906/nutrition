<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Video;

class VideoController extends Controller
{

   protected $number_of_records = 50;
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
       return view('video.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
       $error = false;
       $video = Video::find($id);
        if($video != null) {
           $result = $video;
        }else{
            $error = true;
            $result = "No Video Found.";
        }
        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
         $error = false;
         $video =Video::find($id);
         if($video != null) {

           $input = $request->all();
           $input['assigned_properties'] =1;
           $result = $video->update($input);

        }else{
            $error = true;
            $result = "No Video Found.";
        }
        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $error =false;
        $video = Video::find($id);
        if(!$video){
          $error= true;
        }
        $video->delete();
        return response()->json(['error' =>$error],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function getList(Request $request){

        $videos =[];
        $input = $request->all();
        $perPage = (isset($input['itemPerPage']))?$input['itemPerPage']:$this->number_of_records;
        if(!empty($input) && isset($input['keyword'])){
            $videos = Video::available()
                            ->search($input)
                            ->paginate($perPage);
        }else{
          $videos = Video::available()->paginate($perPage);
        }
        foreach ($videos as $key => $video) {
            $video->pictures = json_decode($video->pictures);
            $video->imageUrl = $video->pictures->sizes[4]->link;
            $video->pictures->imageUrl = $video->pictures->sizes[4]->link;
         }
        return response()->json(['error' => false,'response'=>$videos->toArray()],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function getTrainingTypes(){
        $training_types = \App\Models\Training_type::orderBy('sort_order','asc')->get(['id','display_name']);
        return response()->json(['error' => false,'response'=>$training_types],200)
                         ->setCallback(\Request::input('callback'));

    }

    public function getEquipments(){
        $equipments = \App\Models\Equipment::orderBy('sort_order','asc')->get(['id','display_name']);
        return response()->json(['error' => false,'response'=>$equipments],200)
                         ->setCallback(\Request::input('callback'));

    }

    public function fetchVideos(){

        try{
             $call_command = \Artisan::call('videos:fetch');
             $videos =[];
             $videos = Video::available()->paginate($this->number_of_records);
             foreach ($videos as $key => $video) {
                $video->pictures = json_decode($video->pictures);

                $video->pictures->imageUrl = $video->pictures->sizes[4]->link;
             }
            return response()->json(['error' => false,'response'=>$videos->toArray()],200)
                         ->setCallback(\Request::input('callback'));

        }catch (\Exception $e) {
            return response()->json(['error' => true,'response'=>$e->getMessage()],200)
                         ->setCallback(\Request::input('callback'));
        }

    }

     public function getBodyParts(){
        $body_parts = \App\Models\Body_part::orderBy('id','asc')->get(['id','name']);
        /*if(\Request::header('request-from') != 'iPhone'){

            foreach ($body_parts as $key => $parts) {
              if($parts->parent == null){
                $parent_id = $parts->id;
               // $parent_name = $parts->name;
                //  unset($body_parts[$key]);
              }
              if($parent_id == $parts->parent){
                    $parts->parent_name = $parent_name;
              }
            }

        }*/

        return response()->json(['error' => false,'response'=>$body_parts],200)
                         ->setCallback(\Request::input('callback'));

    }


}
