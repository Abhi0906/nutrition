<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use RingCentral\SDK\SDK;
use RingCentral\SDK\Http\HttpException;
use RingCentral\http\Response;
use Config;

class HomeController extends Controller
{
    public function index(){
        return view('home');
    }

    public function whyUs(){
        return view('whyChooseUs');
    }

    public function contactUs(){
        return view('emails.contact_email');
    }

    public function user_plan(){
        return view('front.my_plans.user_plan');
    }

    public function create_user_meal(){
        return view('front.my_plans.create_user_meal');
    }

    public function recommended_food(){
        return view('recommended.recommended_food');
    }

    public function recommended_addfood(){
        return view('recommended.recommended_addfood');
    }

    public function recommended_meal(){
        return view('recommended.recommended_meal');
    }

    public function mdIndex(){
        return view('md_meals.index');
    }

    public function recommended_meal_items_data(){
        return view('recommended.recommended_meal_items_data');
    }

    public function new_recommended_meal_items_data(){
        return view('recommended.new_recommended_meal_items_data');
    }

    public function recommended_meals_display(){
        return view('recommended.recommended_meals_display');
    }

    public function md_meals_display(){
        return view('md_meals.md_meals_display');
    }

    public function recommended_food_content(){
        return view('recommended.recommended_food_content');
    }

    public function add_recommended_food(){
        return view('recommended.add_recommended_food');
    }

    public function edit_recommended_food(){
        return view('recommended.edit_recommended_food');
    }

    public function food_home_page(){
        return view('front.food.food-home-page');
    }

    public function log_food(){
        return view('front.food.log_food');
    }

    public function add_custom_food(){
        return view('front.food.add-custom-food');
    }

    public function complete_plans_display(){
        return view('complete_plans.complete_plans_display');
    }

    public function day_plans_display(){
        return view('day_plans.day_plans_display');
    }

    public function assign_plans_display(){
        return view('assign_plan.assign_plans_display');
    }

    public function assign_plan_details(){
        return view('assign_plan.assign_plan_details');
    }

    public function assign_day_plan_details(){
        return view('assign_plan.assign_day_plan_details');
    }

    public function view_complete_plan(){
        return view('complete_plans.view_complete_plan');
    }

    public function view_day_plan(){
        return view('day_plans.view_day_plan');
    }

    public function custom_foods(){
        return view('front.food.custom-foods');
    }

    public function exercise_home_page(){
        return view('front.exercise.exercise-home-page');
    }

    public function log_exercise(){
        return view('front.exercise.log_exercise');
    }

    public function create_meal(){
        return view('front.food.create-meal');
    }

    public function add_recommended_meal_food(){
        return view('recommended.add_recommended_meal_food');
    }

    public function md_recommended_food_type(){
        return view('recommended.md_recommended_food_type');
    }

    public function list_exercise_image(){
        return view('exercise_image.list_exercise_image');
    }

    public function list_challenge(){
        return view('challenges.list_challenge');
    }

    public function list_referfriend(){
        return view('refer_friend.list_referfriend');
    }

    public function add_challenge(){
        return view('challenges.add_challenge');
    }

    public function edit_challenge(){
        return view('challenges.edit_challenge');
    }

    public function add_exercise_image(){
        return view('exercise_image.add_exercise_image');
    }

    public function edit_exercise_image(){
        return view('exercise_image.edit_exercise_image');
    }

    public function user_meals(){
        return view('front.food.user-meals');
    }

    public function list_workouts(){
        return view('workout.list_workouts');
    }

    public function edit_workout(){
        return view('workout.edit_workout');
    }

    public function add_workout(){
        return view('workout.add_workout');
    }

    public function add_template(){
        return view('template.add_template');
    }

    public function list_templates(){
        return view('template.list_templates');
    }

    public function edit_template(){
        return view('template.edit_template');
    }

    public function classes(){
        return view('class_schedule.classes');
    }

    public function event_setting(){
        return view('class_schedule.event_setting');
    }

    public function club_holidays(){
        return view('class_schedule.club_holidays');
    }

    public function deal_list(){
        return view('deal.deal_list');
    }

    public function create_deal(){
        return view('deal.create_deal');
    }

    public function edit_deal(){
        return view('deal.edit_deal');
    }

    public function deal(){
        return view('front.deal');
    }

    public function workout_details(){
        return view('front.user_workout.workout_details');
    }

    public function display_challenges(){
        return view('front.display_challenges');
    }

    public function diary_home_page(){
        return view('front.diary.diary_home_page');
    }

    public function user_workout(){
        return view('front.user_workout.user_workout');
    }

    public function loadWorkout_details(){
        return view('front.user_workout.workout_details');
    }

    public function create_meal_templatename(){
        return view('recommended.create_meal_templatename');
    }

    public function meal_template_names(){
        return view('recommended.meal_template_names');
    }

    public function edit_meal_templatename(){
        return view('recommended.create_meal_templatename');
    }

    public function routine_details(){
        return view('front.user_workout.routine_details');
    }

    public function admin_custom_foods(){
        return view('custom_food.admin_custom_foods');
    }

    public function custom_foods_form(){
        return view('custom_food.custom_foods_form');
    }

    public function food_content(){
        return view('manage_food.food_content');
    }

    public function add_edit_food_content(){
        return view('manage_food.add_edit_food_content');
    }

    public function sendMessage(Request $request){
        $input = $request->all();
        $cell_number = $input['phone'];
        try {
            $clientID=Config::get('app.clientid');
            $clientSecret=Config::get('app.clientsecret');
            $sandBoxNumber=Config::get('app.sandboxnumber');
            $sandBoxExtension=Config::get('app.sanboxextension');
            $sandBoxPassword=Config::get('app.sandboxpassword');
            $ringCentralNumber=Config::get('app.ringcentralnumber');
            $rcsdk = new SDK($clientID, $clientSecret, SDK::SERVER_SANDBOX);
            $platform = $rcsdk->platform();
            $auth = $platform->login($sandBoxNumber, $sandBoxExtension, $sandBoxPassword);

            //Send SMS
            $apiResponse = $platform->post('.account.~.extension.~.sms', array(
                'from' => array('phoneNumber'=> $ringCentralNumber),
                'to' => array(
                            array('phoneNumber' => $cell_number),
                            ),
                'text' => 'Testing !',
                ));
            print 'Sent SMS ' . $apiResponse->json()->uri . PHP_EOL;
        } catch (Exception $e) {
            print 'The error is : ' . $e->getMessage() . PHP_EOL;
        }
    }
}
