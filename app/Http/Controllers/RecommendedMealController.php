<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RecommendedMealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createRecommendedMeal(Request $request){
        $error = false;
        $status_code = 200;

        $result = [];
        $input = $request->all();
        
        if(!empty($input)){
            try{
                if(isset($input['user_agent']) && $input['user_agent']=="server"){
                    $input['guid'] = guid();
                    $input['timestamp'] = \Carbon\Carbon::now()->timestamp;

                }
                $input['meal_name'] = trim($input['meal_name']);
                $recommended_meal = \App\Models\Recommended_meal::where('meal_template_id', "=", $input['meal_template_id'])
                                                                ->where('meal_time', '=', $input['meal_time'])
                                                                ->first();
                if($recommended_meal==null){
                    $create_recommended_meal = \App\Models\Recommended_meal::create($input);
                    if($create_recommended_meal){
                        $last_recommended_meal = \App\Models\Recommended_meal::find($create_recommended_meal->id);
                        $result['last_recommended_meal'] = $last_recommended_meal;
                        $result['message'] = "Recommended meal created successfully";
                    }
                    else{
                        $error = true;
                        $result['error_message'] = "Recommended meal not created"; 
                    }  
                }
                else{
                    $error = true;
                    $result['error_message'] = "Meal is already exist"; 
                    $result['meal_exist'] = true;  
                }
                
            }
            catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }
            
        }
        else{
            $error = true;
            $result['error_message'] = "Request data is empty";
        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
        
    }

    public function getRecommendedMeals($id){

        $error = false;
        $status_code = 200;

        $result = [];
        
        try{

            $recommended_meals = \App\Models\Recommended_meal::with('meal_foods');

            if($id){
                $recommended_meals = $recommended_meals->where('meal_template_id', $id);
            }
            
            $recommended_meals = $recommended_meals->get();
            
            if($recommended_meals){
                foreach ($recommended_meals as $key => $recommended_meal) {
                    if($recommended_meal->meal_foods !=null ){
                        $max_calorie = (int) $recommended_meal->meal_foods->max('calories');

                        if($max_calorie==null){
                            $recommended_meal->max_calorie = 0;
                        }
                        else{
                            $recommended_meal->max_calorie = $max_calorie;
                        }   
                        $meal_foods_names = [];

                        foreach ($recommended_meal->meal_foods as $key=>$meal_foods) {
                            if($meal_foods->nutritions!=null){
                                $meal_foods_names[] = $meal_foods->nutritions->name;
                            }
                            
                        }                                                    
                        $recommended_meal->meal_foods_names = $meal_foods_names;

                        unset($recommended_meal->meal_foods);
                    }
                    
                }
                $result = $recommended_meals;
            }
            else{
                $error = true;
                $result['error_message'] = "No record found";   
            } 
        }
        catch(\Exception $ex){
            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

     public function getMDMeals(){

        $error = false;
        $status_code = 200;

        $result = [];
        
        try{

            $recommended_meals = \App\Models\Recommended_meal::whereHas('meal_foods')->with('meal_foods');
            $recommended_meals = $recommended_meals->get();

             $last_recommended_meal = \App\Models\Recommended_meal::orderBy('id', 'desc')->first();
              $new_meal_id=1;
             if($last_recommended_meal != null){
                $new_meal_id=$last_recommended_meal->id+1;
              }

            if($recommended_meals){
                foreach ($recommended_meals as $key => $recommended_meal) {
                    if($recommended_meal->meal_foods !=null ){
                        $max_calorie = (int) $recommended_meal->meal_foods->max('calories');

                        if($max_calorie==null){
                            $recommended_meal->max_calorie = 0;
                        }
                        else{
                            $recommended_meal->max_calorie = $max_calorie;
                        }   
                        $meal_foods_names = [];

                        foreach ($recommended_meal->meal_foods as $key=>$meal_foods) {
                            if($meal_foods->nutritions!=null){
                                $meal_foods_names[] = $meal_foods->nutritions->name;
                            }
                            
                        }                                                    
                        $recommended_meal->meal_foods_names = $meal_foods_names;

                        unset($recommended_meal->meal_foods);
                    }
                    
                }
                $result['md_meals'] = $recommended_meals;
                $result['new_meal_id'] = $new_meal_id;
            }
            else{
                $error = true;
                $result['error_message'] = "No record found";   
            } 
        }
        catch(\Exception $ex){
            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function recommendedMealItemData($meal_id){
        $error = false;
        $status_code = 200;
        $result = [];

        try{

            $recommended_meal_item_types = \App\Models\Recommended_meal_item_type::all();
            if($recommended_meal_item_types!=null){
                $data = [];
                foreach ($recommended_meal_item_types as $item) {

                    $data['item_id'] = $item->id;
                    $data['item_name'] = $item->item_name;
                    $data['tech_name'] = $item->tech_name;
                    $data['sub_type'] = $item->sub_type;


                    $recommended_meal_foods = \App\Models\Recommended_meal_food::with('meal')->where('meal_id', '=', $meal_id)
                                                                                    ->where('item_id', '=', $item->id)
                                                                                    ->get();
                    if($recommended_meal_foods){
                        $nutrition_detail = [];
                        foreach ($recommended_meal_foods as $meal_food) {

                            $nutrition_data['recommended_meal_food_prim_id'] = $meal_food->id;
                            $nutrition_data['serving_string'] = $meal_food->serving_string;
                            $no_of_servings = $meal_food->no_of_servings;
                            $nutrition_data['serving_quantity'] = $meal_food->serving_quantity;
                            $nutrition_data['no_of_servings'] = $meal_food->no_of_servings;
                            
                            $nutrition = \App\Models\Nutrition::find($meal_food->nutrition_id);
                 
                            if($nutrition['nutrition_data']){
                                $nutrition['nutrition_data'] = convertStringToArray($nutrition['nutrition_data']);
                            }

                            $nutrition_data['recommended_food_id'] = $nutrition['id'];
                            $nutrition_data['food_name'] = $nutrition['name'];
                            $nutrition_data['brand_name'] = $nutrition['brand_name'];
                            $nutrition_data['calories'] = $meal_food->calories;
                            $total_fat = clearFoodDataValue($nutrition['nutrition_data']['total_fat']);
                            $nutrition_data['total_fat'] = $total_fat * $no_of_servings;

                            $dietary_fiber = clearFoodDataValue($nutrition['nutrition_data']['dietary_fiber']);
                            $nutrition_data['dietary_fiber'] = $dietary_fiber * $no_of_servings;

                            $total_carb = clearFoodDataValue($nutrition['nutrition_data']['total_carb']);
                            $nutrition_data['total_carb'] = $total_carb * $no_of_servings;

                            $sodium = clearFoodDataValue($nutrition['nutrition_data']['sodium']);
                            $nutrition_data['sodium'] = $sodium * $no_of_servings;

                            $protein = clearFoodDataValue($nutrition['nutrition_data']['protein']);
                            $nutrition_data['protein'] = $protein * $no_of_servings;

                            $nutrition_detail[] = $nutrition_data;
                        }

                        $data['meal_item_foods'] = $nutrition_detail;
                    }
                    else{
                        $data['meal_item_foods'] = [];
                    }

                    $result[] = $data;
                }
            }
            else{
                $error = true;
                $result['error_message'] = "No items Found";
            }
        }
        catch(\Exception $ex){
            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function createRecommendedMealFood(Request $request){
        $error = false;
        $status_code = 200;

        $result = [];
        $input = $request->all();
        //dd($input);
        if(!empty($input)){
            try{
                if(isset($input['user_agent']) && $input['user_agent']=="server"){
                    $input['guid'] = guid();
                    $input['timestamp'] = \Carbon\Carbon::now()->timestamp;

                } 

               
                $md_recommended_food = \App\Models\Recommended_meal_food::create($input); 

                if($md_recommended_food){
                    $result['message'] = "Food created successfully";
                }
                else{
                    $error = true;
                    $result['error_message'] = "Food not created successfully";
                }
                
            }
            catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }
            
        }
        else{
            $error = true;
            $result['error_message'] = "Request data is empty";
        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
        
    }

    public function getRecommendedMealItems(){

        $error = false;
        $status_code = 200;

        $result = [];
        
        try{

            $recommended_meal_types = \App\Models\Recommended_meal_item_type::all();
            if($recommended_meal_types){
                $result = $recommended_meal_types;
            }
            else{
                $error = true;
                $result['error_message'] = "No record found";   
            } 
        }
        catch(\Exception $ex){
            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function deleteRecommendedMeal($id){
        $error = false;
        $status_code = 200;

        $result = [];

        try{

            $recommended_meal = \App\Models\Recommended_meal::find($id);
            if($recommended_meal){

                $recommended_meal_foods = \App\Models\Recommended_meal_food::where('meal_id', '=', $id)->delete();       

                $del_recommended_meal = $recommended_meal->delete();
                if($del_recommended_meal){
                    $result['message'] = "Meal deleted successfully";
                }
            }
            else{

                $result['error_message'] = "Meal does not exist";
            }
        }
        catch(\Exception $ex){
            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));       
    }

    public function deleteRecommendedMealFood($id){
        $error = false;
        $status_code = 200;

        $result = [];

        try{

            
            $recommended_meal_foods = \App\Models\Recommended_meal_food::where('id', '=', $id)->delete();       

            if($recommended_meal_foods){
                $result['message'] = "Food deleted successfully";
            }
        }
        catch(\Exception $ex){
            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));       
    }

    public function getMeal($id){
        $error = false;
        $status_code = 200;
        $result = [];
        try{
             $user_meal = \App\Models\Recommended_meal::find($id);
             $result = $user_meal;
        }
        catch(\Exception $ex){
            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }
    
    public function updateMeal($id, Request $request){

        $error = false;
        $status_code = 200;

        $result = [];
        $input = $request->all();
        if(!empty($input)){
            try{
                $check_recommended_meal = \App\Models\Recommended_meal::where('meal_template_id', "=", $input['meal_template_id'])
                                                                ->where('meal_time', '=', $input['meal_time'])
                                                                ->where('id', '<>', $id)
                                                                ->get();
                if($check_recommended_meal->count() >0 ){

                    $error = true;
                    $result['error_message'] = "Meal is already exist"; 
                    $result['meal_exist'] = true;  
                }
                else{
                    $recommended_meal =  \App\Models\Recommended_meal::where('id',$id)->first();
                    if($recommended_meal != null){
                       $update_meal = $recommended_meal->update($input);
                       if($update_meal){
                        $result = $recommended_meal;
                       }
                    }
                }
                
             }
            catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }
            
        }
        else{
            $error = true;
            $result['error_message'] = "Request data is empty";
        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
        
    }

    public function updateRecommendedMealFood(Request $request, $id){
       
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        if(!empty($input) && $id > 0) {
            try{
                 
                 $recommended_food = \App\Models\Recommended_meal_food::where('id',$id)->first();

                 if(!empty($recommended_food)){
                    $recommended_food->update($input);
                    $result['message']='Food Updated Successfully.';
                 } else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Food Found.';
                 }   
               }catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }
             
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
 
    }

    public function getTemplateNames(){
        $error = false;
        $status_code = 200;
        $result = [];
        try{
             $template_names = \App\Models\Meal_template::all();
             if($template_names){
                $result['template_names'] = $template_names;
             }
             else{
                $error = true;
                $result['error_message'] = "No data found";
             }
        }
        catch(\Exception $ex){
            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function getTemplate($id){
        $error = false;
        $status_code = 200;
        $result = [];
        try{
             $template = \App\Models\Meal_template::find($id);
             $last_recommended_meal = \App\Models\Recommended_meal::orderBy('id', 'desc')->first();
              $new_meal_id=1;
             if($last_recommended_meal != null){
                $new_meal_id=$last_recommended_meal->id+1;
             }
            
             $result['template'] = $template;
             $result['new_meal_id'] = $new_meal_id;
        }
        catch(\Exception $ex){
            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function createTemplateName(Request $request){
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();

        try{
            $create_template_name = \App\Models\Meal_template::create($input);
            if($create_template_name){
                $error = false;
                $result['msg'] = "Template name created successfully";
            }
            else{
                $error = true;
                $result['error_msg'] = "Template name not created";   
            }
        }
        catch(\Exception $ex){
            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }
    
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function setDefaultTemplate($id){
        $error = false;
        $status_code = 200;
        $result =[];

        if($id){
            try{
                $meal_template = \App\Models\Meal_template::find($id);

                if($meal_template){
                   $update_template = \App\Models\Meal_template::where('is_default', '1')
                                                                ->where('template_gender', $meal_template->template_gender)
                                                                ->update(['is_default'=>'0']);
    

                    $set_default_template = $meal_template->update(['is_default'=>'1']);
                    if($set_default_template){
                        $error = false;
                        $result['msg'] = "Default template is set successfully";
                    } 
                    else{
                        $error = true;
                        $result['error_msg'] = "Default template is not set successfully";
                    }
                }
                else{
                    $error = true;
                    $result['msg'] = "Template does not exist";
                }
                
            }
            catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }
        }
        else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
        
    }

    public function updateMealTemplate(Request $request, $id){
       
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        if(!empty($input) && $id > 0) {
            try{
                 
                 $meal_template = \App\Models\Meal_template::find($id);

                 if(!empty($meal_template)){
                    $meal_template->update($input);
                    $result['message']='Template Updated Successfully.';
                 } else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Template Found.';
                 }   
               }catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }
             
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
 
    }

    public function deleteMealTemplate($id){
        $error = false;
        $status_code = 200;

        $result = [];
        if($id!=null){
            try{

                $meal_template = \App\Models\Meal_template::with('user')->where('id', $id)->first();
                
                
                if($meal_template->count() > 0){

                    // delete meals and foods of template
                    $meals = $meal_template->meals()->get();
                    if($meals){
                        foreach ($meals as $key => $meal) {
                            $meal_foods = $meal->meal_foods()->delete();
                            $meal->delete();
                        }
                    }
                    // end: delete meals and foods of template

                    // delete meals and foods of template
                    if($meal_template->user->count() > 0){
                        $users = $meal_template->user->pluck('id')->toArray();

                        $meal_template->user()->detach();

                        $default_meal_template = \App\Models\Meal_template::where('template_gender', $meal_template->template_gender)
                                                                    ->where('is_default', '1')
                                                                    ->first(); 
                                      
                        $default_meal_template->user()->attach($users);
                    }      

                    $meal_template_delete = $meal_template->delete();

                    if($meal_template_delete){

                        $result['message'] = "Template deleted successfully";
                    }
                    else{
                        $error = true;
                        $result['error_message'] = "Template not deleted successfully";
                    }
                }
                else{
                    $error = true;
                    $result['error_message'] = "No record found";   
                } 
                
            }
            catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }
        }
        else{
            $error = true;
            $result['error_message'] = "Request data is empty";   
        } 

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));       
    }

    public function createNewMeal(Request $request){

        $error = false;
        $status_code = 200;

        $result = [];
        $input = $request->all();
              
        if(!empty($input)){
            try{
                if(isset($input['user_agent']) && $input['user_agent']=="server"){
                    $input['guid'] = guid();
                    $input['timestamp'] = \Carbon\Carbon::now()->timestamp;

                }
                $input['meal_name'] = "Meal-".str_random(5);
                $input['meal_time'] = date("H:i:s");
                $input['meal_template_id'] = $input['meal_template_id'];
                $recommended_meal = \App\Models\Recommended_meal::where('meal_template_id', "=", $input['meal_template_id'])
                                                                ->where('id', '=', $input['new_meal_id'])
                                                                ->first();
                if($recommended_meal==null){
                    $create_recommended_meal = \App\Models\Recommended_meal::create($input);
                    if($create_recommended_meal){
                        $last_recommended_meal = \App\Models\Recommended_meal::find($create_recommended_meal->id);
                        $result['recommended_meal'] = $last_recommended_meal;
                        $result['message'] = "Recommended meal created successfully";
                    }
                    else{
                        $error = true;
                        $result['error_message'] = "Recommended meal not created"; 
                    }  
                }
                else{
                    $error = false;
                    $result['recommended_meal'] =$recommended_meal;
                    $result['meal_exist'] = false;  
                }
                
            }
            catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }
            
        }
        else{
            $error = true;
            $result['error_message'] = "Request data is empty";
        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
        

    }

    public function getRecommendedTypeFood(Request $request){
        $error = false;
        $status_code = 200;
        $result = [];

        try{
            $input = $request->all();

            $recommended_meal_id =$input['recommended_meal_id'];
            $item_id =$input['item_id'];

            $recommended_meal = \App\Models\Recommended_meal::where('id', '=', $recommended_meal_id)
                                                              ->first();
            if($recommended_meal != null){
                $gender = $recommended_meal->gender;

                if($gender=='Male'){
                    $md_recommended_foods = \App\Models\Recommended_food::with('nutritions')
                                                                            ->where('food_recommendation_type_id','=', $item_id)
                                                                            ->where('food_for', '<>', 'Female')
                                                                            ->get();
                }
                else if($gender=='Female'){
                    $md_recommended_foods = \App\Models\Recommended_food::with('nutritions')
                                                                ->where('food_recommendation_type_id','=', $item_id)
                                                                ->where('food_for', '<>', 'Male')
                                                                ->get();
                }

                if($md_recommended_foods->count()>0) {

                    foreach ($md_recommended_foods as $key => $foods_data) {

                        if ($foods_data->nutritions['nutrition_data']) {
                            $foods_data->nutritions['nutrition_data'] = convertStringToArray($foods_data->nutritions['nutrition_data']);
                        }

                        if ($foods_data->nutritions['serving_data']) {
                            $foods_data->nutritions['serving_data'] = convertStringToArray($foods_data->nutritions['serving_data']);
                        }




                    }
                }
                $result['recommended_foods'] = $md_recommended_foods;

            }

        }
        catch(\Exception $ex){
            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }

        return response()->json(['error' => $error,'response'=>$result],200)
            ->setCallback(\Request::input('callback'));
    }

    public function saveRecommendedMealFood(Request $request){
        $error = false;
        $status_code = 200;

        $result = [];
        $input = $request->all();
       // print_r($input);
       // exit;

        if(!empty($input)){
            try{

                $selectedFoods = $input['selectedFoods'];

                foreach ($selectedFoods as $key => $food){
                    $foodData =[];
                    $foodData['guid'] = guid();
                    $foodData['timestamp'] = \Carbon\Carbon::now()->timestamp;
                    $foodData['meal_id'] = $input['meal_id'];
                    $foodData['item_id'] = $food['food_recommendation_type_id'];
                    $foodData['nutrition_id'] = $food['nutrition_id'];
                    $foodData['calories'] = $food['calories'];
                    $foodData['serving_quantity'] = $food['serving_quantity'];
                    $foodData['serving_size_unit'] = $food['serving_size_unit'];
                    $foodData['no_of_servings'] = $food['no_of_servings'];
                    $foodData['serving_string'] = $food['serving_string'];
                    \App\Models\Recommended_meal_food::create($foodData);

                }


            }
            catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }
        else{
            $error = true;
            $result['error_message'] = "Request data is empty";
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
            ->setCallback(\Request::input('callback'));

    }
}
