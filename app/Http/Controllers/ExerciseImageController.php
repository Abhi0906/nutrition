<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Image;
use File;

class ExerciseImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $number_of_records = 50;
    public function index()
    {

        return view('exercise_image.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();

        if(!empty($input)) {
            try{
                
                $steps =[];
                $exercise_steps = $input['exercise_steps'];
                $i=1;
                if(count($exercise_steps) > 0){
                    foreach ($exercise_steps as $key => $value) {
                        $steps['step-'.$i] =$value['desc'];
                        $i++;
                    }
                 $input['steps'] = json_encode($steps);
                }
                
                $save_exercise = \App\Models\Exercise_image::create($input);
                $result['message']='Exercise Image Added Successfully.';
            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }
             
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
       
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
        $error =false;
        $status_code = 200;
        $server_timestamp = \Carbon\Carbon::now()->timestamp;
        if($id !="" && $id > 0){
            try{

                $exercise_image = \App\Models\Exercise_image::find($id);
                $exercise_image->steps = json_decode( $exercise_image->steps);
                if($exercise_image != null){
                    $result = $exercise_image;
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Exercise Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode(); 
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        } 
        return response()->json(['error' => $error,'timestamp'=>$server_timestamp,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $error =false;
        $status_code = 200;
       
        if($id !="" && $id > 0){
            try{
                $input = $request->all();
                $exercise_image = \App\Models\Exercise_image::find($id);
                if($exercise_image != null){

                    $steps =[];
                    $exercise_steps = $input['exercise_steps'];
                    $i=1;
                    if(count($exercise_steps) > 0){
                        foreach ($exercise_steps as $key => $value) {
                            $steps['step-'.$i] =$value['desc'];
                            $i++;
                        }
                     $input['steps'] = json_encode($steps);
                    }
                    
                    $save_exercise = $exercise_image->update($input);
                    $result['message']='Exercise Image Updated Successfully.';

                    
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Exercise Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode(); 
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        } 
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $error = false;
        $status_code = 200;
        $result =[];
        if(!empty($id)) {
            try{
                     
                $exercise_image = \App\Models\Exercise_image::find($id);
                                                          
                  if($exercise_image != null){
                   
                    $exercise_image->delete();

                    $result['message'] = 'Exercise Image Removed Successfully.';
                  }else{

                      $result['message'] = 'No Exercise found.';
                  }
                
                    
                  
                }catch(\Exception $ex){
                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback')); 
    }

    public function uploadImage(Request $request){
        $error = true;
       $file_name = "No File";
        if ($request->hasFile('exercise_image')) {
            $file_name = $request->file('exercise_image')->getClientOriginalName();
                        
            $destination_path = 'exercise-images/';
            $request->file('exercise_image')->move($destination_path, $file_name);

            $thumb_img = Image::make($destination_path.$file_name)->resize(300, 200);
            
            $thumb_path = "exercise-images/thumb";
            if(!File::exists($thumb_path)) {
                $result = File::makeDirectory($thumb_path, 0777, true, true);
            }

            $thumb_img->save($thumb_path.'/'.$file_name);

            $error = false;
        }

        return response()->json(['error' => $error,'file_name'=>$file_name],200)
                         ->setCallback(\Request::input('callback'));

    }

    public function removeImage($file_name){
         
        $destination_path = public_path().'/exercise-images/'.$file_name; 
        $thumb_destination_path = public_path().'/exercise-images/thumb/'.$file_name; 
       
        @unlink($destination_path);
        @unlink($thumb_destination_path);

        return response()->json(['error' => false,'file_name'=>$file_name],200)
                         ->setCallback(\Request::input('callback'));
    }
    
   

    public function getAllImages(Request $request){

         $input = $request->all();
         $perPage = (isset($input['itemPerPage']))?$input['itemPerPage']:$this->number_of_records;
        if(!empty($input)){
            $exercises = \App\Models\Exercise_image::search($input)
                            ->orderBy('name', 'asc')
                            ->paginate($perPage); 
                           
        }else{
          $exercises = \App\Models\Exercise_image::orderBy('name', 'asc')
                                                  ->paginate($perPage); 
         

        }
        return response()->json(['error' => false,'response'=>$exercises->toArray()],200)
                         ->setCallback(\Request::input('callback'));

    }
}
