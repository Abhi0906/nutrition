<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('template.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $template = \App\Models\Template::create($input);

        return response()->json(['error' => false,'response'=>$template],200)
                         ->setCallback(\Request::input('callback'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $error =false;
        $status_code = 200;
        $result=[];
        $server_timestamp = \Carbon\Carbon::now()->timestamp;
        if($id !="" && $id > 0){
            try{

                $template = \App\Models\Template::with('workouts')
                                                  ->where('id',$id)->first();

                if($template != null){
                    $template->workout_count = $template->workouts->count();
                    $template->video_count = 0;
                    $template->exercise_count = 0;
                    $workouts = $template->workouts;
                    $total_video_count =0;
                    $total_exercise_count=0;
                     foreach ($workouts as $key => $workout) {
                        $workout->video_count = 0;
                        $workout->exercise_count = 0;
                        $exercise_total_min=0;
                        $video_total_min=0;
                        $workout_exercise = $workout['workout_exercise'];
                        if($workout_exercise->count() > 0){
                          foreach ($workout_exercise as $key => $item) {
                            if($item->exercise_image_id != null){
                               $workout->exercise_count = $workout->exercise_count+1;
                               $exercise_total_min =  $exercise_total_min+ $item['exercise_images']['exercise_time'];
                            }
                             if($item->video_id != null){
                              $workout->video_count =  $workout->video_count+1;
                              $video_total_min =  $video_total_min+ $item['videos']['workout_time'];
                             }
                          }

                       }

                          $workout->exercise_total_mins =$exercise_total_min;
                          $workout->video_total_mins =$video_total_min;
                          $workout->total_mins = $workout->exercise_total_mins+ $workout->video_total_mins;

                         $total_video_count  =  $total_video_count+$workout->video_count;
                         $total_exercise_count  =  $total_exercise_count+$workout->exercise_count;

                         $workout->exercise_list = $this->getAssignedExercise($workout);
                         $config = json_decode($workout->pivot->config,1);
                         $sort_week_day = $config['week_day'];

                         if($sort_week_day != null){
                            sort($sort_week_day);
                         }

                        $workout->pivot->config = $sort_week_day;
                        unset($workout->workout_exercise);
                    }

                     $template->video_count = $total_video_count;
                     $template->exercise_count = $total_exercise_count;
                     unset($template['workouts']);
                     $result['workouts'] = $workouts;
                     $result['template'] = $template;
                   //dd($result->toArray());

                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Workout Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'timestamp'=>$server_timestamp,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $error = false;
        $template = \App\Models\Template::findOrFail($id);
        if($template != null){
            $input = $request->all();
            $result = $template->update($input);
        }else{
            $error = true;
            $result = "No Template Found.";
        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $error =false;
        $template = \App\Models\Template::findOrFail($id);
        if(!$template){
          $error= true;
        }else{
          $file_name = $template['image'];
          $destination_path = public_path().'/template-images/'.$file_name;
          @unlink($destination_path);
          $template->delete();
       }
        return response()->json(['error' =>$error],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function getTemplateWithWorkout(Request $request){
        $error = false;
        $templates =[];

        $input = $request->all();
        if(!empty($input)){
            $templates = \App\Models\Template::with('workouts')
                                               ->search($input)
                                               ->get();
        }else{
          $templates = \App\Models\Template::with('workouts')->get();
        }

        foreach ($templates as $key => $template) {
             $template->workout_count = $template->workouts->count();
             $template->video_count = 0;
             $template->exercise_count = 0;
             if(!empty($template['workouts'])){

                foreach ($template['workouts'] as $key => $workout) {

                     $config = json_decode($workout->pivot->config,1);
                     $sort_week_day = $config['week_day'];

                     if($sort_week_day != null){
                        sort($sort_week_day);
                     }

                    $workout->pivot->config = $sort_week_day;

                    $workout_exercise = $workout['workout_exercise'];
                   if($workout_exercise->count() > 0){
                      foreach ($workout_exercise as $key => $item) {
                        if($item->exercise_image_id != null){
                          $template->exercise_count = $template->exercise_count+1;
                        }
                         if($item->video_id != null){
                          $template->video_count =  $template->video_count+1;
                         }
                      }

                   }

                    //  if($workout->videos->count() > 0){

                    //     $template->video_count  =  $template->video_count+$workout->videos->count();
                    // }
                    // if($workout->exercise_images->count() > 0){
                    //     $template->exercise_count  =  $template->exercise_count+$workout->exercise_images->count();
                    // }
                }
                //unset($template['workouts']);

             }
        }

        if($templates != null) {
            $result = $templates;
        }else{
            $error = true;
            $result = "No Templates Found.";
        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function attachedWorkout(Request $request){

        $input = $request->all();
        $workout_id = $input['workout_id'];
        $template_id = $input['template_id'];
        $error = false;
        $template = \App\Models\Template::findOrFail($template_id);
        if($template != null){
           $result = $template->workouts()->attach($workout_id);
        }else{
            $error = true;
            $result = "No Template Found.";
        }
        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function deleteWorkout($template_id,$workout_id){

        $error = false;
        $template = \App\Models\Template::findOrFail($template_id);
        if($template != null){
           $result = $template->workouts()->detach($workout_id);
        }else{
            $error = true;
            $result = "No Template Found.";
        }
        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));

    }

    public function updateWorkout(Request $request){

        $input = $request->all();
        $workout_id = $input['workout_id'];
        $template_id = $input['template_id'];
        $config =null;
        if(isset($input['config'])){
          $config =json_encode(['week_day'=>$input['config']]);
        }

        $error = false;
        $template = \App\Models\Template::findOrFail($template_id);
        $template->days = $input['template_days'];
        $template->save();
        if($template != null){
           $template_workout = $template->workouts()->where('template_workout.workout_id',$workout_id )->first();
           $template_workout->pivot->start = $input['start'];
           $template_workout->pivot->end = $input['end'];
           $template_workout->pivot->config = $config;
           $result =  $template_workout->pivot->save();
        }else{
            $error = true;
            $result = "No Template Found.";
        }
        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));



    }

   private function getAssignedExercise($workout){

        $workout_exercise = $workout->workout_exercise;
        $exercise_collection = new \Illuminate\Database\Eloquent\Collection;
        if($workout_exercise->count() > 0){
           foreach ($workout_exercise as $key => $value) {

              if($value->exercise_image_id != null)
              {

                 $value->exercise_images->workout_type ='exercise_images';
                 $value->exercise_images->imageUrl = '/exercise-images/'.$value->exercise_images->image_name;
                 $exercise_collection->push($value->exercise_images);
              }

              if($value->video_id != null)
              {
                  $value->videos->workout_type ='videos';
                  $value->videos->pictures = json_decode($value->videos->pictures);
                  $value->videos->imageUrl = $value->videos->pictures->sizes[4]->link;
                  $exercise_collection->push($value->videos);
              }

            }

         }

       $result = $exercise_collection->toArray();
       return $result;

    }


    public function uploadImage(Request $request){

        $file_name = 'no file';
        if ($request->hasFile('image_file')) {
            $file_name = $request->file('image_file')->getClientOriginalName();
            $destination_path = 'template-images/';
            $request->file('image_file')->move($destination_path, $file_name);
        }

        return response()->json(['error' => false,'file_name'=>$file_name],200)
                         ->setCallback(\Request::input('callback'));

    }

    public function removeImage($file_name){

        $destination_path = public_path().'/template-images/'.$file_name;
        @unlink($destination_path);
        return response()->json(['error' => false,'file_name'=>$file_name],200)
                         ->setCallback(\Request::input('callback'));
    }

}
