<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DiaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('front.diary')->with('user_id',\Auth::User()->getUserInfo()["id"]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateDiaryNote(Request $request, $user_id){

        $error =false;
        $status_code = 200;
        if($user_id !="" && $user_id > 0){
            try{

                $input = $request->all();
                $user = \App\Models\User::find($user_id);
                if($user != null){
                   $user_food_note = \App\Models\User_diary_note::where('user_id',$user_id)
                                                                  ->where('log_date',$input['log_date'])
                                                                  ->first();
                   if($user_food_note != null){
                     $user_food_note->update($input);

                   }else{

                         $input['user_id'] = $user_id;
                        \App\Models\User_diary_note:: create($input);
                  }


                  $result['message']='Notes Updated successfully.';
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }
            }catch(\Exception $ex){
               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();
            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function calculateMonthlyCalories($user_id, Request $request){

        $result= [];
        $error =false;
        $status_code = 200;

        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::with(['profile'])->find($user_id);

                if($user != null){
                    $calories_goal = 0;
                    $user_calorie_goals = \App\Models\User_calories_goal::where('user_id',$user_id)->first();
                    if($user_calorie_goals != null){
                        $calories_goal = $user_calorie_goals->calorie_intake_g;
                    }else{

                        if($user->profile != null){
                          $calories_goal = $user->profile->calories;
                         }
                    }
                    $req_month = $request->get('req_month');
                    $req_year = $request->get('req_year');

                    $month = $req_month ? $req_month : date('m');
                    $year = $req_year ? $req_year : date('Y');



                    $nutrition_user = \App\Models\Nutrition_user::where('user_id',$user_id)
                                                         ->whereMonth('serving_date', '=', $month)
                                                         ->whereYear('serving_date', '=', $year)
                                                         ->selectRaw('serving_date, sum(calories) as sum')
                                                         ->groupBy('serving_date')
                                                         ->pluck('sum','serving_date');

                    $exercise_user = \App\Models\Exercise_user::where('user_id',$user_id)
                                                         ->whereMonth('exercise_date', '=', $month)
                                                         ->whereYear('exercise_date', '=', $year)
                                                         ->selectRaw('exercise_date, sum(calories_burned) as sum')
                                                         ->groupBy('exercise_date')
                                                         ->pluck('sum','exercise_date');


                    $water_intake_user = \App\Models\WaterIntake::where('user_id',$user_id)
                                                         ->whereMonth('log_date', '=', $month)
                                                         ->whereYear('log_date', '=', $year)
                                                         ->selectRaw('log_date, sum(log_water) as sum')
                                                         ->groupBy('log_date')
                                                         ->pluck('sum','log_date');

                    $temp_date = $year."-".$month."-01";
                    $month_day = date("t", strtotime($temp_date));
                    $data1 = array();

                    $data = [];
                    for($i=1; $i<=$month_day; $i++){


                        $total_cal = 0;
                        $total_water_intake = 0;

                        $month_date = strtotime($i."-".$month."-".$year);

                        foreach ($nutrition_user as $key1 => $nutrition_value) {

                            $nutrition_date = strtotime($key1);

                            if($month_date == $nutrition_date){
                                $total_cal = $nutrition_value;
                                break;
                            }
                        }

                        // exercise data loop
                        $total_cal_burned = 0;
                        foreach ($exercise_user as $key1 => $exercise_value) {
                              $exercise_date = strtotime($key1);
                              if($month_date == $exercise_date){
                                  $total_cal_burned = $exercise_value;
                                  break;
                              }
                        }

                        // end: exercise data loop

                        foreach ($water_intake_user as $key2 => $water_value) {

                            $waterlog_date = strtotime($key2);

                            if($month_date == $waterlog_date){
                                $total_water_intake = $water_value;
                                break;
                            }
                        }

                        $total_consumed = $total_cal;
                        /*$remaining_calories = $calories_goal - $total_consumed;*/

                        $actual_calories = $total_consumed - $total_cal_burned;
                        $remaining_calories = $calories_goal - $actual_calories;
                        $weekly_data_day_count = max( $total_consumed, $total_water_intake, $total_cal_burned );
                        if( $weekly_data_day_count > 0 ){

                            $data1['total_consumed'] = $total_consumed;

                            $data1['total_calorie_burned'] = $total_cal_burned;

                            $data1['water_intake'] = round($total_water_intake/8);
                            $data1['date'] = date('Y-m-d', $month_date);
                            $data1['day'] = date('d', $month_date);
                            $data1['month'] = date('m', $month_date);
                            $data1['year'] = date('Y', $month_date);

                            if($remaining_calories > 0){

                                $data1['diff_text'] = "Deficit";
                                $data1['diff_text_color'] = "#33CC33";

                            }
                            elseif($remaining_calories < 0){

                                $data1['diff_text'] = "Excess";
                                $data1['diff_text_color'] = "#FF1A1A";
                            }
                            else{

                                $data1['diff_text'] = "Neutral";
                                $data1['diff_text_color'] = "#FDE674";
                            }
                            $data1['diff'] = -$remaining_calories;
                            $data[] = $data1;

                        }

                    }

                    $result['date_wise'] = $data;
                    $result['average'] = $this->getAverageCalorie($user_id, $calories_goal);



                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function getAverageCalorie($user_id, $calorie_goal, Request $request){

        $result= [];
        $error =false;
        $status_code = 200;

        // get weekly average data
        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::with(['profile'])->find($user_id);

                if($user != null){

                    $req_month = $request->get('req_month');
                    $req_year = $request->get('req_year');

                    $month = $req_month ? $req_month : date('m');
                    $year = $req_year ? $req_year : date('Y');

                    $weekly_data = [];
                    $temp_date = $year."-".$month."-01";
                    $month_day = date("t", strtotime($temp_date));
                    $first_day_this_month = date('Y-m-d', strtotime('01-'.$month.'-'.$year)); // hard-coded '01' for first day
                    $last_day_this_month  = date('Y-m-d', strtotime($month_day.'-'.$month.'-'.$year));

                    $month_weeks = array();
                    for($date = $first_day_this_month; $date <= $last_day_this_month; $date = date('Y-m-d', strtotime($date. ' + 7 days')))
                    {
                        $week =  date('W', strtotime($date));
                        $year =  date('Y', strtotime($date));

                        $from = date("Y-m-d", strtotime("{$year}-W{$week}-0"));

                        if($from < $first_day_this_month) $from = $first_day_this_month;
                        $to = date("Y-m-d", strtotime("{$year}-W{$week}-6"));

                        if($to > $last_day_this_month) $to = $last_day_this_month;

                        $month_weeks[] = array("start_date" => $from, "end_date" => $to );

                    }

                    $week_no = 1;
                    $total_consumed_monthly = 0;
                    $total_exercise_monthly = 0;
                    $total_water_intake_monthly = 0;
                    for($j=0; $j < count($month_weeks); $j++){

                        $start_date = date('Y-m-d', strtotime($month_weeks[$j]['start_date']));
                        $end_date = date('Y-m-d', strtotime($month_weeks[$j]['end_date']));

                        $datetime1 = date_create($start_date);
                        $datetime2 = date_create($end_date);
                        $diff_days = date_diff($datetime1, $datetime2);
                        $diff_days = $diff_days->format('%d');
                        $diff_days = $diff_days +1;

                        $weekly_total_consumed = 0;

                        $weekly_current_calories = 0;

                        $total_cal_week_date = 0;


                        $nutrition_user_weekly = \App\Models\Nutrition_user::where('user_id',$user_id)
                                                                        ->whereBetween('serving_date', [$start_date, $end_date]);

                        $weekly_nutrition_count = $nutrition_user_weekly->where('calories', '<>', '0')->groupBy('serving_date')->get(); // is not null

                        $nutrition_user_weekly = $nutrition_user_weekly->selectRaw('serving_date, sum(calories) as sum')
                                                                        ->pluck('sum','serving_date');

                        $total_weekly_nutritions = 0;
                        foreach ($nutrition_user_weekly as $key2 => $value2) {
                            if(!empty($value2)){
                                $total_weekly_nutritions = $total_weekly_nutritions + $value2;

                            }
                        }

                        // Exercise code
                        $exercise_user_weekly = \App\Models\Exercise_user::where('user_id',$user_id)
                                                        ->whereBetween('exercise_date', [$start_date, $end_date]);


                        $user_exercise_count =  $exercise_user_weekly->where('calories_burned', '<>', '0')->groupBy('exercise_date')->get();


                        $exercise_user_weekly = $exercise_user_weekly->selectRaw('exercise_date, sum(calories_burned) as sum')
                                                        ->pluck('sum','exercise_date');

                        $total_cal_burned_weekly = 0;
                        foreach($exercise_user_weekly as $key=>$value)
                        {
                            $total_cal_burned_weekly += $value;
                        }
                        // end: Exercise code

                        // water intake
                        $water_intake_weekly = \App\Models\WaterIntake::where('user_id',$user_id)
                                                             ->whereBetween('log_date', [$start_date, $end_date]);

                        $weekly_intake_count = $water_intake_weekly->where('log_water', '<>', '0')->groupBy('log_date')->get(); // is not null

                        $water_intake_weekly = $water_intake_weekly->selectRaw('log_date, sum(log_water) as sum')
                                                                    ->pluck('sum','log_date');

                        $total_weekly_water_intake = 0;
                        foreach ($water_intake_weekly as $key3 => $value3) {
                            if(!empty($value3)){
                                $total_weekly_water_intake = $total_weekly_water_intake + $value3;
                            }
                        }
                        // end water intake

                        $weekly_data_day_count = max( sizeof($weekly_nutrition_count), sizeof($user_exercise_count) , $total_weekly_water_intake );

                        if($diff_days>0 && $weekly_data_day_count >0){

                            $total_consumed_monthly = $total_consumed_monthly + $total_weekly_nutritions;
                            $total_exercise_monthly = $total_exercise_monthly + $total_cal_burned_weekly;
                            $total_water_intake_monthly = $total_water_intake_monthly + $total_weekly_water_intake;

                            $weekly_remaining_calories = ( ($calorie_goal * $diff_days) - $total_weekly_nutritions + $total_cal_burned_weekly ) / $diff_days;

                            $weekly_data1 = array();

                            if( $weekly_remaining_calories && $total_weekly_nutritions ){

                                $weekly_data1['total_consumed_weekly_avg'] = round( $total_weekly_nutritions/$diff_days , 2 );
                                $weekly_data1['total_exercise_weekly_avg'] = round( $total_cal_burned_weekly/$diff_days , 2 );
                                $weekly_data1['total_weekly_water_intake_avg'] = round( ( $total_weekly_water_intake/8 ) / $diff_days , 1 );

                                if($weekly_remaining_calories>0){

                                    $weekly_data1['weekly_diff_text_avg'] = "Deficit";
                                    $weekly_data1['weekly_diff_text_class_avg'] = "text-deficit";

                                }
                                elseif($weekly_remaining_calories<0){

                                    $weekly_data1['weekly_diff_text_avg'] = "Excess";
                                    $weekly_data1['weekly_diff_text_class_avg'] = "text-excess";
                                }
                                else{

                                    $weekly_data1['weekly_diff_text_avg'] = "Neutral";
                                    $weekly_data1['weekly_diff_text_class_avg'] = "text-neutral";
                                }

                                $weekly_data1['weekly_diff_avg'] = - round($weekly_remaining_calories , 2 );

                                $weekly_data[$j+1] = $weekly_data1;
                            }else{
                               $weekly_data[$j+1] = [];
                            }
                        }
                        else{
                            $weekly_data[$j+1] = [];
                        }

                    }
                    //exit;
                    $result['week_wise_avg'] = $weekly_data;

                    $monthly_average = [];

                    $monthly_average['monthly_consumed_avg'] = round( ($total_consumed_monthly) / $month_day , 2);
                    $monthly_average['monthly_exercise_avg'] = round( ($total_exercise_monthly) / $month_day , 2);

                    $monthly_average['monthly_water_avg'] = round( ($total_water_intake_monthly/8) / $month_day , 1);
                    $monthly_remain_calorie = round( (($calorie_goal *$month_day) - $total_consumed_monthly + $total_exercise_monthly )/ $month_day , 2);

                    if($monthly_remain_calorie>0){

                        $monthly_average['monthly_diff_text_avg'] = "Deficit";
                        $monthly_average['monthly_diff_text_class_avg'] = "text-deficit";

                    }
                    elseif($weekly_remaining_calories<0){

                        $monthly_average['monthly_diff_text_avg'] = "Excess";
                        $monthly_average['monthly_diff_text_class_avg'] = "text-excess";
                    }
                    else{

                        $monthly_average['monthly_diff_text_avg'] = "Neutral";
                        $monthly_average['monthly_diff_text_class_avg'] = "text-neutral";
                    }

                    $monthly_average['monthly_diff_avg'] = -$monthly_remain_calorie;

                    $result['monthly_avg'] = $monthly_average;
                }
                else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        // end: get weekly average data
        return $result;
    }
}
