<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Requests\OrganizationCreateRequest;
use App\Http\Requests\OrganizationUpdateRequest;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Events\OrganizationSchema;
use App\Models\Organization;



class OrganizationController extends Controller
{

	protected $number_of_records = 50;

    public function index(){

       	return view('super_admin.organizations.index');
    }

    public function create()
    {
	    return view('super_admin.organizations.create');
    }

    public function store(OrganizationCreateRequest $request){

		$input = $request->all();
	    $organization = \App\Models\Organization::create($input);
	    event(new OrganizationSchema($organization->id,'CreateSchema'));
	    $user_data =[];
	    $user_data['user_type']='doctor';
        $user_data['username']= str_random(10);
        $user_data['created_by']= \Auth::id();
        $user_data['updated_by']= \Auth::id();
        $random_password = 'genemedics';
        $user_data['password'] = \Hash::make($random_password);
        $user_data['email']= $input['email'];
        $user_data['first_name']= $input['contact_first_name'];
        $user_data['last_name']= $input['contact_last_name'];
        $user_data['address']= $input['address'];
        $user_data['post_code']= $input['postcode'];
        $user_data['phone']= $input['telephone'];
        $user_data['created_at']= date('Y-m-d H:i:s');
        $user_data['updated_at']= date('Y-m-d H:i:s');
        \DB::table($organization->id.'_users')->insert($user_data);
	    return redirect('admin/organization');

    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(Organization $organization)
    {
       //$organization = \App\Models\Organization::find($id);
    	$user = \DB::table($organization->id.'_users')
                ->where('email', '>=', $organization->email)
                ->first();

      $organization->organization_user_id = $user->id;
      return view('super_admin.organizations.edit')->with('organization',$organization);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrganizationUpdateRequest $request, Organization $organization)
    {

            try{
                $input = $request->all();
                $save_org = $organization->update($input);

                 $user_data =[];
			     $user_data['updated_by']= \Auth::id();
		         $user_data['email']= $input['email'];
		         $user_data['first_name']= $input['contact_first_name'];
		         $user_data['last_name']= $input['contact_last_name'];
		         $user_data['address']= $input['address'];
		         $user_data['post_code']= $input['postcode'];
		         $user_data['phone']= $input['telephone'];
		         $user_data['updated_at']= date('Y-m-d H:i:s');
                 \DB::table($organization->id.'_users')
		            ->where('id',$input['organization_user_id'])
		            ->update($user_data);

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }

        return redirect('admin/organization');


    }


      /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $error = false;
        $status_code = 200;
        $result =[];
        if(!empty($id)) {
            try{

                $organization = \App\Models\Organization::find($id);

                  if($organization != null){

                    event(new OrganizationSchema($id,'DropSchema'));
                    $organization->delete();

                    $result['message'] = 'Organization Image Removed Successfully.';
                  }else{

                      $result['message'] = 'No Organization found.';
                  }



                }catch(\Exception $ex){
                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function uploadLogo(Request $request){

    	if ($request->hasFile('org_logo')) {
            $file_name = $request->file('org_logo')->getClientOriginalName();
            $destination_path = 'organization_logo/';
            $request->file('org_logo')->move($destination_path, $file_name);
        }

        return response()->json(['error' => false,'file_name'=>$file_name],200)
                         ->setCallback(\Request::input('callback'));

    }

    public function removeLogo($file_name){

        $destination_path = public_path().'/organization_logo/'.$file_name;
         @unlink($destination_path);
         return response()->json(['error' => false,'file_name'=>$file_name],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function checksubDomain(Request $request){
     	$input = $request->all();
     	$sub_domain = $input['sub_domain'];
        $id=$input['id'];
        if($id != "" && $id > 0 ){
           $organization = \App\Models\Organization::where('sub_domain',$sub_domain)
                                                        ->where('id','<>',$id)
                                                        ->first();
       }else{
        $organization = \App\Models\Organization::where('sub_domain',$sub_domain)->first();
       }

     	$isExist = false;
     	if($organization != null){
     		$isExist = true;
     	}
        return response()->json(['error' => false,'isExist'=>$isExist],200)
                         ->setCallback(\Request::input('callback'));

    }

    public function getOrganizationList(Request $request){

    	$input = $request->all();
        $perPage = (isset($input['itemPerPage']))?$input['itemPerPage']:$this->number_of_records;
        if(!empty($input) && isset($input['keyword'])){
            $organizations = \App\Models\Organization::search($input)
                            ->paginate($perPage);
        }else{
          $organizations = \App\Models\Organization::paginate($perPage);

        }
        return response()->json(['error' => false,'response'=>$organizations->toArray()],200)
                         ->setCallback(\Request::input('callback'));

    }

    public function impersonateOrganization($organization_id){

    	$organization = \App\Models\Organization::find($organization_id);
    	\Session::put('organization_id', $organization_id);
    	 \Session::put('super_admin_user', \Auth::id() );
    	$organization_user = \DB::table($organization->id.'_users')
						          ->where('email',$organization->email)
						          ->first();
    	$user = \App\Models\User::find($organization_user->id);
    	\Auth::login($user);
        return redirect('/dashboard');


    }

    public function stopImpersonate($user_id){

     \Session::forget('organization_id');
     \Session::forget('super_admin_user');
      $orig_user = \App\Models\User::find( $user_id );
    //  dd($orig_user);
      \Auth::login($orig_user);
      return redirect('/dashboard');
    }

}
