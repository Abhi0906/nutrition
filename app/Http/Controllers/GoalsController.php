<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class GoalsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('front.goals')->with('user_id',\Auth::User()->getUserInfo()['id']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
        $error =false;
        $status_code = 200;
        $date = date('Y-m-d');
        $result =[];
      //  echo $user_id; exit;

        if($user_id !="" && $user_id > 0){
            try{
                $user = \App\Models\User::with('profile')->find($user_id);


                if($user != null){

                  $user_calorie_goal = $this->getCalorieGoal($user_id, $date,$user);

                  $user_water_goal = $this->getWaterGoal($user_id, $date, $user);

                  $weight_goal = $this->getWeightGoal($user_id, $date, $user);

                  $bp_goal = $this->getBpGoal($user_id, $date, $user);

                  $bm_goal = $this->getBmGoal($user_id, $date);

                  $sleep_goal = $this->getSleepGoal($user_id, $date);

                  $result['calorie_goal'] = $user_calorie_goal;
                  $result['weight_goal'] = isset($weight_goal['weight_goal'])?$weight_goal['weight_goal']:"";
                  $result['bm_goal'] = $bm_goal;
                  $result['bp_goal'] = $bp_goal;
                  $result['water_goal'] = $user_water_goal;
                  $result['sleep_goal'] = $sleep_goal;
                  $result['last_sleep'] = isset($sleep_goal['last_sleep'])?$sleep_goal['last_sleep']:"";
                  $result['last_body_measurements'] = isset($bm_goal['last_body_measurements'])?$bm_goal['last_body_measurements']:"";
                  $result['last_blood_pressure'] = isset($bp_goal['last_blood_pressure'])?$bp_goal['last_blood_pressure']:"";
                  $result['last_pulse'] = isset($bp_goal['last_pulse'])?$bp_goal['last_pulse']:"";
                  $result['last_weight'] = isset($weight_goal['last_weight'])?$weight_goal['last_weight']:"";

                }
                else
                {
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function getCalorieGoal($user_id, $date,$user)
    {
      $user_calorie_goals = [];
      $calories_burned_percentage = 0;
      $calorie_intake_percentage = 0;

      $user_calorie_goals = \App\Models\User_calories_goal::where('user_id',$user_id)->first();

      if($user_calorie_goals != null){
        $user_calorie_goals->calorie_intake_g = (int) $user_calorie_goals->calorie_intake_g;
        $user_calorie_goals->calorie_burned_g = (int) $user_calorie_goals->calorie_burned_g;

        $total_calories_burned = \App\Models\Exercise_user::where('user_id',$user_id)
                                                    ->where('exercise_date',$date)
                                                    ->sum('calories_burned');

        $total_caloires = \App\Models\Nutrition_user::where('user_id',$user_id)
                                                    ->where('serving_date',$date)
                                                    ->sum('calories');

       if($total_caloires > 0){
          $calorie_intake_percentage = ($total_caloires/$user_calorie_goals['calorie_intake_g'])*100;
        }

        if($total_calories_burned > 0){
          $calories_burned_percentage = ($total_calories_burned/$user_calorie_goals['calorie_burned_g'])*100;
        }
        $user_calorie_goals->calorie_intake_percentage = floor($calorie_intake_percentage);
        $user_calorie_goals->calories_burned_percentage = floor($calories_burned_percentage);
      }else{
          $user_calorie_goals['calorie_intake_g']= $user->profile->calories;
          $total_caloires = \App\Models\Nutrition_user::where('user_id',$user_id)
                                                    ->where('serving_date',$date)
                                                    ->sum('calories');
          if($total_caloires > 0){
            $calorie_intake_percentage = ($total_caloires/$user_calorie_goals['calorie_intake_g'])*100;
          }
          $user_calorie_goals['calorie_intake_percentage'] = floor($calorie_intake_percentage);
      }

      return $user_calorie_goals;
    }

    public function getWaterGoal($user_id, $date, $user)
    {
      $user_water_goal = [];
      $water_logged_percentage = 0;
      $user_water_goal = \App\Models\User_water_goal::where('user_id',$user_id)->first();

      if($user_water_goal !=null){
          $user_water_goal->water_g = (int) $user_water_goal->water_g;
          $total_waterlog = \App\Models\WaterIntake::where('user_id',$user_id)
                                                ->where('log_date',$date)
                                                ->sum('log_water');
      if($total_waterlog > 0){
         $water_logged_percentage = ($total_waterlog/$user_water_goal['water_g'])*100;
      }
      $user_water_goal->water_logged_percentage = floor($water_logged_percentage);

      }else{
        $user_water_goal['water_g']= $user->profile->water_intake;
        $user_water_goal['water_logged_percentage'] = $water_logged_percentage;
      }
      return $user_water_goal;
    }

    public function getWeightGoal($user_id, $date, $user)
    {
      $weight_goal = [];
      $weight_percentage =0;
      $body_fat_percentage=0;
      $last_weight_data=null;

      $user_weight_goal = \App\Models\User_weight_goal::where('user_id',$user_id)->first();

      if($user_weight_goal != null){
          $user_weight_goal->weight_g = (int) $user_weight_goal->weight_g;
          $user_weight_goal->body_fat_g = (int) $user_weight_goal->body_fat_g;
          $last_weight_data = \App\Models\User_weight::where('user_id',$user_id)
                                                          ->orderBy('log_date','desc')
                                                          ->orderBy('id','desc')
                                                          ->first();

        if($last_weight_data != null){
          $weight_start_date =$user_weight_goal->weight_start_date;
          $body_fat_start_date =$user_weight_goal->body_fat_start_date;

          $start_weight_data = $user_weight_goal->start_weight;
          $user_weight_goal->weight_percentage = $weight_percentage;
          $user_weight_goal->current_body_fat = $last_weight_data->body_fat;
          $user_weight_goal->body_fat_percentage = $body_fat_percentage;
          if($start_weight_data != null){
              $weight_percentage = calculatePercent($start_weight_data,$last_weight_data->current_weight,$user_weight_goal->weight_g);
              $user_weight_goal->weight_percentage = $weight_percentage;
          }

          $start_body_fat_data = $user_weight_goal->start_body_fat;

          if($start_body_fat_data != null){
            $body_fat_percentage = calculatePercent($start_body_fat_data,$last_weight_data->body_fat,$user_weight_goal->body_fat_g);
            $user_weight_goal->body_fat_percentage = $body_fat_percentage;

          }
        }
      }

      $weight_goal['weight_goal']=$user_weight_goal;
      $weight_goal['weight_goal']['height_in_feet']=$user->profile->height_in_feet;;
      $weight_goal['weight_goal']['height_in_inch']=$user->profile->height_in_inch;
      $weight_goal['weight_goal']['baseline_weight']=$user->profile->baseline_weight;
      $weight_goal['weight_goal']['recommended_daily_calorie']=$user->profile->calories;
      $birth_date =  \Carbon\Carbon::createFromFormat('Y-m-d',$user->birth_date);
      $weight_goal['weight_goal']['age']= $birth_date->age;
      $weight_goal['weight_goal']['gender']= ucfirst($user->gender);
      $weight_goal['last_weight'] = $last_weight_data;

      return $weight_goal;
    }

    public function getBpGoal($user_id, $date, $user)
    {
      $result = [];
      $pulse_percentage=0;
      $diastolic_percentage=0;
      $systolic_percentage=0;

      $bp_goal = \App\Models\User_bp_pulse_goal::where('user_id',$user_id)->get();

      if($bp_goal->count() > 0){

        foreach ($bp_goal as $key => $bp_data) {
            //$bp_goal['goal_date'] = $bp_data['0']['goal_date'];
            $result['goal_date'] = $bp_data['goal_date'];
            if($bp_data->bp_type == 'systolic'){
                $result['systolic_g'] = (int) $bp_data->goal;
                 $start_date = $bp_data->start_date;
                 $systolic_latest_reading = \App\Models\Blood_pressure_pulse::where('user_id',$user_id)
                                                                  ->where('log_date','>=',$start_date)
                                                                  ->latest()
                                                                  ->first();
                 if($systolic_latest_reading !=null){
                   $systolic_percentage = calculateBpPercent($systolic_latest_reading->systolic,$bp_data->goal);
                 }
                 $result['systolic_percentage'] = floor($systolic_percentage);

            }else if($bp_data->bp_type == 'diastolic'){
                 $result['diastolic_g'] = (int) $bp_data->goal;
                 $start_date = $bp_data->start_date;
                 $diastolic_latest_reading = \App\Models\Blood_pressure_pulse::where('user_id',$user_id)
                                                                  ->where('log_date','>=',$start_date)
                                                                  ->latest()
                                                                  ->first();
                  if($diastolic_latest_reading != null){
                    $diastolic_percentage = calculateBpPercent($diastolic_latest_reading->diastolic,$bp_data->goal);
                  }
                 $result['diastolic_percentage'] = floor($diastolic_percentage);

            }else if($bp_data->bp_type == 'heart_rate'){
                 $result['heart_rate_g'] = (int) $bp_data->goal;
                 $start_date = $bp_data->start_date;
                 $pulse_latest_reading = \App\Models\User_pulse::where('user_id',$user_id)
                                                                  ->where('log_date','>=',$start_date)
                                                                  ->latest()
                                                                  ->first();
                  if($pulse_latest_reading != null){
                    $pulse_percentage = calculateBpPercent($pulse_latest_reading->pulse,$bp_data->goal);
                  }
                 $result['pulse_percentage'] = floor($pulse_percentage);
            }
        }
      }
      $result['last_blood_pressure'] = $user->blood_pressure_pulse()->orderBy('log_date', 'desc')->first(['systolic', 'diastolic', 'log_date']);

      $result['last_pulse'] = $user->user_pulse()->orderBy('log_date', 'desc')->first(['pulse', 'log_date']);
      return $result;
    }

    public function getBmGoal($user_id, $date)
    {
      $result=[];
      $neck_percentage=0;
      $waist_percentage=0;
      $arms_percentage=0;
      $hips_percentage=0;
      $legs_percentage=0;
      $last_measurement_data=null;
      $bm_goal = \App\Models\User_body_measurement_goal::where('user_id',$user_id)->get();

      if($bm_goal->count() > 0){

         $last_measurement_data = \App\Models\User_body_measurements::where('user_id',$user_id)
                                                                                  ->orderBy('log_date','desc')
                                                                                  ->orderBy('id','desc')
                                                                                 ->first();

           $result['goal_date'] = $bm_goal['0']['goal_date'];
           foreach ($bm_goal as $key => $bm_data) {
              if($bm_data->body_measurement_type == 'neck'){

                  $result['neck_g'] = (int) $bm_data->goal;
                  $start_date = $bm_data->start_date;

                   if($bm_data->start_measurement != null && $last_measurement_data != null){
                      $neck_percentage = calculatePercent($bm_data->start_measurement,$last_measurement_data->neck,$bm_data->goal);
                      $result['neck_percentage'] = $neck_percentage;
                   }

                  $result['neck_percentage'] = $neck_percentage;

              }else if($bm_data->body_measurement_type == 'arms'){
                   $result['arms_g'] = (int) $bm_data->goal;
                   $start_date = $bm_data->start_date;

                   if($bm_data->start_measurement != null && $last_measurement_data != null){
                      $arms_percentage = calculatePercent($bm_data->start_measurement,$last_measurement_data->arms,$bm_data->goal);
                      $result['arms_percentage'] = $arms_percentage;
                   }

                  $result['arms_percentage'] = $arms_percentage;

              }else if($bm_data->body_measurement_type == 'waist'){

                   $result['waist_g'] = (int) $bm_data->goal;
                   $start_date = $bm_data->start_date;

                   if($bm_data->start_measurement != null && $last_measurement_data != null){
                      $waist_percentage = calculatePercent($bm_data->start_measurement,$last_measurement_data->waist,$bm_data->goal);
                      $result['waist_percentage'] = $waist_percentage;
                   }

                  $result['waist_percentage'] = $waist_percentage;

              }else if($bm_data->body_measurement_type == 'hips'){
                   $result['hips_g'] = (int) $bm_data->goal;
                   $start_date = $bm_data->start_date;

                   if($bm_data->start_measurement != null && $last_measurement_data != null){
                      $hips_percentage = calculatePercent($bm_data->start_measurement,$last_measurement_data->hips,$bm_data->goal);
                      $result['hips_percentage'] = $hips_percentage;
                   }

                  $result['hips_percentage'] = $hips_percentage;

              }else if($bm_data->body_measurement_type == 'legs'){
                  $result['legs_g'] = (int) $bm_data->goal;
                  $start_date = $bm_data->start_date;
                  if($bm_data->start_measurement != null && $last_measurement_data != null){
                      $legs_percentage = calculatePercent($bm_data->start_measurement,$last_measurement_data->legs,$bm_data->goal);
                      $result['legs_percentage'] = $legs_percentage;
                   }

                  $result['legs_percentage'] = $legs_percentage;

              }
           }
           $result['last_body_measurements'] = $last_measurement_data;

      }
      return $result;
    }

    public function getSleepGoal($user_id, $date)
    {
      $sleep_goal = [];
      $sleep_percentage = 0;

      $user_sleep_goal = \App\Models\User_sleep_goal::where('user_id',$user_id)->first();

      if($user_sleep_goal != null){

        $user_sleep_goal->goal = $user_sleep_goal->goal;

        $sleep_goal = decimalToHours($user_sleep_goal->goal);

        $sleep_goal = array_merge($user_sleep_goal->toArray(), $sleep_goal);

        $sleep_progress = new \App\Common\Sleep_progress($user_sleep_goal);
        $sleep_goal['last_sleep'] = $sleep_progress->getSleepProgress('last_sleep');

      }
      return $sleep_goal;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user_id)
    {

        $error =false;
        $status_code = 200;
        if($user_id !="" && $user_id > 0){
            try{
            }catch(\Exception $ex){

            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateCaloireGoal(Request $request, $user_id){

        $error =false;
        $status_code = 200;
        if($user_id !="" && $user_id > 0){
            try{

                $input = $request->all();
                $user = \App\Models\User::find($user_id);
                $input['created_by'] = $user_id;
                $input['updated_by'] = $user_id;
                //check session
                if(Session::has('orig_user') ){
                  $input['created_by'] = Session::get( 'orig_user' );
                  $input['updated_by'] = Session::get( 'orig_user' );
                }
                if($user != null){
                   $user_calories_goal = \App\Models\User_calories_goal::where('user_id',$user_id)->first();
                   if($user_calories_goal != null){
                     $user_calories_goal->update($input);

                   }else{

                         $input['user_id'] = $user_id;
                        \App\Models\User_calories_goal:: create($input);
                  }

                  $profile['calories'] = isset($input['calorie_intake_g']) ? $input['calorie_intake_g']: $user->profile->calories;
                  $user->profile()->update($profile);
                  $result['message']='Calorie Goals Updated successfully.';
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }
            }catch(\Exception $ex){
               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();
            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function updateWaterGoal(Request $request, $user_id){

        $error =false;
        $status_code = 200;
        if($user_id !="" && $user_id > 0){
            try{

                $input = $request->all();
                $input['created_by'] = $user_id;
                $input['updated_by'] = $user_id;
                //check session
                if(Session::has('orig_user') ){
                  $input['created_by'] = Session::get( 'orig_user' );
                  $input['updated_by'] = Session::get( 'orig_user' );

                }
                $user = \App\Models\User::find($user_id);
                if($user != null){
                   $user_water_goal = \App\Models\User_water_goal::where('user_id',$user_id)->first();
                   if($user_water_goal != null){
                     $user_water_goal->update($input);

                   }else{

                         $input['user_id'] = $user_id;
                        \App\Models\User_water_goal:: create($input);
                  }

                  $profile['water_intake'] = isset($input['water_g'])?$input['water_g']:$user->profile->water_intake;
                  $user->profile()->update($profile);
                  $result['message']='Water Goals Updated successfully.';
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }
            }catch(\Exception $ex){
               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();
            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function updateBodyMeasurementGoal(Request $request, $user_id){

        $error =false;
        $status_code = 200;
        if($user_id !="" && $user_id > 0){
            try{

                $input = $request->all();
                $user = \App\Models\User::find($user_id);
                if($user != null){
                   $neck_goal = $input['neck_g'];
                   $arms_goal = $input['arms_g'];
                   $waist_goal = $input['waist_g'];
                   $hips_goal = $input['hips_g'];
                   $legs_goal = $input['legs_g'];
                   $last_measurement_data = \App\Models\User_body_measurements::where('user_id',$user_id)
                                                                                              ->orderBy('log_date','desc')
                                                                                              ->orderBy('id','desc')
                                                                                              ->first();

                   if($neck_goal !=""){
                     $this->saveNeckGoal($user_id,$input,$last_measurement_data);
                   }
                   if($arms_goal !=""){
                     $this->saveArmsGoal($user_id,$input,$last_measurement_data);
                   }
                   if($waist_goal !=""){
                     $this->saveWaistGoal($user_id,$input,$last_measurement_data);
                   }
                   if($hips_goal !=""){
                     $this->saveHipsGoal($user_id,$input,$last_measurement_data);
                   }
                   if($legs_goal !=""){
                     $this->saveLegsGoal($user_id,$input,$last_measurement_data);
                   }

                  $result['message']='Body Measurement Goals Updated successfully.';
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }
            }catch(\Exception $ex){
               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();
            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }


    public function updateWeightGoal(Request $request, $user_id){

        $error =false;
        $status_code = 200;
        if($user_id !="" && $user_id > 0){
            try{

                $input = $request->all();
                $user = \App\Models\User::find($user_id);

                $input['created_by'] = $user_id;
                $input['updated_by'] = $user_id;
                //check session
                if(Session::has('orig_user') ){
                  $input['created_by'] = Session::get( 'orig_user' );
                  $input['updated_by'] = Session::get( 'orig_user' );

                }


                 if($user != null){

                   $user_weight_goal = \App\Models\User_weight_goal::where('user_id',$user_id)->first();
                    if($user_weight_goal != null){

                        if($user_weight_goal->weight_g != null && $user_weight_goal->weight_g != $input['weight_g']){

                          $input['weight_start_date'] = date('Y-m-d');
                          $input['start_weight'] =  isset($input['baseline_weight']) ? $input['baseline_weight'] : $user->profile->baseline_weight;
                        }

                        if($user_weight_goal->body_fat_g != null && $user_weight_goal->body_fat_g != $input['body_fat_g']){

                           $input['body_fat_start_date'] = date('Y-m-d');
                           $input['start_body_fat'] =   isset($input['current_body_fat']) ? $input['current_body_fat'] : 0;
                        }
                       // $input['start_body_fat'] =  isset($input['current_body_fat']) ? $input['current_body_fat'] : 0;
                        unset($input['current_body_fat']);

                        $user_weight_goal->update($input);


                   }else{

                     $input['user_id'] = $user_id;
                     $input['weight_start_date'] = date('Y-m-d');
                     $input['body_fat_start_date'] = date('Y-m-d');
                     $input['start_weight'] =  isset($input['baseline_weight']) ? $input['baseline_weight'] : $user->profile->baseline_weight;
                     $input['current_weight'] =  isset($input['baseline_weight']) ? $input['baseline_weight'] : $user->profile->baseline_weight;
                     $input['start_body_fat'] =   isset($input['current_body_fat']) ? $input['current_body_fat'] : 0;
                     $input['current_body_fat'] =  isset($input['current_body_fat']) ? $input['current_body_fat'] : 0;
                     \App\Models\User_weight_goal:: create($input);
                   }

                  $profile['baseline_weight'] = isset($input['baseline_weight']) ? $input['baseline_weight'] : $user->profile->baseline_weight;
                  $profile['height_in_feet'] = isset($input['height_in_feet'])?$input['height_in_feet']:$user->profile->height_in_feet;
                  $profile['height_in_inch'] = isset($input['height_in_inch'])?$input['height_in_inch']:$user->profile->height_in_inch;
                  $user->profile()->update($profile);
                  $result['message']='Weight Goals Updated successfully.';

                 }else{
                    $error = true;
                     $status_code = 404;
                     $result['message']='No Patient Found.';
                 }
            }catch(\Exception $ex){
               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();
            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function updateBpGoal(Request $request, $user_id){

        $error =false;
        $status_code = 200;
        if($user_id !="" && $user_id > 0){
            try{

                $input = $request->all();
                $user = \App\Models\User::find($user_id);

                if($user != null){
                   $systolic_goal = $input['systolic_g'];
                   $diastolic_goal = $input['diastolic_g'];
                   $pulse_goal = $input['heart_rate_g'];

                   if($systolic_goal !=""){
                     $this->saveSystolicGoal($user_id,$input);
                   }
                   if($diastolic_goal !=""){
                     $this->saveDiastolicGoal($user_id,$input);
                   }
                   if($pulse_goal !=""){
                     $this->savePulseGoal($user_id,$input);
                   }


                  $result['message']='BP Pulse Goals Updated successfully.';
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }
            }catch(\Exception $ex){
               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();
            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function updateSleepGoal(Request $request, $user_id){

        $error =false;
        $status_code = 200;
        if($user_id !="" && $user_id > 0){
            try{

                $input = $request->all();
                $user = \App\Models\User::find($user_id);
                $input['created_by'] = $user_id;
                $input['updated_by'] = $user_id;
                //check session
                if(Session::has('orig_user') ){
                  $input['created_by'] = Session::get( 'orig_user' );
                  $input['updated_by'] = Session::get( 'orig_user' );

                }

                if($user != null){
                   $user_sleep_goal = \App\Models\User_sleep_goal::where('user_id',$user_id)->first();
                   if($user_sleep_goal != null){

                      if($user_sleep_goal->goal != null && $user_sleep_goal->goal != $input['goal']){
                        $input['start_date'] = date('Y-m-d');
                      }
                      $user_sleep_goal->update($input);

                   }else{

                         $input['user_id'] = $user_id;
                         $input['start_date'] = date('Y-m-d');
                        \App\Models\User_sleep_goal:: create($input);
                  }


                  $result['message']='Sleep Goals Updated successfully.';
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }
            }catch(\Exception $ex){
               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();
            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }



    private function saveNeckGoal($user_id,$data,$last_measurement){

        $neck_data =[];
        $goal = $data['neck_g'];
        $neck_data['goal'] = $goal;
        $neck_data['goal_date']=$data['goal_date'];
        $neck_data['created_by'] = $user_id;
        $neck_data['updated_by'] = $user_id;
        //check session
        if(Session::has('orig_user') ){
          $neck_data['created_by'] = Session::get( 'orig_user' );
          $neck_data['updated_by'] = Session::get( 'orig_user' );

        }
        $start_measurement = null;
        if($last_measurement != null){
           $start_measurement = $last_measurement->neck;
        }
        $user_bm_neck_goal = \App\Models\User_body_measurement_goal::where('user_id',$user_id)
                                                                    ->where('body_measurement_type','neck')
                                                                    ->first();
        if($user_bm_neck_goal != null){

            if($user_bm_neck_goal->goal != null && $user_bm_neck_goal->goal != $goal){
              $neck_data['start_date'] = date('Y-m-d');
              $neck_data['start_measurement'] = $start_measurement;
            }

              $user_bm_neck_goal->update($neck_data);

        }else{

            $neck_data['user_id'] = $user_id;
            $neck_data['body_measurement_type'] = 'neck';
            $neck_data['start_date'] = date('Y-m-d');
            \App\Models\User_body_measurement_goal:: create($neck_data);
        }

    }

    private function saveArmsGoal($user_id,$data,$last_measurement){

        $arms_data =[];
        $goal = $data['arms_g'];
        $arms_data['goal'] = $goal;
        $arms_data['goal_date']=$data['goal_date'];

        $start_measurement = null;
        if($last_measurement != null){
           $start_measurement = $last_measurement->arms;
        }
        $arms_data['created_by'] = $user_id;
        $arms_data['updated_by'] = $user_id;
        //check session
        if(Session::has('orig_user') ){
          $arms_data['created_by'] = Session::get( 'orig_user' );
          $arms_data['updated_by'] = Session::get( 'orig_user' );

        }
        $user_bm_arms_goal = \App\Models\User_body_measurement_goal::where('user_id',$user_id)
                                                                    ->where('body_measurement_type','arms')
                                                                    ->first();
        if($user_bm_arms_goal != null){

            if($user_bm_arms_goal->goal != null && $user_bm_arms_goal->goal != $goal){
              $arms_data['start_date'] = date('Y-m-d');
              $arms_data['start_measurement'] = $start_measurement;
            }

              $user_bm_arms_goal->update($arms_data);

        }else{

            $arms_data['user_id'] = $user_id;
            $arms_data['body_measurement_type'] = 'arms';
            $arms_data['start_date'] = date('Y-m-d');
            \App\Models\User_body_measurement_goal:: create($arms_data);
        }

    }

    private function saveWaistGoal($user_id,$data,$last_measurement){

        $waist_data =[];
        $goal = $data['waist_g'];
        $waist_data['goal'] = $goal;
        $waist_data['goal_date']=$data['goal_date'];
        $start_measurement = null;
        if($last_measurement != null){
           $start_measurement = $last_measurement->waist;
        }
        $waist_data['created_by'] = $user_id;
        $waist_data['updated_by'] = $user_id;
        //check session
        if(Session::has('orig_user') ){
          $waist_data['created_by'] = Session::get( 'orig_user' );
          $waist_data['updated_by'] = Session::get( 'orig_user' );

        }
        $user_bm_waist_goal = \App\Models\User_body_measurement_goal::where('user_id',$user_id)
                                                                    ->where('body_measurement_type','waist')
                                                                    ->first();
        if($user_bm_waist_goal != null){

            if($user_bm_waist_goal->goal != null && $user_bm_waist_goal->goal != $goal){
              $waist_data['start_date'] = date('Y-m-d');
               $waist_data['start_measurement'] = $start_measurement;
            }

            $user_bm_waist_goal->update($waist_data);

        }else{

            $waist_data['user_id'] = $user_id;
            $waist_data['body_measurement_type'] = 'waist';
            $waist_data['start_date'] = date('Y-m-d');
            \App\Models\User_body_measurement_goal:: create($waist_data);
        }

    }

    private function saveHipsGoal($user_id,$data,$last_measurement){

        $hips_data =[];
        $goal = $data['hips_g'];
        $hips_data['goal'] = $goal;
        $hips_data['goal_date']=$data['goal_date'];
        $user_bm_hips_goal = \App\Models\User_body_measurement_goal::where('user_id',$user_id)
                                                                    ->where('body_measurement_type','hips')
                                                                    ->first();
        $start_measurement = null;
        if($last_measurement != null){
           $start_measurement = $last_measurement->hips;
        }
        $hips_data['created_by'] = $user_id;
        $hips_data['updated_by'] = $user_id;
        //check session
        if(Session::has('orig_user') ){
          $hips_data['created_by'] = Session::get( 'orig_user' );
          $hips_data['updated_by'] = Session::get( 'orig_user' );

        }
        if($user_bm_hips_goal != null){

            if($user_bm_hips_goal->goal != null && $user_bm_hips_goal->goal != $goal){
              $hips_data['start_date'] = date('Y-m-d');
              $hips_data['start_measurement'] = $start_measurement;
            }

            $user_bm_hips_goal->update($hips_data);

        }else{

            $hips_data['user_id'] = $user_id;
            $hips_data['body_measurement_type'] = 'hips';
            $hips_data['start_date'] = date('Y-m-d');
            \App\Models\User_body_measurement_goal:: create($hips_data);
        }

    }

    private function saveLegsGoal($user_id,$data,$last_measurement){

        $legs_data =[];
        $goal = $data['legs_g'];
        $legs_data['goal'] = $goal;
        $legs_data['goal_date']=$data['goal_date'];
        $start_measurement = null;
        if($last_measurement != null){
           $start_measurement = $last_measurement->legs;
        }
        $legs_data['created_by'] = $user_id;
        $legs_data['updated_by'] = $user_id;
        //check session
        if(Session::has('orig_user') ){
          $legs_data['created_by'] = Session::get( 'orig_user' );
          $legs_data['updated_by'] = Session::get( 'orig_user' );

        }
        $user_bm_legs_goal = \App\Models\User_body_measurement_goal::where('user_id',$user_id)
                                                                    ->where('body_measurement_type','legs')
                                                                    ->first();
        if($user_bm_legs_goal != null){

            if($user_bm_legs_goal->goal != null && $user_bm_legs_goal->goal != $goal){
              $legs_data['start_date'] = date('Y-m-d');
               $legs_data['start_measurement'] =$start_measurement;;
            }

            $user_bm_legs_goal->update($legs_data);

        }else{

            $legs_data['user_id'] = $user_id;
            $legs_data['body_measurement_type'] = 'legs';
            $legs_data['start_date'] = date('Y-m-d');
            \App\Models\User_body_measurement_goal:: create($legs_data);
        }

    }

    private function saveSystolicGoal($user_id,$data){

        $systolic_data =[];

        $systolic_data['goal'] = $data['systolic_g'];

        $systolic_data['created_by'] = $user_id;
        $systolic_data['updated_by'] = $user_id;
        //check session
        if(Session::has('orig_user') ){
          $systolic_data['created_by'] = Session::get( 'orig_user' );
          $systolic_data['updated_by'] = Session::get( 'orig_user' );

        }
        $systolic_data['goal_date']=$data['goal_date'];
        $user_systolic_goal = \App\Models\User_bp_pulse_goal::where('user_id',$user_id)
                                                                    ->where('bp_type','systolic')
                                                                    ->first();
        if($user_systolic_goal != null){

            if($user_systolic_goal->goal != null && $user_systolic_goal->goal != $data['systolic_g']){
              $systolic_data['start_date'] = date('Y-m-d');
            }

            $user_systolic_goal->update($systolic_data);

        }else{

            $systolic_data['user_id'] = $user_id;
            $systolic_data['bp_type'] = 'systolic';
            $systolic_data['start_date'] = date('Y-m-d');
            \App\Models\User_bp_pulse_goal:: create($systolic_data);
        }

    }

    private function saveDiastolicGoal($user_id,$data){

        $diastolic_data =[];

        $diastolic_data['goal'] = $data['diastolic_g'];

        $diastolic_data['goal_date']=$data['goal_date'];

        $diastolic_data['created_by'] = $user_id;
        $diastolic_data['updated_by'] = $user_id;
        //check session
        if(Session::has('orig_user') ){
          $diastolic_data['created_by'] = Session::get( 'orig_user' );
          $diastolic_data['updated_by'] = Session::get( 'orig_user' );
        }

        $user_diastolic_goal = \App\Models\User_bp_pulse_goal::where('user_id',$user_id)
                                                                    ->where('bp_type','diastolic')
                                                                    ->first();
        if($user_diastolic_goal != null){

            if($user_diastolic_goal->goal != null && $user_diastolic_goal->goal != $data['diastolic_g']){
              $diastolic_data['start_date'] = date('Y-m-d');
            }

            $user_diastolic_goal->update($diastolic_data);

        }else{

            $diastolic_data['user_id'] = $user_id;
            $diastolic_data['bp_type'] = 'diastolic';
            $diastolic_data['start_date'] = date('Y-m-d');
            \App\Models\User_bp_pulse_goal:: create($diastolic_data);
        }

    }


    private function savePulseGoal($user_id,$data){

        $pulse_data =[];

        $pulse_data['goal'] = $data['heart_rate_g'];
        $pulse_data['goal_date']=$data['goal_date'];

        $pulse_data['created_by'] = $user_id;
        $pulse_data['updated_by'] = $user_id;
        //check session
        if(Session::has('orig_user') ){
          $pulse_data['created_by'] = Session::get( 'orig_user' );
          $pulse_data['updated_by'] = Session::get( 'orig_user' );
        }


        $user_pulse_goal = \App\Models\User_bp_pulse_goal::where('user_id',$user_id)
                                                                    ->where('bp_type','heart_rate')
                                                                    ->first();
        if($user_pulse_goal != null){

            if($user_pulse_goal->goal != null && $user_pulse_goal->goal != $data['heart_rate_g']){
              $pulse_data['start_date'] = date('Y-m-d');
            }

            $user_pulse_goal->update($pulse_data);

        }else{

            $pulse_data['user_id'] = $user_id;
            $pulse_data['bp_type'] = 'heart_rate';
            $pulse_data['start_date'] = date('Y-m-d');
            \App\Models\User_bp_pulse_goal:: create($pulse_data);
        }

    }

}
