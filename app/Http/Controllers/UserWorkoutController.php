<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class UserWorkoutController extends Controller
{
    public function index()
    {
    	$user_templates_info = [];

    	//$id = \Auth::user()->getUserInfo()["id"];
      $user = \App\Models\User::find(\Auth::user()->__get('id'));
    	$user_templates = $user->user_template()->get();

    	if($user_templates){
    		$templates_unique = $user_templates->unique();

	    	foreach ($templates_unique as $key => $template) {
	    		$user_templates_info[] = $template->pivot->toArray();
	    	}
    	}

        return view('front.user_workout.index')->with('user_templates_info', $user_templates_info);
    }

    public function clearAllMyWorkouts($user_id){

        $error = false;
        $status_code = 200;
        $result =[];

        if(!empty($user_id)){
            try{
                $user = \App\Models\User::find($user_id);
                if(!empty($user)){
                    $user_workouts = $user->workouts()->get();
                    if($user_workouts->count() > 0){
                        $workouts_ids = [];
                        foreach ($user_workouts as $key => $workout) {
                            $workouts_ids[] =  $workout->id;
                            $workout_exercise = $workout->workout_exercise()->delete();
                        }

                        $user->workouts()->detach();

                        $delete_workouts = \App\Models\Workout::whereIn('id', $workouts_ids)->forceDelete();

                        if($delete_workouts){
                            $result['message'] = 'All My Workouts are cleared successfully. ';
                        }
                        else{
                            $result['error_message'] = 'Workouts are not cleared successfully. ';
                        }
                    }
                    else{
                        $result['message'] = 'No Workouts exists. ';
                    }

                }
                else{
                    $error = true;
                    $status_code = 400;
                    $result['message']='Request data is empty';
                }
            }catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }
        else{
            $error = true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function removeMyworkout(Request $request, $user_id){

        $error = false;
        $status_code = 200;
        $result = [];
        $input = $request->all();

        $w_id = $input['w_id'];
        $opt = $input['opt'];
        $dates = $input['dates'];

        if(!empty($user_id) && !empty($w_id)){
            try{
                $user = \App\Models\User::find($user_id);
                if(!empty($user)){

                    $user_workouts = $user->workouts()->where('id', $w_id)->first();

                    if($opt == "delete"){
                        $user_workouts->workout_exercise()->delete();
                        $user->workouts()->detach($w_id);

                        $delete_workout = \App\Models\Workout::where('id', $w_id)->forceDelete();

                        $result['message']='Workout deleted successfully';
                    }
                    else{

                        if(!empty($user_workouts->pivot->deleted_dates)){
                            $deleted_dates = json_decode($user_workouts->pivot->deleted_dates, true);
                            $dates = array_merge($dates, $deleted_dates);
                        }

                        $dates = array_unique($dates);
                        $user->workouts()->updateExistingPivot($w_id, ["deleted_dates"=> json_encode($dates)]);

                        $result['message']='Workout removed successfully';
                    }

                }
                else{
                    $error = true;
                    $status_code = 400;
                    $result['message']='Request data is empty';
                }
            }catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }
        else{
            $error = true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

}
