<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Aws\DynamoDb\Exception\DynamoDbException;
use Aws\DynamoDb\Marshaler;
use Session;

class FoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      return view('front.food')->with('user_id',\Auth::User()->getUserInfo()['id']);
       /* $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        if(!empty($input)){
            try{

                $food_name = $input['keyword'];
                $limit= isset($input['limit'])?$input['limit']:100;
                $result = \App\Models\Nutrition::with('nutrition_brand')
                                                ->serachNutrition($food_name)
                                                ->orderBy('nutritions.name', 'asc')
                                                ->take($limit)
                                                ->get();

            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback')); */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();

        if(!empty($input) && $id > 0) {
            try{

                $user_id = $input['user_id'];
                $user = \App\Models\User::patient()->find($user_id);

                if($user != null){
                    $nutrition_data =[];
                    $nutrition_id = $input['nutrition_id'];
                    $nutrition_user =  $user->nutritions()->where('nutrition_user.id','=',$id)->first();
                    if($nutrition_user != null){
                         $logged_nutrition =  \App\Models\Nutrition_user::find($id);
                         $logged_nutrition->update($input);
                         $result['message']='Logged Food Updated Successfully.';

                    }else{

                        $error = true;
                        $status_code = 404;
                        $result['message']='No Food Found.';
                    }


                }else{

                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }


            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function logFood(Request $request){

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        $user_agent = isset($input['user_agent'])? $input['user_agent']:'';

        if(!empty($input)) {
            try{

                $user_id = $input['user_id'];
                $user = \App\Models\User::patient()->find($user_id);

                $input['created_by'] = $user_id;
                $input['updated_by'] = $user_id;
                //check session
                if(Session::has('orig_user') ){
                  $input['created_by'] = Session::get( 'orig_user' );
                  $input['updated_by'] = Session::get( 'orig_user' );

                }
                if($user != null){

                    if(isset($input['guid_server'])){
                        $current_timestamp = \Carbon\Carbon::now()->timestamp;
                        $input['guid'] = guid();
                        $input['timestamp'] = $current_timestamp;
                        $user_agent = 'server';


                    }

                     $check_user_nutrition = \App\Models\Nutrition_user::
                                                      where('user_id',$user_id)
                                                      ->where('guid', $input['guid'])
                                                      ->first();

                      if(!empty($check_user_nutrition)){

                        $input['updated_at']=date('Y-m-d H:i:s');
                        $check_user_nutrition->update($input);
                        $id = $check_user_nutrition->id;
                        $guid = $check_user_nutrition->guid;

                      }else{

                        $input['created_at']=date('Y-m-d H:i:s');
                        $input['updated_at']=date('Y-m-d H:i:s');

                        $log_food = \App\Models\Nutrition_user::create($input);
                        $id = $log_food->id;
                        $guid = $log_food->guid;
                      }


                    // for iPhone notification
                    if($user_agent != "" && $user_agent == 'server'){
                        $notification['user_id'] = $user_id;
                        $notification['notification_text'] = 'FOOD_ADDED';
                        sendNotification($notification);

                    }
                    $result['log_food'] = array("id"=>$id, "guid"=>$guid);
                    $result['message']='Food Added Successfully.';
                }else{

                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }


            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));


    }

    // web service for iphone
    public function loggedFood($user_id, Request $request){

        $error =false;
        $status_code = 200;
        $server_timestamp = \Carbon\Carbon::now()->timestamp;
        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::find($user_id);
                if($user != null){
                    $timestamp = $request->get('timestamp');
                    $compare_date = gmdate("Y-m-d H:i:s", $timestamp);
                    if($timestamp != null){

                        $user_food = \App\Models\Nutrition_user::with('nutritions')
                                                           ->where('user_id',$user_id)
                                                           ->where('is_custom',0)
                                                           ->where('updated_at','>=',$compare_date)
                                                           ->orWhere('deleted_at','>=',$compare_date)
                                                           ->withTrashed()->get();

                        $user_custom_food = \App\Models\Nutrition_user::with('custom_nutritions')
                                                           ->where('user_id',$user_id)
                                                           ->where('is_custom',1)
                                                           ->where('updated_at','>=',$compare_date)
                                                           ->orWhere('deleted_at','>=',$compare_date)
                                                           ->withTrashed()->get();

                    }else{

                       $user_food = \App\Models\Nutrition_user::with('nutritions')
                                                           ->where('user_id',$user_id)
                                                           ->where('is_custom',0)
                                                           ->get();
                        $user_custom_food = \App\Models\Nutrition_user::with('custom_nutritions')
                                                           ->where('user_id',$user_id)
                                                           ->where('is_custom',1)
                                                           ->get();
                    }



                   $logged_food = new \Illuminate\Database\Eloquent\Collection;

                    if($user_food != null){
                       $logged_food = $user_food;
                     }

                     if( $user_custom_food != null){

                       foreach ($user_custom_food as $key => $value) {
                          $value['nutritions'] = $value['custom_nutritions'];
                          unset($value['custom_nutritions']);
                          $logged_food->push($value);
                      }

                     }
                   $result = $logged_food;
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'timestamp'=>$server_timestamp,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function getPatientFood(){

        return view('front.food')->with('user_id',\Auth::User()->getUserInfo()['id']);
    }

    public function getPatientDiary(Request $request){
       $input = $request->all();
       $view_reports = isset($input['viewreports'])?$input['viewreports']:'all';
       return view('front.reports')
        ->with('viewreports',$view_reports)
        ->with('user_id',\Auth::User()->getUserInfo()['id']);
    }

    // web service for web
    public function patientFood($user_id, Request $request){

        $error =false;
        $status_code = 200;
        $today = date("Y-m-d");
        $food_summary =[];

        $req_date = $request->get('log_date');


        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::with('profile')->find($user_id);
                if($user != null){

                    $todayLoggedFood = $this->getTodayLggedFood($user_id, $req_date);

                    $result['todayLoggedFood'] = $todayLoggedFood;
                    $calories_intake_goal = 0;

                    $user_calorie_goal = $user->User_calorie_goal()->first();

                    if($user_calorie_goal != null){
                        $calories_intake_goal =  $user_calorie_goal->calorie_intake_g;
                    }
                    else{

                        if($user->profile != null){
                          $calories_intake_goal = $user->profile->calories;
                         }
                    }

                    $food_summary['calorie_goal'] =$calories_intake_goal;

                    $total_caloires = \App\Models\Nutrition_user::where('user_id',$user_id)
                                                                ->where('serving_date',$req_date)
                                                                ->sum('calories');
                   $calorie_intake_percentage=0;
                   if($calories_intake_goal > 0){
                      $calorie_intake_percentage = ($total_caloires/$calories_intake_goal)*100;
                    }
                    $food_summary['calorie_percentage'] = floor($calorie_intake_percentage);
                    $food_summary['total_consumed'] = $total_caloires;
                    $food_summary['total_remaining'] = $calories_intake_goal - $total_caloires;
                    $result['food_summary'] = $food_summary;

                    $result['patientExercise'] = $this->patientExercise($user_id, $req_date);

                    $result['food_note'] = $this->getUserFoodNote($user_id, $req_date);
                    $result['total_water_intake'] = $this->getTotalWaterIntake($user_id, $req_date);
                    $result['favourite_nutritions'] = $this->getFavouriteFood($user_id,  $call_from = "server");
                    $user_logged_food = $this->user_all_logged_food($user_id);
                    $result['frequent_nutritions'] = $this->getFrequentNutritions($user_logged_food);
                    $result['recent_nutritions'] = $this->getRecentNutritions($user_logged_food);
                    $result['custom_nutritions'] = $this->getCustomFood($user_id, $call_from = "server");
                    $result['meals'] = $this->getMeals($user_id, $call_from = "server");
                    $result['md_meals'] = $this->getMealsByUser($user_id, $call_from = "server");
                    $result['md_meal_foods'] = $this->getMdRecommendedFoodsFront($user_id, $call_from = "server");

                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function getTodayLggedFood($user_id, $date){


        $today = $date ? $date : date("Y-m-d");


        if($user_id != null){

            $schedule_times = $this->loggedFoodGroupBySceduledTime($user_id, $date);

            $filter_data = [];

            $no_of_serving = 1;

            $foodType_total = [];

            foreach ($schedule_times as $key1 => $schedule_time) {
              if($key1 == 'meal_1'){
                $calorie_arr = [];
                $fat_arr = [];
                $fiber_arr = [];
                $carbs_arr = [];
                $sodium_arr = [];
                $protein_arr = [];
                foreach ($schedule_time as $key => $value) {
                    $foodType_values = [];
                    $no_of_serving = $value->no_of_servings;
                    $foodType_values['id'] = $value->id;
                    $foodType_values['user_id'] = $value->user_id;
                    $foodType_values['nutrition_id'] = $value->nutrition_id;
                    $foodType_values['name'] = $value->nutritions->name;
                    $foodType_values['calories'] = $value->calories;
                    $foodType_values['food_type'] = $key1;
                    $foodType_values['serving_quantity'] = $value->serving_quantity;
                    $foodType_values['serving_size_unit'] = $value->serving_size_unit;
                    $foodType_values['no_of_servings'] = $no_of_serving;
                    $foodType_values['is_custom'] = $value->is_custom;

                    if($value->nutritions->nutrition_data){
                        $value->nutritions->nutrition_data = convertStringToArray($value->nutritions->nutrition_data);
                    }

                    //calculate the total
                    $calorie_arr[] = $value->calories;

                    $fat_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['total_fat']);
                    $logged_fat = $fat_value * $no_of_serving;
                    $fat_arr[] = $logged_fat;
                    $foodType_values['fat'] = $logged_fat;

                    $fibar_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['dietary_fiber']);
                    $logged_fiber = $fibar_value * $no_of_serving;
                    $fiber_arr[] = $logged_fiber;
                    $foodType_values['fiber'] = $logged_fiber;

                    $carb_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['total_carb']);
                    $logged_carb = $carb_value * $no_of_serving;
                    $carbs_arr[] = $logged_carb;
                    $foodType_values['carb'] = $logged_carb;

                    $sodium_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['sodium']);
                    $logged_sodium = $sodium_value * $no_of_serving;
                    $sodium_arr[] = $logged_sodium;
                    $foodType_values['sodium'] = $logged_sodium;

                    $protein_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['protein']);
                    $logged_protein = $protein_value * $no_of_serving;
                    $protein_arr[] = $logged_protein;
                    $foodType_values['protein'] = $logged_protein;
                    //end calculate the total

                    $filter_data[$key1][$key] =$foodType_values;
                }

                $foodType_total = [
                                        "calorie"=> array_sum($calorie_arr),
                                        "fat"=> array_sum($fat_arr),
                                        "fiber"=> array_sum($fiber_arr),
                                        "carbs"=> array_sum($carbs_arr),
                                        "sodium"=> array_sum($sodium_arr),
                                        "protein"=> array_sum($protein_arr),

                                    ];

                $filter_data['meal_one_total'] = $foodType_total;

              }

              if($key1 == 'meal_2'){
                $calorie_arr = [];
                $fat_arr = [];
                $fiber_arr = [];
                $carbs_arr = [];
                $sodium_arr = [];
                $protein_arr = [];
                foreach ($schedule_time as $key => $value) {
                    $foodType_values = [];
                    $no_of_serving = $value->no_of_servings;
                    $foodType_values['id'] = $value->id;
                    $foodType_values['user_id'] = $value->user_id;
                    $foodType_values['nutrition_id'] = $value->nutrition_id;
                    $foodType_values['name'] = $value->nutritions->name;
                    $foodType_values['calories'] = $value->calories;
                    $foodType_values['food_type'] = $key1;
                    $foodType_values['serving_quantity'] = $value->serving_quantity;
                    $foodType_values['serving_size_unit'] = $value->serving_size_unit;
                    $foodType_values['no_of_servings'] = $no_of_serving;
                    $foodType_values['is_custom'] = $value->is_custom;

                    if($value->nutritions->nutrition_data){
                        $value->nutritions->nutrition_data = convertStringToArray($value->nutritions->nutrition_data);
                    }

                    //calculate the total
                    $calorie_arr[] = $value->calories;

                    $fat_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['total_fat']);
                    $logged_fat = $fat_value * $no_of_serving;
                    $fat_arr[] = $logged_fat;
                    $foodType_values['fat'] = $logged_fat;

                    $fibar_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['dietary_fiber']);
                    $logged_fiber = $fibar_value * $no_of_serving;
                    $fiber_arr[] = $logged_fiber;
                    $foodType_values['fiber'] = $logged_fiber;

                    $carb_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['total_carb']);
                    $logged_carb = $carb_value * $no_of_serving;
                    $carbs_arr[] = $logged_carb;
                    $foodType_values['carb'] = $logged_carb;

                    $sodium_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['sodium']);
                    $logged_sodium = $sodium_value * $no_of_serving;
                    $sodium_arr[] = $logged_sodium;
                    $foodType_values['sodium'] = $logged_sodium;

                    $protein_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['protein']);
                    $logged_protein = $protein_value * $no_of_serving;
                    $protein_arr[] = $logged_protein;
                    $foodType_values['protein'] = $logged_protein;
                    //end calculate the total

                    $filter_data[$key1][$key] =$foodType_values;
                }

                $foodType_total = [
                                        "calorie"=> array_sum($calorie_arr),
                                        "fat"=> array_sum($fat_arr),
                                        "fiber"=> array_sum($fiber_arr),
                                        "carbs"=> array_sum($carbs_arr),
                                        "sodium"=> array_sum($sodium_arr),
                                        "protein"=> array_sum($protein_arr),

                                    ];

                $filter_data['meal_two_total'] = $foodType_total;

              }

              if($key1 == 'meal_3'){
                $calorie_arr = [];
                $fat_arr = [];
                $fiber_arr = [];
                $carbs_arr = [];
                $sodium_arr = [];
                $protein_arr = [];
                foreach ($schedule_time as $key => $value) {
                    $foodType_values = [];
                    $no_of_serving = $value->no_of_servings;
                    $foodType_values['id'] = $value->id;
                    $foodType_values['user_id'] = $value->user_id;
                    $foodType_values['nutrition_id'] = $value->nutrition_id;
                    $foodType_values['name'] = $value->nutritions->name;
                    $foodType_values['calories'] = $value->calories;
                    $foodType_values['food_type'] = $key1;
                    $foodType_values['serving_quantity'] = $value->serving_quantity;
                    $foodType_values['serving_size_unit'] = $value->serving_size_unit;
                    $foodType_values['no_of_servings'] = $no_of_serving;
                    $foodType_values['is_custom'] = $value->is_custom;

                    if($value->nutritions->nutrition_data){
                        $value->nutritions->nutrition_data = convertStringToArray($value->nutritions->nutrition_data);
                    }

                    //calculate the total
                    $calorie_arr[] = $value->calories;

                    $fat_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['total_fat']);
                    $logged_fat = $fat_value * $no_of_serving;
                    $fat_arr[] = $logged_fat;
                    $foodType_values['fat'] = $logged_fat;

                    $fibar_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['dietary_fiber']);
                    $logged_fiber = $fibar_value * $no_of_serving;
                    $fiber_arr[] = $logged_fiber;
                    $foodType_values['fiber'] = $logged_fiber;

                    $carb_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['total_carb']);
                    $logged_carb = $carb_value * $no_of_serving;
                    $carbs_arr[] = $logged_carb;
                    $foodType_values['carb'] = $logged_carb;

                    $sodium_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['sodium']);
                    $logged_sodium = $sodium_value * $no_of_serving;
                    $sodium_arr[] = $logged_sodium;
                    $foodType_values['sodium'] = $logged_sodium;

                    $protein_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['protein']);
                    $logged_protein = $protein_value * $no_of_serving;
                    $protein_arr[] = $logged_protein;
                    $foodType_values['protein'] = $logged_protein;
                    //end calculate the total

                    $filter_data[$key1][$key] =$foodType_values;
                }

                $foodType_total = [
                                        "calorie"=> array_sum($calorie_arr),
                                        "fat"=> array_sum($fat_arr),
                                        "fiber"=> array_sum($fiber_arr),
                                        "carbs"=> array_sum($carbs_arr),
                                        "sodium"=> array_sum($sodium_arr),
                                        "protein"=> array_sum($protein_arr),

                                    ];

                $filter_data['meal_three_total'] = $foodType_total;

              }

              if($key1 == 'meal_4'){
                $calorie_arr = [];
                $fat_arr = [];
                $fiber_arr = [];
                $carbs_arr = [];
                $sodium_arr = [];
                $protein_arr = [];

                foreach ($schedule_time as $key => $value) {
                    $foodType_values = [];
                    $no_of_serving = $value->no_of_servings;
                    $foodType_values['id'] = $value->id;
                    $foodType_values['user_id'] = $value->user_id;
                    $foodType_values['nutrition_id'] = $value->nutrition_id;
                    $foodType_values['name'] = $value->nutritions->name;
                    $foodType_values['calories'] = $value->calories;
                    $foodType_values['food_type'] = $key1;
                    $foodType_values['serving_quantity'] = $value->serving_quantity;
                    $foodType_values['serving_size_unit'] = $value->serving_size_unit;
                    $foodType_values['no_of_servings'] = $no_of_serving;
                    $foodType_values['is_custom'] = $value->is_custom;

                    if($value->nutritions->nutrition_data){
                        $value->nutritions->nutrition_data = convertStringToArray($value->nutritions->nutrition_data);
                    }

                    //calculate the total
                    $calorie_arr[] = $value->calories;

                    $fat_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['total_fat']);
                    $logged_fat = $fat_value * $no_of_serving;
                    $fat_arr[] = $logged_fat;
                    $foodType_values['fat'] = $logged_fat;

                    $fibar_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['dietary_fiber']);
                    $logged_fiber = $fibar_value * $no_of_serving;
                    $fiber_arr[] = $logged_fiber;
                    $foodType_values['fiber'] = $logged_fiber;

                    $carb_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['total_carb']);
                    $logged_carb = $carb_value * $no_of_serving;
                    $carbs_arr[] = $logged_carb;
                    $foodType_values['carb'] = $logged_carb;

                    $sodium_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['sodium']);
                    $logged_sodium = $sodium_value * $no_of_serving;
                    $sodium_arr[] = $logged_sodium;
                    $foodType_values['sodium'] = $logged_sodium;

                    $protein_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['protein']);
                    $logged_protein = $protein_value * $no_of_serving;
                    $protein_arr[] = $logged_protein;
                    $foodType_values['protein'] = $logged_protein;
                    //end calculate the total

                    $filter_data[$key1][$key] =$foodType_values;
                }

                $foodType_total = [
                                        "calorie"=> array_sum($calorie_arr),
                                        "fat"=> array_sum($fat_arr),
                                        "fiber"=> array_sum($fiber_arr),
                                        "carbs"=> array_sum($carbs_arr),
                                        "sodium"=> array_sum($sodium_arr),
                                        "protein"=> array_sum($protein_arr),

                                    ];

                $filter_data['meal_four_total'] = $foodType_total;

              }

              if($key1 == 'meal_5'){
                    $calorie_arr = [];
                    $fat_arr = [];
                    $fiber_arr = [];
                    $carbs_arr = [];
                    $sodium_arr = [];
                    $protein_arr = [];

                    foreach ($schedule_time as $key => $value) {
                        $foodType_values = [];
                        $no_of_serving = $value->no_of_servings;
                        $foodType_values['id'] = $value->id;
                        $foodType_values['user_id'] = $value->user_id;
                        $foodType_values['nutrition_id'] = $value->nutrition_id;
                        $foodType_values['name'] = $value->nutritions->name;
                        $foodType_values['calories'] = $value->calories;
                        $foodType_values['food_type'] = $key1;
                        $foodType_values['serving_quantity'] = $value->serving_quantity;
                        $foodType_values['serving_size_unit'] = $value->serving_size_unit;
                        $foodType_values['no_of_servings'] = $no_of_serving;
                        $foodType_values['is_custom'] = $value->is_custom;

                        if($value->nutritions->nutrition_data){
                            $value->nutritions->nutrition_data = convertStringToArray($value->nutritions->nutrition_data);
                        }

                        //calculate the total
                        $calorie_arr[] = $value->calories;

                        $fat_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['total_fat']);
                        $logged_fat = $fat_value * $no_of_serving;
                        $fat_arr[] = $logged_fat;
                        $foodType_values['fat'] = $logged_fat;

                        $fibar_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['dietary_fiber']);
                        $logged_fiber = $fibar_value * $no_of_serving;
                        $fiber_arr[] = $logged_fiber;
                        $foodType_values['fiber'] = $logged_fiber;

                        $carb_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['total_carb']);
                        $logged_carb = $carb_value * $no_of_serving;
                        $carbs_arr[] = $logged_carb;
                        $foodType_values['carb'] = $logged_carb;

                        $sodium_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['sodium']);
                        $logged_sodium = $sodium_value * $no_of_serving;
                        $sodium_arr[] = $logged_sodium;
                        $foodType_values['sodium'] = $logged_sodium;

                        $protein_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['protein']);
                        $logged_protein = $protein_value * $no_of_serving;
                        $protein_arr[] = $logged_protein;
                        $foodType_values['protein'] = $logged_protein;
                        //end calculate the total

                        $filter_data[$key1][$key] =$foodType_values;
                    }

                    $foodType_total = [
                        "calorie"=> array_sum($calorie_arr),
                        "fat"=> array_sum($fat_arr),
                        "fiber"=> array_sum($fiber_arr),
                        "carbs"=> array_sum($carbs_arr),
                        "sodium"=> array_sum($sodium_arr),
                        "protein"=> array_sum($protein_arr),

                    ];

                    $filter_data['meal_five_total'] = $foodType_total;

                }

              if($key1 == 'meal_6'){
                    $calorie_arr = [];
                    $fat_arr = [];
                    $fiber_arr = [];
                    $carbs_arr = [];
                    $sodium_arr = [];
                    $protein_arr = [];

                    foreach ($schedule_time as $key => $value) {
                        $foodType_values = [];
                        $no_of_serving = $value->no_of_servings;
                        $foodType_values['id'] = $value->id;
                        $foodType_values['user_id'] = $value->user_id;
                        $foodType_values['nutrition_id'] = $value->nutrition_id;
                        $foodType_values['name'] = $value->nutritions->name;
                        $foodType_values['calories'] = $value->calories;
                        $foodType_values['food_type'] = $key1;
                        $foodType_values['serving_quantity'] = $value->serving_quantity;
                        $foodType_values['serving_size_unit'] = $value->serving_size_unit;
                        $foodType_values['no_of_servings'] = $no_of_serving;
                        $foodType_values['is_custom'] = $value->is_custom;

                        if($value->nutritions->nutrition_data){
                            $value->nutritions->nutrition_data = convertStringToArray($value->nutritions->nutrition_data);
                        }

                        //calculate the total
                        $calorie_arr[] = $value->calories;

                        $fat_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['total_fat']);
                        $logged_fat = $fat_value * $no_of_serving;
                        $fat_arr[] = $logged_fat;
                        $foodType_values['fat'] = $logged_fat;

                        $fibar_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['dietary_fiber']);
                        $logged_fiber = $fibar_value * $no_of_serving;
                        $fiber_arr[] = $logged_fiber;
                        $foodType_values['fiber'] = $logged_fiber;

                        $carb_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['total_carb']);
                        $logged_carb = $carb_value * $no_of_serving;
                        $carbs_arr[] = $logged_carb;
                        $foodType_values['carb'] = $logged_carb;

                        $sodium_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['sodium']);
                        $logged_sodium = $sodium_value * $no_of_serving;
                        $sodium_arr[] = $logged_sodium;
                        $foodType_values['sodium'] = $logged_sodium;

                        $protein_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['protein']);
                        $logged_protein = $protein_value * $no_of_serving;
                        $protein_arr[] = $logged_protein;
                        $foodType_values['protein'] = $logged_protein;
                        //end calculate the total

                        $filter_data[$key1][$key] =$foodType_values;
                    }

                    $foodType_total = [
                        "calorie"=> array_sum($calorie_arr),
                        "fat"=> array_sum($fat_arr),
                        "fiber"=> array_sum($fiber_arr),
                        "carbs"=> array_sum($carbs_arr),
                        "sodium"=> array_sum($sodium_arr),
                        "protein"=> array_sum($protein_arr),

                    ];

                    $filter_data['meal_six_total'] = $foodType_total;

                }

              if($key1 == 'pre_workout_meal'){
                    $calorie_arr = [];
                    $fat_arr = [];
                    $fiber_arr = [];
                    $carbs_arr = [];
                    $sodium_arr = [];
                    $protein_arr = [];

                    foreach ($schedule_time as $key => $value) {
                        $foodType_values = [];
                        $no_of_serving = $value->no_of_servings;
                        $foodType_values['id'] = $value->id;
                        $foodType_values['user_id'] = $value->user_id;
                        $foodType_values['nutrition_id'] = $value->nutrition_id;
                        $foodType_values['name'] = $value->nutritions->name;
                        $foodType_values['calories'] = $value->calories;
                        $foodType_values['food_type'] = $key1;
                        $foodType_values['serving_quantity'] = $value->serving_quantity;
                        $foodType_values['serving_size_unit'] = $value->serving_size_unit;
                        $foodType_values['no_of_servings'] = $no_of_serving;
                        $foodType_values['is_custom'] = $value->is_custom;

                        if($value->nutritions->nutrition_data){
                            $value->nutritions->nutrition_data = convertStringToArray($value->nutritions->nutrition_data);
                        }

                        //calculate the total
                        $calorie_arr[] = $value->calories;

                        $fat_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['total_fat']);
                        $logged_fat = $fat_value * $no_of_serving;
                        $fat_arr[] = $logged_fat;
                        $foodType_values['fat'] = $logged_fat;

                        $fibar_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['dietary_fiber']);
                        $logged_fiber = $fibar_value * $no_of_serving;
                        $fiber_arr[] = $logged_fiber;
                        $foodType_values['fiber'] = $logged_fiber;

                        $carb_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['total_carb']);
                        $logged_carb = $carb_value * $no_of_serving;
                        $carbs_arr[] = $logged_carb;
                        $foodType_values['carb'] = $logged_carb;

                        $sodium_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['sodium']);
                        $logged_sodium = $sodium_value * $no_of_serving;
                        $sodium_arr[] = $logged_sodium;
                        $foodType_values['sodium'] = $logged_sodium;

                        $protein_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['protein']);
                        $logged_protein = $protein_value * $no_of_serving;
                        $protein_arr[] = $logged_protein;
                        $foodType_values['protein'] = $logged_protein;
                        //end calculate the total

                        $filter_data[$key1][$key] =$foodType_values;
                    }

                    $foodType_total = [
                        "calorie"=> array_sum($calorie_arr),
                        "fat"=> array_sum($fat_arr),
                        "fiber"=> array_sum($fiber_arr),
                        "carbs"=> array_sum($carbs_arr),
                        "sodium"=> array_sum($sodium_arr),
                        "protein"=> array_sum($protein_arr),

                    ];

                    $filter_data['pre_workout_total'] = $foodType_total;

                }

              if($key1 == 'post_workout_meal'){
                    $calorie_arr = [];
                    $fat_arr = [];
                    $fiber_arr = [];
                    $carbs_arr = [];
                    $sodium_arr = [];
                    $protein_arr = [];

                    foreach ($schedule_time as $key => $value) {
                        $foodType_values = [];
                        $no_of_serving = $value->no_of_servings;
                        $foodType_values['id'] = $value->id;
                        $foodType_values['user_id'] = $value->user_id;
                        $foodType_values['nutrition_id'] = $value->nutrition_id;
                        $foodType_values['name'] = $value->nutritions->name;
                        $foodType_values['calories'] = $value->calories;
                        $foodType_values['food_type'] = $key1;
                        $foodType_values['serving_quantity'] = $value->serving_quantity;
                        $foodType_values['serving_size_unit'] = $value->serving_size_unit;
                        $foodType_values['no_of_servings'] = $no_of_serving;
                        $foodType_values['is_custom'] = $value->is_custom;

                        if($value->nutritions->nutrition_data){
                            $value->nutritions->nutrition_data = convertStringToArray($value->nutritions->nutrition_data);
                        }

                        //calculate the total
                        $calorie_arr[] = $value->calories;

                        $fat_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['total_fat']);
                        $logged_fat = $fat_value * $no_of_serving;
                        $fat_arr[] = $logged_fat;
                        $foodType_values['fat'] = $logged_fat;

                        $fibar_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['dietary_fiber']);
                        $logged_fiber = $fibar_value * $no_of_serving;
                        $fiber_arr[] = $logged_fiber;
                        $foodType_values['fiber'] = $logged_fiber;

                        $carb_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['total_carb']);
                        $logged_carb = $carb_value * $no_of_serving;
                        $carbs_arr[] = $logged_carb;
                        $foodType_values['carb'] = $logged_carb;

                        $sodium_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['sodium']);
                        $logged_sodium = $sodium_value * $no_of_serving;
                        $sodium_arr[] = $logged_sodium;
                        $foodType_values['sodium'] = $logged_sodium;

                        $protein_value = $this->getValueWithoutUnit($value->nutritions->nutrition_data['protein']);
                        $logged_protein = $protein_value * $no_of_serving;
                        $protein_arr[] = $logged_protein;
                        $foodType_values['protein'] = $logged_protein;
                        //end calculate the total

                        $filter_data[$key1][$key] =$foodType_values;
                    }

                    $foodType_total = [
                        "calorie"=> array_sum($calorie_arr),
                        "fat"=> array_sum($fat_arr),
                        "fiber"=> array_sum($fiber_arr),
                        "carbs"=> array_sum($carbs_arr),
                        "sodium"=> array_sum($sodium_arr),
                        "protein"=> array_sum($protein_arr),

                    ];

                    $filter_data['post_workout_total'] = $foodType_total;

                }
            }

            return $filter_data;
        }

        return [];
    }

    public function getValueWithoutUnit($string){
        if(!empty($string)){
            $string = trim($string);
            $string_arr = explode(' ', $string);

            return trim($string_arr[0]);
        }

        return '';
    }

    public function getloggedFood($user_id){ // for web

        $error =false;
        $status_code = 200;
        $today = date("Y-m-d");


        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::find($user_id);

                if($user != null){


                    $user_logged_food = $this->user_all_logged_food($user_id);
                    $result['user_logged_food'] = $user_logged_food;
                    $result['frequent_nutritions'] = $this->getFrequentNutritions($user_logged_food);
                    $result['recent_nutritions'] = $this->getRecentNutritions($user_logged_food);
                    $result['custom_nutritions'] = $this->getCustomFood($user_id, $call_from = "server");
                    $result['meal_foods'] = $this->getMeals($user_id, $call_from = "server");
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }
            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }
    // web service for web
    public function getFrequentNutritions($logged_nutrition){

        //start: get frequent nutritions
        $frequent_nutritions = array();
        if(count($logged_nutrition) > 0){

            $nutritions_groupBy = $logged_nutrition->groupBy('nutrition_id')->sortByDesc('id');
            if(count($nutritions_groupBy)>0){

                foreach ($nutritions_groupBy as $key => $grouped_nutritions) {

                   if(count($grouped_nutritions)>1){

                        $grouped_nutritions = $grouped_nutritions->take(-1)->first();

                        if($grouped_nutritions['nutritions']['nutrition_data']){
                          $grouped_nutritions['nutritions']['nutrition_data'] = convertStringToArray($grouped_nutritions['nutritions']['nutrition_data']);
                        }

                        if($grouped_nutritions['nutritions']['serving_data']){
                          $grouped_nutritions['nutritions']['serving_data'] = convertStringToArray($grouped_nutritions['nutritions']['serving_data']);
                        }

                        $grouped_nutritions['is_favourite'] = $this->check_is_favourite($grouped_nutritions['nutrition_id'], $grouped_nutritions['is_custom']);
                        $frequent_nutritions[] = $grouped_nutritions;

                   }
                }
            }
        }

        return $frequent_nutritions;
        //end: get frequent nutritions
    }

    public function getRecentNutritions($logged_nutrition){
        //start: get recent nutritions
        $recent_nutritions = array();

        if(count($logged_nutrition) > 0){
            $recent_nutritions = $logged_nutrition->sortByDesc('id')->take(10);

            foreach($recent_nutritions as $data){
                if($data['nutritions']['nutrition_data']){
                  $data['nutritions']['nutrition_data'] = convertStringToArray($data['nutritions']['nutrition_data']);
                }

                if($data['nutritions']['serving_data']){
                  $data['nutritions']['serving_data'] = convertStringToArray($data['nutritions']['serving_data']);
                }

                $data['is_favourite'] = $this->check_is_favourite($data['nutrition_id'], $data['is_custom']);

            }
        }

        return $recent_nutritions;
        //end: get recent nutritions
    }

    // web service for web
    public function patientExercise($user_id, $req_date){

        $exe_result = [];
        $exercise_summary =[];
        $calories_burned_goal="";

        if($user_id !="" && $user_id > 0){

                $user = \App\Models\User::find($user_id);
                if($user != null){

                    $loggedExercise = $this->getLoggedExercise($user_id, $req_date);

                    $exe_result['loggedExercise'] = $loggedExercise;

                    $user_calorie_goal = $user->User_calorie_goal()->first();

                    if($user_calorie_goal != null){
                         $calories_burned_goal =  $user_calorie_goal->calorie_burned_g;
                         $calorie_burned_percentage=0;
                          $total_caloires_burned = \App\Models\Exercise_user::where('user_id',$user_id)
                                                                ->where('exercise_date',$req_date)
                                                                ->sum('calories_burned');

                         if($total_caloires_burned > 0){
                            $calorie_burned_percentage = ($total_caloires_burned/$calories_burned_goal)*100;
                          }
                          $exercise_summary['calorie_burned_percentage'] = floor($calorie_burned_percentage);
                          $exercise_summary['total_burned'] = $total_caloires_burned;
                          $total_remaining = $calories_burned_goal - $total_caloires_burned;
                           $display_remaing=0;
                           if($total_remaining > 0){
                              $display_remaing =$total_remaining;
                           }
                          $exercise_summary['total_remaining'] =$display_remaing;
                    }

                    $exercise_summary['calorie_burned_goal'] =$calories_burned_goal;



                    $exe_result['exercise_summary'] = $exercise_summary;
                    $user_logged_exercise =  \App\Models\Exercise_user::with('exercise')
                                                   ->where('user_id',$user_id)
                                                   ->get();
                    $exe_result['frequent_exercises'] = $this->getFrequentExercise($user_logged_exercise);
                    $exe_result['recent_exercises'] = $this->getRecentExercise($user_logged_exercise);

                    $exe_result['apple_health_exercises']= $this->getAppleHealthExercise($user_id,$req_date);
                }else{
                    $error = true;
                    $status_code = 404;
                    $exe_result['message']='No Patient Found.';

                }

        }

        return $exe_result;
    }

    public function getLoggedExercise($user_id, $date){

        $log_date = $date ? $date : date("Y-m-d");
        if($user_id > 0){

            $logged_exercise = \App\Models\Exercise_user::with('exercise')
                                                   ->where('user_id',$user_id)
                                                   ->whereDate('exercise_date','=', $log_date)
                                                   ->get();
            $exercise_type = $logged_exercise->sortByDesc('id')->groupBy('exercise_type');
            //dd($exercise_type->toArray());
            $filter_data = [];
            $exercise_type_total = [];

            foreach ($exercise_type as $key => $type) {
              if($key == 'Cardio'){
                    $calorie_burned_arr = [];
                    $time_arr = [];
                    foreach ($type as $key1 => $value) {
                        $exercise_values = [];
                        $exercise_values['id'] = $value->id;
                        $exercise_values['user_id'] = $value->user_id;
                        $exercise_values['exercise_id'] = $value->exercise_id;
                        $exercise_values['name'] = $value->exercise->name;
                        $exercise_values['calories_burned'] = $value->calories_burned;
                        $exercise_values['time'] = $value->time;
                        $exercise_values['exercise_type'] = $key;

                        //calculate the total

                        $calorie_burned_arr[] = $value->calories_burned;
                        $time_arr[] =$value->time;
                        $filter_data[$key][$key1] =$exercise_values;
                    }

                    $exercise_type_total = [
                                            "calories_burned"=> array_sum($calorie_burned_arr),
                                            "time"=> array_sum($time_arr)
                                           ];

                    $filter_data['cardio_total'] = $exercise_type_total;

              }

             if($key == 'Resistance'){
                    $calorie_burned_arr = [];
                    $time_arr = [];
                    $sets_arr = [];
                    $reps_arr = [];
                    $weight_lbs_arr = [];
                    foreach ($type as $key1 => $value) {
                        $exercise_values = [];
                        $exercise_values['id'] = $value->id;
                        $exercise_values['user_id'] = $value->user_id;
                        $exercise_values['exercise_id'] = $value->exercise_id;
                        $exercise_values['name'] = $value->exercise->name;
                        $exercise_values['calories_burned'] = $value->calories_burned;
                        $exercise_values['time'] = $value->time;
                        $exercise_values['exercise_type'] = $key;
                        $exercise_values['sets'] = $value->sets;
                        $exercise_values['reps'] = $value->reps;
                        $exercise_values['weight_lbs'] = $value->weight_lbs;

                        //calculate the total
                        $calorie_burned_arr[] = $value->calories_burned;
                        $time_arr[] =$value->time;
                        $sets_arr[] = $value->sets;
                        $reps_arr[] =  $value->reps;
                        $weight_lbs_arr[] = $value->weight_lbs;


                        $filter_data[$key][$key1] =$exercise_values;
                    }

                    $exercise_type_total = [
                                            "calories_burned"=> array_sum($calorie_burned_arr),
                                            "time"=> array_sum($time_arr),
                                            "sets"=> array_sum($sets_arr),
                                            "reps"=> array_sum($reps_arr),
                                            "weight_lbs"=> array_sum($weight_lbs_arr)
                                           ];

                    $filter_data['resistance_total'] = $exercise_type_total;

             }

            }

            return $filter_data;
        }

        return [];
    }

    public function getFrequentExercise($logged_Exercise){
        //start: get frequent exercise

        $frequent_exercise = array();
        $exercise_groupBy = $logged_Exercise->groupBy('exercise_id');

        if(count($exercise_groupBy)>0){
            foreach ($exercise_groupBy as $key => $grouped_exercise) {

               if(count($grouped_exercise)>1){

                    foreach ($grouped_exercise as $key1 => $frequent_exercise_value) {
                        $frequent_exercise_value->name = $frequent_exercise_value->exercise->name;
                        $frequent_exercise[] =$frequent_exercise_value->toArray();
                        break;
                    }
               }
            }
        }
        return $frequent_exercise;
        //end: get frequent nutritions
    }

    public function getRecentExercise($logged_Exercise){
        //start: get recent exercise
        $recent_Exercise = array();
        $recent_Exercise_data = $logged_Exercise->sortByDesc('id')->take(10);

        if(count($recent_Exercise_data)>0){

            foreach ($recent_Exercise_data as $key => $recent_exercise_value) {

                if(count($recent_exercise_value)>0){
                    $recent_exercise_value->name = $recent_exercise_value->exercise->name;
                    $recent_Exercise[] =$recent_exercise_value->toArray();
                }
            }
        }

        return $recent_Exercise;
        //end: get recent exercise
    }

    public function getAppleHealthExercise($user_id,$log_date){

         $logged_exercise = \App\Models\Exercise_user::with('exercise')
                                                   ->where('user_id',$user_id)
                                                   ->where('exercise_type','=', 'Apple_Health')
                                                   ->whereDate('exercise_date','=', $log_date)
                                                   ->get();

            $calorie_burned_arr = [];
            $time_arr = [];
            $filter_data=[];
            if($logged_exercise->count() > 0){

              foreach ($logged_exercise as $key1 => $value) {
                $exercise_values = [];
                $exercise_values['id'] = $value->id;
                $exercise_values['user_id'] = $value->user_id;
                $exercise_values['exercise_id'] = $value->exercise_id;
                $exercise_values['name'] = $value->exercise->name;
                $exercise_values['calories_burned'] = $value->calories_burned;
                $exercise_values['time'] = $value->time;
                $exercise_values['exercise_type'] = $value->exercise_type;

                //calculate the total
                if($value->exercise_sub_type == 'Active_Energy' || $value->exercise_sub_type == 'Resting_Energy'){
                     $calorie_burned_arr[] = $value->calories_burned;
                }
                if($value->exercise_sub_type != 'Active_Energy'){
                   $time_arr[] =$value->time;
                }

                $filter_data['apple_health'][] =$exercise_values;
              }

             $exercise_type_total = [
                                    "calories_burned"=> array_sum($calorie_burned_arr),
                                    "time"=> array_sum($time_arr)
                                   ];

             $filter_data['apple_health_total'] = $exercise_type_total;

            }
           return $filter_data;

    }

    public function getBlood_pressure(){
       return view('front.blood_pressure')->with('user_id',\Auth::User()->getUserInfo()['id']);
    }


    public function getNutritionsByUpc(Request $request)
    {
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        if(!empty($input)){
            try{

                if(isset($input['upc']) && !empty($input['upc'])){
                  $upc = trim($input['upc']);
                  $upcnon_zero = ltrim($upc,0);
                  $result = \App\Models\Nutrition::with('nutrition_brand')
                                                ->where('upc', 'LIKE', "%$upcnon_zero%")
                                                ->get();
                }


            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function logFavouriteFood(Request $request){

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();

        if(!empty($input)) {
            try{

                $user_id = $input['user_id'];
                $nutrition_id = $input['nutrition_id'];
                $user = \App\Models\User::patient()->find($user_id);

                if($user != null){

                    if(isset($input['guid_server'])){
                        $current_timestamp = \Carbon\Carbon::now()->timestamp;
                        $input['guid'] = guid();
                        $input['timestamp'] = $current_timestamp;
                        $user_agent = 'server';
                    }
                    $favourite_nutrition = \App\Models\Favourite_nutrition_user::where('user_id', $user_id)
                                                  ->where('nutrition_id', $nutrition_id)->first();

                    if(!empty($favourite_nutrition)){
                        $favourite_nutrition_user = $favourite_nutrition->update($input);
                        $last_id = $favourite_nutrition->id;
                        $result['message']='Favourite Nutrition Updated Successfully.';

                    }
                    else{
                        $favourite_nutrition_user = \App\Models\Favourite_nutrition_user::create($input);
                        $last_id = $favourite_nutrition_user->id;
                        $result['message']='Favourite Nutrition Added Successfully.';
                    }
                      if($favourite_nutrition_user->is_custom == 1){

                         $custom_fav_food  = \App\Models\Favourite_nutrition_user::with('custom_nutritions')->find($last_id);
                         $custom_fav_food->nutritions = $custom_fav_food->custom_nutritions;
                         unset($custom_fav_food['custom_nutritions']);
                         $result['favourite_nutrition']=$custom_fav_food;
                      }else{
                         $result['favourite_nutrition'] = \App\Models\Favourite_nutrition_user::with('nutritions')->find($last_id);
                      }


                }else{

                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }


            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function getFavouriteFood($user_id,  $call_from = "client"){

        $error =false;
        $status_code = 200;

        if(!empty($user_id)){
            try{

                $user = \App\Models\User::find($user_id);

                $favourite_nutritions = $user->favourite_nutritions()->where('is_custom',0)->orderBy('id', 'desc')->get();

                $custom_favourite_nutritions = \App\Models\Favourite_nutrition_user::
                                                                                with('custom_nutritions')
                                                                                    ->where('is_custom',1)
                                                                                    ->where('user_id',$user_id)
                                                                                    ->orderBy('id', 'desc')
                                                                                    ->get();

                $fav_food = new \Illuminate\Database\Eloquent\Collection;

                if($favourite_nutritions != null){
                    $fav_food = $favourite_nutritions;

                }
                if( $custom_favourite_nutritions != null){

                    foreach ($custom_favourite_nutritions as $key => $value) {
                        $value['nutritions'] = $value['custom_nutritions'];
                        unset($value['custom_nutritions']);
                        $fav_food->push($value);
                    }

                }

                foreach($fav_food as $data){
                    if($data['nutritions']['nutrition_data']){
                      $data['nutritions']['nutrition_data'] = convertStringToArray($data['nutritions']['nutrition_data']);
                    }

                    if($data['nutritions']['serving_data']){
                      $data['nutritions']['serving_data'] = convertStringToArray($data['nutritions']['serving_data']);
                    }

                    $data['is_favourite'] = $this->check_is_favourite($data['nutrition_id'], $data['is_custom']);

                }

                $result['favourite_nutritions'] = $fav_food;


            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        else{
            $error = true;
            $status_code = 404;
            $result['message']='No Patient Found.';

        }
        if($call_from=='server'){
            return $result['favourite_nutritions'];
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function deleteFavouriteFood($id){

        $error = false;
        $status_code = 200;
        $result =[];

        if(!empty($id)) {
            try{
                  $favourite_nutrition = \App\Models\Favourite_nutrition_user::find($id);
                  if($favourite_nutrition){
                      $favourite_nutrition->delete();
                      $result['message'] = 'Favourite Food Removed Successfully.';
                  }
                  else{
                      $error = true;
                      $result['error_message'] = 'record does not exist.';
                  }


               }catch(\Exception $ex){

                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function addCustomNutrition(Request $request){

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        $user_agent = isset($input['user_agent'])? $input['user_agent']:'';
        if(!empty($input)) {
            try{

                  $user_id = $input['user_id'];
                  $user = \App\Models\User::find($user_id);

                  $input['created_by'] = $user_id;
                  $input['updated_by'] = $user_id;
                  //check session
                  if(Session::has('orig_user') ){
                    $input['created_by'] = Session::get( 'orig_user' );
                    $input['updated_by'] = Session::get( 'orig_user' );
                  }


                  if(!empty($user)){

                    if($user_agent != "" && $user_agent == 'server'){

                        $current_timestamp = \Carbon\Carbon::now()->timestamp;
                        $input['guid'] = guid();
                        $input['timestamp'] = $current_timestamp;

                      }
                      $input['brand_with_name'] = $input['brand_name']." - ".$input['name'];
                      $input['nutrition_data'] = json_encode($input['nutrition_data']);

                      $check_user_custom_food = \App\Models\Custom_nutrition_user::where('user_id',$user_id)
                                                      ->where('guid', $input['guid'])
                                                      ->first();

                      if(!empty($check_user_custom_food)){

                        $check_user_custom_food->update($input);
                        $id = $check_user_custom_food->id;
                        $guid = $check_user_custom_food->guid;

                      }else{
                        $log_custom_food = \App\Models\Custom_nutrition_user::create($input);
                        $id = $log_custom_food->id;
                        $guid = $log_custom_food->guid;
                      }

                       // for iPhone notification
                    if($user_agent != "" && $user_agent == 'server'){
                        $notification['user_id'] = $user_id;
                        $notification['notification_text'] = 'CUSTOM_FOOD_ADDED';
                        sendNotification($notification);

                    }
                    $result['log_custom_food'] = array("id"=>$id, "guid"=>$guid);
                    $result['message']='Custom Food Added Successfully.';

                  }
                  else{

                      $error = true;
                      $status_code = 404;
                      $result['message']='No Patient Found.';
                  }



            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }


    public function getCustomFood($user_id, $call_from = "client"){

        $error =false;
        $status_code = 200;

        if(!empty($user_id)){
            try{

                $user = \App\Models\User::find($user_id);

                if(!empty($user)){

                    $custom_nutritions = \App\Models\Custom_nutrition_user::where('user_id', $user_id)->orderBy('id', 'desc')->get();
                    if($custom_nutritions){
                        foreach($custom_nutritions as $data){
                            if($data['nutrition_data']){
                              $data['nutrition_data'] = convertStringToArray($data['nutrition_data']);
                            }

                            $data['is_favourite'] = $this->check_is_favourite($data['id'], 1);

                        }
                    }


                    $result['custom_nutritions'] = $custom_nutritions;
                }
                else{

                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        else{
            $error = true;
            $status_code = 404;
            $result['message']='No Patient Found.';

        }

        if($call_from=='server'){
            return $result['custom_nutritions'];
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    //  Update custom food darshan

    public function updateCustomNutrition(Request $request, $id)
    {

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        if(!empty($input) && $id > 0) {
            try{

                 $custom_food = \App\Models\Custom_nutrition_user::where('id',$id)->first();
                  $input['brand_with_name'] = $input['brand_name']." - ".$input['name'];
                  $input['nutrition_data'] = json_encode($input['nutrition_data']);

                 if(!empty($custom_food)){
                    $custom_food->update($input);
                    $result['message']='Custom Food Updated Successfully.';
                 } else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Food Found.';
                 }
               }catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function deleteCustomNutrition($id){

        $error = false;
        $status_code = 200;
        $result =[];
        if(!empty($id)) {
            try{

                $nutrition_user = \App\Models\Nutrition_user::where('nutrition_id',$id)
                                                                ->where('is_custom',1)->first();
                  if($nutrition_user != null){
                    $nutrition_user->delete();
                  }

                   $faviroute_nutrition_user = \App\Models\Favourite_nutrition_user::where('nutrition_id',$id)
                                                                ->where('is_custom',1)->first();

                   if($faviroute_nutrition_user != null){
                     $faviroute_nutrition_user->delete();
                   }

                    $custom_nutrition = \App\Models\Custom_nutrition_user::where('id',$id)->first();
                    $custom_nutrition->delete();
                    $result['message'] = 'Custom Food Removed Successfully.';


                }catch(\Exception $ex){
                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }


    public function searchDynamodb(Request $request)
    {
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        if(!empty($input)){
            try{

                $food_name = $input['keyword'];
                $dynamodb = \AWS::createDynamoDb();
                $marshaler = new Marshaler();
                $tableName = 'Nutritions';
                $params = [
                          'TableName' => $tableName,
                           'ExpressionAttributeValues' => [':value' => ['S' => $food_name]] ,
                            'FilterExpression' => 'contains(brand_with_name, :value)',
                            'Limit'=>10
                         ];

              //  echo "Scanning Nutritions table.\n";

                try {
                  while (true) {
                  $result = $dynamodb->scan($params);

                  foreach ($result['Items'] as $i) {
                     $food = $marshaler->unmarshalItem($i);
                     dd($food);
                  }

                }

                } catch (DynamoDbException $e) {
                  echo "Unable to scan:\n";
                  echo $e->getMessage() . "\n";
                }



            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function searchFood(Request $request)
    {
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();

        if(!empty($input)){
            try{

                $food_name = $input['keyword'];
                $limit= isset($input['limit'])?$input['limit']:100;
                // $result = \App\Models\New_nutrition::whereRaw(
                //                                         "MATCH(brand_with_name) AGAINST(? IN NATURAL LANGUAGE MODE)",
                //                                          array($food_name))
                //                                   ->orderBy('new_nutritions.name', 'asc')
                //                                   ->take($limit)
                //                                   ->get(['id','name','brand_name','brand_with_name','nutrition_data','serving_data']);
            //  \Storage::append('query_log.txt', 'before--------'.\Carbon\Carbon::now().'.......');
               // $result = \DB::table('new_nutritions')
               //                ->whereRaw("MATCH(brand_with_name) AGAINST(? IN NATURAL LANGUAGE MODE)",array($food_name))
               //                                    ->orderBy('new_nutritions.name', 'asc')
               //                                    ->take($limit)
               //                                    ->get(['id','name','brand_name','brand_with_name','nutrition_data','serving_data']);

              $result = \DB::select('select id,name,brand_name,brand_with_name,nutrition_data,serving_data FROM new_nutritions WHERE MATCH (brand_with_name) AGAINST(? IN NATURAL LANGUAGE MODE) limit ?',[$food_name,$limit]);


             // \Storage::append('query_log.txt', 'after......'.\Carbon\Carbon::now().'.......');

            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function deleteLoggedNutrition($id){

        $error = false;
        $status_code = 200;
        $result =[];
        if(!empty($id) && $id> 0) {
            try{

                $nutrition_user = \App\Models\Nutrition_user::where('id',$id)
                                                                ->first();
                  if($nutrition_user != null){
                     $nutrition_user->delete();
                     $result['message'] = 'Food Removed Successfully.';
                  }else{
                     $result['message'] = ' No Food Found.';
                  }

                }catch(\Exception $ex){
                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function updateFoodNote(Request $request, $user_id){

        $error =false;
        $status_code = 200;
        if($user_id !="" && $user_id > 0){
            try{

                $input = $request->all();
                $user = \App\Models\User::find($user_id);
                if($user != null){
                   $user_food_note = \App\Models\User_food_note::where('user_id',$user_id)
                                                                  ->where('log_date',$input['log_date'])
                                                                  ->first();
                   if($user_food_note != null){
                     $user_food_note->update($input);

                   }else{

                         $input['user_id'] = $user_id;
                        \App\Models\User_food_note:: create($input);
                  }


                  $result['message']='Notes Updated successfully.';
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }
            }catch(\Exception $ex){
               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();
            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function logUpc(Request $request){

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        $user_agent = "";
        if(!empty($input)) {
            try{

                $user_id = $input['user_id'];

                $user = \App\Models\User::find($user_id);

                if($user != null){
                    if(isset($input['guid_server'])){

                        $current_timestamp = strtotime(date("Y-m-d H:i"));
                        $input['guid'] = guid();
                        $input['timestamp'] = $current_timestamp;
                    }

                    $check_user_nutrition_upc = $user->nutrition_upc()->where('timestamp', $input['timestamp'])
                                                      ->first();

                    if(!empty($check_user_nutrition_upc)){

                        $check_user_nutrition_upc->update($input);
                        $id = $check_user_nutrition_upc->id;
                        $guid = $check_user_nutrition_upc->guid;
                    }
                    else{

                        $log_nutrition_upc = $user->nutrition_upc()->create($input);
                        $id = $log_nutrition_upc->id;
                        $guid = $log_nutrition_upc->guid;
                    }

                    // for iPhone notification
                    if($user_agent != "" && $user_agent == 'server'){
                        $notification['user_id'] = $user_id;
                        $notification['notification_text'] = 'UPC_ADDED';
                        sendNotification($notification);

                    }

                    $result['log_nutrition_upc'] = array("id"=>$id, "guid"=>$guid);
                    $result['message']='UPC Logged Successfully.';
                }else{

                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }


            }catch(\Exception $ex){
                throw $ex;
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function loggedUpc(Request $request)
    {
        $error = false;
        $status_code = 200;
        $result =null;
        $input = $request->all();

        if(!empty($input)){
            try{

                if(isset($input['upc']) && !empty($input['upc'])){
                  $upc = trim($input['upc']);
                 // $upcnon_zero = ltrim($upc,0);
                  $nutrition_data = \App\Models\Nutrition_upc::with('nutrition')
                                                ->where('upc', $upc)
                                                ->get();
                   if($nutrition_data->count() > 0){
                    $result = $nutrition_data;
                   }

                }


            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function calculateMonthlyCalories($user_id, Request $request){

        $result= [];
        $error =false;
        $status_code = 200;

        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::with(['profile'])->find($user_id);

                if($user != null){
                    $calories_goal = 0;
                    $user_calorie_goals = \App\Models\User_calories_goal::where('user_id',$user_id)->first();
                    if($user_calorie_goals != null){
                        $calories_goal = $user_calorie_goals->calorie_intake_g;
                    }else{

                        if($user->profile != null){
                          $calories_goal = $user->profile->calories;
                         }
                    }
                    $req_month = $request->get('req_month');
                    $req_year = $request->get('req_year');

                    $month = $req_month ? $req_month : date('m');
                    $year = $req_year ? $req_year : date('Y');



                    $nutrition_user = \App\Models\Nutrition_user::where('user_id',$user_id)
                                                         ->whereMonth('serving_date', '=', $month)
                                                         ->whereYear('serving_date', '=', $year)
                                                         ->selectRaw('serving_date, sum(calories) as sum')
                                                         ->groupBy('serving_date')
                                                         ->pluck('sum','serving_date');


                    $water_intake_user = \App\Models\WaterIntake::where('user_id',$user_id)
                                                         ->whereMonth('log_date', '=', $month)
                                                         ->whereYear('log_date', '=', $year)
                                                         ->selectRaw('log_date, sum(log_water) as sum')
                                                         ->groupBy('log_date')
                                                         ->pluck('sum','log_date');

                    $temp_date = $year."-".$month."-01";
                    $month_day = date("t", strtotime($temp_date));
                    $data1 = array();

                    $data = [];
                    for($i=1; $i<=$month_day; $i++){


                        $total_cal = 0;
                        $total_water_intake = 0;

                        $month_date = strtotime($i."-".$month."-".$year);



                        foreach ($nutrition_user as $key1 => $nutrition_value) {

                            $nutrition_date = strtotime($key1);

                            if($month_date == $nutrition_date){
                                $total_cal = $nutrition_value;
                                break;
                            }
                        }

                        foreach ($water_intake_user as $key2 => $water_value) {

                            $waterlog_date = strtotime($key2);

                            if($month_date == $waterlog_date){
                                $total_water_intake = $water_value;
                                break;
                            }
                        }

                        $total_consumed = $total_cal;
                        $remaining_calories = $calories_goal - $total_consumed;
                        $weekly_data_day_count = max( $total_consumed, $total_water_intake );
                        if( $weekly_data_day_count > 0 ){

                            $data1['total_consumed'] = $total_consumed;

                            $data1['water_intake'] = round($total_water_intake/8);
                            $data1['date'] = date('Y-m-d', $month_date);
                            $data1['day'] = date('d', $month_date);
                            $data1['month'] = date('m', $month_date);
                            $data1['year'] = date('Y', $month_date);

                            if($remaining_calories > 0){

                                $data1['diff_text'] = "Deficit";
                                $data1['diff_text_color'] = "#33CC33";

                            }
                            elseif($remaining_calories < 0){

                                $data1['diff_text'] = "Excess";
                                $data1['diff_text_color'] = "#FF1A1A";
                            }
                            else{

                                $data1['diff_text'] = "Neutral";
                                $data1['diff_text_color'] = "#FDE674";
                            }
                            $data1['diff'] = -$remaining_calories;
                            $data[] = $data1;

                        }

                    }

                    $result['date_wise'] = $data;
                    $result['average'] = $this->getAverageCalorie($user_id, $calories_goal);



                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function getAverageCalorie($user_id, $calorie_goal, Request $request){

        $result= [];
        $error =false;
        $status_code = 200;

        // get weekly average data
        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::with(['profile'])->find($user_id);

                if($user != null){

                    $req_month = $request->get('req_month');
                    $req_year = $request->get('req_year');

                    $month = $req_month ? $req_month : date('m');
                    $year = $req_year ? $req_year : date('Y');

                    $weekly_data = [];
                    $temp_date = $year."-".$month."-01";
                    $month_day = date("t", strtotime($temp_date));
                    $first_day_this_month = date('Y-m-d', strtotime('01-'.$month.'-'.$year)); // hard-coded '01' for first day
                    $last_day_this_month  = date('Y-m-d', strtotime($month_day.'-'.$month.'-'.$year));

                    $month_weeks = array();
                    for($date = $first_day_this_month; $date <= $last_day_this_month; $date = date('Y-m-d', strtotime($date. ' + 7 days')))
                    {
                        $week =  date('W', strtotime($date));
                        $year =  date('Y', strtotime($date));

                        $from = date("Y-m-d", strtotime("{$year}-W{$week}-0"));

                        if($from < $first_day_this_month) $from = $first_day_this_month;
                        $to = date("Y-m-d", strtotime("{$year}-W{$week}-6"));

                        if($to > $last_day_this_month) $to = $last_day_this_month;

                        $month_weeks[] = array("start_date" => $from, "end_date" => $to );

                    }

                    $week_no = 1;
                    $total_consumed_monthly = 0;
                    $total_water_intake_monthly = 0;
                    for($j=0; $j < count($month_weeks); $j++){

                        $start_date = date('Y-m-d', strtotime($month_weeks[$j]['start_date']));
                        $end_date = date('Y-m-d', strtotime($month_weeks[$j]['end_date']));

                        $datetime1 = date_create($start_date);
                        $datetime2 = date_create($end_date);
                        $diff_days = date_diff($datetime1, $datetime2);
                        $diff_days = $diff_days->format('%d');
                        $diff_days = $diff_days +1;

                        $weekly_total_consumed = 0;

                        $weekly_current_calories = 0;

                        $total_cal_week_date = 0;


                        $nutrition_user_weekly = \App\Models\Nutrition_user::where('user_id',$user_id)
                                                                        ->whereBetween('serving_date', [$start_date, $end_date]);

                                                                /*->where('serving_date', '>=', $start_date)
                                                                ->where('serving_date', '<=', $end_date);*/
                                                            //
                        $weekly_nutrition_count = $nutrition_user_weekly->where('calories', '<>', '0')->groupBy('serving_date')->get(); // is not null

                        $nutrition_user_weekly = $nutrition_user_weekly->selectRaw('serving_date, sum(calories) as sum')
                                                            ->pluck('sum','serving_date');

                       /* $nutrition_user_weekly = \App\Models\Nutrition_user::where('user_id',$user_id)
                                                ->whereBetween('serving_date', [$start_date, $end_date])
                                                ->selectRaw('serving_date, sum(calories) as sum')
                                                ->pluck('sum','serving_date');*/

                        $total_weekly_nutritions = 0;
                        foreach ($nutrition_user_weekly as $key2 => $value2) {
                            if(!empty($value2)){
                                $total_weekly_nutritions = $total_weekly_nutritions + $value2;

                            }
                        }

                        // water intake
                        $water_intake_weekly = \App\Models\WaterIntake::where('user_id',$user_id)
                                                             ->whereBetween('log_date', [$start_date, $end_date]);

                        $weekly_intake_count = $water_intake_weekly->where('log_water', '<>', '0')->groupBy('log_date')->get(); // is not null

                        $water_intake_weekly = $water_intake_weekly->selectRaw('log_date, sum(log_water) as sum')
                                                                    ->pluck('sum','log_date');

                        $total_weekly_water_intake = 0;
                        foreach ($water_intake_weekly as $key3 => $value3) {
                            if(!empty($value3)){
                                $total_weekly_water_intake = $total_weekly_water_intake + $value3;
                            }
                        }
                        // end water intake

                        $weekly_data_day_count = max( sizeof($weekly_nutrition_count) , sizeof($total_weekly_water_intake) );

                        if($diff_days>0 && $weekly_data_day_count >0){

                            $total_consumed_monthly = $total_consumed_monthly + $total_weekly_nutritions;
                            $total_water_intake_monthly = $total_water_intake_monthly + $total_weekly_water_intake;

                            $weekly_remaining_calories = ( ($calorie_goal * $diff_days) - $total_weekly_nutritions) / $diff_days;

                            $weekly_data1 = array();

                            if( $weekly_remaining_calories && $total_weekly_nutritions ){

                                $weekly_data1['total_consumed_weekly_avg'] = round( $total_weekly_nutritions/$diff_days , 2 );
                                $weekly_data1['total_weekly_water_intake_avg'] = round( ( $total_weekly_water_intake/8 ) / $diff_days , 1 );

                                if($weekly_remaining_calories>0){

                                    $weekly_data1['weekly_diff_text_avg'] = "Deficit";
                                    $weekly_data1['weekly_diff_text_class_avg'] = "text-deficit";

                                }
                                elseif($weekly_remaining_calories<0){

                                    $weekly_data1['weekly_diff_text_avg'] = "Excess";
                                    $weekly_data1['weekly_diff_text_class_avg'] = "text-excess";
                                }
                                else{

                                    $weekly_data1['weekly_diff_text_avg'] = "Neutral";
                                    $weekly_data1['weekly_diff_text_class_avg'] = "text-neutral";
                                }

                                $weekly_data1['weekly_diff_avg'] = - round($weekly_remaining_calories , 2 );

                                $weekly_data[$j+1] = $weekly_data1;
                            }else{
                               $weekly_data[$j+1] = [];
                            }
                        }
                        else{
                            $weekly_data[$j+1] = [];
                        }

                    }
                    //exit;
                    $result['week_wise_avg'] = $weekly_data;

                    $monthly_average = [];

                    $monthly_average['monthly_consumed_avg'] = round( ($total_consumed_monthly) / $month_day , 2);
                    $monthly_average['monthly_water_avg'] = round( ($total_water_intake_monthly/8) / $month_day , 1);
                    $monthly_remain_calorie = round( (($calorie_goal *$month_day) - $total_consumed_monthly)/ $month_day , 2);

                    if($monthly_remain_calorie>0){

                        $monthly_average['monthly_diff_text_avg'] = "Deficit";
                        $monthly_average['monthly_diff_text_class_avg'] = "text-deficit";

                    }
                    elseif($weekly_remaining_calories<0){

                        $monthly_average['monthly_diff_text_avg'] = "Excess";
                        $monthly_average['monthly_diff_text_class_avg'] = "text-excess";
                    }
                    else{

                        $monthly_average['monthly_diff_text_avg'] = "Neutral";
                        $monthly_average['monthly_diff_text_class_avg'] = "text-neutral";
                    }

                    $monthly_average['monthly_diff_avg'] = -$monthly_remain_calorie;

                    $result['monthly_avg'] = $monthly_average;
                }
                else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        // end: get weekly average data
        return $result;
    }

    public function user_all_logged_food($user_id){

        $user_food = \App\Models\Nutrition_user::with('nutritions')
                                                           ->where('user_id',$user_id)
                                                           ->where('is_custom',0)
                                                           ->get();
        $user_custom_food = \App\Models\Nutrition_user::with('custom_nutritions')
                                               ->where('user_id',$user_id)
                                               ->where('is_custom',1)
                                               ->get();

        $logged_food = new \Illuminate\Database\Eloquent\Collection;

        if($user_food != null){
            $logged_food = $user_food;
        }

        if( $user_custom_food != null){

            foreach ($user_custom_food as $key => $value) {
                $value['nutritions'] = $value['custom_nutritions'];
                unset($value['custom_nutritions']);
                $logged_food->push($value);
            }

        }

        return $logged_food;
    }

    public function getUserFoodNote($user_id, $log_date){

        $result = [];
        $error =false;
        $status_code = 200;

        $today = date("Y-m-d");

        if(empty($log_date)){
            $log_date = $today;
        }


        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::find($user_id);
                if($user != null){

                    $result = \App\Models\User_diary_note::where('user_id', $user_id)
                                                             ->where('log_date', $log_date)
                                                             ->first();

                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }

        return $result;
    }

    public function deleteFavouriteFoodByFoodId($nutrition_id, Request $request){

        $error = false;
        $status_code = 200;
        $result =[];

        if(!empty($nutrition_id)) {
            try{
                    $user_id = $request->get('user_id');
                    $user = \App\Models\User::find($user_id);
                    if($user != null){

                          $favourite_nutrition = \App\Models\Favourite_nutrition_user::where('nutrition_id', $nutrition_id)
                                                                                        ->where('user_id', $user_id)->first();
                          if($favourite_nutrition){
                              $favourite_nutrition->delete();
                              $result['message'] = 'Favourite Food Removed Successfully.';
                          }
                          else{
                              $error = true;
                              $result['error_message'] = 'record does not exist.';
                          }
                    }
                    else{
                        $error = true;
                        $status_code = 404;
                        $result['message']='No Patient Found.';

                    }

               }catch(\Exception $ex){

                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function check_is_favourite($nutrition_id, $is_custom){

        if($nutrition_id){
            $is_favourite = \App\Models\Favourite_nutrition_user::where('nutrition_id',$nutrition_id)
                                                                ->where('is_custom', $is_custom)
                                                                ->first();
            if($is_favourite){

               return 1;
            }
            else{

                return 0;
            }
        }

        return null;
    }

    public function getTotalWaterIntake($user_id, $date)
    {
        $total_waterlog = \App\Models\WaterIntake::where('user_id',$user_id)
                                ->where('log_date',$date)
                                ->sum('log_water');

        $total_waterlog_cup = round($total_waterlog/8, 1);

        return $total_waterlog_cup;
    }

    public function deleteWater($log_date, $user_id){

        $error = false;
        $status_code = 200;
        $result = [];

        try{
                $last_water_intake = \App\Models\WaterIntake::where('user_id', $user_id)
                                                    ->whereDate('log_date', '=', $log_date)
                                                    ->orderBy('id', 'desc')
                                                    ->first();

                if($last_water_intake!=null){

                    $delete_last_water = $last_water_intake->delete();

                    if($delete_last_water){
                        $result['message'] = 'Water Intake Removed Successfully.';
                    }
                    else{

                        $error = true;
                        $result['error_message'] = "Water Intake Not Removed Successfully.";
                    }
                }
                else{
                        $error = true;
                        $status_code = 404;
                        $result['error_code'] = 404;
                        $result['error_message'] = "record does not exist";
                }
        }
        catch(\Exception $ex){

            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }


        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function loggedFoodGroupBySceduledTime($user_id, $date){

        $user_food = \App\Models\Nutrition_user::with('nutritions')
                                                   ->where('user_id',$user_id)
                                                   ->whereDate('serving_date','=', $date)
                                                   ->where('is_custom',0)
                                                   ->get();

        $user_custom_food = \App\Models\Nutrition_user::with('custom_nutritions')
                                       ->where('user_id',$user_id)
                                       ->whereDate('serving_date','=', $date)
                                       ->where('is_custom',1)
                                       ->get();

        $logged_nutrition = new \Illuminate\Database\Eloquent\Collection;

        if($user_food->toArray() != null){
            $logged_nutrition = $user_food;
        }

        if( $user_custom_food != null){

            foreach ($user_custom_food as $key => $value) {
                $value['nutritions'] = $value['custom_nutritions'];
                unset($value['custom_nutritions']);
                $logged_nutrition->push($value);
            }

        }

        if($logged_nutrition->count() > 0){
            $logged_nutrition = $logged_nutrition->sortByDesc('id')->groupBy('schedule_time');
            return $logged_nutrition;
        }

        return $logged_nutrition;
    }

    public function createMeal(Request $request){

        $error = false;
        $status_code = 200;
        $result = [];

        try{

            $input = $request->all();
            $user_id = $input['user_id'];
            $food_type = $input['food_type'];
            $logged_date = date('Y-m-d', strtotime($input['logged_date']));

            $logged_nutrition = $input['logged_nutritions'];



            if($logged_nutrition!=null){

                if(isset($input['guid_server'])){

                    $current_timestamp = strtotime(date("Y-m-d H:i"));
                    $input['guid'] = guid();
                    $input['timestamp'] = $current_timestamp;
                }

                $create_meal = \App\Models\User_meals::create($input);



                if(!empty($create_meal)){

                    $last_id = $create_meal->id;
                    $last_guid = $create_meal->guid;

                    $logged_nutrition_count = count($logged_nutrition);

                    for($i=0; $i<$logged_nutrition_count; $i++){

                        if(isset($input['guid_server'])){

                            $logged_nutrition[$i]['guid'] = guid();
                            $logged_nutrition[$i]['timestamp'] = strtotime(date("Y-m-d H:i"));
                        }

                        $logged_nutrition[$i]['created_at'] = date('Y-m-d H:i:s');
                        $logged_nutrition[$i]['updated_at'] = date('Y-m-d H:i:s');

                        $logged_nutrition[$i]['meal_id'] = $create_meal->id;
                    }


                    $meal_foods = $create_meal->meal_foods()->insert($logged_nutrition);
                    if($meal_foods){

                        $result['id'] = $last_id;
                        $result['guid'] = $last_guid;

                        $last_inserted_meal_foods = \App\Models\User_meal_foods::where('meal_id', $last_id)
                                                               ->orderBy('id', 'desc')
                                                               ->take($logged_nutrition_count)
                                                               ->get();
                        $inserted_meal_foods = [];
                        foreach ($last_inserted_meal_foods as $value) {
                            $data = [];

                            $data['id'] = $value->id;
                            $data['guid'] = $value->guid;
                            $inserted_meal_foods[] = $data;
                        }

                        $result['meal_foods'] = $inserted_meal_foods;

                        $result['meassage'] = "Meal added successfully";
                    }
                    else{

                        $error = true;
                        $result['error_message'] = "Meal foods Not added successfully";
                    }

                }
                else{

                    $error = true;
                    $result['error_message'] = "Meal Not added successfully";
                }
            }
            else{

                $error = true;
                $result['error_message'] = "Meal foods are not exist";
            }

        }
        catch(\Exception $ex){

            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function getMeals($user_id, $call_from = "client"){

        $error = false;
        $status_code = 200;
        $result = [];

        try{

            $user = \App\Models\User::patient()->find($user_id);

            if($user != null){

                $user_meals = \App\Models\User_meals::with('meal_foods')->where('user_id', $user_id)->get();
                //dd($user_meals->toArray());
                foreach ($user_meals as $meal_foods) {

                    $cal_sum = 0;
                    foreach ($meal_foods['meal_foods'] as $meal_food) {

                        $cal_sum += intval($meal_food['calories']);


                        if($meal_food['nutritions']['nutrition_data']){
                            $meal_food['nutritions']['nutrition_data'] = convertStringToArray($meal_food['nutritions']['nutrition_data']);
                        }

                        if($meal_food['nutritions']['serving_data']){
                            $meal_food['nutritions']['serving_data'] = convertStringToArray($meal_food['nutritions']['serving_data']);
                        }

                    }

                    $meal_foods['total_calories'] = $cal_sum;
                }

                $result = $user_meals;
            }else{

                $error = true;
                $status_code = 404;
                $result['message']='No Patient Found.';
            }
        }
        catch(\Exception $ex){

            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }

        if($call_from == 'server'){
            return $result;
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function getMeal($meal_id, $call_from = "client"){

        $error = false;
        $status_code = 200;
        $result = [];

        try{


            $user_meal = \App\Models\User_meals::with('meal_foods')->find($meal_id);

               /* $cal_sum = 0;
                foreach ($meal_foods['meal_foods'] as $value) {

                    $cal_sum += intval($value['calories']);
                }

                $user_meal['total_calories'] = $cal_sum;*/

            $result = $user_meal;

        }
        catch(\Exception $ex){

            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }

        if($call_from == 'server'){
            return $result;
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function logMeal(Request $request){

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();

        if(!empty($input)) {
            try{

                $user_id = $input['user_id'];
                if($user_id!=null){
                    $logged_foods = $input['user_meal_foods'];
                    if($logged_foods!=null){
                        $meal_foods = [];
                        foreach ($logged_foods as $logged_food) {
                            $id = $logged_food['id'];
                            $meal_food = [];
                            if(isset($input['guid_server'])){

                                $logged_food_guid = guid();
                            }
                            else{

                                $logged_food_guid = $logged_food['guid'];
                            }

                            $user_meal_foods =  \App\Models\User_meal_foods::where('id', '=', $id)->orWhere('guid', '=', $logged_food_guid)->first();
                            if($user_meal_foods!=null){
                                $meal_food['user_id'] = $user_id;
                                $meal_food['nutrition_id'] = $user_meal_foods->nutrition_id;
                                $meal_food['calories'] = $user_meal_foods->calories;
                                $meal_food['serving_quantity'] = $user_meal_foods->serving_quantity;
                                $meal_food['serving_size_unit'] = $user_meal_foods->serving_size_unit;
                                $meal_food['no_of_servings'] = $user_meal_foods->no_of_servings;
                                $meal_food['serving_string'] = $user_meal_foods->serving_quantity." ".$user_meal_foods->serving_size_unit;
                                $meal_food['is_custom'] = $user_meal_foods->is_custom;
                                $meal_food['schedule_time'] = $input['schedule_time'];
                                $meal_food['serving_date'] = $input['serving_date'];

                                if(isset($input['guid_server'])){
                                    $current_timestamp = \Carbon\Carbon::now()->timestamp;
                                    $meal_food['guid'] = guid();
                                    $meal_food['timestamp'] = $current_timestamp;
                                }else{
                                    $meal_food['guid'] = $logged_food['guid'];
                                    $meal_food['timestamp'] = $logged_food['timestamp'];
                                }

                                $meal_food['created_at'] = date('Y-m-d H:i:s');
                                $meal_food['updated_at'] = date('Y-m-d H:i:s');

                                $meal_foods[] = $meal_food;
                            }

                        }

                        $food_logged_count = count($meal_foods);
                        if($food_logged_count>0){
                            $log_meal_foods = \App\Models\Nutrition_user::insert($meal_foods);

                            if($log_meal_foods){
                                $result['message']='Meal Logged Successfully';
                                $last_inserted = \App\Models\Nutrition_user::where('user_id', $user_id)
                                                               ->orderBy('id', 'desc')
                                                               ->take($food_logged_count)
                                                               ->get();
                                $disply_inserted_foods = [];
                                $inserted_data = [];
                                foreach ($last_inserted as $value) {
                                    $data = [];

                                    $data['id'] = $value->id;
                                    $data['guid'] = $value->guid;
                                    $inserted_data[] = $data;

                                    $disply_inserted_foods[] = $this->foodsTobePushed($value);
                                }

                                $result['log_food'] = $inserted_data;
                                $result['disply_inserted_foods'] = $disply_inserted_foods;
                            }
                            else{
                                $error = true;
                                $result['error_message']='Meal is not logged Successfully';
                            }
                        }
                        else{
                            $error = true;
                            $result['error_message']='Foods are not exist in this meal';
                        }

                    }
                    else{
                        $error = true;
                        $result['error_message']='Foods are not exist in this meal';
                    }
                }
                else{
                    $error = true;
                    $result['error_message']='No Patient Found.';
                }


            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }


    public function deleteMeal($id){

        $error = false;
        $status_code = 200;
        $result =[];
        if(!empty($id)) {
            try{

                $user_meal = \App\Models\User_meals::where('id',$id)
                                                          ->first();
                  if($user_meal != null){
                    \App\Models\User_meal_foods::where('meal_id',$id)
                                                ->delete();

                    $user_meal->delete();

                    $result['message'] = 'Meal Removed Successfully.';
                  }else{

                      $result['message'] = 'No meal found.';
                  }



                }catch(\Exception $ex){
                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function saveFoodCompleted(Request $request) {
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        $user_agent = "";
        if(!empty($input)) {
             try{
                    $user_id = $input['user_id'];
                    $user = \App\Models\User::patient()->find($user_id);
                    if($user != null){

                    $check_food_completed = $user->Food_Completed()->where('date', $input['date'])->first();
                        if(!empty($check_food_completed)){
                            $check_food_completed->update($input);
                            $id = $check_food_completed->id;
                        }
                        else{
                            $foodCompleted = $user->Food_Completed()->create($input);
                            $id = $foodCompleted->id;
                        }

                        $result['food_comepleted'] = array("id"=>$id);
                        $result['message']='Food Completed Logged Successfully.';
                    }
                    else
                    {
                        $error = true;
                        $status_code = 404;
                        $result['message']='No Patient Found.';
                }
            }
            catch(\Exception $ex)
            {
                    throw $ex;
                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
            }
        }
        else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function getFoodRecommendedTypes($call_from = "client"){

        $error = false;
        $status_code = 200;
        $result =[];

        try{

            $food_recommended_types = \App\Models\Food_recommendation_type::all();
            foreach ($food_recommended_types as $key => $value) {
                $value->id = (int) $value->id;
            }

            if($food_recommended_types){

                $result = $food_recommended_types;
            }
            else{
                $error = true;
                $result['error_message'] = 'Record does not exists.';
            }
        }
        catch(\Exception $ex){
            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }

        if($call_from == "server"){
            return $result;
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function createMdRecommendedFood(Request $request){

        $error = false;
        $status_code = 200;

        $input = $request->all();

        $result = [];
        if($input!=null) {

            try{

                $md_recommended_food = \App\Models\Recommended_food::create($input);

                if($md_recommended_food){
                    $result['message'] = "Food created successfully";
                }
                else{
                    $error = true;
                    $result['error_message'] = "Food not created successfully";
                }


            }catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function getMdRecommendedFoods($user_id, Request $request){
        $error = false;
        $status_code = 200;
        $result = [];

        $type_id = null;
        if($request->get('type_id')){
           $type_id = $request->get('type_id');
        }


        if($user_id!=null){
            try{

                $user = \App\Models\User::find($user_id);

                if($user!=null){
                    if($type_id != null){

                        if($user->gender=='male'){
                          $md_recommended_foods = \App\Models\Recommended_food::where('food_recommendation_type_id','=', $type_id)
                                                                            ->where('food_for', '<>', 'Female')
                                                                            ->get();
                        }
                        else if($user->gender=='female'){
                            $md_recommended_foods = \App\Models\Recommended_food::where('food_recommendation_type_id','=', $type_id)
                                                                            ->where('food_for', '<>', 'Male')
                                                                            ->get();
                        }
                        else{
                            $md_recommended_foods = \App\Models\Recommended_food::where('food_recommendation_type_id','=', $type_id)
                                                                                ->get();
                        }

                    }
                    else{

                        if($user->gender=='male'){
                          $md_recommended_foods = \App\Models\Recommended_food::where('food_for', '<>', 'Female')
                                                                            ->get();
                        }
                        else if($user->gender=='female'){
                            $md_recommended_foods = \App\Models\Recommended_food::where('food_for', '<>', 'Male')
                                                                            ->get();
                        }
                        else{

                            $md_recommended_foods = \App\Models\Recommended_food::all();
                        }

                    }

                    if($md_recommended_foods->count()>0){

                        foreach ($md_recommended_foods as $key => $foods_data) {

                            if($foods_data->nutritions['nutrition_data']){
                                $foods_data->nutritions['nutrition_data'] = convertStringToArray($foods_data->nutritions['nutrition_data']);
                            }

                            if($foods_data->nutritions['serving_data']){
                                $foods_data->nutritions['serving_data'] = convertStringToArray($foods_data->nutritions['serving_data']);
                            }

                            $foods_data['is_favourite'] = $this->check_is_favourite($foods_data->nutrition_id, 0);


                        }

                        $result = $md_recommended_foods;

                    }
                    else{
                        $error = true;
                        $result['error_message'] = 'Record does not exist.';
                    }
                }
                else{
                        $error = true;
                        $result['error_message'] = 'Patient does not exist';
                }

            }
            catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }
        }
        else{
                $error = true;
                $result['error_message'] = 'Request data is empty';
        }


        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function mdRecommendedFoodsDisplayData(){

        $error = false;
        $status_code = 200;
        $result = [];

        try{
            $food_recommended_types = $this->getFoodRecommendedTypes('server');

            if($food_recommended_types->count()>0){
                $food_recommended = [];
                foreach ($food_recommended_types as $key => $recommended_type) {

                    $recommended_type_id = $recommended_type->id;
                    $food_recommended['recommended_type_id'] = $recommended_type_id;
                    $food_recommended['recommended_type_name'] = $recommended_type->name;

                      $md_recommended_foods = \App\Models\Recommended_food::where('food_recommendation_type_id', '=', $recommended_type_id)
                                                                        ->get();

                    if($md_recommended_foods->count()>0){
                        $nutrition_detail = [];
                        foreach ($md_recommended_foods as $key => $foods_data) {
                            $nutrition_data=[];
                            // nutritions accessed from Recommended_food model
                            if($foods_data->nutritions['nutrition_data']){
                                $foods_data->nutritions['nutrition_data'] = convertStringToArray($foods_data->nutritions['nutrition_data']);
                            }

                            $no_of_servings = $foods_data->no_of_servings ? $foods_data->no_of_servings : 1;


                            $nutrition_data['nutrition_id'] = $foods_data->nutrition_id;
                            $nutrition_data['recommended_food_id'] = $foods_data->id;
                            $nutrition_data['food_name'] = $foods_data->nutritions['name'];
                            $nutrition_data['brand_name'] = $foods_data->nutritions['brand_name'];
                            $nutrition_data['food_for'] = $foods_data->food_for;
                            $nutrition_data['calories'] = (double) $foods_data->calories;
                            $nutrition_data['no_of_servings'] = $foods_data->no_of_servings;
                            $nutrition_data['serving_string'] = $foods_data->serving_string;
                            $nutrition_data['serving_range'] = $foods_data->serving_range;


                            $total_fat = clearFoodDataValue($foods_data->nutritions['nutrition_data']['total_fat']);
                            $nutrition_data['total_fat'] = $total_fat * $no_of_servings * $foods_data->serving_range;

                            $dietary_fiber = clearFoodDataValue($foods_data->nutritions['nutrition_data']['dietary_fiber']);
                            $nutrition_data['dietary_fiber'] = $dietary_fiber * $no_of_servings * $foods_data->serving_range;

                            $total_carb = clearFoodDataValue($foods_data->nutritions['nutrition_data']['total_carb']);
                            $nutrition_data['total_carb'] = $total_carb * $no_of_servings * $foods_data->serving_range;

                            $sodium = clearFoodDataValue($foods_data->nutritions['nutrition_data']['sodium']);
                            $nutrition_data['sodium'] = $sodium * $no_of_servings * $foods_data->serving_range;

                            $protein = clearFoodDataValue($foods_data->nutritions['nutrition_data']['protein']);
                            $nutrition_data['protein'] = $protein * $no_of_servings * $foods_data->serving_range;

                            $nutrition_detail[] = $nutrition_data;
                        }

                        $food_recommended['recommended_foods'] = $nutrition_detail;

                    }
                    else{
                        $food_recommended['recommended_foods'] = [];
                    }

                    $result[] = $food_recommended;
                }

            }
            else{
                $error = true;
                $result['error_message'] = 'Food recommended types are not exist.';
            }
        }
        catch(\Exception $ex){
            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }


        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function deleteMdRecommendedFood($id){
        $error = false;
        $status_code = 200;
        $result = [];

        if(!empty($id)){

            try{
                $md_recommended_food = \App\Models\Recommended_food::find($id);
                if(!empty($md_recommended_food)){
                    $delete = $md_recommended_food->delete();
                    if($delete){
                        $result['message'] = "Food deleted successfully";
                    }
                    else{
                        $error = true;
                        $result['error_message'] = "Food not deleted successfully";
                    }
                }
                else{
                    $error = true;
                    $result['error_message'] = "Food does not exist";
                }
            }
            catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }
        }
        else{
            $error = true;
            $result['error_message'] = "Request data is empty";
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function getMealsByUser($user_id, $call_from = "client"){

        $error = false;
        $status_code = 200;
        $result =[];
        $recommended_meals =[];

        if($user_id!=null){

            try{
                $user = \App\Models\User::with('meal_template_with_meals')->find($user_id);
                if($user){
                    if($user->meal_template_with_meals->count() > 0){
                        $meal_template_with_meals = $user->meal_template_with_meals->first();

                        $recommended_meals = $meal_template_with_meals->meals;

                        // make a custom select box display names
                        foreach ($recommended_meals as $key => $recommended_meal) {
                            $food_names = [];
                            foreach ($recommended_meal->meal_foods as $key2 => $foods) {
                                $food_names[] = $foods->nutritions->name;
                            }

                            if(count($food_names)>0){
                                $food_names_string = implode(', ', array_unique($food_names));
                                $meal_name_display = strtoupper($recommended_meal['meal_name'])." (".$food_names_string.")";
                            }
                            else{
                                $meal_name_display = strtoupper($recommended_meal['meal_name']);
                            }

                            $recommended_meals[$key]['meal_name_display'] = $meal_name_display;
                        }

                    }


                    $result = $recommended_meals;
                }
                else{
                    $error = true;
                    $result['error_message'] = "No patient found";
                }
                // end: make a custom select box display names

            }
            catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }


        }
        else{
            $error = true;
            $result['error_message'] = "Request data is empty";
        }

        if($call_from == "server"){
            return $result;
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    /* old code - /  public function getRecommendedMealFoods($meal_id){

        $error = false;
        $status_code = 200;
        $result =[];

        try{
            if(!empty($meal_id)){


                $recommended_meal_foods = \App\Models\Recommended_meal_food::where('meal_id', '=', $meal_id)
                                            ->get();

                $meal_item_types = \App\Models\Recommended_meal_item_type::all();

                foreach ($meal_item_types as $item) {
                    $item_data = [];
                    $item_data['item_id'] = $item->id;
                    $item_data['item_name'] = $item->item_name;

                    $food_data = [];
                    foreach ($recommended_meal_foods as $meal_food) {
                        $data = [];
                        if($item->id == $meal_food->item_id){
                            $data['recommended_meal_food_id'] = $meal_food->id;
                            $data['nutrition_id'] = $meal_food->nutrition_id;

                            $data['no_of_servings'] = $meal_food->no_of_servings;
                            $data['serving_string'] = $meal_food->serving_string;
                            $data['calories'] = $meal_food->calories;
                            $data['serving_quantity'] = $meal_food->serving_quantity;
                            $data['serving_size_unit'] = $meal_food->serving_size_unit;

                            $nutrition = \App\Models\Nutrition::find($meal_food->nutrition_id)->toArray();

                            if($nutrition['nutrition_data']){
                                $nutrition['nutrition_data'] = convertStringToArray($nutrition['nutrition_data']);
                            }

                            if($nutrition['serving_data']){
                                $nutrition['serving_data'] = convertStringToArray($nutrition['serving_data']);
                            }

                            $data['nutritions'] = $nutrition;
                            $food_data[] = $data;
                        }

                    }
                    $item_data['recommended_food'] = $food_data;
                    $item_data['recommended_food_count'] = count($food_data);
                    if(!empty($item_data['recommended_food'])){
                        $result[] = $item_data;
                    }

                }

            }
            else{
                $error = true;
                $result['error_message'] = "Meal does not exist";
            }
        }
        catch(\Exception $ex){
            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }*/

    public function getRecommendedMealFoods($meal_id, Request $request){
        $error = false;
        $status_code = 200;
        $result = [];

        if($request->get('user_id')){
           $user_id = $request->get('user_id');
        }

        if($user_id!=null){
            try{

                $user = \App\Models\User::find($user_id);

                if($user!=null){

                    $md_recommended_foods_all_types = \App\Models\Recommended_meal_item_type::with(['item_foods'=> function($q) use ($meal_id){
                                                                                                        $q->where('meal_id', '=', $meal_id);
                                                                                                    }])->get();

                    if($md_recommended_foods_all_types->count()>0){

                        foreach ($md_recommended_foods_all_types as $key => $all_types_foods) {

                            foreach ($all_types_foods->item_foods as $key2 => $foods_data) {

                                if($foods_data['nutritions']['nutrition_data']){
                                    $foods_data['nutritions']['nutrition_data'] = convertStringToArray($foods_data['nutritions']['nutrition_data']);
                                }

                                if($foods_data['nutritions']['serving_data']){
                                    $foods_data['nutritions']['serving_data'] = convertStringToArray($foods_data['nutritions']['serving_data']);
                                }

                                $foods_data['is_favourite'] = $this->check_is_favourite($foods_data->nutrition_id, 0);

                            }
                        }

                        $result = $md_recommended_foods_all_types;

                    }
                    else{
                        $error = true;
                        $result['error_message'] = 'Record does not exist.';
                    }
                }
                else{
                        $error = true;
                        $result['error_message'] = 'Patient does not exist';
                }

            }
            catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }
        }
        else{
                $error = true;
                $result['error_message'] = 'Request data is empty';
        }


        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function logRecommendedFoods(Request $request){

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();

        if(!empty($input)) {
            try{
                $user_id = $input[0]['user_id'];
                if($user_id!=null){
                    $user = \App\Models\User::find($user_id);

                    if($user!=null){

                        foreach ($input as $key=>$selected_foods) {

                            if(isset($selected_foods['user_agent']) && $selected_foods['user_agent']=="server"){

                                $current_timestamp = \Carbon\Carbon::now()->timestamp;
                                $input[$key]['guid'] = guid();
                                $input[$key]['timestamp'] = $current_timestamp;
                                unset($input[$key]['user_agent']);

                            }

                            $input[$key]['created_at'] = date('Y-m-d H:i:s');
                            $input[$key]['updated_at'] = date('Y-m-d H:i:s');

                        }

                        $food_logged_count = count($input);
                        $log_meal_foods = \App\Models\Nutrition_user::insert($input);
                        if($log_meal_foods){

                            $result['message'] = "Foods are logged successfully";

                            $last_inserted = \App\Models\Nutrition_user::where('user_id', $user_id)
                                                                   ->orderBy('id', 'desc')
                                                                   ->take($food_logged_count)
                                                                   ->with('nutritions')
                                                                   ->get();
                            $inserted_data = [];
                            $disply_inserted_foods = [];
                            foreach ($last_inserted as $value) {
                                $data = [];

                                $data['id'] = $value->id;
                                $data['guid'] = $value->guid;
                                $inserted_data[] = $data;

                                $disply_inserted_foods[] = $this->foodsTobePushed($value);

                            }

                            $result['logged_food'] = $inserted_data;
                            $result['disply_inserted_foods'] = $disply_inserted_foods;
                        }
                        else{

                            $result['error_message'] = "Foods are not logged successfully";
                        }
                    }
                    else{
                        $error = true;
                        $result['error_message'] = "Patient does not exists";
                    }
                }
                else{
                    $error = true;
                    $result['error_message'] = "Patient does not exists";
                }

            }
            catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function foodsTobePushed($value){
        $disply_inserted_foods_temp = [];
        if($value!=null){

            $value->nutritions->nutrition_data = convertStringToArray($value->nutritions->nutrition_data);
            $disply_inserted_foods_temp['id'] = $value->id;
            $disply_inserted_foods_temp['name'] = $value->nutritions->name;
            $disply_inserted_foods_temp['calories'] = $value->calories;
            $disply_inserted_foods_temp['carb'] = clearFoodDataValue($value->nutritions->nutrition_data['total_carb']) * $value->no_of_servings;
            $disply_inserted_foods_temp['fat'] = clearFoodDataValue($value->nutritions->nutrition_data['total_fat']) * $value->no_of_servings;
            $disply_inserted_foods_temp['fiber'] = clearFoodDataValue($value->nutritions->nutrition_data['dietary_fiber']) * $value->no_of_servings;
            $disply_inserted_foods_temp['food_type'] = $value->schedule_time;
            $disply_inserted_foods_temp['is_custom'] = $value->is_custom;
            $disply_inserted_foods_temp['no_of_servings'] = $value->no_of_servings;
            $disply_inserted_foods_temp['nutrition_id'] = $value->nutrition_id;
            $disply_inserted_foods_temp['protein'] = clearFoodDataValue($value->nutritions->nutrition_data['protein']) * $value->no_of_servings;
            $disply_inserted_foods_temp['serving_quantity'] = $value->serving_quantity;
            $disply_inserted_foods_temp['serving_size_unit'] = $value->serving_size_unit;
            $disply_inserted_foods_temp['sodium'] = clearFoodDataValue($value->nutritions->nutrition_data['sodium']) * $value->no_of_servings;
            $disply_inserted_foods_temp['user_id'] = $value->user_id;

        }

        return $disply_inserted_foods_temp;
    }

    public function getNutrition($nutrition_id){
        $error = false;
        $status_code = 200;
        $result = [];

        if($nutrition_id!=null){

            try{
                $nutrition = \App\Models\Nutrition::find($nutrition_id)->toArray();

                if($nutrition['nutrition_data']){
                    $nutrition['nutrition_data'] = convertStringToArray($nutrition['nutrition_data']);
                }

                if($nutrition['serving_data']){
                    $nutrition['serving_data'] = convertStringToArray($nutrition['serving_data']);
                }

                $result = $nutrition;
            }
            catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }
        else{
            $error = true;
            $result['error_message'] = "Request data is empty";
        }


        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function updateRecommendedFood(Request $request, $id)
    {

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        if(!empty($input) && $id > 0) {
            try{

                 $recommended_food = \App\Models\Recommended_food::where('id',$id)->first();

                 if(!empty($recommended_food)){
                    $recommended_food->update($input);
                    $result['message']='Food Updated Successfully.';
                 } else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Food Found.';
                 }
               }catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function getMdRecommendedFoodsFront($user_id, $call_from = "client"){
        $error = false;
        $status_code = 200;
        $result = [];

        if($user_id!=null){
            try{

                $user = \App\Models\User::find($user_id);

                if($user!=null){

                    if($user->gender=='male'){
                      $md_recommended_foods_all_types = \App\Models\Food_recommendation_type::with(['recommended_foods'=> function($q){
                                                                                                        $q->where('food_for', '<>', 'Female');
                                                                                                    }])->get();
                    }
                    else if($user->gender=='female'){
                        $md_recommended_foods_all_types = \App\Models\Food_recommendation_type::with(['recommended_foods'=> function($q){
                                                                                                        $q->where('food_for', '<>', 'Male');
                                                                                                    }])->get();
                    }
                    else{

                        $md_recommended_foods_all_types = \App\Models\Food_recommendation_type::with('recommended_foods')->get();
                    }

                    if($md_recommended_foods_all_types->count()>0){

                        foreach ($md_recommended_foods_all_types as $key => $all_types_foods) {

                            foreach ($all_types_foods->recommended_foods as $key2 => $foods_data) {

                                if($foods_data['nutritions']['nutrition_data']){
                                    $foods_data['nutritions']['nutrition_data'] = convertStringToArray($foods_data['nutritions']['nutrition_data']);
                                }

                                if($foods_data['nutritions']['serving_data']){
                                    $foods_data['nutritions']['serving_data'] = convertStringToArray($foods_data['nutritions']['serving_data']);
                                }

                                $foods_data['is_favourite'] = $this->check_is_favourite($foods_data->nutrition_id, 0);

                            }
                        }

                        $result = $md_recommended_foods_all_types;

                    }
                    else{
                        $error = true;
                        $result['error_message'] = 'Record does not exist.';
                    }
                }
                else{
                        $error = true;
                        $result['error_message'] = 'Patient does not exist';
                }

            }
            catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }
        }
        else{
                $error = true;
                $result['error_message'] = 'Request data is empty';
        }

        if($call_from == "server"){
            return $result;
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    // services for iphone apps
    public function foodRecommendedTypes(){

        $error = false;
        $status_code = 200;
        $result =[];

        try{

            $food_recommended_types = \App\Models\Food_recommendation_type::all();

            if($food_recommended_types){

                $result = $food_recommended_types;
            }
            else{
                $error = true;
                $result['error_message'] = 'Record does not exists.';
            }
        }
        catch(\Exception $ex){
            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function recommendedMeals($user_id){

        $error = false;
        $status_code = 200;
        $result =[];

        if($user_id!=null){

            try{

                $user = \App\Models\User::find($user_id);
                if( $user!=null ){

                    if($user->gender=='male'){
                      $md_Recommended_meals = \App\Models\Recommended_meal::where('meal_for', '<>', 'Female')
                                                                        ->get();
                    }
                    else if($user->gender=='female'){
                        $md_Recommended_meals = \App\Models\Recommended_meal::where('meal_for', '<>', 'Male')
                                                                        ->get();
                    }
                    else{
                        $md_Recommended_meals = \App\Models\Recommended_meal::all();
                    }

                    if($md_Recommended_meals!=null){
                        $result['md_Recommended_meals'] = $md_Recommended_meals;
                    }
                    else{
                        $result['md_Recommended_meals'] = [];
                    }

                    /*$md_Recommended_meals = \App\Models\Recommended_meal::with('meal_foods')
                                                                        ->get();

                    foreach ($md_Recommended_meals as $key => $value) {
                        $md_Recommended_meals[$key]['meal_foods'] = $value->meal_foods->groupBy('item_id');
                    }
                    dd($md_Recommended_meals->toArray());*/

                    /*$md_Recommended_meals =\App\Models\Recommended_meal::with(['meal_foods' => function($q){
                        $q->with('item');
                    }])->get();
                    dd($md_Recommended_meals->toArray());*/

                    /*$md_Recommended_meals =\App\Models\Recommended_meal::with('meal_items')->get();
                    print_r($md_Recommended_meals->toArray()); exit; */

                    //$md_Recommended_meals =\App\Models\Recommended_meal::with('meal_items')->get();

                    $md_recommended_items_foods =\App\Models\Recommended_meal_item_type::with('item_foods')
                                                                                        ->get();
                    if($md_recommended_items_foods!=null){
                        $result['md_recommended_items_foods'] = $md_recommended_items_foods;
                    }
                    else{
                        $result['md_recommended_items_foods'] = [];
                    }
                }
                else{
                    $error = true;
                    $result['error_message'] = "Patient does not exist";
                }
            }
            catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }
        }
        else{
            $error = true;
            $result['error_message'] = "Request data is empty";
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }
    // end : services for iphone apps

}
