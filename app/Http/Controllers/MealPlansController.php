<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MealPlansController extends Controller
{

    public function index(){

      return view('complete_plans.index');
    }

    public function dayplan(){

      return view('day_plans.index');
    }

    public function getMealPlans(Request $request){

        $error = false;
        $status_code = 200;
        $result =[];
        try {
            $result = \App\Models\Meal_plan::where('plan_type',$request->plan_type)->get()->toArray();
            if(isset($request->gender)){
               $result = \App\Models\Meal_plan::where('gender',$request->gender)
                                                ->where('plan_type','complete_plan')
               ->get()->toArray();
            }

             if(isset($request->details)){
                $result = \App\Models\Meal_plan::with('nutrition_exercise_data')->where('plan_type',$request->plan_type)->get()->toArray();
            }

        }catch(\Exception $ex){

            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function saveMealPlan(Request $request){

    	$error = false;
        $status_code = 200;

        $result = [];
        $input = $request->all();

        if(!empty($input)){

            try{

                $meal_plan = \App\Models\Meal_plan::create($input);
                if($meal_plan){

                    $result['message'] = "Meal Plan created successfully";
                    $result['meal_plan'] = $meal_plan;

                }
                else{
                    $error = true;
                    $result['error_message'] = "Meal Plan not created";
                }

            }
            catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }
        else{
            $error = true;
            $result['error_message'] = "Request data is empty";
        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));

    }

    public function delete($id)
    {
        $error = false;
        $status_code = 200;
        $result =[];

        if(!empty($id)) {
            try{
                  $meal_plan = \App\Models\Meal_plan::find($id);
                  if($meal_plan){
                      \App\Models\Nutrition_exercise_plan::where('meal_plan_id',$id)->delete();
                      \App\Models\Nutrition_exercise_day_time::where('meal_plan_id',$id)->delete();
                      \App\Models\Meal_day_plan::where('complete_plan_id',$id)->delete();
                      \App\Models\Meal_day_plan_config::where('complete_plan_id',$id)->delete();

                      $meal_plan->delete();
                      $result['message'] = 'Meal Plan deleted Successfully.';
                  }
                  else{
                      $error = true;
                      $result['error_message'] = 'Meal Plan does not exist.';
                  }


               }catch(\Exception $ex){

                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function updatePlan($id,Request $request){
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        if(!empty($input) && $id > 0) {
            try{

                 $meal_plan = \App\Models\Meal_plan::find($id);
                 if($meal_plan != null){
                    $meal_plan->name = $input['name'];
                    $meal_plan->gender = $input['gender'];
                    $meal_plan->save();

                    $result['message'] = 'Meal Plan Updated Successfully.';
                 }else{
                   $error = true;
                   $result['error_message'] = 'Meal Plan does not exist.';
                 }
               }catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function updateMealPlan($id,Request $request){
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();

        if(!empty($input) && $id > 0) {
            try{

                $completePlanData = $input['completePlanData'];
                $dayPlanData = $input['dayPlanData'];
                $daysTemplateArr = $input['daysTemplateArr'];

                // check plan is exist

                $checkMealPlan = \App\Models\Nutrition_exercise_plan::where('meal_plan_id',$id)->get();
                if($checkMealPlan->count() > 0){
                    \App\Models\Nutrition_exercise_plan::where('meal_plan_id',$id)->delete();
                     \App\Models\Nutrition_exercise_day_time::where('meal_plan_id',$id)->delete();
                }


                $dayPlan_ids = $this->saveMealDayPlan($dayPlanData,$id);

                if(count($dayPlan_ids) > 0){

                  $meal_plan_details = \App\Models\Meal_plan::with('nutrition_exercise_data')
                                                            ->whereIn('id',$dayPlan_ids)
                                                            ->where('plan_type','day_plan')->get();

                    foreach ($meal_plan_details as $key => $day_plan) {
                        $nutri_day_data =  $day_plan['nutrition_exercise_data'];
                        $day_plan_details = \App\Models\Meal_day_plan::with('config')
                                                                        ->where('day_plan_id',$day_plan['id'])
                                                                        ->where('complete_plan_id',$id)
                                                                        ->first();


                        foreach ($nutri_day_data as $key2 => $nx) {
                            $daysConfigArray = $day_plan_details['config'];
                            $finalDaysConfigArray = [];
                            foreach ($daysConfigArray as $dayConfig) {
                                $dayKey =  $nx['master_template_id'] . "-" . $dayConfig['dayNumber'];
                                if (!in_array($dayKey, $daysTemplateArr)) {
                                    $finalDaysConfigArray[] = $dayConfig;
                                }
                            }



                            $day_nutri_ex_data =[];
                            $day_nutri_ex_data['meal_plan_id'] = $id;
                            $day_nutri_ex_data['master_template_id'] = $nx['master_template_id'];
                            $day_nutri_ex_data['meal_id'] = $nx['meal_id'];
                            $day_nutri_ex_data['workout_id'] =$nx['workout_id'];
                            $day_nutri_ex_data['day_plan_id'] =$day_plan['id'];
                            $day_nutri_ex_data['meal_template_type'] = 'complete_day_template';
                            $nutrition_exercise_plan = \App\Models\Nutrition_exercise_plan::create($day_nutri_ex_data);
                            $config_day_obj =  $this->saveConfigDayObj($nutrition_exercise_plan->id,$id,$nx['config'],$finalDaysConfigArray);
                        }
                    }
                }


                foreach ($completePlanData as $key => $ne) {
                    $nutri_ex_data =[];
                    $nutri_ex_data['meal_plan_id'] = $id;
                    $nutri_ex_data['master_template_id'] = $ne['master_template_id'];
                    $nutri_ex_data['meal_id'] = $ne['meal_id'];
                    $nutri_ex_data['workout_id'] =$ne['workout_id'];
                    $nutri_ex_data['meal_template_type'] = 'complete_meal_template';
                    $nutrition_exercise_com = \App\Models\Nutrition_exercise_plan::create($nutri_ex_data);
                    if($ne['action'] == 'add'){
                        $this->saveCompletePlanConfig($nutrition_exercise_com->id,$id,$ne['template_days'],$ne['template_time'],$ne['notification'],$ne['action']);
                    }else{
                        $this->saveCompletePlanConfig($nutrition_exercise_com->id,$id,$ne['template_days'],$ne['template_time'],$ne['notification'],$ne['action']);
                    }


                }

               }catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    private function saveMealDayPlan($dayPlanData,$complete_plan_id){

        $checkDayPlan = \App\Models\Meal_day_plan::where('complete_plan_id',$complete_plan_id)->get();
        if($checkDayPlan->count() > 0){
            \App\Models\Meal_day_plan::where('complete_plan_id',$complete_plan_id)->delete();
            \App\Models\Meal_day_plan_config::where('complete_plan_id',$complete_plan_id)->delete();
        }
        $day_plan_id =[];
        foreach ($dayPlanData as $key => $dp) {
            if($dp['day_plan_id'] > 0 ){

                $meal_day_data =[];
                $meal_day_data['complete_plan_id'] = $complete_plan_id;
                $meal_day_data['day_plan_id'] = $dp['day_plan_id'];
                $meal_day_plan = \App\Models\Meal_day_plan::create($meal_day_data);
                $this->saveMealDayPlanConfig($meal_day_plan->id,$complete_plan_id,$dp['day_plan_days']);
                $day_plan_id[] = $dp['day_plan_id'];
            }

        }
        return $day_plan_id;
    }

    private function saveConfigDayObj($id,$meal_plan_id,$nutrition_ex_time,$meal_day_plan){

        $config =[];
        foreach ($meal_day_plan as $key => $value) {

            $config['dayName']=$value['dayName'];
            $config['dayNumber']=$value['dayNumber'];
            $config['meal_time']=$nutrition_ex_time[0]['meal_time'];
            $config['notification']=1;
            $config['guid']=guid();
            $config['meal_plan_id'] = $meal_plan_id;
            $this->saveNutritionExerciseDayTime($id,$config);
        }

    }



    public function updateDayPlan($id,Request $request){
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();

        if(!empty($input) && $id > 0) {
            try{
               // dd($input['nutritionExerciseData']);
                $nutritionExerciseData = $input['nutritionExerciseData'];
                $meal_plan_id = $id;
                // check plan is exist
                $checkMealPlan = \App\Models\Nutrition_exercise_plan::where('meal_plan_id',$id)->get();
                if($checkMealPlan->count() > 0){
                    \App\Models\Nutrition_exercise_plan::where('meal_plan_id',$id)->delete();
                     \App\Models\Nutrition_exercise_day_time::where('meal_plan_id',$id)->delete();
                }

                foreach ($nutritionExerciseData as $key => $ne) {
                    if(isset($ne['meal_time'])){

                        $nutri_ex_data =[];
                        $nutri_ex_data['meal_plan_id'] = $id;
                        $nutri_ex_data['master_template_id'] = $key;
                        $nutri_ex_data['meal_id'] = isset($ne['meal_id'])?$ne['meal_id']:null;
                        $nutri_ex_data['workout_id'] =isset($ne['workout_id'])?$ne['workout_id']:null;
                        $nutri_ex_data['meal_template_type'] ='day_meal_template';
                        $config_obj = ["dayName"=>"ALL",
                                        "dayNumber"=>"0",
                                        "meal_plan_id"=>$id,
                                        "meal_time"=>$ne['meal_time'],
                                        "notification"=>1,
                                        "guid"=>guid()
                                    ];
                        $nutrition_exercise_plan =  \App\Models\Nutrition_exercise_plan::create($nutri_ex_data);
                        $this->saveNutritionExerciseDayTime($nutrition_exercise_plan->id,$config_obj);
                    }
                }

               }catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    private function saveNutritionExerciseDayTime($id,$config){

       $day_time_data =[];
       $day_time_data['nutrition_exercise_plan_id'] = $id;
       $day_time_data['meal_plan_id'] = $config['meal_plan_id'];
       $day_time_data['dayName'] = $config['dayName'];
       $day_time_data['dayNumber'] = $config['dayNumber'];
       $day_time_data['meal_time'] = floatval($config['meal_time']);
       $day_time_data['notification'] = $config['notification'];
       $day_time_data['guid'] = $config['guid'];
       \App\Models\Nutrition_exercise_day_time::create($day_time_data);
    }

    public function getMealPlanDetails($id){

        $error =false;
        $status_code = 200;
        if($id !="" && $id > 0){
            try{

                $meal_plan = \App\Models\Meal_plan::with('nutrition_exercise_data')->find($id);
                $day_plan_details = \App\Models\Meal_day_plan::with('config')->where('complete_plan_id',$id)->get();
                 $result['day_plan_details'] =  $day_plan_details;
                if($meal_plan != null){
                    $result['meal_plan_details'] =  $meal_plan;
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Meal Plan Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    //get data user if plan is assign to user
    public function getUserMealPlanDetails($meal_plan_id, Request $request){

        $error =false;
        $status_code = 200;
        $user_id = $request->get('user_id');

        if($user_id != null ) {
            try {
                $user_meal_plan = \App\Models\User_meal_plan::with(['new_meal_plan','nutrition_exercise_data'])
                                                                ->where('meal_plan_id',$meal_plan_id)
                                                                ->where('user_id',$user_id)
                                                                ->first();
                $day_plan_details = \App\Models\User_meal_day_plan::with('config')
                                                                     ->where('user_id',$user_id)
                                                                     ->where('complete_plan_id',$meal_plan_id)->get();
                $result['day_plan_details'] =  $day_plan_details;
                if($user_meal_plan != null) {
                    $result['meal_plan_details'] =  $user_meal_plan;
                }
                else {
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Meal Plan Found.';
                }
            } catch(\Exception $ex) {
               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();
            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    // for iPhone app function only

    public function getMealPlanAppDetails($id){

        $error =false;
        $status_code = 200;
        if($id !="" && $id > 0){
            try{

                $meal_plan = \App\Models\Meal_plan::with('nutrition_exercise_app')->find($id);
                if($meal_plan != null){
                    $result['meal_plan_details'] =  $meal_plan;
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Meal Plan Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function assignPlanToUser(Request $request){

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();

        if(!empty($input)) {
            try{

                $user_id = $input['user_id'];
                $meal_plan_id = $input['meal_plan_id'];

                // check plan is exist
                $user_meal_plan_count = \App\Models\User_meal_plan::where('user_id',$user_id)->get();

                if($user_meal_plan_count->count() > 0) {
                    $user_nutrition_data = \App\Models\User_nutrition_exercise_plan::where('user_meal_plan_id', '=' ,$user_meal_plan_count[0]->id)->get();
                    if(count($user_nutrition_data) > 0) {
                        foreach ($user_nutrition_data as $key2 => $value2) {
                            \App\Models\User_meal_plan_day_time::where('user_nutrition_exercise_plan_id',$value2->id)->delete();
                        }
                        \App\Models\User_nutrition_exercise_plan::where('user_meal_plan_id', '=' ,$user_meal_plan_count[0]->id)->delete();
                    }
                    \App\Models\User_meal_plan::where('user_id',$user_id)->delete();

                    $user_meal_day_plan = \App\Models\User_meal_day_plan::where('user_id',$user_id)
                                                                        ->where('complete_plan_id',$meal_plan_id)->get();
                    if($user_meal_day_plan->count() > 0) {

                        foreach ($user_meal_day_plan as $key3 => $u_meal_day_plan){
                            \App\Models\User_meal_day_plan_config::where('user_meal_day_plan_id',$u_meal_day_plan->id)->delete();

                        }
                        \App\Models\User_meal_day_plan::where('user_id',$user_id)
                                                        ->where('complete_plan_id',$meal_plan_id)->delete();
                    }
                }
               $user_meal_plan = \App\Models\User_meal_plan::create($input);
               $user_meal_plan_id = $user_meal_plan->id;
               $meal_plan_details = \App\Models\Meal_plan::with('nutrition_exercise_data')->find($meal_plan_id);
               $meal_day_plan_details = \App\Models\Meal_day_plan::with('config')->where('complete_plan_id',$meal_plan_id)->get();

               foreach ($meal_plan_details['nutrition_exercise_data'] as $key => $meal_plan) {

                    $user_nutri_ex_data =[];
                    $user_nutri_ex_data['user_meal_plan_id'] = $user_meal_plan->id;
                    $user_nutri_ex_data['master_template_id'] = $meal_plan['master_template_id'];
                    $user_nutri_ex_data['meal_id'] = $meal_plan['meal_id'];
                    $user_nutri_ex_data['workout_id'] = $meal_plan['workout_id'];
                    $user_nutri_ex_data['day_plan_id'] = $meal_plan['day_plan_id'];
                    $user_nutri_ex_data['meal_template_type'] = $meal_plan['meal_template_type'];

                    $nutrition_exercise_com = \App\Models\User_nutrition_exercise_plan::create($user_nutri_ex_data);

                    foreach ($meal_plan['config'] as $key1 => $value) {

                       $user_meal_plan_data =[];
                       $user_meal_plan_data['user_nutrition_exercise_plan_id'] = $nutrition_exercise_com->id;
                       $user_meal_plan_data['meal_plan_id'] = $value['meal_plan_id'];
                       $user_meal_plan_data['dayName'] = $value['dayName'];
                       $user_meal_plan_data['dayNumber'] =$value['dayNumber'];
                       $user_meal_plan_data['meal_time'] =$value['meal_time'];
                       $user_meal_plan_data['notification'] = $value['notification'];
                       $user_meal_plan_data['guid'] = $value['guid'];

                        \App\Models\User_meal_plan_day_time::create($user_meal_plan_data);
                    }

                }

                if($meal_day_plan_details->count() > 0){

                    foreach ($meal_day_plan_details as $key4 => $meal_day_plan) {

                        $user_meal_day_data =[];
                        $user_meal_day_data['user_id'] = $user_id;
                        $user_meal_day_data['complete_plan_id'] = $meal_day_plan['complete_plan_id'];
                        $user_meal_day_data['day_plan_id'] = $meal_day_plan['day_plan_id'];


                        $user_meal_day_plan = \App\Models\User_meal_day_plan::create($user_meal_day_data);

                        foreach ($meal_day_plan['config'] as $key5 => $day_plan_config) {

                            $user_day_config_data =[];
                            $user_day_config_data['user_meal_day_plan_id'] = $user_meal_day_plan->id;
                            $user_day_config_data['complete_plan_id'] = $day_plan_config['complete_plan_id'];
                            $user_day_config_data['dayName'] = $day_plan_config['dayName'];
                            $user_day_config_data['dayNumber'] =$day_plan_config['dayNumber'];
                            $user_day_config_data['guid'] = $day_plan_config['guid'];

                            \App\Models\User_meal_day_plan_config::create($user_day_config_data);
                        }

                    }

                }

               }catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function unassignPlanToUser(Request $request){

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();

        if(!empty($input)) {
            try{

                $user_id = $input['user_id'];
                // check plan is exist

                $user_meal_plan_count = \App\Models\User_meal_plan::where('user_id',$user_id)->get();

                if($user_meal_plan_count->count() > 0) {
                    $user_nutrition_data = \App\Models\User_nutrition_exercise_plan::where('user_meal_plan_id', '=' ,$user_meal_plan_count[0]->id)->get();
                    if(count($user_nutrition_data) > 0) {
                        foreach ($user_nutrition_data as $key2 => $value2) {
                            \App\Models\User_meal_plan_day_time::where('user_nutrition_exercise_plan_id',$value2->id)->delete();
                        }
                        \App\Models\User_nutrition_exercise_plan::where('user_meal_plan_id', '=' ,$user_meal_plan_count[0]->id)->delete();
                    }
                    \App\Models\User_meal_plan::where('user_id',$user_id)->where('meal_plan_id',$input['meal_plan_id'])->delete();

                    $user_meal_day_plan = \App\Models\User_meal_day_plan::where('user_id',$user_id)->get();
                    if($user_meal_day_plan->count() > 0) {

                        foreach ($user_meal_day_plan as $key3 => $u_meal_day_plan){
                            \App\Models\User_meal_day_plan_config::where('user_meal_day_plan_id',$u_meal_day_plan->id)->delete();

                        }
                        \App\Models\User_meal_day_plan::where('user_id',$user_id)->delete();
                    }

                }



               }catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function newUserMealPlan($user_id, Request $request){

        $error =false;
        $status_code = 200;
        $server_timestamp = \Carbon\Carbon::now()->timestamp;
        $result =[];
        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::find($user_id);
                if($user != null){
                    $timestamp = $request->get('timestamp');
                    $compare_date = gmdate("Y-m-d H:i:s", $timestamp);
                    if($timestamp != null){

                        $user_meal_plan = \App\Models\User_meal_plan::where('user_id',$user_id)->first();
                        if($user_meal_plan != null){

                            $user_meal_plan = \App\Models\User_meal_plan::with(['new_meal_plan','user_nutrition_exercise_plan'])
                                ->where('user_id',$user_id)
                                ->where('updated_at','>=',$compare_date)
                                ->orWhere('deleted_at','>=',$compare_date)
                                ->get();

                        }

                    }else{
                        $user_meal_plan = \App\Models\User_meal_plan::with(['new_meal_plan','user_nutrition_exercise_plan'])->where('user_id',$user_id)->first();

                    }

                        $result['user_meal_plan'] =  $user_meal_plan;


                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='User not Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'timestamp'=>$server_timestamp,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    private function saveCompletePlanConfig($id,$meal_plan_id,$template_days,$template_times,$notification,$action){

        $week_day_arr =['0'=>'ALL','1'=>'MON','2'=>'TUE',
                        '3'=>'WED','4'=>'THU','5'=>'FRI',
                        '6'=>'SAT','7'=>'SUN'];
        $config =[];

        foreach ($template_days as $key => $value) {

            $config['dayName']=$week_day_arr[$value];
            $config['dayNumber']=$value;
            $config['notification']=0;
            if($action == 'add'){
                $config['meal_time']=$template_times[$value]['meal_time'];
            }else if($action == 'update'){
               $config['meal_time']=$template_times[$value];
            }

            foreach($notification as $key =>$notifi){
                if($notifi == $value){
                    $config['notification']=1;
                }
            }
            $config['guid']=guid();
            $config['meal_plan_id']=$meal_plan_id;

            //print_r($config);
            $this->saveNutritionExerciseDayTime($id,$config);
        }
        //exit();
    }

    private function saveMealDayPlanConfig($id,$cid,$dayPlanDays){
         $week_day_arr =['0'=>'ALL','1'=>'MON','2'=>'TUE',
                        '3'=>'WED','4'=>'THU','5'=>'FRI',
                        '6'=>'SAT','7'=>'SUN'];

        foreach ($dayPlanDays as $key => $value) {
            $temp =[];
            $temp['dayName']=$week_day_arr[$value];
            $temp['dayNumber']=$value;
            $temp['complete_plan_id']=$cid;
            $temp['meal_day_plan_id']=$id;
            $temp['guid']=guid();
           \App\Models\Meal_day_plan_config::create($temp);
        }
    }

    public function updateUserMealPlan($user_id,Request $request){
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();

        if(!empty($input) && $user_id > 0) {
            try{

                $user = \App\Models\User::patient()->find($user_id);
                if($user != null){

                    $user_meal_plan_day_time_id = $input['id'];
                    $user_meal_plan_day_time = \App\Models\User_meal_plan_day_time::find($user_meal_plan_day_time_id);
                    if($user_meal_plan_day_time != null){
                        $user_meal_plan_day_time->update($input);
                        $result['message']='User Schdule Updated Successfully.';
                    }else{
                        $error = true;
                        $status_code = 404;
                        $result['message']='No User Schdule Found.';
                    }


                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }

            }catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
            ->setCallback(\Request::input('callback'));

    }

    public function getUserMealPlan($user_id){

         $error =false;
         $status_code = 200;
         if($user_id !="" && $user_id > 0){
             try{

                 $user = \App\Models\User::find($user_id);
                 if($user != null){
                     $user_meal_plan = \App\Models\User_meal_plan::with('meal_plan')->where('user_id',$user_id)->get();
                     $result['user_meal_plan'] =  $user_meal_plan;
                 }else{
                     $error = true;
                     $status_code = 404;
                     $result['message']='User not Found.';

                 }

             }catch(\Exception $ex){

                 $error = true;
                 $status_code =$ex->getCode();
                 $result['error_code'] = $ex->getCode();
                 $result['error_message'] = $ex->getMessage();

             }
         }
         return response()->json(['error' => $error,'response'=>$result],$status_code)
             ->setCallback(\Request::input('callback'));



    }

    public function updateUserMealPlanDetail($id,Request $request){
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();

        if(!empty($input) && $id > 0) {
            try{

                $completePlanData = $input['completePlanData'];
                $dayPlanData = $input['dayPlanData'];
                $daysTemplateArr = $input['daysTemplateArr'];
                $user_id = $input['user_id'];

                // check user meal plan is exist

                $checkUserMealPlan = \App\Models\User_meal_plan::where('meal_plan_id',$id)
                                                                ->where('user_id',$user_id)
                                                                ->get();


                if($checkUserMealPlan->count() > 0) {
                    $user_nutrition_data = \App\Models\User_nutrition_exercise_plan::where('user_meal_plan_id', '=' ,$checkUserMealPlan[0]->id)->get();
                    if(count($user_nutrition_data) > 0) {
                        foreach ($user_nutrition_data as $key2 => $value2) {
                            \App\Models\User_meal_plan_day_time::where('user_nutrition_exercise_plan_id',$value2->id)->delete();
                        }
                        \App\Models\User_nutrition_exercise_plan::where('user_meal_plan_id', '=' ,$checkUserMealPlan[0]->id)->delete();
                    }


                    $user_meal_day_plan = \App\Models\User_meal_day_plan::where('user_id',$user_id)
                                                                        ->where('complete_plan_id',$id)->get();
                    if($user_meal_day_plan->count() > 0) {

                        foreach ($user_meal_day_plan as $key3 => $u_meal_day_plan){
                            \App\Models\User_meal_day_plan_config::where('user_meal_day_plan_id',$u_meal_day_plan->id)->delete();

                        }
                        \App\Models\User_meal_day_plan::where('user_id',$user_id)
                            ->where('complete_plan_id',$id)->delete();
                    }
                }


                $dayPlan_ids = $this->saveUserMealDayPlan($dayPlanData,$id,$user_id);

                if(count($dayPlan_ids) > 0){

                    $day_plan_details = \App\Models\Meal_plan::with('nutrition_exercise_data')
                        ->whereIn('id',$dayPlan_ids)
                        ->where('plan_type','day_plan')->get();

                    foreach ($day_plan_details as $key => $day_plan) {
                        $nutri_day_data =  $day_plan['nutrition_exercise_data'];
                        $user_day_plan_details = \App\Models\User_meal_day_plan::with('config')
                                                                                ->where('day_plan_id',$day_plan['id'])
                                                                                ->where('complete_plan_id',$id)
                                                                                ->where('user_id',$user_id)
                                                                                ->first();


                        foreach ($nutri_day_data as $key2 => $nx) {
                            $daysConfigArray = $user_day_plan_details['config'];
                            $finalDaysConfigArray = [];
                            foreach ($daysConfigArray as $dayConfig) {
                                $dayKey =  $nx['master_template_id'] . "-" . $dayConfig['dayNumber'];
                                if (!in_array($dayKey, $daysTemplateArr)) {
                                    $finalDaysConfigArray[] = $dayConfig;
                                }
                            }



                            $day_nutri_ex_data =[];
                            $day_nutri_ex_data['user_meal_plan_id'] = $checkUserMealPlan[0]->id;
                            $day_nutri_ex_data['master_template_id'] = $nx['master_template_id'];
                            $day_nutri_ex_data['meal_id'] = $nx['meal_id'];
                            $day_nutri_ex_data['workout_id'] =$nx['workout_id'];
                            $day_nutri_ex_data['day_plan_id'] =$day_plan['id'];
                            $day_nutri_ex_data['meal_template_type'] = 'complete_day_template';
                            $user_nutrition_exercise_plan = \App\Models\User_nutrition_exercise_plan::create($day_nutri_ex_data);
                            $config_day_obj =  $this->saveUserConfigDayObj($user_nutrition_exercise_plan->id,$id,$nx['config'],$finalDaysConfigArray);
                        }
                    }
                }


                foreach ($completePlanData as $key => $ne) {
                    $nutri_ex_data =[];
                    $nutri_ex_data['user_meal_plan_id'] = $checkUserMealPlan[0]->id;
                    $nutri_ex_data['master_template_id'] = $ne['master_template_id'];
                    $nutri_ex_data['meal_id'] = $ne['meal_id'];
                    $nutri_ex_data['workout_id'] =$ne['workout_id'];
                    $nutri_ex_data['meal_template_type'] = 'complete_meal_template';
                    $user_nutrition_exercise_com = \App\Models\User_nutrition_exercise_plan::create($nutri_ex_data);
                    if($ne['action'] == 'add'){
                        $this->saveUserCompletePlanConfig($user_nutrition_exercise_com->id,$id,$ne['template_days'],$ne['template_time'],$ne['notification'],$ne['action']);
                    }else{
                        $this->saveUserCompletePlanConfig($user_nutrition_exercise_com->id,$id,$ne['template_days'],$ne['template_time'],$ne['notification'],$ne['action']);
                    }


                }

            }catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
            ->setCallback(\Request::input('callback'));

    }

    public function saveUserMealDayPlan($dayPlanData,$complete_plan_id,$user_id){

        $day_plan_id =[];
        foreach ($dayPlanData as $key => $dp) {
            if($dp['day_plan_id'] > 0 ){

                $user_meal_day_data =[];
                $user_meal_day_data['complete_plan_id'] = $complete_plan_id;
                $user_meal_day_data['day_plan_id'] = $dp['day_plan_id'];
                $user_meal_day_data['user_id'] = $user_id;
                $user_meal_day_plan = \App\Models\User_meal_day_plan::create($user_meal_day_data);
                $this->saveUserMealDayPlanConfig($user_meal_day_plan->id,$complete_plan_id,$dp['day_plan_days']);
                $day_plan_id[] = $dp['day_plan_id'];
            }

        }
        return $day_plan_id;

    }

    private function saveUserMealDayPlanConfig($id,$cid,$dayPlanDays){
        $week_day_arr =['0'=>'ALL','1'=>'MON','2'=>'TUE',
            '3'=>'WED','4'=>'THU','5'=>'FRI',
            '6'=>'SAT','7'=>'SUN'];

        foreach ($dayPlanDays as $key => $value) {
            $temp =[];
            $temp['dayName']=$week_day_arr[$value];
            $temp['dayNumber']=$value;
            $temp['complete_plan_id']=$cid;
            $temp['user_meal_day_plan_id']=$id;
            $temp['guid']=guid();
            \App\Models\User_meal_day_plan_config::create($temp);
        }
    }

    private function saveUserConfigDayObj($id,$meal_plan_id,$nutrition_ex_time,$meal_day_plan){

        $config =[];
        foreach ($meal_day_plan as $key => $value) {

            $config['dayName']=$value['dayName'];
            $config['dayNumber']=$value['dayNumber'];
            $config['meal_time']=$nutrition_ex_time[0]['meal_time'];
            $config['notification']=1;
            $config['guid']=guid();
            $config['meal_plan_id'] = $meal_plan_id;
            $this->saveUserNutritionExerciseDayTime($id,$config);
        }

    }

    private function saveUserNutritionExerciseDayTime($id,$config){

        $day_time_data =[];
        $day_time_data['user_nutrition_exercise_plan_id'] = $id;
        $day_time_data['meal_plan_id'] = $config['meal_plan_id'];
        $day_time_data['dayName'] = $config['dayName'];
        $day_time_data['dayNumber'] = $config['dayNumber'];
        $day_time_data['meal_time'] = floatval($config['meal_time']);
        $day_time_data['notification'] = $config['notification'];
        $day_time_data['guid'] = $config['guid'];
        \App\Models\User_meal_plan_day_time::create($day_time_data);
    }

    private function saveUserCompletePlanConfig($id,$meal_plan_id,$template_days,$template_times,$notification,$action){

        $week_day_arr =['0'=>'ALL','1'=>'MON','2'=>'TUE',
            '3'=>'WED','4'=>'THU','5'=>'FRI',
            '6'=>'SAT','7'=>'SUN'];
        $config =[];

        foreach ($template_days as $key => $value) {

            $config['dayName']=$week_day_arr[$value];
            $config['dayNumber']=$value;
            $config['notification']=0;
            if($action == 'add'){
                $config['meal_time']=$template_times[$value]['meal_time'];
            }else if($action == 'update'){
                $config['meal_time']=$template_times[$value];
            }

            foreach($notification as $key =>$notifi){
                if($notifi == $value){
                    $config['notification']=1;
                }
            }
            $config['guid']=guid();
            $config['meal_plan_id']=$meal_plan_id;

            //print_r($config);
            $this->saveUserNutritionExerciseDayTime($id,$config);
        }
        //exit();
    }



}
