<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('deal.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $error = false;
        $status_code = 200;

        $result = [];
        $input = $request->all();
        if(!empty($input)){
            try{

                $created = \App\Models\Deal::create($input);
                if($created){

                    $result['message'] = "Deal created successfully";
                }
                else{
                    $error = true;
                    $result['error_message'] = "Deal not created";
                }

            }
            catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }
        else{
            $error = true;
            $result['error_message'] = "Request data is empty";
        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));


        //dd($input);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $error = false;
        $status_code = 200;
        $result = [];

        if(!empty($id)){
            try{
                $result = \App\Models\Deal::find($id);
            }
            catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }
        }
        else{
            $error = true;
            $result['error_message'] = "Request data is empty";
        }


        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();

        if(!empty($input) && $id > 0) {
            try{

                    $deal =  \App\Models\Deal::find($id);
                    if($deal != null){

                         $deal->update($input);
                         $result['message']='Deal updated Successfully.';

                    }else{

                        $error = true;
                        $status_code = 404;
                        $result['message']='No record Found.';
                    }


            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $error = false;
        $status_code = 200;
        $result =[];

        if(!empty($id)) {
            try{
                  $deal = \App\Models\Deal::find($id);
                  if($deal){
                      $file_name = $deal['image'];
                      $destination_path = public_path().'/deal-images/'.$file_name;
                      @unlink($destination_path);
                      $deal->delete();
                      $result['message'] = 'Deal deleted Successfully.';
                  }
                  else{
                      $error = true;
                      $result['error_message'] = 'record does not exist.';
                  }


               }catch(\Exception $ex){

                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function uploadImage(Request $request){

        $file_name = 'no file';
        if ($request->hasFile('image_file')) {
            $file_name = $request->file('image_file')->getClientOriginalName();
            $destination_path = 'deal-images/';
            $request->file('image_file')->move($destination_path, $file_name);
        }

        return response()->json(['error' => false,'file_name'=>$file_name],200)
                         ->setCallback(\Request::input('callback'));

    }

    public function removeImage($file_name){

        $destination_path = public_path().'/deal-images/'.$file_name;
        @unlink($destination_path);
        return response()->json(['error' => false,'file_name'=>$file_name],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function getDealList(Request $request){

        $error = false;
        $status_code = 200;
        $result =[];
        $server_timestamp = \Carbon\Carbon::now()->timestamp;
        try{
                $timestamp = $request->get('timestamp');
                $compare_date = gmdate("Y-m-d H:i:s", $timestamp);
                if($timestamp != null){

                    $result = \App\Models\Deal::where('updated_at','>=',$compare_date)
                                                ->orWhere('deleted_at','>=',$compare_date)
                                                ->withTrashed()
                                                ->get();
                }
                else{
                    $result = \App\Models\Deal::all();
                }


        }catch(\Exception $ex){

            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }

        return response()->json(['error' => $error,'timestamp'=>$server_timestamp,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }
    public function getDealListFront(){

        $error = false;
        $status_code = 200;
        $result =[];

        try{
                $result = \App\Models\Deal::where('is_expired', '<>', '1')
                        ->get();


        }catch(\Exception $ex){

            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }
}
