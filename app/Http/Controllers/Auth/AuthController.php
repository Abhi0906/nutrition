<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\Repositories\Auth0Interface;


class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectPath = "/dashboard";   // dded Darshan date 18-12-15
   // protected $redirectPath = "/patient";
    protected $loginPath = "/admin";
    protected $redirectAfterLogout = "/auth/login";
     protected $auth0Repository;
    //protected $username = 'username';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
       public function __construct(Auth0Interface $auth0Repository)

        {
            $this->auth0Repository = $auth0Repository;
            $this->middleware('guest', ['except' => 'getLogout']);
        }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'username' => 'required|min:3',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function authLogin(Request $request){

        $input = $request->all();
        $error =false;
        $status_code = 200;
          if(!empty($input)){

            try{
                 $result = $this->auth0Repository->login($input);
                 if(isset($result['error']) && $result['error']){
                    $error = true;
                    $result['message'] = 'Invalid credential';

                 }else{
                    $user = \App\Models\User::where("email",$input['email'])->first();
                    \Auth::login($user); 
                 }
                 
                           
            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

          }else{
            $error = true;
            $status_code = 400;
            $result['message']= 'Request data is empty';
          }
          
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

   
}
