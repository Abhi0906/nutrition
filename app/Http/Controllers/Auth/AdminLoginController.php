<?php
namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
class AdminLoginController extends Controller
{
    /**
     * Show the application’s login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        //echo Auth::user()->id;exit;
        return view('auth.login');
    }

    protected function guard(){
        return Auth::guard('admin');
    }

    use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/patient';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }


    public function superadminlogin(Request $request){
        $data = $request->input();
        //echo 'dfsdfdfg';exit;
       $credentials = array(
            'email'     => $request->email,
            'password'  => $request->password
        );
        
        if (auth()->attempt($credentials)) {
            
            return redirect('admin/patient')->with('success','Login success');
        }else{
            return redirect('admin/login')->with('error','Incorrect email or password.');
        }
    }
}
