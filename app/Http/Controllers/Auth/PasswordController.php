<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Mail\Message;
use Illuminate\Http\Request;
use App\Models\Medicine;
use App\Http\Requests;
use App\Repositories\Auth0Interface;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */


    use ResetsPasswords;
    protected $redirectTo = '/patient';
    protected $auth0Repository;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct(Auth0Interface $auth0Repository)
    {
       // $this->middleware('guest');
        $this->auth0Repository = $auth0Repository;
    }


    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postEmail(Request $request)
    {
      $input = $request->all();
      $validator = \Validator::make($request->all(), ['email' => 'required|email']);
      if($validator->fails()) {
        $result['message'] = $validator->errors()->first();
        return \Response::json(['error' => true, 'response' => $result], 200);
      }
      $curl = curl_init();
      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://genemedicsnutrition.auth0.com/dbconnections/change_password",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => '{"client_id": "HMIHO5fMRh5hc6rGCNuGOqhcFBCQLQ5O","email": "' . $input['email'] . '","connection": "Username-Password-Authentication"}',
        CURLOPT_HTTPHEADER => array(
          "content-type: application/json"
        ),
      ));
      $response = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);
      if ($err) {
        $result['message'] = $err;
        return \Response::json(['error' => true, 'response' => $result], 200);
      } else {
        $result['message'] = $response;
        return \Response::json(['error' => false, 'response' => $result], 200);
      }
        /*$response = \Password::sendResetLink($request->only('email'), function (Message $message) {
            $message->subject($this->getEmailSubject());
        });

        switch ($response) {
            case \Password::RESET_LINK_SENT:
                $result['message'] =  trans($response);
                return \Response::json(['error' => false, 'response' => $result], 200);

            case \Password::INVALID_USER:
                $result['message'] =  trans($response);
                return \Response::json(['error' => true, 'response' => $result], 200);
        }*/
    }



    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postReset(Request $request)
    {
        $rules = [
            'token'    => 'required',
            'email'    => 'required|email',
            'password' => 'required|confirmed|min:6',
        ];

        $validator = \Validator::make($request->all(), $rules);

        if($validator->fails()) {
            $error = true;
            $result['message']=$validator->errors()->all();
            return response()->json(['error' => $error, 'response'=>$result],200)
                              ->setCallback(\Request::input('callback'));
        }

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $response = \Password::reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);

        });
        \Session::flash('reset_password','Your Password Change Successfully');
        switch ($response) {
            case \Password::PASSWORD_RESET:
                return \Response::json(['error' => false, 'status' => trans($response)], 200);

            default:
                return \Response::json(['error' => true, 'status' => trans($response)], 200);
        }
    }


    protected function resetPassword($user, $password)
    {
        $user->password = bcrypt($password);

        $user->save();
        $update_data['password'] = $password;
        $this->auth0Repository->updateUser($user->auth0_user_id,$update_data);

       // Auth::login($user);
    }


      /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postCreate(Request $request)
    {
        $error = false;
        $rules = [
            'token'    => 'required',
            'email'    => 'required|email',
            'password' => 'required|confirmed|min:6',
        ];

        $validator = \Validator::make($request->all(), $rules);

        if($validator->fails()) {
            $error = true;
            $result['message']=$validator->errors()->all();
            return response()->json(['error' => $error, 'response'=>$result],200)
                              ->setCallback(\Request::input('callback'));
        }
        $input = $request->all();
        $user = \App\Models\User::where('email',$input['email'])->first();
        if($user != null){
          $input['email_verified']= true;
          $input['first_name']= $user->first_name;
          $input['last_name']= $user->last_name;
          $auth0User =  $this->auth0Repository->create($input);
          $user->auth0_user_id = $auth0User['user_id'];
          $user->email_varify = 1;
          $user->password = bcrypt($input['password']);
          $result =  $user->save();
          \Session::flash('reset_password','Your Password Change Successfully');
        }else{
             $error = true;
             $result['message']='User not found.';
        }
       return response()->json(['error' => $error, 'response'=>$result],200)
                              ->setCallback(\Request::input('callback'));
    }

    public function sendResetLink(Request $request){

      $validator = \Validator::make($request->all(), ['email' => 'required|email']);

      if($validator->fails()) {
            $result['message'] = $validator->errors()->first();
            return \Response::json(['error' => true, 'response' => $result], 200);
      }

      $email = $request->email;
      $isExist =  $this->auth0Repository->checkEmailExist($email);

      if($isExist){

           $response = \Password::sendResetLink($request->only('email'), function (Message $message) {
              $message->subject($this->getEmailSubject());
           });

            switch ($response) {
                case \Password::RESET_LINK_SENT:
                    $result['message'] =  trans($response);
                    return \Response::json(['error' => false, 'response' => $result], 200);

                case \Password::INVALID_USER:
                    $result['message'] =  trans($response);
                    return \Response::json(['error' => true, 'response' => $result], 200);
            }

       }else{

        $result['message'] =  'We can\'t find a user with that e-mail address';
        return \Response::json(['error' => true, 'response' => $result], 200);

       }


    }

  public function changedPassword(Request $request){

      $input = $request->all();
      $error =false;
      $status_code = 200;
      if(!empty($input)){

          try{

            //check the validation
            $validator = \Validator::make($request->all(), [
               'email' => 'required|email|max:255',
               'old_password' => 'required',
               'new_password' => 'required'

            ]);

             if($validator->fails()){
                $error = true;
                $result['message']=$validator->errors();
                 return response()->json(['error' => $error,
                                         'response'=>$result],200)
                                  ->setCallback(\Request::input('callback'));
            }

              $email = $request->email;
              $checkUser = \App\Models\User::where('email',$email)->first();

              if($checkUser != null){
                 $hashedPassword = $checkUser->password;
                 $auth0_user_id = $checkUser->auth0_user_id;
                 if (\Hash::check($request->old_password, $hashedPassword)) {

                      $update_data['password'] =$request->new_password;
                      $this->auth0Repository->updateUser($auth0_user_id,$update_data);

                     //Change the password
                      $checkUser->fill([
                          'password' => \Hash::make($request->new_password)
                      ])->save();
                       $result['message'] =  'Password has been updated.';

                 }else{

                     $error = true;
                     $result['message'] =  'Old Password does not match';

                 }

              }else{
                 $error = true;
                 $result['message'] =  'We can\'t find a user with that e-mail address';
              }


             }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

      }else{
        $error = true;
        $status_code = 400;
        $result['message']= 'Request data is empty';
      }
      return response()->json(['error' => $error,'response'=>$result],$status_code)
                     ->setCallback(\Request::input('callback'));



  }


}
