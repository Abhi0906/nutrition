<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Auth::check()){
            return redirect('/patient');
        }

        return view('pages.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function termsAndPrivacy(Request $request){
        $input = $request->all();
        $layout = false;
        if(isset($input['layout'])){
             $layout = $input['layout'];
        }

        return view('pages.terms_and_privacy')->with('layout',$layout);
    }

    public function whyChooseUs(Request $request){
         $input = $request->all();
         $layout = false;
        if(isset($input['layout'])){
             $layout = $input['layout'];
        }
        return view('pages.why_us')->with('layout',$layout);
    }

    public function sendEmail(Request $request)
    {

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();

        if(!empty($input)) {
            try{

             \App\Models\Contact::create($input);

              $email_data = array();
              $email_data['subject']='Inquiry from genemedicshealth.com';
             if(app()->environment() == 'local'){
                $email_data['to_email']= ['md@genemedics.com', 'gh@odoo.genemedics.com'];
             }else{
                $email_data['to_email']= "md@genemedics.com";
             }

              $email_data['name'] = $input['name'];
              $email_data['email_address'] = $input['email'];
              $email_data['text_message'] = $input['message'];
              $email_data['datetime'] = date('F j, Y, h:i:s A');
              $email_data['ip_address'] = $request->ip();
              $template_name = 'contact_email';
              $email_template_name = 'emails.'.$template_name;
             // echo  $email_template_name;
              //dd($email_data);

               \Mail::send($email_template_name, $email_data, function($message) use ($email_data)
                {
                    $message->to($email_data['to_email'])->subject($email_data['subject']);
                });

            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }
}
