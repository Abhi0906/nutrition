<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class SleepController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sleep_goal = \App\Models\User_sleep_goal::where('user_id',\ Auth::User()->id)->first();
        if(is_null($sleep_goal)){
            $sleep_data['user_id']=\ Auth::User()->id;
            $sleep_data['goal']=8;
            $sleep_data['start_date']=date('Y-m-d');
            $sleep_data['goal_date']=addDayswithdate(date('Y-m-d'),7);
           \App\Models\User_sleep_goal::create($sleep_data);
        }
        return view('front.sleep')->with('user_id',\ Auth::User()->id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {

        $error = false;
        $status_code = 200;
        $result =[];
        if($id != "" && $id > 0) {
            try{
                  $user_sleep = \App\Models\User_sleep::find($id);
                  if($user_sleep){
                      $guid = $user_sleep->guid;
                      $user_sleep->delete();
                      $result["guid"] = $guid;
                      $result['message'] = 'User Sleep Removed Successfully.';
                  }
                  else{
                      $error = true;
                      $result['error_message'] = 'Record does not exists.';
                  }
               }catch(\Exception $ex){
                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function logSleep(Request $request){

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        $user_agent = "";

        if(!empty($input)) {
            try{

                $user_id = $input['user_id'];

                $user = \App\Models\User::find($user_id);

                $input['created_by'] = $user_id;
                $input['updated_by'] = $user_id;
                //check session
                if(Session::has('orig_user') ){
                  $input['created_by'] = Session::get( 'orig_user' );
                  $input['updated_by'] = Session::get( 'orig_user' );

                }


                if($user != null){
                    if(isset($input['guid_server'])){

                        $current_timestamp = \Carbon\Carbon::now()->timestamp;
                        $input['guid'] = guid();
                        $input['timestamp'] = $current_timestamp;
                    }

                    $check_user_sleep = $user->User_sleep()->where('guid', $input['guid'])
                                                      ->first();

                    if(!empty($check_user_sleep)){

                        $check_user_sleep->update($input);
                        $id = $check_user_sleep->id;
                        $guid = $check_user_sleep->guid;
                    }
                    else{

                        $log_sleep = $user->User_sleep()->create($input);
                        $id = $log_sleep->id;
                        $guid = $log_sleep->guid;
                    }

                    // for iPhone notification
                    if($user_agent != "" && $user_agent == 'server'){
                        $notification['user_id'] = $user_id;
                        $notification['notification_text'] = 'LOG_SLEEP_ADDED';
                        sendNotification($notification);

                    }

                    $result['log_sleep'] = array("id"=>$id, "guid"=>$guid);
                    $result['message']='Sleep Logged Successfully.';
                }else{

                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }


            }catch(\Exception $ex){
                throw $ex;
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }
    public function loggedSleep($user_id, Request $request){

        $error =false;
        $status_code = 200;
        $server_timestamp = \Carbon\Carbon::now()->timestamp;

        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::find($user_id);

                if($user != null){

                    $timestamp = $request->get('timestamp');

                    $compare_date = gmdate("Y-m-d H:i:s", $timestamp);

                    if($timestamp != null){

                       $user_logged_sleep = \App\Models\User_sleep::where('user_id',$user_id)
                                                            ->where('updated_at','>=',$compare_date)
                                                            ->orWhere('deleted_at','>=',$compare_date)
                                                            ->withTrashed()->get();
                    }else{
                      /*$user_sleep_goals = $user->User_sleep_goal()->get();
                       dd($user_sleep_goals); */
                       $user_logged_sleep = \App\Models\User_sleep::where('user_id',$user_id)
                                                            ->get();
                    }
                    if($user_logged_sleep != null){
                       $logged_sleep = $user_logged_sleep;
                    }

                    $result = $logged_sleep;

                }else{

                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'timestamp'=>$server_timestamp,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function getUserSleep($user_id){

         $error =false;
         $status_code = 200;
         $server_timestamp = \Carbon\Carbon::now()->timestamp;
         if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::find($user_id);
                $last_sleep_arr =[];
                $final_arr =[];
                $chart_data =[];

                if($user != null){

                    $user_logged_sleep = \App\Models\User_sleep::where('user_id',$user_id)
                                                                ->orderBy('log_date','desc')
                                                                ->get();


                    if($user_logged_sleep->count() > 0){
                       $sleep_log_date_arr = $user_logged_sleep->groupBy("log_date")->take(4);

                       foreach ($sleep_log_date_arr as $key1 => $log_date) {

                            $intervals = $this->createIntervals($log_date);
                            $overlapTime = $this->overlapTime($intervals);
                            $total_bed_hours =  $this->timeStampDiff($overlapTime);

                            $date_time_formats = $this->getDateTimeIntervals($overlapTime);


                            $total_awakened_minutes =  $log_date->sum("times_awakened_minutes");
                            $total_minutes_fall_alseep =  $log_date->sum("minutes_fall_alseep");
                            $total_times_awakened =  $log_date->sum("times_awakened");

                            $final_arr[$key1]['total_bed_hours'] =$total_bed_hours;
                            $final_arr[$key1]['date_time_formats'] =$date_time_formats;
                            $final_arr[$key1]['total_times_awakened'] =$total_times_awakened;
                            $final_arr[$key1]['total_awakened_minutes'] =  $total_awakened_minutes;
                            $final_arr[$key1]['total_minutes_fall_alseep'] =  $total_minutes_fall_alseep;
                            $demical_min =($total_awakened_minutes+$total_minutes_fall_alseep)/60;
                            $acutal_decimal_hours = $total_bed_hours - $demical_min;
                            $final_arr[$key1]['acutal_decimal_hours'] =  $acutal_decimal_hours;
                            $actual_hours_min = decimalToHours($acutal_decimal_hours);
                            $final_arr[$key1]['acutal_hours'] =  (int) $actual_hours_min['hours'];
                            $final_arr[$key1]['acutal_minutes'] =  (int) $actual_hours_min['min'];

                            $date=date_create($key1);
                            $date_format_log_date = date_format($date,"M-d-Y");
                            $date_format_arr = explode("-", $date_format_log_date);
                            //dd($date_format_arr);
                            $display_date = $date_format_arr[0]." - ".$date_format_arr[1]."-".((int)$date_format_arr[1]+1).", ".$date_format_arr[2];
                            $final_arr[$key1]['display_date'] = $display_date;
                       }


                        $last_sleep_data =   $user_logged_sleep->groupBy("log_date")->take(1);


                        foreach ($last_sleep_data as $key => $last_sleep) {
                          foreach ($last_sleep as $key => $value) {
                            $last_sleep_arr[$key]['minutes_fall_alseep'] =  $value['minutes_fall_alseep'];
                            $last_sleep_arr[$key]['times_awakened'] =  $value['times_awakened'];
                            $last_sleep_arr[$key]['you_went_to_bed_at'] =  date('h:i A',strtotime($value['went_to_bed']));
                            $total_bed_decimal = decimalToHours($value['total_bed']);
                            $actual_bed_decimal = decimalToHours($value['total_sleep']);
                            $last_sleep_arr[$key]['total_bed'] =  displayTimeFormat($total_bed_decimal['hours'],$total_bed_decimal['min'],'capital');
                            $last_sleep_arr[$key]['actual_sleep'] =  displayTimeFormat($actual_bed_decimal['hours'],$actual_bed_decimal['min'],'capital');
                          }
                        }

                        $first_ele_history =current($final_arr);
                        $total_last_hours = decimalToHours($first_ele_history['total_bed_hours']);
                        $result['last_sleep']= $last_sleep_arr;
                        $result['total']['total_sleep']= displayTimeFormat($total_last_hours['hours'],$total_last_hours['min'],'capital');
                        $result['total']['total_acutal_sleep']=  displayTimeFormat($first_ele_history['acutal_hours'],$first_ele_history['acutal_minutes'],'capital');
                        $result['total']['last_efficiency']= calculateEfficiency($first_ele_history['total_bed_hours'],$first_ele_history['acutal_decimal_hours']);
                        $chart_data = $this->createChartData($first_ele_history);
                    }

                     $sleep_goal = \App\Models\User_sleep_goal::where('user_id',$user_id)->first();
                     $sleep_progress = new \App\Common\Sleep_progress($sleep_goal);

                     $result['sleep_progess'] =$sleep_progress->getSleepProgress();
                     $result['sleep_history'] = $final_arr;
                     $result['chart_data'] = json_encode($chart_data);

                }else{

                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
         }
         return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function getWeeklySleep($user_id, Request $request){

         $error =false;
         $status_code = 200;
         $result=[];
         $server_timestamp = \Carbon\Carbon::now()->timestamp;
         if($user_id !="" && $user_id > 0){
            try{
                 $user = \App\Models\User::find($user_id);
                 $weekly_arr =[];
                if($user != null){
                    $from_date = $request->get('from_date');
                    $to_date = $request->get('to_date');

                    $user_logged_sleep = \App\Models\User_sleep::where('user_id',$user_id)
                                                                 ->whereDate('log_date','>=',$from_date)
                                                                 ->WhereDate('log_date','<=',$to_date)
                                                                 ->orderBy('log_date','asc')
                                                                 ->get();
                    if($user_logged_sleep->count() > 0){
                       $sleep_log_date_arr = $user_logged_sleep->groupBy("log_date");
                       foreach ($sleep_log_date_arr as $key1 => $log_date) {

                            $intervals = $this->createIntervals($log_date);

                            $overlapTime = $this->overlapTime($intervals);

                            $total_bed_hours =  $this->timeStampDiff($overlapTime);

                            $date_time_formats = $this->getDateTimeIntervals($overlapTime);

                            $total_awakened_minutes =  $log_date->sum("times_awakened_minutes");

                            $total_minutes_fall_alseep =  $log_date->sum("minutes_fall_alseep");

                            $total_times_awakened =  $log_date->sum("times_awakened");

                            $went_to_bed = $log_date->min("went_to_bed");

                            $rise_from_bed = $log_date->max("rise_from_bed");

                            $weekly_arr[$key1]['total_bed_hours'] =$total_bed_hours;
                            $weekly_arr[$key1]['total_awakened_minutes'] =  $total_awakened_minutes;
                            $weekly_arr[$key1]['total_minutes_fall_alseep'] =  $total_minutes_fall_alseep;
                            $weekly_arr[$key1]['total_times_awakened'] =  $total_times_awakened;

                            $demical_min =($total_awakened_minutes+$total_minutes_fall_alseep)/60;
                            $acutal_decimal_hours = $total_bed_hours - $demical_min;
                            $weekly_arr[$key1]['acutal_decimal_hours'] =  $acutal_decimal_hours;
                            $actual_hours_min = decimalToHours($acutal_decimal_hours);
                            $total_hours_min = decimalToHours($total_bed_hours);
                            $weekly_arr[$key1]['acutal_sleep_times'] =  displayTimeFormat($actual_hours_min['hours'],$actual_hours_min['min'],'capital');
                            $weekly_arr[$key1]['total_sleep_times'] =   displayTimeFormat($total_hours_min['hours'],$total_hours_min['min'],'capital');
                            $weekly_arr[$key1]['sleep_efficiency'] =  calculateEfficiency($total_bed_hours,$acutal_decimal_hours);

                            $date=date_create($key1);
                            $date1 =date_create($key1);
                            $date_format_log_date = date_format($date,"M-d-Y");
                            $date_format_arr = explode("-", $date_format_log_date);

                            $display_date = $date_format_arr[0]."-".$date_format_arr[1]."-".((int)$date_format_arr[1]+1).", ".$date_format_arr[2];

                            $tomarrow = date_add($date1,date_interval_create_from_date_string("1 days"));

                            $display_day =date_format($date,"D")." - ".date_format($tomarrow,"D");


                            $weekly_arr[$key1]['went_to_bed'] = date('h:i A',strtotime($went_to_bed));
                            $weekly_arr[$key1]['rise_from_bed'] = date('h:i A',strtotime($rise_from_bed));

                            $weekly_arr[$key1]['display_date'] = $display_date;
                             $weekly_arr[$key1]['display_day'] = $display_day;

                            $weekly_arr[$key1]['date_time_formats'] =$date_time_formats;
                            $weekly_arr[$key1]['chart_data'] =$this->createChartData($weekly_arr[$key1]);
                       }

                      $result['weekly_history'] = $weekly_arr;
                    }


                }else{

                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
         }
         return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }


    public function overlapTime($intervals){
        // Overlapped intervals

        $overlapped = array();
        foreach ($intervals as $i => $a) {
            $group = [$a];
            foreach ($intervals as $j => $b) {
                if (
                    $i !== $j && (($a[0] <= $b[0] && $a[1] >= $b[0])
                        || ($a[0] <= $b[1] && $a[0] >= $b[0]))
                ) {
                    $group[] = $b;
                }
            }
            sort($group);
            $overlapped[] = $group;
        }
        // Multidimensional array_unique()
        $overlapped = array_map('unserialize',
            array_unique(array_map('serialize', $overlapped))
        );
        // Output
        $output = array();
        // Get min/max dates for each overlapped group
        foreach ($overlapped as $group) {
            $min = null;
            $max = null;
            foreach ($group as $interval) {
                if ($min === null || $interval[0] < $min) {
                    $min = $interval[0];
                }
                if ($max === null || $interval[1] > $max) {
                    $max = $interval[1];
                }
            }
            $output[] = array($min, $max);
        }

        return $output;

    }

    public function createIntervals($sleep_logged_arr){
        $intervals = [];
        foreach ($sleep_logged_arr as $key => $value) {
            $temp=[];
            $temp[0] =\Carbon\Carbon::parse($value['went_to_bed'])->timestamp;
            $temp[1] =\Carbon\Carbon::parse($value['rise_from_bed'])->timestamp;
            $intervals[] = $temp;
        }

        return $intervals;
    }

    public function timeStampDiff($overlaparr){

        $diff_arr =[];
         $aa = $this->overlapTime($overlaparr);
        foreach ($aa as $key => $value) {
            $start_date = \Carbon\Carbon::createFromTimestamp($value[0]);
            $end_date = \Carbon\Carbon::createFromTimestamp($value[1]);
            $diffHours = $start_date->diffInHours($end_date,false);
            $diffMinutes = $start_date->diffInMinutes($end_date,false);
            $diff_arr[$key] = $diffMinutes/60;
        }

        return array_sum($diff_arr);
    }

    public function getDateTimeIntervals($overlaparr){


        $dateTime =[];
        $aa = $this->overlapTime($overlaparr);
        foreach ($aa as $key => $value) {

            $start_date =  date("Y-m-d H:i:s",$value[0]);
            $end_date =  date("Y-m-d H:i:s",$value[1]);
            $dateTime[$key]['start_date'] = $start_date;
            $dateTime[$key]['end_date'] = $end_date;
        }

        return $dateTime;

    }

    public function createChartData($chart_data){

       $date_time_arr = $chart_data['date_time_formats'];
       $dataProviders = [];
       $new_hours = 0;
       $new_min =0;
       $bar_value =0;
       $first_bar_value = $chart_data['total_minutes_fall_alseep']/count($date_time_arr);
       if($chart_data['total_times_awakened'] > 0){
          $cal_bar_value = (int)($chart_data['total_awakened_minutes']/$chart_data['total_times_awakened']);
          $bar_value =  max(10,$cal_bar_value);
          $new_hours_decimal = $chart_data['total_bed_hours']/($chart_data['total_times_awakened']+1);
          $min_hours =decimalToHours($new_hours_decimal);
          $new_min = $min_hours['min'];
          $new_hours = $min_hours['hours'];
       }
          //  dd($new_hours);
          //  dd(decimalToHours($new_hours));

       foreach ($date_time_arr as $key => $value) {
        $all_days = GetDayTime($value['start_date'],$value['end_date']);

           foreach ($all_days as $key => $value) {

                $temp =[];
                $temp['column-1'] =5;
                $temp['column-2']=0;
                $old_time =   \Carbon\Carbon::createFromFormat('Y-m-d H:i',$value);
                if($key == 0){
                    $temp['column-2']= max(10,$first_bar_value);
                    $new_time =  $old_time->copy()->addHour($new_hours)->addMinutes($new_min);
                }
                if($new_hours > 0 && ($old_time->timestamp > $new_time->timestamp)){
                     $temp['column-2']= $bar_value;
                     $new_time =  $old_time->copy()->addHour($new_hours)->addMinutes($new_min);
                }


                $temp['date']=$value;
                $dataProviders[] = $temp;
           }
        }
       // dd($dataProviders);
        return $dataProviders;
    }


}
