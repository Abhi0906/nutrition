<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Paxx\Withings\Api as WithingsApi;
use Paxx\Withings\Server\Withings as WithingsAuth;
session_start();

class WithingsController extends Controller
{
    public function getUserData(){

    	try{
    		$user = \Auth::User();
	    	$user_id = $user->id;

	    	$temporaryCredentials_db = [];

	    	$config = array(
			    'identifier' => env('WITHINGS_IDENTIFIER'),
			    'secret' => env('WITHINGS_SECRET'),
			    'callback_uri' => env('WITHINGS_CALLBACK'),
			);
			$server = new WithingsAuth($config);

			if (isset($_GET['oauth_token'])) {
			    // Step 2
			    // Retrieve the temporary credentials from step 2
			    $temporaryCredentials = unserialize($_SESSION['temporary_credentials']);
			    // Retrieve token credentials - you can save these for permanent usage
			    $tokenCredentials = $server->getTokenCredentials($temporaryCredentials, $_GET['oauth_token'], $_GET['oauth_verifier']);
			    // Also save the userId

			    $device_userId = $_GET['userid'];
			    $credentials = array('access_token' => $tokenCredentials->getIdentifier(),
				    'token_secret' => $tokenCredentials->getSecret());

			    $input = array(
			    	'user_id' => $user_id,
			    	'device_user_id'      => $device_userId,
			    	'device_type' => 'withings',
				    'auth_config' => json_encode($credentials)
				    
				);

				$user->synch_device_auth()->create($input);
			} else {

				// Step 1
				
				$data = $user->synch_device_auth()->first();
				
				if(!empty($data)){
					
					$temporaryCredentials_db = json_decode($data->auth_config, 1);					
					$device_userId = $data['device_user_id'];
					
				}
				else{

					// These identify you as a client to the server.
				    $temporaryCredentials = $server->getTemporaryCredentials();
				   
				    // Store the credentials in the session.
				    $_SESSION['temporary_credentials'] = serialize($temporaryCredentials);
				    // Redirect the resource owner to the login screen on Withings.
				    $server->authorize($temporaryCredentials);
				    exit;
				}
			}	

			if(count($temporaryCredentials_db) > 0){
				$config = $config + array(
				    'access_token' => $temporaryCredentials_db['access_token'],
				    'token_secret' => $temporaryCredentials_db['token_secret'],
				    'user_id'      => $device_userId
				);
			}
			else{
				$config = $config + array(
				    'access_token' => $tokenCredentials->getIdentifier(),
				    'token_secret' => $tokenCredentials->getSecret(),
				    'user_id'      => $device_userId
				);
			}		

			$api = new WithingsApi($config);
			$withings_user = $api->getUser();
			
			/**
			 * @var \Paxx\Withings\Entity\Measure $measure
			 */
			
			$weight_data = [];
			foreach($withings_user->getMeasures() as $measure) {
		
				$date_arr = explode(' ', $measure->getCreatedAt());
				$check_date = \App\Models\User_weight::where('user_id', $user_id)
										->where('source', 'WITHINGS')
										->whereDate('log_date', '>=' , $date_arr[0])
										->first();
				if($check_date) { break; }
				$curr_weight = $measure->imperial()->getWeight() ? $measure->imperial()->getWeight() : null;

				$fat_ratio = $measure->getFatRatio() ? $measure->getFatRatio() : null;
				
				if($curr_weight || $fat_ratio){
					
					$current_timestamp = \Carbon\Carbon::now()->timestamp;
					$guid = guid();
					$timestamp = $current_timestamp;
					$created_at = date('Y-m-d H:i:s');
					$updated_at = date('Y-m-d H:i:s');

					$temp_arr = ["user_id" => $user_id,
						"current_weight" => $curr_weight,
						"body_fat" => $fat_ratio,
						"log_date" => $date_arr[0],
						"guid" => $guid,					
						"source" => "WITHINGS",
						"timestamp" => $timestamp,
						"created_at" => $created_at,
						"updated_at" => $updated_at
						];

					array_push($weight_data, $temp_arr);
				}	
			}
			
			if(!empty($weight_data)){

				\App\Models\User_weight::insert($weight_data);

			}
    	}catch(\Exception $ex){

            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }    	

		return redirect('/weight');
    }
}
