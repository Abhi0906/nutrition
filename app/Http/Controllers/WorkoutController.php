<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class WorkoutController extends Controller
{
    /**
     * Display a listing of the Worko.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('workout.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $input = $request->all();

         $input['is_admin']="1";
         $current_timestamp = \Carbon\Carbon::now()->timestamp;
         $input['guid'] = guid();
         $input['timestamp'] = $current_timestamp;
         $workout = \App\Models\Workout::create($input);

         return response()->json(['error' => false,'response'=>$workout],200)
                         ->setCallback(\Request::input('callback'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $error =false;
        $status_code = 200;
        $server_timestamp = \Carbon\Carbon::now()->timestamp;
        if($id !="" && $id > 0){
            try{

                $workout = \App\Models\Workout::with(['workout_exercise'])
                                                       ->where('id',$id)->first();
                if($workout != null){
                    $result = $workout;
                    $video_count =0;
                    $exercise_image_count =0;
                    $exercise_total_min=0;
                    $video_total_min=0;
                    $workout_exercise = $workout->workout_exercise;

                    foreach ($workout_exercise as $key => $item) {
                        if($item->exercise_image_id != null){
                           $exercise_image_count++;
                           $exercise_total_min =  $exercise_total_min+ $item['exercise_images']['exercise_time'];
                        }
                         if($item->video_id != null){
                           $video_count++;
                           $video_total_min =  $video_total_min+ $item['videos']['workout_time'];
                        }
                    }

                    $result['video_count']= $video_count;
                    $result['exercise_image_count']=$exercise_image_count;

                     $workout->exercise_total_mins =$exercise_total_min;
                     $workout->video_total_mins =$video_total_min;
                     $result['total_mins'] = $workout->exercise_total_mins+$workout->video_total_mins;

                    $result['exercise_lists'] = $this->getAssignedExercise($workout);
                    unset($result['workout_exercise']);
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Workout Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'timestamp'=>$server_timestamp,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $error = false;
        $workout = \App\Models\Workout::findOrFail($id);
        if($workout != null){
            $input = $request->all();
            unset($input['videos']);
            $result = $workout->update($input);
        }else{
            $error = true;
            $result = "No Workout Found.";
        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $error =false;
        $workout = \App\Models\Workout::findOrFail($id);
        if(!$workout){
          $error= true;
        }else{
          $file_name = $workout['image'];
          $destination_path = public_path().'/workout-images/'.$file_name;
          @unlink($destination_path);
          $workout->workout_exercise()->delete();
          $workout->delete();
       }
        return response()->json(['error' =>$error],200)
                         ->setCallback(\Request::input('callback'));
    }

    //getting all workouts list.
    public function getWorkoutWithVideosList(Request $request){

        $error = false;
        $workouts =[];
        $input = $request->all();
        if(!empty($input)){
            $workouts = \App\Models\Workout::with('workout_exercise')
                                               ->search($input)
                                               ->admin()
                                               ->get();
        }else{
          $workouts = \App\Models\Workout::with('workout_exercise')
                                          ->admin()
                                          ->get();
        }

        foreach ($workouts as $key => $workout) {
            $video_count =0;
            $exercise_image_count =0;
            $exercise_total_min=0;
            $video_total_min=0;
            $workout_exercise = $workout->workout_exercise;
            foreach ($workout_exercise as $key => $item) {
                if($item->exercise_image_id != null){
                   $exercise_image_count++;
                   $exercise_total_min =  $exercise_total_min+ $item['exercise_images']['exercise_time'];
                }
                 if($item->video_id != null){
                   $video_count++;
                   $video_total_min =  $video_total_min+ $item['videos']['workout_time'];
                }
            }

            $workout->video_count= $video_count;
            $workout->exercise_image_count=$exercise_image_count;
            $workout->exercise_total_mins =$exercise_total_min;
            $workout->video_total_mins =$video_total_min;
            $workout->total_mins = $workout->exercise_total_mins+$workout->video_total_mins;
            $workout->exercise_list = $this->getAssignedExercise($workout);
            unset($workout->workout_exercise);
        }

        if($workouts != null) {
            $result = $workouts;
        }else{
            $error = true;
            $result = "No Workouts Found.";
        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function getWorkoutTypes(){

       $workout_types = \App\Models\Workout_type::get(['id','title','no_of_times']);
       return response()->json(['error' => false,'response'=>$workout_types],200)
                         ->setCallback(\Request::input('callback'));

    }

    // public function attachedVideo(Request $request){
    //     $input = $request->all();
    //     $workout_id = $input['workout_id'];
    //     $error = false;
    //     $workout = \App\Models\Workout::findOrFail($workout_id);
    //     if($workout != null){
    //        $result = $workout->videos()->attach($input['video_id']);
    //     }else{
    //         $error = true;
    //         $result = "No Workout Found.";
    //     }
    //     return response()->json(['error' => $error,'response'=>$result],200)
    //                      ->setCallback(\Request::input('callback'));

    // }

    // public function deleteVideo($workout_id,$video_id){

    //     $error = false;
    //     $workout = \App\Models\Workout::findOrFail($workout_id);
    //     if($workout != null){
    //        $result = $workout->videos()->detach($video_id);
    //     }else{
    //         $error = true;
    //         $result = "No Workout Found.";
    //     }
    //     return response()->json(['error' => $error,'response'=>$result],200)
    //                      ->setCallback(\Request::input('callback'));

    // }


    public function attachedExercise(Request $request){
        $input = $request->all();
        $workout_id = $input['workout_id'];
        $error = false;
        $workout = \App\Models\Workout::findOrFail($workout_id);
        if($workout != null){
            $workout->workout_exercise()->delete();
            $workout_exercises = $input['exrecise_list'];
            $video_count =0;
            $exercise_image_count =0;
            $exercise_total_min=0;
            $video_total_min=0;

            foreach ($workout_exercises as $key => $item) {
                $workout_exercise_data=[];
                $workout_exercise_data['workout_id']=$workout_id;
                $workout_exercise_data['sort_order']=$key+1;
                if($item['workout_type']=='exercise_images'){
                     $workout_exercise_data['exercise_image_id']=$item['id'];
                      $exercise_image_count++;
                      $exercise_total_min =  $exercise_total_min+ $item['exercise_time'];
                }
                if($item['workout_type']=='videos'){
                     $workout_exercise_data['video_id']=$item['id'];
                     $video_count++;
                     $video_total_min =  $video_total_min+ $item['workout_time'];
                }

              $result =\App\Models\Workout_exercise::create($workout_exercise_data);

            }

            $result['video_count']= $video_count;
            $result['exercise_image_count']=$exercise_image_count;
            $result['total_mins'] = $exercise_total_min+$video_total_min;

        }else{
            $error = true;
            $result = "No Workout Found.";
        }
        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));

    }

    public function deleteExercise($workout_id,$exrecise_list){

        $error = false;
        $workout = \App\Models\Workout::findOrFail($workout_id);
        if($workout != null){
           $result = $workout->exercise_images()->detach($exercise_image_id);
        }else{
            $error = true;
            $result = "No Workout Found.";
        }
        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));

    }

    private function getAssignedExercise($workout){

        $workout_exercise = $workout->workout_exercise;

        $exercise_collection = new \Illuminate\Database\Eloquent\Collection;
        if($workout_exercise->count() > 0){
           foreach ($workout_exercise as $key => $value) {

              if($value->exercise_image_id != null)
              {

                if($value->exercise_images){
                    $value->exercise_images->workout_type ='exercise_images';
                     $value->exercise_images->imageUrl = '/exercise-images/'.$value->exercise_images->image_name;
                     $exercise_collection->push($value->exercise_images);
                }

              }

              if($value->video_id != null)
              {
                if($value->videos){
                  $value->videos->workout_type ='videos';
                  $value->videos->pictures = json_decode($value->videos->pictures);
                  $value->videos->imageUrl = $value->videos->pictures->sizes[4]->link;
                  $exercise_collection->push($value->videos);
                }
              }

            }

         }

       $result = $exercise_collection->toArray();
       return $result;

    }

    public function saveUserWorkout(Request $request){

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        $user_agent = isset($input['user_agent'])? $input['user_agent']:'';


        if(!empty($input)) {
            try{

                $user_id = $input['user_id'];
                $user = \App\Models\User::patient()->find($user_id);
                if($user != null){

                    $workout_data =[];
                    $user_workout_data =[];
                    $workout_data['title'] = (isset($input['title']))?$input['title']:'';
                    $workout_data['description'] = (isset($input['description']))?$input['description']:'';
                    $workout_data['experience'] = (isset($input['experience']))?$input['experience']:'';
                    $workout_data['body_part'] = (isset($input['body_part']))?$input['body_part']:'';

                    if(isset($input['user_agent']) && $input['user_agent']=="server"){

                        $current_timestamp = \Carbon\Carbon::now()->timestamp;
                        $workout_data['guid'] = guid();
                        $workout_data['timestamp'] = $current_timestamp;
                    }
                    else{

                        $workout_data['guid'] = (isset($input['guid']))?$input['guid']:'';
                        $workout_data['timestamp'] = (isset($input['timestamp']))?$input['timestamp']:'';
                    }


                     $user_workout_data['start'] = (isset($input['start']))?$input['start']:'';
                     $user_workout_data['end'] = (isset($input['end']))?$input['end']:'';
                     $user_workout_data['config'] = (isset($input['config']))?json_encode($input['config']):'';

                     $check_user_workout = \App\Models\Workout::where('guid', $workout_data['guid'])->first();

                      if(!empty($check_user_workout)){
                         $wokrout_id = $check_user_workout->id;
                         $guid = $check_user_workout->guid;
                         $check_user_workout->update($workout_data);
                         $check_user_workout->users()->updateExistingPivot($user_id,$user_workout_data);
                         $check_user_workout->workout_exercise()->delete();
                         $workout = $check_user_workout;
                      }else{

                        $workout = \App\Models\Workout::create($workout_data);
                        $wokrout_id = $workout->id;
                        $guid = $workout->guid;
                        $workout->users()->attach($user_id,$user_workout_data);

                      }


                    if(isset($input['workout_exercise']) && count($input['workout_exercise']) > 0){
                        foreach ($input['workout_exercise'] as $key => $item) {
                            $workout_exercise_data=[];
                            $workout_exercise_data['workout_id']=$wokrout_id;
                            $workout_exercise_data['sort_order']=$item['sort_order'];
                            $workout_exercise_data['exercise_image_id']=(isset($item['exercise_image_id']))?$item['exercise_image_id']:'';
                            $workout_exercise_data['video_id']=(isset($item['video_id']))?$item['video_id']:'';
                            $workout_exercise_data['exercise_id']=(isset($item['exercise_id']))?$item['exercise_id']:'';
                            $workout_exercise = \App\Models\Workout_exercise::create($workout_exercise_data);
                            $input['workout_exercise'][$key]['id']=  $workout_exercise->id;
                        }

                    }

                    $result['user_workout'] = array("workout_id"=>$wokrout_id, "guid"=>$guid,'workout_exercise'=>$input['workout_exercise']);
                    $result['message']='User Workout Save Successfully.';
                }else{

                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }


            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)->setCallback(\Request::input('callback'));

    }

    public function saveUserRoutine(Request $request){
        $result = [];
        $error = false;
        $status_code = 200;

        $input = $request->all();

        if(!empty($input)){
            try{

                $server_timestamp = \Carbon\Carbon::now()->timestamp;

                $user_id = $input['user_id'];
                $routine_id = $input['routine_id'];
                $start_date = $input['start_date'];
                $end_date = $input['end_date'];
                $created_at = date('Y-m-d H:i:s');
                $updated_at = date('Y-m-d H:i:s');

                $user = \App\Models\User::find($user_id);
                if($user != null){


                    $user->templates()->attach($routine_id, ["start_date"=>$start_date,
                                                                "end_date"=>$end_date,
                                                                "created_at"=>$created_at,
                                                                "updated_at"=>$updated_at
                                                            ]);

                    foreach ($input['myRoutineData'] as $key => $workout_data) {

                        $user_workout_data = [];
                        $user_workout_data['start'] = (isset($workout_data['start']))?$workout_data['start']:'';
                        $user_workout_data['end'] = (isset($workout_data['end']))?$workout_data['end']:'';
                        $user_workout_data['config'] = (isset($workout_data['config']))?json_encode($workout_data['config']):'';

                        $workout = \App\Models\Workout::create($workout_data);
                        $wokrout_id = $workout->id;
                        $guid = $workout->guid;
                        $workout->users()->attach($user_id,$user_workout_data);

                        if(isset($workout_data['workout_exercise']) && count($workout_data['workout_exercise']) > 0){

                            $inserted_workouts = [];
                            foreach ($workout_data['workout_exercise'] as $key => $item) {
                                $workout_exercise_data=[];
                                $workout_exercise_data['workout_id']=$wokrout_id;
                                $workout_exercise_data['sort_order']=$item['sort_order'];
                                $workout_exercise_data['exercise_image_id']=(isset($item['exercise_image_id']))?$item['exercise_image_id']:'';
                                $workout_exercise_data['video_id']=(isset($item['video_id']))?$item['video_id']:'';
                                $workout_exercise_data['exercise_id']=(isset($item['exercise_id']))?$item['exercise_id']:'';

                                $workout_exercise = \App\Models\Workout_exercise::create($workout_exercise_data);

                            }
                        }

                    }

                }
                else{
                    $error =true;
                    $status_code = 400;
                    $result['message']='No Patient Found';
                }

            }
            catch(\Exception $ex){
                $error = true;
                $status_code =$ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }
        }
        else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'timestamp'=>$server_timestamp,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function getUserWorkout($user_id){

        $error =false;
        $status_code = 200;
        $server_timestamp = \Carbon\Carbon::now()->timestamp;
        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::find($user_id);
                if($user != null){
                   $user_workouts = $user->workouts()->get();
                   $result['user_workouts'] = $user_workouts->toArray();
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message'] = 'No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }

        return response()->json(['error' => $error,'timestamp'=>$server_timestamp,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));


    }

    public function getUserAllWorkouts($user_id, Request $request){

        $error =false;
        $status_code = 200;
        $server_timestamp = \Carbon\Carbon::now()->timestamp;

        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::find($user_id);
                if($user != null){
                   $user_workouts = $user->workouts()->get();

                   $current_week_workouts = [];
                   //$week_start = \Carbon\Carbon::now()->startOfWeek();

                   // $week_end = \Carbon\Carbon::now()->endOfWeek();

                   $week_start = new \Carbon\Carbon($request->get('start'));
                   $week_end = new \Carbon\Carbon($request->get('end'));
                    $current_week_allDay_workouts = [];
                    while($week_start <= $week_end){

                        $temp_date = explode(' ', $week_start);
                        $week_date = date('Y-m-d', strtotime($temp_date[0]));
                        $week_day = date('l', strtotime($temp_date[0]));

                        $current_day_workouts = [];

                        foreach ($user_workouts as $key => $user_workout) {


                            $workout_start = date('Y-m-d', strtotime($user_workout->pivot->start));

                            $workout_end = date('Y-m-d', strtotime($user_workout->pivot->end));

                            $user_sel_days = convertStringToArray($user_workout->pivot->config);
                            $days = [];
                            foreach ($user_sel_days as $key2 => $sel_days) {

                                switch ($sel_days->week_day) {

                                    case 0:
                                        $days[] = 'Sunday';
                                        break;

                                    case 1:
                                        $days[] = 'Monday';
                                        break;

                                    case 2:
                                        $days[] = 'Tuesday';
                                        break;

                                    case 3:
                                        $days[] = 'Wednesday';
                                        break;

                                    case 4:
                                        $days[] = 'Thursday';
                                        break;

                                    case 5:
                                        $days[] = 'Friday';
                                        break;

                                    case 6:
                                        $days[] = 'Saturday';
                                        break;

                                    default:
                                        # code...
                                        break;
                                }

                            }

                            $deleted_dates = [];
                            if(!empty($user_workout->pivot->deleted_dates)){
                                $deleted_dates = json_decode($user_workout->pivot->deleted_dates, true);
                            }
                            if($week_date >= $workout_start && $week_date <= $workout_end){
                                if(in_array($week_day, $days) && !in_array($week_date, $deleted_dates)){
                                    $current_day_workouts[] = $user_workout;
                                }

                                if(!in_array($week_date, $deleted_dates)){
                                    $current_week_allDay_workouts[] = $user_workout;
                                }

                            }

                        }

                        $current_week_workouts[$week_day]['date'] = $week_date;
                        $current_week_workouts[$week_day]['data'] = $current_day_workouts;

                        $week_start->addDay();
                    }

                   $result['user_current_week_workouts'] = $current_week_workouts;
                   $result['user_all_workouts'] = array_unique($current_week_allDay_workouts);
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'timestamp'=>$server_timestamp,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));


    }

    public function logUserWorkoutRoutine(Request $request){

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        $user_agent = isset($input['user_agent'])? $input['user_agent']:'';
        if(!empty($input)) {
            try{

                $user_id = $input['user_id'];
                $user = \App\Models\User::patient()->find($user_id);
                if($user != null){

                    $check_user_workout_routine = \App\Models\User_workout_routine::where('guid', $input['guid'])->first();
                    if($check_user_workout_routine != null){
                      $user_workout_routine_id = $check_user_workout_routine->id;
                      $guid = $check_user_workout_routine->guid;
                       $user_routine_data =[];
                       $user_routine_data['total_calories'] =$input['total_calories'];
                       $user_routine_data['total_weight_lifted'] =$input['total_weight_lifted'];
                       $user_routine_data['total_miles'] =$input['total_miles'];
                       $check_user_workout_routine->update($user_routine_data);
                       $deleteExercise = \App\Models\User_workout_routine_exercise::where('user_workout_routine_id',$user_workout_routine_id)->delete();

                     if(isset($input['workout_routine_exercise']) && count($input['workout_routine_exercise']) > 0){

                            foreach ($input['workout_routine_exercise'] as $key => $routine_exercise) {
                                $routine_exercise['user_workout_routine_id'] =$check_user_workout_routine->id;
                               \App\Models\User_workout_routine_exercise::create($routine_exercise);
                            }
                        }

                    }else{
                        $user_routine_data =[];
                        $user_routine_data['user_id'] =$input['user_id'];
                        $user_routine_data['workout_id'] =$input['workout_id'];
                        $user_routine_data['guid'] =$input['guid'];
                        $user_routine_data['timestamp'] =$input['timestamp'];
                        $user_routine_data['log_date'] =$input['log_date'];
                        $user_routine_data['total_calories'] =$input['total_calories'];
                        $user_routine_data['total_weight_lifted'] =$input['total_weight_lifted'];
                        $user_routine_data['total_miles'] =$input['total_miles'];
                        $user_routine = \App\Models\User_workout_routine::create($user_routine_data);
                        $user_workout_routine_id = $user_routine->id;
                        $guid = $user_routine->guid;
                        if(isset($input['workout_routine_exercise']) && count($input['workout_routine_exercise']) > 0){

                            foreach ($input['workout_routine_exercise'] as $key => $routine_exercise) {
                                $routine_exercise['user_workout_routine_id'] =$user_workout_routine_id;
                               \App\Models\User_workout_routine_exercise::create($routine_exercise);
                            }
                        }
                    }

                    $result['user_workout_routine'] = array("user_workout_routine_id"=>$user_workout_routine_id, "guid"=>$guid);
                    $result['message']='User Workout Routine Save Successfully.';
                }else{

                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }


            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)->setCallback(\Request::input('callback'));

    }

    public function loggedUserWorkedRoutines($user_id){

        $error =false;
        $status_code = 200;
        $result =[];
        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::find($user_id);
                if($user != null){

                   $user_workout_routines = \App\Models\User_workout_routine::
                                                 with(['workout_routine','user_workout_rouitne_exercises'])
                                                 ->where('user_id',$user_id)->get();
                   if($user_workout_routines->count() > 0){
                    $result['user_workout_routines'] = $user_workout_routines;
                   }
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }
    // delete logged user workout routine
    public function deleteUserWorkoutRoutine($id){

        $error = false;
        $status_code = 200;
        $result =[];
        if(!empty($id) && $id> 0) {
            try{

                $user_workout_routine = \App\Models\User_workout_routine::where('id',$id)
                                                                ->first();
                  if($user_workout_routine != null){
                     $user_workout_routine->user_workout_rouitne_exercises()->delete();
                      $user_workout_routine->delete();
                     $result['message'] = 'User Workout Routine Removed Successfully.';
                  }else{
                     $result['message'] = ' No Workout Routine Found.';
                  }

                }catch(\Exception $ex){
                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }
    // delete user workout routine
    public function deleteUserWorkout($id){


        $error = false;
        $status_code = 200;
        $result =[];
        if(!empty($id) && $id> 0) {
            try{

                 $workout = \App\Models\Workout::findOrFail($id);
                 if(!$workout){
                      $error= true;
                      $result['message'] ='No Workout Routine Found.';
                    }else{

                      $workout->workout_exercise()->delete();
                      $workout->users()->detach();
                      $workout->delete();
                      $result['message'] = 'User Workout Routine Removed Successfully.';
                   }


                }catch(\Exception $ex){
                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function uploadImage(Request $request){

        $file_name = 'no file';
        if ($request->hasFile('image_file')) {
            $file_name = $request->file('image_file')->getClientOriginalName();
            $destination_path = 'workout-images/';
            $request->file('image_file')->move($destination_path, $file_name);
        }

        return response()->json(['error' => false,'file_name'=>$file_name],200)
                         ->setCallback(\Request::input('callback'));

    }

    public function removeImage($file_name){

        $destination_path = public_path().'/workout-images/'.$file_name;
        @unlink($destination_path);
        return response()->json(['error' => false,'file_name'=>$file_name],200)
                         ->setCallback(\Request::input('callback'));
    }


}
