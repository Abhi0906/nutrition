<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SocialMediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return view('social_media.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request){

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getSocialMedia(Request $request){
        $error =false;
        $status_code = 200;
        $result = [];
        $server_timestamp = \Carbon\Carbon::now()->timestamp;
        try{

            $timestamp = $request->get('timestamp');
                $compare_date = gmdate("Y-m-d H:i:s", $timestamp);
                if($timestamp != null){
                    $result = \App\Models\SocialMedia::where('updated_at','>=',$compare_date)
                                                        ->orWhere('deleted_at','>=',$compare_date)
                                                        ->withTrashed()
                                                        ->orderBy('id', 'DESC')->get();
                }
                else{
                    $result = \App\Models\SocialMedia::orderBy('id', 'DESC')->get();
                }

           }
           catch(\Exception $ex){
               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();
            }
        return response()->json(['error' => $error,'timestamp'=>$server_timestamp,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }
    public function updateSocialMedia(Request $request){
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();

        if(!empty($input)) {
            try{
                $media_data = $input['media_data'];
                //dd($media_data);
                foreach ($media_data as $key => $media) {
                    $media_id = $media['id'];

                    \App\Models\SocialMedia::where('id', $media_id)->update(['url'=>$media['url']]);
                }
               }catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }
}
