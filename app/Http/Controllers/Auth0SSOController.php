<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth0\Login\Contract\Auth0UserRepository;

class Auth0SSOController extends Controller
{

    /**
     * @var Auth0UserRepository
     */
    protected $userRepository;

    public function __construct(Auth0UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    /**
     * Callback action that should be called by auth0, logs the user in
     */
    public function callback(\Illuminate\Http\Request $request) {
        // Get a handle of the Auth0 service (we don't know if it has an alias)

        $service = \App::make('auth0');

        // Try to get the user information
        $profile = $service->getUser();

        // Get the user related to the profile
        $auth0User = $this->userRepository->getUserByUserInfo($profile);

        if ($auth0User) {
            // If we have a user, we are going to log him in, but if
            // there is an onLogin defined we need to allow the Laravel developer
            // to implement the user as he wants an also let him store it.
            if ($service->hasOnLogin()) {
                $user = $service->callOnLogin($auth0User);
            } else {
                // If not, the user will be fine
                $user = $auth0User;
            }

            $user = \App\Models\User::where("email",$user->email)->first();
            if($user != null){
                \Auth::login($user);
            }else{
                // Create Auth User to local database
                 $input['first_name']= $auth0User->user_metadata['first_name'];
                 $input['last_name']= $auth0User->user_metadata['last_name'];
                 $input['user_type']='patient';
                 $input['password'] = \Hash::make(str_random(6));
                 $input['isGuest']= '1';
                 $input['email_varify']= '1';
                 $input['status']= 'Incomplete';
                 $input['username'] =  str_random(10);
                 $input['email'] = $auth0User->email;
                 if($auth0User->sub){
                    $input['auth0_user_id'] = $auth0User->sub;
                 }else{
                    $input['auth0_user_id'] = $auth0User->user_id;
                 }
                 $input['photo']= 'no_img.png';
                 // insert record in users table
                 $userObj = \App\Models\User::create($input);
                 $user_profile['user_id']=$userObj->id;
                 $user_profile['news_letter']= 0;
                 $user_profile['activity_id']=1;
                 // insert record in user profile table
                 \App\Models\User_profile::create($user_profile);
                 \Auth::login($userObj);
            }

            if($request->query('state')){
              if(isset($_COOKIE['url'])) {
                  return  \Redirect::intended($_COOKIE['url']);
              }else{
                  return  \Redirect::intended('/dashboard');
              }
            }else{
                return  \Redirect::intended('/dashboard');
            }
        }
        return \App::make('auth0')->login(null, null, ['scope' => 'openid email email_verified'], 'code');
    }

}
