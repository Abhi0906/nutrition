<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ChallengeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
           return view('challenges.index')->with('user_id',\Auth::User()->id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();

        $user_agent = "";
        if(!empty($input)) {
            try{
                    $allowed = implode(",",$input['allowed_device']);
                    $config =[];
                    $goal_type = $input['goal_type'];
                    if($goal_type == 'body_fat_and_weight'){
                        $config['body_fat_percentage']=$input['target_calories'];
                        $config['weeks']=$input['daily_max_credit_calories'];
                        $config['weight_in_lbs']=$input['weight_in_lbs'];
                    }else if($goal_type == 'calories'){
                        $config['target_calories']=$input['target_calories'];
                        $config['daily_max_credit_calories']=$input['daily_max_credit_calories'];
                    }else if($goal_type == 'steps_run'){
                        $config['target_steps']=$input['target_calories'];
                        $config['daily_max_steps']=$input['daily_max_credit_calories'];
                    }/*else if($goal_type == 'steps_run'){
                        $config['target_steps']=$input['target_calories'];
                        $config['daily_max_steps']=$input['daily_max_credit_calories'];
                    }else if($goal_type == 'weight_loss'){
                        $config['weight_loss_lbs']=$input['target_calories'];
                        $config['weight_loss_weeks']=$input['daily_max_credit_calories'];
                    }else if($goal_type == 'body_transformation'){
                          $config['body_transformation_one']= "one";
                          $config['body_transformation_two']=  "two";
                    }else if($goal_type == 'muscle_gain'){
                          $config['muscle_gain_one']=  "one";
                          $config['muscle_gain_two']= "two";
                    }*/
                    $challenges_input_data =[];
                    $challenges_input_data['title'] = $input['title'];
                    $challenges_input_data['description'] = $input['description'];
                    $challenges_input_data['start_date'] = $input['start_date'];
                    $challenges_input_data['end_date'] = $input['end_date'];
                    $challenges_input_data['goal_type'] = $input['goal_type'];
                    $challenges_input_data['image']= isset($input['image']) ? $input['image'] : "";
                    $challenges_input_data['allowed_devices'] = $allowed;
                    $challenges_input_data['goal_type_config']=json_encode($config);
                    $log_challenges = \App\Models\Challenge::create($challenges_input_data);
                    $id = $log_challenges->id;
                    $result['message']='Challenege Create Successfully.';
            }catch(\Exception $ex){
                throw $ex;
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $error = false;
        $status_code = 200;

        $result = [];
        try{

            $edit_challenge_data = \App\Models\Challenge::where('id',$id)->first();

            $edit_challenge_data['goal_type_config'] = json_decode($edit_challenge_data['goal_type_config'],1);
            if($edit_challenge_data){
                $result = $edit_challenge_data;
            }
            else{
                $error = true;
                $result['error_message'] = "No record found";
            }
        }
        catch(\Exception $ex){
            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $error = false;
        $status_code = 200;
        $result =[];
        if($id != "" && $id > 0) {
            try{
                  $challenges_delete = \App\Models\Challenge::find($id);
                  if($challenges_delete){
                     $file_name = $challenges_delete['image'];
                     $destination_path = public_path().'/challenge-images/'.$file_name;
                     @unlink($destination_path);
                      $challenges_delete->delete();
                      $result['message'] = 'Challenge Removed Successfully.';
                  }
                  else{
                      $error = true;
                      $result['error_message'] = 'Record does not exists.';
                  }
               }catch(\Exception $ex){
                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
        }

    public function get_challengelist(Request $request){
        $error =false;
        $status_code = 200;
        $result = [];
        $server_timestamp = \Carbon\Carbon::now()->timestamp;
        try{
                $timestamp = $request->get('timestamp');
                $compare_date = gmdate("Y-m-d H:i:s", $timestamp);
                if($timestamp != null){

                    $get_challenges = \App\Models\Challenge::orderBy('id', 'DESC')
                                                            ->where('updated_at','>=',$compare_date)
                                                            ->orWhere('deleted_at','>=',$compare_date)
                                                            ->withTrashed()
                                                            ->get();
                }
                else{
                    $get_challenges = \App\Models\Challenge::orderBy('id', 'DESC')->get();
                }

                foreach ($get_challenges as $key => $challenges) {
                    $get_challenges[$key]['allowed_devices'] = $this->getDeviceName($challenges['allowed_devices']);
                    $get_challenges[$key]['allowed_devices_arr'] = explode(', ', $get_challenges[$key]['allowed_devices']);
                }
                $result = $get_challenges;
           }
           catch(\Exception $ex){
               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();
            }
        return response()->json(['error' => $error,'timestamp'=>$server_timestamp,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function getChallengelistFront(){
        $error =false;
        $status_code = 200;
        try{
             $get_challenges = \App\Models\Challenge::where('is_expired', '<>', '1')

                        ->get();
             foreach ($get_challenges as $key => $challenges) {
                $get_challenges[$key]['allowed_devices'] = $this->getDeviceName($challenges['allowed_devices']);
                $get_challenges[$key]['allowed_devices_arr'] = explode(', ', $get_challenges[$key]['allowed_devices']);
             }
             $result = $get_challenges;
           }
           catch(\Exception $ex){
               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();
            }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function get_alloweddevicelist(){
        $error =false;
        $status_code = 200;
        try{

             $allowed_device = \App\Models\AllowedDevice::get(['id','allowed_device']);
             $result = $allowed_device;
           }
           catch(\Exception $ex){
               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();
            }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function updateChallenge($id, Request $request){
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        //dd($input);
        if(!empty($input) && $id > 0) {
            try{
                $allowed = implode(",", $input['allowed_device']);
                $config =[];
                $goal_type = $input['goal_type'];
                if($goal_type == 'body_fat_and_weight'){
                    $config['body_fat_percentage']=$input['target_calories'];
                    $config['weeks']=$input['daily_max_credit_calories'];
                    $config['weight_in_lbs']=$input['weight_in_lbs'];
                }else if($goal_type == 'calories'){
                    $config['target_calories']=$input['target_calories'];
                    $config['daily_max_credit_calories']=$input['daily_max_credit_calories'];
                }else if($goal_type == 'steps_run'){
                    $config['target_steps']=$input['target_calories'];
                    $config['daily_max_steps']=$input['daily_max_credit_calories'];
                }/*
                else if($goal_type == 'steps_run'){
                    $config['target_steps']=$input['target_calories'];
                    $config['daily_max_steps']=$input['daily_max_credit_calories'];
                }else if($goal_type == 'weight_loss'){
                    $config['weight_loss_lbs']=$input['target_calories'];
                    $config['weight_loss_weeks']=$input['daily_max_credit_calories'];
                }else if($goal_type == 'body_transformation'){
                      $config['body_transformation_one']= "one";
                      $config['body_transformation_two']=  "two";
                }else if($goal_type == 'muscle_gain'){
                      $config['muscle_gain_one']=  "one";
                      $config['muscle_gain_two']= "two";
                }*/
                $challenges_input_data =[];
                $challenges_input_data['title'] = $input['title'];
                $challenges_input_data['description'] = $input['description'];
                $challenges_input_data['start_date'] = $input['start_date'];
                $challenges_input_data['end_date'] = $input['end_date'];
                $challenges_input_data['goal_type'] = $input['goal_type'];
                $challenges_input_data['is_expired'] = $input['is_expired'];
                $challenges_input_data['image'] = $input['image'];

                $challenges_input_data['allowed_devices'] = $allowed;
                $challenges_input_data['goal_type_config']=json_encode($config);
                $challenges_update = \App\Models\Challenge::where('id',$id)->first();
                if(!empty($challenges_update)){
                    $challenges_update->update($challenges_input_data);
                    $result['message']='Challenge Updated Successfully.';
                 } else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Food Found.';
                 }
               }catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function getDeviceName($deviceIds){
        $deviceIds = explode(",", $deviceIds);
        $device_name = \App\Models\AllowedDevice::whereIn('id',$deviceIds)
                    ->get();
        $device_name_arr =$device_name->pluck('allowed_device');
        return implode(", ",$device_name_arr->toArray());
    }

    public function displayChallenges()
    {
         return view('front.display_challenges');
    }
    public function uploadImage(Request $request){

        $file_name = 'no file';
        if ($request->hasFile('image_file')) {
            $file_name = $request->file('image_file')->getClientOriginalName();
            $destination_path = 'challenge-images/';
            $request->file('image_file')->move($destination_path, $file_name);
        }

        return response()->json(['error' => false,'file_name'=>$file_name],200)
                         ->setCallback(\Request::input('callback'));

    }

    public function removeImage($file_name){

        $destination_path = public_path().'/challenge-images/'.$file_name;
        @unlink($destination_path);
        return response()->json(['error' => false,'file_name'=>$file_name],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function challengeParticipant(Request $request){

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        if(!empty($input)) {
           try{

               $challengeId = $input['challenge_id'];
               $user_id = $input['user_id'];
               $challenge = \App\Models\Challenge::find($challengeId);
               if($challenge != null){

                 $checkChallengeJoin = \App\Models\Challenge_participant::where('user_id',$user_id)
                                                                         ->where('challenge_id',$challengeId)
                                                                                ->first();
                 if($checkChallengeJoin != null){

                    $error = true;
                    $result['error_message'] = "Yor are already joined this Challenge.";


                 }else{
                    $participant = \App\Models\Challenge_participant::create($input);
                    $id = $participant->id;
                    $result['challenge_participant_id']=$id;
                    $result['message']='Challenege Joined Successfully.';

                 }

               }else{
                    $error = true;
                    $result['error_message'] = "No Challenge found";
               }

            }catch(\Exception $ex){
                throw $ex;
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function userChallengeData(Request $request){

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        if(!empty($input)) {
           try{

               $challengeId = $input['challenge_id'];
               $user_id = $input['user_id'];
               $challenge = \App\Models\Challenge::find($challengeId);
               if($challenge != null){

                 $checkChallengeJoin = \App\Models\Challenge_participant::where('user_id',$user_id)
                                                                         ->where('challenge_id',$challengeId)
                                                                                ->first();
                 if($checkChallengeJoin != null){
                   $device_names = $this->getDeviceName($challenge->allowed_devices);
                   $device_name_arr = explode(', ', $device_names);
                   $challenge_device_name = $input['device'];
                   if (in_array($challenge_device_name, $device_name_arr)){

                      $user_challenge_data = \App\Models\User_challenge_data::where('guid',$input['guid'])->first();
                       if($user_challenge_data != null){
                        $user_challenge_data->update($input);
                       }else{
                          $user_challenge_data = \App\Models\User_challenge_data::create($input);
                       }

                       // Insert record in User Challenge Daily data table
                        $this->saveUserChallengeDailyData($input,$challenge);
                        // update User Paticipant table or Update All total
                        $this->updateUserParticipant($user_id,$challenge);
                         // Update User Rank
                        $this->updateUserRank($challenge);
                        $result['user_challenge'] = array("id"=>$user_challenge_data->id, "guid"=>$input['guid']);
                        $result['message']='User Challenge data Save Successfully.';

                   }else{
                     $error = true;
                     $result['error_message'] = "This Challenge not allow for this device.";
                   }



                 }else{

                   $error = true;
                   $result['error_message'] = "Yor are not joined this Challenge.";
                 }

               }else{
                    $error = true;
                    $result['error_message'] = "No Challenge found";
               }

            }catch(\Exception $ex){
                throw $ex;
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function getChallengeDetail($challenge_id){

        $error =false;
        $status_code = 200;
        if($challenge_id !="" && $challenge_id > 0){
            try{

                $challenge = \App\Models\Challenge::with('challenge_participants')->find($challenge_id);
                if($challenge != null){
                    $challenge->total_participants = $challenge->challenge_participants->count();
                    $challenge->allow_device_name = $this->getDeviceName($challenge->allowed_devices);
                    $result['challenge_details'] =  $challenge;
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Challenge Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    private function saveUserChallengeDailyData($input,$challenge){
        $challenge_id = $input['challenge_id'];
        $user_id = $input['user_id'];
        $log_date = $input['log_date'];
        $challenge_goal_type = $challenge->goal_type;
        $challenge_goal_config = $challenge->goal_type_config;
        $goal_config_arr = json_decode($challenge_goal_config,1);
        $dialy_max_credit =0;
        if($challenge_goal_type == 'steps_run'){
           $dialy_max_credit =  $goal_config_arr['daily_max_steps'];
        }else if($challenge_goal_type == 'calories'){
            $dialy_max_credit =  $goal_config_arr['daily_max_credit_calories'];
        }

         $max_credit = intval(preg_replace('/[^\d.]/', '',$dialy_max_credit));
         $user_challenge_data = \App\Models\User_challenge_data::where('user_id',$user_id)
                                                                ->where('challenge_id',$challenge_id)
                                                                ->where('log_date',$log_date)
                                                                ->get();
        $total_daily_data = $user_challenge_data->sum('data');
        if($total_daily_data > $max_credit){
            $total_daily_data =  $max_credit;
        }

        $user_challenge_daily_data = \App\Models\User_challenge_daily_data::where('user_id',$user_id)
                                                                ->where('challenge_id',$challenge_id)
                                                                ->where('log_date',$log_date)
                                                                ->first();
        if($user_challenge_daily_data != null){
            $user_challenge_daily_data->update(['data'=> $total_daily_data]);
        }else{
            $challenge_daily_data = [];
            $challenge_daily_data['challenge_id'] = $challenge_id;
            $challenge_daily_data['user_id'] = $user_id;
            $challenge_daily_data['log_date'] = $log_date;
            $challenge_daily_data['data'] = $total_daily_data;
            \App\Models\User_challenge_daily_data::create($challenge_daily_data);
        }
    }

    private function updateUserParticipant($user_id,$challenge){

      $challenge_id = $challenge->id;
      $challenge_goal_type = $challenge->goal_type;
      $checkChallengeJoin = \App\Models\Challenge_participant::where('user_id',$user_id)
                                                               ->where('challenge_id',$challenge_id)
                                                               ->first();

      $user_challenge_daily_data = \App\Models\User_challenge_daily_data::where('user_id',$user_id)
                                                                ->where('challenge_id',$challenge_id)
                                                                ->get();
      $total_data = $user_challenge_daily_data->sum('data');

      if($challenge_goal_type == 'calories'){
        $checkChallengeJoin->update(['total_calories_burned'=>$total_data]);
      }else if($challenge_goal_type == 'steps_run'){
        $checkChallengeJoin->update(['total_steps'=>$total_data]);
      }
    }

    public function updateUserRank($challenge){

            $goal_type = $challenge->goal_type;
            if($challenge->only_participants->count() > 0){
                if($goal_type == 'steps_run'){
                     $participants = $challenge->only_participants->sortByDesc('total_steps');
                     $count_rank=0;
                     $rank=0;
                     foreach ($participants as $key => $cp) {
                        if($cp->total_steps != null && $cp->total_steps > 0 ){
                            $rank = $count_rank+1;
                        }else{
                            $rank =$rank+1;
                        }

                        $challenge_participant_id = $cp['id'];
                        $cpu =\App\Models\Challenge_participant::find($challenge_participant_id);
                        $cpu->update(['rank'=>$rank]);
                        $count_rank++;
                     }
                }
            }
    }

    public function removeUserChallengeData($id){
        $error = false;
        $status_code = 200;
        $result =[];

        if(!empty($id)) {
            try{

                  $user_challenge_data = \App\Models\User_challenge_data::find($id);

                  if($user_challenge_data){
                        $user_id = $user_challenge_data->user_id;
                        $challenge_id = $user_challenge_data->challenge_id;
                        $log_date = $user_challenge_data->log_date;
                        $data = $user_challenge_data->data;

                      $delete_user_challenge_data = $user_challenge_data->delete();
                      if($delete_user_challenge_data){

                        $user_challenge_daily_data = \App\Models\User_challenge_daily_data::where('user_id', $user_id)
                                                                                    ->where('challenge_id', $challenge_id)
                                                                                    ->where('log_date', $log_date)
                                                                                    ->first();

                        if($user_challenge_daily_data){
                            $new_challenge_daily_data = $user_challenge_daily_data->data - $data;
                            $subtract_from_daily_data = $user_challenge_daily_data->update(['data' => $new_challenge_daily_data]);

                            if($subtract_from_daily_data){
                                $challenge_participant = \App\Models\Challenge_participant::where('user_id', $user_id)
                                                                                    ->where('challenge_id', $challenge_id)
                                                                                    ->first();
                                $new_total_steps =  $challenge_participant->total_steps - $data;
                                $challenge_participant->update(['total_steps'=> $new_total_steps]);

                                $challenge = \App\Models\Challenge::find($challenge_id);
                                $this->updateUserRank($challenge);
                            }

                        }
                      }
                      else{
                        $result['message'] = 'User challenge data not removed Successfully';
                      }
                  }
                  else{
                      $error = true;
                      $result['error_message'] = 'User challenge does not exist.';
                  }


               }catch(\Exception $ex){

                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function challengeListWithParticipant($user_id, Request $request){
        $error =false;
        $status_code = 200;
        $result = [];

        if($user_id !="" && $user_id > 0){
            $server_timestamp = \Carbon\Carbon::now()->timestamp;
            try{
                    $user = \App\Models\User::find($user_id);
                    if($user!==null){
                        $timestamp = $request->get('timestamp');
                        $compare_date = gmdate("Y-m-d H:i:s", $timestamp);

                        $get_challenges = \App\Models\Challenge::with(['participantOfChallenge'=>function($q) use ($user_id){
                            $q->where('user_id', $user_id);
                        }]);

                        if($timestamp != null){
                            $get_challenges = $get_challenges->where('updated_at','>=',$compare_date)
                                                            ->orWhere('deleted_at','>=',$compare_date)
                                                            ->withTrashed();
                        }

                        $get_challenges = $get_challenges->get();

                        foreach ($get_challenges as $key => $challenges) {
                            $get_challenges[$key]['allowed_devices'] = $this->getDeviceName($challenges['allowed_devices']);
                            $get_challenges[$key]['allowed_devices_arr'] = explode(', ', $get_challenges[$key]['allowed_devices']);
                        }
                        $result = $get_challenges;
                    }
                    else{
                        $error = true;
                        $status_code = 404;
                        $result['message']='No Patient Found.';
                    }
               }
               catch(\Exception $ex){
                   $error = true;
                   $status_code =$ex->getCode();
                   $result['error_code'] = $ex->getCode();
                   $result['error_message'] = $ex->getMessage();
                }
        }

        return response()->json(['error' => $error,'timestamp'=>$server_timestamp,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

}
