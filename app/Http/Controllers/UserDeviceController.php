<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class UserDeviceController extends Controller
{

    public function saveUserDevice(Request $request){

    	$error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        if(!empty($input)) {
            try{

                $user_id = $input['user_id'];
                $user = \App\Models\User::patient()->find($user_id);
                if($user != null){
                    $device_uuid = $input['device_uuid'];
                    $check_device = \App\Models\Apns_device::where('device_uuid',$device_uuid)->first();
                    if($check_device !=null){
                        $check_device->update($input);
                    }else{
                       $user_device = \App\Models\Apns_device::create($input);
                    }
              		$result['message']='Device added successfully.';
                }else{

                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }


            }catch(\Exception $ex){
                throw $ex;
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function sendPushNotification($user_id){

        $error = false;
        $status_code = 200;
        $result =[];

        if($user_id > 0) {
            try{
                $user = \App\Models\User::patient()->find($user_id);
                if($user){


                    $optionBuilder = new OptionsBuilder();
                    $optionBuilder->setTimeToLive(60*20);

                    $notificationBuilder = new PayloadNotificationBuilder('test');
                    $notificationBuilder->setBody('Pre workout meal')
                                        ->setSound('default');
                    $custom_data = ['type'=>'Meal','id'=>1];
                    $dataBuilder = new PayloadDataBuilder();
                    $dataBuilder->addData(['custom_data' =>  $custom_data]);

                    $option = $optionBuilder->build();
                    $notification = $notificationBuilder->build();
                    $data = $dataBuilder->build();
                    $apns_device =  \App\Models\Apns_device::where('user_id',$user_id)->first();

                   $device_token = $apns_device->device_token;
                    $downstreamResponse = FCM::sendTo($device_token, $option, $notification, $data);
                   // insert data on apn_messages table
                    $apn_message_data =[];
                    $apn_message_data['user_id'] = $user_id;
                    $apn_message_data['apns_device_id'] = $apns_device->id;
                    $apn_message_data['message'] = 'Pre workout meal';
                    $apn_message_data['type'] = 'general';
                    $apn_message_data['json_data'] = json_encode($custom_data);
                    $apn_message_data['delivered_at'] = date('Y-m-d H:i:s');
                    $apn_message_data['status'] = $downstreamResponse->numberSuccess();
                    \App\Models\Apns_message::create($apn_message_data);
                    $result['notification_data']= $custom_data;
                    $result['notification_scussess']= $downstreamResponse->numberSuccess();
                    $result['notification_failure']= $downstreamResponse->numberFailure();

                  }
                  else{
                      $error = true;
                      $result['error_message'] = 'No Patient found.';
                  }


               }catch(\Exception $ex){

                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));


    }

    public function removeUserApnDevice(Request $request){

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        if(!empty($input)) {
            try{

                $user_id = $input['user_id'];
                $user = \App\Models\User::patient()->find($user_id);
                if($user != null){
                    $device_uuid = $input['device_uuid'];
                    $check_device = \App\Models\Apns_device::where('device_uuid',$device_uuid)
                                                            ->where('user_id',$user_id)
                                                            ->first();
                    if($check_device !=null){
                        $check_device->delete();
                    }else{
                        $result['message']='No Device attach with this user';
                    }

                }else{

                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }


            }catch(\Exception $ex){
                throw $ex;
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function checkNotification(){

        $user_timezones =  \DB::table('user_timezone')->get();
        $day_time_arr =[];
          foreach ($user_timezones as $key => $value) {
                $timezone = $value->timezone_name;
                $now =\Carbon\Carbon::now($timezone)->subMinutes(5);
                $temp=[];
                $h = $now->hour;
                $m = $now->minute;
                $time =$h+($m/60);
                $temp['time'] = number_format($time,2);
                $dayNumber =$now->dayOfWeek;
                if($now->dayOfWeek == 0){
                    $dayNumber =7;
                }
                $temp['dayNumber']=$dayNumber;
                $temp['timezone']=$timezone;
                $temp['time_format'] = $now->toDayDateTimeString();
                $day_time_arr[] =$temp;

            }
        // echo "<pre>";
        // print_r($day_time_arr);
        $nutrition_exercise_day_times = \App\Models\Nutrition_exercise_day_time::with('nutrition_exercise_plan')
                                     ->where(function($q) use($day_time_arr){
                                        foreach ($day_time_arr as $key => $value) {
                                           $q->orWhere(function($q1) use($value){
                                             $q1->where('dayNumber', $value['dayNumber']);
                                           //  $q1->where('meal_time',$value['time']);
                                           });

                                        }

                                    })->get();
        // echo "<pre>";
         //print_r($nutrition_exercise_day_times->toArray());

        foreach ($nutrition_exercise_day_times as $key => $nutritionconfig) {
                $meal_plan_id = $nutritionconfig->meal_plan_id;
                $user_meal_plan =  \App\Models\User_meal_plan::with('user_with_token')
                                                                           ->where('meal_plan_id',$meal_plan_id)
                                                                           ->get();
              // echo "<pre>";
              // print_r( $user_meal_plan->toArray());
              // exit;
               foreach ($user_meal_plan as $key => $mp) {

                 // $user_device_data = $mp->user_with_token->device_token;
                  //  if($user_device_data !=null){
                      //   print_r($user_device_data->toArray());
                        $send_notification = new \App\Common\Send_pushnotification();
                        $send_notification->send($mp->user_with_token,$nutritionconfig);
                  //  }
               }

        }


    }

    public function getUserPushNotification($user_id, Request $request){

        $error =false;
        $status_code = 200;
        $server_timestamp = \Carbon\Carbon::now()->timestamp;
        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::find($user_id);
                if($user != null){
                    $timestamp = $request->get('timestamp');
                    $compare_date = gmdate("Y-m-d H:i:s", $timestamp);
                    if($timestamp != null){
                       $user_pushnotification = \App\Models\User_pushnotification::where('user_id',$user_id)
                                                           ->where('updated_at','>=',$compare_date)
                                                           ->orWhere('deleted_at','>=',$compare_date)
                                                           ->withTrashed()->get();
                     }else{
                       $user_pushnotification = \App\Models\User_pushnotification::where('user_id',$user_id)
                                                                ->get();
                    }

                  $result = $user_pushnotification;
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'timestamp'=>$server_timestamp,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function deleteUserPushNotification($id){

        $error = false;
        $status_code = 200;
        $result =[];
        if(!empty($id) && $id > 0) {
            try{

                $user_pushnotification = \App\Models\User_pushnotification::find($id);
                  if($user_pushnotification != null){
                     $user_pushnotification->delete();
                     $result['message'] = 'User Notification delete Successfully.';
                  }else{
                     $result['message'] = ' No Notification Found.';
                  }

                }catch(\Exception $ex){
                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function updateUserPushNotification($id,Request $request){

        $error = false;
        $status_code = 200;
        $result =[];
        if(!empty($id) && $id > 0) {
            try{

                $user_pushnotification = \App\Models\User_pushnotification::find($id);
                  if($user_pushnotification != null){
                     $user_pushnotification->is_taken = $request->is_taken;
                     $user_pushnotification->is_read = $request->is_read;
                     $user_pushnotification->save();
                     $result['message'] = 'User Notification update Successfully.';
                  }else{
                     $result['message'] = ' No Notification Found.';
                  }

                }catch(\Exception $ex){
                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }
}
