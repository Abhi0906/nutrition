<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserStepController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        $today = date("Y-m-d");
        $user_agent = "";

        if(!empty($input)) {
            try{

                $user_id = $input['user_id'];
                $user = \App\Models\User::patient()->find($user_id);
                if($user != null){

                    if(empty($input['log_date'])){
                        $input['log_date'] = $today;
                    }

                    $check_user_steps = \App\Models\UserStep:: where('user_id', $user_id)
                                                            ->where('log_date', $input['log_date'])
                                                            ->first();

                    if(!empty($check_user_steps)){

                        $check_user_steps->update($input);
                        $id = $check_user_steps->id;
                        $guid = $check_user_steps->guid;
                    }
                    else{

                        $user_step = \App\Models\UserStep::create($input);
                        $id = $user_step->id;
                        $guid = $user_step->guid;
                    }





                    $result['log_steps'] = array("id"=>$id, "guid"=>$guid);
                    $result['message']='Steps Logged Successfully.';
                }else{

                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }


            }catch(\Exception $ex){
                throw $ex;
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
            ->setCallback(\Request::input('callback'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
        $error =false;
        $status_code = 200;


        if($user_id !="" && $user_id > 0){
            try{


                $user = \App\Models\User::find($user_id);
                if($user != null){
                    $user_steps = \App\Models\UserStep:: where('user_id', $user_id)
                        ->get();

                    $result['$user_steps'] = $user_steps;


                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

                $error = true;
                $status_code =$ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
            ->setCallback(\Request::input('callback'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
