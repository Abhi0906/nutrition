<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $go_to = $request->get('go_to');
        return view('front.reports')->with('user_id', \Auth::User()->getUserInfo()['id'])
                                    ->with('go_to', $go_to);
    }

     public function getCharts($user_id, Request $request){

         $error =false;
         $status_code = 200;
         $result=[];

         if($user_id !="" && $user_id > 0){
            try{
                 $user = \App\Models\User::find($user_id);
                 $weekly_arr =[];
                if($user != null){
                    $date_range = $request->get('date_range');


                    $calorie_reports = new \App\Common\Calorie_report($user_id,$date_range);
                    $calorie_chart =  $calorie_reports->getCalorieChart();

                    $weight_reports = new \App\Common\Weight_report($user_id,$date_range);
                    $weight_chart =  $weight_reports->getWeightChart();
                    $body_fat_chart =  $weight_reports->getBodyFatChart();

                    $water_reports = new \App\Common\Water_report($user_id,$date_range);
                    $water_chart =  $water_reports->getWaterChart();

                    $bp_reports = new \App\Common\Bp_report($user_id,$date_range);
                    $bp_chart =  $bp_reports->getBpChart();
                    $pulse_chart =  $bp_reports->getPulseChart();

                    $bm_reports = new \App\Common\Bm_report($user_id,$date_range);
                    $bm_chart =  $bm_reports->getBmChart();

                    $sleep_reports = new \App\Common\Sleep_report($user_id,$date_range);
                    $sleep_chart =  $sleep_reports->getSleepChart();


                    $workout_reports = new \App\Common\Workout_report($user_id,$date_range);
                    $workout_chart =  $workout_reports->getWorkoutChart();

                    $result['calorie_chart'] = $calorie_chart;
                    $result['weight_chart'] = $weight_chart;
                    $result['body_fat_chart'] = $body_fat_chart;
                    $result['water_chart'] = $water_chart;
                    $result['bp_chart'] = $bp_chart;
                    $result['pulse_chart'] = $pulse_chart;
                    $result['bm_chart'] = $bm_chart;
                    $result['sleep_chart']=  $sleep_chart;
                    $result['workout_chart']=  $workout_chart;

                }else{

                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
         }
         return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }


}
