<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class ExerciseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        if(!empty($input)){
            try{

                $ex_name = $input['keyword'];
                $ex_type = $input['type'];
                $limit= isset($input['limit'])?$input['limit']:50;
                $result = \App\Models\Exercise::serachExercise($ex_name,$ex_type)
                                                ->take($limit)
                                                ->orderBy('exercises.name', 'asc')
                                                ->get();

            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }


    public function searchExercise(Request $request){

        $error = false;
        $status_code = 200;
        $exercises =[];
        $input = $request->all();
        if(!empty($input)){
            try{

                $ex_name = $input['keyword'];
                $ex_type = $input['type'];
                $limit= isset($input['limit'])?$input['limit']:50;
                $user_weight = \Auth::User()->profile->baseline_weight;
                $exercises = \App\Models\Exercise::serachExercise($ex_name,$ex_type)
                                                ->take($limit)
                                                ->orderBy('exercises.name', 'asc')
                                                ->get();
                if($exercises->count() > 0){
                    foreach ($exercises as $key => $ex) {
                       $default_cal = $ex->calories_burned;
                       $default_weight = $ex->weight;
                       $update_cal = floor(($user_weight* $default_cal)/$default_weight);
                       $exercises[$key]['calories_burned'] = $update_cal;
                    }
                }


            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$exercises],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();

        if(!empty($input) && $id > 0) {
            try{

                $user_id = $input['user_id'];
                $user = \App\Models\User::patient()->find($user_id);

                if($user != null){

                    $exercise_id = $input['exercise_id'];
                    $exercise_user =  $user->exercises()->where('exercise_user.id','=',$id)->first();
                    if($exercise_user != null){
                         $logged_exercise =  \App\Models\Exercise_user::find($id);
                         $logged_exercise->update($input);
                         $result['message']='Logged Exercise Updated Successfully.';

                    }else{

                        $error = true;
                        $status_code = 404;
                        $result['message']='No Exercise Found.';
                    }


                }else{

                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }


            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        $error = false;
        $status_code = 200;
        $result =[];
        if(!empty($id) && $id> 0) {
            try{

                $exercise_user = \App\Models\Exercise_user::where('id',$id)
                                                                ->first();
                  if($exercise_user != null){
                     $exercise_user->delete();
                     $result['message'] = 'Exercise Removed Successfully.';
                  }else{
                     $result['message'] = ' No Exercise Found.';
                  }

                }catch(\Exception $ex){
                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function logExercise(Request $request){

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        $user_agent = isset($input['user_agent'])? $input['user_agent']:'';
        if(!empty($input)) {
            try{

                $user_id = $input['user_id'];
                $user = \App\Models\User::patient()->find($user_id);

                $input['created_by'] = $user_id;
                $input['updated_by'] = $user_id;
                //check session
                if(Session::has('orig_user') ){
                  $input['created_by'] = Session::get( 'orig_user' );
                  $input['updated_by'] = Session::get( 'orig_user' );

                }

                if($user != null){

                    if(isset($input['guid_server'])){
                        $current_timestamp = \Carbon\Carbon::now()->timestamp;
                        $input['guid'] = guid();
                        $input['timestamp'] = $current_timestamp;
                        $user_agent = 'server';
                        $user_weight = \Auth::User()->profile->baseline_weight;
                        $input['weight']=$user_weight;

                    }
                      if($input['exercise_type'] == 'Apple_Health'){

                         $check_exercise_user = \App\Models\Exercise_user::
                                                      where('user_id',$user_id)
                                                      ->where('exercise_id',$input['exercise_id'])
                                                      ->where('exercise_date', $input['exercise_date'])
                                                      ->first();

                      }else{

                        $check_exercise_user = \App\Models\Exercise_user::
                                                      where('user_id',$user_id)
                                                      ->where('guid', $input['guid'])
                                                      ->first();
                      }


                      if(!empty($check_exercise_user)){

                        $input['updated_at']=date('Y-m-d H:i:s');
                        $check_exercise_user->update($input);
                        $id = $check_exercise_user->id;
                        $guid = $check_exercise_user->guid;

                      }else{

                        $input['created_at']=date('Y-m-d H:i:s');
                        $input['updated_at']=date('Y-m-d H:i:s');

                        $log_exercise = \App\Models\Exercise_user::create($input);
                        $id = $log_exercise->id;
                        $guid = $log_exercise->guid;
                      }


                    // for iPhone notification
                    if($user_agent != "" && $user_agent == 'server'){
                        $notification['user_id'] = $user_id;
                        $notification['notification_text'] = 'EXERCISE_ADDED';
                        sendNotification($notification);

                    }
                    $result['log_exercise'] = array("id"=>$id, "guid"=>$guid);
                    $result['message']='Exercise Added Successfully.';
                }else{

                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }


            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));


    }

     // web service for web
    public function patientExercise($user_id, Request $request){

        $error =false;
        $status_code = 200;
        $today = date("Y-m-d");
        $exercise_summary =[];
        $calories_burned_goal="";
        $req_date = $request->get('log_date');

        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::find($user_id);
                if($user != null){

                    $loggedExercise = $this->getLoggedExercise($user_id, $req_date);

                    $result['loggedExercise'] = $loggedExercise;

                    $user_calorie_goal = $user->User_calorie_goal()->first();

                    if($user_calorie_goal != null){
                         $calories_burned_goal =  $user_calorie_goal->calorie_burned_g;
                         $calorie_burned_percentage=0;
                          $total_caloires_burned = \App\Models\Exercise_user::where('user_id',$user_id)
                                                                ->where('exercise_date',$req_date)
                                                                ->sum('calories_burned');

                         if($total_caloires_burned > 0){
                            $calorie_burned_percentage = ($total_caloires_burned/$calories_burned_goal)*100;
                          }
                          $exercise_summary['calorie_burned_percentage'] = floor($calorie_burned_percentage);
                          $exercise_summary['total_burned'] = $total_caloires_burned;
                          $total_remaining = $calories_burned_goal - $total_caloires_burned;
                           $display_remaing=0;
                           if($total_remaining > 0){
                              $display_remaing =$total_remaining;
                           }
                          $exercise_summary['total_remaining'] =$display_remaing;
                    }

                    $exercise_summary['calorie_burned_goal'] =$calories_burned_goal;



                    $result['exercise_summary'] = $exercise_summary;
                    $user_logged_exercise =  \App\Models\Exercise_user::with('exercise')
                                                   ->where('user_id',$user_id)
                                                   ->get();
                    $result['frequent_exercises'] = $this->getFrequentExercise($user_logged_exercise);
                    $result['recent_exercises'] = $this->getRecentExercise($user_logged_exercise);
                    $result['exercise_notes'] = $this->getExerciseNote($user_id, $req_date);
                    $result['apple_health_exercises']= $this->getAppleHealthExercise($user_id,$req_date);
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }



    public function loggedExercise($user_id, Request $request){

        $error =false;
        $status_code = 200;
        $server_timestamp = \Carbon\Carbon::now()->timestamp;

        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::find($user_id);
                if($user != null){
                  $timestamp = $request->get('timestamp');
                  $compare_date = gmdate("Y-m-d H:i:s", $timestamp);
                  if($timestamp != null){
                      $logged_exercise = \App\Models\Exercise_user::with('exercise')
                                                   ->where('user_id',$user_id)
                                                   ->where('updated_at','>=',$compare_date)
                                                    ->orWhere('deleted_at','>=',$compare_date)
                                                    ->withTrashed()->get();

                  }else{

                     $logged_exercise = \App\Models\Exercise_user::with('exercise')
                                                   ->where('user_id',$user_id)
                                                   ->get();
                  }
                   $result = $logged_exercise;

                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'timestamp'=>$server_timestamp,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function getFrequentExercise($logged_Exercise){
        //start: get frequent exercise

        $frequent_exercise = array();
        $exercise_groupBy = $logged_Exercise->groupBy('exercise_id');

        if(count($exercise_groupBy)>0){
            foreach ($exercise_groupBy as $key => $grouped_exercise) {

               if(count($grouped_exercise)>1){

                    foreach ($grouped_exercise as $key1 => $frequent_exercise_value) {
                        $frequent_exercise_value->name = $frequent_exercise_value->exercise->name;
                        $frequent_exercise[] =$frequent_exercise_value->toArray();
                        break;
                    }
               }
            }
        }
        return $frequent_exercise;
        //end: get frequent nutritions
    }

    public function getRecentExercise($logged_Exercise){
        //start: get recent exercise
        $recent_Exercise = array();
        $recent_Exercise_data = $logged_Exercise->sortByDesc('id')->take(10);

        if(count($recent_Exercise_data)>0){

            foreach ($recent_Exercise_data as $key => $recent_exercise_value) {

                if(count($recent_exercise_value)>0){
                    $recent_exercise_value->name = $recent_exercise_value->exercise->name;
                    $recent_Exercise[] =$recent_exercise_value->toArray();
                }
            }
        }

        return $recent_Exercise;
        //end: get recent exercise
    }

    public function getExerciseNote($user_id,$log_date){

       $user_exercise_note =  \App\Models\User_exercise_note::where('user_id',$user_id)
                                                            ->where('log_date',$log_date)
                                                             ->first();
       return $user_exercise_note['notes'];

    }

    public function getPatientExercise(){
        $user_weight = null;
        $user_profile = \Auth::User()->profile;
        if(!empty($user_profile)){
            $user_weight = $user_profile->baseline_weight;
        }

        return view('front.exercise')->with('user_id', \Auth::User()->id)
                                     ->with('user_weight', $user_weight);
    }

    public function trackExercise(){

      $error =false;
      $status_code = 200;

      try{

           $result = \App\Models\Exercise::where('trackable',1)
                                          ->get();
        }catch(\Exception $ex){

            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }

      return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function getLoggedExercise($user_id, $date){

        $log_date = $date ? $date : date("Y-m-d");
        if($user_id > 0){

            $logged_exercise = \App\Models\Exercise_user::with('exercise')
                                                   ->where('user_id',$user_id)
                                                   ->whereDate('exercise_date','=', $log_date)
                                                   ->get();
            $exercise_type = $logged_exercise->sortByDesc('id')->groupBy('exercise_type');
            //dd($exercise_type->toArray());
            $filter_data = [];
            $exercise_type_total = [];

            foreach ($exercise_type as $key => $type) {
              if($key == 'Cardio'){
                    $calorie_burned_arr = [];
                    $time_arr = [];
                    foreach ($type as $key1 => $value) {
                        $exercise_values = [];
                        $exercise_values['id'] = $value->id;
                        $exercise_values['user_id'] = $value->user_id;
                        $exercise_values['exercise_id'] = $value->exercise_id;
                        $exercise_values['name'] = $value->exercise->name;
                        $exercise_values['calories_burned'] = $value->calories_burned;
                        $exercise_values['time'] = $value->time;
                        $exercise_values['exercise_type'] = $key;

                        //calculate the total

                        $calorie_burned_arr[] = $value->calories_burned;
                        $time_arr[] =$value->time;
                        $filter_data[$key][$key1] =$exercise_values;
                    }

                    $exercise_type_total = [
                                            "calories_burned"=> array_sum($calorie_burned_arr),
                                            "time"=> array_sum($time_arr)
                                           ];

                    $filter_data['cardio_total'] = $exercise_type_total;

              }

             if($key == 'Resistance'){
                    $calorie_burned_arr = [];
                    $time_arr = [];
                    $sets_arr = [];
                    $reps_arr = [];
                    $weight_lbs_arr = [];
                    foreach ($type as $key1 => $value) {
                        $exercise_values = [];
                        $exercise_values['id'] = $value->id;
                        $exercise_values['user_id'] = $value->user_id;
                        $exercise_values['exercise_id'] = $value->exercise_id;
                        $exercise_values['name'] = $value->exercise->name;
                        $exercise_values['calories_burned'] = $value->calories_burned;
                        $exercise_values['time'] = $value->time;
                        $exercise_values['exercise_type'] = $key;
                        $exercise_values['sets'] = $value->sets;
                        $exercise_values['reps'] = $value->reps;
                        $exercise_values['weight_lbs'] = $value->weight_lbs;

                        //calculate the total
                        $calorie_burned_arr[] = $value->calories_burned;
                        $time_arr[] =$value->time;
                        $sets_arr[] = $value->sets;
                        $reps_arr[] =  $value->reps;
                        $weight_lbs_arr[] = $value->weight_lbs;


                        $filter_data[$key][$key1] =$exercise_values;
                    }

                    $exercise_type_total = [
                                            "calories_burned"=> array_sum($calorie_burned_arr),
                                            "time"=> array_sum($time_arr),
                                            "sets"=> array_sum($sets_arr),
                                            "reps"=> array_sum($reps_arr),
                                            "weight_lbs"=> array_sum($weight_lbs_arr)
                                           ];

                    $filter_data['resistance_total'] = $exercise_type_total;

             }

            }

            return $filter_data;
        }

        return [];
    }


    public function allloggedExercise($user_id,$exercise_type){ // for web

        $error =false;
        $status_code = 200;
        $today = date("Y-m-d");
        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::find($user_id);

                if($user != null){
                    $type ='Cardio';
                    if($exercise_type =='resistance'){
                        $type ='Resistance';
                    }
                    $user_logged_exercise =  \App\Models\Exercise_user::with('exercise')
                                                   ->where('user_id',$user_id)
                                                   ->where('exercise_type',$type)
                                                   ->get();

                    $result['frequent_exercises'] = $this->getFrequentExercise($user_logged_exercise);
                    $result['recent_exercises'] = $this->getRecentExercise($user_logged_exercise);

                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }
            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function calculateMonthlySummary($user_id, Request $request){

        $result= [];
        $error =false;
        $status_code = 200;

        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::find($user_id);

                if($user != null){
                    $calories_burned_goal = 0;
                    $user_calorie_goals = \App\Models\User_calories_goal::where('user_id',$user_id)->first();
                    if($user_calorie_goals != null){
                        $calories_burned_goal = $user_calorie_goals->calorie_burned_g;
                    }
                    $req_month = $request->get('req_month');
                    $req_year = $request->get('req_year');

                    $month = $req_month ? $req_month : date('m');
                    $year = $req_year ? $req_year : date('Y');

                    $exercise_user = \App\Models\Exercise_user::where('user_id',$user_id)
                                                         ->whereMonth('exercise_date', '=', $month)
                                                         ->whereYear('exercise_date', '=', $year)
                                                         ->selectRaw('exercise_date, sum(calories_burned) as sum')
                                                         ->groupBy('exercise_date')
                                                         ->pluck('sum','exercise_date');


                    $temp_date = $year."-".$month."-01";
                    $month_day = date("t", strtotime($temp_date));
                    $data1 = array();
                    $data = [];
                    for($i=1; $i<=$month_day; $i++){
                        $total_cal_burned = 0;
                        $month_date = strtotime($i."-".$month."-".$year);
                        foreach ($exercise_user as $key1 => $exercise_value) {
                              $exercise_date = strtotime($key1);
                              if($month_date == $exercise_date){
                                  $total_cal_burned = $exercise_value;
                                  break;
                              }
                        }
                        $total_burned = $total_cal_burned;

                        $remaining_calories_bruned = $calories_burned_goal - $total_burned;


                        if( $total_burned > 0 ){
                              $data1['total_calorie_burned'] = $total_burned;
                              $data1['date'] = date('Y-m-d', $month_date);
                              $data1['day'] = date('d', $month_date);
                              $data1['month'] = date('m', $month_date);
                              $data1['year'] = date('Y', $month_date);
                              if($remaining_calories_bruned > 0){
                                  $data1['diff_text'] = "Deficit";
                                  $data1['diff_text_color'] = "#33CC33";
                              }
                              elseif($remaining_calories_bruned < 0){
                                  $data1['diff_text'] = "Excess";
                                  $data1['diff_text_color'] = "#FF1A1A";
                              }
                              else{
                                  $data1['diff_text'] = "Neutral";
                                  $data1['diff_text_color'] = "#FDE674";
                              }
                              $data1['diff'] = - $remaining_calories_bruned;
                              $data[] = $data1;
                         }
                    }
                    $result['date_wise'] = $data;

                    // Weekly Data start //
                    $weekly_data = [];
                    $temp_date = $year."-".$month."-01";
                    $month_day = date("t", strtotime($temp_date));
                    $first_day_this_month = date('Y-m-d', strtotime('01-'.$month.'-'.$year)); // hard-coded '01' for first day
                    $last_day_this_month  = date('Y-m-d', strtotime($month_day.'-'.$month.'-'.$year));

                    $month_weeks = array();
                    for($date = $first_day_this_month; $date <= $last_day_this_month; $date = date('Y-m-d', strtotime($date. ' + 7 days')))
                    {
                        $week =  date('W', strtotime($date));

                        $year =  date('Y', strtotime($date));

                        $from = date("Y-m-d", strtotime("{$year}-W{$week}-0"));

                        if($from < $first_day_this_month) $from = $first_day_this_month;

                        $to = date("Y-m-d", strtotime("{$year}-W{$week}-6"));

                        if($to > $last_day_this_month) $to = $last_day_this_month;

                        $month_weeks[] = array("start_date" => $from, "end_date" => $to );

                    }

                    $week_no = 1;
                    $total_calroies = 0;

                    for($j=0; $j < count($month_weeks); $j++){
                       /*$start_date = '2016-06-19';
                       $end_date = '2016-06-25';
                        */
                        $start_date = date('Y-m-d', strtotime($month_weeks[$j]['start_date']));
                        $end_date = date('Y-m-d', strtotime($month_weeks[$j]['end_date']));
                        $datetime1 = date_create($start_date);
                        $datetime2 = date_create($end_date);
                        $diff_days = date_diff($datetime1, $datetime2);
                        $diff_days = $diff_days->format('%d');
                        $diff_days = $diff_days +1;



                        $calories_burned_goal = 0;
                        $user_calorie_goals = \App\Models\User_calories_goal::where('user_id',$user_id)->first();
                        if($user_calorie_goals != null){
                            $calories_burned_goal = $user_calorie_goals->calorie_burned_g;
                        }
                        // get user goals data
                       $calories_burned_goal = $calories_burned_goal ? $calories_burned_goal:null;

                       $exercise_user = \App\Models\Exercise_user::where('user_id',$user_id)
                                                         ->selectRaw('exercise_date, sum(calories_burned) as sum')
                                                         ->whereBetween('exercise_date', [$start_date, $end_date])
                                                         ->groupBy('exercise_date')
                                                         ->pluck('sum','exercise_date');


                        $total_cal_burned = 0;
                        foreach($exercise_user as $key=>$value)
                        {
                            $total_cal_burned += $value;
                        }

                        $total_burned  =  $total_cal_burned;
                        $total_days_week = $diff_days ;

                        $calories_burned_goal_week = $calories_burned_goal;
                        $total_goals_of_week = $calories_burned_goal_week * $total_days_week;
                        $week_total = $total_burned - $total_goals_of_week;
                        $total_calroies = $total_calroies + $total_burned;

                        if($total_burned > 0 ){
                              $weekly_data1 = array();

                                $weekly_data1['total_calories_weekly_avg'] = round( $total_burned/$diff_days , 2 );
                                $weekly_data1 ['week_total_deficit'] = round($week_total/$diff_days, 2);

                                if($weekly_data1['total_calories_weekly_avg'] < $calories_burned_goal){
                                    $weekly_data1['weekly_calories_class_avg'] = "text-deficit";
                                }
                                elseif($weekly_data1['total_calories_weekly_avg'] > $calories_burned_goal){
                                    $weekly_data1['weekly_calories_class_avg'] = "text-excess";
                                }
                                else{
                                    $weekly_data1['weekly_calories_class_avg'] = "text-neutral";
                                }

                                if( $weekly_data1 ['week_total_deficit'] < $total_goals_of_week){
                                    $weekly_data1['weekly_class_avg'] = "text-deficit";
                                    $weekly_data1['diff_text'] = "Deficit";

                                }
                                elseif($weekly_data1 ['week_total_deficit'] > $total_goals_of_week){
                                    $weekly_data1['weekly_class_avg'] = "text-excess";
                                    $weekly_data1['diff_text'] = "Excess";

                                }
                                else{
                                    $weekly_data1['weekly_class_avg'] = "text-neutral";
                                    $weekly_data1['diff_text'] = "Neutral";
                                }

                              $weekly_data[$j+1] = $weekly_data1;
                        }
                        else{
                            $weekly_data[$j+1] = [];
                        }
                    }

                  $result['week_wise_avg'] = $weekly_data;
                  $monthly_average = [];

                   $exercise_user_month = \App\Models\Exercise_user::where('user_id',$user_id)
                                                 ->selectRaw('exercise_date, sum(calories_burned) as sum')
                                                 ->whereBetween('exercise_date', [$first_day_this_month, $last_day_this_month])
                                                 ->groupBy('exercise_date')
                                                 ->pluck('sum','exercise_date');


                  $total_cal_burned_month = 0;

                  foreach($exercise_user_month as $key=>$value)
                  {
                      $total_cal_burned_month += $value;
                  }

                  $total_burned_month  =  $total_cal_burned_month;
                  $total_days_month = $month_day;

                  $calories_burned_goal_month = $calories_burned_goal;
                  $total_goals_of_month = $calories_burned_goal_month * $total_days_month;
                  $month_total = $total_burned_month - $total_goals_of_month;
                  $monthly_average['monthly_avg_calories'] = round( ($total_calroies) / $month_day , 2);
                  $monthly_average['monthly_month_calories'] = round( ($month_total) / $month_day , 2);


                    if($monthly_average['monthly_avg_calories'] < $calories_burned_goal ){
                       $monthly_average['monthly_calories_class_avg'] = "text-deficit";
                    }elseif($monthly_average['monthly_avg_calories'] > $calories_burned_goal){
                        $monthly_average['monthly_calories_class_avg'] = "text-excess";
                    }else{
                        $monthly_average['monthly_calories_class_avg'] = "text-neutral";
                    }

                    if($monthly_average['monthly_avg_calories'] < $total_goals_of_month ){
                        $monthly_average['diff_text'] = "Deficit";
                        $monthly_average['monthly_class_avg'] = "text-deficit";
                    }elseif($monthly_average['monthly_avg_calories'] > $total_goals_of_month){
                        $monthly_average['diff_text'] = "Excess";
                        $monthly_average['monthly_class_avg'] = "text-excess";
                    }else{
                        $monthly_average['diff_text'] = "Neutral";
                        $monthly_average['monthly_class_avg'] = "text-neutral";
                    }
                    $result['monthly_avg'] = $monthly_average;
                    // Montly Data End //
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
               }

            }catch(\Exception $ex){
               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();
            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }


    public function updateExerciseNote(Request $request, $user_id){

        $error =false;
        $status_code = 200;
        if($user_id !="" && $user_id > 0){
            try{

                $input = $request->all();
                $user = \App\Models\User::find($user_id);
                if($user != null){

                   if(isset($input['guid_server'])){
                        $current_timestamp = \Carbon\Carbon::now()->timestamp;
                        $input['guid'] = guid();
                        $input['timestamp'] = $current_timestamp;
                   }

                   $user_exercise_note = \App\Models\User_exercise_note::where('user_id',$user_id)
                                                                ->where('log_date',$input['log_date'])
                                                                ->first();
                   if($user_exercise_note != null){
                     $user_exercise_note->update($input);

                   }else{

                         $input['user_id'] = $user_id;
                        \App\Models\User_exercise_note:: create($input);
                  }


                  $result['message']='Notes Updated successfully.';
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }
            }catch(\Exception $ex){
               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();
            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function userLoggedExercise($user_id, Request $request){

        $error =false;
        $status_code = 200;
        $today = date("Y-m-d");
        $req_date = $request->get('req_date');
        $result = [];
        if(!$req_date){
          $req_date = $today;
        }

        if($user_id !="" && $user_id > 0){
            try{

              $user = \App\Models\User::find($user_id);
              if($user != null){

                $loggedExercise = $this->getLoggedExercise($user_id, $req_date);
                $result = $loggedExercise;
              }

            }catch(\Exception $ex){
                $error = true;
                $status_code =$ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function saveAppleHeath(){

        // $exercise = new \App\Models\Exercise;

        // $exercise->name = 'Yoga';
        // $exercise->tech_name = 'yoga';
        // $exercise->trackable = 1;
        // $exercise->type = 'Workouts';
        // $exercise->source_name='apple_health';

        // $exercise->save();
        // echo "record_save successfully".$exercise->id;
        $this->getAppleHealthExercise(3,date('Y-m-d'));

    }

    public function getAppleHealthExercise($user_id,$log_date){

         $logged_exercise = \App\Models\Exercise_user::with('exercise')
                                                   ->where('user_id',$user_id)
                                                   ->where('exercise_type','=', 'Apple_Health')
                                                   ->whereDate('exercise_date','=', $log_date)
                                                   ->get();

            $calorie_burned_arr = [];
            $time_arr = [];
            $filter_data=[];
            if($logged_exercise->count() > 0){

              foreach ($logged_exercise as $key1 => $value) {
                $exercise_values = [];
                $exercise_values['id'] = $value->id;
                $exercise_values['user_id'] = $value->user_id;
                $exercise_values['exercise_id'] = $value->exercise_id;
                $exercise_values['name'] = $value->exercise->name;
                $exercise_values['calories_burned'] = $value->calories_burned;
                $exercise_values['time'] = $value->time;
                $exercise_values['exercise_type'] = $value->exercise_type;

                //calculate the total
                if($value->exercise_sub_type == 'Active_Energy' || $value->exercise_sub_type == 'Resting_Energy'){
                     $calorie_burned_arr[] = $value->calories_burned;
                }
                if($value->exercise_sub_type != 'Active_Energy'){
                   $time_arr[] =$value->time;
                }

                $filter_data['apple_health'][] =$exercise_values;
              }

             $exercise_type_total = [
                                    "calories_burned"=> array_sum($calorie_burned_arr),
                                    "time"=> array_sum($time_arr)
                                   ];

             $filter_data['apple_health_total'] = $exercise_type_total;

            }
           return $filter_data;

    }
}
