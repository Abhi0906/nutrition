<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
class WeightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('front.weight')->with('user_id',\Auth::User()->getUserInfo()['id']);
    }

    public function logWeight(Request $request){

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        $today = date("Y-m-d");
        $user_agent = "";

        if(!empty($input)) {
            try{

                $user_id = $input['user_id'];

                $user = \App\Models\User::patient()->find($user_id);

                $input['created_by'] = $user_id;
                $input['updated_by'] = $user_id;
                //check session
                if(Session::has('orig_user') ){
                  $input['created_by'] = Session::get( 'orig_user' );
                  $input['updated_by'] = Session::get( 'orig_user' );

                }


                if($user != null){

                    if(empty($input['log_date'])){
                        $input['log_date'] = $today;
                    }

                    if(isset($input['guid_server'])){

                        $current_timestamp = strtotime(date("Y-m-d H:i"));

                        $input['guid'] = guid();
                        $input['timestamp'] = $current_timestamp;
                        $user_agent = 'server';
                    }

                    $check_user_weight = $user->weight()->where('timestamp', $input['timestamp'])
                                                      ->first();

                    if(!empty($check_user_weight)){

                        $check_user_weight->update($input);
                        $id = $check_user_weight->id;
                        $guid = $check_user_weight->guid;
                    }
                    else{

                        $log_weight = $user->weight()->create($input);
                        $id = $log_weight->id;
                        $guid = $log_weight->guid;
                    }

                    //update user weight in profile
                    $profile_values = array("baseline_weight" => $input['current_weight']);
                    $user->profile()->update($profile_values);

                    // for iPhone notification
                    if($user_agent != "" && $user_agent == 'server'){
                        $notification['user_id'] = $user_id;
                        $notification['notification_text'] = 'WEIGHT_ADDED';
                        sendNotification($notification);

                    }

                    $result['log_weight'] = array("id"=>$id, "guid"=>$guid);
                    $result['message']='Weight Logged Successfully.';
                }else{

                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }


            }catch(\Exception $ex){
                throw $ex;
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function loggedWeight($user_id, Request $request){

        $error =false;
        $status_code = 200;
        $init_date = null;
        $requested_days = null;

        $from_server  = $request->get('from_server');
        $start_date  = date('Y-m-d', strtotime($request->get('start_date')) );
        $end_date  = date('Y-m-d', strtotime($request->get('end_date')) );

        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::find($user_id);

                if($user != null){

                    if($from_server){ //get weight between date range

                      $user_weight = $user->weight()->whereDate('log_date', '>=', $start_date)
                                                    ->whereDate('log_date', '<=', $end_date)
                                                    ->orderBy('log_date','desc')
                                                    ->get();

                      $user_weight_last_four_log = $user->weight()->orderBy('log_date','desc')
                                                                  ->limit('4')
                                                                  ->get();



                      $weight_goal = $user->user_weight_goal()->first();

                      $user_prifile = $user->profile()->first();
                      $user_height = floatval($user_prifile->height_in_feet . "." . $user_prifile->height_in_inch);

                      $result['user_weight'] = $user_weight;
                      $result['user_weight_last_log'] = $user_weight_last_four_log;
                      $result['user_weight_goal'] = $weight_goal ? $weight_goal:null;
                      $result['user_height'] = $user_height;
                    }
                    else{
                      $user_weight = $user->weight()->get();
                      $result = $user_weight;
                    }
                    //end: get user weight


                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();

        if(!empty($input) && $id > 0) {
            try{

                $user_id = $input['user_id'];
                $user = \App\Models\User::patient()->find($user_id);

                if($user != null){

                    $user_weight =  $user->weight()->where('user_weight.id','=',$id)->first();
                    if($user_weight != null){
                       // $current_timestamp = \Carbon\Carbon::now()->timestamp;
                       // $input['timestamp'] =  $current_timestamp;
                        $user_weight->update($input);
                        //update user weight in profile
                        $profile_values = array("baseline_weight" => $input['current_weight']);
                        $user->profile()->update($profile_values);
                        $result['log_weight'] = array("id"=>$user_weight->id, "guid"=>$user_weight->guid);
                        $result['message']='Logged Weight Updated Successfully.';

                    }else{

                        $error = true;
                        $status_code = 404;
                        $result['message']='No Weight Found.';
                    }


                }else{

                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }


            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        $error = false;
        $status_code = 200;
        $result =[];
        if($id != "" && $id > 0) {
            try{
                  $user_weight = \App\Models\User_weight::find($id);
                  if($user_weight){
                      $guid = $user_weight->guid;
                      $user_weight->delete();
                      $result["guid"] = $guid;
                      $result['message'] = 'User Weight Removed Successfully.';
                  }
                  else{
                      $error = true;
                      $result['error_message'] = 'Record does not exists.';
                  }
               }catch(\Exception $ex){
                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function getLoggedBodyMeasurements($user_id, Request $request){
      $error =false;
      $status_code = 200;

      $from_server  = $request->get('from_server');
      $start_date  = date('Y-m-d', strtotime($request->get('start_date')) );
      $end_date  = date('Y-m-d', strtotime($request->get('end_date')) );
      $user_body_measurements_last_log = [];

      if($user_id !="" && $user_id > 0){
          try{

              $user = \App\Models\User::find($user_id);
              if($user != null){

                if($from_server){ //get last 7 weight
                    $user_body_measurements = $user->body_measurements()->whereDate('log_date', '>=', $start_date)
                                              ->whereDate('log_date', '<=', $end_date)
                                              ->orderBy('log_date','desc')
                                              ->get();
                    $user_body_measurements_last_log = $user->body_measurements()
                                                   ->orderBy('log_date','desc')
                                                   ->get();
                }
                else{
                    // get user weight
                    $user_body_measurements = $user->body_measurements()->get();

                    //end: get user weight
                }

                $user_bm_goal = $user->user_body_measurements_goal()->get();
                $bm_goal = [];
                if($user_bm_goal->count() > 0){

                  $bm_goal['goal_date'] = $user_bm_goal['0']['goal_date'];

                  foreach ($user_bm_goal as $key => $bm_data) {

                    if($bm_data->body_measurement_type == 'neck'){
                       $bm_goal['neck_g'] = $bm_data->goal;
                    }else if($bm_data->body_measurement_type == 'arms'){
                         $bm_goal['arms_g'] = $bm_data->goal;
                    }else if($bm_data->body_measurement_type == 'waist'){
                         $bm_goal['waist_g'] = $bm_data->goal;
                    }else if($bm_data->body_measurement_type == 'hips'){
                         $bm_goal['hips_g'] = $bm_data->goal;
                    }else if($bm_data->body_measurement_type == 'legs'){
                        $bm_goal['legs_g'] = $bm_data->goal;
                    }
                  }
                }
                $result['user_body_measurements_last_log'] = $user_body_measurements_last_log;
                $result['user_body_measurements'] = $user_body_measurements;
                $result['body_measurement_goal'] = $bm_goal ? $bm_goal:null;

              }else{
                  $error = true;
                  $status_code = 404;
                  $result['message']='No Patient Found.';

              }

          }catch(\Exception $ex){

             $error = true;
             $status_code =$ex->getCode();
             $result['error_code'] = $ex->getCode();
             $result['error_message'] = $ex->getMessage();

          }
      }

      return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function logBodyMeasurements(Request $request){

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        $today = date("Y-m-d");
        $user_agent = "";

        if(!empty($input)) {
            try{

                $user_id = $input['user_id'];

                $input['created_by'] = $user_id;
                $input['updated_by'] = $user_id;
                //check session
                if(Session::has('orig_user') ){
                  $input['created_by'] = Session::get( 'orig_user' );
                  $input['updated_by'] = Session::get( 'orig_user' );

                }


                $user = \App\Models\User::patient()->find($user_id);

                if($user != null){

                    if(empty($input['log_date'])){
                        $input['log_date'] = $today;
                    }

                    if(isset($input['guid_server'])){
                        $current_timestamp = \Carbon\Carbon::now()->timestamp;
                        $input['guid'] = guid();
                        $input['timestamp'] = $current_timestamp;
                        $user_agent = 'server';
                    }

                    $check_user_body_measurements = $user->body_measurements()->where('guid', $input['guid'])
                                                      ->first();


                    if(!empty($check_user_body_measurements)){

                        $check_user_body_measurements->update($input);
                        $id = $check_user_body_measurements->id;
                        $guid = $check_user_body_measurements->guid;
                    }
                    else{

                        $log_body_measurements = $user->body_measurements()->create($input);
                        $id = $log_body_measurements->id;
                        $guid = $log_body_measurements->guid;
                    }

                    // for iPhone notification
                    if($user_agent != "" && $user_agent == 'server'){
                        $notification['user_id'] = $user_id;
                        $notification['notification_text'] = 'BODY_MEASUREMENTS_ADDED';
                        sendNotification($notification);

                    }

                    $result['log_body_measurements'] = array("id"=>$id, "guid"=>$guid);
                    $result['message']='Body Measurements Logged Successfully.';
                }else{

                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }


            }catch(\Exception $ex){
                throw $ex;
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function delete_body_measurements($id)
    {


        $error = false;
        $status_code = 200;
        $result =[];
        if($id != "" && $id > 0) {
            try{
                  $user_bodyMeasurements = \App\Models\User_body_measurements::find($id);
                  if($user_bodyMeasurements){
                      $guid = $user_bodyMeasurements->guid;
                      $user_bodyMeasurements->delete();
                      $result["guid"] = $guid;
                      $result['message'] = 'User Body Measurements Removed Successfully.';
                  }
                  else{
                      $error = true;
                      $result['error_message'] = 'Record does not exists.';
                  }
               }catch(\Exception $ex){
                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }
}
