<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Medicine;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use Validator;
use App\Http\Requests\UserRegistrationFormRequest;
use App\Repositories\Auth0Interface;
use Carbon\Carbon;
use Session;
use Auth;

class UserController extends Controller
{

    protected $userType;

    protected $number_of_records = 100;

    protected $redirectPath;

    protected $auth0Repository;

    public function __construct(){
        $routeName = \Route::currentRouteName();
        $this->userType = explode(".", $routeName)[0];
        $this->redirectPath = "/admin".'/'.$this->userType;
        //$this->auth0Repository = $auth0Repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //echo \Auth::User()->user_type;exit;
      if(\Auth::User()->user_type == 'doctor' || \Auth::User()->user_type == 'super_admin' ){
        //echo \Auth::User()->user_type;exit;
          return view('user.index')->with('user_type',$this->userType);

      }else{
        return redirect('/admin/login')
              ->withErrors([
                  'email' => 'These credentials do not match our records'
              ]);
      }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
       $doctor_titles = \App\Models\DoctorTitle::orderBy('sort_order','asc')->pluck('name','name');
       $meal_templates_male = $this->getMealTemplates('Male');
        $meal_templates_female = $this->getMealTemplates('Female');

       return view('user.create')->with('user_type',$this->userType)
                                 ->with('doctor_titles',$doctor_titles)
                                 ->with('meal_templates_male',$meal_templates_male)
                                 ->with('meal_templates_female',$meal_templates_female);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UserRegistrationFormRequest  $request
     * @return Response
     */
    public function store(UserRegistrationFormRequest $request)
    {

        $input = $request->all();

        if($this->userType == 'patient'){
             $emailExist =  '';//$this->auth0Repository->checkEmailExist($input['email']);
             if($emailExist){
                  return redirect()->back()->withErrors(['email'=>'Email has already been taken.']);
             }
        }
        // Default value of users table
        $input['user_type']=$this->userType;
        $input['username']= str_random(10);
        $input['created_by']= \Auth::id();
        $input['updated_by']= \Auth::id();

        if($request->photo == ""){
           $input['photo']= $request->gender.".jpg";
        }

        if($request->has('password')) {
            $input['password'] = \Hash::make($request->password);
        }

        $input['birth_date'] = date('Y-m-d',strtotime($input['birth_date']));
        if($this->userType == 'patient'){
            $random_password = str_random(8);
            $input['password'] = \Hash::make($random_password);
        }

        $user = User::create($input);

        if(isset($input['meal_template_id']) && !empty($input['meal_template_id'])){
            $user->meal_template()->attach($input['meal_template_id']); // assign meal template
        }

        $token = \Password::getRepository()->create($user);

        if($this->userType == 'patient'){
            $input['activity_value']=1.2;
            $calories = calculateUsCalories($input);
            $user_profile['user_id']=$user->id;

            $user_profile['news_letter']= isset($input['news_letter']) ? $input['news_letter'] : 0;
            $user_profile['activity_id']=1;
            $user_profile['baseline_weight'] = isset($input['baseline_weight']) ?  $input['baseline_weight'] : 0;
            $user_profile['height_in_feet'] = isset($input['height_in_feet']) ?  $input['height_in_feet'] : 0;
            $user_profile['height_in_inch'] = isset($input['height_in_inch']) ?  $input['height_in_inch'] : 0;
            $user_profile['water_intake'] = $input['baseline_weight']*0.5;
            $user_profile['calories'] = $calories;
            \App\Models\User_profile::create($user_profile);
            // save default vitals of patient
            // $this->saveDefaultVitalParams($user->id);
            // $this->saveDefaultRoutineTimes($user->id);


        }
        if($user != null)
        {
            $email_data = array();
            $email_data['subject']='Welcome to Genemedics!';
            $email_data['message'] = "Genemedics Health & Wellness";
            $email_data['toemail'] = $user->email;
            if($user->user_type == 'patient'){
               $email_data['name'] = $user->full_name;
               $email_data['token'] = $token;
               $email_data['email'] = $user->email;
               $email_data['password'] =  $random_password;
               sendEmail('welcome_patient',$email_data);
            }
            if($user->user_type == 'doctor'){
                $email_data['token'] = $token;
                $email_data['name'] = $user->name_with_title;
                $email_data['username'] = $user->email;
                $email_data['password'] = $request->password;

                sendEmail('welcome_doctor',$email_data);
            }
        }

        return redirect($this->redirectPath);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

       $user = $this->checkUser($id);
       if(!$user){
         return view('errors.404')->with('user_type',$this->userType);
       }

       if(\Auth::User()->user_type == 'doctor' || \Auth::User()->user_type == 'super_admin'){
         $doctor_titles = \App\Models\DoctorTitle::orderBy('sort_order','asc')->pluck('name','name');
         //dd($user->profile);
         $meal_templates_male = $this->getMealTemplates('Male');
         $meal_templates_female = $this->getMealTemplates('Female');
         $user_template = $this->getUserMealTemplate($id);

        return view('user.edit')->with('user_type',$this->userType)
                                       ->with('user_id',$id)
                                       ->with('doctor_titles',$doctor_titles)
                                       ->with('meal_templates_male', $meal_templates_male)
                                       ->with('meal_templates_female', $meal_templates_female)
                                       ->with('user_template', $user_template)
                                       ->with('user',$user);
       }else{
        return view('front.patient_profile')->with('user_type',$this->userType)
           ->with('user_id',$id)
           ->with('user',$user);
       }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(UserRegistrationFormRequest $request, $id)
    {

        $error = false;
        if($request->user_type == 'patient')
            $user = User::with('profile')->patient()->find($id);
        else if($request->user_type == 'doctor'){
            $user = User::doctor()->find($id);
        }else{
            $user =null;
        }

        if(!$user){
          $error = true;
          $result = "No User Found";
        }
        $input = $request->all();

        if ($request->has('password') && strlen(trim($input['password'])) > 0) {
            $input['password'] = \Hash::make($request->password);
            $update_data['password'] = trim($input['password']);
        }else{
            unset($input['password']);
        }

        if($input['birth_date']){
            $input['birth_date'] = date('Y-m-d',strtotime($input['birth_date']));
        }


        $result = $user->update($input);
        // Update assigned templates
        $user->meal_template()->detach();
        $user->meal_template()->attach($input['meal_template_id']);
        //End: Update assigned templates
        if($request->user_type == 'patient'){

            //Update user_metadata of auth0
            if($user->auth0_user_id != null && $user->auth0_user_id != ""){

                $user_metadata = ['first_name'=>$input['first_name'],'last_name'=>$input['last_name']];
                $update_data['user_metadata'] = $user_metadata;
                //$this->auth0Repository->updateUser($user->auth0_user_id,$update_data);
            }
            $user->profile->news_letter = $input['news_letter'];
            $user->profile->baseline_weight = $input['baseline_weight'];
            $user->profile->height_in_feet = $input['height_in_feet'];
            $user->profile->height_in_inch = $input['height_in_inch'];

            $user->profile->save();
            // for iPhone notification
            $notification['user_id'] = $id;
            $notification['notification_text'] = 'PROFILE_UPDATED';
            sendNotification($notification);
        }
        return redirect($this->redirectPath);
        // return response()->json(['error' =>$error,'response'=>$result],200)
        //                  ->setCallback(\Request::input('callback'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $error =false;
        $user = User::find($id);
        if(!$user){
          $error= true;
        }
        $user->delete();
        return response()->json(['error' =>$error],200)
                         ->setCallback(\Request::input('callback'));
    }

    /**
     * Showing dashboard of User as per User Type
     * e.g. For Doctor, Patient etc.
     *
     * @return Response
     */
    // Check the user type.(doctor,patient)
    public function dashboard()
    {
        //dd(\Auth::User());
        if(\Auth::check() && \Auth::User()->user_type == 'patient'){
            $auth0_user_data =[];
            if(\Auth::User()) {
             $auth0_user_data = \Auth::User()->getUserInfo();// $this->auth0Repository->getUser(\Auth::User()->auth0_user_id);
             $auth0_created_at = $auth0_user_data['created_at'];
             $auth0_created_at_diff =  \Carbon\Carbon::createFromTimeStamp(strtotime($auth0_created_at))->diffInDays();
             $auth0_user_data['created_at_diff'] =  $auth0_created_at_diff;
            }
            return view('front.new_dashboard')
                 ->with('auth0_user_data',$auth0_user_data)
                 ->with('user_id',\Auth::User()->id);

        }else if(\Auth::User()->user_type == 'doctor'){
            return redirect('/admin/patient');
       }else if(\Auth::User()->user_type == 'super_admin'){
          return redirect('/admin/patient');
       }

    }

    /**
     * Check the user specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */

    private function checkUser($id){

        $user =null;
        if($this->userType == 'patient'){

            $user = User::patient()->find($id);

        }else if($this->userType == 'doctor'){

            $user = User::doctor()->find($id);
        }

        return $user;

    }

    public function upload(Request $request){


        if ($request->hasFile('user_photo')) {
            $file_name = $request->file('user_photo')->getClientOriginalName();
            $destination_path = 'uploads/';
            $request->file('user_photo')->move($destination_path, $file_name);
        }

        return response()->json(['error' => false,'file_name'=>$file_name],200)
                         ->setCallback(\Request::input('callback'));


    }

    public function remove($file_name){

        $destination_path = public_path().'/uploads/'.$file_name;
         @unlink($destination_path);
         return response()->json(['error' => false,'file_name'=>$file_name],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function medicines($med_name = null){

        //$medicines = \App\Models\Medicine::mostConsumed()->get()->take(5);
         $medicines = \App\Models\Medicine::serachMedicines($med_name)->take(100)->get();

         return response()->json(['error' => false,'response'=>$medicines],200)
                         ->setCallback(\Request::input('callback'));

    }

    public function saveVitalParameters(Request $request){

      $input = $request->all();
      $vital_type = $input['vital_type'];

      switch ($vital_type)
        {
            case 'weight' :
                $weight_data = $input;
                $weight_data['created_by'] = \Auth::id();
                $weight_data['updated_by'] = \Auth::id();
                $id = $input['id'];
                $weight_parameter =  \App\Models\Weight_parameter:: where('id',$id)->first();
                if($weight_parameter != null){
                   $weight_parameter->update($weight_data);
                }
               break;

            case 'blood_pressure' :

                $blood_pressure_data =  $input;

                $blood_pressure_data['created_by'] = \Auth::id();
                $blood_pressure_data['updated_by'] = \Auth::id();
                $id = $input['id'];

                $blood_pressure_parameter =  \App\Models\Blood_pressure_parameter:: where('id',$id)->first();

                if($blood_pressure_parameter != null){
                   $blood_pressure_parameter->update($blood_pressure_data);
                }

                break;

            case 'pulse_ox' :

                $pulse_ox_data = $input;
                $pulse_ox_data['created_by'] = \Auth::id();
                $pulse_ox_data['updated_by'] = \Auth::id();
                $id = $input['id'];
                $pulse_ox_parameter =  \App\Models\Pulse_ox_parameter:: where('id',$id)->first();

                if($pulse_ox_parameter != null){
                   $pulse_ox_parameter->update($pulse_ox_data);
                }
                break;

            case 'blood_sugar' :

                $blood_sugar_data = $input;
                $blood_sugar_data['created_by'] = \Auth::id();
                $blood_sugar_data['updated_by'] = \Auth::id();
                $id = $input['id'];
                $blood_sugar_parameter =  \App\Models\Blood_sugar_parameter:: where('id',$id)->first();
                if($blood_sugar_parameter != null){
                   $blood_sugar_parameter->update($blood_sugar_data);
                }
                break;

        }
        return response()->json(['error' => false,'response'=>"Save Vital Parameters Successfully"],200)
                         ->setCallback(\Request::input('callback'));

    }

    private function saveDefaultVitalParams($user_id) {

        $allconfiguration = \App\Models\Configuration::vitalParameters()->get(['id','title', 'param','value'])->toArray();
        $weight=array();
        $pulseox=array();
        $blood_pressure=array();
        $blood_sugar=array();

        $weight['user_id']=$user_id;
        $weight['created_by'] = \Auth::id();
        $weight['updated_by'] = \Auth::id();

        $pulseox['user_id']=$user_id;
        $pulseox['created_by'] = \Auth::id();
        $pulseox['updated_by'] = \Auth::id();

        $blood_pressure['user_id']=$user_id;
        $blood_pressure['created_by'] = \Auth::id();
        $blood_pressure['updated_by'] = \Auth::id();

        $blood_sugar['user_id']=$user_id;
        $blood_sugar['created_by'] = \Auth::id();
        $blood_sugar['updated_by'] = \Auth::id();

        foreach ($allconfiguration as $item)
        {
            if(strtolower(trim($item['title']))=='weight')
            {
                $weight[$item['param']]=$item['value'];

            }
            if(strtolower(trim($item['title']))=='pulseox')
            {
                $pulseox[$item['param']]=$item['value'];

            }
            if(strtolower(trim($item['title']))=='bloodpressure')
            {
                $blood_pressure[$item['param']]=$item['value'];

            }
            if(strtolower(trim($item['title']))=='bloodsugar')
            {
                $blood_sugar[$item['param']]=$item['value'];

            }

        }

        $weight_parameter=\App\Models\Weight_parameter::create($weight);
        $pulse_ox_parameter=\App\Models\Pulse_ox_parameter::create($pulseox);
        $blood_pressure_parameter=\App\Models\Blood_pressure_parameter::create($blood_pressure);
        $blood_sugar_parameter=\App\Models\Blood_sugar_parameter::create($blood_sugar);

    }

    public function saveProfile(Request $request) {

        $input = $request->all();

        $input['created_by']= \Auth::id();
        $input['updated_by']= \Auth::id();
        $input['program_start'] = date('Y-m-d',strtotime($input['program_start']));

        $user_profile =  \App\Models\User_profile:: where('user_id',$input['user_id'])->first();

        $user_goal =  \App\Models\User_goal:: where('user_id',$input['user_id'])->first();

        if($user_goal != null) {
             $user_goal->update($input);
              // for iPhone notification
              $notification['user_id'] = $input['user_id'];
              $notification['notification_text'] = 'PROFILE_UPDATED';
              sendNotification($notification);

        }else{
             \App\Models\User_goal::create($input);
        }


        if($user_profile != null) {
             $user_profile->update($input);
              // for iPhone notification
              $notification['user_id'] = $input['user_id'];
              $notification['notification_text'] = 'PROFILE_UPDATED';
              sendNotification($notification);

        }else{
             \App\Models\User_profile::create($input);
        }

       /* if(sizeof($input['routine_times']) >0){

            $user = \App\Models\User::findOrFail($input['user_id']);
            foreach ($input['routine_times'] as $key => $value) {
               $user_routine_times = $user->routine_times()->where('routine_time_user.routine_time_id' ,$value['routine_time_id'])->first();

                $user_routine_times->pivot->time = $value['time'];
                $user_routine_times->pivot->save();

            }
        }*/

        return response()->json(['error' => false,'response'=>"Update Profile Successfully"],200)
                         ->setCallback(\Request::input('callback'));

    }

    public function getActivity(){
        $activities = \App\Models\Activity::orderBy('sort_order','asc')->get(['id','name','value']);
        return response()->json(['error' => false,'response'=>$activities],200)
                         ->setCallback(\Request::input('callback'));

    }

    //set default routine time when patient is created.
    private function saveDefaultRoutineTimes($user_id){

        $routineTimes = \App\Models\Routine_time::all();
        if (sizeof($routineTimes) >0){

            $user = User::findOrFail($user_id);
            $allTimes = [];
            foreach ($routineTimes as $key => $value) {
                $time = ['time'=>$value['time']];
                $allTimes[$value['id']] = $time;
            }
            $user->routine_times()->attach($allTimes);
        }
    }
    public function getUserRoutinetimes($user_id){
        $user = User::findOrFail($user_id);
        $user_routines = $user->routine_times()->get();

        return response()->json(['error' => false,'response'=>$user_routines],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function getList(Request $request){
        $search_keyword = $request->get('search_keyword') ? $request->get('search_keyword') : '';
        $itemsPerPage = $request->get('itemPerPage') ? $request->get('itemPerPage') : 50;

        $users =[];
        if($this->userType == 'patient'){
            $users = User::with('assign_doctor')
                            ->with('profile')
                            ->patient();

            if($search_keyword){

                $users = $users->where('email', 'LIKE', '%'. $search_keyword .'%')
                               ->orWhere('first_name', 'LIKE', '%'. $search_keyword .'%')
                               ->orWhere('last_name', 'LIKE', '%'. $search_keyword .'%');
            }


            $users =  $users->orderBy('last_active', 'desc')
                            ->paginate($itemsPerPage);

        }else{
             $users = User::doctor()
                            ->orderBy('last_active', 'desc')
                            ->paginate(100);
        }


        return response()->json(['error' => false,'response'=>$users->toArray()],200)
                         ->setCallback(\Request::input('callback'));

    }

    public function getVitals($id){

     $vitals = User::with('weight_parameters','blood_pressure_parameters',
                           'blood_sugar_parameters','pulse_ox_parameters')->find($id);

     return response()->json(['error' => false,'response'=>$vitals],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function getProfile($id){

        $error = false;
        $user_profile = [];
        $user_profile['profile']=[];
        $user_profile['user_goal']=[];
        $user_profile['user_weight']=[];
        $user = User::find($id);
        if($user != null) {

            $user = User::with('profile','user_goal','weight')->find($id);

            $user->age = null;
            if($user->birth_date != null && $user->birth_date != '0000-00-00'){
                  $birth_date =  \Carbon\Carbon::createFromFormat('Y-m-d',$user->birth_date);
                  $user->age = $birth_date->age;
             }
             if($user->user_goal != null){
                 $user_profile['user_goal']['goal_weight'] = $user->user_goal['goal_weight'];
                 $user_profile['user_goal']['weekly_goal_id'] = $user->user_goal['weekly_goal_id'];
              }
             if($user->weight->count() > 0){
                   $user_weight = $user->weight->last();
                   $user_profile['user_weight'] = $user_weight->toArray();
              }

             $user_profile['id']= $user->id;
             $user_profile['age']= $user->age;
             $user_profile['gender']= ucfirst($user->gender);
             $user_profile['profile']['program_start'] = date('F d, Y');
             if($user->profile != null){
                $user_profile['profile']= $user->profile->toArray();
                if($user->profile->program_start != null){
                   $user_profile['profile']['program_start'] = date('F d, Y',strtotime($user->profile->program_start));
                }
                unset($user_profile['profile']['created_by'],
                     $user_profile['profile']['updated_by'],
                     $user_profile['profile']['updated_by'],
                     $user_profile['profile']['created_at'],
                     $user_profile['profile']['updated_at'],
                     $user_profile['profile']['deleted_at']
                   );
             }

             // $routine_times =   $user->routine_times->toArray();

             // foreach ($routine_times as $key => $routine_time) {
             //        $user_profile['routine_times'][$key]['event_type'] =  $routine_time['event_type'];
             //        $user_profile['routine_times'][$key]['event_name'] =  $routine_time['event_name'];
             //        $user_profile['routine_times'][$key]['routine_time_id'] =  $routine_time['pivot']['routine_time_id'];
             //        $user_profile['routine_times'][$key]['time'] =  $routine_time['pivot']['time'];
             // }


            $result = $user_profile;

        }else{
            $error = true;
            $result = "No Patient Found.";
        }

      return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function getUser($id){

       $error = false;
       $user = User::with('profile')->find($id);

       if($user != null) {
           $user['birth_date'] = date('F d, Y',strtotime($user['birth_date']));
           if($user['user_type'] == 'patient'){
                $user['news_letter'] = isset($user->profile->news_letter) ? $user->profile->news_letter: 0;
            }
           $result = $user;
        }else{
            $error = true;
            $result = "No Patient Found.";
        }
      return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));

    }

    public function getWeeklygoals(){
        $getWeeklygoals = \App\Models\Weeklygoals::orderBy('sort_order','asc')->get(['id','name','value','short_name','sort_order']);

        return response()->json(['error' => false,'response'=>$getWeeklygoals],200)
                         ->setCallback(\Request::input('callback'));

    }


    public function getDoctorTitles(){

        $doctor_titles = \App\Models\DoctorTitle::orderBy('sort_order','asc')->get(['id','name']);

        return response()->json(['error' => false,'response'=>$doctor_titles],200)
                         ->setCallback(\Request::input('callback'));
    }

    //save medicine into database
    public function saveMedication(Request $request){

        $error = false;
        $input = $request->all();
        $data = array("med_frequency"=> isset($input['new_medicine']['med_frequency']) ? $input['new_medicine']['med_frequency'] : null,
                      "config"=> json_encode(isset($input['new_medicine']['config']) ? $input['new_medicine']['config'] : null),
                      "days"=> isset($input['new_medicine']['days']) ? $input['new_medicine']['days'] : null,
                      "start"=> isset($input['new_medicine']['start']) ? date('Y-m-d',strtotime($input['new_medicine']['start'])) : null ,
                      "end"=> isset($input['new_medicine']['end']) ? date('Y-m-d',strtotime($input['new_medicine']['end'])) : null,
                      "time"=>json_encode(isset($input['new_medicine']['time']) ? $input['new_medicine']['time'] : null),
                    );
        $user = User::findOrFail($input['new_medicine']['user_id']);

        if($user != null) {
             $result = $user->medicines()->attach($input['new_medicine']['medicine_id'],$data);
        }else{
            $error = true;
            $result = "No Patient Found.";
        }

         return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    //getting user medication list
    public function getUserMedicinelist($id){

        $error = false;
        $user = User::find($id);
        $data = [];
        $medicines = [];

        if($user != null) {

            $user_medicines = $user->medicines()->orderBy('id', 'DESC')->get();
            if(sizeof($user_medicines) > 0){
                foreach ($user_medicines as $user_medicine) {
                    $data['medicine'] = $user_medicine['pivot'];
                    $data['medicine']['frequency_times'] = $user_medicine['frequency_times'];
                    $data['medicine']['name'] = $user_medicine['name'];
                    $data['medicine']['generic_name'] = $user_medicine['generic_name'];
                    $data['medicine']['pivot_start'] = date('F d, Y',strtotime($user_medicine['pivot_start']));
                    $data['medicine']['pivot_end'] = date('F d, Y',strtotime($user_medicine['pivot_end']));
                    $data['medicine']['time'] = json_decode( $user_medicine['pivot']['time']);
                    //$data['medicine']['dose_strength'] = $user_medicine['dose_strength'];
                   // $data['medicine']['dose_form'] = $user_medicine['dose_form'];
                   // $data['medicine']['dose'] = $user_medicine['dose'];
                  //  $data['medicine']['uom'] = $user_medicine['uom'];
                   // $data['medicine']['route'] = $user_medicine['route'];
                    $data['medicine']['config'] = json_decode($user_medicine['pivot']['config']);
                    $data['medicine']['display_medicine_name'] = $user_medicine['display_medicine_name'];
                    $data['medicine']['display_medicine_direction'] = $user_medicine['display_medicine_direction'];
                    $medicines[] = $data['medicine'];
                }
            }

            $result = $medicines;
        }else{
            $error = true;
            $result = "No Patient Found.";
        }

        return response()->json(['error' => false,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    //deleting medicine of a user
    public function deleteMedicine($user_id,$medicine_id){

        $error =false;
        $user = User::find($user_id);
        if(!$user){
          $error= true;
        }
        $user->medicines()->detach($medicine_id);

        return response()->json(['error' =>$error],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function getUserSchedule($id){

        $error = false;
        $schedule = [];
        $user = User::with('profile')->find($id);
        $user_program_start = date('Y-m-d');

        if($user != null) {
           $user_schedule = $user->workouts()->get();
           if($user->profile != null){
             $user_program_start = $user->profile->program_start;
           }
           $schedule['program_start'] = $user_program_start;
           foreach ($user_schedule as $key => $workout) {
                $workout->video_count  = count($workout['videos']);
                $config = json_decode($workout->pivot->config,1);
                $workout->pivot->start = date('F d, Y',strtotime($workout->pivot->start));
                $workout->pivot->end = date('F d, Y',strtotime($workout->pivot->end));
                $workout->week_day = $config['week_day'];
                if(!empty($workout['videos'])){
                    foreach ($workout['videos'] as $key => $video) {
                         if (!is_object($video->pictures)) {
                            $video->pictures = json_decode($video->pictures);
                            $video->pictures->imageUrl = $video->pictures->sizes[4]->link;
                        }
                    }

               }

            }

           $schedule['user_schedule'] = $user_schedule->toArray();
           $result = $schedule;


        }else{
            $error = true;
            $result = "No Patient Found.";
        }

        return response()->json(['error' => false,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));


    }

    public function attachedTemplate(Request $request){

        $input = $request->all();
        $user_id = $input['user_id'];
        $template_id = $input['template_id'];
        $workouts = $input['workouts'];
        $result = [];
        $template_array = [];
        $error = false;
        $user = \App\Models\User::findOrFail($user_id);
        if($user != null){

            $user_templates = $user->templates()->get(['id'])->toArray();
            //dd($user_templates);
            if(!empty($user_templates)){
                foreach ($user_templates as $key => $value) {
                    $template_array[] = $value['id'];
                }
                //dd($template_array);
                if(!in_array($template_id, $template_array)){
                    $result = $user->templates()->attach($template_id);
                }
            }
            else{
                $result = $user->templates()->attach($template_id);
            }
              //  $result = $user->templates()->attach($template_id);
            foreach ($workouts as $key => $workout) {
                    $workout_id = $workout['id'];
                    $start = date('Y-m-d',strtotime($workout['pivot']['start']));
                    $end = date('Y-m-d',strtotime($workout['pivot']['end']));
                    $config = json_encode(['week_day'=>$workout['pivot']['config']]);
                    $pivot_data = ['config'=> $config,'start'=>$start,'end'=>$end];
                    $user->workouts()->attach($workout_id,$pivot_data);
            }
        }else{
            $error = true;
            $result = "No Patient Found.";
        }
        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));

    }

    public function deleteUserWorkout($user_id,$workout_id){

        $error =false;
        $user = \App\Models\User::findOrFail($user_id);
        if(!$user){
          $error= true;
        }else{
          $user->workouts()->detach($workout_id);
        }
        return response()->json(['error' =>$error],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function getDurations(){
        $durations = \App\Models\Program_duration::get(['id','name','days']);
        return response()->json(['error' => false,'response'=>$durations],200)
                         ->setCallback(\Request::input('callback'));

    }

    public function register(Request $request){
        $input = $request->all();
        $error =false;
        $status_code = 200;

          if(!empty($input)){

            try{
                // check the validation
                // $validator = Validator::make($request->all(), [
                //    'email' => 'required|email|max:255|unique:users,email',
                //     'password' => 'required',

                // ]);
                 //$response =  $this->auth0Repository->create($input);

                 if(isset($response['isError'])){

                    $error = true;
                    $result['message']='Email has already been taken';
                     return response()->json(['error' => $error,
                                             'response'=>$result],200)
                                      ->setCallback(\Request::input('callback'));
                }

                $confirmation_code = str_random(30);
                $input['user_type']='patient';
                $input['password'] = \Hash::make($request->password);
                $input['isGuest']= '1';
                $input['status']= 'Incomplete';
                $input['username'] =  str_random(10);
                $input['confirmation_code']=$confirmation_code;
                $input['auth0_user_id'] = $response['user_id'];
               // insert record in users table
                $user = User::create($input);



                // insert record in user_profiles table
                $user_profile['user_id']=$user->id;
                $user_profile['news_letter']= isset($input['news_letter']) ? $input['news_letter'] : 0;
                $user_profile['activity_id']=1;


                \App\Models\User_profile::create($user_profile);

                // insert record in all vitals table
               // $this->saveDefaultVitalParams($user->id);
                // insert record in user routine times table

                // insert record in user routine times table name list following
               // $this->saveDefaultRoutineTimes($user->id);
                $result['message']='Your registration has been successfully, Please check your email for verification.';
                $result['user_id'] = $user->id;
                $result['auth0_user_id'] = $user->auth0_user_id;
                $result['email_varify'] = 0;

                 $email_data = array();
                 $email_data['subject']='E-mail Verification';
                 $email_data['message'] = "Genemedics Health & Wellness";
                 $email_data['toemail']= $user->email;
                 $email_data['confirmation_code']= $user->confirmation_code;
                // sendEmail('verify_email',$email_data);

            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

          }else{
            $error = true;
            $status_code = 400;
            $result['message']= 'Request data is empty';
          }

         return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function login(Request $request, Auth0Interface $auth0Repository){
        $this->auth0Repository = $auth0Repository;
        $input = $request->all();
        $error =false;
        $status_code = 200;
        if(!empty($input)){
            try{
                $response =  $this->auth0Repository->login($input);
                if($response['error']){
                  return response()->json(['error' => $response['error'],'response'=>$response['error_message']],$status_code)
                                  ->setCallback(\Request::input('callback'));
                }else{
                  $user = User::where('email',$input['email'])->first();
                  $auth0_user_id = $input['auth0_user_id'];
                  $user->update(['password'=>\Hash::make($request->password)]);
                }
                $email =$input['email'];
                $password = $input['password'];

                // check auth0 user id from request parameter
                if(isset($input['auth0_user_id'])){
                    $user = User::where('email',$input['email'])->first();
                    if($user == null){
                        $input['password'] = \Hash::make($request->password);
                        $input['isGuest']= '1';
                        $input['status']= 'Incomplete';
                        $input['username'] =  str_random(10);
                        $input['user_type']='patient';
                        $user = User::create($input);

                        // insert record in user_profiles table
                        $user_profile['user_id']=$user->id;
                        $user_profile['news_letter']= 0;
                        $user_profile['activity_id']=1;
                        \App\Models\User_profile::create($user_profile);

                    }else{
                       //$auth0_user_id = $input['auth0_user_id'];
                       //$user->update(['auth0_user_id'=>$auth0_user_id,'password'=>\Hash::make($request->password)]);
                    }
                    if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {
                      $result['user_id']=$user->id;
                      $result['username']=$user->username;
                      $result['first_name']=$user->first_name;
                      $result['last_name']=$user->last_name;
                      $result['email_varify']=$user->email_varify;
                      $result['status']=$user->status;
                    }else{
                      $error = true;
                      $status_code = 401;
                      $result['message']='Username or password incorrect';
                    }
                }else{
                    if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {
                         $result['user_id']=\Auth::guard('admin')->User()->id;
                         $result['username']=\Auth::guard('admin')->User()->username;
                         $result['first_name']=\Auth::guard('admin')->User()->first_name;
                         $result['last_name']=\Auth::guard('admin')->User()->last_name;
                         $result['email_varify']=\Auth::guard('admin')->User()->email_varify;
                         $result['status']=\Auth::guard('admin')->User()->status;

                         //Update last active date
                         $user = User::find(\Auth::guard('admin')->User()->id);
                         if($user){
                            $user->last_active = \Carbon\Carbon::now();

                            $user->save();
                         }

                      }else{
                        $error = true;
                        $status_code = 401;
                        $result['message']='Username or password incorrect';
                    }
               }

            }catch(\Exception $ex){

               $error = true;
               $status_code = $ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();
            }

        }else{
           $error = true;
           $status_code = 400;
           $result['message']='Request data is empty';
        }
         return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function getUserProfile($id){

        $error =false;
        $status_code = 200;
        if($id !="" && $id > 0){
            try{
                $user = User::with('profile')->find($id);
                if($user != null){
                    $result['user_id'] =$user->id;
                    $result['username'] =$user->username;
                    $result['email'] =$user->email;
                    $result['first_name'] =$user->first_name;
                    $result['last_name'] =$user->last_name;
                    $result['gender'] =$user->gender;
                    $result['photo'] =$user->photo;
                    $result['birth_date'] =$user->birth_date;
                    $result['email_varify'] =$user->email_varify;
                    $result['membership_code'] = $user->membership_code;
                    if($user->profile != null){
                        unset($user->profile->created_at);
                        unset($user->profile->created_by);
                        unset($user->profile->updated_by);
                        unset($user->profile->updated_at);
                        unset($user->profile->deleted_at);
                        $result['profile'] =$user->profile->toArray();
                    }


                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function updateUserProfile(Request $request){

       $input = $request->all();
       $error =false;
       $status_code = 200;
        if(!empty($input)){
           try{

                $user_id = $input['user_id'];
                $user = User::with('profile')->find($user_id);

                if($user != null){
                    $userdata['birth_date'] = date('Y-m-d',strtotime($input['birth_date']));
                    $userdata['first_name']=$input['first_name'];
                    $userdata['last_name']=$input['last_name'];
                   // $userdata['username']=  str_random(10);
                    $userdata['gender']= $input['gender'];
                    if($user->first_name == null || $user->status == 'Incomplete'){
                        $userdata['photo']= strtolower($input['gender']).".jpg";
                        $userdata['status']= 'Guest';
                        $email_data = array();
                        $email_data['subject']='Welcome to Genemedics!';
                        $email_data['message'] = "Genemedics Health & Wellness";
                        $email_data['toemail'] = $user->email;
                        $email_data['name'] = $input['first_name']. " ". $input['last_name'];
                      //  sendEmail('welcome_message_app',$email_data);

                    }
                    $user->update($userdata);
                    if($user->auth0_user_id != null && $user->auth0_user_id !=""){
                        $user_metadata = ['first_name'=>$input['first_name'],'last_name'=>$input['last_name']];
                        $update_data['user_metadata'] = $user_metadata;
                        //$this->auth0Repository->updateUser($user->auth0_user_id,$update_data);
                    }


                     $profile_data =[];
                    //calculate the calories
                    $calories = calculateUsCalories($input);
                    $profile_data['user_id'] =$user_id;
                    $profile_data['activity_id'] =$input['activity_id'];
                    $profile_data['height_in_feet'] =$input['height_in_feet'];
                    $profile_data['height_in_inch'] =$input['height_in_inch'];
                    $profile_data['baseline_weight'] =$input['baseline_weight'];
                    $profile_data['calories'] =$calories;
                    $profile_data['water_intake'] = $profile_data['baseline_weight']*0.5;
                    // insert record in user profile table
                    if($user->profile != null){
                        // update user profile
                       $user_profile =  \App\Models\User_profile:: where('user_id',$input['user_id'])->first();
                       $user_profile->update($profile_data);
                    }else{
                        // create user profile
                       \App\Models\User_profile::create($profile_data);
                    }

                    // // End: assign meal template
                    // $template_gender = isset($input['gender']) && $input['gender'] == 'male' ? 'Male':'Female';

                    // $default_template = \App\Models\Meal_template::where('template_gender', $template_gender)
                    //                          ->where('is_default', 1)
                    //                          ->first();
                    // if($default_template!=null){
                    //     $user->meal_template()->attach($default_template->id);
                    // }

                    // End: assign meal template

                     $sleep_goal = \App\Models\User_sleep_goal::where('user_id',$user_id)->first();
                        if(is_null($sleep_goal)){
                            $sleep_data['user_id']=$user_id;
                            $sleep_data['goal']=8;
                            $sleep_data['start_date']=date('Y-m-d');
                            $sleep_data['goal_date']=addDayswithdate(date('Y-m-d'),7);
                           \App\Models\User_sleep_goal::create($sleep_data);
                        }

                    $result['message']='Save Profile suceessfully.';
                    $result['user_id'] = $user_id;
                   // \Auth::loginUsingId($user->id);


                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }

            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error = true;
            $status_code = 400;
            $result['message']= 'Request data is empty';
        }

         return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }
    public function assignPatientToDoctor(Request $request){
        $error = false;
        $result= [];
        $input = $request->all();
        $doctor = \App\Models\User::doctor()->find($input['doctor_id']);

        if($doctor != null){
            $doctor->assign_patient()->attach($input['patient_id']);
            $result['user'] =  \App\Models\User::patient()->find($input['patient_id']);
        }
        else{
            $error = true;
            $result = "No Doctor Found.";
        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function getNonAssignPatients($id){

        $error = false;
        $doctor = \App\Models\User::doctor()->find($id);

        if($doctor != null){
            $result =  \App\Models\User::patient()
                       ->whereNotIn('users.id',function($user) use($id){
                        $user->select(\DB::raw('assign_patients.patient_id from assign_patients where doctor_id="'.$id.'"'));
                       })->get();
        }
        else{
            $error = true;
            $result = "No Doctor Found.";
        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));

    }

    public function getAssignPatients($id){
        $error = false;

        $doctor = \App\Models\User::doctor()->find($id);

        if($doctor != null){
            $result = $doctor->assign_patient()->with('profile')->get();

        }
        else{
            $error = true;
            $result = "No Doctor Found.";
        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function unassignPatientFromDoctor($doctor_id,$patient_id){

        $error =false;
        $result= [];

        $doctor = \App\Models\User::doctor()->find($doctor_id);
        if(!$doctor){
          $error= true;
        }
       $result = $doctor->assign_patient()->detach($patient_id);

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function getUnassignDoctorsList($id){
        $error = false;
        $patient = \App\Models\User::patient()->find($id);

        if($patient != null){
            $result =  \App\Models\User::doctor()
                       ->whereNotIn('users.id',function($user) use($id){
                        $user->select(\DB::raw('assign_patients.doctor_id from assign_patients where patient_id="'.$id.'"'));
                       })->get();
        }
        else{
            $error = true;
            $result = "No Patient Found.";
        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function assignedDoctor(Request $request){
        $error = false;
        $result= [];
        $input = $request->all();
        $patient = \App\Models\User::patient()->find($input['patient_id']);

        if($patient != null){
            $patient->assign_doctor()->attach($input['doctor_id']);
            $result['user'] =  \App\Models\User::doctor()->find($input['doctor_id']);
        }
        else{
            $error = true;
            $result = "No Doctor Found.";
        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function removeDoctor($doctor_id,$patient_id){
        $error =false;
        $result= [];

        $patient = \App\Models\User::patient()->find($patient_id);
        if(!$patient){
          $error= true;
        }
        $patient->assign_doctor()->detach($doctor_id);
        $result['user'] =  \App\Models\User::doctor()->find($doctor_id);

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function calculateCalories($user_id, Request $request){

        $result= [];
        $error =false;
        $status_code = 200;

        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::with(['profile'])->find($user_id);

                if($user != null){
                  $current_calories = 0;

                  $remaing_calories =0;
                  $req_date = $request->get('req_date');
                  if($req_date && !empty($req_date)){
                    $date = date('Y-m-d', strtotime($req_date));
                  }
                  else{
                    $date = date('Y-m-d');
                  }

                  if($user->profile != null){
                    $current_calories = $user->profile->calories;
                  }
                  $calorie_goal = 0;
                  $exercise_goal = 0;

                  $user_calories_goal = \App\Models\User_calories_goal::where('user_id', $user_id)->first();

                  if($user_calories_goal){
                        $calorie_goal = $user_calories_goal->calorie_intake_g;
                        $exercise_goal = $user_calories_goal->calorie_burned_g;
                  }

                  $calories_consumed = \App\Models\Nutrition_user::where('user_id',$user_id)
                                                             ->where('serving_date',$date)
                                                             ->sum('calories');
                  $calories_burned = \App\Models\Exercise_user::where('user_id',$user_id)
                                                             ->where('exercise_date',$date)
                                                             ->sum('calories_burned');

                  $result['calorie_goal'] = $calorie_goal;
                  $result['exercise_goal'] = $exercise_goal;

                  $result['calories_consumed'] = $calories_consumed;
                  if($calorie_goal!=0) {
                    $calories_consumed_persent = ($calories_consumed/$calorie_goal)*100;
                    $result['calories_consumed_persent'] = round($calories_consumed_persent, 2);
                  }
                  else{
                    $result['calories_consumed_persent'] = 0;
                  }

                  $result['calories_burned'] = $calories_burned;
                  if($exercise_goal!=0) {
                        $calories_burned_persent = ($calories_burned/$exercise_goal)*100;
                        $result['calories_burned_persent'] = round($calories_burned_persent, 2);
                  }
                   else{
                    $result['calories_burned_persent'] = 0;
                  }



                  $total_calories = $calories_consumed - $calories_burned;
                  $total_calorie_goal = $calorie_goal - $exercise_goal;

                  $result['total_calories'] = $total_calories;
                  $result['total_calorie_goal'] = $total_calorie_goal;

                  if($total_calorie_goal!=0){
                        $total_calories_persent = ($total_calories/$total_calorie_goal)*100;
                        $result['total_calories_persent'] = round($total_calories_persent, 2);
                  }
                   else{
                    $result['total_calories_persent'] = 0;
                  }




                  $User_water_goal = \App\Models\User_water_goal::where('user_id', $user_id)->first();

                  if($User_water_goal){
                    $water_goal = $User_water_goal->water_g;

                  }
                  else{
                    $water_goal = 0;
                  }
                  $water_goal = round($water_goal/8);
                  $result['water_goal'] = $water_goal;
                  $total_waterlog = $this->getTotalWaterLog($user_id, $date);
                  $result['total_waterlog'] = $total_waterlog;
                  $result['remaining_waterToDrink'] = $water_goal - $total_waterlog;;

                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function calculateMonthlyCalories($user_id, Request $request){

        $result= [];
        $error =false;
        $status_code = 200;

        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::with(['profile'])->find($user_id);

                if($user != null){
                    $current_calories = 0;

                    $req_month = $request->get('req_month');
                    $req_year = $request->get('req_year');

                    $month = $req_month ? $req_month : date('m');
                    $year = $req_year ? $req_year : date('Y');

                    if($user->profile != null){
                        $current_calories = $user->profile->calories;
                    }

                    $exersie_user = \App\Models\Exercise_user::where('user_id',$user_id)
                                                         ->whereMonth('exercise_date', '=', $month)
                                                         ->whereYear('exercise_date', '=', $year)
                                                         ->selectRaw('exercise_date, sum(calories_burned) as sum')
                                                         ->groupBy('exercise_date')
                                                         ->pluck('sum','exercise_date');

                    $nutrition_user = \App\Models\Nutrition_user::where('user_id',$user_id)
                                                         ->whereMonth('serving_date', '=', $month)
                                                         ->whereYear('serving_date', '=', $year)
                                                         ->selectRaw('serving_date, sum(calories) as sum')
                                                         ->groupBy('serving_date')
                                                         ->pluck('sum','serving_date');

                    $water_intake_user = \App\Models\WaterIntake::where('user_id',$user_id)
                                                         ->whereMonth('log_date', '=', $month)
                                                         ->whereYear('log_date', '=', $year)
                                                         ->selectRaw('log_date, sum(log_water) as sum')
                                                         ->groupBy('log_date')
                                                         ->pluck('sum','log_date');

                    $temp_date = $year."-".$month."-01";
                    $month_day = date("t", strtotime($temp_date));
                    $data1 = array();

                    $data = [];
                    for($i=1; $i<=$month_day; $i++){

                        $total_exercise = 0;
                        $total_cal = 0;
                        $total_water_intake = 0;

                        $month_date = strtotime($i."-".$month."-".$year);

                        foreach ($exersie_user as $key => $exercise_value) {

                            $exercise_date = strtotime($key);

                            if($month_date == $exercise_date){
                                $total_exercise = $exercise_value;
                                break;
                            }
                        }

                        foreach ($nutrition_user as $key1 => $nutrition_value) {

                            $nutrition_date = strtotime($key1);

                            if($month_date == $nutrition_date){
                                $total_cal = $nutrition_value;
                                break;
                            }
                        }

                        foreach ($water_intake_user as $key2 => $water_value) {

                            $waterlog_date = strtotime($key2);

                            if($month_date == $waterlog_date){
                                $total_water_intake = $water_value;
                                break;
                            }
                        }

                        $total_consumed = $total_cal - $total_exercise;
                        $remaining_calories = $current_calories - $total_consumed;

                        if( $remaining_calories && $remaining_calories != $current_calories ){

                            $data1['total_consumed'] = $total_consumed;

                            $data1['water_intake'] = round($total_water_intake/8);
                            $data1['date'] = date('Y-m-d', $month_date);
                            $data1['day'] = date('d', $month_date);
                            $data1['month'] = date('m', $month_date);
                            $data1['year'] = date('Y', $month_date);

                            if($remaining_calories>0){

                                $data1['diff_text'] = "Deficit";
                                $data1['diff_text_color'] = "#33CC33";

                            }
                            elseif($remaining_calories<0){

                                $data1['diff_text'] = "Excess";
                                $data1['diff_text_color'] = "#FF1A1A";
                            }
                            else{

                                $data1['diff_text'] = "Neutral";
                                $data1['diff_text_color'] = "#FDE674";
                            }
                            $data1['diff'] = -$remaining_calories;
                            $data[] = $data1;

                        }

                    }

                    $result['date_wise'] = $data;

                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function getAverageCalorie($user_id, Request $request){

        $result= [];
        $error =false;
        $status_code = 200;

        // get weekly average data
        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::with(['profile'])->find($user_id);

                if($user != null){

                    $current_calories = 0;
                    if($user->profile != null){
                        $current_calories = $user->profile->calories;
                    }

                    $req_month = $request->get('req_month');
                    $req_year = $request->get('req_year');

                    $month = $req_month ? $req_month : date('m');
                    $year = $req_year ? $req_year : date('Y');

                    $weekly_data = [];
                    $temp_date = $year."-".$month."-01";
                    $month_day = date("t", strtotime($temp_date));
                    $first_day_this_month = date('Y-m-d', strtotime('01-'.$month.'-'.$year)); // hard-coded '01' for first day
                    $last_day_this_month  = date('Y-m-d', strtotime($month_day.'-'.$month.'-'.$year));

                    $month_weeks = array();
                    for($date = $first_day_this_month; $date <= $last_day_this_month; $date = date('Y-m-d', strtotime($date. ' + 7 days')))
                    {
                        $week =  date('W', strtotime($date));
                        $year =  date('Y', strtotime($date));

                        $from = date("Y-m-d", strtotime("{$year}-W{$week}-0"));

                        if($from < $first_day_this_month) $from = $first_day_this_month;
                        $to = date("Y-m-d", strtotime("{$year}-W{$week}-6"));

                        if($to > $last_day_this_month) $to = $last_day_this_month;

                        $month_weeks[] = array("start_date" => $from, "end_date" => $to );

                    }

                    $week_no = 1;
                    $total_consumed_monthly = 0;
                    $total_water_intake_monthly = 0;
                    for($j=0; $j < count($month_weeks); $j++){

                        $start_date = date('Y-m-d', strtotime($month_weeks[$j]['start_date']));
                        $end_date = date('Y-m-d', strtotime($month_weeks[$j]['end_date']));

                        $datetime1 = date_create($start_date);
                        $datetime2 = date_create($end_date);
                        $diff_days = date_diff($datetime1, $datetime2);
                        $diff_days = $diff_days->format('%d');
                        $diff_days = $diff_days +1;

                        $weekly_total_consumed = 0;
                        $weekly_water_intake = 0;
                        $weekly_current_calories = 0;

                        $total_exercise_week_date = 0;
                        $total_cal_week_date = 0;
                        $total_water_intake_week_date = 0;

                        $exersie_user_weekly = \App\Models\Exercise_user::where('user_id',$user_id)
                                                         ->whereBetween('exercise_date', [$start_date, $end_date]);

                        $weekly_exercise_count = $exersie_user_weekly->where('calories_burned', '<>', '0')->groupBy('exercise_date')->get(); // is not null

                        $exersie_user_weekly = $exersie_user_weekly->selectRaw('exercise_date, sum(calories_burned) as sum')
                                                                   ->pluck('sum','exercise_date');


                        $total_weekly_exercise = 0;
                        foreach ($exersie_user_weekly as $key => $value) {
                            if(!empty($value)){
                                $total_weekly_exercise = $total_weekly_exercise + $value;
                            }
                        }

                        $nutrition_user_weekly = \App\Models\Nutrition_user::where('user_id',$user_id)
                                                                        ->whereBetween('serving_date', [$start_date, $end_date]);

                                                                /*->where('serving_date', '>=', $start_date)
                                                                ->where('serving_date', '<=', $end_date);*/
                                                            //
                        $weekly_nutrition_count = $nutrition_user_weekly->where('calories', '<>', '0')->groupBy('serving_date')->get(); // is not null

                        $nutrition_user_weekly = $nutrition_user_weekly->selectRaw('serving_date, sum(calories) as sum')
                                                            ->pluck('sum','serving_date');

                       /* $nutrition_user_weekly = \App\Models\Nutrition_user::where('user_id',$user_id)
                                                ->whereBetween('serving_date', [$start_date, $end_date])
                                                ->selectRaw('serving_date, sum(calories) as sum')
                                                ->pluck('sum','serving_date');*/

                        $total_weekly_nutritions = 0;
                        foreach ($nutrition_user_weekly as $key2 => $value2) {
                            if(!empty($value2)){
                                $total_weekly_nutritions = $total_weekly_nutritions + $value2;

                            }
                        }

                        $water_intake_weekly = \App\Models\WaterIntake::where('user_id',$user_id)
                                                             ->whereBetween('log_date', [$start_date, $end_date]);

                        $weekly_intake_count = $water_intake_weekly->where('log_water', '<>', '0')->groupBy('log_date')->get(); // is not null

                        $water_intake_weekly = $water_intake_weekly->selectRaw('log_date, sum(log_water) as sum')
                                                                    ->pluck('sum','log_date');

                        $total_weekly_water_intake = 0;
                        foreach ($water_intake_weekly as $key3 => $value3) {
                            if(!empty($value3)){
                                $total_weekly_water_intake = $total_weekly_water_intake + $value3;
                            }
                        }


                        $weekly_data_day_count = max(sizeof($weekly_nutrition_count), sizeof($weekly_nutrition_count), sizeof($weekly_intake_count));
                        if($diff_days>0 && $weekly_data_day_count >0){

                            $total_consumed_weekly = $total_weekly_nutritions - $total_weekly_exercise;

                            $total_consumed_monthly = $total_consumed_monthly + $total_consumed_weekly;
                            $total_water_intake_monthly = $total_water_intake_monthly + $total_weekly_water_intake;

                            $curr_weekly_calories = $current_calories;

                            $weekly_remaining_calories = ( ($curr_weekly_calories * $diff_days) - $total_consumed_weekly) / $diff_days;

                            $weekly_data1 = array();


                            if( $weekly_remaining_calories && $weekly_remaining_calories != $curr_weekly_calories ){

                                $weekly_data1['total_consumed_weekly_avg'] = round( $total_consumed_weekly/$diff_days , 2 );

                                $weekly_data1['total_weekly_water_intake_avg'] = round( ( $total_weekly_water_intake/8 ) / $diff_days , 1 );


                                if($weekly_remaining_calories>0){

                                    $weekly_data1['weekly_diff_text_avg'] = "Deficit";
                                    $weekly_data1['weekly_diff_text_class_avg'] = "text-deficit";

                                }
                                elseif($weekly_remaining_calories<0){

                                    $weekly_data1['weekly_diff_text_avg'] = "Excess";
                                    $weekly_data1['weekly_diff_text_class_avg'] = "text-excess";
                                }
                                else{

                                    $weekly_data1['weekly_diff_text_avg'] = "Neutral";
                                    $weekly_data1['weekly_diff_text_class_avg'] = "text-neutral";
                                }

                                $weekly_data1['weekly_diff_avg'] = - round($weekly_remaining_calories , 2 );

                                $weekly_data[$j+1] = $weekly_data1;
                            }
                        }
                        else{
                            $weekly_data[$j+1] = [];
                        }

                    }
                    //exit;
                    $result['week_wise_avg'] = $weekly_data;

                    $monthly_average = [];

                    $monthly_average['monthly_consumed_avg'] = round( ($total_consumed_monthly) / $month_day , 2);
                    $monthly_average['monthly_water_avg'] = round( ($total_water_intake_monthly/8) / $month_day , 1);
                    $monthly_remain_calorie = round( (($current_calories *$month_day) - $total_consumed_monthly)/ $month_day , 2);

                    if($monthly_remain_calorie>0){

                        $monthly_average['monthly_diff_text_avg'] = "Deficit";
                        $monthly_average['monthly_diff_text_class_avg'] = "text-deficit";

                    }
                    elseif($weekly_remaining_calories<0){

                        $monthly_average['monthly_diff_text_avg'] = "Excess";
                        $monthly_average['monthly_diff_text_class_avg'] = "text-excess";
                    }
                    else{

                        $monthly_average['monthly_diff_text_avg'] = "Neutral";
                        $monthly_average['monthly_diff_text_class_avg'] = "text-neutral";
                    }

                    $monthly_average['monthly_diff_avg'] = -$monthly_remain_calorie;

                    $result['monthly_avg'] = $monthly_average;
                }
                else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        // end: get weekly average data
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function getUserHealthDiary(Request $request){

        $error = false;
        $input = $request->all();
        $filter = array();
        if(!empty($input['filter'])){
            $filter = explode(',', $input['filter']);
        }

        $user_id = $input['user_id'];
        $diary_from = isset($input['diary_from']) ? $input['diary_from'] : date("Y-m-d");
        $diary_to = isset($input['diary_to']) ? $input['diary_to'] : date("Y-m-d");
        $input['filter']= isset($input['filter']) ? $input['filter'] : 'all';
        if($user_id > 0){

            $user = \App\Models\User::find($user_id);

            $nutrition_user_log = \App\Models\Nutrition_user::with('nutritions')->where('user_id','=',$user_id)->where('serving_date','>=',$diary_from)
                                        ->whereDate('serving_date','<=',$diary_to)->orderBy('serving_date','asc')->get();

            $exercise_user_log = \App\Models\Exercise_user::with('exercise')->where('user_id','=',$user_id)->where('exercise_date','>=',$diary_from)
                                        ->whereDate('exercise_date','<=',$diary_to)->orderBy('exercise_date','asc')->get();

            $weight_user_log = \App\Models\User_weight::where('user_id','=',$user_id)->where('log_date','>=',$diary_from)
                                        ->whereDate('log_date','<=',$diary_to)->orderBy('log_date','asc')->get();

            $diary_log_data = new \Illuminate\Database\Eloquent\Collection;

            if(in_array('all_filter', $filter)){

                $diary_log_data = append_array_values($nutrition_user_log, $exercise_user_log);
                $diary_log_data = append_array_values($diary_log_data, $weight_user_log);

            }
            else{
                //start
                if(in_array('food_filter', $filter)){
                    $diary_log_data =$nutrition_user_log;
                }
                if(in_array('exercise_filter', $filter)){
                    $diary_log_data = append_array_values($diary_log_data, $exercise_user_log);
                }
                if(in_array('weight_filter', $filter)){
                    $diary_log_data = append_array_values($diary_log_data, $weight_user_log);
                }
                //end
            }

            $health_diary = new \Illuminate\Database\Eloquent\Collection;
            $health_diary_date_data = $diary_log_data->groupBy('execute_date');
            //$exercise_dates = $exercise_user_log->groupBy('exercise_date');
            //dd($serving_dates->toArray());
            foreach ($health_diary_date_data as $key => $date_log) {

                $grouped = $date_log->groupBy(function ($item, $key) {
                    if($item->schedule_time){
                        return strtoupper($item->schedule_time);
                    }
                    if($item->exercise_type){
                        return strtoupper($item->exercise_type);
                    }
                    if($item->current_weight){
                        return strtoupper("weight");
                    }
                });

                $health_diary->push($grouped);
            }

            /*foreach ($exercise_dates as $key => $exercise_log) {

                $grouped = $exercise_log->groupBy(function ($item, $key) {
                    return strtoupper($item->exercise_type);
                });

                $exercise_diary->push($grouped);
            }*/

            foreach ($health_diary as $key => $dates) {
                $gexercise_total = [];
                $exercise_total_time = 0;
                $exercise_total_calories_burned = 0;

                $gfood_total = [];
                $food_total_calories = 0;
                $food_total_carb = 0;
                $food_total_fat = 0;
                $food_total_protein = 0;
                $food_total_sodium = 0;
                $food_total_sugars = 0;

                $weight = [];

                foreach ($dates as $key2 => $schedule_time) {

                    if($key2=='STRENGTH' || $key2='CARDIO'){
                        foreach ($schedule_time as $key3 => $values) {
                            $exercise_total_time = $exercise_total_time + $values->time;
                            $exercise_total_calories_burned = $exercise_total_calories_burned + $values->calories_burned;
                        }

                    }
                    if($key2=='BREAKFAST' || $key2='STRENGTH' || $key2='LUNCH' || $key2='SNACKS'){

                        foreach ($schedule_time as $key3 => $values) {
                            $food_total_calories = $food_total_calories + $values->calories;
                            $food_total_carb = $food_total_carb + $values->total_carb;
                            $food_total_fat = $food_total_fat + $values->total_fat;
                            $food_total_protein = $food_total_protein + $values->protein;
                            $food_total_sodium = $food_total_sodium + $values->sodium;
                            $food_total_sugars = $food_total_sugars + $values->sugars;
                        }

                    }
            if($key2=='WEIGHT'){
                        foreach ($schedule_time as $key3 => $values) {
                            $current_weight = $values->current_weight ? $values->current_weight:0;
                            $body_fat = $values->body_fat ? $values->body_fat : 0;
                            $source = $values->source ? $values->source : '';
                        }
                    }
                }

                $gexercise_total['total_time'] = $exercise_total_time;
                $gexercise_total['total_calories_burned'] = $exercise_total_calories_burned;

                $health_diary[$key]['exercise_total'] = $gexercise_total;

                $gfood_total['food_total_calories'] = $food_total_calories;
                $gfood_total['food_total_carb'] = $food_total_carb;
                $gfood_total['food_total_fat'] = $food_total_fat;
                $gfood_total['food_total_protein'] = $food_total_protein;
                $gfood_total['food_total_sodium'] = $food_total_sodium;
                $gfood_total['food_total_sugars'] = $food_total_sugars;

                $health_diary[$key]['food_total'] = $gfood_total;

                $weight['current_weight'] = $current_weight;
                $weight['body_fat'] = $body_fat;
                $weight['source'] = $source;

                $health_diary[$key]['weight_data'] = $weight;
            }

            $result = $health_diary->keyBy(function ($item) {
                foreach ($item as $key => $value) {
                    foreach ($value as $key => $value) {
                        return strtoupper($value['execute_date']);
                    }
                }
            });

            /*$result['exercise_diary'] = $exercise_diary->keyBy(function ($item) {
                foreach ($item as $key => $value) {
                    foreach ($value as $key => $value) {
                        return strtoupper($value['exercise_date']);
                    }
                }
            });
            */


            $result_count = count($result);

            if(!$result){
              $error= true;
            }
        }else{
            $error = true;
            $result = "No Patient Found.";
            $result_count = 0;
        }

        if(isset($input['btn_value']) && $input['btn_value']=='download'){
            $pdf = \PDF::loadView('template.diary_pdf', compact('result', 'user', 'filter', 'result_count'))
                  ->setPaper('a4')
                  ->setOrientation('portrait');
            return $pdf->stream($user->first_name.'_'.$user->last_name.'_dairy.pdf');
        }

        return response()->json(['error' => $error,'response'=>$result, 'filter'=>$filter, 'result_count'=>$result_count],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function verifyEmail($confirmation_code,Request $request){


        $user = User::with('profile')->whereConfirmationCode($confirmation_code)->first();
        if ( ! $user)
        {
             return view('errors.404')->with('user_type','patient');
        }

        $user->email_varify = 1;
        $user->save();

        //$response =  $this->auth0Repository->verifiedEmail($user->auth0_user_id);

        $email_data = array();
        $email_data['subject']='New Registration - genemedicshealth.com';
        $email_data['toemail']= "gh@odoo.genemedics.com";
        $email_data['first_name'] = $user->first_name;
        $email_data['last_name'] = $user->last_name;
        $email_data['email'] = $user->email;
        $email_data['gender'] = $user->gender;
        $email_data['birth_date'] = date("F j, Y", strtotime($user->birth_date));
        $email_data['baseline_weight'] = $user['profile']['baseline_weight'];
        $email_data['height_in_feet'] = $user['profile']['height_in_feet'];
        $email_data['height_in_inch'] = $user['profile']['height_in_inch'];
        $email_data['calories'] = $user['profile']['calories'];
        $email_data['datetime'] = date('F j, Y, h:i:s A');
        $email_data['ip_address'] = $request->ip();
        if(app()->environment() == 'local') {
             sendEmail('odoo_email',$email_data);
        }


        return view('user.verify');
    }

    public function resendVerifiedEmail($user_id){

        $error =false;
        $status_code = 200;
        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::find($user_id);
                if($user != null){

                 $email_data = array();
                 $email_data['subject']='E-mail Verification';
                 $email_data['message'] = "Genemedics Health & Wellness";
                 $email_data['toemail']= $user->email;
                 $email_data['confirmation_code']= $user->confirmation_code;
                 sendEmail('verify_email',$email_data);
                 $result['message']='Email has been successfully sent, Please check your email for verification.';

                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }
            }catch(\Exception $ex){
               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function changeEmail(Request $request, $id){


        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();

        if(!empty($input) && $id > 0) {
            try{

                $user = \App\Models\User::find($id);

                if($user != null){

                    // $validator = Validator::make($request->all(), [
                    //   'email' => 'required|email|max:255|unique:users,email'
                    // ]);
                //$checkIsExist = $this->auth0Repository->checkEmailExist($input['email']);
                if($checkIsExist){

                    $error = true;
                    $result['message']='Email has already been taken';
                     return response()->json(['error' => $error,
                                             'response'=>$result],200)
                                      ->setCallback(\Request::input('callback'));
                }
                 $user->email = $input['email'];
                 $user->save();
                 $update_data['email'] = $input['email'];
                 //$this->auth0Repository->updateUser($user->auth0_user_id,$update_data);
                 $result['message']='Email address updated successfully.';
                 $email_data = array();
                 $email_data['subject']='E-mail Verification';
                 $email_data['message'] = "Genemedics Health & Wellness";
                 $email_data['toemail']= $user->email;
                 $email_data['confirmation_code']= $user->confirmation_code;
                 sendEmail('verify_email',$email_data);

                }else{

                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }


            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function checkStatus($user_id){

        $error =false;
        $status_code = 200;
        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::find($user_id);
                if($user != null){

                    $created = new \Carbon\Carbon($user->created_at);
                    $now = \Carbon\Carbon::now();
                    $difference = $created->diff($now)->days;
                    $result['email_varify']=$user->email_varify;
                    $result['no_of_days']=$difference;

                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function getNotification($user_id){

        $error =false;
        $status_code = 200;
        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::find($user_id);
                if($user != null){
                   $user_notification = \App\Models\Notification:: where('user_id',$user_id)->get();
                   $result = $user_notification;
                   \App\Models\Notification:: where('user_id',$user_id)->delete();
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function getPatientGoal($user_id){

        $error =false;
        $status_code = 200;
        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::with('profile')->find($user_id);
                if($user != null){
                    $user_goal_data = [];
                   $user_goal = \App\Models\User_goal::where('user_id',$user_id)->first();
                  if($user_goal != null){

                     $user_goal_data['goal_water'] = $user->profile->water_intake;
                     $user_goal_data['baseline_weight'] =$user->profile->baseline_weight;
                     $user_goal_data['calories'] =$user->profile->calories;
                     $user_goal_data['user_id'] =$user_id;
                     $user_goal_data['weekly_goal_id'] =$user_goal->weekly_goal_id;
                     $user_goal_data['goal_weight'] =$user_goal->goal_weight;
                     $user_goal_data['goal_body_fat'] =$user_goal->goal_body_fat;

                  }else{

                     $user_goal_data['baseline_weight'] =$user->profile->baseline_weight;
                     $user_goal_data['calories'] =$user->profile->calories;
                     $user_goal_data['goal_water'] = $user->profile->water_intake;
                     $user_goal_data['user_id'] =$user_id;
                     $user_goal_data['weekly_goal_id'] ='';
                     $user_goal_data['goal_weight'] ='';
                     $user_goal_data['goal_body_fat'] ='';
                 }
                 $result = $user_goal_data;
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }
    public function updatePatientGoal(Request $request,$user_id){

        $error =false;
        $status_code = 200;
        if($user_id !="" && $user_id > 0){
            try{
                $input = $request->all();

                $user = \App\Models\User::find($user_id);
                if($user != null){
                   $user_goal = \App\Models\User_goal::where('user_id',$user_id)->first();

                   if($user_goal != null){
                   $input['baseline_weight'] = isset($input['baseline_weight']) ? $input['baseline_weight'] : 0;
                   $user_goal->goal_weight = isset($input['goal_weight']) ? $input['goal_weight'] : 0;
                   $user_goal->weekly_goal_id = isset($input['weekly_goal_id']) ? $input['weekly_goal_id'] : 0 ;
                   $user_goal->goal_body_fat = isset($input['goal_body_fat']) ? $input['goal_body_fat'] : 0;
                   $user_goal->goal_water = isset($input['goal_water'])?$input['goal_water']:$input['baseline_weight']*0.5;

                    $user_goal->save();

                   }else{
                        \App\Models\User_goal:: create($input);
                  }
                  $profile['baseline_weight'] = isset($input['baseline_weight']) ? $input['baseline_weight'] : 0;
                  $profile['calories'] = isset($input['calories']) ? $input['calories']:  0;
                  $profile['water_intake'] = isset($input['goal_water'])?$input['goal_water']:$input['baseline_weight']*0.5;
                  $user->profile()->update($profile);

                  $result['message']='Goals Updated successfully.';
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }
            }catch(\Exception $ex){
               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();
            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }
    public function saveWaterintake(Request $request) {
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        $user_agent = "";
        if(!empty($input)) {
             try{
                    $user_id = $input['user_id'];
                    $user = \App\Models\User::patient()->find($user_id);
                    if($user != null){
                    if(isset($input['guid_server'])){
                        $input['guid'] = guid();
                        $current_timestamp = \Carbon\Carbon::now()->timestamp;
                        $input['timestamp'] = $current_timestamp;
                        $user_agent = 'server';
                    }

                    $check_user_waterintake = $user->WaterIntake()->where('guid', $input['guid'])->first();

                    if(!empty($check_user_waterintake)){
                        $check_user_waterintake->update($input);
                        $id = $check_user_waterintake->id;
                        $guid = $check_user_waterintake->guid;
                    }
                    else{
                        $waterIntake = $user->WaterIntake()->create($input);
                        $id = $waterIntake->id;
                        $guid = $waterIntake->guid;
                    }
                    // for iPhone notification
                    if($user_agent != "" && $user_agent == 'server'){
                        $notification['user_id'] = $user_id;
                        $notification['notification_text'] = 'WATER_LOG_ADDED';
                        sendNotification($notification);
                    }
                        $result['water_intake'] = array("id"=>$id, "guid"=>$guid);
                        $result['message']='WaterIntake Logged Successfully.';
                    }
                    else
                    {
                        $error = true;
                        $status_code = 404;
                        $result['message']='No Patient Found.';
                }
            }
            catch(\Exception $ex)
            {
                    throw $ex;
                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
            }
        }
        else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function getwaterlog($user_id) {
        $error =false;
        $status_code = 200;
        if($user_id !="" && $user_id > 0){
        try{
                $today = date('Y-m-d');
                $getwaterlog = \App\Models\WaterIntake::where('user_id',$user_id)
                                                        ->where('log_date',$today)
                                                        ->orderBy('id')->get(['id','log_water']);
                if($getwaterlog != null){
                $user_data = \App\Models\User::with('profile')->find($user_id);
                $result['water_intake'] = $user_data->profile->water_intake;
                $result['logged_water_intake'] =$getwaterlog;
               }
            }catch(\Exception $ex){
               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();
             }
        }else{
            $error = true;
            $status_code = 404;
            $result['message']='No Patient Found.';

        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function auth0Register(Request $request){

         $input = $request->all();
         $error =false;
         $status_code = 200;
         if(!empty($input)){

              try{

               //check the validation
                $validator = Validator::make($request->all(), [
                   'email' => 'required|email|max:255',
                   'password' => 'required',
                   'first_name' => 'required',
                   'last_name' => 'required'

                ]);

                 if($validator->fails()){

                    $error = true;
                    $result['message']=$validator->errors();
                     return response()->json(['error' => $error,
                                             'response'=>$result],422)
                                      ->setCallback(\Request::input('callback'));
                }


                   // check the validation
                  //$response =  $this->auth0Repository->create($input);
                  if(isset($response['isError'])){
                     $error =true;
                     $result['message'] ='Email has already been taken';
                  }else{

                    $confirmation_code = str_random(30);
                    $input['user_type']='patient';
                    $input['password'] = \Hash::make($request->password);
                    $input['isGuest']= '1';
                    $input['email_varify']= '1';
                    $input['status']= 'Incomplete';
                    $input['username'] =  str_random(10);
                    $input['confirmation_code']=$confirmation_code;
                    $input['photo']= isset($input['gender'])?strtolower($input['gender']).".jpg":'no_img.png';
                    $input['auth0_user_id'] = $response['user_id'];
                   // insert record in users table
                    $user = User::create($input);

                    // insert record in user_profiles table
                    $user_profile['user_id']=$user->id;
                    $user_profile['news_letter']= isset($input['news_letter']) ? $input['news_letter'] : 0;
                    $user_profile['activity_id']=1;
                    $user_profile['activity_value']=1.2;

                    if(isset($input['height_in_feet'])){
                         $user_profile['height_in_feet'] =$input['height_in_feet'];
                    }
                     if(isset($input['height_in_inch'])){
                         $user_profile['height_in_inch'] =$input['height_in_inch'];
                    }
                    if(isset($input['baseline_weight'])){
                         $user_profile['baseline_weight'] =$input['baseline_weight'];
                         $user_profile['water_intake'] = $input['baseline_weight']*0.5;
                    }
                    if(isset($input['gender'])){
                       $user_profile['gender'] =$input['gender'];
                    }
                    if(isset($input['birth_date'])){
                       $user_profile['birth_date'] =$input['birth_date'];
                    }
                    $calories = calculateUsCalories($user_profile);

                    if($calories > 0){
                      $user->update(['status'=>'Active']);
                    }

                    $user_profile['calories'] =$calories;

                    \App\Models\User_profile::create($user_profile);

                     $email_data = array();
                     $email_data['subject']='E-mail Verification';
                     $email_data['message'] = "Genemedics Health & Wellness";
                     $email_data['toemail']= $user->email;
                     $email_data['confirmation_code']= $user->confirmation_code;
                   //  sendEmail('verify_email',$email_data);
                     $result = $response;

                  }


                }catch(\Exception $ex){

                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }

         }else{
            $error = true;
            $status_code = 400;
            $result['message']= 'Request data is empty';
         }

         return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function auth0liveRegister(Request $request){

         $input = $request->all();
         $error =false;
         $status_code = 200;
         if(!empty($input)){

              try{

                //check the validation
                $validator = Validator::make($request->all(), [
                   'email' => 'required|email|max:255',
                   'first_name' => 'required',
                   'last_name' => 'required'
                ]);

                 if($validator->fails()){

                    $error = true;
                    $result['message']=$validator->errors();
                     return response()->json(['error' => $error,
                                             'response'=>$result],422)
                                      ->setCallback(\Request::input('callback'));
                }

                 // check the validation from auth0
                  $password = 'password';
                  $input['password'] = $password;
                  //$response =  $this->auth0Repository->create($input);
                  if(isset($response['isError'])){
                     $error =true;
                     $result['message'] ='Email has already been taken';
                  }else{

                   // $confirmation_code = str_random(30);
                    $input['user_type']='patient';
                    $input['password'] = \Hash::make($password);
                    $input['isGuest']= '1';
                    $input['email_varify']= '1';
                    $input['status']= 'Incomplete';
                    $input['username'] =  str_random(10);
                  //  $input['confirmation_code']=$confirmation_code;
                    $input['photo']= isset($input['gender'])?strtolower($input['gender']).".jpg":'no_img.png';
                    $input['auth0_user_id'] = $response['user_id'];
                   // insert record in users table
                    if(isset($input['birth_date'])){
                       $input['birth_date'] = date('Y-m-d',strtotime($input['birth_date']));
                    }

                    $user = User::create($input);

                    // insert record in user_profiles table
                    $user_profile['user_id']=$user->id;
                    $user_profile['news_letter']= isset($input['news_letter']) ? $input['news_letter'] : 0;
                    $user_profile['activity_id']=1;
                    $user_profile['activity_value']=1.2;

                    if(isset($input['height_in_feet'])){
                         $user_profile['height_in_feet'] =$input['height_in_feet'];
                    }
                     if(isset($input['height_in_inch'])){
                         $user_profile['height_in_inch'] =$input['height_in_inch'];
                    }
                    if(isset($input['baseline_weight'])){
                         $user_profile['baseline_weight'] =$input['baseline_weight'];
                         $user_profile['water_intake'] = $input['baseline_weight']*0.5;
                    }
                    if(isset($input['gender'])){
                       $user_profile['gender'] =$input['gender'];
                    }
                    if(isset($input['birth_date'])){
                       $user_profile['birth_date'] =$input['birth_date'];
                    }
                    $calories = calculateUsCalories($user_profile);

                    if($calories > 0){
                      $user->update(['status'=>'Active']);
                    }

                    $user_profile['calories'] =$calories;

                    \App\Models\User_profile::create($user_profile);

                     $email_data = array();
                     $email_data['subject']='Genemedics Nutrition Account Information';
                     $email_data['message'] = "Genemedics Health & Wellness";
                     $email_data['toemail']= $user->email;
                     $email_data['password']= $password;
                     $email_data['name']= $user->first_name ." ".$user->last_name;
                     sendEmail('live_users',$email_data);
                     $result = $response;

                  }


                }catch(\Exception $ex){

                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }

         }else{
            $error = true;
            $status_code = 400;
            $result['message']= 'Request data is empty';
         }

         return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function checkExists(Request $request){

      $validator = \Validator::make($request->all(), ['email' => 'required|email']);
      if($validator->fails()) {
            $result['message'] = $validator->errors()->first();
            return \Response::json(['error' => true, 'response' => $result], 200);
      }
      $response = false;
      $email = $request->email;
      //$isExist =  $this->auth0Repository->checkEmailExist($email);
      $checkUser = \App\Models\User::where('email',$email)->first();
      if($isExist && $checkUser != null){
        $response['email'] =$checkUser->email;
        $response['auth0_user_id'] =$checkUser->auth0_user_id;
        $response['first_name'] =$checkUser->first_name;
        $response['last_name'] =$checkUser->last_name;
      }
      return \Response::json(['response' => $response], 200);
    }

    public function auth0ChangePassword(Request $request) {

         $input = $request->all();
         $error =false;
         $status_code = 200;
         if(!empty($input)){

              try{

                //check the validation
                $validator = Validator::make($request->all(), [
                   'email' => 'required|email|max:255',
                   'password' => 'required'

                ]);

                 if($validator->fails()){

                    $error = true;
                    $result['message']=$validator->errors();
                     return response()->json(['error' => $error,
                                             'response'=>$result],200)
                                      ->setCallback(\Request::input('callback'));
                }

                  $email = $request->email;
                  //$users =  $this->auth0Repository->searchUsers('email',$email);

                  if(sizeof($users) > 0){

                    foreach ($users as $key => $value) {
                        if($value['email'] == $email){
                           $auth0_user_id = $value['user_id'];
                            $update_data['password'] =$input['password'];
                            //$this->auth0Repository->updateUser($auth0_user_id,$update_data);
                        }
                    }
                     $checkUser = \App\Models\User::where('email',$email)->first();
                      if($checkUser != null){
                         $checkUser->password = bcrypt($input['password']);
                         $result1 =  $checkUser->save();
                      }

                     $result['message'] =  'Password has been updated.';
                  }else{
                     $error = true;
                     $result['message'] =  'We can\'t find a user with that e-mail address';
                  }

                 }catch(\Exception $ex){

                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }

         }else{
            $error = true;
            $status_code = 400;
            $result['message']= 'Request data is empty';
         }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));


    }

    public function getEmail($token){

        $error =false;
        $status_code = 200;
        if($token !=""){
        try{

             $reset_data =\DB::table('password_resets')->where('token', $token)->first();
             if($reset_data != null){
                 $result['reset_data']=$reset_data;
             }else{
                 $error = true;
                 $status_code = 404;
                 $result['message']='Invalid Token';
             }
            }catch(\Exception $ex){
               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();
             }
        }else{
            $error = true;
            $status_code = 404;
            $result['message']='Token is empty.';

        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));

    }

    public function loggedWater($user_id){
        $error =false;
        $status_code = 200;
        if($user_id !="" && $user_id > 0){

        try{
              $user = \App\Models\User::find($user_id);
              if($user != null){

                $getwaterlog = \App\Models\WaterIntake::where('user_id',$user_id)->orderBy('id')->get();
                $result = $getwaterlog;

                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }
            }
            catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }


    public function saveCustomWater(Request $request) {
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();

        $user_agent = "";
        if(!empty($input)) {
             try{
                    $user_id = $input['user_id'];

                    $user = \App\Models\User::patient()->find($user_id);
                    if($user != null){
                    if(isset($input['guid_server'])){
                        $input['guid'] = guid();
                        $user_agent = 'server';
                    }
                    $check_user_custom_water = $user->User_custom_water()->where('guid', $input['guid'])->first();

                    if(!empty($check_user_custom_water)){
                        $check_user_custom_water->update($input);
                        $id = $check_user_custom_water->id;
                        $guid = $check_user_custom_water->guid;
                    }
                    else{
                        $custom_waterIntake = $user->User_custom_water()->create($input);
                        $id = $custom_waterIntake->id;
                        $guid = $custom_waterIntake->guid;
                    }
                    // for iPhone notification
                    if($user_agent != "" && $user_agent == 'server'){
                        $notification['user_id'] = $user_id;
                        $notification['notification_text'] = 'CUSTOM_WATER_LOG_ADDED';
                        sendNotification($notification);
                    }
                        $result['custom_water_intake'] = array("id"=>$id, "guid"=>$guid);
                        $result['message']='CustomWaterIntake Logged Successfully.';
                    }
                    else
                    {
                        $error = true;
                        $status_code = 404;
                        $result['message']='No Patient Found.';
                }
            }
            catch(\Exception $ex)
            {
                    throw $ex;
                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
            }
        }
        else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function loggedCustomWater($user_id){
        $error =false;
        $status_code = 200;
        if($user_id !="" && $user_id > 0){
        try{
              $user = \App\Models\User::find($user_id);
              if($user != null){

                $get_custom_waterlog = \App\Models\User_custom_water::where('user_id',$user_id)->orderBy('id')->get();
                $result = $get_custom_waterlog;

                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }
            }
            catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }


    public function deleteCustomWater($id){
        $error = false;
        $status_code = 200;
        $result =[];
        if(!empty($id)) {
            try{
                  $user_custom_water = \App\Models\User_custom_water::find($id);
                  if($user_custom_water){
                      $user_custom_water->delete();
                      $result['message'] = 'User Custom WaterIntake Removed Successfully.';
                  }
                  else{
                      $error = true;
                      $result['error_message'] = 'record does not exist.';
                  }
               }catch(\Exception $ex){
                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function deleteWater($id){

        $error = false;
        $status_code = 200;
        $result =[];
        if($id != "" && $id > 0) {
            try{
                  $water_intake = \App\Models\WaterIntake::find($id);
                  if($water_intake){
                      $guid = $water_intake->guid;
                      $water_intake->delete();
                      $result["guid"] = $guid;
                      $result['message'] = 'User Water Removed Successfully.';
                  }
                  else{
                      $error = true;
                      $result['error_message'] = 'Record does not exists.';
                  }
               }catch(\Exception $ex){
                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function getTotalWaterLog($user_id, $date){

        $total_waterlog = \App\Models\WaterIntake::where('user_id',$user_id)
                                                        ->where('log_date',$date)
                                                        ->sum('log_water');

        $total_waterlog_cup = round($total_waterlog/8);

        return $total_waterlog_cup;
    }

    public function getSwitchToDashboard( $id )
    {
      $new_user = \App\Models\User::patient()->find($id);
      Session::put( 'orig_user', Auth::id() );
      //\Auth::loginUsingId($id);dd(\Auth::guard());
      Auth::login($new_user);
      return redirect('/dashboard');
    }

    public function getSwitchToAdmin()
    {
      $id = Session::get( 'orig_user' );
      $orig_user = User::find( $id );
      Auth::login($orig_user);
      return redirect('/admin/patient');
    }

    public function getResetPassword($token,Request $request){

            $reset_data =[];
            $reset_data['token']=$token;
            $reset_data['email']=$request->email;

         return view('auth.reset_password')->with('reset_data',(object)$reset_data);
    }

    public function getCreatePassword($token){
        if($token != ""){
         $reset_data =\DB::table('password_resets')->where('token', $token)->first();
         return view('auth.create_password')->with('reset_data',$reset_data);
       }
    }

    public function getPatientEMR(){

       $vista_client = new \App\Common\VistAClient();
       $emailId =\Auth::User()->email; //"test@ttadfs.com";
       $patient_forms = $vista_client->getPatientInfo('patientForms',$emailId);
       $patient_meds =  $vista_client->getPatientInfo('patientMeds',$emailId);
       $patient_other_meds = $vista_client->getPatientInfo('patientOtherMeds',$emailId);
       $patient_vitals = $vista_client->getPatientInfo('patientVitals',$emailId);
       $patient_problems = $vista_client->getPatientInfo('patientProblems',$emailId);
       $patient_allergies = $vista_client->getPatientInfo('patientAllergies',$emailId);
       $patient_surgeries = $vista_client->getPatientInfo('patientSurgeries',$emailId);
       $familiy_history = $vista_client->getPatientInfo('patientFamilyHistory',$emailId);
      // dd($patient_surgeries);
       $result['forms'] =isset($patient_forms['forms'])?$patient_forms['forms']:[];
       $result['meds']=  isset($patient_meds['meds'])?$patient_meds['meds']:[];
       $result['othermedications']= isset($patient_other_meds['meds'])?$patient_other_meds['meds']:[];
       $result['vitals']=  isset($patient_vitals['vitals'])?$patient_vitals['vitals']:[];
       $result['problems']=  isset($patient_problems['problems'])?$patient_problems['problems']:[];
       $result['allergies']=isset($patient_allergies['result']['ALLERGIES'])?$patient_allergies['result']['ALLERGIES']:[];
       $result['surgeries']=isset($patient_surgeries['result']['SURGERIES'])?$patient_surgeries['result']['SURGERIES']:[];
       $result['familyHistory']=isset($familiy_history['familyHistory'])?$familiy_history['familyHistory']:[];


        //dd($result['forms']);
       return view('front.patient_emr')->with('result', $result);
    }

    public function setMembershipCode(Request $request, $user_id){
        $error =false;
        $status_code = 200;
        $result = [];

        $input = $request->all();
        if(!empty($user_id) && !empty($input)){

            try{
                $user = \App\Models\User::find($user_id);
                if(!empty($user)){


                    $check_other_user = \App\Models\User::where('id', '<>', $user_id)
                                                         ->where('membership_code', '<>', '')
                                                         ->where('membership_code', $input['membership_code']);
                    if($check_other_user and $check_other_user->count() > 0){
                        $error =true;
                        $result['error_message']='Membership code has already been taken.';
                    }
                    else{

                        $set_mem_code = $user->update($input);
                        if($set_mem_code){
                            $error =false;
                            $result['message']='Membership code set successfully';
                        }
                        else{
                            $error =true;
                            $result['error_message']='Membership code not set successfully';
                        }
                    }

                }
                else{
                    $error =true;
                    $result['message']='Patient Not found';
                }
            }
            catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }
        else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function saveReferedFriend(Request $request)
    {
        $error = false;
        $status_code = 200;
        $result = [];
        $input = $request->all();
        if(!empty($input)){
            try{
                $user_id = $input['user_id'];
                //$email = $input['email'];
                //$check_email = \App\Models\Refered_friend::where('email', $email)->first();
                $user = \App\Models\User::find($user_id);
                ///if($check_email == null){
                    if($user != null){
                        $created = \App\Models\Refered_friend::create($input);
                        if($created){
                            $email_data = array();
                            $email_data['toemail'] = $input['email'];
                            $email_data['first_name'] = $input['first_name'];
                            $email_data['message'] = "Genemedics Health & Wellness";
                            sendEmail('refered_friend',$email_data);
                            $result['message'] = "Refered Friend send successfully";
                        }
                        else{
                            $error = true;
                            $result['error_message'] = "Refered frined not send";
                        }
                    }else{
                        $error = true;
                        $status_code = 404;
                        $result['message']='No Patient Found.';
                    }
                /*}else
                {
                        $error = true;
                        $result['error_message'] = "already refered frined";
                }*/
            }
            catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }
        }
        else{
            $error = true;
            $result['error_message'] = "Request data is empty";
        }
        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }
    public function getReferedFriend(){
        $error = false;
        $status_code = 200;
        $referedfrend = [];
        $referedfrend = \App\Models\Refered_friend::with('user')->get();
        return response()->json(['error' => false,'response'=>$referedfrend],200)
                         ->setCallback(\Request::input('callback'));


    }

    public function referFriend()
    {
           return view('refer_friend.index')->with('user_id',\Auth::User()->id);
    }

    public function getMealTemplates($gender){
        return \App\Models\Meal_template::where('template_gender', $gender)
                                        ->pluck('template_name', 'id');
    }

    public function getUserMealTemplate($user_id){
        return \App\Models\Meal_template_user::where('user_id', $user_id)->first();
    }

    public function addCustomFood(Request $request)
    {
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        $user_agent = isset($input['user_agent'])? $input['user_agent']:'';

        if(!empty($input)) {

            try{

                $input['brand_with_name'] = $input['brand_name']." - ".$input['name'];
                $input['nutrition_data'] = json_encode($input['nutrition_data']);
                $input['serving_data'] = json_encode($input['serving_data']);

                $log_custom_food = \App\Models\Nutrition::create($input);
                if($log_custom_food){
                    $result['message']='Custom food added successfully.';
                }
                else{
                    $result['error_message']='Custom food not added successfully.';
                }


            }catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function magentoSessionLogin($email){
        $user = \App\Models\User::where('email', $email)->first();
        if(!empty($user['email'])){
            Auth::login($user);
            return redirect('/dashboard');
        }
        else {
            return redirect('/login');
        }
    }

     public function saveTimeZone($id,Request $request){
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();

        if(!empty($input) && $id > 0) {
            try{
                  $user = \App\Models\User::find($id);
                  if($user != null){
                     $user->timezone_id =$input['timezone_id'];
                     $user->timezone_name =$input['timezone_name'];
                     $user->save();
                     $checkUserTimeZone =  \DB::table('user_timezone')->where('timezone_name',$input['timezone_id'])->first();

                     if($checkUserTimeZone ==null){
                       \DB::table('user_timezone')->insert(
                            ['timezone_name' => $input['timezone_id']]
                        );
                     }
                     $result['message']='Time zone save successfully.';
                  }else{

                    $error =true;
                    $result['message']='Patient Not found';
                  }


               }catch(\Exception $ex){
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

    public function SignupSteps(Request $request, Auth0Interface $auth0Repository){

        $this->auth0Repository = $auth0Repository;
        $input = $request->all();
        $error =false;
        $status_code = 200;

        if(!empty($input)){

            try{

                $response =  $this->auth0Repository->create($input);
                if(isset($response['isError'])){
                    $error = true;
                    $result['message']='Email has already been taken';
                     return response()->json(['error' => $error,
                                             'response'=>$result],200)
                                      ->setCallback(\Request::input('callback'));
                }

                    $confirmation_code = str_random(30);
                    $input['user_type']='patient';
                    $input['password'] = \Hash::make($request->password);
                    $input['isGuest']= '1';
                    $input['status']= 'Guest';
                    $input['username'] =  str_random(10);
                    $input['confirmation_code']=$confirmation_code;
                    $input['auth0_user_id'] = $response['user_id'];
                    $input['gender']= strtolower($input['gender']);
                    $input['photo']= strtolower($input['gender']).".jpg";
                   // insert record in users table
                    $user = User::create($input);
                    // insert record in user_profiles table
                    $calories = calculateUsCalories($input);
                    $user_profile['user_id']=$user->id;
                    $user_profile['news_letter']= isset($input['news_letter']) ? $input['news_letter'] : 0;
                    $user_profile['activity_id']=$input['activity_id'];
                    $user_profile['height_in_feet'] =$input['height_in_feet'];
                    $user_profile['height_in_inch'] =$input['height_in_inch'];
                    $user_profile['baseline_weight'] =$input['baseline_weight'];
                    $user_profile['calories'] =$calories;
                    $user_profile['water_intake'] = $input['baseline_weight']*0.5;

                    $user_profile = \App\Models\User_profile::create($user_profile);

                    if($user->auth0_user_id != null && $user->first_name != null){
                        $user_metadata = ['first_name'=>$input['first_name'],'last_name'=>$input['last_name']];
                        $update_data['user_metadata'] = $user_metadata;
                        $this->auth0Repository->updateUser($user->auth0_user_id,$update_data);
                    }

                     $weight_goal['user_id'] =$user->id;
                     $weight_goal['weight_start_date'] = date('Y-m-d');
                     $weight_goal['body_fat_start_date'] = date('Y-m-d');
                     $weight_goal['start_weight'] =  isset($input['baseline_weight']) ? $input['baseline_weight'] : null;
                     $weight_goal['current_weight'] =  isset($input['baseline_weight']) ? $input['baseline_weight'] : null;
                     $weight_goal['start_body_fat'] =   isset($input['current_body_fat']) ? $input['current_body_fat'] : 0;
                     $weight_goal['current_body_fat'] =  isset($input['current_body_fat']) ? $input['current_body_fat'] : 0;
                     $weight_goal['weight_g'] = $input['weight_g'];
                     $weight_goal['body_fat_g'] = $input['body_fat_g'];
                     $weight_goal['body_fat_g'] = $input['body_fat_g'];
                     $weight_goal['weekly_goal_id'] = isset($input['weekly_goal_id']) ? $input['weekly_goal_id'] : 0;
                     \App\Models\User_weight_goal:: create($weight_goal);

                    $result['message']='Your registration has been successfully, Please check your email for verification.';
                    // $result['user_id'] = $user->id;
                    // $result['auth0_user_id'] = $user->auth0_user_id;
                    // $result['email_varify'] = 0;
                    $result['user_profile'] = User::with('profile')->find($user->id);
                    // $email_data = array();
                    // $email_data['subject']='E-mail Verification';
                    // $email_data['message'] = "Genemedics Health & Wellness";
                    // $email_data['toemail']= $user->email;
                    // $email_data['confirmation_code']= $user->confirmation_code;
                   // sendEmail('verify_email',$email_data);

            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error = true;
            $status_code = 400;
            $result['message']= 'Request data is empty';
        }

         return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

}
