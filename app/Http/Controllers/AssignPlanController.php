<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AssignPlanController extends Controller
{
    public function index($user_id){

    	$user = \App\Models\User::find($user_id);
    	if(!$user){
         return view('errors.404')->with('user_type','patient');
        }
        return view('assign_plan.index')->with('user',$user);
    }

    public function getNutritionTemplate(Request $request){
    	$gender = $request->gender;
    	$error = false;
    	$nutrition_templates = \App\Models\Meal_template::where('template_gender', $gender)->get(['id','template_name']);
    	$result['nutrition_templates'] =  $nutrition_templates->toArray();
    	return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function geMealsTemplate(Request $request){
        $gender = $request->gender;
        $error = false;
        $meal_templates = \App\Models\Recommended_meal::select(\DB::raw('*,meal_name as template_name'))
                                                        ->where('meal_template_id', "=", 1)
                                                        ->get();

        $result['meal_templates'] =  $meal_templates->toArray();
        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function geWorkoutsTemplate(){

        $error = false;
        $workout_templates = \App\Models\Workout::select(\DB::raw('*,title as template_name'))
                                                        ->where('is_admin', "=", '1')
                                                        ->get();

        $result['workout_templates'] =  $workout_templates->toArray();
        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function getUserMealPlan($user_id){

        $error =false;
        $status_code = 200;
        if($user_id !="" && $user_id > 0){
            try{

                $user = \App\Models\User::find($user_id);
                if($user != null){
                    $user_meal_plan = \App\Models\User_meal_plan::where('user_id',$user_id)->first();
                    $result['user_meal_plan'] =  $user_meal_plan;
                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='User not Found.';

                }

            }catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }

}
