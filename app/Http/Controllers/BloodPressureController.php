<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
class BloodPressureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    return view('front.bp_pulse')->with('user_id',\Auth::User()->getUserInfo()['id']);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $error = false;
        $status_code = 200;
        $result =[];
        if($id != "" && $id > 0) {
            try{
                  $user_pulse = \App\Models\User_pulse::find($id);
                  if($user_pulse){
                      $guid = $user_pulse->guid;
                      $user_pulse->delete();
                      $result["guid"] = $guid;
                      $result['message'] = 'User Pulse Removed Successfully.';
                  }
                  else{
                      $error = true;
                      $result['error_message'] = 'Record does not exists.';
                  }
               }catch(\Exception $ex){
                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }





    public function loggedBpPulse($user_id, Request $request){

        $error =false;
        $status_code = 200;
        $user_bp_pulse_goals = array();

        $from_server  = $request->get('from_server');
        $start_date  = date('Y-m-d', strtotime($request->get('start_date')) );
        $end_date  = date('Y-m-d', strtotime($request->get('end_date')) );
        $user_bp_pulse_history_data = [];

        if($user_id !="" && $user_id > 0){
            try{
                $user = \App\Models\User::find($user_id);
                if($user != null){
                    if($from_server){ //get last 7 weight
                      $user_bp_pulse_history_data = $user->blood_pressure_pulse()->with('pulse')->whereDate('log_date', '>=', $start_date)
                                                    ->whereDate('log_date', '<=', $end_date)
                                                    ->get();

                      $user_bp_pulse_last_log = $user->blood_pressure_pulse()->with('pulse')
                                                                             ->orderBy('log_date')
                                                                             ->get();


                      $user_bp_pulse_goals = $user->User_bp_pulse_goal()->get();
                      $bppulse = [];
                      $result_count = count($user_bp_pulse_goals);
                      if($result_count > 0){
                            foreach ($user_bp_pulse_goals as $item)
                              {
                                 if(($item['bp_type'])=='systolic')
                                  {
                                    $bppulse['systolic'] =  $item['goal'];
                                    $bppulse['start_date_systolic'] = $item['start_date'];
                                    $bppulse['goal_date_systolic'] = $item['goal_date'];
                                  }
                                  if(($item['bp_type'])=='diastolic')
                                  {
                                     $bppulse['diastolic'] =  $item['goal'];
                                     $bppulse['start_date_diastolic'] = $item['start_date'];
                                     $bppulse['goal_date_diastolic'] = $item['goal_date'];
                                  }
                                  if(($item['bp_type'])=='heart_rate')
                                  {
                                     $bppulse['heart_rate'] =  $item['goal'];
                                     $bppulse['start_date_pulse'] = $item['start_date'];
                                     $bppulse['goal_date_pulse'] = $item['goal_date'];
                                  }
                                  if(($item['goal_date']) != null)
                                  {
                                     $bppulse['goal_date'] =  $item['goal_date'];
                                  }
                                  if(($item['user_id']) != null)
                                  {
                                     $bppulse['user_id'] =  $item['user_id'];
                                  }
                              }
                              $bppulse_progress = new \App\Common\BpPulse_progress($bppulse);
                              $result['bppulse_progress'] =$bppulse_progress->getBpPulseProgress();
                              $result['user_bp_pulse_goals'] = $bppulse;
                            }

                          $result['user_bp_pulse_history_data'] = $user_bp_pulse_history_data ? $user_bp_pulse_history_data : [];
                          $result['user_bp_pulse_last_log'] = $user_bp_pulse_last_log ? $user_bp_pulse_last_log :[];

                    }
                    else{
                      $user_bp_pulse_history_data = $user->blood_pressure_pulse()->with('pulse')->get();
                      $result = $user_bp_pulse_history_data;
                    }

                  }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }

            }catch(\Exception $ex){
               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();
            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }
    public function loggedPulse($user_id){
        $error =false;
        $status_code = 200;

        if($user_id !="" && $user_id > 0){

        try{
              $user = \App\Models\User::find($user_id);
              if($user != null){

                $get_user_pulse = \App\Models\User_pulse::where('user_id',$user_id)->orderBy('id')->get();
                $result = $get_user_pulse;

                }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }
            }
            catch(\Exception $ex){

               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();

            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));

    }
    public function logBpPulse(Request $request){

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
        $today = date("Y-m-d");
        $user_agent = "";

        if(!empty($input)) {
            try{

                $user_id = $input['user_id'];

                $input['created_by'] = $user_id;
                $input['updated_by'] = $user_id;
                //check session
                if(Session::has('orig_user') ){
                  $input['created_by'] = Session::get( 'orig_user' );
                  $input['updated_by'] = Session::get( 'orig_user' );

                }
                $user = \App\Models\User::find($user_id);

                if($user != null){

                    if(empty($input['log_date'])){
                        $input['log_date'] = $today;
                    }
                    // get pulse id
                    $log_pulse = $this->add_pulse($input);
                    if(count($log_pulse)>0)
                    {
                        $input['user_pulse_id'] = $log_pulse['log_pulse']['id'];

                        if(isset($input['guid_server'])){
                            $current_timestamp = \Carbon\Carbon::now()->timestamp;
                            $input['guid'] = guid();
                            $input['timestamp'] = $current_timestamp;
                            $user_agent = 'server';
                        }

                        $check_user_bp_pulse = $user->blood_pressure_pulse()->where('guid', $input['guid'])
                                                          ->first();
                        if(!empty($check_user_bp_pulse)){
                            $check_user_bp_pulse->update($input);
                            $id = $check_user_bp_pulse->id;
                            $guid = $check_user_bp_pulse->guid;
                        }
                        else{
                            $log_blood_pressure = $user->blood_pressure_pulse()->create($input);
                            $id = $log_blood_pressure->id;
                            $guid = $log_blood_pressure->guid;
                        }

                        // for iPhone notification
                        if($user_agent != "" && $user_agent == 'server'){
                            $notification['user_id'] = $user_id;
                            $notification['notification_text'] = 'BP_PULSE_ADDED';
                            sendNotification($notification);
                        }
                        $result['log_blood_pressure'] = array("id"=>$id, "guid"=>$guid);
                        $result['message']='Blood Pressure And Pulse Logged Successfully.';
                    }
                    else{
                      $error = true;
                      $result['message']='Pulse data not inserted properly';
                    }
                }else{

                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }


            }catch(\Exception $ex){
                throw $ex;
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function logPulse(Request $request){

        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();

        $result = $this->add_pulse($input);

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function add_pulse($input){
        $result = [];
        $today = date("Y-m-d");
        $user_agent = "";
        if(!empty($input)) {
            try{

                $user_id = $input['user_id'];

                $user = \App\Models\User::find($user_id);

                if($user != null){

                    if(empty($input['log_date'])){
                        $input['log_date'] = $today;
                    }

                    if(isset($input['guid_server'])){
                        $current_timestamp = \Carbon\Carbon::now()->timestamp;
                        $input['guid'] = guid();
                        $input['timestamp'] = $current_timestamp;
                        $user_agent = 'server';
                    }

                    $check_user_pulse = $user->user_pulse()->where('guid', $input['guid'])
                                                      ->first();

                    if(!empty($check_user_pulse)){

                        $check_user_pulse->update($input);
                        $id = $check_user_pulse->id;
                        $guid = $check_user_pulse->guid;
                    }
                    else{

                        $log_pulse = $user->user_pulse()->create($input);
                        $id = $log_pulse->id;
                        $guid = $log_pulse->guid;
                    }

                    if($id>0){
                      // for iPhone notification
                      if($user_agent != "" && $user_agent == 'server'){
                          $notification['user_id'] = $user_id;
                          $notification['notification_text'] = 'BP_PULSE_ADDED';
                          sendNotification($notification);
                      }

                      $result['log_pulse'] = array("id"=>$id, "guid"=>$guid );
                      $result['message']='Pulse Logged Successfully.';
                    }


                }else{

                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
                }
             }catch(\Exception $ex){
                throw $ex;
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return $result;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteBpPulse($id)
    {

        $error = false;
        $status_code = 200;
        $result =[];
        if($id != "" && $id > 0) {
            try{
                  $user_bp_pulse = \App\Models\Blood_pressure_pulse::find($id);
                  if($user_bp_pulse){
                      $guid = $user_bp_pulse->guid;
                      $user_pulse_id = $user_bp_pulse->user_pulse_id;
                      $user_bp_pulse->delete();
                      $user_pulse = \App\Models\User_pulse::find($user_pulse_id);
                      if($user_pulse){
                         $user_pulse->delete();
                      }
                      $result["guid"] = $guid;
                      $result['message'] = 'User Bp & Pulse Removed Successfully.';
                  }
                  else{
                      $error = true;
                      $result['error_message'] = 'Record does not exists.';
                  }
               }catch(\Exception $ex){
                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }
        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function calculateMonthlyBpPulsesummary($user_id, Request $request){
        $result= [];
        $error =false;
        $status_code = 200;

        if($user_id !="" && $user_id > 0){
            try{
                $user = \App\Models\User::find($user_id);
                if($user != null){
                    $current_summary = 0;

                    $req_month = $request->get('req_month');
                    $req_year = $request->get('req_year');


                    $month = $req_month ? $req_month : date('m');
                    $year = $req_year ? $req_year : date('Y');
                    $user_bp_pulse_data = \DB::select('SELECT
                                        user_pulse.*, user_bp_pulse.`systolic`, user_bp_pulse.`diastolic`
                                        FROM
                                        user_pulse
                                        INNER JOIN
                                            (SELECT MAX(u.id) AS maxid
                                             FROM user_pulse u WHERE u.user_id=? GROUP BY u.log_date) AS user_pulse2 ON
                                             user_pulse.id = user_pulse2.maxid
                                        INNER JOIN user_bp_pulse ON user_pulse.id = user_bp_pulse.`user_pulse_id`
                                        WHERE user_pulse.`deleted_at` IS NULL
                                        AND user_pulse.user_id = ?
                                        AND MONTH(user_pulse.log_date) = ?
                                        AND YEAR(user_pulse.log_date) = ?
                                        AND user_bp_pulse.`deleted_at` IS NULL
                                        ORDER BY user_pulse.`log_date` ASC', [$user_id, $user_id, $month, $year]);

                      $user_goals = $user->User_bp_pulse_goal()->where('user_id', $user_id)->get();

                      $bp_pulse_goal = [];
                      if($user_goals->count() > 0){
                         $bp_pulse_goal['bp_type'] = $user_goals['0']['goal_date'];
                         foreach ($user_goals as $key => $bp_pulse_data) {
                                  if($bp_pulse_data->bp_type == 'systolic'){
                                     $bp_pulse_goal['systolic'] = $bp_pulse_data->goal;
                                  }else if($bp_pulse_data->bp_type == 'diastolic'){
                                     $bp_pulse_goal['diastolic'] = $bp_pulse_data->goal;
                                  }else if($bp_pulse_data->bp_type == 'heart_rate'){
                                     $bp_pulse_goal['heart_rate'] = $bp_pulse_data->goal;
                              }
                           }
                        }
                      $result['goals_data'] = $bp_pulse_goal ? $bp_pulse_goal:null;
                      $result['bp_pulse_data'] = $user_bp_pulse_data;
                      $weekly_data_bp_pulse = $this->getAverageSystolicDystolicPulse($user_id, $req_month,$req_year,$user);
                      $result['avg_data'] = $weekly_data_bp_pulse;

                 }else{
                    $error = true;
                    $status_code = 404;
                    $result['message']='No Patient Found.';
               }
            }catch(\Exception $ex){
               $error = true;
               $status_code =$ex->getCode();
               $result['error_code'] = $ex->getCode();
               $result['error_message'] = $ex->getMessage();
            }
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }


    public function getAverageSystolicDystolicPulse($user_id, $req_month, $req_year,$user){
    $result= [];
        // get weekly average data
        if($user_id !="" && $user_id > 0){
                    $current_systolic = 0;
                    $month = $req_month ? $req_month : date('m');
                    $year = $req_year ? $req_year : date('Y');

                    $weekly_data = [];
                    $temp_date = $year."-".$month."-01";

                    $month_day = date("t", strtotime($temp_date));

                    $first_day_this_month = date('Y-m-d', strtotime('01-'.$month.'-'.$year)); // hard-coded '01' for first day

                    $last_day_this_month  = date('Y-m-d', strtotime($month_day.'-'.$month.'-'.$year));

                    $month_weeks = array();
                    for($date = $first_day_this_month; $date <= $last_day_this_month; $date = date('Y-m-d', strtotime($date. ' + 7 days')))
                    {
                        $week =  date('W', strtotime($date));

                        $year =  date('Y', strtotime($date));

                        $from = date("Y-m-d", strtotime("{$year}-W{$week}-0"));

                        if($from < $first_day_this_month) $from = $first_day_this_month;

                        $to = date("Y-m-d", strtotime("{$year}-W{$week}-6"));

                        if($to > $last_day_this_month) $to = $last_day_this_month;

                        $month_weeks[] = array("start_date" => $from, "end_date" => $to );

                    }
                    $week_no = 1;
                    $total_sys = 0;
                    $total_dystolic = 0;
                    $total_pulse = 0;
                    for($j=0; $j < count($month_weeks); $j++){

                       $start_date = date('Y-m-d', strtotime($month_weeks[$j]['start_date']));
                       $end_date = date('Y-m-d', strtotime($month_weeks[$j]['end_date']));
                        $datetime1 = date_create($start_date);
                        $datetime2 = date_create($end_date);

                        $diff_days = date_diff($datetime1, $datetime2);
                        $diff_days = $diff_days->format('%d');

                        $diff_days = $diff_days +1;
                        $user_goals = $user->User_bp_pulse_goal()->where('user_id', $user_id)->get();
                        $bm_goal = [];
                        if($user_goals->count() > 0){

                          $bm_goal['bp_type'] = $user_goals['0']['goal_date'];

                          foreach ($user_goals as $key => $bm_data) {
                                  if($bm_data->bp_type == 'systolic'){
                                     $bm_goal['systolic'] = $bm_data->goal;
                                  }else if($bm_data->bp_type == 'diastolic'){
                                       $bm_goal['diastolic'] = $bm_data->goal;
                                  }else if($bm_data->bp_type == 'heart_rate'){
                                       $bm_goal['heart_rate'] = $bm_data->goal;
                            }
                          }
                        }
                        // get user goals data
                        $result['goals_data'] = $bm_goal ? $bm_goal:null;
                        $systolic_goal = $result['goals_data']['systolic'];
                        $diastolic_goal = $result['goals_data']['diastolic'];
                        $pulse_goal = $result['goals_data']['heart_rate'];

                        $sum = \DB::select('SELECT
                                  user_pulse.*, user_bp_pulse.`systolic`, user_bp_pulse.`diastolic`,user_pulse.`pulse`
                                  FROM
                                  user_pulse
                                  INNER JOIN
                                      (SELECT MAX(u.id) AS maxid
                                       FROM user_pulse u WHERE u.user_id=?  GROUP BY u.log_date ) AS user_pulse2 ON
                                       user_pulse.id = user_pulse2.maxid
                                  INNER JOIN user_bp_pulse ON user_pulse.id = user_bp_pulse.`user_pulse_id`
                                  WHERE (user_pulse.log_date BETWEEN ? AND ?)
                                  AND user_pulse.user_id = ?
                                  ORDER BY user_pulse.`log_date` ASC', [$user_id, $start_date,$end_date,$user_id]);


                        $systolic_sum_data = 0;
                        $diastolic_sum_data = 0;
                        $pulse_sum_data = 0;

                        foreach($sum as $key=>$value)
                        {
                           $systolic_sum_data += $value->systolic;
                           $diastolic_sum_data += $value->diastolic;
                           $pulse_sum_data += $value->pulse;
                        }

                        $systolic_sum =  $systolic_sum_data;
                        $diastolic_sum =   $diastolic_sum_data;
                        $pulse_sum  = $pulse_sum_data;


                        $total_sys = $total_sys + $systolic_sum;
                        $total_dystolic = $total_dystolic + $diastolic_sum;
                        $total_pulse = $total_pulse + $pulse_sum;
                        $weekly_data_day_count = max($systolic_sum, $diastolic_sum, $pulse_sum);

                        if($diff_days > 0 && $weekly_data_day_count > 0){

                            $weekly_data1 = array();

                                $weekly_data1['total_systolic_weekly_avg'] = round( $systolic_sum/$diff_days , 2 );
                                $weekly_data1['total_diastolic_weekly_avg'] = round( $diastolic_sum/$diff_days , 2 );
                                $weekly_data1['total_pulse_weekly_avg'] = round( $pulse_sum/$diff_days , 2 );

                                $calculatePercent_systolic = calculateBpPercent($weekly_data1['total_systolic_weekly_avg'], $systolic_goal);
                                // compare goals data and set automatically css class.
                                if($calculatePercent_systolic > 95){
                                    $weekly_data1['weekly_systolic_class_avg'] = "text-deficit";
                                }
                                elseif($weekly_data1['total_systolic_weekly_avg'] > 139){
                                    $weekly_data1['weekly_systolic_class_avg'] = "text-excess";
                                }
                                else{
                                    $weekly_data1['weekly_systolic_class_avg'] = "text-neutral";
                                }
                                // diastolic
                                $calculatePercent_diastolic = calculateBpPercent($weekly_data1['total_diastolic_weekly_avg'], $diastolic_goal);

                                if($calculatePercent_diastolic > 95){
                                    $weekly_data1['weekly_diastolic_class_avg'] = "text-deficit";
                                }
                                elseif($weekly_data1['total_diastolic_weekly_avg'] > 89){
                                    $weekly_data1['weekly_diastolic_class_avg'] = "text-excess";
                                }
                                else{
                                    $weekly_data1['weekly_diastolic_class_avg'] = "text-neutral";
                                }

                                $calculatePercent_pulse_week = calculateBpPercent($weekly_data1['total_pulse_weekly_avg'], $pulse_goal);

                                if($calculatePercent_pulse_week > 95){
                                    $weekly_data1['weekly_pulse_class_avg'] = "text-deficit";
                                }
                                elseif($weekly_data1['total_pulse_weekly_avg'] > 149 ){
                                    $weekly_data1['weekly_pulse_class_avg'] = "text-excess";
                                }
                                else{
                                    $weekly_data1['weekly_pulse_class_avg'] = "text-neutral";
                                }


                            $weekly_data[$j+1] = $weekly_data1;

                        }
                        else{
                            $weekly_data[$j+1] = [];
                        }

                    }
                  // return weekly data
                  $result['week_wise_avg'] = $weekly_data;
                  // intilization montly data
                  $monthly_average = [];

                  $monthly_average['monthly_avg_systolic'] = round( ($total_sys) / $month_day , 2);

                  $monthly_average['monthly_avg_dystolic'] = round( ($total_dystolic) / $month_day , 2);

                  $monthly_average['monthly_avg_pulse'] = round( ($total_pulse) / $month_day , 2);

                    // set automatically css class using compate goals systolic
                    $calculatePercent_systolic_month = calculateBpPercent($monthly_average['monthly_avg_systolic'], $systolic_goal);

                    if($calculatePercent_systolic_month > 95){
                        $monthly_average['monthly_systolic_class_avg'] = "text-deficit";
                    }elseif($monthly_average['monthly_avg_systolic'] > 139){
                        $monthly_average['monthly_systolic_class_avg'] = "text-excess";
                    }else{
                        $monthly_average['monthly_systolic_class_avg'] = "text-neutral";
                    }
                    //dystolic
                    $calculatePercent_dystolic_month = calculateBpPercent($monthly_average['monthly_avg_dystolic'], $diastolic_goal);

                    if($calculatePercent_dystolic_month > 95){
                        $monthly_average['monthly_dystolic_class_avg'] = "text-deficit";
                    }elseif($monthly_average['monthly_avg_dystolic'] > 89){
                        $monthly_average['monthly_dystolic_class_avg'] = "text-excess";
                    }else{
                        $monthly_average['monthly_dystolic_class_avg'] = "text-neutral";
                    }
                    //heart rate
                    $calculatePercent_pulse_month = calculateBpPercent($monthly_average['monthly_avg_pulse'], $pulse_goal);

                    if($calculatePercent_pulse_month > 95){
                        $monthly_average['monthly_pulse_class_avg'] = "text-deficit";
                    }elseif($monthly_average['monthly_avg_pulse'] > 149){
                        $monthly_average['monthly_pulse_class_avg'] = "text-excess";
                    }else{
                        $monthly_average['monthly_pulse_class_avg'] = "text-neutral";
                    }
                    $result['monthly_avg'] = $monthly_average;

        }
        // end: get weekly average data
        return $result;
    }
}
