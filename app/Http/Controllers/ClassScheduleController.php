<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ClassScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $isEventSetting =0;
        $event_settings = \App\Models\Event_setting::all();
        if($event_settings->count() > 0){
           $isEventSetting =1;
        }
        return view('class_schedule.class_schedules')->with('isEventSetting', $isEventSetting);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // public function createClass(Request $request){
    //     $input = $request->all();
    //    // dd($input);

    //     $class = \App\Models\Class_schedule::create($input);

    //     $class->class_schedule_days()->create($input['class_days']);
    //     dd($input);
    //     //$input['']
    // }

    public function updateEventSetting(Request $request){


        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();


        if(!empty($input)) {
            try{

                $checkIsEmpty = \App\Models\Event_setting::all();
                if($checkIsEmpty->count() > 0){
                    \DB::table('event_settings')->update(['is_active' => 0]);
                }

                $config =[];
                $event_type = $input['type'];
                if($event_type == 'google_calendar'){
                    $config['api_key']= $input['api_key'];
                    $config['calendarId']=$input['calendarId'];
                }else if($event_type == 'events_url'){
                   $config['url']=$input['url'];
                }else if($event_type == 'events_pdf'){
                     $config['file_name']=$input['pdf_name'];
                     $config['pdf_url']= '/events-pdf/'.$input['pdf_name'];
                }
                $event_setting_data =[];
                $event_setting_data['type']=$event_type;
                $event_setting_data['is_active']=1;
                $event_setting_data['config']=json_encode($config);
                $check_event_settings =  \App\Models\Event_setting::where('type',$event_type)->first();
                if($check_event_settings != null){
                   $check_event_settings->update($event_setting_data);
                }else{

                 \App\Models\Event_setting::create($event_setting_data);
                }
                $result['message']='Event Settings Updated Successfully.';
            }catch(\Exception $ex){

                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }

        }else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }

        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    public function getEventSettings(){

        $setting_data =[];
        $event_settings = \App\Models\Event_setting::all();
        if($event_settings->count() > 0){
           foreach ($event_settings as $key => $setting) {
             $event_type = $setting['type'];
             $config = json_decode($setting['config'],1);
             if($event_type == 'google_calendar'){
                $setting_data['google_calendar']['api_key']= $config['api_key'];
                $setting_data['google_calendar']['calendarId']= $config['calendarId'];
             }else if($event_type == 'events_url'){
               $setting_data['events_url']['url']= $config['url'];
            }else if($event_type == 'events_pdf'){
                $setting_data['events_pdf']['file_name']= $config['file_name'];
                $setting_data['events_pdf']['pdf_url']= $config['pdf_url'];
            }
             if($setting['is_active'] == 1){
                $setting_data['active_event']['event_type']= $event_type;
                $setting_data['active_event']['is_active']= $setting['is_active'];
             }

           }
        }
        return response()->json(['error' => false,'response'=>$setting_data],200)
                         ->setCallback(\Request::input('callback'));

    }

    public function getActiveSettings(Request $request){
        $server_timestamp = \Carbon\Carbon::now()->timestamp;
        $timestamp = $request->get('timestamp');
        $compare_date = gmdate("Y-m-d H:i:s", $timestamp);
        if($timestamp != null){
            $event_settings = \App\Models\Event_setting::where('is_active',1)
                                                        ->where('updated_at','>=',$compare_date)
                                                        ->orWhere('deleted_at','>=',$compare_date)
                                                        ->withTrashed()
                                                        ->first();
        }
        else{
            $event_settings = \App\Models\Event_setting::where('is_active',1)->first();
        }

        if($event_settings != null){
           $event_settings->config = json_decode($event_settings->config,1);
        }
        return response()->json(['error' => false,'timestamp'=>$server_timestamp,'response'=>$event_settings],200)
                         ->setCallback(\Request::input('callback'));

    }


    public function uploadImage(Request $request){


        if ($request->hasFile('image_file')) {
            $file_name = $request->file('image_file')->getClientOriginalName();
            $destination_path = 'events-pdf/';
            $request->file('image_file')->move($destination_path, $file_name);
        }

        return response()->json(['error' => false,'file_name'=>$file_name],200)
                         ->setCallback(\Request::input('callback'));

    }

    public function removeImage($file_name){

        $destination_path = public_path().'/events-pdf/'.$file_name;
        @unlink($destination_path);
        return response()->json(['error' => false,'file_name'=>$file_name],200)
                         ->setCallback(\Request::input('callback'));
    }

    public function displayEvents()
    {
      return view('front.display_events');
    }

}
