<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ManageFoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('manage_food.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $error = false;
        $status_code = 200;

        $result = [];
        try{
            
            $edit_food_data = \App\Models\Nutrition::where('id',$id)->first();
            $edit_food_data['nutrition_data'] = json_decode($edit_food_data['nutrition_data'],1);
            $edit_food_data['serving_data'] =   json_decode($edit_food_data['serving_data'],1);

            if($edit_food_data){
                $result = $edit_food_data;
            }
            else{
                $error = true;
                $result['error_message'] = "No record found";   
            } 
        }
        catch(\Exception $ex){
            $error = true;
            $status_code = $ex->getCode();
            $result['error_code'] = $ex->getCode();
            $result['error_message'] = $ex->getMessage();
        }

        return response()->json(['error' => $error,'response'=>$result],200)
                         ->setCallback(\Request::input('callback'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $error = false;
        $status_code = 200;
        $result =[];
        $input = $request->all();
       
        if(!empty($input) && $id > 0) {
            try {
                    $nutrition_data =[];
                    $nutrition_data['calories']=$input['nutrition_data']['calories'];
                    $nutrition_data['sodium']=$input['nutrition_data']['sodium'];
                    $nutrition_data['total_fat']=$input['nutrition_data']['total_fat'];
                    $nutrition_data['potassium']=$input['nutrition_data']['potassium'];
                    $nutrition_data['saturated_fat']=$input['nutrition_data']['saturated_fat'];
                    
                    $nutrition_data['total_carb']=$input['nutrition_data']['total_carb'];
                    $nutrition_data['polyunsaturated']=$input['nutrition_data']['polyunsaturated'];
                    $nutrition_data['dietary_fiber']=$input['nutrition_data']['dietary_fiber'];
                    $nutrition_data['monounsaturated']=$input['nutrition_data']['monounsaturated'];
                    $nutrition_data['sugars']=$input['nutrition_data']['sugars'];

                    $nutrition_data['protein']=$input['nutrition_data']['protein'];
                    $nutrition_data['trans_fat']=$input['nutrition_data']['trans_fat'];
                    $nutrition_data['cholesterol']=$input['nutrition_data']['cholesterol'];
                    $nutrition_data['vitamin_a']=$input['nutrition_data']['vitamin_a'];
                    $nutrition_data['calcium_dv']=$input['nutrition_data']['calcium_dv'];

                    $nutrition_data['vitamin_c']=$input['nutrition_data']['vitamin_c'];
                    $nutrition_data['iron_dv']=$input['nutrition_data']['iron_dv'];
                    $nutrition_data['serving_quantity']=$input['nutrition_data']['serving_quantity'];
                    $nutrition_data['serving_size_unit']=$input['nutrition_data']['serving_size_unit'];

                    $food_input_data =[];
                    $food_input_data['source_name'] = $input['source_name'];
                    $food_input_data['name'] = $input['name'];
                    $food_input_data['brand_name'] = $input['brand_name'];
                    $food_input_data['nutrition_data']=json_encode($nutrition_data);
                    $food_input_data['serving_data'] = json_encode($input['serving_data']);

                    $manage_food_update = \App\Models\Nutrition::where('id',$id)->first();
                    if(!empty($manage_food_update)) {
                        $manage_food_update->update($food_input_data);
                        $result['message']='Food Updated Successfully.';
                    } else {
                        $error = true;
                        $status_code = 404;
                        $result['message']='No Food Found.';
                    }   
                } catch(\Exception $ex) {
                    $error = true;
                    $status_code = $ex->getCode();
                    $result['error_code'] = $ex->getCode();
                    $result['error_message'] = $ex->getMessage();
                }
        } else{
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $error = false;
        $status_code = 200;
        $result =[];

        if(!empty($id)) {
            try { 
                $food = \App\Models\Nutrition::where('id', $id)->first();
                if($food){
                    $food->delete();
                    $result['message'] = 'Food deleted Successfully.';
                } else {
                    $error = true;
                    $result['error_message'] = 'record does not exist.';
                }
            } catch(\Exception $ex) {
                $error = true;
                $status_code = $ex->getCode();
                $result['error_code'] = $ex->getCode();
                $result['error_message'] = $ex->getMessage();
            }
        } else {
            $error =true;
            $status_code = 400;
            $result['message']='Request data is empty';
        }
        return response()->json(['error' => $error,'response'=>$result],$status_code)
                         ->setCallback(\Request::input('callback'));
    }
}
