<?php

namespace App\Http\ViewComposers;

//use Illuminate\Contracts\View\View;
use Illuminate\View\View;
//use Illuminate\Users\Repository as UserRepository;

class ProUIConfigComposer
{
   /**
 * config.php
 *
 * Author: plenartech
 *
 * Configuration file. It contains variables used in the template as well as the primary navigation array from which the navigation is created
 *
 */



    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if(\Session::has('organization_id')){
                $organization_id = \Session::get('organization_id');
                $organization = \App\Models\Organization::find($organization_id);
                $view->with('organization',$organization);
                            
        }

        if(\Auth::check() && \Auth::user()->user_type == "super_admin"){
            $view->with('template', [
            'name'=> 'Genemedics',
            'version'           => '1.0',
            'author'            => 'genemedics',
            'robots'            => 'noindex, nofollow',
            'title'             => 'Genemedics - Health and Wellness',
            'description'       => 'Genemedics - Health and Wellness.',
            // true                     enable page preloader
            // false                    disable page preloader
            'page_preloader'    => true,
            // true                     enable main menu auto scrolling when opening a submenu
            // false                    disable main menu auto scrolling when opening a submenu
            'menu_scroll'       => true,
            // 'navbar-default'         for a light header
            // 'navbar-inverse'         for a dark header
            'header_navbar'     => 'navbar-default',
            // ''                       empty for a static layout
            // 'navbar-fixed-top'       for a top fixed header / fixed sidebars
            // 'navbar-fixed-bottom'    for a bottom fixed header / fixed sidebars
            'header'            => '',
            // ''                                               for a full main and alternative sidebar hidden by default (> 991px)
            // 'sidebar-visible-lg'                             for a full main sidebar visible by default (> 991px)
            // 'sidebar-partial'                                for a partial main sidebar which opens on mouse hover, hidden by default (> 991px)
            // 'sidebar-partial sidebar-visible-lg'             for a partial main sidebar which opens on mouse hover, visible by default (> 991px)
            // 'sidebar-mini sidebar-visible-lg-mini'           for a mini main sidebar with a flyout menu, enabled by default (> 991px + Best with static layout)
            // 'sidebar-mini sidebar-visible-lg'                for a mini main sidebar with a flyout menu, disabled by default (> 991px + Best with static layout)
            // 'sidebar-alt-visible-lg'                         for a full alternative sidebar visible by default (> 991px)
            // 'sidebar-alt-partial'                            for a partial alternative sidebar which opens on mouse hover, hidden by default (> 991px)
            // 'sidebar-alt-partial sidebar-alt-visible-lg'     for a partial alternative sidebar which opens on mouse hover, visible by default (> 991px)
            // 'sidebar-partial sidebar-alt-partial'            for both sidebars partial which open on mouse hover, hidden by default (> 991px)
            // 'sidebar-no-animations'                          add this as extra for disabling sidebar animations on large screens (> 991px) - Better performance with heavy pages!
            'sidebar'           => 'sidebar-partial sidebar-visible-lg sidebar-no-animations',
            // ''                       empty for a static footer
            // 'footer-fixed'           for a fixed footer
            'footer'            => '',
            // ''                       empty for default style
            // 'style-alt'              for an alternative main style (affects main page background as well as blocks style)
            'main_style'        => '',
            // ''                           Disable cookies (best for setting an active color theme from the next variable)
            // 'enable-cookies'             Enables cookies for remembering active color theme when changed from the sidebar links (the next color theme variable will be ignored)
            'cookies'           => '',
            // 'night', 'amethyst', 'modern', 'autumn', 'flatie', 'spring', 'fancy', 'fire', 'coral', 'lake',
            // 'forest', 'waterlily', 'emerald', 'blackberry' or '' leave empty for the Default Blue theme
            'theme'             => '',
            // ''                       for default content in header
            // 'horizontal-menu'        for a horizontal menu in header
            // This option is just used for feature demostration and you can remove it if you like. You can keep or alter header's content in page_head.php
            'header_content'    => 'horizontal-menu',
            'active_page'       => '/'.\Request::segment(1)
                                    ]);
            $view->with('primary_nav', [
                array(
                    'name'  => 'Patients',
                    'opt'   => '<a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a>' .
                               '<a href="javascript:void(0)" data-toggle="tooltip" title="Create the most amazing pages with the widget kit!"><i class="gi gi-lightbulb"></i></a>',
                    'url'   => '/patient',
                    'icon'  => 'fa fa-user fa-fw',
                ),
                array(
                    'name'  => 'Doctors',
                    'opt'   => '<a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a>' .
                               '<a href="javascript:void(0)" data-toggle="tooltip" title="Create the most amazing pages with the widget kit!"><i class="gi gi-lightbulb"></i></a>',
                    'url'   => '/doctor',
                    'icon'  => 'fa fa-user-md fa-fw',
                ),
                array(
                    'name'  => 'Workout Master',
                    'icon'  => 'flaticon-sport188',
                    'sub'   => array(
                        array(
                            'name'  => 'Workout Routines',
                            'url'   => '/template'
                        ),
                        array(
                            'name'  => 'Workouts',
                            'url'   => '/workout'
                        ),
                        array(
                            'name'  => 'Videos',
                            'url'   => '/video'
                        ),
                        array(
                            'name'  => 'Exercise Images',
                            'url'   => '/exercise_images'
                        )
                    )
                ),
                array(
                    'name'  => 'MD Recommended',
                    'icon'  => 'fa fa-cutlery',
                    'sub'   => array(
                        array(
                            'name'  => 'Foods',
                            'url'   => '/recommended_foods'
                        ),
                        
//                        array(
//                            'name'  => 'Custom Foods',
//                            'url'   => '/admin_custom_foods'
//                        ),
                        array(
                            'name'  => 'Complete Plan Template',
                            'url'   => '/complete_meal_plans'
                        ),
                        array(
                            'name'  => 'Day Plan Template',
                            'url'   => '/day_meal_plans'
                        ),
                        array(
                            'name'  => 'Meal Plan Template',
                            'url'   => '/md_meals'
                        ),
                    )
                )
                ,
                array(
                    'name'  => 'Events',
                    'icon'  => 'fa fa-calendar',
                    'url'   => '/class_schedules'
                    
                ),
                array(
                    'name'  => 'Challenges',
                    'icon'  => 'hi hi-ok-circle',
                    'url'   => '/challenges'
                ),
                array(
                    'name' => 'Deals',
                    'icon'  => 'fa fa-thumbs-up',
                    'url'   => '/deal'
                ),
                array(
                    'name' => 'Social Media',
                    'icon'  => 'fa si si-rss',
                    'url'   => '/social_media'
                ),

                 array(
                    'name'  => 'Organizations',
                    'url'   => '/organization',
                    'icon'  => 'fa fa-user fa-fw',
                ),

                 array(
                    'name'  => 'Referrals',
                    'url'   => '/refer',
                    'icon'  => 'gi gi-send fa-fw',
                ),
                array(
                    'name'  => 'Foods',
                    'url'   => '/manage_food',
                    'icon'  => 'fa fa-cutlery',
                ),

            ]);
        }
        else if(\Auth::check() && \Auth::user()->user_type == "patient"){
            $view->with('template', [
            'name'=> 'Genemedics',
            'version'           => '1.0',
            'author'            => 'genemedics',
            'robots'            => 'noindex, nofollow',
            'title'             => 'Genemedics - Health and Wellness',
            'description'       => 'Genemedics - Health and Wellness.',
            // true                     enable page preloader
            // false                    disable page preloader
            'page_preloader'    => true,
            // true                     enable main menu auto scrolling when opening a submenu
            // false                    disable main menu auto scrolling when opening a submenu
            'menu_scroll'       => true,
            // 'navbar-default'         for a light header
            // 'navbar-inverse'         for a dark header
            'header_navbar'     => 'navbar-default',
            // ''                       empty for a static layout
            // 'navbar-fixed-top'       for a top fixed header / fixed sidebars
            // 'navbar-fixed-bottom'    for a bottom fixed header / fixed sidebars
            'header'            => '',
            // ''                                               for a full main and alternative sidebar hidden by default (> 991px)
            // 'sidebar-visible-lg'                             for a full main sidebar visible by default (> 991px)
            // 'sidebar-partial'                                for a partial main sidebar which opens on mouse hover, hidden by default (> 991px)
            // 'sidebar-partial sidebar-visible-lg'             for a partial main sidebar which opens on mouse hover, visible by default (> 991px)
            // 'sidebar-mini sidebar-visible-lg-mini'           for a mini main sidebar with a flyout menu, enabled by default (> 991px + Best with static layout)
            // 'sidebar-mini sidebar-visible-lg'                for a mini main sidebar with a flyout menu, disabled by default (> 991px + Best with static layout)
            // 'sidebar-alt-visible-lg'                         for a full alternative sidebar visible by default (> 991px)
            // 'sidebar-alt-partial'                            for a partial alternative sidebar which opens on mouse hover, hidden by default (> 991px)
            // 'sidebar-alt-partial sidebar-alt-visible-lg'     for a partial alternative sidebar which opens on mouse hover, visible by default (> 991px)
            // 'sidebar-partial sidebar-alt-partial'            for both sidebars partial which open on mouse hover, hidden by default (> 991px)
            // 'sidebar-no-animations'                          add this as extra for disabling sidebar animations on large screens (> 991px) - Better performance with heavy pages!
            'sidebar'           => 'sidebar-partial sidebar-visible-lg sidebar-no-animations',
            // ''                       empty for a static footer
            // 'footer-fixed'           for a fixed footer
            'footer'            => '',
            // ''                       empty for default style
            // 'style-alt'              for an alternative main style (affects main page background as well as blocks style)
            'main_style'        => '',
            // ''                           Disable cookies (best for setting an active color theme from the next variable)
            // 'enable-cookies'             Enables cookies for remembering active color theme when changed from the sidebar links (the next color theme variable will be ignored)
            'cookies'           => '',
            // 'night', 'amethyst', 'modern', 'autumn', 'flatie', 'spring', 'fancy', 'fire', 'coral', 'lake',
            // 'forest', 'waterlily', 'emerald', 'blackberry' or '' leave empty for the Default Blue theme
            'theme'             => '',
            // ''                       for default content in header
            // 'horizontal-menu'        for a horizontal menu in header
            // This option is just used for feature demostration and you can remove it if you like. You can keep or alter header's content in page_head.php
            'header_content'    => 'horizontal-menu',
            'active_page'       => '/'.\Request::segment(1)
            ]);
    	    if(\Auth::check()){
               $Profile_url = '/'.\Auth::user()->user_type.'/'.\Auth::user()->id.'/demographics';
            }
            else{
                $Profile_url = '/demographics';
            }
            $view->with('primary_nav', [
                array(
                    'name'  => 'Dashboard',
                    'opt'   => '<a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a>' .
                               '<a href="javascript:void(0)" data-toggle="tooltip" title="Create the most amazing pages with the widget kit!"><i class="gi gi-lightbulb"></i></a>',
                    'url'   => '/dashboard',
                    'icon'  => 'gi gi-stopwatch fa-fw',
                ),
                array(
                    'name'  => 'Food',
                    'opt'   => '<a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a>' .
                               '<a href="javascript:void(0)" data-toggle="tooltip" title="Create the most amazing pages with the widget kit!"><i class="gi gi-lightbulb"></i></a>',
                    'url'   => '/food',
                    'icon'  => 'fa fa-cutlery fa-fw',
                ),
                array(
                    'name'  => 'Exercise',
                    'opt'   => '<a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a>' .
                               '<a href="javascript:void(0)" data-toggle="tooltip" title="Create the most amazing pages with the widget kit!"><i class="gi gi-lightbulb"></i></a>',
                    'url'   => '/exercise',
                    'icon'  => 'flaticon-stick5 fa-fw',
                ),
                array(
                    'name'  => 'Weight',
                    'opt'   => '<a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a>' .
                               '<a href="javascript:void(0)" data-toggle="tooltip" title="Create the most amazing pages with the widget kit!"><i class="gi gi-lightbulb"></i></a>',
                    'url'   => '/weight',
                    'icon'  => 'fa wellness-icon-weight fa-fwx',
                ),
                 array(
                    'name'  => 'BP & Pulse',
                    'opt'   => '<a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a>' .
                               '<a href="javascript:void(0)" data-toggle="tooltip" title="Create the most amazing pages with the widget kit!"><i class="gi gi-lightbulb"></i></a>',
                    'url'   => '/blood_pressure',
                    'icon'  => 'gi gi-notes fa-fw',
                ),
                  array(
                    'name'  => 'Body Measurements',
                    'opt'   => '<a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a>' .
                               '<a href="javascript:void(0)" data-toggle="tooltip" title="Create the most amazing pages with the widget kit!"><i class="gi gi-lightbulb"></i></a>',
                    'url'   => '/body_measurements',
                    'icon'  => 'gi gi-notes_2 fa-fw',
                ),
                  array(
                    'name'  => 'Sleep',
                    'opt'   => '<a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a>' .
                               '<a href="javascript:void(0)" data-toggle="tooltip" title="Create the most amazing pages with the widget kit!"><i class="gi gi-lightbulb"></i></a>',
                    'url'   => '/sleep',
                    'icon'  => 'gi gi-notes_2 fa-fw',
                ),
                array(
                    'name'  => 'Goals',
                    'opt'   => '<a href="javascript:void(0)" data-toggle="tooltip" ><i class="gi gi-piano"></i></a>' .
                               '<a href="javascript:void(0)" data-toggle="tooltip"><i class="gi gi-piano"></i></a>',
                    'url'   => '/goals',
                    'icon'  => 'fa fa-bullseye',
                 ),
                array(
                    'name'  => 'Reports',
                    'opt'   => '<a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a>' .
                               '<a href="javascript:void(0)" data-toggle="tooltip" title="Create the most amazing pages with the widget kit!"><i class="gi gi-lightbulb"></i></a>',
                    'url'   => '/diary',
                    'icon'  => 'gi gi-notes_2 fa-fw',
                )
                // array(
                //     'name'  => 'New Dashboard',
                //     'opt'   => '<a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a>' .
                //                '<a href="javascript:void(0)" data-toggle="tooltip" title="Create the most amazing pages with the widget kit!"><i class="gi gi-lightbulb"></i></a>',
                //     'url'   => '/new_dashboard',
                //     'icon'  => 'gi gi-stopwatch fa-fw',
                // )
                  
            ]);
        }
        else if(\Auth::check() && \Auth::user()->user_type == "doctor"){

             $view->with('template', [
            'name'=> 'Genemedics',
            'version'           => '1.0',
            'author'            => 'genemedics',
            'robots'            => 'noindex, nofollow',
            'title'             => 'Genemedics - Health and Wellness',
            'description'       => 'Genemedics - Health and Wellness.',
            // true                     enable page preloader
            // false                    disable page preloader
            'page_preloader'    => true,
            // true                     enable main menu auto scrolling when opening a submenu
            // false                    disable main menu auto scrolling when opening a submenu
            'menu_scroll'       => true,
            // 'navbar-default'         for a light header
            // 'navbar-inverse'         for a dark header
            'header_navbar'     => 'navbar-default',
            // ''                       empty for a static layout
            // 'navbar-fixed-top'       for a top fixed header / fixed sidebars
            // 'navbar-fixed-bottom'    for a bottom fixed header / fixed sidebars
            'header'            => '',
            // ''                                               for a full main and alternative sidebar hidden by default (> 991px)
            // 'sidebar-visible-lg'                             for a full main sidebar visible by default (> 991px)
            // 'sidebar-partial'                                for a partial main sidebar which opens on mouse hover, hidden by default (> 991px)
            // 'sidebar-partial sidebar-visible-lg'             for a partial main sidebar which opens on mouse hover, visible by default (> 991px)
            // 'sidebar-mini sidebar-visible-lg-mini'           for a mini main sidebar with a flyout menu, enabled by default (> 991px + Best with static layout)
            // 'sidebar-mini sidebar-visible-lg'                for a mini main sidebar with a flyout menu, disabled by default (> 991px + Best with static layout)
            // 'sidebar-alt-visible-lg'                         for a full alternative sidebar visible by default (> 991px)
            // 'sidebar-alt-partial'                            for a partial alternative sidebar which opens on mouse hover, hidden by default (> 991px)
            // 'sidebar-alt-partial sidebar-alt-visible-lg'     for a partial alternative sidebar which opens on mouse hover, visible by default (> 991px)
            // 'sidebar-partial sidebar-alt-partial'            for both sidebars partial which open on mouse hover, hidden by default (> 991px)
            // 'sidebar-no-animations'                          add this as extra for disabling sidebar animations on large screens (> 991px) - Better performance with heavy pages!
            'sidebar'           => 'sidebar-partial sidebar-visible-lg sidebar-no-animations',
            // ''                       empty for a static footer
            // 'footer-fixed'           for a fixed footer
            'footer'            => '',
            // ''                       empty for default style
            // 'style-alt'              for an alternative main style (affects main page background as well as blocks style)
            'main_style'        => '',
            // ''                           Disable cookies (best for setting an active color theme from the next variable)
            // 'enable-cookies'             Enables cookies for remembering active color theme when changed from the sidebar links (the next color theme variable will be ignored)
            'cookies'           => '',
            // 'night', 'amethyst', 'modern', 'autumn', 'flatie', 'spring', 'fancy', 'fire', 'coral', 'lake',
            // 'forest', 'waterlily', 'emerald', 'blackberry' or '' leave empty for the Default Blue theme
            'theme'             => '',
            // ''                       for default content in header
            // 'horizontal-menu'        for a horizontal menu in header
            // This option is just used for feature demostration and you can remove it if you like. You can keep or alter header's content in page_head.php
            'header_content'    => 'horizontal-menu',
            'active_page'       => '/'.\Request::segment(1)
                                    ]);
             $view->with('primary_nav', [
                array(
                    'name'  => 'Patients',
                    'opt'   => '<a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a>' .
                               '<a href="javascript:void(0)" data-toggle="tooltip" title="Create the most amazing pages with the widget kit!"><i class="gi gi-lightbulb"></i></a>',
                    'url'   => '/patient',
                    'icon'  => 'fa fa-user fa-fw',
                ),
                array(
                    'name'  => 'Doctors',
                    'opt'   => '<a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a>' .
                               '<a href="javascript:void(0)" data-toggle="tooltip" title="Create the most amazing pages with the widget kit!"><i class="gi gi-lightbulb"></i></a>',
                    'url'   => '/doctor',
                    'icon'  => 'fa fa-user-md fa-fw',
                ),
                /* array(
                    'name'  => 'Workout Master',
                    'icon'  => 'flaticon-sport188',
                    'sub'   => array(
                        array(
                            'name'  => 'Workout Plans',
                            'url'   => '/template'
                        ),
                        array(
                            'name'  => 'Workout Routines',
                            'url'   => '/workout'
                        ),
                        array(
                            'name'  => 'Videos',
                            'url'   => '/video'
                        ),
                        array(
                            'name'  => 'Exercise Images',
                            'url'   => '/exercise_images'
                        )
                    )
                ),
               array(
                    'name'  => 'MD Recommended',
                    'icon'  => 'fa fa-cutlery',
                    'sub'   => array(
                        array(
                            'name'  => 'Foods',
                            'url'   => '/recommended_foods'
                        ),
                        array(
                            'name'  => 'Meals',
                            'url'   => '/recommended_meals'
                        )
                    )
                )
                ,*/
                array(
                    'name'  => 'Events',
                    'icon'  => 'fa fa-calendar',
                    'url'   => '/class_schedules'
                    
                ),
                array(
                    'name'  => 'Challenges',
                    'icon'  => 'hi hi-ok-circle',
                    'url'   => '/challenges'
                ),
                array(
                    'name' => 'Deals',
                    'icon'  => 'fa fa-thumbs-up',
                    'url'   => '/deal'
                ),
                array(
                    'name' => 'Social Media',
                    'icon'  => 'fa si si-rss',
                    'url'   => '/social_media'
                ),

                

            ]);
        }
    }
}