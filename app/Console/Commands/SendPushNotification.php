<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SendPushNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:push_notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Push Notification to Users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        try{

            $this->info('User Push Notification send started...!');
            $this->info('Current time -- '.date("Y-m-d h:i:s a"));

            $user_timezones =  \DB::table('user_timezone')->get();
            $day_time_arr =[];
            foreach ($user_timezones as $key => $value) {
                $timezone = $value->timezone_name;
                $now =\Carbon\Carbon::now($timezone)->subMinutes(5);
                $temp=[];
                $h = $now->hour;
                $m = $now->minute;
                $time =$h+($m/60);
                $temp['time'] = number_format($time,2);
               // $dayNumber =$now->dayOfWeek;
                $dayNumber= date('N', strtotime($now));
                $dayName= date('D', strtotime($now));
                $meal_time = number_format($time,2);
                if($now->dayOfWeek == 0){
                    $dayNumber =7;
                }
                $temp['dayNumber']=$dayNumber;
                $temp['dayName']=$dayName;
                $temp['timezone']=$timezone;
                $temp['time_format'] = $now->toDayDateTimeString();
                $day_time_arr[] =$temp;

            
                 // $nutrition_exercise_day_times = \App\Models\Nutrition_exercise_day_time::with('nutrition_exercise_plan')
                 //                            ->where(function($q) use($day_time_arr){
                 //                                    foreach ($day_time_arr as $key => $value) {
                 //                                        $q->orWhere(function($q1) use($value){
                 //                                            $q1->where('dayNumber', $value['dayNumber']);
                 //                                            $q1->where('meal_time',$value['time']);
                 //                                        });

                 //                                    }

                 //                            })->get();

                 $nutrition_exercise_day_times = \App\Models\User_meal_plan_day_time::with('user_nutrition_exercise_plan')
                                                                                          ->where('dayNumber', $dayNumber)
                                                                                          ->where('meal_time',$meal_time)
                                                                                          ->where('notification',1)
                                                                                          ->get();
                     
                                            
                    foreach ($nutrition_exercise_day_times as $key => $nutritionconfig) {
                        $user_meal_plan_id = $nutritionconfig->user_nutrition_exercise_plan->user_meal_plan_id;
                            $user_meal_plan =  \App\Models\User_meal_plan::with('user_with_token')
                                                                        ->where('id',$user_meal_plan_id)
                                                                        ->get();

                           foreach ($user_meal_plan as $key => $mp) {
                           
                             // $user_device_data = $mp->user_with_token->device_token;
                                    $user_timezone = $mp->user_with_token->timezone_id;
                                    if($user_timezone == $timezone){
                                       $send_notification = new \App\Common\Send_pushnotification();
                                       $send_notification->send($mp->user_with_token,$nutritionconfig); 
                                    }
                                    
                           }
                           
                    }
            }
            $this->info('User Push Notification end..!');

           }catch(\Exception $e){

                dd($e);
           }
    }
}
