<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Aws\DynamoDb\Exception\DynamoDbException;
use Aws\DynamoDb\Marshaler;

class ImportDynamodb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:dynamodb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

    
       try{

            $file_names_arr = [                
                'fit4_6.csv',
                'fit4_7.csv',
                'fit4_8.csv',
                'fit4_9.csv',
                'fit4_10.csv',
                'fit4_11.csv',
                'fit4_12.csv',                
                'fit4_13.csv',
                'fit4_14.csv',
                'fit4_15.csv',
                'fit4_16.csv',
                'fit4_17.csv',
                'fit4_18.csv',
                'fit4_19.csv',
                'fit4_20.csv',
                'fit4_21.csv',
                'fit4_22.csv',
                'fit4_23.csv',
                'fit4_24.csv',
                'fit4_25.csv',
                'fit4_26.csv',
                'fit4_27.csv',
                'fit4_28.csv',
                'fit4_29.csv',
                'fit4_37.csv',
                'fit4_38.csv',
                'fit4_39.csv',
                'fit4_40.csv'
                    
            ];
            $dynamodb = \AWS::createDynamoDb();
            $marshaler = new Marshaler();
            foreach ($file_names_arr as $key => $file_name) {
                $this->info('Nutrition import started'.$file_name.'----------');
                $nt_file_path = public_path().'/nutrition/'.$file_name; 
                $nt_reader = \CsvReader::open($nt_file_path);
                $i=1;
                $brand_name ="";
                $nutrition_name = "";
                $calories="";
                $sodium="";
                $total_fat="";
                $potassium="";
                $saturated_fat="";
                $total_carb="";
                $polyunsaturated="";
                $monounsaturated="";
                $dietary_fiber="";
                $sugars="";
                $trans_fat="";
                $protein="";
                $cholesterol="";
                $vitamin_a="";
                $vitamin_c="";
                $calcium_dv="";
                $iron_dv="";
                $serving_quantity =  1;
                $serving_size_unit =  "serving(s)";

                while (($line = $nt_reader->readLine()) !== false) {
                      
                        $row_id = isset($line[0])?$line[0]:'';
                        $brand_with_name = isset($line[1])?$line[1]:'';
                        $nutrition_data = isset($line[2])?$line[2]:'';
                        $serving_data = isset($line[3])?$line[3]:'';
                        if($row_id != "" && $brand_with_name != "") {
                            $nutrition_data_arr = []; 
                            $checkValidString = $this->checkValidString($brand_with_name);
                            $nutrition_data = str_replace('"','',$nutrition_data);
                            if($checkValidString){

                                // Start Column B code
                                if($brand_with_name !="") {
                                    $brand_with_name_explode = explode("-", $brand_with_name);
                                    $countbrand_with_name =count($brand_with_name_explode);
                                    if($countbrand_with_name == 1){
                                        $brand_name = "No brand";
                                        $nutrition_name = $brand_with_name_explode[0];
                                    }
                                    if($countbrand_with_name == 2){
                                        if($brand_with_name_explode[1] != ""){
                                             $brand_name = $brand_with_name_explode[0];
                                             $nutrition_name = $brand_with_name_explode[1];
                                        }else{
                                             $brand_name = "No brand";
                                             $nutrition_name = $brand_with_name_explode[0];
                                        }
                                       
                                    }
                                    if($countbrand_with_name > 2){

                                        $k = $brand_with_name_explode;
                                        $sliced = array_slice($k, 0, -1); // array ( "Hello", "World" )

                                        $brand_name = implode("-", $sliced);  // "Hello World";

                                        $nutrition_name = array_last($brand_with_name_explode,function($key,$value){
                                        return $value;
                                        });
                                    }
                                } // End Column B code
                                // Start Column C code
                                if($nutrition_data != "") {

                                    $nutrition_data_explode = explode("<<>>", $nutrition_data);
                                    
                                    foreach ($nutrition_data_explode as $key => $Ntvalue) {
                                        
                                        $nt_value_explode = explode(":::", $Ntvalue);
                                        $nt_key_value = trim($nt_value_explode[0]);
                                       
                                        if($nt_key_value == "Calories"){
                                            $calories = isset($nt_value_explode[1])?$nt_value_explode[1]:'';
                                        }

                                        if($nt_key_value == "Sodium"){
                                            $sodium = isset($nt_value_explode[1])?$nt_value_explode[1]:'';
                                        }

                                        if($nt_key_value == "Total Fat"){
                                          
                                            $total_fat = isset($nt_value_explode[1])?$nt_value_explode[1]:'';
                                        }

                                        if($nt_key_value == "Potassium"){

                                            $potassium = isset($nt_value_explode[1])?$nt_value_explode[1]:'';
                                        }

                                        if($nt_key_value == "Saturated"){

                                            $saturated_fat = isset($nt_value_explode[1])?$nt_value_explode[1]:'';
                                        }

                                        if($nt_key_value == "Total Carbs"){

                                            $total_carb = isset($nt_value_explode[1])?$nt_value_explode[1]:'';
                                        }

                                        if($nt_key_value == "Polyunsaturated"){

                                            $polyunsaturated = isset($nt_value_explode[1])?$nt_value_explode[1]:'';
                                        }

                                        if($nt_key_value == "Dietary Fiber"){

                                            $dietary_fiber = isset($nt_value_explode[1])?$nt_value_explode[1]:'';
                                        }

                                        if($nt_key_value == "Monounsaturated"){
                                            $monounsaturated =isset($nt_value_explode[1])?$nt_value_explode[1]:'';
                                        }

                                        if($nt_key_value == "Sugars"){
                                            $sugars =isset($nt_value_explode[1])?$nt_value_explode[1]:'';
                                        }

                                        if($nt_key_value == "Trans"){
                                            $trans_fat =isset($nt_value_explode[1])?$nt_value_explode[1]:'';
                                        }

                                        if($nt_key_value == "Protein"){

                                           $protein =isset($nt_value_explode[1])?$nt_value_explode[1]:'';
                                        }

                                        if($nt_key_value == "Cholesterol"){

                                            $cholesterol =isset($nt_value_explode[1])?$nt_value_explode[1]:'';
                                        }

                                        if($nt_key_value == "Vitamin A"){
                                            $vitamin_a =isset($nt_value_explode[1])?$nt_value_explode[1]:'';
                                        }

                                        if($nt_key_value == "Calcium"){
                                            $calcium_dv =isset($nt_value_explode[1])?$nt_value_explode[1]:'';
                                        }

                                        if($nt_key_value == "Vitamin C"){
                                            $vitamin_c =isset($nt_value_explode[1])?$nt_value_explode[1]:'';
                                          
                                        }

                                        if($nt_key_value == "Iron") {

                                            $iron_dv =isset($nt_value_explode[1])?$nt_value_explode[1]:'';
                                         
                                        }
                                    }

                                }// End Column C code
                                // Start Column D code
                                if($serving_data != "") {

                                    $serving_data_explode = explode("<<>>", $serving_data);
                                    $serving_data_count = count($serving_data_explode);
                                    $serving_size_data = $serving_data_explode[0];
                                    $serving_size_data_explode = explode(" ", $serving_size_data);

                                    $serving_quantity =  $this->calculateServingQuantity($serving_size_data_explode[0]);
                                    $unit = $serving_size_data_explode;
                                    $sliced_unit = array_slice($unit, 1); // array ( "Hello", "World" )

                                    $serving_size_unit = implode(" ", $sliced_unit);  // "Hello World";

                                    //$serving_size_unit =  $serving_size_data_explode[1];
                                    $serving_json_data = [];

                                    foreach ($serving_data_explode as $key => $ServingValue) {
                                        $serving_json_data[$key]['label'] =$ServingValue;
                                        $serving_json_data[$key]['unit'] =$serving_size_unit;

                                    }
                                  //  $servings_json = json_encode($serving_json_data);
                             
                                }
                                if(!empty(trim($nutrition_name)) && trim($nutrition_name) !=""){

                                    $nutrition_data_arr['calories'] = $calories;
                                    $nutrition_data_arr['sodium'] = $sodium;
                                    $nutrition_data_arr['total_fat'] = $total_fat;
                                    $nutrition_data_arr['potassium'] = $potassium;
                                    $nutrition_data_arr['saturated_fat'] = $saturated_fat; 
                                    $nutrition_data_arr['total_carb'] = $total_carb;
                                    $nutrition_data_arr['polyunsaturated'] = $polyunsaturated;
                                    $nutrition_data_arr['dietary_fiber'] = $dietary_fiber;
                                    $nutrition_data_arr['monounsaturated'] = $monounsaturated;
                                    $nutrition_data_arr['sugars'] = $sugars;
                                    $nutrition_data_arr['protein'] = $protein;
                                    $nutrition_data_arr['trans_fat'] = $trans_fat;
                                    $nutrition_data_arr['cholesterol'] = $cholesterol;
                                    $nutrition_data_arr['vitamin_a'] = $vitamin_a;
                                    $nutrition_data_arr['calcium_dv'] = $calcium_dv;
                                    $nutrition_data_arr['vitamin_c'] = $vitamin_c;
                                    $nutrition_data_arr['iron_dv'] = $iron_dv;
                                    $nutrition_data_arr['serving_quantity'] = $serving_quantity;
                                    $nutrition_data_arr['serving_size_unit'] = $serving_size_unit;
                                    $nutrition_row = ['row_id'=>$row_id,
                                                      'name'=>trim($nutrition_name),
                                                      'brand_name'=>trim($brand_name),
                                                      'brand_with_name'=>$brand_with_name,
                                                      'nutrition_data'=>json_encode($nutrition_data_arr),
                                                      'serving_data'=>json_encode($serving_json_data)   
                                                     ];
                               
                                    $nutrition_row_json = json_encode($nutrition_row);
                                    $item = $marshaler->marshalJson($nutrition_row_json);
                                  
                                    $params = [
                                        'TableName' => 'Nutritions',
                                        'Item' => $item
                                    ]; 

                                    try {
                                          
                                             $result = $dynamodb->putItem($params);
                                                             

                                    } catch (DynamoDbException $e) {

                                          // $this->info($e->getMessage());

                                    }
                                    $this->info($row_id.' Nutrition save successfully!'.$file_name);
                                }
                            }else{
                                // store Invalid nutrition

                                 $nutrition_row = ['row_id'=>$row_id,
                                                   'brand_with_name'=>$brand_with_name,
                                                   'file_name'=>$file_name,
                                                   'nutrition_data'=>$nutrition_data,
                                                   'serving_data'=>$serving_data   
                                                 ];
                               
                                $nutrition_row_json = json_encode($nutrition_row);
                                $item = $marshaler->marshalJson($nutrition_row_json);
                                $params = [
                                    'TableName' => 'Invalid_nutritions',
                                    'Item' => $item
                                ]; 
                                try {
                                    $result = $dynamodb->putItem($params);
                                } catch (DynamoDbException $e) {
                                  // $this->info($e->getMessage());
                                }
                            }
                        }
                        $this->info($i.' Line number of file! '.$file_name);
                       
                    $i++;

                }
                $nt_reader->close();
                $this->info('Nutrition imported end...!'.$file_name);   
            }   
       }catch(\Exception $e){

        // $this->info($e);
       }
    }

    public function checkValidString($string){
        if (preg_match('/[ÃÂ©]/', $string))
            {
                return false;
            }else{
                return true;  
            }
    }

    public function calculateServingQuantity($serving_quantity_data){

        if($serving_quantity_data !="") {

            $explode_quantity_data = explode("/", $serving_quantity_data);
            if(count($explode_quantity_data) >1){
                 $quanty = (float)$explode_quantity_data[0]/(float) $explode_quantity_data[1];
            }else{
                  $quanty = (float)$explode_quantity_data[0];
            }
            return round($quanty,2);
        }else{
            return $serving_quantity_data;
        }
    }
}
