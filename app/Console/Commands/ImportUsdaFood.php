<?php

namespace App\Console\Commands;

use App\Models\FoodCategory;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Wilgucki\PhpCsv\Reader;

class ImportUsdaFood extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:usda-food';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            $file_name = "food_category.csv";
            $this->info('Nutrition import started' . $file_name . '----------');
            $nt_file_path =   "E:/xampp/htdocs/nutrition/public/usda_food_data/" . $file_name;
            $nt_reader = \Wilgucki\Csv\Facades\Reader::open($nt_file_path);
            $i = 1;
            $foodData =[];
            $dateTimeArray = ['created_at'=> Carbon::now()->format('Y-m-d H:i:s'),'updated_at' =>Carbon::now()->format('Y-m-d H:i:s')];
            while (($line = $nt_reader->readLine()) !== false) {

                $foodData[] =$line;
                $i++;

            }
            $mainArray = $this->convertCogsAssociateArray($foodData);
            $chunkArray = array_chunk($mainArray, 10);
            foreach ($chunkArray as $index => $fData) {
                $nc = FoodCategory::insert($fData);
                $this->info('Food data of insert index...!' . $index);
            }


            $nt_reader->close();
            $this->info('Nutrition imported end...!' . $file_name);

        } catch (\Exception $e) {

            $this->info($e);
        }
    }

    public function convertCogsAssociateArray($reportData)
    {
        $associateArray = $fields = []; $i = 0;
        foreach ($reportData as $row) {
            if (empty($fields)) {
                $fields = $row;
                continue;
            }
            foreach ($row as $k => $value) {
                if(isset($fields[$k])){
                    $associateArray[$i][$fields[$k]] = ($value && ($value != 'N/A')) ? $value : null;
                }
            }
            $i++;
        }

        return $associateArray;
    }
}
