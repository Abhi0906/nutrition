<?php
//http://stackoverflow.com/questions/14330713/converting-float-decimal-to-fraction
namespace App\Console\Commands;

use Illuminate\Console\Command;

class CorrectServingSize extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'correct:serving_data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $rows = [
                '6000000'=>'200000',
                '8200000'=>'200000',
                '8400000'=>'200000',
                '8600000'=>'200000',
                '8800000'=>'200000'   
                ];
        foreach ($rows as $Pkey => $row) {
            $nutritions = \App\Models\Nutrition::skip($Pkey)->take($row)->get();
            foreach ($nutritions as $key => $value) {
                $id = $value->id;
                $name = $value->name;
                $nutrition_data = $value->nutrition_data;
                $serving_data = $value->serving_data;
                $serving_data_arr = json_decode( $serving_data,1);
                $nutrition_data_arr = json_decode($nutrition_data,1);
                $find_string = false;
                $this->info($id.' Nutrition started !'.$name);
                foreach ($serving_data_arr as $key => $sevings) {
                    $serving_label = $sevings['label'];
                    $serving_unit =$sevings['unit'];
                    $subject = $serving_label;
                    //First Pattern example 1 1/4 convert 1.25
                    $pattern1 = '/\d\s\d\D\d+/';
                    $update_serving_label = $serving_label;
                    $quantity =  $nutrition_data_arr['serving_quantity'];
                    preg_match($pattern1, $subject, $matches1);
                    if(count($matches1) > 0){
                       
                        $matchString =$matches1[0];
                        $quantity =  $this->calculateServingQuantity($matchString);
                       
                       // $update_serving_label =str_replace($matchString,$quantity,$serving_label);
                       // $serving_data_arr[$key]['label']=$update_serving_label;
                        $find_string = true;

                    }

                    //Second Pattern example 1 /4 convert 0.25
                     $pattern2 = '/(\d\s\D\d+.\d+)|(\d\s\D\d+)/';
                     preg_match($pattern2, $subject, $matches2);
                     if(count($matches2) > 0){
                           // dd($matches2);
                        $matchString2 =$matches2[0];
                        $quantity =  $this->calculateServingQuantity2($matchString2);
                        $new_label = preg_replace('/\s+/', '', $matchString2);
                        $update_serving_label =str_replace($matchString2,$new_label,$serving_label);
                       
                        $serving_data_arr[$key]['label']=$update_serving_label;
                        $find_string = true;

                    }


                    $update_serving_unit = $this->removeNumberFromUnit($serving_unit);
                    $serving_data_arr[$key]['unit']=$update_serving_unit;
                   

                    if($key == 0){
                       $nutrition_data_arr['serving_quantity']=$quantity;
                       $nutrition_data_arr['serving_size_unit']=$update_serving_unit;
                    }
                }
                $jsonNutrition_data = json_encode($nutrition_data_arr);
                $jsonServing_data =json_encode($serving_data_arr);
               
                if($find_string){
                    //dd($jsonServing_data);
                   \DB::table('nutritions')->where('id', $id)
                     ->update(['nutrition_data' => $jsonNutrition_data,'serving_data'=>$jsonServing_data]);
                     $this->info($id.' Nutrition Updated successfully!'.$name); 
                }
            }
          $this->info($row.' has finished !');    
        }
    }

    public function old_handle()
     {
        $rows = ['0'=>'100'      
                ];
        foreach ($rows as $Pkey => $row) {
            $nutritions = \App\Models\Nutrition::skip($Pkey)->take($row)->get();
            foreach ($nutritions as $key => $value) {
                $id = $value->id;
                $name = $value->name;
                $nutrition_data = $value->nutrition_data;
                $serving_data = $value->serving_data;
                $serving_data_arr = json_decode( $serving_data,1);
                $nutrition_data_arr = json_decode($nutrition_data,1);
                $find_string = false;
                $this->info($id.' Nutrition started !'.$name);
                foreach ($serving_data_arr as $key => $sevings) {
                    $serving_label = $sevings['label'];
                    $serving_unit =$sevings['unit'];
                    $subject = $serving_label;
                    //First Pattern example 0.5 convert 1/2 as well as 1.25 to 1 1/4
                    $pattern1 = '/\d+\.\d+/';
                    $update_serving_label = $serving_label;
                    $quantity =  $nutrition_data_arr['serving_quantity'];
                    preg_match($pattern1, $subject, $matches1);
                    if(count($matches1) > 0){
                       
                        $matchString =$matches1[0];
                     
                        $label =  $this->createLabel($matchString);
                      
                       
                        $update_serving_label =str_replace($matchString,$label,$serving_label);
                        $serving_data_arr[$key]['label']=$update_serving_label;
                          //dd($update_serving_label);
                        $find_string = true;

                    }
                    
                }
              
                $jsonServing_data =json_encode($serving_data_arr);

               
                if($find_string){
                   //  dd($jsonServing_data);
                   \DB::table('nutritions')->where('id', $id)
                     ->update(['serving_data'=>$jsonServing_data]);
                     $this->info($id.' Nutrition Updated successfully!'.$name); 
                }
            }
          $this->info($row.' has finished !');    
        }
    }


    public function calculateServingQuantity($Nstring){
            $explode_quantity_data = explode(" ", $Nstring); 
            if(count($explode_quantity_data) > 0){

             $firstString =   $explode_quantity_data[0];
             $secondString =   $explode_quantity_data[1];
             $secondStringArr = explode("/", $secondString);
             if(count($secondStringArr) > 0){
                 if(isset($secondStringArr[1])){
                     $quanty = (float)$secondStringArr[0]/(float) $secondStringArr[1];
                     $sum_quanty = (int) $firstString + $quanty;
                     return round($sum_quanty,2);
                 }else{
                    return $firstString;
                 }
                
             }
            
            }else{
                return $Nstring;
            }

           
    }

    public function calculateServingQuantity2($Nstring){

          
            $explode_quantity_data = explode("/", $Nstring);
           
            if(count($explode_quantity_data) > 0 && isset($explode_quantity_data[1])){

             $firstString =   trim($explode_quantity_data[0]);
             $secondString =   trim($explode_quantity_data[1]);

             $quanty = (float)$firstString/(float) $secondString;
             return round($quanty,2);
            
            }else{
                return $Nstring;
            }


    }

    public function removeNumberFromUnit($serving_unit){

        $serving_unit_arr = explode(" ", $serving_unit);
        if(count($serving_unit_arr) >0){

            return end($serving_unit_arr);
        }else{
            return $serving_unit;
        } 

    }

   public function float2rat($n, $tolerance = 1.e-6) {
        $h1=1; $h2=0;
        $k1=0; $k2=1;
        $b = 1/$n;
        do {
            $b = 1/$b;
            $a = floor($b);
            $aux = $h1; $h1 = $a*$h1+$h2; $h2 = $aux;
            $aux = $k1; $k1 = $a*$k1+$k2; $k2 = $aux;
            $b = $b-$a;
        } while (abs($n-$h1/$k1) > $n*$tolerance);
       // echo  "$h1/$k1";
    return "$h1/$k1";
   }

   public function createLabel($Nstring){

   //$serving_unit_arr = explode(".", $Nstring);

    $whole = floor($Nstring);      // 1
    $fraction = $Nstring - $whole; // .25
       if($fraction > 0.0){

            $float_string = $this->float2rat($fraction);
            if($whole > 0){
              $new_lable = $whole. " ".$float_string;  
              }else{
                  $new_lable = $float_string;
              }
            

            return $new_lable; 

       }else{
        return $Nstring;
       }
    


   }



}
