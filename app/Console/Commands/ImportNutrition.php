<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ImportNutrition extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:nutrition';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

                
                $file_names_arr = [
                                   'fit4_37.csv',
                                   'fit4_38.csv',
                                   'fit4_39.csv',
                                   'fit4_40.csv', 
                                   'fit4_41.csv',
                                   'fit4_42.csv',
                                   'fit4_43.csv',
                                   'fit4_44.csv',
                                   //'fit4_25.csv',
                                   'fit4_46.csv',
                                   'fit4_47.csv',
                                   'fit4_48.csv',
                                   'fit4_49.csv',
                                   'fit4_50.csv',
                                   'fit4_51.csv',
                                   'fit4_52.csv',
                                   'fit4_53.csv',
                                   'fit4_54.csv',
                                   'fit4_55.csv',
                                   'fit4_56.csv',
                                   'fit4_57.csv',
                                   'fit4_58.csv',
                                   'fit4_59.csv',
                                   'fit4_60.csv'
                                   ];    
                foreach ($file_names_arr as $key => $file_name) {
                           
                    $this->info('Nutrition import started'.$file_name.'----------');
                    $nt_file_path = public_path().'/nutrition/'.$file_name; 
                    $nt_reader = \CsvReader::open($nt_file_path);
                    $i=0;
                    $brand_name ="";
                    $nutrition_name = "";
                    $calories="";
                    $sodium="";
                    $sodium_unit="";
                    $total_fat="";
                    $total_fat_unit="";
                    $potassium="";
                    $potassium_unit="";
                    $saturated_fat="";
                    $saturated_fat_unit="";
                    $total_carb="";
                    $total_carb_unit="";
                    $polyunsaturated="";
                    $polyunsaturated_unit="";
                    $monounsaturated="";
                    $monounsaturated_unit="";
                    $dietary_fiber="";
                    $dietary_fiber_unit="";
                    $sugars="";
                    $sugars_unit="";
                    $trans_fat="";
                    $trans_fat_unit="";
                    $protein="";
                    $protein_unit="";
                    $cholesterol="";
                    $cholesterol_unit="";
                    $vitamin_a="";
                    $vitamin_a_unit="";
                    $vitamin_c="";
                    $vitamin_c_unit="";
                    $calcium_dv="";
                    $calcium_dv_unit="";
                    $iron_dv="";
                    $iron_dv_unit="";
                    $serving_quantity =  1;
                    $serving_size_unit =  "serving(s)";
                    $servings_json="";
                    while (($line = $nt_reader->readLine()) !== false) {
                        
                        $row_id = isset($line[0])?$line[0]:'';
                        $brand_with_name = isset($line[1])?$line[1]:'';
                        $nutrition_data = isset($line[2])?$line[2]:'';
                        $serving_data = isset($line[3])?$line[3]:'';
                        $checkValidString = $this->checkValidString($brand_with_name);
                        if($row_id != "" && $brand_with_name != "") {

                           // $exists_nutrition_data = \App\Models\Nutrition::where('row_id',$row_id)->first();
                           // if(is_null($exists_nutrition_data)){

                                $nutrition_data = str_replace('"','',$nutrition_data);
                                // Start Column B code
                                if($brand_with_name !="") {
                                    $brand_with_name_explode = explode("-", $brand_with_name);
                                    $countbrand_with_name =count($brand_with_name_explode);
                                    if($countbrand_with_name == 1){
                                        $brand_name = "No brand";
                                        $nutrition_name = $brand_with_name_explode[0];
                                    }
                                    if($countbrand_with_name == 2){
                                        $brand_name = $brand_with_name_explode[0];
                                        $nutrition_name = $brand_with_name_explode[1];
                                    }
                                    if($countbrand_with_name > 2){

                                        $k = $brand_with_name_explode;
                                        $sliced = array_slice($k, 0, -1); // array ( "Hello", "World" )

                                        $brand_name = implode("-", $sliced);  // "Hello World";

                                        $nutrition_name = array_last($brand_with_name_explode,function($key,$value){
                                                 return $value;
                                        });
                                    }
                                } // End Column B code
                                // Start Column C code
                                if($nutrition_data != "") {

                                    $nutrition_data_explode = explode("<<>>", $nutrition_data);
                                    
                                    foreach ($nutrition_data_explode as $key => $Ntvalue) {

                                        $nt_value_explode = explode(":::", $Ntvalue);

                                        $nt_key_value = trim($nt_value_explode[0]);

                                        if($nt_value_explode[1] != ""){
                                          $nt_unit_with_name = explode(" ", $nt_value_explode[1]);   
                                        }
                                        

                                        if($nt_key_value == "Calories"){
                                             $calories = $nt_value_explode[1];
                                        }

                                        if($nt_key_value == "Sodium"){
                                            $sodium =isset($nt_unit_with_name[0])?$nt_unit_with_name[0]:'';
                                            $sodium_unit = isset($nt_unit_with_name[1])?$nt_unit_with_name[1]:'';
                                        }

                                        if($nt_key_value == "Total Fat"){

                                            $total_fat =isset($nt_unit_with_name[0])?$nt_unit_with_name[0]:'';
                                            $total_fat_unit = isset($nt_unit_with_name[1])?$nt_unit_with_name[1]:'';

                                        }

                                        if($nt_key_value == "Potassium"){

                                            $potassium =isset($nt_unit_with_name[0])?$nt_unit_with_name[0]:'';
                                            $potassium_unit = isset($nt_unit_with_name[1])?$nt_unit_with_name[1]:'';
                                        }

                                        if($nt_key_value == "Saturated"){
                                            $saturated_fat =isset($nt_unit_with_name[0])?$nt_unit_with_name[0]:'';
                                            $saturated_fat_unit = isset($nt_unit_with_name[1])?$nt_unit_with_name[1]:'';
                                        }

                                        if($nt_key_value == "Total Carbs"){

                                            $total_carb =isset($nt_unit_with_name[0])?$nt_unit_with_name[0]:'';
                                            $total_carb_unit = isset($nt_unit_with_name[1])?$nt_unit_with_name[1]:'';
                                        }

                                        if($nt_key_value == "Polyunsaturated"){

                                            $polyunsaturated =isset($nt_unit_with_name[0])?$nt_unit_with_name[0]:'';
                                            $polyunsaturated_unit = isset($nt_unit_with_name[1])?$nt_unit_with_name[1]:'';
                                        }

                                        if($nt_key_value == "Dietary Fiber"){

                                            $dietary_fiber =isset($nt_unit_with_name[0])?$nt_unit_with_name[0]:'';
                                            $dietary_fiber_unit = isset($nt_unit_with_name[1])?$nt_unit_with_name[1]:'';
                                        }

                                        if($nt_key_value == "Monounsaturated"){
                                            $monounsaturated =isset($nt_unit_with_name[0])?$nt_unit_with_name[0]:'';
                                            $monounsaturated_unit = isset($nt_unit_with_name[1])?$nt_unit_with_name[1]:'';
                                        }

                                        if($nt_key_value == "Sugars"){

                                            $sugars =isset($nt_unit_with_name[0])?$nt_unit_with_name[0]:'';
                                            $sugars_unit = isset($nt_unit_with_name[1])?$nt_unit_with_name[1]:'';
                                        }

                                        if($nt_key_value == "Trans"){

                                            $trans_fat =isset($nt_unit_with_name[0])?$nt_unit_with_name[0]:'';
                                            $trans_fat_unit = isset($nt_unit_with_name[1])?$nt_unit_with_name[1]:'';
                                        }

                                        if($nt_key_value == "Protein"){

                                            $protein =isset($nt_unit_with_name[0])?$nt_unit_with_name[0]:'';
                                            $protein_unit = isset($nt_unit_with_name[1])?$nt_unit_with_name[1]:'';
                                        }

                                        if($nt_key_value == "Cholesterol"){

                                            $cholesterol =isset($nt_unit_with_name[0])?$nt_unit_with_name[0]:'';
                                            $cholesterol_unit = isset($nt_unit_with_name[1])?$nt_unit_with_name[1]:'';
                                        }

                                        if($nt_key_value == "Vitamin A"){
                                            $vitamin_a =rtrim($nt_value_explode[1], "%");
                                            $vitamin_a_unit = substr($nt_value_explode[1], -1);
                                        }

                                        if($nt_key_value == "Calcium"){
                                            $calcium_dv =rtrim($nt_value_explode[1], "%");
                                            $calcium_dv_unit = substr($nt_value_explode[1], -1);
                                        }

                                        if($nt_key_value == "Vitamin C"){
                                            $vitamin_c =rtrim($nt_value_explode[1], "%");
                                            $vitamin_c_unit = substr($nt_value_explode[1], -1);
                                        }

                                        if($nt_key_value == "Iron"){
                                            $iron_dv =rtrim($nt_value_explode[1], "%");
                                            $iron_dv_unit = substr($nt_value_explode[1], -1);
                                        }
                                    }

                                }// End Column C code
                                // Start Column D code
                                if($serving_data != "") {

                                    $serving_data_explode = explode("<<>>", $serving_data);
                                    $serving_data_count = count($serving_data_explode);
                                    $serving_size_data = $serving_data_explode[0];
                                    $serving_size_data_explode = explode(" ", $serving_size_data);

                                    $serving_quantity =  $this->calculateServingQuantity($serving_size_data_explode[0]);
                                    $unit = $serving_size_data_explode;
                                    $sliced_unit = array_slice($unit, 1); // array ( "Hello", "World" )

                                    $serving_size_unit = implode(" ", $sliced_unit);  // "Hello World";

                                    //$serving_size_unit =  $serving_size_data_explode[1];
                                    $serving_json_data = [];

                                    foreach ($serving_data_explode as $key => $ServingValue) {
                                        $serving_json_data[$key]['label'] =$ServingValue;
                                        $serving_json_data[$key]['unit'] =$serving_size_unit;

                                    }
                                    $servings_json = json_encode($serving_json_data);
                             
                                }
                                // Logic implementation of Save Nutrition brand and Nutritoin
               
                                if($checkValidString){

                                    // Save data in nutrition table
                                    $nutrition = new \App\Models\Nutrition();
                                    // check  nutrtion brand exist
                                    $nutrition_brand = \App\Models\Nutrition_brand::select('id','name')
                                                                                    ->where('name',$brand_name)
                                                                                    ->first();
                                    if($nutrition_brand != null){
                                        $nutrition->nutrition_brand_id = $nutrition_brand->id;

                                    }else{
                                        $brand_data['name']= $brand_name;
                                        $brand_data['brand_id']= str_random(24);
                                        $nutrition_brand = \App\Models\Nutrition_brand::Create($brand_data);
                                        $nutrition->nutrition_brand_id = $nutrition_brand->id;

                                    }
                                    $nutrition->row_id = $row_id;
                                    $nutrition->name = $nutrition_name;
                                    $nutrition->name_with_brand = $brand_with_name;
                                    $nutrition->source_name = "myfitnesspal";
                                    $nutrition->calories = $calories;
                                    $nutrition->serving_quantity = $serving_quantity;
                                    $nutrition->serving_size_unit = $serving_size_unit;
                                    $nutrition->servings_json = $servings_json;
                                    $nutrition->calcium_dv = $calcium_dv;
                                    $nutrition->calcium_dv_unit = $calcium_dv_unit;
                                    $nutrition->cholesterol = $cholesterol;
                                    $nutrition->cholesterol_unit = $cholesterol_unit;
                                    $nutrition->dietary_fiber = $dietary_fiber;
                                    $nutrition->dietary_fiber_unit = $dietary_fiber_unit;
                                    $nutrition->iron_dv = $iron_dv;
                                    $nutrition->iron_dv_unit = $iron_dv_unit;
                                    $nutrition->polyunsaturated = $polyunsaturated;
                                    $nutrition->polyunsaturated_unit = $polyunsaturated_unit;
                                    $nutrition->monounsaturated = $monounsaturated;
                                    $nutrition->monounsaturated_unit = $monounsaturated_unit;
                                    $nutrition->potassium = $potassium;
                                    $nutrition->potassium_unit = $potassium_unit;
                                    $nutrition->protein = $protein;
                                    $nutrition->protein_unit = $protein_unit;
                                    $nutrition->saturated_fat = $saturated_fat;
                                    $nutrition->saturated_fat_unit = $saturated_fat_unit;
                                    $nutrition->sodium = $sodium;
                                    $nutrition->sodium_unit = $sodium_unit;
                                    $nutrition->sugars = $sugars;
                                    $nutrition->sugars_unit = $sugars_unit;
                                    $nutrition->total_carb = $total_carb;
                                    $nutrition->total_carb_unit = $total_carb_unit;
                                    $nutrition->total_fat = $total_fat;
                                    $nutrition->total_fat_unit = $total_fat_unit;
                                    $nutrition->trans_fat = $trans_fat;
                                    $nutrition->trans_fat_unit = $trans_fat_unit;
                                    $nutrition->vitamin_a = $vitamin_a;
                                    $nutrition->vitamin_a_unit = $vitamin_a_unit;
                                    $nutrition->vitamin_c = $vitamin_c;
                                    $nutrition->vitamin_c_unit = $vitamin_c_unit;
                                    $nutrition->save();

                                }else{
                                     // Insert data in invalid nutritions table
                                    // $exits_invalid_nutrition_data = \App\Models\Invalid_nutrition::where('row_id',$row_id)->first();
                                    // if(is_null($exits_invalid_nutrition_data)){
                                        $invalid_nutrition = new \App\Models\Invalid_nutrition();
                                        $invalid_nutrition->row_id =$row_id;
                                        $invalid_nutrition->barnd_with_name =$brand_with_name;
                                        $invalid_nutrition->nutrition_data =$nutrition_data;
                                        $invalid_nutrition->serving_data =$serving_data;
                                        $invalid_nutrition->save();
                                    // }    
                                }

                                    $this->info($row_id.' Nutrition save successfully!'.$file_name);
                            //}  
                        }

                      //  $this->info($i.' Line number of file! '.$file_name);
                        $i++;
                    }

                    $nt_reader->close();
                    $this->info('Nutrition imported end...!'.$file_name);  
                }
                 

        }catch(\Exception $e) {
          $this->info($e);
         
       }
    }

    public function calculateServingQuantity($serving_quantity_data){
        if($serving_quantity_data !="") {
             
             $explode_quantity_data = explode("/", $serving_quantity_data);
             if(count($explode_quantity_data) >1){
                $quanty = (float)$explode_quantity_data[0]/(float) $explode_quantity_data[1];
             }else{
                $quanty = (float)$explode_quantity_data[0];
             }
             return round($quanty,2);
        }else{
            return $serving_quantity_data;
        }
       

    }

    public function checkValidString($string){

        // if (preg_match('/[A-Za-z0-9_~\-!@#\$%\^&\*\(\)]+$/',$string)) {
        //     return "valid  ".$string;
        // }else{
        //     return "Invalid  ".$string;
        // }

        if (preg_match('/[ÃÂ©]/', $string))
            {
                return false;
            }else{
                return true;  
            }
    }    
}
