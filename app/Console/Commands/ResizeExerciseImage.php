<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Image;
use File;

class ResizeExerciseImage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resize:exercise_images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will resize images and save in thumb folder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         try{
            $this->info('Processing..');
            $exercise_images = \App\Models\Exercise_image::get(['image_name']);
            
            if($exercise_images->count() > 0){

                $destination_path = "public/exercise-images/";
                foreach ($exercise_images as $key => $value) {
                    
                    $thumb_img = Image::make($destination_path.$value->image_name)->resize(300, 200);
                    
                    $thumb_path = "public/exercise-images/thumb";
                    if(!File::exists($thumb_path)) {
                        $result = File::makeDirectory($thumb_path, 0777, true, true);
                    }
            

                    $thumb_img->save($thumb_path.'/'.$value->image_name);

                    $this->info($key+1 . " file created." );
                }

                $this->info('Process completed!');
                
            }
            else{
                $this->info('No images found!');
            }

        }catch(\Exception $e){
             $this->info($e);
        }
    }
}
