<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;

class NutritionBrandsImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nutritions:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Nutrition brands and Nutrition import from csv or text  file.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

       
       //return;
       /*$brands = \App\Models\Nutrition_brand::where('id', '>', 8511)->get();
       foreach ($brands as $brand) {
          $bid = trim($brand->brand_id);
          echo 'brand_id='.$brand->id."\n";

          \DB::table('nutritions')
            ->where('brand_id', $bid)
            ->update(array('nutrition_brand_id' => $brand->id));
         
       }
       dd("exit");*/
       // try{

       //    $file_path = public_path().'/nutrition/nt_brand.TXT'; 
       //    $nt_brand_reader = \CsvReader::open($file_path,"\t");
       //    $nt_brand = 0;
       //    $this->info('Nutrition Brand import started...!');
       //   while (($line = $nt_brand_reader->readLine()) !== false) {

       //          $nutrition_brand = new \App\Models\Nutrition_brand();
       //          $nutrition_brand->name = $line[0];
       //          $nutrition_brand->brand_id = $line[1];
       //          $nutrition_brand->save();
       //          $this->info($nt_brand.' Nutrition Brand save successfully!');
       //          $nt_brand++;
                
       //    }
       //     $nt_brand_reader->close();
       //     $this->info('Nutrition Brand imported end...!');

       // }catch(\Exception $e){


       // }
       


      try {
           $this->info('Nutrition import started...!');
           $nt_file_path = public_path().'/nutrition/NT_FULL_inch.TXT'; 
           $nt_reader = \CsvReader::open($nt_file_path,"\t");
           $i=0;

           while (($line = $nt_reader->readLine()) !== false) {
                 // if($i <= 169803) {$i++;continue;}
                 // $i++;
                 // if(count($line) != 29){
                    //$this->info('line no...!'.$i.':::count'.count($line).'::name::'.$line[1]);
                 // }
                 // continue;
                  try{
                   //print_r($line);
                   $brand_id = null;
                   $nutrition_brand = null;
                   try{
                    $brand_id = trim($line[3]);
                   }catch(\Exception $e){

                   }

                   if($line[0] != "" ){
                      if($brand_id != null){
                        $nutrition_brand = \App\Models\Nutrition_brand :: where('brand_id',$brand_id)->first();
                      }
                      
                      $nutrition = new \App\Models\Nutrition();
                      if($nutrition_brand != null){
                        $nutrition->nutrition_brand_id = $nutrition_brand->id;
                        $nutrition->brand_id = $brand_id;
                      }
                      
                      $nutrition->name = $line[0];
                       try{
                        $nutrition->item_id = $line[1];
                       }catch(\Exception $e){

                       }

                      try{
                        $nutrition->calories = $line[2];
                       }catch(\Exception $e){

                       }

                       try{
                        $nutrition->serving_quantity = $line[4];
                       }catch(\Exception $e){

                       }

                       try{
                         $nutrition->serving_size_unit = $line[5];
                       }catch(\Exception $e){

                       }

                       try{
                         $nutrition->serving_size_weight_grams = $line[6];
                       }catch(\Exception $e){

                       }

                       try{
                        $nutrition->servings = $line[7];
                       }catch(\Exception $e){

                       }

                       try{
                         $nutrition->calcium_dv = $line[8];
                       }catch(\Exception $e){

                       }

                       try{
                        $nutrition->calories_from_fat = $line[9];
                       }catch(\Exception $e){

                       }

                       try{
                        $nutrition->cholesterol = $line[10];
                       }catch(\Exception $e){

                       }

                       try{
                         $nutrition->data_source = $line[11];
                       }catch(\Exception $e){

                       }

                       try{
                         $nutrition->dietary_fiber = $line[12];
                       }catch(\Exception $e){

                       }

                       try{
                        $nutrition->iron_dv = $line[13];
                       }catch(\Exception $e){

                       }

                       try{
                        $nutrition->metric_unit = $line[14];
                       }catch(\Exception $e){

                       }

                       try{
                        $nutrition->potassium = $line[15];
                       }catch(\Exception $e){

                       }

                       try{
                         $nutrition->protein = $line[16];
                       }catch(\Exception $e){

                       }

                       try{
                         $nutrition->saturated_fat = $line[17];
                       }catch(\Exception $e){

                       }

                       try{
                        $nutrition->sodium = $line[18];
                       }catch(\Exception $e){

                       }

                       try{
                        $nutrition->sugars = $line[19];
                       }catch(\Exception $e){

                       }

                       try{
                         $nutrition->total_carb = $line[20];
                       }catch(\Exception $e){

                       }

                       try{
                         $nutrition->total_fat = $line[21];
                       }catch(\Exception $e){

                       }

                       try{
                          $nutrition->trans_fat = $line[22];
                       }catch(\Exception $e){

                       }

                       try{
                         $nutrition->upc = $line[23];
                       }catch(\Exception $e){

                       }

                       try{
                        $nutrition->vitamin_a = $line[24];
                       }catch(\Exception $e){

                       }

                       try{
                        $nutrition->vitamin_c = $line[25];
                       }catch(\Exception $e){

                       }

                       try{
                        $nutrition->image_name = $line[26];
                       }catch(\Exception $e){

                       }

                       try{
                         $nutrition->ingredient_statement = $line[27];
                       }catch(\Exception $e){

                       }

                      
                      $nutrition->save();
                      //$this->info($i.' Nutrition save successfully!');
                   }
                 }catch(\Exception $e){
                    $this->info($i.'error...!');
                    dd('exit');
                 }
               
              $i++;
           }
           $nt_reader->close();
           $this->info('Nutrition imported end...!');

       }catch(\Exception $e){
          $this->info($e);
         // dd('exit');
       }
    
    }
}
