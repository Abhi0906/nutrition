<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

class ChallengesExpire extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expire:challenges';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will expire chllenges';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         try{
            $now = Carbon::now();

            $challenges = \App\Models\Challenges::whereDate('end_date', '<=', $now)
                                        ->where('is_expired', 0)
                                        ->get();
            if($challenges->count() > 0){
                
                $challenges_expire = \App\Models\Challenges::whereDate('end_date', '<=', $now)
                                    ->where('is_expired', 0)
                                    ->update(['is_expired' => '1' ]);

                if($challenges_expire){
                    $this->info('Challenges expired successfully!');
                }
                else{
                    $this->info('Challenges are not expired successfully!');
                }
            }
            else{
                $this->info('No challenges are expiring now!');
            }

        }catch(\Exception $e){
             $this->info($e);
        }
    }
}
