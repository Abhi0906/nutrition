<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ImportExercise extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:exercise';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        try{

              $file_name = "bodybuilding_steps.csv";
              $exercise_file_path = public_path().'/workouts/'.$file_name; 
              //  echo $exercise_file_path;exit;
              $ex_reader = \CsvReader::open($exercise_file_path);

              $count = 0;

              $this->info('Exercise import started...!');
                 while (($line = $ex_reader->readLine()) !== false) {
                       // print_r($line);exit;
                      if($count > 0 && $line[0] != ""){
                         $title = isset($line[0])? $line[0]:'';
                         $type = isset($line[1])? $line[1]:'';
                         $main_muscle_worked = isset($line[2])?$line[2] :'';
                         $other_muscles = isset($line[3])?$line[3] : '';
                         $equipment = isset($line[4]) ? $line[4]:'';
                         $mechanics_type = isset($line[5]) ? $line[5]:'';
                         $level = isset($line[6])?$line[6] :'';
                         $sport = isset($line[7]) ? $line[7]:'';
                         $force = isset($line[8]) ? $line[8]:'';
                         $step_1 = isset($line[9]) ? $line[9]:'';
                         $step_2 = isset($line[10]) ? $line[10]:'';
                         $step_3 = isset($line[11]) ? $line[11]:'';
                         $step_4 = isset($line[12]) ? $line[12]:'';
                         $step_5 = isset($line[13]) ? $line[13]:'';
                         $step_6 = isset($line[14]) ? $line[14]:'';
                         $step_7 = isset($line[15]) ? $line[15]:'';
                         $step_8 = isset($line[16]) ? $line[16]:'';
                         $step_9 = isset($line[17]) ? $line[17]:'';
                         $step_10 = isset($line[18]) ? $line[18]:'';
                         $step_11 = isset($line[19]) ? $line[19]:'';
                         $step_12 = isset($line[20]) ? $line[20]:'';
                         $step_13 = isset($line[21]) ? $line[21]:'';
                         $step_14 = isset($line[22]) ? $line[22]:'';
                         $step_15 = isset($line[23]) ? $line[23]:'';
                         $step_16 = isset($line[24]) ? $line[24]:'';
                         $step_17 = isset($line[25]) ? $line[25]:'';
                         $step_18 = isset($line[26]) ? $line[26]:'';
                         $step_19 = isset($line[27]) ? $line[27]:'';
                         $step_20 = isset($line[28]) ? $line[28]:'';
                         $step_21 = isset($line[29]) ? $line[29]:'';
                         $step_22 = isset($line[30]) ? $line[30]:'';
                         $step_23 = isset($line[31]) ? $line[31]:'';
                         $step_24 = isset($line[32]) ? $line[32]:'';

                         $steps =[];

                         for($i=1;$i <= 24; $i++){
                            $steps['step_'.$i] =${"step_" . $i};
                         }
                        

                            $exercise = new \App\Models\Exercise();

                            $exercise->name = $title;
                            $exercise->weight = 150;
                            $exercise->time = 30;
                            $exercise->calories_burned = 180;
                            $exercise->source_name = 'bodybuilding';
                            $exercise->type = $type;
                            $exercise->main_muscle_worked = $main_muscle_worked;
                            $exercise->other_muscles = $other_muscles;
                            $exercise->equipment = $equipment;
                            $exercise->mechanics_type = $mechanics_type;
                            $exercise->level = $level;
                            $exercise->sport = $sport;
                            $exercise->force = $force;
                            $exercise->steps = json_encode($steps);
                            $exercise->save();
                            $this->info($count.' Exercise save successfully!');

                       }
                       $count++;
                        
                  }
               $ex_reader->close();
               $this->info('Exercise imported end...!');

           }catch(\Exception $e){


           }
       


    }
}
