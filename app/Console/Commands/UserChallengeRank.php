<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UserChallengeRank extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user_challenge:rank';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $challenges = \App\Models\Challenge::with('only_participants')
                                                ->where('is_expired', '<>', '1')
                                                ->get();
         foreach ($challenges as $key => $challenge) {
             $goal_type = $challenge->goal_type;
             if($challenge->only_participants->count() > 0){
                if($goal_type == 'steps_run'){
                     $participants = $challenge->only_participants->sortByDesc('total_steps');
                     $count_rank=0;
                     $rank=0;
                     foreach ($participants as $key => $cp) {
                        if($cp->total_steps != null && $cp->total_steps > 0 ){
                            $rank = $count_rank+1;
                        }else{
                            $rank =$rank+1;
                        }
                        
                        $challenge_participant_id = $cp['id']; 
                        $cpu =\App\Models\Challenge_participant::find($challenge_participant_id);
                        $cpu->update(['rank'=>$rank]);
                        $count_rank++;
                     }
                }
             }
         
         }
    }
}
