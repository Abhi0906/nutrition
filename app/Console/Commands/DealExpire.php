<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;


class DealExpire extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expire:deals';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make deal expire with this deal:expire code';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            $now = Carbon::now();

            $deals = \App\Models\Deal::whereDate('end', '<=', $now)
                                        ->where('is_expired', 0)
                                        ->get();
            if($deals->count() > 0){
                
                $deal_expire = \App\Models\Deal::whereDate('end', '<=', $now)
                                    ->where('is_expired', 0)
                                    ->update(['is_expired' => '1' ]);

                if($deal_expire){
                    $this->info('Deals expired successfully!');
                }
                else{
                    $this->info('Deals are not expired successfully!');
                }
            }
            else{
                $this->info('No deals are expiring now!');
            }

        }catch(\Exception $e){
             $this->info($e);
        }
        
    }
}
