<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Goutte\Client;

class WebScrapper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'web:scrapper';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Web scrapper.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // 
        /*
        Variables :: 
        $top_category, $category, $sub_category, $food, $servingSize, $calories, $calories_kj, $calories_from_fat,  $total_fat,  $total_fat_daily_value, $sat_fat,  $sat_fat_daily_value, $cholesterol, $cholesterol_daily_value, $sodium, $sodium_daily_value, $total_carbs, $total_carbs_daily_value, $carbs_dietary_fiber, $carbs_dietary_fiber_daily_value, $carbs_sugars, $carbs_sugars_daily_value, $protein, $protein_daily_value, $calcium, $calcium_daily_value, $potassium, $potassium_daily_value, 
         */
        
        $writer = \CsvWriter::create(storage_path().'/food.csv');
        $writer->writeLine(['top_category', 'category', 'sub_category', 'food', 'servingSize', 'calories', 'calories_kj', 'calories_from_fat',  'total_fat',  'total_fat_daily_value', 'sat_fat',  'sat_fat_daily_value', 'cholesterol', 'cholesterol_daily_value', 'sodium', 'sodium_daily_value', 'total_carbs', 'total_carbs_daily_value', 'carbs_dietary_fiber', 'carbs_dietary_fiber_daily_value', 'carbs_sugars', 'carbs_sugars_daily_value', 'protein', 'protein_daily_value', 'calcium', 'calcium_daily_value', 'potassium', 'potassium_daily_value' ]);
        $writer->flush();
       
        print "Web scrapping started...\n";
        $startTime = \Carbon\Carbon::now();
        $client = new Client();
        $counter = 0;
        $crawler = $client->request('GET', 'http://www.calorieking.com/foods/');
        //$client->getClient()->setDefaultOption('config/curl/'.CURLOPT_TIMEOUT, [60]);
        //
        $crawler->filter('#topcategories > .topcategory')->each(function ($node) use ($crawler, $client,&$counter, &$writer){
            $node->filter('h4 > a')->each(function ($node1) use ($node, $crawler, $client,&$counter, &$writer) {
                //print $node1->text()."\n";
                $top_category = $node1->text();
                $node->filter('.sub-menu > .menu-arrow > .menu-items  >  .category > a')->each(function ($node2) use ($crawler, $client,&$counter, $top_category, &$writer) {
                    //print "\t=>  ".$node2->text()."\n";
                    $category = $node2->text();
                    $node2->siblings()->filter('.menu-arrow > .menu-items  >  .subcategory > a')->each(function ($node3) use ($crawler, $client,&$counter,$top_category,$category, &$writer) {
                        //print "\t\t=> ".$node3->text()."\n";
                        $sub_category = $node3->text();
                        $link = $node3->attr('href');
                        $crawler1 = $client->request('GET',$link);
                        //$link = $crawler->selectLink($node3->text())->link();
                        
                        //$crawler1 = $client->click($link);
                        $hasChildren = false; 
                        $crawler1->filter('.result > div > ul > li > a')->each(function ($node4) use ($crawler1,  $client, &$counter, &$hasChildren, $top_category, $category, $sub_category, &$writer){
                            print "adding...".$node4->text()."\n";
                            $hasChildren = true;
                            
                           // try{
                                //$link1 = $crawler1->selectLink($node4->text())->link();
                                $link1 = $node4->attr('href');
                                $crawler2 = $client->request('GET',$link1);
                                //$crawler2 = $client->click($link1);
                                $this->extractData($crawler2,$top_category, $category, $sub_category,$writer,$node4->text()); 

                                $counter = $counter+1;
                                print "added...".$node4->text()."::".$counter."\n";
                                print "\n----------------------\n";
                           // }catch(\Exception $e){

                           // }
                        });
                        if($hasChildren == false){
                            print "\n---------- No children -------------\n";
                            print "adding...".$node3->text()."\n";
                            $this->extractData($crawler1,$top_category, $category, $sub_category,$writer,"");
                            $counter = $counter+1;
                            print "added...".$node3->text()."::".$counter."\n";
                            print "\n----------------------\n";
                        }                      
                    });
                });
               
            });
        });    
        /*$crawler->filter('#topcategories > div > h4 > a')->each(function ($node) use ($crawler){
            print $node->text()."\n";
            $node->filter($node > '#topcategories > div > div > div > div > div > a')->each(function ($node1) {
                print "\t".$node1->text()."\n";
            });    
           
        }); */
        $writer->close();
        
        
        
        print "\n total records::".$counter;
        
        print_r('Web scrapping end...');
    }

    public function extractData($crawler2,$top_category, $category, $sub_category,$writer, $food){
        
        if(empty($food)){
            try{
                $food = trim($crawler2->filter('#heading-food-cat-desc')->text());
            }catch(\Exception $e){
           
            }    
        }       
        
       // print "\n---------food----------".$food."\n";
        $servingSize = "";
        try{
            $servingSize = $crawler2->filter('#units')->first()->children()->first()->text();
        }catch(\Exception $e){

        }
        $calories = "";
        try{
            $calories = $crawler2->filter('.energy > .calories > .amount')->first()->text();
        }catch(\Exception $e){

        }
        $calories_kj = "";
        try{
            $calories_kj = $crawler2->filter('.energy > .kilojoules > .amount')->first()->text();
        }catch(\Exception $e){

        }
        $calories_from_fat = "";
        try{
            $calories_from_fat = $crawler2->filter('.fat-calories > td > .amount')->first()->text();
        }catch(\Exception $e){

        }
        $total_fat = "";
        try{
            $total_fat = $crawler2->filter('.total-fat > .amount')->first()->text();
        }catch(\Exception $e){

        }
        $total_fat_daily_value = "";
        try{
            $total_fat_daily_value = $crawler2->filter('.total-fat > .daily-value')->first()->text();
        }catch(\Exception $e){

        }
        $sat_fat = "";
        try{
            $sat_fat = $crawler2->filter('.sat-fat > .amount')->first()->text();
        }catch(\Exception $e){

        }
        $sat_fat_daily_value = "";
        try{
            $sat_fat_daily_value = $crawler2->filter('.sat-fat > .daily-value')->first()->text();
        }catch(\Exception $e){

        }
        $cholesterol = "";
        try{
            $cholesterol = $crawler2->filter('.cholesterol > .amount')->first()->text();
        }catch(\Exception $e){

        }
        $cholesterol_daily_value = "";
        try{
            $cholesterol_daily_value = $crawler2->filter('.cholesterol > .daily-value')->first()->text();
        }catch(\Exception $e){

        }
        $sodium = "";
        try{
            $sodium = $crawler2->filter('.sodium > .amount')->first()->text();
        }catch(\Exception $e){

        }
        $sodium_daily_value = "";
        try{
            $sodium_daily_value = $crawler2->filter('.sodium > .daily-value')->first()->text();
        }catch(\Exception $e){

        }
        $total_carbs = "";
        try{
            $total_carbs = $crawler2->filter('.total-carbs > .amount')->first()->text();
        }catch(\Exception $e){

        }
        $total_carbs_daily_value = "";
        try{
            $total_carbs_daily_value = $crawler2->filter('.total-carbs > .daily-value')->first()->text();
        }catch(\Exception $e){

        }
        $carbs_dietary_fiber = "";
        try{
            $carbs_dietary_fiber = $crawler2->filter('.fiber > .amount')->first()->text();
        }catch(\Exception $e){

        }                                                      
        $carbs_dietary_fiber_daily_value = "";
        try{
            $carbs_dietary_fiber_daily_value = $crawler2->filter('.fiber > .daily-value')->first()->text();
        }catch(\Exception $e){

        }
        $carbs_sugars = "";
        try{
            $carbs_sugars = $crawler2->filter('.sugars > .amount')->first()->text();
        }catch(\Exception $e){

        }
        $carbs_sugars_daily_value = "";
        try{
            $carbs_sugars_daily_value = $crawler2->filter('.sugars > .daily-value')->first()->text();
        }catch(\Exception $e){

        }
        $protein = "";
        try{
            $protein = $crawler2->filter('.protein > .amount')->first()->text();
        }catch(\Exception $e){

        }
        $protein_daily_value = "";
        try{
            $protein_daily_value = $crawler2->filter('.protein > .daily-value')->first()->text();
        }catch(\Exception $e){

        }                           
        $calcium = "";
        try{
            $calcium = $crawler2->filter('.calcium > .amount')->first()->text();
        }catch(\Exception $e){

        }
        $calcium_daily_value = "";
        try{
            $calcium_daily_value = $crawler2->filter('.calcium > .daily-value')->first()->text();
        }catch(\Exception $e){

        }
        $potassium = "";
        try{
            $potassium = $crawler2->filter('.potassium > .amount')->first()->text();
        }catch(\Exception $e){

        }
        $potassium_daily_value = "";
        try{
            $potassium_daily_value = $crawler2->filter('.potassium > .daily-value')->first()->text();
            //dd($potassium_daily_value);
           
        }catch(\Exception $e){

        }
        $writer->writeLine([ $top_category, $category, $sub_category, $food, $servingSize, $calories, $calories_kj, $calories_from_fat,  $total_fat,  $total_fat_daily_value, $sat_fat,  $sat_fat_daily_value, $cholesterol, $cholesterol_daily_value, $sodium, $sodium_daily_value, $total_carbs, $total_carbs_daily_value, $carbs_dietary_fiber, $carbs_dietary_fiber_daily_value, $carbs_sugars, $carbs_sugars_daily_value, $protein, $protein_daily_value, $calcium, $calcium_daily_value, $potassium, $potassium_daily_value ]);   
       // echo $writer->flush();
       // print "\n-------------------------\n";
        

    }
}
