<?php

namespace App\Console\Commands;
use App\Models\Video;
use Illuminate\Console\Command;
use Vimeo\Laravel\VimeoManager;

class FetchVideos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'videos:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch Videos.';
    protected $vimeo;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(VimeoManager $vimeo)
    {
        parent::__construct();
        $this->vimeo = $vimeo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $videos =  $this->vimeo->request('/me/videos','' , 'GET');
        if(!empty($videos)){
             $this->info('Videos fetch starting !');
            foreach ($videos['body']['data'] as $key => $video) {
              
                $video['external_video_id'] = array_last((explode('/', $video['uri'])));
                //  dd($video['external_video_id']);
                $video['embed'] = $video['embed']['html'];
                $video['pictures'] = json_encode($video['pictures']);

                $result = Video::where('external_video_id', '=', $video['external_video_id'])->first();

                if(is_null($result)){
                     $this->info('Videos create',$key);
                   Video::create($video); 
                }
                else{
                    $this->info('Videos update',$key);
                    $video_update = [ 'name' => $video['name'],
                                      'description' => $video['description'],
                                      'duration' => $video['duration'],
                                      'width'=> $video['width'],
                                      'language' => $video['language'],
                                      'height' => $video['height'],
                                      'status' => $video['status'],
                                      'embed' => $video['embed'] ,
                                      'pictures' => $video['pictures']];
                    
                    Video::where('external_video_id', '=', $video['external_video_id'])
                            ->update($video_update);
                }


            }
            $this->info('Videos fetch successfully!');
        }
    }
}
