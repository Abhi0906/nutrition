<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CheckVerifyEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'verify_email:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Verify email for all register users.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
             $this->info('Email verification started...!');

             $users = \App\Models\User::where('user_type','patient')
                                        ->where('email_varify',0)
                                        ->get();
            
             foreach ($users as $key => $user) {
               $diffIndays = $user->created_at->diffInDays();
               if($diffIndays <= 7){
                dd($user->forceDelete());
               }
               
                
             }

             $this->info('Email verification end...!');


           }catch(\Exception $e){
                 $this->info($e);
           }
    }
}
