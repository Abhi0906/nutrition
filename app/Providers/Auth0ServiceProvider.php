<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class Auth0ServiceProvider extends ServiceProvider{

    public function register(){

     $this->app->bind('App\Repositories\Auth0Interface', 'App\Repositories\Auth0Repositories');

	}
  
}
