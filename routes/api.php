<?php
header('Access-Control-Allow-Origin: *');
header( 'Access-Control-Allow-Headers: Authorization, Content-Type, mobile-app' );
header('Access-Control-Allow-Methods', '*');
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/user', function (Request $request) {
//     return $request->user();
// })->middleware('auth:api');
Route::prefix('/v2/admin')->group(function() {
  Route::get('/patient/list', ['uses' => 'UserController@getList', 'as' => 'patient.getList'])->middleware('auth:admin');
  Route::get('/doctor/list', ['uses' => 'UserController@getList', 'as' => 'doctor.getList'])->middleware('auth:admin');
  //Route::get('/video/body_parts', ['uses' => 'VideoController@getBodyParts', 'as' => 'video.getBodyParts'])->middleware('auth:admin');
  //Route::get('/video/equipments', ['uses' => 'VideoController@getEquipments', 'as' => 'video.getEquipments'])->middleware('auth:admin');
  //Route::get('/video/list', ['uses' => 'VideoController@getList', 'as' => 'video.getList'])->middleware('auth:admin');
  //Route::resource('/video','VideoController')->middleware('auth:admin');
  Route::get('/allowed_device/allowedevicelist', 'ChallengeController@get_alloweddevicelist')->middleware('auth:admin');
  Route::get('/social_media/get_social_media', 'SocialMediaController@getSocialMedia')->middleware('auth:admin');
  Route::post('/organization/upload', 'OrganizationController@uploadLogo')->middleware('auth:admin');
  Route::get('/organization/remove/{file_name}','OrganizationController@removeLogo')->middleware('auth:admin');
  Route::post('/organization/checksubDomain', 'OrganizationController@checksubDomain')->middleware('auth:admin');
  Route::get('/organization/list','OrganizationController@getOrganizationList')->middleware('auth:admin');
  Route::delete('/organization/delete/{id}','OrganizationController@destroy')->middleware('auth:admin');
  Route::post('/events/update_settings', 'ClassScheduleController@updateEventSetting')->middleware('auth:admin');
  Route::get('/events/settings', 'ClassScheduleController@getEventSettings')->middleware('auth:admin');
  Route::get('/events/active_settings', 'ClassScheduleController@getActiveSettings')->middleware('auth:admin');
  //Route::post('/events/upload', 'ClassScheduleController@uploadImage')->middleware('auth:admin');
  //Route::get('/events/remove/{file_name}', 'ClassScheduleController@removeImage')->middleware('auth:admin');
  Route::get('/challenge/challengelist', 'ChallengeController@get_challengelist')->middleware('auth:admin');
  Route::delete('challenge/delete/{id}', 'ChallengeController@destroy')->middleware('auth:admin');
  Route::get('challenge/item_data/{challenge_id}', 'ChallengeController@edit')->middleware('auth:admin');
  Route::put('/challenge/update/{id}','ChallengeController@updateChallenge')->middleware('auth:admin');
  Route::get('/challenge/challenges_front', 'ChallengeController@getChallengelistFront')->middleware('auth:admin');
  Route::post('/challenge/create_challenge', 'ChallengeController@store')->middleware('auth:admin');
  Route::post('/challenge_image/upload', 'ChallengeController@uploadImage')->middleware('auth:admin');
  Route::get('/challenge_image/remove/{file_name}', 'ChallengeController@removeImage')->middleware('auth:admin');
  Route::get('/challenge/challenges_with_participant/{user_id}', 'ChallengeController@challengeListWithParticipant')->middleware('auth:admin');
  Route::post('/deal_image/upload', 'DealController@uploadImage')->middleware('auth:admin');
  Route::post('/deal/create_deal', 'DealController@store')->middleware('auth:admin');
  Route::get('/deal_image/remove/{file_name}', 'DealController@removeImage')->middleware('auth:admin')->middleware('auth:admin');
  Route::get('/deal/getlist', 'DealController@getDealList')->middleware('auth:admin');
  Route::get('/deal/getlist_front', 'DealController@getDealListFront')->middleware('auth:admin');
  Route::get('/deal/get_deal/{edit_id}', 'DealController@show')->middleware('auth:admin');
  Route::put('/deal/update_deal/{edit_id}', 'DealController@update')->middleware('auth:admin');
  Route::delete('/deal/delete/{id}', 'DealController@delete')->middleware('auth:admin');
  Route::post('/social_media/update_social_media', 'SocialMediaController@updateSocialMedia')->middleware('auth:admin');
  Route::get('/referfriend/getRefered', 'UserController@getReferedFriend')->middleware('auth:admin');
  Route::get('/manage_food/edit_data/{food_id}', 'ManageFoodController@edit' )->middleware('auth:admin');
  Route::put('/manage_food/update/{id}','ManageFoodController@update')->middleware('auth:admin');
  Route::delete('/manage_food/delete/{id}', 'ManageFoodController@destroy')->middleware('auth:admin');
  Route::post('/add_custom_food', 'UserController@addCustomFood')->middleware('auth:admin');
  Route::get('/patient/switch_to_dashborad/{id}', ['uses' => 'UserController@getSwitchToDashboard'])->middleware('auth:admin');
  Route::get('/meal_plan/get_meal_plans', 'MealPlansController@getMealPlans')->middleware('auth:admin');
  Route::get('recommended_meal/item_data/{meal_id}', 'RecommendedMealController@recommendedMealItemData' )->middleware('auth:admin');
  Route::get('md_meals/meals', 'RecommendedMealController@getMDMeals' )->middleware('auth:admin');
  Route::get('/my_plan/get_workouts_template','MyPlanController@geWorkoutsTemplate')->middleware('auth:admin');
  Route::get('/my_plan/get_master_templates','MyPlanController@geMasterTemplate')->middleware('auth:admin');
  Route::get('/meal_plan/get_mplan_details/{id}','MealPlansController@getMealPlanDetails')->middleware('auth:admin');
  Route::get('/meal_plan/get_user_mplan_details/{id}','MealPlansController@getUserMealPlanDetails')->middleware('auth:admin');
  Route::put('/meal_plan/update_meal_plan/{id}','MealPlansController@updateMealPlan')->middleware('auth:admin');
  Route::put('/meal_plan/update_user_meal_plan/{id}','MealPlansController@updateUserMealPlanDetail')->middleware('auth:admin');
  Route::get('/assign_plan/get_user_plan/{user_id}','AssignPlanController@getUserMealPlan')->middleware('auth:admin');
  Route::post('/meal_plan/assign_plan_to_user','MealPlansController@assignPlanToUser')->middleware('auth:admin');
  Route::post('/meal_plan/unassign_plan_to_user','MealPlansController@unassignPlanToUser')->middleware('auth:admin');
  Route::put('/meal_plan/update_day_plan/{id}','MealPlansController@updateDayPlan')->middleware('auth:admin');
  //Route::get('/video/fetch_videos', ['uses' => 'VideoController@fetchVideos', 'as' => 'video.fetchVideos'])->middleware('auth:admin');
  Route::post('/exercise_image/upload', 'ExerciseImageController@uploadImage')->middleware('auth:admin');
  Route::get('/exercise_image/remove/{file_name}', 'ExerciseImageController@removeImage')->middleware('auth:admin');
  Route::get('/exercise_image/list', 'ExerciseImageController@getAllImages')->middleware('auth:admin');
  Route::post('/workout/attached_exercise', ['uses' => 'WorkoutController@attachedExercise', 'as' => 'workout.attachedExercise'])->middleware('auth:admin');
  Route::get('/workout/list', ['uses' => 'WorkoutController@getWorkoutWithVideosList', 'as' => 'workout.getWorkoutWithVideosList'])->middleware('auth:admin');
  Route::delete('/workout/delete_video/{workout_id}/{video_id}', ['uses' => 'WorkoutController@deleteVideo', 'as' => 'workout.deleteVideo'])->middleware('auth:admin');
  Route::post('/template/attached_workout', ['uses' => 'TemplateController@attachedWorkout', 'as' => 'template.attachedWorkout'])->middleware('auth:admin');
  Route::delete('/template/delete_workout/{template_id}/{workout_id}', ['uses' => 'TemplateController@deleteWorkout', 'as' => 'template.deleteWorkout'])->middleware('auth:admin');
  Route::post('/template/updateWorkout', ['uses' => 'TemplateController@updateWorkout', 'as' => 'template.updateWorkout'])->middleware('auth:admin');
  Route::get('/template/list', ['uses' => 'TemplateController@getTemplateWithWorkout', 'as' => 'template.getTemplateWithWorkout'])->middleware('auth:admin');
  Route::post('recommended_food/create', 'FoodController@createMdRecommendedFood' )->middleware('auth:admin');
  Route::get('recommended_food/types', 'FoodController@getFoodRecommendedTypes')->middleware('auth:admin');
  Route::get('recommended_food/display', 'FoodController@mdRecommendedFoodsDisplayData')->middleware('auth:admin');
  Route::delete('recommended_food/delete/{id}', 'FoodController@deleteMdRecommendedFood')->middleware('auth:admin');
  Route::get('recommended_food/edit/{id}', 'FoodController@getNutrition' )->middleware('auth:admin');
  Route::put('recommended_food/update/{update_id}', 'FoodController@updateRecommendedFood' )->middleware('auth:admin');
  Route::get('recommended_food/all_types_foods/{user_id}', 'FoodController@getMdRecommendedFoodsFront')->middleware('auth:admin');
  Route::get('recommended_meal/items', 'RecommendedMealController@getRecommendedMealItems')->middleware('auth:admin');
  Route::post('/meal_plan/create_meal_plan', 'MealPlansController@saveMealPlan')->middleware('auth:admin');
  Route::delete('/meal_plan/delete/{id}','MealPlansController@delete')->middleware('auth:admin');
  Route::put('/meal_plan/update_plan/{id}','MealPlansController@updatePlan')->middleware('auth:admin');
  Route::post('recommended_meal/create_meal', 'RecommendedMealController@createRecommendedMeal' )->middleware('auth:admin');
  Route::delete('recommended_meal/delete/{id}', 'RecommendedMealController@deleteRecommendedMeal')->middleware('auth:admin');
  Route::post('/meal/create_meal','RecommendedMealController@createNewMeal')->middleware('auth:admin');
  Route::delete('recommended_meal/food_delete/{id}', 'RecommendedMealController@deleteRecommendedMealFood')->middleware('auth:admin');
  Route::get('/food/meal_id/{id}', 'RecommendedMealController@getMeal' )->middleware('auth:admin');
  Route::put('/meal/update_meal/{id}','RecommendedMealController@updateMeal')->middleware('auth:admin');
  Route::get('recommended_meal/food_type', 'RecommendedMealController@getRecommendedTypeFood' )->middleware('auth:admin');
  Route::post('recommended_meal/create_meal_food', 'RecommendedMealController@createRecommendedMealFood' )->middleware('auth:admin');
  Route::post('recommended_meal/save_meal_foods', 'RecommendedMealController@saveRecommendedMealFood')->middleware('auth:admin');
  Route::put('recommended_meal/update_meal_food/{update_id}', 'RecommendedMealController@updateRecommendedMealFood' )->middleware('auth:admin');
  Route::post('/upload', 'UserController@upload')->middleware('auth:admin');;
  Route::get('/removephoto/{file_name}','UserController@remove')->middleware('auth:admin');;
  Route::get('/patient/demographics/{id}', ['uses' => 'UserController@getUser', 'as' => 'patient.getUser'])->middleware('auth:admin');;
  Route::get('/doctor/demographics/{id}', ['uses' => 'UserController@getUser', 'as' => 'doctor.getUser'])->middleware('auth:admin');;
  Route::resource('/workout','WorkoutController')->middleware('auth:admin');
  Route::resource('/template','TemplateController')->middleware('auth:admin');
  Route::resource('/exercise_image','ExerciseImageController')->middleware('auth:admin');
  Route::resource('/patient','UserController')->middleware('auth:admin');
  Route::resource('/doctor','UserController')->middleware('auth:admin');
});

Route::prefix('/v2')->group(function() {
  Route::get('/video/list', ['uses' => 'VideoController@getList', 'as' => 'video.getList'])->middleware('auth:admin');
  Route::get('/video/training_types', ['uses' => 'VideoController@getTrainingTypes', 'as' => 'video.getTrainingTypes'])->middleware('auth:admin');
  Route::get('/video/equipments', ['uses' => 'VideoController@getEquipments', 'as' => 'video.getEquipments'])->middleware('auth:admin');
  Route::get('/video/fetch_videos', ['uses' => 'VideoController@fetchVideos', 'as' => 'video.fetchVideos'])->middleware('auth:admin');
  Route::get('/video/body_parts', ['uses' => 'VideoController@getBodyParts', 'as' => 'video.getBodyParts'])->middleware('auth:admin');
  Route::resource('/video','VideoController')->middleware('auth:admin');
});
//Prefix V3 For Client side
Route::group(['prefix' => '/v3','middleware'=>['apiAuth','useractivity']], function () {
//Route::prefix('/v1')->group(function() {
   Route::get('/video/body_parts', ['uses' => 'VideoController@getBodyParts', 'as' => 'video.getBodyParts']);
   Route::get('/video/equipments', ['uses' => 'VideoController@getEquipments', 'as' => 'video.getEquipments']);
   Route::post('/upload', 'UserController@upload');
   Route::get('/removephoto/{file_name}','UserController@remove');
   Route::get('/patient/medicines/{med_name}', ['uses' => 'UserController@medicines', 'as' => 'patient.medicines']);
   Route::post('/patient/vitalParameters', ['uses' => 'UserController@saveVitalParameters', 'as' => 'patient.saveVitalParameters']);
   Route::post('/patient/profile', ['uses' => 'UserController@saveProfile', 'as' => 'patient.saveProfile']);
   Route::get('/activities', ['uses' => 'UserController@getActivity', 'as' => 'patient.getActivity']);
   Route::get('/patient/list', ['uses' => 'UserController@getList', 'as' => 'patient.getList']);
   Route::get('/doctor/list', ['uses' => 'UserController@getList', 'as' => 'doctor.getList']);
   Route::get('/patient/vitals/{id}', ['uses' => 'UserController@getVitals', 'as' => 'patient.getVitals']);
   Route::get('/patient/profile/{id}', ['uses' => 'UserController@getProfile', 'as' => 'patient.getProfile']);
   Route::get('/patient/demographics/{id}', ['uses' => 'UserController@getUser', 'as' => 'patient.getUser']);
   Route::get('/patient/weeklygoals', ['uses' => 'UserController@getWeeklygoals','as' => 'goals.getWeeklygoals']);
   Route::get('/patient/waterlog/{id}', ['uses' => 'UserController@getwaterlog','as' => 'front.getwaterlog']);
   Route::post('/patient/water_intake', ['uses' => 'UserController@saveWaterintake', 'as' => 'patient.saveWaterintake']);
   Route::get('/doctor/demographics/{id}', ['uses' => 'UserController@getUser', 'as' => 'doctor.getUser']);
   Route::get('/doctor_titles', ['uses' => 'UserController@getDoctorTitles', 'as' => 'doctor.getDoctorTitles']);

   Route::get('/getReferedFriend', ['uses' => 'UserController@getReferedFriend']);


   Route::get('/patient/routine_times/{id}', ['uses' => 'UserController@getUserRoutinetimes', 'as' => 'patient.getRoutinetimes']);
   Route::post('/patient/medicines', ['uses' => 'UserController@saveMedication', 'as' => 'patient.saveMedication']);
   Route::get('/patient/user_medicine_list/{id}', ['uses' => 'UserController@getUserMedicinelist', 'as' => 'patient.getUserMedicinelist']);
   Route::delete('/patient/user_medicine_delete/{user_id}/{medicine_id}', ['uses' => 'UserController@deleteMedicine', 'as' => 'patient.deleteMedicine']);

   Route::get('/patient/user_schedule/{id}', ['uses' => 'UserController@getUserSchedule', 'as' => 'patient.getUserSchedule']);

   Route::post('/user_schedule/attached_template', ['uses' => 'UserController@attachedTemplate', 'as' => 'patient.attachedTemplate']);
   Route::delete('/user_schedule/delete_workout/{user_id}/{workout_id}', ['uses' => 'UserController@deleteUserWorkout', 'as' => 'patient.deleteUserWorkout']);
   Route::get('/durations', ['uses' => 'UserController@getDurations', 'as' => 'patient.getDurations']);
   Route::get('/patient/user_health_diary', ['uses' => 'UserController@getUserHealthDiary', 'as' => 'patient.getUserHealthDiary']);
   Route::get('/patient/unassign_doctors/{id}', ['uses' => 'UserController@getUnassignDoctorsList', 'as' => 'doctor.getUnassignDoctorsList']);
   Route::post('/patient/assigned_doctor', ['uses' => 'UserController@assignedDoctor', 'as' => 'patient.assignedDoctor']);
   Route::delete('/patient/remove_doctor/{doctor_id}/{patient_id}', ['uses' => 'UserController@removeDoctor', 'as' => 'patient.removeDoctor']);

   //Route::get('/patient/switch_to_dashborad/{id}', ['uses' => 'UserController@getSwitchToDashboard']);
   Route::get('/patient/switch_to_admin', ['uses' => 'UserController@getSwitchToAdmin']);
   //workout controllers methods
   Route::get('/workout/list', ['uses' => 'WorkoutController@getWorkoutWithVideosList', 'as' => 'workout.getWorkoutWithVideosList']);
   Route::get('/workout/workout_types', ['uses' => 'WorkoutController@getWorkoutTypes', 'as' => 'workout.getWorkoutTypes']);
   Route::post('/workout/attached_video', ['uses' => 'WorkoutController@attachedVideo', 'as' => 'workout.attachedVideo']);
   Route::delete('/workout/delete_video/{workout_id}/{video_id}', ['uses' => 'WorkoutController@deleteVideo', 'as' => 'workout.deleteVideo']);

   Route::post('/workout/attached_exercise', ['uses' => 'WorkoutController@attachedExercise', 'as' => 'workout.attachedExercise']);
   Route::delete('/workout/delete_exercise/{workout_id}/{exercise_image_id}', ['uses' => 'WorkoutController@deleteExercise', 'as' => 'workout.deleteExercise']);

   //template controllers method
   Route::get('/template/list', ['uses' => 'TemplateController@getTemplateWithWorkout', 'as' => 'template.getTemplateWithWorkout']);
   Route::post('/template/attached_workout', ['uses' => 'TemplateController@attachedWorkout', 'as' => 'template.attachedWorkout']);
   Route::delete('/template/delete_workout/{template_id}/{workout_id}', ['uses' => 'TemplateController@deleteWorkout', 'as' => 'template.deleteWorkout']);
   Route::post('/template/updateWorkout', ['uses' => 'TemplateController@updateWorkout', 'as' => 'template.updateWorkout']);

   Route::post('/doctor/patients', ['uses' => 'UserController@assignPatientToDoctor', 'as' => 'doctor.assignPatientToDoctor']);
   Route::get('/doctor/nonassign_patients/{id}', ['uses' => 'UserController@getNonAssignPatients', 'as' => 'doctor.getNonAssignPatients']);
   Route::get('/doctor/assign_patients/{id}', ['uses' => 'UserController@getAssignPatients', 'as' => 'doctor.getAssignPatients']);
   Route::delete('/doctor/remove_assign_patient/{doctor_id}/{patient_id}', ['uses' => 'UserController@unassignPatientFromDoctor', 'as' => 'doctor.unassignPatientFromDoctor']);


   // front Goal API

   Route::put('/goals/update_calorie_goal/{user_id}','GoalsController@updateCaloireGoal');
   Route::put('/goals/update_water_goal/{user_id}','GoalsController@updateWaterGoal');
   Route::put('/goals/update_bm_goal/{user_id}','GoalsController@updateBodyMeasurementGoal');
   Route::put('/goals/update_weight_goal/{user_id}','GoalsController@updateWeightGoal');
   Route::put('/goals/update_bp_pulse_goal/{user_id}','GoalsController@updateBpGoal');
   Route::put('/goals/update_sleep_goal/{user_id}','GoalsController@updateSleepGoal');

   // front patient food
   Route::get('/food/patient_food/{user_id}','FoodController@patientFood');
   Route::put('/user_food_notes/update_note/{user_id}','FoodController@updateFoodNote');
   Route::get('/food/monthly_calorie_summary/{user_id}', 'FoodController@calculateMonthlyCalories' );
   Route::get('/food/all_logged_food/{user_id}', 'FoodController@getloggedFood');
   Route::get('/food/average_calories/{user_id}', 'FoodController@getAverageCalorie' );
   Route::post('/food/create_meal', 'FoodController@createMeal' );
   Route::get('/food/meals/{user_id}', 'FoodController@getMeals' );
   Route::get('/food/meal/{id}', 'FoodController@getMeal' );
   Route::post('/food/log_meal', 'FoodController@logMeal' );
   Route::delete('/food/delete_meal/{id}', 'FoodController@deleteMeal');
   Route::post('/food/log_recommended_foods', 'FoodController@logRecommendedFoods' );
   Route::get('/food/meal_id/{id}', 'RecommendedMealController@getMeal' );

   // front patient exercise
   Route::get('/exercise/patient_exercise/{user_id}','ExerciseController@patientExercise');
   Route::get('/exercise/user_logged_exercise/{user_id}','ExerciseController@userLoggedExercise');
   Route::get('/exercise/search_exercise','ExerciseController@searchExercise');
   Route::get('/exercise/all_logged_exercise/{user_id}/{exercise_type}', 'ExerciseController@allloggedExercise');
   Route::get('/exercise/monthly_calorie_summary/{user_id}', 'ExerciseController@calculateMonthlySummary' );
   Route::delete('/exercise/delete_exercise/{id}','ExerciseController@destroy');
   Route::put('/user_exercise_notes/update_note/{user_id}','ExerciseController@updateExerciseNote');

   Route::post('/meal/create_meal','RecommendedMealController@createNewMeal');
   Route::put('/meal/update_meal/{id}','RecommendedMealController@updateMeal');

   // front patient sleep
    Route::get('/sleep/user_sleep/{user_id}','SleepController@getUserSleep');
    Route::get('/sleep/weekly_sleep/{user_id}','SleepController@getWeeklySleep');

   Route::get('/patient/monthly_calories/{user_id}', 'UserController@calculateMonthlyCalories' );
   Route::get('/patient/average_calories/{user_id}', 'UserController@getAverageCalorie' );
   Route::get('/patient/monthly_bp_pulse_summary/{user_id}', 'BloodPressureController@calculateMonthlyBpPulsesummary');
   Route::get('/patient/average_bp_pulse/{user_id}', 'BloodPressureController@getAverageSystolicDystolicPulse' );

   //front patient report
   Route::get('/reports/display_charts/{user_id}','ReportController@getCharts');

   //front Dashboard
   Route::get('/dashboard/health_data/{user_id}','UserController@getDashboradHealthData');

   // front patient diary
   Route::put('/diary/update_diary_note/{user_id}','DiaryController@updateDiaryNote');
   Route::get('/diary/monthly_calorie_summary/{user_id}', 'DiaryController@calculateMonthlyCalories' );

   // iPhone webservices routes for Auth User
   Route::get('/user/profile/{id}', ['uses' => 'UserController@getUserProfile', 'as' => 'patient.getUserProfile']);
   Route::post('/food/log_food', 'FoodController@logFood');
   Route::get('/food/logged_food/{user_id}','FoodController@loggedFood');
   Route::get('/patient/calories/{user_id}',['uses' => 'UserController@calculateCalories', 'as' => 'patient.calculateCalories']);
   Route::get('/patient/resend_email/{user_id}','UserController@resendVerifiedEmail');
   Route::put('/patient/change_email/{user_id}','UserController@changeEmail');
   Route::get('/patient/check_status/{user_id}','UserController@checkStatus');
   Route::get('/patient/notification/{user_id}','UserController@getNotification');
   Route::post('/exercise/log_exercise', 'ExerciseController@logExercise');
   Route::get('/exercise/logged_exercise/{user_id}','ExerciseController@loggedExercise');
   Route::get('/exercise/track_exercise','ExerciseController@trackExercise');
   Route::get('/goals/patient_goal/{user_id}','UserController@getPatientGoal');
   Route::put('/goals/update_goal/{user_id}','UserController@updatePatientGoal');
   Route::post('/weight/log_weight', 'WeightController@logWeight');

   Route::put('/patient/update_mem_code/{user_id}', 'UserController@setMembershipCode');

   // pulse iPhone service
   Route::post('/pulse/log_pulse', 'BloodPressureController@logPulse');
   Route::get('/pulse/logged_pulse/{user_id}','BloodPressureController@loggedPulse');
   Route::delete('/pulse/delete/{id}', 'BloodPressureController@destroy');

   // bppulse iPhone service
   Route::post('/bppulse/log_bppulse', 'BloodPressureController@logBpPulse');
   Route::get('/bppulse/logged_bppulse/{user_id}','BloodPressureController@loggedBpPulse');
   Route::delete('/bppulse/delete/{id}', 'BloodPressureController@deleteBpPulse');

   Route::post('/referfriend/refere_friend', 'UserController@saveReferedFriend');
   Route::get('/referfriend/getRefered', 'UserController@getReferedFriend');
   Route::post('/challenge/create_challenge', 'ChallengeController@store');
   Route::get('/challenge/challengelist', 'ChallengeController@get_challengelist');
   Route::get('/challenge/challenges_front', 'ChallengeController@getChallengelistFront');
   Route::delete('challenge/delete/{id}', 'ChallengeController@destroy');
   Route::get('challenge/item_data/{challenge_id}', 'ChallengeController@edit' );
   Route::get('/allowed_device/allowedevicelist', 'ChallengeController@get_alloweddevicelist');
   Route::put('/challenge/update/{id}','ChallengeController@updateChallenge');
   Route::get('/challenge/challenges_with_participant/{user_id}', 'ChallengeController@challengeListWithParticipant');

   Route::post('/challenge_image/upload', 'ChallengeController@uploadImage');
   Route::get('/challenge_image/remove/{file_name}', 'ChallengeController@removeImage');


  Route::post('/workout_image/upload', 'WorkoutController@uploadImage');
  Route::get('/workout_image/remove/{file_name}', 'WorkoutController@removeImage');


  Route::post('/template_image/upload', 'TemplateController@uploadImage');
  Route::get('/template_image/remove/{file_name}', 'TemplateController@removeImage');


  Route::get('/social_media/get_social_media', 'SocialMediaController@getSocialMedia');
  Route::post('/social_media/update_social_media', 'SocialMediaController@updateSocialMedia');

   // upc iPhone service
   Route::post('/upc/log_upc', 'FoodController@logUpc');
   Route::get('/upc/logged_upc','FoodController@loggedUpc');

   Route::post('/sleep/log_sleep', 'SleepController@logSleep');
   Route::get('/sleep/logged_sleep/{user_id}','SleepController@loggedSleep');
   Route::delete('/sleep/delete_sleep/{id}', 'SleepController@destroy');

   //Manage Food
   Route::get('/manage_food/edit_data/{food_id}', 'ManageFoodController@edit' );
   Route::put('/manage_food/update/{id}','ManageFoodController@update');
   Route::delete('/manage_food/delete/{id}', 'ManageFoodController@destroy');

   Route::get('/weight/logged_weight/{user_id}','WeightController@loggedWeight');
   Route::put('/weight/update_weight/{id}','WeightController@update');
   Route::get('/weight/logged_body_measurements/{user_id}','WeightController@getLoggedBodyMeasurements');
   Route::delete('/body_measurements/delete/{id}', 'WeightController@delete_body_measurements');

   Route::post('/weight/log_body_measurements', 'WeightController@logBodyMeasurements');
   Route::delete('/weight/delete/{id}', 'WeightController@destroy');
   Route::get('/food/nutritions_by_upc','FoodController@getNutritionsByUpc');
   Route::post('/food/log_fav_food', 'FoodController@logFavouriteFood');
   Route::get('/food/favourite_food/{user_id}','FoodController@getFavouriteFood');
   Route::delete('/food/delete_favourite_food/{id}', 'FoodController@deleteFavouriteFood');
   Route::delete('/food/delete_favourite_food_by_foodid/{food_id}', 'FoodController@deleteFavouriteFoodByFoodId');

   Route::post('/food/add_custom_food', 'FoodController@addCustomNutrition');
   Route::get('/food/custom_food/{user_id}','FoodController@getCustomFood');
   Route::post('/water/log_water','UserController@saveWaterintake');
   Route::get('/water/logged_water/{user_id}','UserController@loggedWater');
   Route::delete('/water/delete/{id}', 'UserController@deleteWater');
   Route::delete('/water/delete/{user_id}/{date}', 'FoodController@deleteWater');
   Route::post('/user/add_custom_water','UserController@saveCustomWater');
   Route::get('/user/get_custom_water/{user_id}','UserController@loggedCustomWater');
   Route::delete('/user/delete_custom_water/{id}', 'UserController@deleteCustomWater');

   Route::post('/food/log_food_completed','FoodController@saveFoodCompleted');

   Route::put('/food/update_custom_food/{id}','FoodController@updateCustomNutrition');
   Route::delete('/food/delete_custom_food/{id}','FoodController@deleteCustomNutrition');
   Route::delete('/food/delete_logged_food/{id}','FoodController@deleteLoggedNutrition');

   //Route::post('admin/add_custom_food', 'UserController@addCustomFood');
   // Md Recommended foods services
   Route::post('recommended_food/create', 'FoodController@createMdRecommendedFood' );
   Route::get('recommended_food/types', 'FoodController@getFoodRecommendedTypes');
   Route::get('recommended_food/foods/{user_id}', 'FoodController@getMdRecommendedFoods');
   Route::get('recommended_food/display', 'FoodController@mdRecommendedFoodsDisplayData');
   Route::delete('recommended_food/delete/{id}', 'FoodController@deleteMdRecommendedFood');
   Route::get('recommended_food/edit/{id}', 'FoodController@getNutrition' );
   Route::put('recommended_food/update/{update_id}', 'FoodController@updateRecommendedFood' );
   Route::get('recommended_food/all_types_foods/{user_id}', 'FoodController@getMdRecommendedFoodsFront');

   // end Md Recommended foods services

   // services for iphone apps
   Route::get('recommended_food/all_types', 'FoodController@foodRecommendedTypes');
   Route::get('recommended_food/meals/{user_id}', 'FoodController@recommendedMeals');
   // end: services for iphone apps

   // Md Recommended Meals services
   Route::post('recommended_meal/create_meal', 'RecommendedMealController@createRecommendedMeal' );
   Route::get('recommended_meal/meals', 'RecommendedMealController@getRecommendedMeals' );
   Route::get('recommended_meal/meals/{id}', 'RecommendedMealController@getRecommendedMeals' );
   Route::get('recommended_meal/item_data/{meal_id}', 'RecommendedMealController@recommendedMealItemData' );
   Route::post('recommended_meal/create_meal_food', 'RecommendedMealController@createRecommendedMealFood' );
   Route::get('recommended_meal/items', 'RecommendedMealController@getRecommendedMealItems');
   Route::delete('recommended_meal/delete/{id}', 'RecommendedMealController@deleteRecommendedMeal');
   Route::delete('recommended_meal/food_delete/{id}', 'RecommendedMealController@deleteRecommendedMealFood');
   Route::put('recommended_meal/update_meal_food/{update_id}', 'RecommendedMealController@updateRecommendedMealFood' );
   Route::get('recommended_meal/template_names', 'RecommendedMealController@getTemplateNames' );
   Route::get('recommended_meal/template/{id}', 'RecommendedMealController@getTemplate' );
   Route::post('recommended_meal/create_template_name', 'RecommendedMealController@createTemplateName' );
   Route::put('recommended_meal/set_default_template/{id}', 'RecommendedMealController@setDefaultTemplate' );
   Route::put('recommended_meal/update_meal_template/{id}', 'RecommendedMealController@updateMealTemplate' );
   Route::delete('recommended_meal/delete_meal_template/{id}', 'RecommendedMealController@deleteMealTemplate');
   Route::get('recommended_meal/food_type', 'RecommendedMealController@getRecommendedTypeFood' );
    //user side
   Route::get('recommended_meal/foods/{meal_id}', 'FoodController@getRecommendedMealFoods');
   Route::get('recommended_meal/meals_user/{user_id}', 'FoodController@getMealsByUser');
   Route::post('recommended_meal/save_meal_foods', 'RecommendedMealController@saveRecommendedMealFood');

    //end : user side

   // end Md Recommended Meals services

   // MD Meals
    Route::get('md_meals/meals', 'RecommendedMealController@getMDMeals' );

   // Classes SChedule services
   Route::post('/class_schedules/create_class', 'ClassScheduleController@createClass' );

   // end: Classes SChedule services

   Route::get('/dynamodb/search/','FoodController@searchDynamodb');

   // Exercise Image
    Route::post('/exercise_image/upload', 'ExerciseImageController@uploadImage');
    Route::get('/exercise_image/remove/{file_name}', 'ExerciseImageController@removeImage');
    Route::get('/exercise_image/list', 'ExerciseImageController@getAllImages');

    // deal routes
    Route::post('/deal_image/upload', 'DealController@uploadImage');
    Route::get('/deal_image/remove/{file_name}', 'DealController@removeImage');
    Route::post('/deal/create_deal', 'DealController@store');
    Route::get('/deal/getlist', 'DealController@getDealList');
    Route::get('/deal/getlist_front', 'DealController@getDealListFront');
    Route::get('/deal/get_deal/{edit_id}', 'DealController@show');
    Route::put('/deal/update_deal/{edit_id}', 'DealController@update');
    Route::delete('/deal/delete/{id}', 'DealController@delete');

    //User Workout Rotuine
    Route::post('/workout/save_user_workout_routine', 'WorkoutController@saveUserWorkout' );
    Route::post('/workout/save_user_routine', 'WorkoutController@saveUserRoutine' );
    Route::get('/workout/get_user_workout_routines/{user_id}', 'WorkoutController@getUserWorkout');
    Route::post('/workout/log_user_workout_routine', 'WorkoutController@logUserWorkoutRoutine');
    Route::get('/workout/logged_user_workout_routines/{user_id}', 'WorkoutController@loggedUserWorkedRoutines');
    Route::delete('/workout/delete_user_workout_routine/{id}', 'WorkoutController@deleteUserWorkoutRoutine');
    Route::delete('/workout/delete_user_workout/{id}', 'WorkoutController@deleteUserWorkout');
    Route::get('/workout/display_user_workout_routines/{user_id}', 'WorkoutController@getUserAllWorkouts');
    Route::get('/workout/user_routines_templates/{user_id}', 'WorkoutController@getUserRoutinesTemplates');
    Route::delete('/workout/clear_all_user_workouts/{user_id}', 'UserWorkoutController@clearAllMyWorkouts');
    Route::put('/workout/remove_myworkout/{user_id}', 'UserWorkoutController@removeMyWorkout');

    //Route::get('/workout/body_parts', 'WorkoutController@getBodyParts');

    Route::post('/events/update_settings', 'ClassScheduleController@updateEventSetting');
    Route::get('/events/settings', 'ClassScheduleController@getEventSettings');
    Route::get('/events/active_settings', 'ClassScheduleController@getActiveSettings');
    Route::post('/events/upload', 'ClassScheduleController@uploadImage');
    Route::get('/events/remove/{file_name}', 'ClassScheduleController@removeImage');

    // organizations
   Route::post('/organization/upload', 'OrganizationController@uploadLogo');
   Route::get('/organization/remove/{file_name}','OrganizationController@removeLogo');
   Route::post('/organization/checksubDomain', 'OrganizationController@checksubDomain');
   Route::get('/organization/list','OrganizationController@getOrganizationList');
   Route::delete('/organization/delete/{id}','OrganizationController@destroy');

   // Challenges
   Route::post('/challenge/challenge_participant', 'ChallengeController@challengeParticipant');
   Route::post('/challenge/user_challenge_data', 'ChallengeController@userChallengeData');
   Route::get('/challenge/get_challenge_detail/{challenge_id}', 'ChallengeController@getChallengeDetail');
   Route::delete('/challenge/remove_challenge_data/{id}', 'ChallengeController@removeUserChallengeData');

   // Assign Plan
   Route::get('/template/get_nutrition_template','AssignPlanController@getNutritionTemplate');
   Route::get('/template/get_meals_template','AssignPlanController@geMealsTemplate');
   Route::get('/template/get_workouts_template','AssignPlanController@geWorkoutsTemplate');
   Route::post('/template/assign_patient', 'AssignPlanController@assignedPlan');
   Route::get('/assign_plan/get_user_plan/{user_id}','AssignPlanController@getUserMealPlan');


  // My Plan

   Route::get('/my_plan/get_nutrition_template','MyPlanController@getNutritionTemplate');
   Route::get('/my_plan/get_master_templates','MyPlanController@geMasterTemplate');
   Route::get('/my_plan/get_workouts_template','MyPlanController@geWorkoutsTemplate');
   Route::get('/my_plan/get_meals_template','MyPlanController@geMealsTemplate');
   //for iPhone
   Route::get('/my_plan/user_template_data/{user_id}','MyPlanController@geUserTemplateData');

   // Meal Plan
   Route::get('/meal_plan/get_meal_plans', 'MealPlansController@getMealPlans');
   Route::post('/meal_plan/create_meal_plan', 'MealPlansController@saveMealPlan');
   Route::delete('/meal_plan/delete/{id}','MealPlansController@delete');
   Route::put('/meal_plan/update_meal_plan/{id}','MealPlansController@updateMealPlan');
   Route::put('/meal_plan/update_user_meal_plan/{id}','MealPlansController@updateUserMealPlanDetail');
   Route::post('/meal_plan/assign_plan_to_user','MealPlansController@assignPlanToUser');
   Route::get('/meal_plan/new_user_plan/{user_id}','MealPlansController@newUserMealPlan');
   Route::get('/meal_plan/get_user_plan/{user_id}','MealPlansController@getUserMealPlan');
   Route::post('/meal_plan/unassign_plan_to_user','MealPlansController@unassignPlanToUser');
   Route::put('/meal_plan/update_plan/{id}','MealPlansController@updatePlan');
   Route::put('/meal_plan/update_plan_config/{id}','MealPlansController@updatePlanConfig');
   Route::put('/meal_plan/update_day_plan/{id}','MealPlansController@updateDayPlan');

   Route::put('/meal_plan/user_reschedule_plan/{user_id}','MealPlansController@updateUserMealPlan');

   // for server side service
   Route::get('/meal_plan/get_mplan_details/{id}','MealPlansController@getMealPlanDetails');
   //get user meal details for assign plan
   Route::get('/meal_plan/get_user_mplan_details/{id}','MealPlansController@getUserMealPlanDetails');

    // for app side service
   Route::get('/meal_plan/get_plan_details/{id}','MealPlansController@getMealPlanAppDetails');

    // Push Notification
    Route::post('/user/save_user_device_data', 'UserDeviceController@saveUserDevice');
    Route::get('/user/send_push_notification/{user_id}', 'UserDeviceController@sendPushNotification');
    Route::post('/user/logout', 'UserDeviceController@removeUserApnDevice');
    Route::put('/user/save_time_zone/{user_id}','UserController@saveTimeZone');
    Route::get('/user/get_push_notification/{user_id}', 'UserDeviceController@getUserPushNotification');
    Route::delete('/user/delete_push_notification/{id}','UserDeviceController@deleteUserPushNotification');
    Route::put('/user/update_user_notification/{id}','UserDeviceController@updateUserPushNotification');
    Route::resource('/food','FoodController');
    Route::resource('/exercise','ExerciseController');
    Route::resource('/patient','UserController');
    Route::resource('/doctor','UserController');

    Route::resource('/workout','WorkoutController');
    Route::resource('/template','TemplateController');
    Route::resource('/goals','GoalsController');
    Route::resource('/exercise_image','ExerciseImageController');
    Route::resource('/user_step','UserStepController');
    //Route::post('/patient/updateProfile', 'UserController@updateUserProfile');
});

Route::prefix('/v3')->group(function() {
    Route::post('/patient/store', 'UserController@register');
    Route::post('/patient/login', 'UserController@login');
    Route::get('/activities', ['uses' => 'UserController@getActivity', 'as' => 'patient.getActivity']);
    Route::post('/patient/updateProfile', 'UserController@updateUserProfile');
    Route::get('/user/email_address/{token}','UserController@getEmail');
    Route::post('/auth/login', 'Auth\AuthController@authLogin');
    //Route::controller('password', 'Auth\PasswordController');
    Route::post('/password/email', 'Auth\PasswordController@postEmail');
    Route::post('/password/reset', 'Auth\PasswordController@postReset');
    Route::post('/user/change_password', 'Auth\PasswordController@changedPassword');
    Route::post('/user/signup_steps', 'UserController@SignupSteps');
});

Route::group(['prefix' => '/v3','middleware'=>'apiAuth'], function () {
   Route::post('/users/register', 'UserController@auth0Register');
   Route::post('/users/forgot_password', 'Auth\PasswordController@sendResetLink');
   Route::post('/live_users/register', 'UserController@auth0liveRegister');
   Route::post('/users/check_exists', 'UserController@checkExists');
   Route::post('/users/change_password', 'UserController@auth0ChangePassword');
});

//Prefix V1 for iphone
//Route::group(['prefix' => '/v1','middleware'=>['auth:admin']], function () {
Route::prefix('/v1')->group(function() {
   Route::get('/video/body_parts', ['uses' => 'VideoController@getBodyParts', 'as' => 'video.getBodyParts']);
   Route::get('/video/equipments', ['uses' => 'VideoController@getEquipments', 'as' => 'video.getEquipments']);
   Route::post('/upload', 'UserController@upload');
   Route::get('/removephoto/{file_name}','UserController@remove');
   Route::get('/patient/medicines/{med_name}', ['uses' => 'UserController@medicines', 'as' => 'patient.medicines']);
   Route::post('/patient/vitalParameters', ['uses' => 'UserController@saveVitalParameters', 'as' => 'patient.saveVitalParameters']);
   Route::post('/patient/profile', ['uses' => 'UserController@saveProfile', 'as' => 'patient.saveProfile']);
   Route::get('/activities', ['uses' => 'UserController@getActivity', 'as' => 'patient.getActivity']);
   Route::get('/patient/list', ['uses' => 'UserController@getList', 'as' => 'patient.getList']);
   Route::get('/doctor/list', ['uses' => 'UserController@getList', 'as' => 'doctor.getList']);
   Route::get('/patient/vitals/{id}', ['uses' => 'UserController@getVitals', 'as' => 'patient.getVitals']);
   Route::get('/patient/profile/{id}', ['uses' => 'UserController@getProfile', 'as' => 'patient.getProfile']);
   Route::get('/patient/demographics/{id}', ['uses' => 'UserController@getUser', 'as' => 'patient.getUser']);
   Route::get('/patient/weeklygoals', ['uses' => 'UserController@getWeeklygoals','as' => 'goals.getWeeklygoals']);
   Route::get('/patient/waterlog/{id}', ['uses' => 'UserController@getwaterlog','as' => 'front.getwaterlog']);
   Route::post('/patient/water_intake', ['uses' => 'UserController@saveWaterintake', 'as' => 'patient.saveWaterintake']);
   Route::get('/doctor/demographics/{id}', ['uses' => 'UserController@getUser', 'as' => 'doctor.getUser']);
   Route::get('/doctor_titles', ['uses' => 'UserController@getDoctorTitles', 'as' => 'doctor.getDoctorTitles']);

   Route::get('/getReferedFriend', ['uses' => 'UserController@getReferedFriend']);


   Route::get('/patient/routine_times/{id}', ['uses' => 'UserController@getUserRoutinetimes', 'as' => 'patient.getRoutinetimes']);
   Route::post('/patient/medicines', ['uses' => 'UserController@saveMedication', 'as' => 'patient.saveMedication']);
   Route::get('/patient/user_medicine_list/{id}', ['uses' => 'UserController@getUserMedicinelist', 'as' => 'patient.getUserMedicinelist']);
   Route::delete('/patient/user_medicine_delete/{user_id}/{medicine_id}', ['uses' => 'UserController@deleteMedicine', 'as' => 'patient.deleteMedicine']);

   Route::get('/patient/user_schedule/{id}', ['uses' => 'UserController@getUserSchedule', 'as' => 'patient.getUserSchedule']);

   Route::post('/user_schedule/attached_template', ['uses' => 'UserController@attachedTemplate', 'as' => 'patient.attachedTemplate']);
   Route::delete('/user_schedule/delete_workout/{user_id}/{workout_id}', ['uses' => 'UserController@deleteUserWorkout', 'as' => 'patient.deleteUserWorkout']);
   Route::get('/durations', ['uses' => 'UserController@getDurations', 'as' => 'patient.getDurations']);
   Route::get('/patient/user_health_diary', ['uses' => 'UserController@getUserHealthDiary', 'as' => 'patient.getUserHealthDiary']);
   Route::get('/patient/unassign_doctors/{id}', ['uses' => 'UserController@getUnassignDoctorsList', 'as' => 'doctor.getUnassignDoctorsList']);
   Route::post('/patient/assigned_doctor', ['uses' => 'UserController@assignedDoctor', 'as' => 'patient.assignedDoctor']);
   Route::delete('/patient/remove_doctor/{doctor_id}/{patient_id}', ['uses' => 'UserController@removeDoctor', 'as' => 'patient.removeDoctor']);

   //Route::get('/patient/switch_to_dashborad/{id}', ['uses' => 'UserController@getSwitchToDashboard']);
   Route::get('/patient/switch_to_admin', ['uses' => 'UserController@getSwitchToAdmin']);
   //workout controllers methods
   Route::get('/workout/list', ['uses' => 'WorkoutController@getWorkoutWithVideosList', 'as' => 'workout.getWorkoutWithVideosList']);
   Route::get('/workout/workout_types', ['uses' => 'WorkoutController@getWorkoutTypes', 'as' => 'workout.getWorkoutTypes']);
   Route::post('/workout/attached_video', ['uses' => 'WorkoutController@attachedVideo', 'as' => 'workout.attachedVideo']);
   Route::delete('/workout/delete_video/{workout_id}/{video_id}', ['uses' => 'WorkoutController@deleteVideo', 'as' => 'workout.deleteVideo']);

   Route::post('/workout/attached_exercise', ['uses' => 'WorkoutController@attachedExercise', 'as' => 'workout.attachedExercise']);
   Route::delete('/workout/delete_exercise/{workout_id}/{exercise_image_id}', ['uses' => 'WorkoutController@deleteExercise', 'as' => 'workout.deleteExercise']);

   //template controllers method
   Route::get('/template/list', ['uses' => 'TemplateController@getTemplateWithWorkout', 'as' => 'template.getTemplateWithWorkout']);
   Route::post('/template/attached_workout', ['uses' => 'TemplateController@attachedWorkout', 'as' => 'template.attachedWorkout']);
   Route::delete('/template/delete_workout/{template_id}/{workout_id}', ['uses' => 'TemplateController@deleteWorkout', 'as' => 'template.deleteWorkout']);
   Route::post('/template/updateWorkout', ['uses' => 'TemplateController@updateWorkout', 'as' => 'template.updateWorkout']);

   Route::post('/doctor/patients', ['uses' => 'UserController@assignPatientToDoctor', 'as' => 'doctor.assignPatientToDoctor']);
   Route::get('/doctor/nonassign_patients/{id}', ['uses' => 'UserController@getNonAssignPatients', 'as' => 'doctor.getNonAssignPatients']);
   Route::get('/doctor/assign_patients/{id}', ['uses' => 'UserController@getAssignPatients', 'as' => 'doctor.getAssignPatients']);
   Route::delete('/doctor/remove_assign_patient/{doctor_id}/{patient_id}', ['uses' => 'UserController@unassignPatientFromDoctor', 'as' => 'doctor.unassignPatientFromDoctor']);


   // front Goal API

   Route::put('/goals/update_calorie_goal/{user_id}','GoalsController@updateCaloireGoal');
   Route::put('/goals/update_water_goal/{user_id}','GoalsController@updateWaterGoal');
   Route::put('/goals/update_bm_goal/{user_id}','GoalsController@updateBodyMeasurementGoal');
   Route::put('/goals/update_weight_goal/{user_id}','GoalsController@updateWeightGoal');
   Route::put('/goals/update_bp_pulse_goal/{user_id}','GoalsController@updateBpGoal');
   Route::put('/goals/update_sleep_goal/{user_id}','GoalsController@updateSleepGoal');

   // front patient food
   Route::get('/food/patient_food/{user_id}','FoodController@patientFood');
   Route::put('/user_food_notes/update_note/{user_id}','FoodController@updateFoodNote');
   Route::get('/food/monthly_calorie_summary/{user_id}', 'FoodController@calculateMonthlyCalories' );
   Route::get('/food/all_logged_food/{user_id}', 'FoodController@getloggedFood');
   Route::get('/food/average_calories/{user_id}', 'FoodController@getAverageCalorie' );
   Route::post('/food/create_meal', 'FoodController@createMeal' );
   Route::get('/food/meals/{user_id}', 'FoodController@getMeals' );
   Route::get('/food/meal/{id}', 'FoodController@getMeal' );
   Route::post('/food/log_meal', 'FoodController@logMeal' );
   Route::delete('/food/delete_meal/{id}', 'FoodController@deleteMeal');
   Route::post('/food/log_recommended_foods', 'FoodController@logRecommendedFoods' );
   Route::get('/food/meal_id/{id}', 'RecommendedMealController@getMeal' );

   // front patient exercise
   Route::get('/exercise/patient_exercise/{user_id}','ExerciseController@patientExercise');
   Route::get('/exercise/user_logged_exercise/{user_id}','ExerciseController@userLoggedExercise');
   Route::get('/exercise/search_exercise','ExerciseController@searchExercise');
   Route::get('/exercise/all_logged_exercise/{user_id}/{exercise_type}', 'ExerciseController@allloggedExercise');
   Route::get('/exercise/monthly_calorie_summary/{user_id}', 'ExerciseController@calculateMonthlySummary' );
   Route::delete('/exercise/delete_exercise/{id}','ExerciseController@destroy');
   Route::put('/user_exercise_notes/update_note/{user_id}','ExerciseController@updateExerciseNote');

   Route::post('/meal/create_meal','RecommendedMealController@createNewMeal');
   Route::put('/meal/update_meal/{id}','RecommendedMealController@updateMeal');

   // front patient sleep
    Route::get('/sleep/user_sleep/{user_id}','SleepController@getUserSleep');
    Route::get('/sleep/weekly_sleep/{user_id}','SleepController@getWeeklySleep');

   Route::get('/patient/monthly_calories/{user_id}', 'UserController@calculateMonthlyCalories' );
   Route::get('/patient/average_calories/{user_id}', 'UserController@getAverageCalorie' );
   Route::get('/patient/monthly_bp_pulse_summary/{user_id}', 'BloodPressureController@calculateMonthlyBpPulsesummary');
   Route::get('/patient/average_bp_pulse/{user_id}', 'BloodPressureController@getAverageSystolicDystolicPulse' );

   //front patient report
   Route::get('/reports/display_charts/{user_id}','ReportController@getCharts');

   //front Dashboard
   Route::get('/dashboard/health_data/{user_id}','UserController@getDashboradHealthData');

   // front patient diary
   Route::put('/diary/update_diary_note/{user_id}','DiaryController@updateDiaryNote');
   Route::get('/diary/monthly_calorie_summary/{user_id}', 'DiaryController@calculateMonthlyCalories' );

   // iPhone webservices routes for Auth User
   Route::get('/user/profile/{id}', ['uses' => 'UserController@getUserProfile', 'as' => 'patient.getUserProfile']);
   Route::post('/food/log_food', 'FoodController@logFood');
   Route::get('/food/logged_food/{user_id}','FoodController@loggedFood');
   Route::get('/patient/calories/{user_id}',['uses' => 'UserController@calculateCalories', 'as' => 'patient.calculateCalories']);
   Route::get('/patient/resend_email/{user_id}','UserController@resendVerifiedEmail');
   Route::put('/patient/change_email/{user_id}','UserController@changeEmail');
   Route::get('/patient/check_status/{user_id}','UserController@checkStatus');
   Route::get('/patient/notification/{user_id}','UserController@getNotification');
   Route::post('/exercise/log_exercise', 'ExerciseController@logExercise');
   Route::get('/exercise/logged_exercise/{user_id}','ExerciseController@loggedExercise');
   Route::get('/exercise/track_exercise','ExerciseController@trackExercise');
   Route::get('/goals/patient_goal/{user_id}','UserController@getPatientGoal');
   Route::put('/goals/update_goal/{user_id}','UserController@updatePatientGoal');
   Route::post('/weight/log_weight', 'WeightController@logWeight');

   Route::put('/patient/update_mem_code/{user_id}', 'UserController@setMembershipCode');

   // pulse iPhone service
   Route::post('/pulse/log_pulse', 'BloodPressureController@logPulse');
   Route::get('/pulse/logged_pulse/{user_id}','BloodPressureController@loggedPulse');
   Route::delete('/pulse/delete/{id}', 'BloodPressureController@destroy');

   // bppulse iPhone service
   Route::post('/bppulse/log_bppulse', 'BloodPressureController@logBpPulse');
   Route::get('/bppulse/logged_bppulse/{user_id}','BloodPressureController@loggedBpPulse');
   Route::delete('/bppulse/delete/{id}', 'BloodPressureController@deleteBpPulse');

   Route::post('/referfriend/refere_friend', 'UserController@saveReferedFriend');
   Route::get('/referfriend/getRefered', 'UserController@getReferedFriend');
   Route::post('/challenge/create_challenge', 'ChallengeController@store');
   Route::get('/challenge/challengelist', 'ChallengeController@get_challengelist');
   Route::get('/challenge/challenges_front', 'ChallengeController@getChallengelistFront');
   Route::delete('challenge/delete/{id}', 'ChallengeController@destroy');
   Route::get('challenge/item_data/{challenge_id}', 'ChallengeController@edit' );
   Route::get('/allowed_device/allowedevicelist', 'ChallengeController@get_alloweddevicelist');
   Route::put('/challenge/update/{id}','ChallengeController@updateChallenge');
   Route::get('/challenge/challenges_with_participant/{user_id}', 'ChallengeController@challengeListWithParticipant');

   Route::post('/challenge_image/upload', 'ChallengeController@uploadImage');
   Route::get('/challenge_image/remove/{file_name}', 'ChallengeController@removeImage');


  Route::post('/workout_image/upload', 'WorkoutController@uploadImage');
  Route::get('/workout_image/remove/{file_name}', 'WorkoutController@removeImage');


  Route::post('/template_image/upload', 'TemplateController@uploadImage');
  Route::get('/template_image/remove/{file_name}', 'TemplateController@removeImage');


  Route::get('/social_media/get_social_media', 'SocialMediaController@getSocialMedia');
  Route::post('/social_media/update_social_media', 'SocialMediaController@updateSocialMedia');

   // upc iPhone service
   Route::post('/upc/log_upc', 'FoodController@logUpc');
   Route::get('/upc/logged_upc','FoodController@loggedUpc');

   Route::post('/sleep/log_sleep', 'SleepController@logSleep');
   Route::get('/sleep/logged_sleep/{user_id}','SleepController@loggedSleep');
   Route::delete('/sleep/delete_sleep/{id}', 'SleepController@destroy');

   //Manage Food
   Route::get('/manage_food/edit_data/{food_id}', 'ManageFoodController@edit' );
   Route::put('/manage_food/update/{id}','ManageFoodController@update');
   Route::delete('/manage_food/delete/{id}', 'ManageFoodController@destroy');

   Route::get('/weight/logged_weight/{user_id}','WeightController@loggedWeight');
   Route::put('/weight/update_weight/{id}','WeightController@update');
   Route::get('/weight/logged_body_measurements/{user_id}','WeightController@getLoggedBodyMeasurements');
   Route::delete('/body_measurements/delete/{id}', 'WeightController@delete_body_measurements');

   Route::post('/weight/log_body_measurements', 'WeightController@logBodyMeasurements');
   Route::delete('/weight/delete/{id}', 'WeightController@destroy');
   Route::get('/food/nutritions_by_upc','FoodController@getNutritionsByUpc');
   Route::post('/food/log_fav_food', 'FoodController@logFavouriteFood');
   Route::get('/food/favourite_food/{user_id}','FoodController@getFavouriteFood');
   Route::delete('/food/delete_favourite_food/{id}', 'FoodController@deleteFavouriteFood');
   Route::delete('/food/delete_favourite_food_by_foodid/{food_id}', 'FoodController@deleteFavouriteFoodByFoodId');

   Route::post('/food/add_custom_food', 'FoodController@addCustomNutrition');
   Route::get('/food/custom_food/{user_id}','FoodController@getCustomFood');
   Route::post('/water/log_water','UserController@saveWaterintake');
   Route::get('/water/logged_water/{user_id}','UserController@loggedWater');
   Route::delete('/water/delete/{id}', 'UserController@deleteWater');
   Route::delete('/water/delete/{user_id}/{date}', 'FoodController@deleteWater');
   Route::post('/user/add_custom_water','UserController@saveCustomWater');
   Route::get('/user/get_custom_water/{user_id}','UserController@loggedCustomWater');
   Route::delete('/user/delete_custom_water/{id}', 'UserController@deleteCustomWater');

   Route::post('/food/log_food_completed','FoodController@saveFoodCompleted');

   Route::put('/food/update_custom_food/{id}','FoodController@updateCustomNutrition');
   Route::delete('/food/delete_custom_food/{id}','FoodController@deleteCustomNutrition');
   Route::delete('/food/delete_logged_food/{id}','FoodController@deleteLoggedNutrition');

   //Route::post('admin/add_custom_food', 'UserController@addCustomFood');
   // Md Recommended foods services
   Route::post('recommended_food/create', 'FoodController@createMdRecommendedFood' );
   Route::get('recommended_food/types', 'FoodController@getFoodRecommendedTypes');
   Route::get('recommended_food/foods/{user_id}', 'FoodController@getMdRecommendedFoods');
   Route::get('recommended_food/display', 'FoodController@mdRecommendedFoodsDisplayData');
   Route::delete('recommended_food/delete/{id}', 'FoodController@deleteMdRecommendedFood');
   Route::get('recommended_food/edit/{id}', 'FoodController@getNutrition' );
   Route::put('recommended_food/update/{update_id}', 'FoodController@updateRecommendedFood' );
   Route::get('recommended_food/all_types_foods/{user_id}', 'FoodController@getMdRecommendedFoodsFront');

   // end Md Recommended foods services

   // services for iphone apps
   Route::get('recommended_food/all_types', 'FoodController@foodRecommendedTypes');
   Route::get('recommended_food/meals/{user_id}', 'FoodController@recommendedMeals');
   // end: services for iphone apps

   // Md Recommended Meals services
   Route::post('recommended_meal/create_meal', 'RecommendedMealController@createRecommendedMeal' );
   Route::get('recommended_meal/meals', 'RecommendedMealController@getRecommendedMeals' );
   Route::get('recommended_meal/meals/{id}', 'RecommendedMealController@getRecommendedMeals' );
   Route::get('recommended_meal/item_data/{meal_id}', 'RecommendedMealController@recommendedMealItemData' );
   Route::post('recommended_meal/create_meal_food', 'RecommendedMealController@createRecommendedMealFood' );
   Route::get('recommended_meal/items', 'RecommendedMealController@getRecommendedMealItems');
   Route::delete('recommended_meal/delete/{id}', 'RecommendedMealController@deleteRecommendedMeal');
   Route::delete('recommended_meal/food_delete/{id}', 'RecommendedMealController@deleteRecommendedMealFood');
   Route::put('recommended_meal/update_meal_food/{update_id}', 'RecommendedMealController@updateRecommendedMealFood' );
   Route::get('recommended_meal/template_names', 'RecommendedMealController@getTemplateNames' );
   Route::get('recommended_meal/template/{id}', 'RecommendedMealController@getTemplate' );
   Route::post('recommended_meal/create_template_name', 'RecommendedMealController@createTemplateName' );
   Route::put('recommended_meal/set_default_template/{id}', 'RecommendedMealController@setDefaultTemplate' );
   Route::put('recommended_meal/update_meal_template/{id}', 'RecommendedMealController@updateMealTemplate' );
   Route::delete('recommended_meal/delete_meal_template/{id}', 'RecommendedMealController@deleteMealTemplate');
   Route::get('recommended_meal/food_type', 'RecommendedMealController@getRecommendedTypeFood' );
    //user side
   Route::get('recommended_meal/foods/{meal_id}', 'FoodController@getRecommendedMealFoods');
   Route::get('recommended_meal/meals_user/{user_id}', 'FoodController@getMealsByUser');
   Route::post('recommended_meal/save_meal_foods', 'RecommendedMealController@saveRecommendedMealFood');

    //end : user side

   // end Md Recommended Meals services

   // MD Meals
    Route::get('md_meals/meals', 'RecommendedMealController@getMDMeals' );

   // Classes SChedule services
   Route::post('/class_schedules/create_class', 'ClassScheduleController@createClass' );

   // end: Classes SChedule services

   Route::get('/dynamodb/search/','FoodController@searchDynamodb');

   // Exercise Image
    Route::post('/exercise_image/upload', 'ExerciseImageController@uploadImage');
    Route::get('/exercise_image/remove/{file_name}', 'ExerciseImageController@removeImage');
    Route::get('/exercise_image/list', 'ExerciseImageController@getAllImages');

    // deal routes
    Route::post('/deal_image/upload', 'DealController@uploadImage');
    Route::get('/deal_image/remove/{file_name}', 'DealController@removeImage');
    Route::post('/deal/create_deal', 'DealController@store');
    Route::get('/deal/getlist', 'DealController@getDealList');
    Route::get('/deal/getlist_front', 'DealController@getDealListFront');
    Route::get('/deal/get_deal/{edit_id}', 'DealController@show');
    Route::put('/deal/update_deal/{edit_id}', 'DealController@update');
    Route::delete('/deal/delete/{id}', 'DealController@delete');

    //User Workout Rotuine
    Route::post('/workout/save_user_workout_routine', 'WorkoutController@saveUserWorkout' );
    Route::post('/workout/save_user_routine', 'WorkoutController@saveUserRoutine' );
    Route::get('/workout/get_user_workout_routines/{user_id}', 'WorkoutController@getUserWorkout');
    Route::post('/workout/log_user_workout_routine', 'WorkoutController@logUserWorkoutRoutine');
    Route::get('/workout/logged_user_workout_routines/{user_id}', 'WorkoutController@loggedUserWorkedRoutines');
    Route::delete('/workout/delete_user_workout_routine/{id}', 'WorkoutController@deleteUserWorkoutRoutine');
    Route::delete('/workout/delete_user_workout/{id}', 'WorkoutController@deleteUserWorkout');
    Route::get('/workout/display_user_workout_routines/{user_id}', 'WorkoutController@getUserAllWorkouts');
    Route::get('/workout/user_routines_templates/{user_id}', 'WorkoutController@getUserRoutinesTemplates');
    Route::delete('/workout/clear_all_user_workouts/{user_id}', 'UserWorkoutController@clearAllMyWorkouts');
    Route::put('/workout/remove_myworkout/{user_id}', 'UserWorkoutController@removeMyWorkout');

    //Route::get('/workout/body_parts', 'WorkoutController@getBodyParts');

    Route::post('/events/update_settings', 'ClassScheduleController@updateEventSetting');
    Route::get('/events/settings', 'ClassScheduleController@getEventSettings');
    Route::get('/events/active_settings', 'ClassScheduleController@getActiveSettings');
    Route::post('/events/upload', 'ClassScheduleController@uploadImage');
    Route::get('/events/remove/{file_name}', 'ClassScheduleController@removeImage');

    // organizations
   Route::post('/organization/upload', 'OrganizationController@uploadLogo');
   Route::get('/organization/remove/{file_name}','OrganizationController@removeLogo');
   Route::post('/organization/checksubDomain', 'OrganizationController@checksubDomain');
   Route::get('/organization/list','OrganizationController@getOrganizationList');
   Route::delete('/organization/delete/{id}','OrganizationController@destroy');

   // Challenges
   Route::post('/challenge/challenge_participant', 'ChallengeController@challengeParticipant');
   Route::post('/challenge/user_challenge_data', 'ChallengeController@userChallengeData');
   Route::get('/challenge/get_challenge_detail/{challenge_id}', 'ChallengeController@getChallengeDetail');
   Route::delete('/challenge/remove_challenge_data/{id}', 'ChallengeController@removeUserChallengeData');

   // Assign Plan
   Route::get('/template/get_nutrition_template','AssignPlanController@getNutritionTemplate');
   Route::get('/template/get_meals_template','AssignPlanController@geMealsTemplate');
   Route::get('/template/get_workouts_template','AssignPlanController@geWorkoutsTemplate');
   Route::post('/template/assign_patient', 'AssignPlanController@assignedPlan');
   Route::get('/assign_plan/get_user_plan/{user_id}','AssignPlanController@getUserMealPlan');


  // My Plan

   Route::get('/my_plan/get_nutrition_template','MyPlanController@getNutritionTemplate');
   Route::get('/my_plan/get_master_templates','MyPlanController@geMasterTemplate');
   Route::get('/my_plan/get_workouts_template','MyPlanController@geWorkoutsTemplate');
   Route::get('/my_plan/get_meals_template','MyPlanController@geMealsTemplate');
   //for iPhone
   Route::get('/my_plan/user_template_data/{user_id}','MyPlanController@geUserTemplateData');

   // Meal Plan
   Route::get('/meal_plan/get_meal_plans', 'MealPlansController@getMealPlans');
   Route::post('/meal_plan/create_meal_plan', 'MealPlansController@saveMealPlan');
   Route::delete('/meal_plan/delete/{id}','MealPlansController@delete');
   Route::put('/meal_plan/update_meal_plan/{id}','MealPlansController@updateMealPlan');
   Route::put('/meal_plan/update_user_meal_plan/{id}','MealPlansController@updateUserMealPlanDetail');
   Route::post('/meal_plan/assign_plan_to_user','MealPlansController@assignPlanToUser');
   Route::get('/meal_plan/new_user_plan/{user_id}','MealPlansController@newUserMealPlan');
   Route::get('/meal_plan/get_user_plan/{user_id}','MealPlansController@getUserMealPlan');
   Route::post('/meal_plan/unassign_plan_to_user','MealPlansController@unassignPlanToUser');
   Route::put('/meal_plan/update_plan/{id}','MealPlansController@updatePlan');
   Route::put('/meal_plan/update_plan_config/{id}','MealPlansController@updatePlanConfig');
   Route::put('/meal_plan/update_day_plan/{id}','MealPlansController@updateDayPlan');

   Route::put('/meal_plan/user_reschedule_plan/{user_id}','MealPlansController@updateUserMealPlan');

   // for server side service
   Route::get('/meal_plan/get_mplan_details/{id}','MealPlansController@getMealPlanDetails');
   //get user meal details for assign plan
   Route::get('/meal_plan/get_user_mplan_details/{id}','MealPlansController@getUserMealPlanDetails');

    // for app side service
   Route::get('/meal_plan/get_plan_details/{id}','MealPlansController@getMealPlanAppDetails');

    // Push Notification
    Route::post('/user/save_user_device_data', 'UserDeviceController@saveUserDevice');
    Route::get('/user/send_push_notification/{user_id}', 'UserDeviceController@sendPushNotification');
    Route::post('/user/logout', 'UserDeviceController@removeUserApnDevice');
    Route::put('/user/save_time_zone/{user_id}','UserController@saveTimeZone');
    Route::get('/user/get_push_notification/{user_id}', 'UserDeviceController@getUserPushNotification');
    Route::delete('/user/delete_push_notification/{id}','UserDeviceController@deleteUserPushNotification');
    Route::put('/user/update_user_notification/{id}','UserDeviceController@updateUserPushNotification');
    Route::resource('/food','FoodController');
    Route::resource('/exercise','ExerciseController');
    Route::resource('/patient','UserController');
    Route::resource('/doctor','UserController');

    Route::resource('/workout','WorkoutController');
    Route::resource('/template','TemplateController');
    Route::resource('/goals','GoalsController');
    Route::resource('/exercise_image','ExerciseImageController');
    Route::resource('/user_step','UserStepController');
    //Route::post('/patient/updateProfile', 'UserController@updateUserProfile');
});


// iPhone webservices routes for Guest User
Route::prefix('/v1')->group(function() {
    Route::post('/patient/store', 'UserController@register');
    Route::post('/patient/login', 'UserController@login');
    Route::get('/activities', ['uses' => 'UserController@getActivity', 'as' => 'patient.getActivity']);
    Route::post('/patient/updateProfile', 'UserController@updateUserProfile');
    Route::get('/user/email_address/{token}','UserController@getEmail');
    Route::post('/auth/login', 'Auth\AuthController@authLogin');
    //Route::controller('password', 'Auth\PasswordController');
    Route::post('/password/email', 'Auth\PasswordController@postEmail');
    Route::post('/password/reset', 'Auth\PasswordController@postReset');
    Route::post('/user/change_password', 'Auth\PasswordController@changedPassword');
    Route::post('/user/signup_steps', 'UserController@SignupSteps');
});
// Third Parity API of User
Route::group(['prefix' => '/v1','middleware'=>'auth:admin'], function () {
   Route::post('/users/register', 'UserController@auth0Register');
   Route::post('/users/forgot_password', 'Auth\PasswordController@sendResetLink');
   Route::post('/live_users/register', 'UserController@auth0liveRegister');
   Route::post('/users/check_exists', 'UserController@checkExists');
   Route::post('/users/change_password', 'UserController@auth0ChangePassword');
});
