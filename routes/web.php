<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Artisan;

use Sentry\Tracing\SpanContext;
use Sentry\Tracing\TransactionContext;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

header('Access-Control-Allow-Origin: *');
header( 'Access-Control-Allow-Headers: Authorization, Content-Type, mobile-app' );
header('Access-Control-Allow-Methods', '*');
Route::get('/user/checkPushNotification', ['uses' => 'UserDeviceController@checkNotification']);

Route::get('/','HomeController@index');
Route::get('/whyChooseUs','HomeController@whyUs');
//Route::post('/sendMessage', 'HomeController@sendMessage');
Route::get('admin', 'AdminController@index');
Route::prefix('admin')->group(function() {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
	Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
	Route::post('/superadminlogin', 'Auth\AdminLoginController@superadminlogin');
    Route::get('/patient/{id}/{slug}', ['uses' => 'UserController@show', 'as' => 'patient.show'])->middleware('auth:admin');
    Route::get('/doctor/{id}/{slug}', ['uses' => 'UserController@show', 'as' => 'doctor.show'])->middleware('auth:admin');
    Route::resource('/patient','UserController')->middleware('auth:admin');
    Route::resource('/doctor','UserController')->middleware('auth:admin');
    Route::resource('/class_schedules','ClassScheduleController')->middleware('auth:admin');
    Route::resource('/challenges','ChallengeController')->middleware('auth:admin');
    Route::resource('/deal','DealController')->middleware('auth:admin');
    Route::resource('/social_media','SocialMediaController')->middleware('auth:admin');
    Route::resource('/organization','OrganizationController')->middleware('auth:admin');
    Route::resource('/manage_food','ManageFoodController')->middleware('auth:admin');
    Route::get('/add_manage_food','HomeController@add_edit_food_content')->middleware('auth:admin');
    Route::get('/refer','UserController@referFriend')->middleware('auth:admin');
    Route::resource('/video','VideoController')->middleware('auth:admin');
    Route::resource('/workout','WorkoutController')->middleware('auth:admin');
    Route::resource('/template','TemplateController')->middleware('auth:admin');
    Route::resource('/exercise_images','ExerciseImageController')->middleware('auth:admin');
    Route::get('/md_meals','HomeController@mdIndex')->middleware('auth:admin');
    Route::get('/day_meal_plans','MealPlansController@dayplan')->middleware('auth:admin');
    Route::get('/complete_meal_plans','MealPlansController@index')->middleware('auth:admin');
    Route::get('/recommended_foods','HomeController@recommended_food')->middleware('auth:admin');
    Route::get('/assign_plan/{id}', ['uses' => 'AssignPlanController@index'])->middleware('auth:admin');
    Route::get('/recommended_food_content','HomeController@recommended_food_content')->middleware('auth:admin');
    Route::get('/complete_plans_display','HomeController@complete_plans_display')->middleware('auth:admin');
    Route::get('/view_complete_plan','HomeController@view_complete_plan')->middleware('auth:admin');
    Route::get('/md_meals_display','HomeController@md_meals_display')->middleware('auth:admin');
    Route::get('/day_plans_display','HomeController@day_plans_display')->middleware('auth:admin');
    Route::get('/view_day_plan','HomeController@view_day_plan')->middleware('auth:admin');
    Route::get('/load_events','HomeController@classes')->middleware('auth:admin');
    Route::get('/load_challenge','HomeController@list_challenge')->middleware('auth:admin');
    Route::get('/deal_list','HomeController@deal_list')->middleware('auth:admin');
    Route::get('/load_referfriend','HomeController@list_referfriend')->middleware('auth:admin');
    Route::get('/manage_food_content','HomeController@food_content')->middleware('auth:admin');
    Route::get('/load_event_setting','HomeController@event_setting')->middleware('auth:admin');
    Route::get('/load_add_challenge','HomeController@add_challenge')->middleware('auth:admin');
  	Route::get('/load_edit_challenge','HomeController@edit_challenge')->middleware('auth:admin');
    Route::get('/create_deal','HomeController@create_deal')->middleware('auth:admin');
  	Route::get('/edit_deal','HomeController@edit_deal')->middleware('auth:admin');
    Route::get('/organization/impersonate/{id}','OrganizationController@impersonateOrganization')->middleware('auth:admin');
  	Route::get('/organization/stopImpersonate/{id}','OrganizationController@stopImpersonate')->middleware('auth:admin');
    Route::get('/assign_plans_display','HomeController@assign_plans_display')->middleware('auth:admin');
    Route::get('/assign_complete_plan_details','HomeController@assign_plan_details')->middleware('auth:admin');
  	Route::get('/assign_day_plan_details','HomeController@assign_day_plan_details')->middleware('auth:admin');
    Route::get('/load_list_workout_plans','HomeController@list_templates')->middleware('auth:admin');
    Route::get('/load_add_workouts_plans','HomeController@add_template')->middleware('auth:admin');
  	Route::get('/load_edit_wokout_plans','HomeController@edit_template')->middleware('auth:admin');
    Route::get('/load_list_workouts','HomeController@list_workouts')->middleware('auth:admin');
    Route::get('/load_edit_workout','HomeController@edit_workout')->middleware('auth:admin');
  	Route::get('/load_add_workouts','HomeController@add_workout')->middleware('auth:admin');
    Route::get('/load_exrcise_images','HomeController@list_exercise_image')->middleware('auth:admin');
    Route::get('/load_add_exercise_img','HomeController@add_exercise_image')->middleware('auth:admin');
  	Route::get('/load_edit_exercise_img','HomeController@edit_exercise_image')->middleware('auth:admin');
    Route::get('/add_recommended_food','HomeController@add_recommended_food')->middleware('auth:admin');
  	Route::get('/edit_recommended_food','HomeController@edit_recommended_food')->middleware('auth:admin');
    Route::get('/md_recommended_food_type','HomeController@md_recommended_food_type')->middleware('auth:admin');
    Route::get('/meal_items_data','HomeController@recommended_meal_items_data')->middleware('auth:admin');
  	Route::get('/new_meal_items_data','HomeController@new_recommended_meal_items_data')->middleware('auth:admin');
});
//Route::get('auth/login', 'Auth\Auth0IndexController@login')->name( 'login' );
//Route::get('auth/signup', 'Auth\LoginController@showSignupForm');

//Route::get('/auth/reset_password/{token}','UserController@getResetPassword');
//Route::get('/auth/create_password/{token}','UserController@getCreatePassword');
Auth::routes();
Route::get('/login', 'Auth\Auth0IndexController@login')->name( 'login' );
Route::get('/logout','Auth\Auth0IndexController@logout')->name( 'logout' )->middleware('auth');

Route::get('register/verify/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'UserController@verifyEmail'
]);

Route::get('contacts', 'HomeController@contactUs');

/*Route::get('sentpassword', function () {
    return view('auth.sentPassword');
});*/

Route::get('/auth0/callback', '\Auth0\Login\Auth0Controller@callback' )->name( 'auth0-callback' );

Route::post('/pages/send_email', 'PagesController@sendEmail');
Route::get('/terms_and_privacy', ['uses' => 'PagesController@termsAndPrivacy']);
//Route::get('/why_us', ['uses' => 'PagesController@whyChooseUs']);
Route::get('/exercise/applehealth','ExerciseController@saveAppleHeath');
Route::get('/withings_callback', 'WithingsController@getUserData');

Route::group(['middleware' => array('auth','useractivity')], function()
{
	//Route::get('/patient/assign_plan/{id}', ['uses' => 'AssignPlanController@index']);
	//Route::get('/patient/{id}/{slug}', ['uses' => 'UserController@show', 'as' => 'patient.show']);
	//Route::get('/doctor/{id}/{slug}', ['uses' => 'UserController@show', 'as' => 'doctor.show']);
	Route::get('/video/edit/{id}', ['uses' => 'VideoController@index', 'as' => 'video.index']);
	Route::get('/blood_pressure', ['uses' => 'FoodController@getBlood_pressure', 'as'=> 'blood_pressure.getBlood_pressure']);
	Route::get('/exercise', ['uses' => 'ExerciseController@getPatientExercise', 'as' => 'patient.getPatientExercise']);
	//Route::get('/reports', ['uses' => 'FoodController@getPatientDiary', 'as' => 'patient.getPatientDiary']);
	Route::get('/diary/download_diary', ['uses' => 'UserController@getUserHealthDiary', 'as' => 'patient.getUserHealthDiary']);
	Route::get('/reports', 'ReportController@index');
	Route::get('/dashboard', 'UserController@dashboard');
	//Route::resource('/patient','UserController');
	//Route::resource('/doctor','UserController');
	//Route::resource('/video','VideoController');
	//Route::resource('/workout','WorkoutController');
	//Route::resource('/template','TemplateController');
	Route::resource('/goals','GoalsController');
	Route::resource('/food','FoodController');
	Route::resource('/weight','WeightController');
	Route::resource('/sleep','SleepController');
	Route::resource('/bp_pulse','BloodPressureController');
	//Route::resource('/exercise_images','ExerciseImageController');
	//Route::resource('/class_schedules','ClassScheduleController');
	//Route::get('/patient_info' ,'UserController@getPatientEMR');
	//Route::resource('/challenges','ChallengeController');
	//Route::resource('/deal','DealController');
	Route::resource('/diary','DiaryController');
	//Route::resource('/social_media','SocialMediaController');
	Route::get('/challenge', 'ChallengeController@displayChallenges');
	Route::get('/event','ClassScheduleController@displayEvents');
  //Route::get('/refer','UserController@referFriend');
	Route::get('/my_plans','MyPlanController@index');
	//Route::get('/complete_meal_plans','MealPlansController@index');
	//Route::get('/day_meal_plans','MealPlansController@dayplan');
	//Route::resource('/organization','OrganizationController');
	//Route::resource('/manage_food','ManageFoodController');
	//Route::get('/organization/impersonate/{id}','OrganizationController@impersonateOrganization');
	//Route::get('/organization/stopImpersonate/{id}','OrganizationController@stopImpersonate');
	Route::get('/workout_routines', 'UserWorkoutController@index');
	/* Load Views */
	Route::get('/load_user_plan','HomeController@user_plan');
	Route::get('/create_user_meal','HomeController@create_user_meal');
	//Route::get('/recommended_foods','HomeController@recommended_food');
	Route::get('/recommended_addfood','HomeController@recommended_addfood');
	Route::get('/recommended_meals','HomeController@recommended_meal');
	//Route::get('/md_meals','HomeController@mdIndex');
	//Route::get('/meal_items_data','HomeController@recommended_meal_items_data');
	//Route::get('/new_meal_items_data','HomeController@new_recommended_meal_items_data');
	Route::get('/recommended_meals_display','HomeController@recommended_meals_display');
	//Route::get('/md_meals_display','HomeController@md_meals_display');
	//Route::get('/recommended_food_content','HomeController@recommended_food_content');
	//Route::get('/add_recommended_food','HomeController@add_recommended_food');
	//Route::get('/edit_recommended_food','HomeController@edit_recommended_food');
	Route::get('/load_food_diary','HomeController@food_home_page');
	Route::get('/log_food','HomeController@log_food');
	Route::get('/add_custom_food','HomeController@add_custom_food');
	//Route::get('/complete_plans_display','HomeController@complete_plans_display');
	//Route::get('/day_plans_display','HomeController@day_plans_display');
	//Route::get('/assign_plans_display','HomeController@assign_plans_display');
	//Route::get('/assign_complete_plan_details','HomeController@assign_plan_details');
	//Route::get('/assign_day_plan_details','HomeController@assign_day_plan_details');
	//Route::get('/view_complete_plan','HomeController@view_complete_plan');
	//Route::get('/view_day_plan','HomeController@view_day_plan');
	Route::get('/custom_foods','HomeController@custom_foods');
	Route::get('/load_exercise_diary','HomeController@exercise_home_page');
	Route::get('/log_exercise','HomeController@log_exercise');
	Route::get('/create_meal','HomeController@create_meal');
	Route::get('/add_recommended_meal_food','HomeController@add_recommended_meal_food');
  //Route::get('/md_recommended_food_type','HomeController@md_recommended_food_type');
	//Route::get('/load_exrcise_images','HomeController@list_exercise_image');
	//Route::get('/load_challenge','HomeController@list_challenge');
	//Route::get('/load_referfriend','HomeController@list_referfriend');
	//Route::get('/load_add_challenge','HomeController@add_challenge');
	//Route::get('/load_edit_challenge','HomeController@edit_challenge');
	//Route::get('/load_add_exercise_img','HomeController@add_exercise_image');
	//Route::get('/load_edit_exercise_img','HomeController@edit_exercise_image');
	Route::get('/meals','HomeController@user_meals');
	//Route::get('/load_list_workouts','HomeController@list_workouts');
	//Route::get('/load_edit_workout','HomeController@edit_workout');
	//Route::get('/load_add_workouts','HomeController@add_workout');
	//Route::get('/load_add_workouts_plans','HomeController@add_template');
	//Route::get('/load_list_workout_plans','HomeController@list_templates');
	//Route::get('/load_edit_wokout_plans','HomeController@edit_template');
	//Route::get('/load_events','HomeController@classes');
	//Route::get('/load_event_setting','HomeController@event_setting');
	Route::get('/club_holidays','HomeController@club_holidays');
	//Route::get('/deal_list','HomeController@deal_list');
	//Route::get('/create_deal','HomeController@create_deal');
	//Route::get('/edit_deal','HomeController@edit_deal');
	Route::get('/deals_list_front','HomeController@deal');
	Route::get('/workout_details','HomeController@workout_details');
	Route::get('/challenges_list_front','HomeController@display_challenges');
	Route::get('/load_diary_page','HomeController@diary_home_page');
  Route::get('/user_workout','HomeController@user_workout');
  Route::get('/load_workout_details','HomeController@loadWorkout_details');
  Route::get('/create_template_name','HomeController@create_meal_templatename');
	Route::get('/template_names','HomeController@meal_template_names');
	Route::get('/edit_template_names','HomeController@edit_meal_templatename');
	Route::get('/load_routine_details','HomeController@routine_details');
	Route::get('/admin_custom_foods','HomeController@admin_custom_foods');
	Route::get('/admin_custom_foods_create','HomeController@custom_foods_form');
	//Manage Food
	//Route::get('/manage_food_content','HomeController@food_content');
	//Route::get('/add_manage_food','HomeController@add_edit_food_content');
});
// Route for session login of user from magento
	Route::get('/user/loginByEmail/email={email}', 'UserController@magentoSessionLogin');
