
<div class="row block-title patient_title">
      <div class="col-sm-6">
        <h3><strong>Custom Foods</strong></h3>
      </div>  
      <div class="col-sm-6 text-alignRight">
          
      </div>
</div>

<div class="row form-horizontal form-bordered">
  <div class="col-md-12">
    <!-- Basic Form Elements Block -->
    <div class="block">
        <div class="block-title">
          <h2>
          <strong>CREATE CUSTOM FOOD</strong>
          </h2>
        </div>
        <div ng-show="add_custom_food.log_newfoodloader"  class="custom_food_loader"></div>
        <!-- Basic Form Elements Content -->
        <div class="form-group">
        <form name="customFoodForm" class="form-horizontal add_custom_food_form" ng-submit="add_custom_food.add_new_food()">
          <div class="col-sm-6">

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-food-name">Food Type<span class="text-danger">&nbsp;*</span></label>
              <div class="col-sm-7">
                  <select class="form-control" ng-model="add_custom_food.custom_food.source_name">
                    <option value="custom">Custom</option>
                    <option value="supplement">Supplement</option>
                  </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-food-name">Food Name<span class="text-danger">&nbsp;*</span></label>
              <div class="col-sm-7">
                  <input type="text" ng-model="add_custom_food.custom_food.food_name"
                      id="txt-food-name"
                      class="form-control"
                      placeholder="Food Name" required>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-food-brand">Food Brand<span class="text-danger">&nbsp;*</span></label>
              <div class="col-sm-7">
                  <input type="text" ng-model="add_custom_food.custom_food.brand_name"
                      id="txt-food-brand"
                      class="form-control"
                      placeholder="Food Brand" required>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-serving-size">Serving Size<span class="text-danger">&nbsp;*</span></label>
              <div class="col-sm-3 no-padding-left">
                  <input type="text" ng-model="add_custom_food.custom_food.serving_quantity"
                      id="txt-serving-size"
                      class="form-control"
                      placeholder="Quantity" required>
              </div>
              <div class="col-sm-4 no-padding-left">
                  <input type="text" ng-model="add_custom_food.custom_food.serving_size_unit"
                      id="txt-serving-size"
                      class="form-control"
                      placeholder="Unit" required>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-calories">Calories<span class="text-danger">&nbsp;*</span></label>
              <div class="col-sm-7">
                  <input type="text" ng-model="add_custom_food.custom_food.calories"
                      id="txt-calories"
                      class="form-control"
                      placeholder="Calories" required>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-total-fat">Total Fat</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_custom_food.custom_food.total_fat"
                      id="txt-total-fat"
                      class="form-control"
                      placeholder="Total Fat">
                  <div class="input-group-addon">g</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-saturated-fat">Saturated Fat</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_custom_food.custom_food.saturated_fat"
                      id="txt-saturated-fat"
                      class="form-control"
                      placeholder="Saturated Fat">
                  <div class="input-group-addon">g</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-trans-fat">Trans Fat</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_custom_food.custom_food.trans_fat"
                      id="txt-trans-fat"
                      class="form-control"
                      placeholder="Trans Fat">
                  <div class="input-group-addon">g</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-cholesterol">Cholesterol</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_custom_food.custom_food.cholesterol"
                      id="txt-cholesterol"
                      class="form-control"
                      placeholder="Cholesterol">
                  <div class="input-group-addon">mg</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-sodium">Sodium</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_custom_food.custom_food.sodium"
                      id="txt-sodium"
                      class="form-control"
                      placeholder="Sodium">
                  <div class="input-group-addon">mg</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-potassium">Potassium</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_custom_food.custom_food.potassium"
                      id="txt-potassium"
                      class="form-control"
                      placeholder="Potassium">
                  <div class="input-group-addon">mg</div>
              </div>
            </div>

          </div>

          <div class="col-sm-6">
            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-total-carbs">Total Carbs</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_custom_food.custom_food.total_carbs"
                      id="txt-total-carbs"
                      class="form-control"
                      placeholder="Total Carbs">
                  <div class="input-group-addon">g</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-polyunsaturated">Polyunsaturated</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_custom_food.custom_food.polyunsaturated"
                      id="txt-polyunsaturated"
                      class="form-control"
                      placeholder="Polyunsaturated">
                  <div class="input-group-addon">g</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-dietary-fiber">Dietary Fiber</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_custom_food.custom_food.dietary_fiber"
                      id="txt-dietary-fiber"
                      class="form-control"
                      placeholder="Dietary Fiber">
                  <div class="input-group-addon">g</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-monounsaturated">Monounsaturated</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_custom_food.custom_food.monounsaturated"
                      id="txt-monounsaturated"
                      class="form-control"
                      placeholder="Monounsaturated">
                  <div class="input-group-addon">g</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-sugar">Sugar</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_custom_food.custom_food.sugar"
                      id="txt-sugar"
                      class="form-control"
                      placeholder="Sugar">
                  <div class="input-group-addon">g</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-protein">Protein</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_custom_food.custom_food.protein"
                      id="txt-protein"
                      class="form-control"
                      placeholder="Protein">
                  <div class="input-group-addon">g</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-vitamin-a">Vitamin A</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_custom_food.custom_food.vitamin_a"
                      id="txt-vitamin-a"
                      class="form-control"
                      placeholder="Vitamin A">
                  <div class="input-group-addon">%</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-vitamin-c">Vitamin C</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_custom_food.custom_food.vitamin_c"
                      id="txt-vitamin-c"
                      class="form-control"
                      placeholder="Vitamin C">
                  <div class="input-group-addon">%</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-calcium">Calcium</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_custom_food.custom_food.calcium"
                      id="txt-calcium"
                      class="form-control"
                      placeholder="Calcium">
                  <div class="input-group-addon">%</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-iron">Iron</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_custom_food.custom_food.iron"
                      id="txt-iron"
                      class="form-control"
                      placeholder="Iron">
                  <div class="input-group-addon">%</div>
              </div>
            </div>

          </div>
          <div class="saperator"></div>
            <div class="form-group row">
              <div class="col-sm-offset-6 col-sm-12">
                <button type="submit" class="btn btn-primary btndesign" ng-disabled="customFoodForm.$invalid">Create</button>
              </div>
            </div>
        </form>
        <!-- END Basic Form Elements Content -->
        </div>
    </div>
    <!-- END Basic Form Elements Block -->
  </div>
</div>