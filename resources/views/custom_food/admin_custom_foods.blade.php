@extends('layout.app')

@section('content')

<div ng-controller="AdminCustomFoodController as customFood">  
    <div ui-view></div>
</div>

@endsection

@section('page_scripts')

{!! HTML::script("js/controllers/AdminCustomFoodController.js") !!}
{!! HTML::script("js/controllers/AdminAddCustomFoodController.js") !!}
@append

@section('jquery_ready')

@append