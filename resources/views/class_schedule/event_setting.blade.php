<div class="block-title patient_title" ng-init="setting.getEventSettings();">
	    <div class="col-sm-2">
	     <h3><strong>Events Setting</strong></h3>
	    </div>
	   <div class="col-sm-6">
          	<label class="radio-inline">
          	<input type="radio"
          	 	   ng-change="setting.changeSetting();"
          	       ng-model="setting.class_setting"
          	       value="google_calendar"
          	 ><b>Google Calendar</b></label>
			<label class="radio-inline">
			<input type="radio"
			 		ng-model="setting.class_setting"
			 		ng-change="setting.changeSetting();"
			  		value="events_pdf"
			 ><b>Events PDF</b></label>
			<label class="radio-inline">
			<input type="radio"
				 ng-model="setting.class_setting"
				 ng-change="setting.changeSetting();" 
				 value="events_url"
			 ><b>Events URL</b></label>
   
      </div>
		
		<div class="col-sm-4 text-alignRight">
    
                 <button ng-show="setting.class_setting == 'google_calendar'" ng-click="setting.saveEventSetting();"
				                  type="button"
				                  class="btn btn-default"
				                  ng-disabled="googleCalendarForm.$invalid"
                    >
                    <i class="fa gi gi-disk_saved"></i> Save &amp; Close
                   </button>

                   <button ng-show="setting.class_setting == 'events_url'" ng-click="setting.saveEventSetting();"
				                  type="button"
				                  class="btn btn-default"
				                  ng-disabled="eventURLForm.$invalid"
                    >
                    <i class="fa gi gi-disk_saved"></i> Save &amp; Close
                   </button>

                   <button ng-show="setting.class_setting == 'events_pdf'" ng-click="setting.saveEventSetting();"
				                  type="button"
				                  class="btn btn-default"
				   >
                    <i class="fa gi gi-disk_saved"></i> Save &amp; Close
                   </button>
                 <a class="btn btn-default" ui-sref="events"> <i class="gi gi-remove_2"></i>
                 &nbsp;Cancel</a>
  		</div>

	 </div>
	 <div class="block admin_side_custom">
		<div class="block-title">
			<h2><strong>@{{setting.settingTitle}}</strong></h2>
		</div>

		<div class="row"  ng-show="setting.class_setting == 'google_calendar'">
			<div class="col-xs-12">
				

				<form name="googleCalendarForm" novalidate class="form-horizontal form-bordered" >
                    <div class="form-group">
                        <label for="api_key_id" class="col-md-3 control-label">API Key <span class="text-danger">*</span></label>
                        <div class="col-md-9">
                            <input type="text"
                                  placeholder="Enter API Key"
                                  class="form-control"
                                  name="api_key"
	                              ng-model="setting.api_key" 
	                              id="api_key_id" required
                              >
                           	<span class="text-danger" ng-show="googleCalendarForm.api_key.$invalid && googleCalendarForm.api_key.$touched">
                           Please enter Google API Key.</span>

                        </div>
                    </div>

                    <div class="form-group">
                        <label for="txt_event_url" class="col-md-3 control-label">Calendar Id <span class="text-danger">*</span></label>
                        <div class="col-md-9">
                            <input type="text"
                              placeholder="Enter Calendar Id"
                              class="form-control"
							  name="calendar_id"
                              ng-model="setting.calendar_id" 
                              id="calendar_id" required
                              >
                              	<span class="text-danger" ng-show="googleCalendarForm.calendar_id.$invalid && googleCalendarForm.calendar_id.$touched">
                           Please enter Google Calendar Id.</span>
                        </div>
                    </div>
                   
                   
                </form>

			</div>
		</div>

		<div class="row"  ng-show="setting.class_setting == 'events_pdf'">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
   			                <div upload-image-dropzone="" file="setting.pdf_name" accepted-files remove-image-url="/api/v3/events/remove/" upload-image-url="/api/v3/events/upload" class="dropzone ng-isolate-scope dz-clickable">
			                     <div class="dz-default dz-message"><span>Drop PDF here to upload</span></div>                     
			                </div>
			            </div>

					</div>
					<div class="col-xs-6"></div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 text-center">
						<object data="@{{setting.event_pdf_url}}" type="application/pdf" width="1024" height="800">
						</object>
					</div>
				</div>

			</div>
			
		</div>

		<div class="row" ng-show="setting.class_setting =='events_url'">
			<div class="col-xs-12">
				

				<form  class="form-horizontal form-bordered" name="eventURLForm" novalidate >
                    <div class="form-group">
                        <label for="url_id" class="col-md-3 control-label">URL <span class="text-danger">*</span></label>
                        <div class="col-md-9">
                            <input type="text"
                                 placeholder="Enter URL.."
                                 class="form-control"
                                 name="url"
	                             ng-model="setting.url" 
	                             id="url_id" required
                              >
                            <span class="text-danger" ng-show="eventURLForm.url.$invalid && eventURLForm.url.$touched">
                           Please enter valid events URL.</span>
                        </div>
                    </div>
                   
                   
                </form>

			</div>
		</div>
</div>