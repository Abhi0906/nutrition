@extends('layout.app')

@section('content')

<div ng-controller="ClassScheduleController as class_schedule" ng-init="class_schedule.isEventSetting='{{$isEventSetting}}';class_schedule.changeRoute();">
	
	<div ui-view></div>   
</div>


@endsection

@section('page_scripts') 
{!! HTML::script("js/controllers/ClassScheduleController.js") !!}
{!! HTML::script("js/controllers/ShowClassController.js") !!}
{!! HTML::script("js/controllers/EventSettingController.js") !!}
{!! HTML::script("js/directives/uploadImageDropZone.js") !!}


@append

@section('jquery_ready')

@append