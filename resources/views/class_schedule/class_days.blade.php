<div class="panel-body add_break_contain">
    <ul class="list-inline ul_block_bottomline" ng-repeat="class_day in add_class.class_days">
        <li class="width_25">@{{class_day.day_title}}</li>
        <li>
            <time-picker name="timepicker" id="timePicker" ng-model="add_class.class_data.class_days[$index].start_time"></time-picker>  
        </li>
        <li>to</li>
        <li>
            <time-picker name="timepicker" id="timePicker" ng-model="add_class.class_data.class_days[$index].end_time"></time-picker>
        </li>                             
    </ul>
</div>