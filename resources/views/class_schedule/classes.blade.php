<div class="block-title patient_title" ng-init="show_class.getActiveEventSetting();">
    <div class="col-sm-2">
        <h3><strong>Events</strong></h3>
    </div>  
    <div class="col-sm-10 text-alignRight">
        <h3>
          <form class="form-inline">
	           <div class="form-group">
	              <a ui-sref="event-setting" class="btn btn-default" data-original-title="Setting"><span><i class="fa fa-cog"></i></span>&nbsp;Settings</a>
	            </div>
	          </form>
        </h3>
    </div>
</div>
<div class="block admin_side_custom">
	<div class="block-title">
	<h2><strong>@{{show_class.settingTitle}}</strong></h2>
	</div>
	<div class="row" ng-if="show_class.event_type == 'google_calendar'">
		<div class="col-xs-12">
			  <div id='calendar' google-calendar api-key ='@{{show_class.google_api_key}}' calendar-id ='@{{show_class.google_calendarId}}'></div>  
		</div>
	</div>

	<div class="row" ng-if="show_class.event_type == 'events_url'">
		<div class="col-xs-12 text-center">
			<iframe  ng-src="@{{show_class.event_url}}" width="1024" height="800" frameborder=0></iframe>
		</div>

		
	</div>

	<div class="row" ng-if="show_class.event_type == 'events_pdf'">
		<div class="col-xs-12 text-center">
			 
			 <object data="@{{show_class.event_pdf_url}}" type="application/pdf" width="1024" height="800">
			 </object>
		</div>
	</div>
</div>


