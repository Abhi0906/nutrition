<div class="block-title patient_title">
    <div class="col-sm-2">
    
        <h3><strong> &nbsp; &nbsp; Add Class/Event</strong></h3>

    </div>  
    <div class="col-sm-10 text-alignRight">
        <h3>
          <form class="form-inline">
             
            <div class="form-group">
               <button class="btn btn-default" ng-disabled="    exerciseForm.$invalid" 
                   type="button" ng-click="add_exercise.saveExerciseImage();">
                        <i class="fa gi gi-disk_saved"></i> Save & Close
                </button>     

              <a ui-sref="classes" class="btn btn-default"> <i class="gi gi-remove_2"></i>
                 &nbsp;Cancel</a>
            </div>
          </form>
        </h3>
    </div>
</div>
<div ng-init=""> 
    <div class="block">
        <div class="block-title">
            
            <h2><strong>&nbsp;</strong></h2>
        </div>
        <form onsubmit="return false;" class="form-horizontal form-bordered" method="post" action="page_forms_components.php">
            <div class="row">
                <div class="col-md-7">
                    
                        <div class="form-group">
                            <label for="example-colorpicker" class="col-sm-4 control-label">Class Name</label>
                            <div class="col-sm-7">
                                <input type="text" 
                                        value="" 
                                        class="form-control input-colorpicker colorpicker-element" 
                                        name="example-colorpicker" 
                                        id="example-colorpicker"
                                        ng-model="add_class.class_data.class_name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-colorpicker2" class="col-sm-4 control-label">Location</label>
                            <div class="col-sm-7">
                                <select class="form-control" 
                                        ng-model="add_class.class_data.class_location"
                                        ng-init="add_class.class_data.class_location='1'"
                                            >
                                    <option value="1">WestChester</option>
                                    <option value="2">Bronx</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-colorpicker3" class="col-sm-4 control-label">Instructor</label>
                            <div class="col-sm-7">
                                <input type="text" value="" 
                                                    class="form-control input-colorpicker-rgba colorpicker-element" 
                                                    name="example-colorpicker3" 
                                                    id="example-colorpicker3"
                                                    ng-model="add_class.class_data.instructor">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-colorpicker2" class="col-sm-4 control-label">Activity Type</label>
                            <div class="col-sm-7">
                                <select class="form-control" 
                                        ng-model="add_class.class_data.activity_type"
                                        ng-init="add_class.class_data.activity_type='1'">
                                    <option value="1">Aerobics</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-colorpicker3" class="col-sm-4 control-label">Class Max Size</label>
                            <div class="col-sm-7">
                                <input type="text" value="" 
                                                    class="form-control input-colorpicker-rgba colorpicker-element" 
                                                    name="example-colorpicker3" 
                                                    id="example-colorpicker3"
                                                    ng-model="add_class.class_data.class_max_size">
                            </div>
                        </div>
                      <!--   <div class="form-group">
                            <label for="frequency" class="col-sm-4 control-label">Frequency</label>
                            <div class="col-sm-7">
                                <div class="radio" ng-init="add_class.class_data.frequency='Daily'">
                                    <label><input type="radio" name="frequency" 
                                                               ng-value="'Daily'"
                                                                ng-model="add_class.class_data.frequency" >Daily</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label><input type="radio" name="frequency" ng-value="'Weekly'" ng-model="add_class.class_data.frequency">Weekly</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label><input type="radio" name="frequency" ng-value="'Monthly'" ng-model="add_class.class_data.frequency">Monthly</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                </div>
                            </div>
                        </div> -->
                        <!-- <div class="form-group">
                            <label for="days" class="col-sm-4 control-label">Every</label>
                            <div class="col-sm-7">
                                <div class="input-group">
                                    <input type="text" name="days" 
                                            class="form-control" 
                                            ng-model="add_class.class_data.frequency_interval" 
                                            ng-init="add_class.class_data.frequency_interval='1'" 
                                            >
                                    <span class="input-group-addon">Day(s)</span>
                                </div>
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label for="days" class="col-sm-4 control-label">Start</label>
                            <div class="col-sm-7">
                                <div class="input-group">
                                    <!-- <input type="text" name="start_day" 
                                            class="form-control"
                                            ng-model="add_class.class_data.class_start_date" 
                                            >
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span> -->
                                    <input  type="text"
                                            class="form-control" 
                                            uib-datepicker-popup = "@{{add_class.format}}"
                                            ng-model="add_class.class_data.class_start_date"
                                            is-open="add_class.class_start_date_popup.opened"
                                            datepicker-options="add_class.dateOptions"
                                            ng-required="true"
                                            show-button-bar=false
                                            close-text="Close" 
                                    />
                                    <span   class="input-group-addon caledar_custom" 
                                            ng-click="add_class.class_start_date_open()"> 

                                        <i aria-hidden="true" class="fa fa-calendar"></i>
                                    </span>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="days" class="col-sm-4 control-label">End</label>
                            <div class="col-sm-7">
                                <div class="input-group">
                                    <!-- <input type="text" name="start_day" 
                                            class="form-control"
                                            ng-model="add_class.class_data.class_start_date" 
                                            >
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span> -->
                                    <input  type="text"
                                                class="form-control" 
                                                uib-datepicker-popup = "@{{add_class.format}}"
                                                ng-model="add_class.class_data.class_end_date"
                                                is-open="add_class.class_end_date_popup.opened"
                                                datepicker-options="add_class.dateOptions"
                                                ng-required="true"
                                                show-button-bar=false
                                                close-text="Close" 
                                        />
                                        <span   class="input-group-addon caledar_custom" 
                                                ng-click="add_class.class_end_date_open()"> 
                                            <i aria-hidden="true" class="fa fa-calendar"></i>
                                        </span>
                                    
                                </div>
                            </div>
                        </div>
                       <!--  <div class="form-group">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-sm-7">
                                <div class="radio">
                                    <label><input type="radio" name="for_end_date"
                                                    ng-model="add_class.class_data.for_end_date">No End Date</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-sm-7">
                                <div class="radio">
                                    <label><input type="radio" name="for_end_date"
                                                ng-model="add_class.class_data.for_end_date"
                                                >End after</label>
                                    <input type="number" name="end" 
                                                class="input_occur" 
                                                value="1"
                                                ng-model="add_class.class_data.end_after_occurences"
                                                > Occurences    
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-sm-7">
                                <div class="radio">
                                    <label><input type="radio" name="for_end_date"
                                                    ng-model="add_class.class_data.for_end_date"
                                                    >End by</label>
                                    <div class="input-group">
                                       
                                        <input  type="text"
                                                class="form-control" 
                                                uib-datepicker-popup = "@{{add_class.format}}"
                                                ng-model="add_class.class_data.class_end_date"
                                                is-open="add_class.class_end_date_popup.opened"
                                                datepicker-options="add_class.dateOptions"
                                                ng-required="true"
                                                show-button-bar=false
                                                close-text="Close" 
                                        />
                                        <span   class="input-group-addon caledar_custom" 
                                                ng-click="add_class.class_end_date_open()"> 
                                            <i aria-hidden="true" class="fa fa-calendar"></i>
                                        </span>

                                        
                                    </div>  
                                </div>
                            </div>
                        </div> -->
                </div>
                <div class="col-md-5">
                    <div class="panel panel-default">
                       <div class="panel-body add_break_contain">
                            <ul class="list-inline ul_block_bottomline" ng-repeat="class_day in add_class.class_days">
                                <li class="width_25">@{{class_day.day_title}}</li>
                                <li>
                                    <time-picker name="timepicker" 
                                                    id="timePicker" 
                                                    ng-model="add_class.class_data.class_days[$index].start_time"
                                                    ng-init="add_class.class_data.class_days[$index].start_time=class_day.start_time"
                                                    >
                                    </time-picker>  
                                </li>
                                <li ng-if="add_class.class_data.class_days[$index].start_time!=''">to</li>
                                <li ng-if="add_class.class_data.class_days[$index].start_time!=''">
                                    <time-picker name="timepicker" 
                                        id="timePicker" 
                                        ng-model="add_class.class_data.class_days[$index].end_time"
                                        ng-init="add_class.class_data.class_days[$index].end_time=class_day.end_time"
                                        >
                                    </time-picker>
                                </li>                             
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button class="btn btn-sm btn-primary" type="submit" ng-click="add_class.saveClass()"><i class="fa fa-angle-right"></i> Submit</button>
                                <button class="btn btn-sm btn-warning" type="reset"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>