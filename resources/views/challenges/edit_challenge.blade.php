<div class="row block-title patient_title" ng-init="edit_challenge.getChallengeData()">
  <div class="col-sm-6">
    <h3><strong>&nbsp; Edit @{{edit_challenge.challenge_data.title | limitTo: 50 }}</strong></h3>
  </div>  
  <div class="col-sm-6 text-alignRight">
               <button class="btn btn-default" 
                                      ng-disabled="ChallengesEditForm.$invalid"
                                      ng-click="edit_challenge.updateChallenge()">
               <i class="fa fa-angle-right"></i> Update</button>
                 <button class="btn btn-default" type="button" ng-click="edit_challenge.deleteChallenge(edit_challenge.challenge_data.id);">
               <i class="fa gi gi-bin"></i> Delete
               </button>
               <a ui-sref="list-challenge" class="btn btn-default" ata-original-title="Cancel"><span><i class="gi gi-remove_2"></i></span>&nbsp;Cancel</a>
  </div>
</div>

<div class="row">
  <form name="ChallengesEditForm" enctype="multipart/form-data" class="form-horizontal form-bordered">
       <div class="col-md-6">
           <div class="block">
               <div class="block-title"><h2><strong>&nbsp;&nbsp; Edit Challenges Information</strong></h2>
               </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label" for="example-text-input">Is Expired</label>
                     <div class="col-md-5">
                         <select class="form-control" ng-model="edit_challenge.challenge_data.is_expired">
                           <option value="0" ng-selected="edit_challenge.challenge_data.is_expired==0">No</option>
                           <option value="1"  ng-selected="edit_challenge.challenge_data.is_expired==1">Yes</option>
                         </select>
                       
                     </div>
                  </div>
                  <div class="form-group">
                       <label class="col-md-3 control-label" for="example-text-input">Title<span class="text-danger">&nbsp;*</span></label>
                       <div class="col-md-8">
                           <input type="text" id="example-text-input" 
                                             name="title" 
                                             class="form-control" 
                                             placeholder="Text" 
                                             ng-model="edit_challenge.challenge_data.title"
                                             required>
            
                       </div>
                       <div class="col-md-offset-3 col-md-9">
                         <span class="text-danger" ng-show="ChallengesEditForm.title.$invalid && ChallengesEditForm.title.$touched">Please enter title</span>
                       </div>
                  </div>
                  <div class="form-group">
                       <label class="col-md-3 control-label" for="example-textarea-input">Description<span class="text-danger">&nbsp;*</span></label>
                       <div class="col-md-8">
                           <textarea id="example-textarea-input" 
                                     name="description" 
                                     rows="4" 
                                     class="form-control" 
                                     placeholder="Content.."
                                     ng-model="edit_challenge.challenge_data.description"
                                     required></textarea>
                       </div>
                       <div class="col-md-offset-3 col-md-9">
                         <span class="text-danger" ng-show="ChallengesEditForm.description.$invalid && ChallengesEditForm.description.$touched">Please enter description</span>
                       </div>
                  </div>    
                  <div class="form-group">
                     <label class="col-md-3 control-label" for="example-text-input">Start<span class="text-danger">&nbsp;*</span></label>
                     <div class="col-md-6">
                        <div class="input-group">
                           <input type="text" class="form-control" 
                              name="start_time"
                              datetime-picker="dd MMM yyyy hh:mm a" 
                              ng-model="edit_challenge.challenge_data.start_date"
                              is-open="edit_challenge.isOpen_startDate"  
                              when-closed="edit_challenge.dateCompareValidation(edit_challenge.challenge_data.start_date, edit_challenge.challenge_data.end_date)"
                              required/>
                           <span class="input-group-btn">
                           <button type="button" class="btn btn-default" ng-click="edit_challenge.openCalendar_startDate($event, prop)">
                           <i class="fa fa-calendar"></i></button>
                           </span>
                        </div>
                     </div>
                     <div class="col-md-offset-3 col-md-9">
                        <span class="text-danger">@{{edit_challenge.errMessage_startdate}}</span>
                        <br>
                        <span class="text-danger" ng-show="ChallengesEditForm.start_time.$invalid && ChallengesEditForm.start_time.$touched">Please select start time</span>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label" for="example-text-input">End<span class="text-danger">&nbsp;*</span></label>
                     <div class="col-md-6">
                        <div class="input-group">
                           <input type="text" class="form-control" 
                              name="end_time"
                              datetime-picker="dd MMM yyyy hh:mm a" 
                              ng-model="edit_challenge.challenge_data.end_date"
                              is-open="edit_challenge.isOpen_endDate" 
                              when-closed="edit_challenge.dateCompareValidation(edit_challenge.challenge_data.start_date, edit_challenge.challenge_data.end_date)" 
                              required/>
                           <span class="input-group-btn">
                           <button type="button" class="btn btn-default" ng-click="edit_challenge.openCalendar_endDate($event, prop)">
                           <i class="fa fa-calendar"></i></button>
                           </span>
                        </div>
                     </div>
                     <div class="col-md-offset-3 col-md-9">
                        <span class="text-danger">@{{edit_challenge.errMessage_enddate}}</span>
                        <br>
                        <span class="text-danger" ng-show="ChallengesEditForm.end_time.$invalid && ChallengesEditForm.end_time.$touched">Please select end time</span>
                     </div>
                  </div>
                  <div class="form-group">
                       <label class="col-md-3 control-label" for="example-text-input">Allowed Devices</label>
                       <div class="col-md-8">
                           <select 
                              data-placeholder="Choose a Devices"
                              chosen="edit_challenge.allowed"
                              ng-model="edit_challenge.allowed_devices" multiple required>
                              <option value="all">All</option>
                              <option ng-repeat="allowedDev in vm.allowed" value="@{{allowedDev.id}}">@{{allowedDev.allowed_device}}</option>
                           </select>
                       </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-3 control-label" for="example-text-input">Goal Type<span class="text-danger">&nbsp;*</span></label>
                     <div class="col-md-5">
                        <select class="form-control"  ng-click="edit_challenge.goal_select()" 
                           ng-model="edit_challenge.goal_type" name="goal_type" required>
                           <option value="">Select Goal Type</option>
                           <!-- <option value="body_fat_and_weight">Weight Loss & Body Fat %</option> -->
                          <!--  <option value="body_transformation">Body transformation</option> -->
                          <!--  <option value="calories">Calories Burned</option> -->
                           <!-- <option value="muscle_gain">Muscle Gain</option> -->
                           <option value="steps_run">Steps/Run</option>
                          <!--  <option value="weight_loss">Weight Loss</option> -->
                        </select>
                     </div>
                     <div class="col-md-offset-3 col-md-9">
                        <span class="text-danger text-center" ng-show="ChallengesEditForm.goal_type.$invalid && ChallengesEditForm.goal_type.$touched">
                        Please select goal type
                        </span>
                     </div>
                  </div>
                  <div ng-show="edit_challenge.calories">
                     <div class="form-group">
                        <label for="example-colorpicker" class="col-sm-3 control-label"> 
                        <p ng-bind-html="edit_challenge.first"></p></label>
                        <div ng-show="edit_challenge.start_unit" class="col-sm-5">
                            <div  class="input-group">
                                    <input type="text" value="" class="form-control" name="target_calories" id="target_calories"
                                       ng-model="edit_challenge.challenge_data.target_calories">
                                    <span class="input-group-btn">
                                    <span class="btn btn-default">&nbsp;&nbsp;
                                    <span ng-bind-html="edit_challenge.percentage"></span> &nbsp;&nbsp;</span></span>
                            </div>
                        </div>
                         <div ng-hide="edit_challenge.start_unit" class="col-sm-5">
                                       <input type="text" 
                                          value="" 
                                          placeholder="" 
                                          class="form-control" 
                                          name="target_calories" 
                                          id="target_calories"
                                          ng-model="edit_challenge.challenge_data.target_calories">
                         </div>

                     </div>
                      
                      <div class="form-group" ng-show="edit_challenge.weight_loss">
                              <label for="example-colorpicker" class="col-sm-3 control-label"> <p>Weight</p> </label>
                              <div class="col-sm-5 input-group">
                                 <input type="text" 
                                    value="" 
                                    placeholder="" 
                                    tabindex="7" 
                                    class="form-control" 
                                    name="weight_in_lbs" 
                                    id="weight_in_lbs"
                                    ng-model="edit_challenge.challenge_data.weight_in_lbs">
                                    <span class="input-group-btn">
                                       <span class="btn btn-default">lbs<span></span>
                              </div>
                        </div>

                     <div class="form-group">
                        <label for="example-colorpicker" class="col-sm-3 control-label">
                        <p ng-bind-html="edit_challenge.second"></p></label>
                        <div class="col-sm-5">
                           <input type="text" 
                              value="" 
                              class="form-control" 
                              name="daily_max_credit_calories" 
                              id="daily_max_credit_calories"
                              ng-model="edit_challenge.challenge_data.daily_max_credit_calories"
                              >
                        </div>
                     </div>
                  </div>
           </div>
       </div>
      <div class="col-md-6">
          <div class="block">
              <div class="block-title"><h2><strong> Edit Challenge Image</strong></h2>
              </div>
              <div class="form-group">
                  <div class="col-md-12">
                      <div class="row">
                        <div class="dropzone col-sm-6" 
                              upload-image-url="/api/v3/challenge_image/upload"
                              remove-image-url="/api/v3/challenge_image/remove/"
                              file="edit_challenge.challenge_data.image" 
                              upload-image-dropzone 
                              old-file-name = '@{{edit_challenge.challenge_data.image}}'>
                             <div class="dz-default dz-message"><span>Drop Image here to upload</span></div>                     
                        </div>
                        <div class="col-sm-6">
                            <img ng-src="/challenge-images/@{{edit_challenge.challenge_data.image}}" style=" object-fit: contain; height:auto; width:100%" alt="image">
                        </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </form>
</div>
