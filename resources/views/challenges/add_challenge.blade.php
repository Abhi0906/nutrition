 <!-- <div>
  
   <div class="block md_recommended_proui"  >
      <div class="block">
         <div class="block-title">
            <h2><strong>Information</strong></h2>
         </div>
         <form name="ChallengesForm" class="form-horizontal form-bordered">
            <div class="row">
                
                   
                  
               

                  
                  <div ng-show="add_challenge.calories">
                        <div class="form-group">
                           <label for="example-colorpicker" class="col-sm-4 control-label">  
                           <p ng-bind-html="add_challenge.first"></p></label>
                                    <div ng-show="add_challenge.start_unit" class="col-sm-4">
                                    <div  class="input-group">
                                       <input type="text" 
                                          value="" 
                                          tabindex="6" 
                                          placeholder="" 
                                          class="form-control" 
                                          name="target_calories" 
                                          id="target_calories"
                                          ng-model="add_challenge.target_calories">
                                          <span class="input-group-btn">
                                       <span class="btn btn-default">&nbsp;&nbsp;<span ng-bind-html="add_challenge.percentage"></span> &nbsp;&nbsp;</span></span>
                                    </div>
                                 </div>
                                 <div ng-hide="add_challenge.start_unit" class="col-sm-4">
                                    <input type="text" 
                                       value="" 
                                       placeholder="" 
                                       tabindex="6" 
                                       class="form-control" 
                                       name="target_calories" 
                                       id="target_calories"
                                       ng-model="add_challenge.target_calories">
                                 </div>
                        </div>
                        <div class="form-group">
                              <label for="example-colorpicker" class="col-sm-4 control-label"> <p ng-bind-html="add_challenge.second"></p> </label>
                              <div class="col-sm-4">
                                 <input type="text" 
                                    value="" 
                                    placeholder="" 
                                    tabindex="7" 
                                    class="form-control" 
                                    name="daily_max_credit_calories" 
                                    id="daily_max_credit_calories"
                                    ng-model="add_challenge.daily_max_credit_calories">
                              </div>
                        </div>
                  </div>
                </div>
            </div>
         </form>
      </div>
   </div>
</div -->

<div class="row block-title patient_title">
      <div class="col-sm-6">
         &nbsp;
         <h3><strong> Create Challenge</strong></h3>
      </div>
      <div class="col-sm-6 text-alignRight">
         <button class="btn btn-default" 
            ng-disabled="ChallengesForm.$invalid"
            ng-click="add_challenge.saveChallenge()"> 
         <i class="fa fa-angle-right"></i> Submit</button>
         <a ui-sref="list-challenge" class="btn btn-default" ata-original-title="Cancel"><span><i class="gi gi-remove_2"></i></span>&nbsp;Cancel</a>
      </div>
</div>
<div class="row">
  <form name="ChallengesForm" enctype="multipart/form-data" class="form-horizontal form-bordered">
    <div class="col-md-6">
        <!-- Basic Form Elements Block -->
        <div class="block">
            <div class="block-title">
              <h2>
              <strong> &nbsp; Challenge Information</strong>
              </h2>
            </div>
            <!-- Basic Form Elements Content -->
            

                <div class="form-group">
                    <label class="col-md-3 control-label" for="example-text-input">Title<span class="text-danger">&nbsp;*</span></label>
                    <div class="col-md-8">
                        <input type="text" type="text" 
                           value="" 
                           tabindex="1" 
                           placeholder="Text" 
                           class="form-control" 
                           name="title" 
                           id="title"
                           ng-model="add_challenge.title" required>
         
                    </div>
                    <div class="col-md-offset-3 col-md-9">
                        <span class="text-danger text-center" ng-show="ChallengesForm.title.$invalid && ChallengesForm.title.$touched">
                        Please enter title
                        </span>
                     </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-3 control-label" for="example-textarea-input">Description<span class="text-danger">&nbsp;*</span></label>
                    <div class="col-md-8">
                    <textarea id="example-textarea-input" 
                           name="description" 
                           rows="4" 
                           tabindex="2" 
                           class="form-control" 
                           placeholder="Content.."
                           ng-model="add_challenge.description"
                           required></textarea>
                    </div>
                     <div class="col-md-offset-3 col-md-9">
                        <span class="text-danger" ng-show="ChallengesForm.description.$invalid && ChallengesForm.description.$touched">Please enter description</span>
                     </div>
                </div>
                <div class="form-group">
                     <label class="col-md-3 control-label" for="example-text-input">Start<span class="text-danger">&nbsp;*</span></label>
                     <div class="col-md-6">
                        <div class="input-group">
                           <input type="text" class="form-control" 
                              name="start_time"
                              tabindex="3" 
                              datetime-picker="dd MMM yyyy hh:mm a" 
                              ng-model="add_challenge.challenge_data.start_datetime"
                              ng-init = "add_challenge.challenge_data.start_datetime = add_challenge.today_datetime"
                              is-open="add_challenge.isOpen_startDate"  
                              when-closed="add_challenge.dateCompareValidation(add_challenge.challenge_data.start_datetime, add_challenge.challenge_data.end_datetime)"
                              required/>
                           <span class="input-group-btn">
                           <button type="button" class="btn btn-default" ng-click="add_challenge.openCalendar_startDate($event, prop)">
                           <i class="fa fa-calendar"></i></button>
                           </span>
                        </div>
                     </div>
                     <div class="col-md-offset-3 col-md-9">
                        <span class="text-danger">@{{add_challenge.errMessage_startdate}}</span>
                        <br>
                        <span class="text-danger" ng-show="ChallengesForm.start_time.$invalid && ChallengesForm.start_time.$touched">Please select start time</span>
                     </div>
                </div>
                <div class="form-group">
                     <label class="col-md-3 control-label" for="example-text-input">End<span class="text-danger">&nbsp;*</span></label>
                     <div class="col-md-6">
                        <div class="input-group">
                           <input type="text" class="form-control" 
                              name="end_time"
                              tabindex="4" 
                              datetime-picker="dd MMM yyyy hh:mm a" 
                              ng-model="add_challenge.challenge_data.end_datetime" 
                              ng-init = "add_challenge.challenge_data.end_datetime = add_challenge.today_datetime"
                              is-open="add_challenge.isOpen_endDate"
                              when-closed="add_challenge.dateCompareValidation(add_challenge.challenge_data.start_datetime, add_challenge.challenge_data.end_datetime)"
                              required/>
                           <span class="input-group-btn">
                           <button type="button" class="btn btn-default" ng-click="add_challenge.openCalendar_endDate($event, prop)">
                           <i class="fa fa-calendar"></i></button>
                           </span>
                        </div>
                     </div>
                     <div class="col-md-offset-3 col-md-9">
                        <span class="text-danger">@{{add_challenge.errMessage_enddate}}</span>
                        <br>
                        <span class="text-danger" ng-show="ChallengesForm.end_time.$invalid && ChallengesForm.end_time.$touched">Please select end time</span>
                     </div>
                </div>
                <div class="form-group">
                    <label for="example-colorpicker" class="col-sm-3 control-label">  Allowed Devices<span class="text-danger">&nbsp;*</span></label>
                     <div class="col-sm-6">
                         <select 
                           data-placeholder="Choose a Devices"
                           chosen="add_challenge.allowed"
                           name="allowed_devices"
                           tabindex="5" 
                           ng-model="add_challenge.allowed_devices" multiple required>
                           <option value="all">All</option>
                           <option ng-repeat="allowedDev in vm.allowed" value="@{{allowedDev.id}}">@{{allowedDev.allowed_device}}</option>

                        </select>
                     </div>
                     <div class="col-md-offset-3 col-md-9">
                          <span class="text-danger">@{{add_challenge.errMessage_all_dev}}</span>
                     </div>
                </div>

                <div class="form-group">
                     <label class="col-md-3 control-label" for="example-text-input">Goal Type<span class="text-danger">&nbsp;*</span></label>
                     <div class="col-md-6">
                        <select class="form-control"  ng-click="add_challenge.goal_select()" 
                           ng-model="add_challenge.goal_type" name="goal_type" required>
                           <option value="">Select Goal Type</option>
                          <!--  <option value="body_fat_and_weight">Weight Loss & Body Fat %</option> -->
                          <!--  <option value="body_transformation">Body transformation</option> -->
                          <!--  <option value="calories">Calories Burned</option> -->
                        <!--    <option value="muscle_gain">Muscle Gain</option> -->
                           <option value="steps_run">Steps/Run</option>
                          <!--  <option value="weight_loss">Weight Loss</option> -->
                        </select>
                     </div>
                     <div class="col-md-offset-3 col-md-9">
                        <span class="text-danger text-center" ng-show="ChallengesForm.goal_type.$invalid && ChallengesForm.goal_type.$touched">
                        Please select goal type
                        </span>
                     </div>
                </div>


                <div ng-show="add_challenge.calories">
                        <div class="form-group">
                           <label for="example-colorpicker" class="col-sm-3 control-label">  
                           <p ng-bind-html="add_challenge.first"></p></label>
                                    <div ng-show="add_challenge.start_unit" class="col-sm-6">
                                      <div  class="input-group">
                                       <input type="text" 
                                          value="" 
                                          tabindex="6" 
                                          placeholder="" 
                                          class="form-control" 
                                          name="target_calories" 
                                          id="target_calories"
                                          ng-model="add_challenge.target_calories">
                                          <span class="input-group-btn">
                                       <span class="btn btn-default">&nbsp;&nbsp;<span ng-bind-html="add_challenge.percentage"></span> &nbsp;&nbsp;</span></span>
                                    </div>
                                 </div>
                                 <div ng-hide="add_challenge.start_unit" class="col-sm-6">
                                    <input type="text" 
                                       value="" 
                                       placeholder="" 
                                       tabindex="6" 
                                       class="form-control" 
                                       name="target_calories" 
                                       id="target_calories"
                                       ng-model="add_challenge.target_calories">
                                 </div>
                        </div>
                        
                        <div class="form-group" ng-show="add_challenge.weight_loss">
                              <label for="example-colorpicker" class="col-sm-3 control-label"> <p>Weight</p> </label>
                              <div class="col-sm-6 input-group">
                                 <input type="text" 
                                    value="" 
                                    placeholder="" 
                                    tabindex="7" 
                                    class="form-control" 
                                    name="weight_in_lbs" 
                                    id="weight_in_lbs"
                                    ng-model="add_challenge.weight_in_lbs">
                                    <span class="input-group-btn">
                                       <span class="btn btn-default">lbs<span></span>
                              </div>
                        </div>

                        <div class="form-group">
                              <label for="example-colorpicker" class="col-sm-3 control-label"> <p ng-bind-html="add_challenge.second"></p> </label>
                              <div class="col-sm-6">
                                 <input type="text" 
                                    value="" 
                                    placeholder="" 
                                    tabindex="7" 
                                    class="form-control" 
                                    name="daily_max_credit_calories" 
                                    id="daily_max_credit_calories"
                                    ng-model="add_challenge.daily_max_credit_calories">
                              </div>
                        </div>
                </div>

            <!-- END Basic Form Elements Content -->
        </div>
        <!-- END Basic Form Elements Block -->
    </div>
    <div class="col-md-6">
      <div class="block">
        <div class="block-title">
          <h2>
               <strong>Challenge Image</strong>
          </h2>
        </div>
        <div class="form-group">
            <div class="col-md-12">
               <div class="dropzone" 
                   upload-image-url="/api/v3/challenge_image/upload"
                   remove-image-url="/api/v3/challenge_image/remove/"
                   file="add_challenge.challenge_data.image" 
                   upload-image-dropzone >
                  <div class="dz-default dz-message"><span>Drop Image here to upload</span></div>                     
               </div>
            </div>
        </div>
      </div>
    </div>
  </form>
</div> 
