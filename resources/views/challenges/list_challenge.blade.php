<div class="row block-title patient_title" ng-init="list_challenge.challengeList();">
      <div class="col-sm-6">
        &nbsp; &nbsp;&nbsp;&nbsp;<h3><strong>Challenges</strong></h3>
      </div>  
      <div class="col-sm-6 text-alignRight">
         <a ui-sref="add-challenge" class="btn btn-default" ata-original-title="New Challenge"><span><i class="fa fa-plus"></i></span>&nbsp;New Challenge</a>

      </div>
</div>
<div class="block md_recommended_proui" >
    <div class="md_rec_food_class" id="mdrecommeded_meal_table">
      <table class="table recommeded_table">
        <thead>
          <tr class="">
            <th class="text-center bg-primary width_10">IMAGE</th>
            <th class="text-center bg-primary width_10">Title</th>
            <th class="text-center bg-primary width_15">Description</th>
            <th class="text-center bg-primary width_15">Allowed Devices</th>
            <th class="text-center bg-primary width_15">Start Date</th>
            <th class="text-center bg-primary width_15">End Date</th>
            <th class="text-center bg-primary width_10">IS EXPIRED</th>
            <th class="text-center bg-primary width_20">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="listchalleges in list_challenge.get_challenge | orderBy:'-start'">
            <td data-title="Image" class="text-center">
            <img height="60" width="60" 
                              alt="avatar" 
                              class="" 
                              ng-src="/challenge-images/@{{listchalleges.image}}">
             </td>
             <td data-title="Challeneg Name" class="text-center">
                <a href="javascript:void(0)" ui-sref="edit-challenge({id: listchalleges.id})">@{{listchalleges.title}}</a>
             </td>
             <td data-title="Description" class="text-center">@{{listchalleges.description}}</td>
             <td data-title="Allowed Device" class="text-center" >@{{listchalleges.allowed_devices}}</td>
             <td data-title="Start Date" class="text-center">@{{listchalleges.start_date  | custom_date_format: 'dd-MMM-yy hh:mm a' }}</td>
             <td data-title="" class="text-center">@{{listchalleges.end_date | custom_date_format: 'dd-MMM-yy hh:mm a' }}</td>
               <td data-title="IS EXPIRED" class="text-center">
                  <span ng-if="listchalleges.is_expired==0">No</span>
                  <span ng-if="listchalleges.is_expired==1">Yes</span>
               </td>

              <td data-title="ACTION" class="text-center">
                <div class="btn-group btn-group-xs">
                  
                   <a class="btn btn-default" href="javascript:void(0)" ui-sref="edit-challenge({id: listchalleges.id})" data-original-title="Edit">
                    <i class="fa fa-pencil-square-o"></i>
                  </a>
                  <a class="btn btn-danger" title="" ng-click="list_challenge.deleteChallenged(listchalleges)" data-toggle="tooltip" href="javascript:void(0)" data-original-title="Delete">
                  <i class="fa fa-times"></i>
                  </a>
                  </a>
                </div>
              </td>
          </tr>
          <tr class="text-center" ng-if="list_challenge.get_challenge.length==0">
          <td colspan="8">
            No Challenge added yet
          </td>
        </tr>
        </tbody>
      </table>
    </div>
    <!-- END Block Content -->
</div>