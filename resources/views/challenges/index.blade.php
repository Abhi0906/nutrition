@extends('layout.app')
@section('content')

<div ng-controller="ChallengeController as vm">
      <!-- Main Row -->
    <div class="row" ng-init="vm.allowedDeviceList();" >
        <div class="col-xs-12">
            <div class="row">
              <div ui-view></div>
            </div>
        </div>
    </div>
    
</div>

<!-- END Main Row -->
@endsection
@section('page_scripts')

{!! HTML::script("js/controllers/ChallengeController.js") !!}
{!! HTML::script("js/controllers/ChallengeAddController.js") !!}
{!! HTML::script("js/controllers/ChallengeEditController.js") !!}
{!! HTML::script("js/directives/chosen.js") !!}
{!! HTML::script("js/filters/dateFormatFilter.js") !!}
{!! HTML::script("js/directives/uploadImageDropZone.js") !!}

@append
@section('jquery_ready')
@append