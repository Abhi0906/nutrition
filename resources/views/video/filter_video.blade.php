
<div class="block full">
	<div class="block-title clearfix">
        <div class="block-options">
            <h2>FIND WORKOUTS</h2>
        </div>
    </div>
	<div  class="container-fluid">
		<div class="row-fluid">
			<div class="" style="border:none">
	            <label for="keywords">Keyword(s)</label>
	            <input type="text" id="keywords" name="keywords" ng-model="keyword" class="form-control" placeholder="enter search term(s)">
	        </div>	
		</div>
		<div class="row-fluid">
			<div class="" style="border:none">
	            <label class="space-top">Workout time in minutes</label>
	            <div >             
	            	<div class="col-xs-6">            		
				            <small>Minimum</small>
				            <input type="number" ng-model="minimun_workout_time" id="min" name="min" class="form-control" placeholder="">		        
	            	</div>
	            	<div class="col-xs-6">            		
				        <small>Maximum</small>
				        <input type="number" id="max" name="max" ng-model="maximum_workout_time" class="form-control" placeholder="">		        
	            	</div>
	            </div>	
	        </div>		
		</div>	
		<div class="row-fluid">
			<div class="" style="border:none">
	            <label class="space-top">Calorie burn</label> 
	            <div >           
	            	<div class="col-xs-6">            		
			            <small>Minimum</small>
			            <input type="number" id="minCal" name="minCal" ng-model="minimun_calorie_burn" class="form-control" placeholder="">	        
	            	</div>
	            	<div class="col-xs-6">            		
				        <small>Maximum</small>
				        <input type="number" id="maxCal" name="maxCal" ng-model="maximum_calorie_burn" class="form-control" placeholder="">	        
	            	</div>
	            </div>	
	        </div>		
		</div>
		<div class="row-fluid">
			<div class="" style="border:none">
	            <label class="space-top">Difficulty</label> 
	            <div class="col-xs-12">            
		        	<label class="checkbox-inline"
		        		   for="example-inline-checkbox1" ng-repeat="diffculty in vm.diffculties">
		                <input type="checkbox" id="example-inline-checkbox@{{diffculty }}" name="example-inline-checkbox@{{diffculty }}" value="@{{diffculty }}"> @{{ diffculty }}
		            </label>
		            
	        	</div>
	        </div>		
		</div>
		<div class="row-fluid">
			<div class="" style="border:none">
	            <label class="space-top">Body focus</label>
	            <div>
	            <div class="col-xs-12" ng-repeat="body in vm.body_focus">            
		        	<div class="checkbox">
                        <label for="@{{body }}">
                            <input type="checkbox" id="@{{body }}" name="@{{body }}" value="@{{body }}"> @{{ body }}
                        </label>
                    </div>
   	        	</div>
	        	</div>
	        </div>		
		</div>
		<div class="row-fluid">
			<div class="" style="border:none">
	            <label class="space-top">Training type</label>
				<div>
	            <div class="col-xs-12">    
	            	<div class="checkbox" ng-repeat="training_type in vm.training_types" >
                        <label for="example-checkbox@{{training_type.id }}">
                            <input type="checkbox" id="example-checkbox@{{training_type.id }}" name="example-checkbox@{{training_type.id }}" value="@{{training_type.display_name }}"> @{{training_type.display_name }}
                        </label>
                    </div>
                            
	        	</div>
	            </div>
	        </div>		
		</div>
		<div class="row-fluid">
			<div class="" style="border:none">
	            <label class="space-top">Equipment needed</label>
	            <div class="col-xs-12">
					<div class="checkbox" ng-repeat="equipment in vm.equipments">
	                    <label for="example-checkbox@{{equipment.id }}">
	                        <input type="checkbox" id="example-checkbox@{{equipment.id }}" name="example-checkbox@{{equipment.id }}" value="@{{equipment.display_name }}"> @{{equipment.display_name }}
	                    </label>
	                </div>
	            </div>
	        </div>		
		</div>
		
	</div>
	
</div>
