<div class="block-title patient_title">
             <div class="col-sm-2">
                <h2><strong>Videos</strong> <!-- <span ng-if ="listing.videos.length > 0">(@{{listing.videos.length}})</span> --></h2>
             </div>  
</div>
<div class="row">
    <div class="col-md-4 col-lg-3">
      <search-video block-title="FIND WORKOUTS" videos="listing.videos" un-categorised></search-video>
    </div>
    <div class="col-md-8 col-lg-9">
       <div class="block">
            <!-- Lightbox Gallery with Options Title -->
            <div class="block-title">
                <div class="block-options pull-right">
                        <a title="Fetch new videos from Vimeo"
                            data-toggle="button"
                            class="btn btn-alt btn-sm btn-default toggle-bordered enable-tooltip"
                            href="javascript:void(0)"
                            ng-click = "listing.fetch_videos()"
                            >Fetch Videos</a>
                </div>
                <h2><strong>Videos</strong> <span ng-if ="listing.videos.length > 0">(@{{listing.videos.length}})</span></h2>
            </div>
            <!-- END Lightbox Gallery with Options Title -->

            <!-- Lightbox Gallery with Options Content -->
            <div class="gallery container animation-fadeInQuick" slim-scroll>
                <div ng-if="vm.isloading">
                  @include('layout.loading')
                </div>
                <div ng-show="listing.videos.length > 0" ng-if="$index%3==0" ng-repeat="xyz in listing.videos" >                     
                    <div  class="row row-eq-height" >
                        <single-video edit="listing.editVideo(video)" remove ="listing.deleteVideo(video)" ng-if="($index>=$parent.$index) && ($index<=($parent.$index+2))" 
                        class="col-sm-4" ng-repeat="video in listing.videos" video="video" width="250" height="150" >                            
                        </single-video>
                    </div>
                    <br>
                </div>
                <div class="alert alert-danger animation-fadeInQuick" ng-cloak ng-show="listing.videos.length <= 0">
                    <button data-dismiss="alert" class="close" type="button">
                      <i class="ace-icon fa fa-times"></i>
                    </button>

                    <strong>
                      No Videos available!
                    </strong>
                </div>                            
            </div>
            <!-- END Lightbox Gallery with Options Content -->
        </div>
    </div>
</div>
