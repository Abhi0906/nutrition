<div class="row block-title patient_title">
 <div class="col-sm-6">
    <h3><strong>  &nbsp;&nbsp; Edit @{{setting.video.name}} </strong></h3>
 </div>
    <div class="col-sm-6 text-alignRight">
			 <h3>
			<div class="form-group">
			    <a href="video" class="btn btn-default">
  				   <span><i class="gi gi-remove_2"></i></span>&nbsp;Cancel
  				</a>
			</div>
		</h3>
    </div>
</div>

<div class="row">
	<div class="col-md-3">

		<div class="block full">
			<div class="block-title clearfix">
		        <div class="block-options">
		            <h2>Edit Settings</h2>
		        </div>
		    </div>
			<div  class="container" slim-scroll low-margin>
				<div class="row-fluid">
					<div class="" style="border:none">
			            <label class="space-top">Workout time in minutes</label>
			            <div class="col-xs-12" >
			            	<input type="text"
			            	       ng-model="setting.video.workout_time"
			            	   	   class="form-control"
			            	   	   ng-blur ="setting.saveSetting()"
			            	       placeholder="">
   			            </div>
			        </div>
				</div>
				<div class="row-fluid">
					<div class="" style="border:none">
			            <label class="space-top">Calorie burn</label>
			            <div >
			            	<div class="col-xs-6">
					            <small>Minimum</small>
					            <input type="text"
		             		    	   ng-model="setting.video.minimun_calorie_burn"
		             		    	   ng-blur ="setting.saveSetting()"
					                class="form-control" placeholder="">
			            	</div>
			            	<div class="col-xs-6">
						        <small>Maximum</small>
						        <input type="text"
						        	   class="form-control"
						               placeholder=""
						               ng-blur ="setting.saveSetting()"
						               ng-model="setting.video.maximum_calorie_burn">
			            	</div>
			            </div>
			        </div>
				</div>

				<div class="row-fluid">
					<div class="" style="border:none">
			            <label class="space-top">Experience</label>
			                <br>
			                <div >
					        	<select
				                    chosen="vm.experiences"
				                    data-placeholder="Choose Experience Level"
				                    ng-options="value for (key,value) in vm.experiences"
				                    ng-model="setting.video.experience" >
		                		</select>
				            </div>
				    </div>
				</div>
				<div class="row-fluid">
					<div class="" style="border:none">
			            <label class="space-top">Body Parts</label>
			                <br>
			                <div >
					        	<select
						        	chosen="vm.body_parts"
				                    data-placeholder="Choose a Body Parts"
				                    ng-options="body.name as body.name group by body.parent_name for body in vm.body_parts"
				                    ng-model="setting.video.body_parts" >
				                  </select>
				            </div>
				    </div>
				</div>

				<div class="row-fluid">
					<div class="" style="border:none">
			            <label class="space-top">Equipment</label>
			            <div class="col-xs-12">
			            	<select id="equipment"
				                    name="equipment"
				                    chosen="vm.equipments"
				                    data-placeholder="Choose a Equipment"
				                    ng-options="equipment.display_name as equipment.display_name for equipment in vm.equipments"
				                    ng-model="setting.video.equipments" >
		                    </select>
			            </div>
			        </div>
				</div>
				<div class="row-fluid hide">
                    <div class="col-lg-9 col-lg-offset-3 col-xs-12 space-top">
                        <button class="btn btn-sm btn-primary" type="submit" ng-click="setting.saveSetting();"><i class="fa fa-angle-right"></i> Save</button>
                        <button class="btn btn-sm btn-warning" type="reset"><i class="fa fa-repeat"></i> Reset</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-9">
		<div class="row">
			<div class="col-xs-8">
				<simple-block block-title="@{{setting.video.name}}">
				   <div class="embed-responsive embed-responsive-16by9" ng-bind-html = 'setting.getEmbedCode(setting.video)'></div>
				 </simple-block>
			</div>
			<div class="col-xs-4">
				<simple-block block-title="Assigned Properties">

					<table class="table table-borderless table-striped table-vcenter">
	                    <tbody>
	                        <tr>
	                            <td style="width: 50%;" class="text-left"><strong>Workout Time</strong></td>
	                            <td><span class="text-success"><strong>@{{setting.video.workout_time}}</strong> </span></td>
	                        </tr>
	                        <tr>
	                            <td class="text-left"><strong>Calorie burn</strong></td>
	                            <td>
	                            	<span class="text-success">
	                            		<strong>@{{setting.video.minimun_calorie_burn}}</strong>
	                            	</span>
	                            	to
	                            	<span class="text-success">
	                            		<strong>@{{setting.video.maximum_calorie_burn}}</strong>
	                            	</span>
	                           </td>
	                        </tr>
	                        <tr>
	                            <td class="text-left"><strong>Experience</strong></td>
	                            <td><span class="text-success"><strong>@{{setting.video.experience}}</strong> </span></td>
	                        </tr>
	                        <tr>
	                            <td class="text-left"><strong>Body Parts</strong></td>
	                            <td><span class="text-success"><strong>@{{setting.video.body_parts}}</strong> </span></td>
	                        </tr>
	                        <tr>
	                            <td class="text-left"><strong>Equipment</strong></td>
	                            <td><span class="text-success"><strong>@{{setting.video.equipments}}</strong> </span></td>
	                        </tr>
	                    </tbody>
	                </table>
				 </simple-block>
			</div>
		</div>
	</div>
</div>
