@extends('layout.app')

@section('content')
    <div ng-controller="VideoController as vm">
    <!-- <ul class="breadcrumb breadcrumb-top">
        <li>Pages</li>
        <li><a ng-click='vm.listView=true' href="javascript:void(0)">Videos</a></li>
    </ul> -->
    <!-- END Files Header -->

    <!-- Main Row -->
   
      
    <div class="row" >
       
        <div class="col-xs-12">            
            <div class="row" ng-controller="VideoListingController as listing" ng-show="vm.listView">
                @include('video.video_listing')
            </div>
            <div ng-controller="VideoSettingController as setting" class="row" ng-hide="vm.listView">
                @include('video.video_setting')
            </div>
        </div>
    </div>
    </div>

    
    <!-- END Main Row -->


@endsection
@section('page_scripts')
{!! HTML::script("js/controllers/VideoController.js") !!}
{!! HTML::script("js/directives/searchVideo.js") !!}
{!! HTML::script("js/directives/chosen.js") !!}
@append

@section('jquery_ready')

$('.scroll_column').slimScroll({
    height: $(window).height()*.63,

});

@append