<div class="row block-title patient_title">
    <div class="col-sm-6">
        <h3><strong> @{{edit_workout.workoutname_title}} </strong></h3>
    </div>  
    <div class="col-sm-6 text-alignRight">
        <h3>   
            <div class="form-group" ng-show="edit_workout.Edit_Form_Details">
              <a ng-click="edit_workout.editformShowHide()" class="btn btn-default"> 
              <span><i class="fa fa-pencil-square-o"></i></span>&nbsp;Edit</a>
              <a ui-sref="list-workouts" class="btn btn-default"> 
              <span><i class="gi gi-remove_2"></i></span>&nbsp;Cancel</a>
            </div> 
            <div class="form-group" ng-show="edit_workout.Edit_Form">
              <a ng-disabled="WorkoutEditForm.$invalid" ng-click="edit_workout.updateWorkout(WorkoutEditForm.$valid)" class="btn btn-default"> 
              <span><i class="fa fa-pencil-square-o"></i></span>&nbsp;Update</a>
              <a ui-sref="list-workouts" class="btn btn-default"> 
              <span><i class="gi gi-remove_2"></i></span>&nbsp;Cancel</a>
            </div> 
        </h3>
    </div>
</div>
<div class="row" ng-show="edit_workout.Edit_Form_Details">
    <div class="col-md-5 hidden-xs">
        <div class="row">
            <div class="col-md-6">
                @include('workout.workout_filter')
            </div>
            <div class="col-md-6 ">
                <simple-block block-title="@{{edit_workout.ResultTitle}} (@{{edit_workout.filterData.length}})">
                    <div class="gallery container" low-margin slim-scroll >
                        <div class="row">
                        <div ng-if="edit_workout.filterLoading">
                          @include('layout.loading')
                        </div>                                   
                        <single-video-image class="col-xs-12 space-top"
                                       not-editable
                                       not-deletable
                                       videoimage="video_image"
                                       width="250"
                                       height="140" 
                                       ng-repeat="video_image in edit_workout.filterData"
                                       dnd-draggable="video_image"
                                       dnd-effect-allowed="move"
                                       view = "vm.viewExercise(video_image)">                            
                         </single-video-image>                                 
                        </div>
                    </div>
                </simple-block>
            </div>
        </div>
    </div>
    
    <div class="col-md-7 col-xs-12">
        <div class="block full">
              <div class="block-title">
                  <h2><strong ng-bind-html="vm.getWorkoutTitle(edit_workout.selectedWorkout);"></strong></h2>
              </div>
                <div class="gallery container-fluid">
                  <div class="simpleDemo row">
                      <div class="col-md-12">

                           <!-- The dnd-list directive allows to drop elements into it.
                             The dropped data will be added to the referenced list -->
                         
                              <ul class="row" dnd-list="edit_workout.exercise_list"
                                  dnd-horizontal-list="true"
                                  dnd-inserted="edit_workout.exerciseInserted(event, index, item, edit_workout.selectedWorkout.id)">
                                  <!-- The dnd-draggable directive makes an element draggable and will
                                       transfer the object that was assigned to it. If an element was
                                       dragged away, you have to remove it from the original list
                                       yourself using the dnd-moved attribute -->
                                  <li class="col-md-4" ng-repeat="exercise in edit_workout.exercise_list"
                                      dnd-draggable="exercise"
                                       dnd-moved="edit_workout.exercise_list.splice($index, 1)"
                                      dnd-effect-allowed="move"
                                      dnd-selected="edit_workout.selecedExercise = exercise"
                                      ng-class="{'selected': edit_workout.selecedExercise === exercise}"
                                      >
                                     
                                       <single-video-image  not-editable
                                               remove ="edit_workout.deleteExercise(exercise, edit_workout.selectedWorkout.id)"
                                               view = "vm.viewExercise(exercise)"
                                                videoimage="exercise"
                                                width="250" height="150"
                                                >                            
                                              </single-video-image>

                                  </li>
                              </ul>
                             

                      </div>
                  </div>

                  
              </div>
    
        </div>
    </div>
</div>
<div class="row" ng-show="edit_workout.Edit_Form">
    <form name="WorkoutEditForm" enctype="multipart/form-data" class="form-horizontal form-bordered">
            <div class="col-md-6">
                <div class="block">
                    <div class="block-title">
                       <h2><strong>Workout Information</strong></h2>
                    </div>
                    
                   <div class="form-group" ng-class="{ 'has-error' : WorkoutEditForm.title.$invalid && !WorkoutEditForm.title.$pristine }">
                        <label for="title">Title
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text"
                               placeholder="Enter Title.."
                               class="form-control" 
                               name="title"
                               ng-model="edit_workout.selectedWorkout.title" 
                               id="title" required>
                        
                        <span class="text-danger text-center" ng-show="WorkoutEditForm.title.$invalid && WorkoutEditForm.title.$touched">
                        Please enter title
                        </span>
                    </div>
                    <div class="form-group">
                            <label for="description">Description<span class="text-danger"></span></label>
                              <textarea placeholder="Content.."
                               class="form-control" 
                               rows="3" 
                               name="description" 
                               ng-model="edit_workout.selectedWorkout.description"
                               id="description"></textarea>
                    </div>
                    <div class="form-group">
                            <label for="body_part_id">Body Parts</label>
                            <span class="text-danger">*</span>
                                <select 
                                chosen="vm.body_parts"
                                data-placeholder="Choose a Body Parts"
                                ng-options="body.name as body.name group by body.parent_name for body in vm.body_parts"
                                ng-model="edit_workout.selectedWorkout.body_part"
                                id="body_part_id"
                                name="body_part"
                                required>
                              </select>     
                            <span class="text-danger"
                                   ng-show="WorkoutEditForm.body_part.$invalid && WorkoutEditForm.body_part.$touched">
                                 You must select your body part.
                            </span>
                    </div>
                    <div class="form-group">
                            <label for="exercise_level_id">Experience Levels</label>
                            <select 
                                    chosen="vm.experiences"
                                    data-placeholder="Choose Experience Level"
                                    ng-options="value for (key,value) in vm.experiences"
                                    ng-model="edit_workout.selectedWorkout.experience"
                                    id="exercise_level_id">
                            </select>       
                            <span class="help-block hide">Please enter your experience levels</span>
                    </div>
                </div>
            </div>
            
          <div class="col-md-6">
           <div class="block">
              <div class="block-title"><h2><strong>Workout Image</strong></h2>
              </div>
              <div class="form-group">
                  <div class="col-md-12">
                      <div class="row">
                        <div class="dropzone col-sm-6" 
                              upload-image-url="/api/v3/workout_image/upload"
                              remove-image-url="/api/v3/workout_image/remove/"
                              file="edit_workout.selectedWorkout.image" 
                              upload-image-dropzone 
                              old-file-name = '@{{edit_workout.selectedWorkout.image}}'>
                             <div class="dz-default dz-message"><span>Drop Image here to upload</span></div>                     
                        </div>
                        <div class="col-sm-6">
                            <img ng-src="/workout-images/@{{edit_workout.selectedWorkout.image}}" style=" object-fit: contain; height:auto; width:100%" alt="image">
                        </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </form>
</div> 