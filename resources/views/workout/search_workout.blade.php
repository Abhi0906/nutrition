<div class="block" ng-init="list_workout.showAddWorkout=true">
    <div class="block-title">
        <div class="block-options pull-right">
            <a ng-show="!list_workout.showAddWorkout" href="javascript:void(0)"
              class="btn btn-alt btn-sm btn-default"
              data-toggle="tooltip" 
              title=""
              data-original-title="Find Workout"
              ng-click="list_workout.showAddWorkout = !list_workout.showAddWorkout;list_workout.addWorkout()"
              >
              <i class="fa fa-search"></i>
            </a>
             <!-- <a ng-show="!!list_workout.showAddWorkout" href="javascript:void(0)"
              class="btn btn-alt btn-sm btn-default"
              data-toggle="tooltip" 
              title=""
              data-original-title="Add Workout"
              ng-click="list_workout.showAddWorkout = !list_workout.showAddWorkout;"
              >
              <i class="fa fa-plus"></i>
            </a> -->
        </div>
        <h3 ng-show="!!list_workout.showAddWorkout"><i class="fa fa-search fa-fw"></i> 
        <strong><span style="font-size:14px!important;">FIND WORKOUTS</span></strong></h3>
        <h2 ng-show="!list_workout.showAddWorkout"><i class="fa fa-tasks fa-fw"></i> <strong>SAVE WORKOUT</strong></h2>
    </div>
    
    <div class="container-fluid">
      <!-- Add Workout -->
      <div ng-show="!list_workout.showAddWorkout">
        <form name="saveWorkoutForm" novalidate="novalidate">
          <div class="form-group" ng-class="{ 'has-error' : saveWorkoutForm.title.$invalid && !saveWorkoutForm.title.$pristine }">
              <label >Title <sup class="text-danger">*</sup></label>
              <input type="text" name="title" placeholder="Enter Title.." class="form-control" ng-model="list_workout.selectedWorkout.title" ng-minlength="3" required>
              <span ng-show="saveWorkoutForm.title.$invalid && !saveWorkoutForm.title.$pristine" class="help-block">Workout title min. 3 character is required</span>
          </div>
          <div class="form-group">
              <label >Description</label>
              <textarea 
                       placeholder="Content.."
                       class="form-control"
                       rows="3"
                       ng-model="list_workout.selectedWorkout.description">
              </textarea>
          </div>
          <div class="form-group" ng-class="{'has-error':saveWorkoutForm.body_parts.$invalid && !saveWorkoutForm.body_parts.$pristine }">
            <label class="space-top">Select body part <sup class="text-danger">*</sup></label>
            <select 
                  name="body_parts"
                  chosen="vm.body_parts"
                  data-placeholder="Choose a Body Parts"
                  ng-options="body.name as body.name group by body.parent_name for body in vm.body_parts"
                  ng-model="list_workout.selectedWorkout.body_part" required>
            </select>
            <p ng-show="saveWorkoutForm.body_parts.$invalid && !saveWorkoutForm.body_parts.$pristine" class="help-block">select body part</p>
          </div>
          <div class="form-group" ng-class="{ 'has-error' : saveWorkoutForm.experience_levels.$invalid && !saveWorkoutForm.experience_levels.$pristine }">
            <label class="space-top">Experience Levels <sup class="text-danger">*</sup></label>
             <select 
                    name="experience_levels"
                    chosen="vm.experiences"
                    data-placeholder="Choose Experience Level"
                    ng-options="value for (key,value) in vm.experiences"
                    ng-model="list_workout.selectedWorkout.experience" required>
              </select>

            <p ng-show="saveWorkoutForm.experience_levels.$invalid && !saveWorkoutForm.experience_levels.$pristine" class="help-block">select experience level</p>
          </div>
          <div class="form-group">  
              <div class="experience space-top">
                <legend><strong>Experience level legend</strong></legend>
                <div style="margin-left:15px">
                  <span>
                  <i class="fa fa-square text-primary"></i>&nbsp;&nbsp;
                  <label for="beginner">Beginner</label>
                 </span>
                  <br />
                  <span>
                  <i class="fa fa-square text-warning"></i>&nbsp;&nbsp;
                  <label for="intermediate">Intermediate</label>
                  </span>
                  <br />
                  <span>
                  <i class="fa fa-square text-success"></i>&nbsp;&nbsp;
                  <label for="advanced">Advanced</label> 
                  </span>
                </div>
              </div>
          </div>
          <div class="form-group form-actions text-center" >
              <button class="btn btn-sm btn-primary" type="button" ng-click="list_workout.saveWorkout(saveWorkoutForm.$valid)" ng-disabled="saveWorkoutForm.$invalid">Save</button>
              <button class="btn btn-sm btn-warning" type="button" ng-click="list_workout.selectedWorkout = null;vm.showAddWorkout = !vm.showAddWorkout;">Cancel</button>
          </div>
          <span class="text-danger" ng-hide="saveWorkoutForm.$valid">&nbsp;&nbsp;(Fields with * are mandatory.)</span>
        </form>
      </div>
      <!-- End Add Workout-->

      <!-- all workouts  -->
      <div ng-show ="!!list_workout.showAddWorkout">
          <div class="form-group">
              <label for="keyword">Keyword(s)</label>
              <input type="text" id="keywords" name="keywords" ng-model="keyword" class="form-control" placeholder="enter search term(s)">
          </div>
          <div class="form-group">
              <label for="body_parts">Body Parts</label>
              <div >       
                <select 
                chosen="vm.body_parts"
                      data-placeholder="Choose a Body Parts"
                      ng-options="body.name as body.name group by body.parent_name for body in vm.body_parts"
                      ng-model="body_part" >
                </select>
              </div>
          </div>
          <div class="form-group">
              <label for="keyword">Experience Levels</label>
              <div >       
                <select 
                      chosen="vm.experiences"
                      data-placeholder="Choose Experience Level"
                      ng-options="value for (key,value) in vm.experiences"
                      ng-model="experience" >
                </select>
              </div>
          </div>
          <div class="form-group">  
              <div class="experience space-top">
                <legend><strong>Experience level legend</strong></legend>
                <div style="margin-left:15px">
                  <span>
                  <i class="fa fa-square text-primary"></i>&nbsp;&nbsp;
                  <label for="beginner">Beginner</label>
                 </span>
                  <br />
                  <span>
                  <i class="fa fa-square text-warning"></i>&nbsp;&nbsp;
                  <label for="intermediate">Intermediate</label>
                  </span>
                  <br />
                  <span>
                  <i class="fa fa-square text-success"></i>&nbsp;&nbsp;
                  <label for="advanced">Advanced</label> 
                  </span>
                </div>
              </div>
          </div>
          <div class="form-group form-actions">
            <div class="col-md-9 col-md-offset-3">
                <button class="btn btn btn-default" type="button" ng-click="list_workout.clearFiler()"><i class="fa fa-repeat"></i> &nbsp;&nbsp;Clear Filter</button>
            </div>
        </div>
      </div>
      <!-- END all workouts -->
    </div>
</div>