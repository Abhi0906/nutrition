<!-- Siple Block -->
<div class="block full">
        <!-- Simple Title -->
        <div class="block-title"> 
    	    <div class="block-options pull-right">
                    <a title="Clear Filter"
                     class="btn btn-alt btn-xs btn-default"
                      href="javascript:void(0)"
                       ng-click="edit_workout.clearFilter()" 
                      >Clear Filter</a>
            </div>       
            <h2><strong>Find Exercise</strong></h2>
        </div>
        <!-- END Simple Title -->

        <!-- Simple Content -->
        <!-- The content you will put inside div.block-content, will be toggled -->
        <div  class="container-fluid">

	        <div class="row-fluid">
				
	            <label class="control-label col-xs-2 col-md-3" style="padding-left:0px !important; padding-top:7px;">Type
	            </label> 
				<div class="col-xs-10 col-md-9" style="padding:0px 0px !important;">	
		            <select class="form-control"
		            		 ng-model="edit_workout.workout_type"
		            		 ng-change="edit_workout.changeFilter();">
		            	<option value="exercise_images">Exercise Images</option>
		            	<option value="videos">Videos</option>
		            </select>	
	            
		    	</div>		
			</div>
			<div class="row-fluid">
				<div class="" style="border:none">
		            <label for="keywords" class="space-top">Keyword(s)</label>
		            <input type="text" id="keywords" name="keywords" ng-model="keyword" class="form-control" placeholder="enter search term(s)">
		        </div>	
			</div>

			<div class="row-fluid" ng-show="edit_workout.workout_type == 'exercise_images'">
				<div class="" style="border:none">
		            <label for="keywords" class="space-top">Exercise Category</label>
     	            <select 
                            chosen="vm.exercise_categories"
                            data-placeholder="Choose a Exercise Category"
                            ng-options="value for (key,value) in vm.exercise_categories"
                            ng-model="exercise_type"
                            id="exercise_type_id"
                            name="exercise_type"
                    ></select>
		        </div>	
			</div>
		
			<div class="row-fluid">
				<div class="" style="border:none">
		            <label class="space-top">Workout time minutes</label>
		            <div >             
		            	<div class="col-xs-6">            		
					            <small>Minimum</small>
					            <input type="text" ng-model="minimun_workout_time" id="min" name="min" class="form-control" placeholder="">		        
		            	</div>
		            	<div class="col-xs-6">            		
					        <small>Maximum</small>
					        <input type="text" id="max" name="max" ng-model="maximum_workout_time" class="form-control" placeholder="">		        
		            	</div>
		            </div>	
		        </div>		
			</div>	
			<div class="row-fluid">
				<div class="" style="border:none">
		            <label class="space-top">Calorie burn</label> 
		            <div >           
		            	<div class="col-xs-6">            		
				            <small>Minimum</small>
				            <input type="text" id="minCal" name="minCal" ng-model="minimun_calorie_burn" class="form-control" placeholder="">	        
		            	</div>
		            	<div class="col-xs-6">            		
					        <small>Maximum</small>
					        <input type="text" id="maxCal" name="maxCal" ng-model="maximum_calorie_burn" class="form-control" placeholder="">	        
		            	</div>
		            </div>	
		        </div>		
			</div>
			<div class="row-fluid">
				<div class="" style="border:none">
		            <label class="space-top">Body Parts</label> 
		                <br>
		                <div >       
				        	<select 
				        	chosen="vm.body_parts"
		                    data-placeholder="Choose a Body Parts"
		                    ng-options="body.name as body.name group by body.parent_name for body in vm.body_parts"
		                    ng-model="body_part" >
		                  </select>
			            </div>
			    </div>		
			</div>
			<div class="row-fluid">
				<div class="" style="border:none">
		            <label class="space-top">Experience Levels</label> 
		                <br>
		                <div >       
				        	<select 
		                    chosen="vm.experiences"
		                    data-placeholder="Choose Experience Level"
		                    ng-options="value for (key,value) in vm.experiences"
		                    ng-model="experience" >
		                </select>
			            </div>
			    </div>		
			</div>
			<div class="row-fluid">
				<div class="" style="border:none">
		            <label class="space-top">Equipments</label>
					<br>
		            <div>    
		                <select 
		                    chosen="vm.equipments"
		                    data-placeholder="Choose a Equipment"
		                    ng-options="equipment.display_name as equipment.display_name for equipment in vm.equipments"
		                    ng-model="equipment" >
		                </select>
	               	</div>
		        </div>		
			</div>
		
		
	   </div>
        
        <!-- END Simple Content -->
</div>
<!-- END Simple Block -->    



