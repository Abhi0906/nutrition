<div class="row block-title patient_title">
      <div class="col-sm-6">
        <h3><strong>Create Workout&nbsp;</strong></h3>
      </div>  
      <div class="col-sm-6 text-alignRight">
                    <button class="btn btn-default" ng-disabled="WorkoutForm.$invalid"
                     ng-click="add_workout.saveWorkout(WorkoutForm.$valid)">
                     <i class="fa fa-angle-right"></i> Save & Close</button>
                     <a ui-sref="list-workouts" class="btn btn-default"> <i class="gi gi-remove_2"></i>&nbsp;Cancel</a>
      </div>
</div>
<div class="row">
    <form name="WorkoutForm" enctype="multipart/form-data" class="form-horizontal form-bordered">
            <div class="col-md-6">
                <div class="block">
                    <div class="block-title">
                       <h2><strong>Workout Information</strong></h2>
                    </div>
                    
                   <div class="form-group" ng-class="{ 'has-error' : WorkoutForm.title.$invalid && !WorkoutForm.title.$pristine }">
                        <label for="title">Title
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text"
                               placeholder="Enter Title.."
                               class="form-control" 
                               name="title"
                               ng-model="add_workout.selectedWorkout.title" 
                               id="title" required>
                        
                        <span class="text-danger text-center" ng-show="WorkoutForm.title.$invalid && WorkoutForm.title.$touched">
                        Please enter title
                        </span>
                    </div>
                    <div class="form-group">
                            <label for="description">Description<span class="text-danger"></span></label>
                              <textarea placeholder="Content.."
                               class="form-control" 
                               rows="3" 
                               name="description" 
                               ng-model="add_workout.selectedWorkout.description"
                               id="description"></textarea>
                        
                    </div>
                    <div class="form-group">
                            <label for="body_part_id">Body Parts</label>
                            <span class="text-danger">*</span>
                                <select 
                                chosen="vm.body_parts"
                                data-placeholder="Choose a Body Parts"
                                ng-options="body.name as body.name group by body.parent_name for body in vm.body_parts"
                                ng-model="add_workout.selectedWorkout.body_part"
                                id="body_part_id"
                                name="body_part"
                                required>
                              </select>     
                            <span class="text-danger"
                                   ng-show="WorkoutForm.body_part.$invalid && WorkoutForm.body_part.$touched">
                                 You must select your body part.
                            </span>
                    </div>
                    <div class="form-group">
                            <label for="exercise_level_id">Experience Levels</label>
                            <select 
                                    chosen="vm.experiences"
                                    data-placeholder="Choose Experience Level"
                                    ng-options="value for (key,value) in vm.experiences"
                                    ng-model="add_workout.selectedWorkout.experience"
                                    id="exercise_level_id">
                            </select>       
                            <span class="help-block hide">Please enter your experience levels</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="block">
                    <div class="block-title">
                        <h2><strong>Workout Image</strong></h2>
                    </div>
                    <div class="dropzone" 
                           upload-image-url="/api/v3/workout_image/upload"
                           remove-image-url="/api/v3/workout_image/remove/"
                           file="add_workout.selectedWorkout.image" 
                           upload-image-dropzone >
                            <div class="dz-default dz-message"><span>Drop Image here to upload</span></div>                     
                    </div>
                </div>
            </div>
    </form>
</div>