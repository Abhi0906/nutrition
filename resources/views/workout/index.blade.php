@extends('layout.app')

@section('content')

<div ng-controller="WorkoutController as vm">   
    <!-- <ul class="breadcrumb breadcrumb-top">
        <li>Pages</li>
        <li><a href="javascript:void(0);">Workouts</a></li>
    </ul> -->
     <div ui-view></div>

     <div id="workout_exercise_image"
      class="modal fade"
       tabindex="-1"
        role="dialog" 
          aria-hidden="true" 
           data-backdrop="static"
            data-keyboard="false">
       @include('workout.workout_exercise_details')
      </div>
</div>

@endsection

@section('page_scripts')
{!! HTML::script("js/controllers/WorkoutController.js") !!}
{!! HTML::script("js/controllers/WorkoutListController.js") !!}
{!! HTML::script("js/controllers/WorkoutEditController.js") !!}
{!! HTML::script("js/controllers/WorkoutAddController.js") !!}
{!! HTML::script("js/directives/singleVideoImage.js") !!}
{!! HTML::script("js/directives/chosen.js") !!}
{!! HTML::script("js/directives/uploadImageDropZone.js") !!}

@append

@section('jquery_ready')

@append