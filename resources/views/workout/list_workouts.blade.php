<div class="row block-title patient_title">
    <div class="col-sm-2">
        <h3><strong>Workouts</strong></h3>
    </div>  
    <div class="col-sm-10 text-alignRight">
        <h3>
          <form class="form-inline">
                <div class="form-group">
                   <!-- <a class="btn btn-default" ata-original-title="New Workout Routines" ng-click="list_workout.showAddWorkout = !list_workout.showAddWorkout;list_workout.addWorkout()"><span><i class="fa fa-plus"></i></span>&nbsp;New Workout Routine</a> -->

                  <a ui-sref="add-workout" class="btn btn-default" ata-original-title="New Workout Routine"><span><i class="fa fa-plus"></i></span>&nbsp;New Workout</a>

                </div>
          </form>
        </h3>
    </div>
</div>
<div class="row">
     <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3">
           @include('workout.search_workout')
     </div>
      <!-- workout details List -->
     <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
        <simple-block  right_title block-title="Workouts (@{{list_workout.workouts.length}})" >
          <div ng-if="list_workout.isloading">
            @include('layout.loading')
          </div>
          <div
          ng-repeat="workout in list_workout.workouts">
          <!-- <div ng-click="list_workout.editWorkout(workout)"
          ng-repeat="workout in list_workout.workouts" ng-class="{'space-top':true,'animation-fadeInQuick': true,'selected-border':workout==list_workout.selectedWorkout}" > -->

          <interactive-block 
           title-click="list_workout.editWorkout(workout, 'edit')"
           block-title="vm.getWorkoutTitle(workout)"
           on-edit="list_workout.editWorkout(workout)" 
           on-remove="list_workout.deleteWorkout(workout)"
           closed
           >
              <block-content> 
                  <div class="gallery">
                      <div ng-if="$index%3==0" ng-repeat="xyz in workout.exercise_list" >                     
                          <div  class="row row-eq-height" >
                              <div ng-if="($index>=$parent.$index) && ($index<=($parent.$index+2))" 
                                  ng-repeat="exercise in workout.exercise_list" 
                                  class="col-sm-4">
                                   <single-video-image  not-editable
                                         remove ="list_workout.deleteExercise(workout,exercise, workout.id)"
                                         view = "vm.viewExercise(exercise)"
                                         videoimage="exercise"
                                         width="250" height="150">                            
                                      </single-video-image>
                                   <br>                              
                             </div>
                              <br>
                          </div>                            
                      </div>
                  </div> 
              </block-content>             
          </interactive-block>
          </div>
          <div class="alert alert-danger animation-fadeInQuick" ng-cloak ng-show="list_workout.workouts.length <= 0">
            <button data-dismiss="alert" class="close" type="button">
              <i class="ace-icon fa fa-times"></i>
            </button>

            <strong>
              No Workouts available!
            </strong>
          </div>
        </simple-block>
     </div>  
      <!-- workout details List -->
</div>