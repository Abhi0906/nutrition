<html>
<head>
    <title></title>
</head>
<body style="margin:0 auto;">
<table style="width:900px; border:1px solid #ccc; margin:0 auto;border-collapse: collapse;padding: 0; box-sizing: border-box;">
    <tbody>
        <tr style="background: url('https://genemedicshealth.com/images/new_backheader.jpg');
        background-repeat: repeat-x;">
            <td colspan="2" style="padding:10px;"><img src="https://genemedicshealth.com/images/new_logo-genemedics.png" width="200" /></td>
        </tr>
        <tr style="background: url('https://genemedicshealth.com/images/new_about-bg.png'); background-repeat:no-repeat; height:250px; background-size:cover;">                    
            <td style="vertical-align:top; color:#0069C5;">
            <p style="position:relative; right:10px; margin-bottom:-15px; text-align:right; font-weight:700; font-size:25px;">
                Thank You for contacting
            </p>
            <p style="position:relative; right:10px; text-align:right; font-size: 32px; font-weight:bold;">Genemedics Health Institute!</p>
            
            </td>
        
            </tr>
            <tr>
                <td colspan="2">
                     <p>To reset your password,&nbsp;

                        <a href="{{ $actionUrl }}">click this link </a> or copy the URL below and paste it into your web browser's navigation bar.<br/> 
                      {{ $actionUrl }}
                    </p>
                    <br> 
                </td>
            </tr>
            <tr>
            <td colspan="2">
            <p>You will need to enter your new password both at that URL and again to log in to your account. Keep this in mind, and be sure you enter your password correctly both times.
            <br/>
            <br/>
            If you initiated password reset from your mobile device, this URL will take you to our mobile web site. Once completed, you may return to the Genemedics app and enter your new password to log in.           <br />
            <br />
            You may wish to learn how to reset a forgotten password.    <br/>
            <br/>
            Thank you,<br/>
            the Genemedicsheath team
            <br />
            &nbsp;</p>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            <p style="font-size: 12px; text-align:center; margin: 0 auto; height: 25px; background: #fff; padding-top: 10px; color: #000; font-weight: bold; text-transform: uppercase;">Visit us Online at <a href="http://genemedics.com" style="color: #3696c2; font-size: 12px;">www.genemedicshealth.com</a></p>
            </td>
        </tr>
        <tr style="background: #1290e3;">
            <td colspan="2">
            <table style="width:90%;margin: 0 auto;">
                <tbody>
                    <tr>
                        <td width="77%">
                        <p style="padding-top:15px;padding-bottom:15px; font-weight: normal; text-rendering: optimizeLegibility !important; -webkit-font-smoothing: antialiased !important; -moz-osx-font-smoothing: grayscale; text-transform: uppercase; color: #3b3b3b;">If you have any questions, please feel free to contact us at<br />
                        <a href="mailto:info@genemedicsnutrition.com" style="color: white;">md@genemedics.com</a> or by phone at <span style="color: white; text-transformation: underline;">(800) 277-4041</span></p>
                        </td>
                        <td width="23%"> 
                            <a href="#"><img src="https://genemedicshealth.com/images/new_footer_fafacebooksquare.png" width="30" style="border-radius:0px;" /></a>
                            <a href="#"><img src="https://genemedicshealth.com/images/new_footer_fawittersquare.png" width="30" style="border-radius:0px;" /></a>
                            <a href="#"><img src="https://genemedicshealth.com/images/new_footer_Plus.png" width="30" style="border-radius:0px;" /></a>
                            <a href="#"><img src="https://genemedicshealth.com/images/new_fagoogleplussquare.png" width="30" style="border-radius:0;" /></a>
                        </td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <tr style="background: #1d88d1;">
            <td colspan="2">
            <p style="font-size: 12px; font-weight: bold; color: #fff; text-rendering: optimizeLegibility !important; -webkit-font-smoothing: antialiased !important; -moz-osx-font-smoothing: grayscale; text-transform: uppercase;text-align:center;padding:10px;">Genemedicshealth.com, LLC. All rights reserved. Genemedicshealth.com</p>
            </td>
        </tr>
    </tbody>
</table>
</body>
</html>