@extends('layout.app')

@section('content')

<div>   
    <div ui-view></div>     
</div>


@endsection

@section('page_scripts')

{!! HTML::script("js/controllers/MdMealsController.js") !!}
{!! HTML::script("js/controllers/RecommendedMealItemController.js") !!}
{!! HTML::script("js/controllers/RecommendedNewMealItemController.js") !!}
{!! HTML::script("js/controllers/EditRecommendedFoodController.js") !!}

{!! HTML::script("js/controllers/AddRecomMealFoodController.js") !!}
{!! HTML::script("js/controllers/MealTemplateNameController.js") !!}
{!! HTML::script("js/directives/foodFor.js") !!}
{!! HTML::script("js/directives/viewFoodDetail.js") !!}
{!! HTML::script("js/filters/stringLimitFilter.js") !!}
@append

@section('jquery_ready')

@append