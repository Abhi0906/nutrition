
 <div class="row block-title patient_title" ng-init="meal.getMdMeals();">
      <div class="col-sm-6">
        <h3><strong>Meal Plan Template</strong></h3>
      </div>  
      <div class="col-sm-6 text-alignRight">
       <!--  
        <a ui-sref="new-meal-items({new_meal_id: meal.new_meal_id, meal_template_id: 1})" 
            class="btn btn-default" data-original-title="New Meal"><span><i class="fa fa-plus"></i></span>&nbsp;New Meal Plan</a> -->


        <a ng-click="meal.addMealPlanGenderModal()"
            class="btn btn-default" data-original-title="New Meal"><span><i class="fa fa-plus"></i></span>&nbsp;New Meal Plan</a>
      </div>
  </div>

<div class="block md_recommended_proui" >
    <div class="md_rec_food_class">
      <table class="table recommeded_table">
        <thead>
          <tr class="">
            <th class="text-center bg-primary width_60">MALE MEAL NAME</th>
            <th class="text-center bg-primary width_15">MAX CALORIE</th>
            <th class="text-center bg-primary width_10">ACTION</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="recommended_meal in meal.recommended_meals | filter : maleTemplateFilter">
             <td data-title="MEAL NAME" class="text-left">
                <!-- <a href="javascript:void(0)" ui-sref="meal-items({recommended_meal_id: recommended_meal.id, recommended_meal_name: recommended_meal.meal_name})">@{{recommended_meal.meal_name}}</a> -->
                <small class="meal_food_name" ng-if="recommended_meal.meal_foods_names.length!=0">
                  &nbsp;
                  [<span ng-repeat="food_name in recommended_meal.meal_foods_names track by $index">
                    <a class="meal_food_name" href="javascript:void(0)" 
                      ui-sref="meal-items({recommended_meal_id: recommended_meal.id, recommended_meal_name: recommended_meal.meal_name})"
                    title="@{{ food_name}}">@{{ food_name }}</a>
                    <span ng-if="recommended_meal.meal_foods_names.length-1!=$index">, </span> 
                  </span>]
                </small>
             </td>
             
             <td data-title="MAX CALORIE" class="text-center">@{{recommended_meal.max_calorie}}</td>
              
              <td data-title="ACTION" class="text-center">
                <div class="btn-group btn-group-xs">
                  <a class="btn btn-default" href="javascript:void(0)" ui-sref="meal-items({recommended_meal_id: recommended_meal.id, recommended_meal_name: recommended_meal.meal_name })" data-original-title="Edit">
                    <i class="fa fa-pencil-square-o"></i>
                  </a>
                  <a class="btn btn-danger" title="" ng-click="meal.deleteRecommendedMeal(recommended_meal)" data-toggle="tooltip" href="javascript:void(0)" data-original-title="Delete">
                    <i class="fa fa-times"></i>
                  </a>
                </div>
              </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="md_rec_food_class">
      <table class="table recommeded_table">
        <thead>
          <tr class="">
            <th class="text-center bg-primary width_60">FEMALE MEAL NAME</th>
            <th class="text-center bg-primary width_15">MAX CALORIE</th>
            <th class="text-center bg-primary width_10">ACTION</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="recommended_meal in meal.recommended_meals | filter : femaleTemplateFilter">
             <td data-title="MEAL NAME" class="text-left">
                <!-- <a href="javascript:void(0)" ui-sref="meal-items({recommended_meal_id: recommended_meal.id, recommended_meal_name: recommended_meal.meal_name})">@{{recommended_meal.meal_name}}</a> -->
                <small class="meal_food_name" ng-if="recommended_meal.meal_foods_names.length!=0">
                  &nbsp;
                  [<span ng-repeat="food_name in recommended_meal.meal_foods_names track by $index">
                    <a class="meal_food_name" href="javascript:void(0)" 
                      ui-sref="meal-items({recommended_meal_id: recommended_meal.id, recommended_meal_name: recommended_meal.meal_name})"
                    title="@{{ food_name}}">@{{ food_name }}</a>
                    <span ng-if="recommended_meal.meal_foods_names.length-1!=$index">, </span> 
                  </span>]
                </small>
             </td>
             
             <td data-title="MAX CALORIE" class="text-center">@{{recommended_meal.max_calorie}}</td>
              
              <td data-title="ACTION" class="text-center">
                <div class="btn-group btn-group-xs">
                  <a class="btn btn-default" href="javascript:void(0)" ui-sref="meal-items({recommended_meal_id: recommended_meal.id, recommended_meal_name: recommended_meal.meal_name })" data-original-title="Edit">
                    <i class="fa fa-pencil-square-o"></i>
                  </a>
                  <a class="btn btn-danger" title="" ng-click="meal.deleteRecommendedMeal(recommended_meal)" data-toggle="tooltip" href="javascript:void(0)" data-original-title="Delete">
                    <i class="fa fa-times"></i>
                  </a>
                </div>
              </td>
          </tr>
        </tbody>
      </table>
    </div>
</div>
<!-- END of MD Recommended Food Block-->

<!-- Add Meal Plan Modal -->
    <div id="addMealGenderModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-primary">
            <button class="close white_close" type="button" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">@{{meal.action}} Meal Plan</h4>
          </div>
          <div class="modal-body">
            <form name="createMealGenderForm" class="form-horizontal" novalidate="novalidate">
                <div class="form-group">
                  <label class="col-sm-4 control-label title_blue" for="add_meal">Gender</label>
                  <div class="col-sm-6">                      
                      <select class="form-control" name="gender"
                                   ng-model="meal.gender"
                                   ng-init="meal.gender='Male'">
                      <option value="Male" >Male</option>
                      <option value="Female">Female</option>
                      </select>
                  </div>
                </div>        
                <div class="form-group">
                  <div class="col-sm-4"></div>
                  <div class="col-sm-8">
                    <a data-dismiss="modal" ui-sref="new-meal-items({new_meal_id: meal.new_meal_id, meal_template_id: 1, meal_gender: meal.gender})" ng-disabled="addMealGenderModal.$invalid"
                       class="btn btn-success" data-original-title="New Meal">@{{meal.action}}</a>         
                  </div>
                </div>
            </form>
          </div>
          <div class="saperator_5"></div>
        </div>
      </div>
    </div>
<!-- End of Add Meal Plan Modal --> 