<div ng-init="add_meal_food.getMDFoodTypes() ">

    <div class="row block-title patient_title">
        <div class="col-sm-6">
            <h3><strong>MD Recommended @{{add_meal_food.item_name}} Foods</strong></h3>
        </div>
        <div class="col-sm-6 text-alignRight">

            <a ui-sref="meal-items({recommended_meal_id: add_meal_food.recommended_meal_id, recommended_meal_name: add_meal_food.meal_name})"
               class="btn btn-default"> <i class="gi gi-remove_2"></i>
                &nbsp;Cancel</a>

            <button  class="btn btn-default"  type="button"
                    ng-click="add_meal_food.addFoodToItem();">
                 Save
            </button>

        </div>
    </div>

    <div class="block">
        <div class="block-title">
            <div class="row padding_top_bottom">
                <div class="col-sm-6">
                      <h2 class="uppercase"><strong>@{{add_meal_food.item_name}}</strong></h2>
                </div>

            </div>



        </div>


        <div class="md_rec_food_class" id="mdrecommeded_food_table">

            <table class="table recommeded_table">
                <thead>
                <tr>

                                <th class="width_30 bg-primary">

                                    <input id="checkbox-all"
                                           class="checkbox-custom"
                                           name="checkbox-all"
                                           type="checkbox"
                                           ng-model="add_meal_food.chk_all_type"
                                           ng-click="add_meal_food.checkAllFood();"

                                    >
                                    <label for="checkbox-all" class="checkbox-custom-label"> Food Name</label>
                                </th>
                                <th class="text-center bg-primary width_10">Food For</th>
                                <th class="text-center bg-primary width_10">Serving Size</th>
                                <th class="text-center bg-primary width_10">Serving Range</th>
                                <th class="text-center bg-primary width_10">Serving Default</th>
                                <th class="text-center bg-primary width_10">CALORIES</th>
                                <th class="text-center bg-primary width_10">FAT</th>
                                <th class="text-center bg-primary width_10">FIBER</th>
                                <th class="text-center bg-primary width_10">CARBS</th>
                                <th class="text-center bg-primary width_10">SODIUM</th>
                                <th class="text-center bg-primary width_10">PROTEIN</th>


                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="food in add_meal_food.md_food_type">
                    <td class="width_30" data-title="NAME">

                        <input id="checkbox-@{{food.nutritions.id}}"
                               class="checkbox-custom"
                               name="checkbox-@{{food.nutritions.id}}"
                               type="checkbox"
                               checklist-model="add_meal_food.selectedFoods"
                               checklist-value="food"
                        >
                        <label for="checkbox-@{{food.nutritions.id}}" class="checkbox-custom-label"> @{{food.nutritions.name}}</label>
                    </td>
                    <td data-title="Food For" class="text-center width_10">@{{food.food_for}}</td>
                    <td data-title="Serving Size" class="text-center width_10">@{{food.serving_string}}</td>
                    <td data-title="Serving Range" class="text-center width_10">@{{food.serving_range}}</td>
                    <td data-title="Serving Default" class="text-center width_10">@{{food.no_of_servings}}</td>
                    <td data-title="CALORIES" class="text-center width_10">@{{food.calories}}</td>
                    <td data-title="FAT" class="text-center width_10">@{{food.nutritions.nutrition_data.total_fat}}</td>
                    <td data-title="FIBER" class="text-center width_10">@{{food.nutritions.nutrition_data.dietary_fiber}}</td>
                    <td data-title="CARBS" class="text-center width_10">@{{food.nutritions.nutrition_data.total_carb}}</td>
                    <td data-title="SODIUM" class="text-center width_10">@{{food.nutritions.nutrition_data.sodium}}</td>
                    <td data-title="PROTEIN" class="text-center width_10">@{{food.nutritions.nutrition_data.protein}}</td>


                </tr>
                </tbody>
            </table>
        </div>
        <!-- END Block Content -->
    </div>
    <!-- END of MD Recommended Food Block -->
</div>