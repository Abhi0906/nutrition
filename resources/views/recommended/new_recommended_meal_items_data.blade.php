 <div class="row block-title patient_title"
         ng-init="new_meal_item.createNewMeal();">
      <div class="col-sm-6">
      
      </div>  
      <div class="col-sm-6 text-alignRight">
          <a ui-sref="meals({ id: new_meal_item.meal_template_id})" class="btn btn-default" title="Cancel"> 
                    <i class="gi gi-remove_2"></i>&nbsp;Cancel</a>
          <a ui-sref="meals({ id: new_meal_item.meal_template_id})" class="btn btn-default" 
                               data-original-title="Back"> Back  </a>

      </div>
  </div>

<div>
   <!--  <ul class="breadcrumb breadcrumb-top">
        <li>Pages</li>
        <li><a href="javascript:void(0);">MD Recommended Meals</a></li>
    </ul>
 -->    
    

    <!--MD Recommended Food Block -->
    <div class="block">
        <div class="block-title">
            
            <!-- <div class="block-options pull-right">
                    <a class="btn btn-alt btn-sm btn-default " 
                     title="Back" 
                     ui-sref="meals">Back</a>
            </div>  
            <div class="block-options pull-right">
                    <a class="btn btn-alt btn-sm btn-default " 
                     href="#" class="btn btn-default" 
                     data-toggle="modal"  
                     ng-click="meal_item.getdata_RecommendedMeal(meal_item.recommended_meal_id)" 
                     data-original-title="Edit">Edit</a>
            </div>  -->
            <!-- <h2><strong>@{{meal_item.meal_name}}&nbsp;</strong></h2>   -->   
        </div>

        <div class="md_rec_food_class" id="mdrecommeded_meal_table">
          <table class="table recommeded_table">
            <thead>   
              <tr>
                <td class="no_padding">
                  <table class="table no_border margin_bottm_0">
                    <tr>
                      <th class="width_34">&nbsp;</th>
                      <th class="text-center bg-primary width_11">CALORIES</th>
                      <th class="text-center bg-primary width_11">SERVING SIZE</th>
                      <th class="text-center bg-primary width_11">NO. Of SERVING</th>
                      <th class="text-center bg-primary width_11">FAT</th>
                      <th class="text-center bg-primary width_11">FIBER</th>
                      <th class="text-center bg-primary width_11">CARBS</th>
                      <th class="text-center bg-primary width_11">SODIUM</th>
                      <th class="text-center bg-primary width_11">PROTEIN</th>
                      
                     
                    </tr>
                  </table>
                </td>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="mealItem in new_meal_item.meal_item_data" class="tr_no_border">
                <td class="no_padding">
                  <table class="table no_border margin_bottm_0">
                    <tr class="subhead-mdrec">
                      <td data-title=" " colspan="9">
                        <h4><span class="uppercase">@{{mealItem.item_name}}</span></h4>
                        <ul class="list-inline">
                          <li>
                            <i class="fa fa-plus-circle facolor"></i> 

                            <a href="javascript:void(0);" ng-if="mealItem.sub_type =='supplement'" ui-sref="add-meal-food({recommended_meal_id: new_meal_item.recommended_meal_id, recommended_meal_name: new_meal_item.meal_name, item_id: mealItem.item_id, item_name: mealItem.item_name })">
                              Add Supplement
                            </a>
                            <a href="javascript:void(0);" ng-if="mealItem.sub_type =='food'" ui-sref="add-meal-food({recommended_meal_id: new_meal_item.recommended_meal_id, recommended_meal_name: new_meal_item.meal_name, item_id: mealItem.item_id, item_name: mealItem.item_name })">
                              Add Food
                            </a> 

                            
                          </li>
                        </ul>
                      </td>
                    </tr>
                    <tr ng-repeat="food in mealItem.meal_item_foods">
                      <td class="width_34" data-title="NAME">
                        <a href="javascript:void(0);" ng-click="new_meal_item.deleteRecommendedMealFood(mealItem, food);">
                          <i class="fa fa-times-circle facolor"></i>
                        </a>
                        <a ui-sref="edit-food({ edit_from : 'md_meal',
                                                recommended_type_id : mealItem.item_id, 
                                                food_data: food, 
                                                food_for_names : new_meal_item.food_for_names,
                                                recommended_types: new_meal_item.recommended_types,
                                                recommended_meal_id: new_meal_item.recommended_meal_id, 
                                                recommended_meal_name: new_meal_item.meal_name,
                                              })">
                          <!-- <i class="fa fa-pencil-square fa-gray"></i> -->
                          @{{food.food_name}}
                        </a>                         
                      </td>
                      <td data-title="CALORIES" class="text-center width_11">@{{food.calories}}</td>
                       <td data-title="SERVING SIZE" class="text-center width_11">@{{food.serving_quantity}}</td> 
                       <td data-title="NO. Of SERVING" class="text-center width_11">@{{food.no_of_servings}}</td> 
                       <td data-title="FAT" class="text-center width_11">@{{food.total_fat}}</td>
                       <td data-title="FIBER" class="text-center width_11">@{{food.dietary_fiber}}</td>
                       <td data-title="CARBS" class="text-center width_11">@{{food.total_carb}}</td>
                       <td data-title="SODIUM" class="text-center width_11">@{{food.sodium}}</td>
                       <td data-title="PROTEIN" class="text-center width_11">@{{food.protein}}</td> 
                       
                      
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td class="no_padding">
                  <table class="table no_border margin_bottm_0">
                    <tr>
                      <td class="width_34" data-title="NAME">
                        <a href="javascript:void(0);">
                          <strong>Total</strong>
                        </a>                         
                      </td>
                      <td data-title="CALORIES" class="text-center width_11">@{{new_meal_item.totalMealItemData.total_calories}}</td>
                      <td data-title="SERVING SIZE" class="text-center width_11">-</td> 
                      <td data-title="SERVING SIZE" class="text-center width_11">-</td> 
                       <td data-title="SERVING SIZE" class="text-center width_11">@{{new_meal_item.totalMealItemData.total_fat}}</td> 
                       <td data-title="NO. Of SERVING" class="text-center width_11">@{{new_meal_item.totalMealItemData.total_fiber}}</td> 
                       <td data-title="FAT" class="text-center width_11">@{{new_meal_item.totalMealItemData.total_carb}}</td>
                       <td data-title="FIBER" class="text-center width_11">@{{new_meal_item.totalMealItemData.total_sodium}}</td>
                       <td data-title="CARBS" class="text-center width_11">@{{new_meal_item.totalMealItemData.total_protein}}</td>
                    </tr>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- END Block Content -->
    </div>
    <!-- END of MD Recommended Food Block -->     
</div>