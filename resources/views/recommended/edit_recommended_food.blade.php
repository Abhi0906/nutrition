<div ng-init="edit_food.getFoodRecommendedTypes(); edit_food.getRecommendedFood();"> 
   
    <div class="row block-title patient_title">
      <div class="col-sm-6">
        <h3><strong>Edit @{{edit_food.nutrition_food.name | limitTo: 50 }} </strong></h3>
      </div>  
      <div class="col-sm-6 text-alignRight">
                  <!--    <a class="btn btn-alt btn-sm btn-default " 
                     title="Back" ng-click="edit_food.backToItemPage();">Back</a> -->

                     <a ng-click="edit_food.backToItemPage();" class="btn btn-default"> <i class="gi gi-remove_2"></i>
                     &nbsp;Cancel</a>
                     
                     <button ng-show="edit_food.del_btn_display" class="btn btn-default"  type="button" 
                        ng-click="edit_food.deleteRecommendedFood();">
                        <i class="fa gi gi-bin"></i> Delete
                     </button>

      </div>
    </div>
    <!--MD Recommended Food Block -->
    <div class="block">

        <div class="block">
          <div class="block-title">
            
            <div class="block-options pull-right">
                   <!--  <a class="btn btn-alt btn-sm btn-default " 
                     title="Back" ng-click="edit_food.backToItemPage();">Back</a> -->

            </div>  
           <!--  <h2><strong>Edit Food&nbsp;</strong></h2> -->
          </div>

          <div id="addrecommeded_food_table">
            <table class="table table-bordered recommeded_table add_food_tbl">
              <thead class="bg-primary">
                <tr>
                  <th class="text-center width_30">FOOD NAME</th>
                  <th class="text-center">CALORIES</th>
                  <th class="text-center">Serving Size</th>
                  <th class="text-center">Serving Range</th>
                  <th class="text-center">Serving Default</th>
                  <th class="text-center">Food Type</th>
                  <th ng-if="edit_food.edit_from == 'md_food'" class="text-center">Food For</th>
                  <th class="text-center">Update FOOD</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-if="edit_food.nutrition_food.length!=0">
                  <td class="text-left" data-title="FOOD NAME">
                    <p class="theme_text_p_lg_md">@{{edit_food.nutrition_food.name}}
                      <span><a href="javascript:void(0);"></a></span>
                    </p>
                    <p class="tag_text_normal">@{{edit_food.nutrition_food.brand_name}}, 
                      @{{edit_food.nutrition_food.nutrition_data.serving_quantity}} @{{edit_food.nutrition_food.nutrition_data.serving_size_unit}}
                    </p>
                  </td>
                  <td data-title="CALORIES" class="text-center">
                    <p class="food_log_normalbold"
                     ng-bind="edit_food.recommended_food.calories" 
                     ng-init="edit_food.recommended_food.calories = edit_food.food_data.calories">
                    </p>
                  </td>

                  <td data-title="SERVING SIZE">
                    <select class="form-control"
                             ng-init="edit_food.recommended_food.serving_string = edit_food.food_data.serving_string" 
                             ng-model="edit_food.recommended_food.serving_string" 
                             ng-options="serving.label as serving.label for serving in edit_food.nutrition_food.serving_data"
                             ng-change="edit_food.calculateServingSize(edit_food.nutrition_food);">
                      </select>
                  </td>
                    <td data-title="SERVING RANGE">

                        <select class="form-control"
                                ng-init="edit_food.recommended_food.serving_range = edit_food.food_data.serving_range"
                                ng-model="edit_food.recommended_food.serving_range"
                                ng-options="range for range in edit_food.serving_ranges"
                                ng-change="edit_food.updateCaloriesRange(edit_food.nutrition_food);">
                        </select>
                    </td>

                  <td data-title="SERVING DEFAULT">
                   <input type="text"
                       ng-init="edit_food.recommended_food.no_of_servings = edit_food.food_data.no_of_servings"
                        size="6"
                         ng-model="edit_food.recommended_food.no_of_servings"
                         ng-change="edit_food.calculateServingSize(edit_food.nutrition_food);"
                         class="form-control"
                        >                     
                  </td>
                  <!-- Food Type -->
                  <td ng-if="edit_food.edit_from == 'md_food'"  data-title="FOOD TYPE">                    

                      <select class="form-control"
                             ng-init="edit_food.recommended_food.food_recommendation_type_id = edit_food.recommended_type_id" 
                             ng-model="edit_food.recommended_food.food_recommendation_type_id" 
                             ng-options="recommended_type.id as recommended_type.name for recommended_type in edit_food.recommended_types"
                             >
                     </select>
                  </td>
                  <!-- OR -->
                  <td ng-if="edit_food.edit_from == 'md_meal'" data-title="FOOD TYPE">                    

                      <select class="form-control"
                             ng-init="edit_food.recommended_food.food_recommendation_type_id = edit_food.recommended_type_id" 
                             ng-model="edit_food.recommended_food.food_recommendation_type_id" 
                             ng-options="recommended_type.id as recommended_type.item_name for recommended_type in edit_food.recommended_types"
                             >
                     </select>
                  </td>
                  <!-- End Food Type -->
                  <td ng-if="edit_food.edit_from == 'md_food'" data-title="FOOD FOR">

                    <food-for class="form-control" ng-model="edit_food.recommended_food.food_for" 
                          ng-init="edit_food.recommended_food.food_for = edit_food.food_data.food_for"
                          ng-change="edit_food.updateDefaultServing(edit_food.nutrition_food);"
                    ></food-for>
                  </td>

                  <td data-title="LOG FOOD" class="text-center">
                    <p><button class="btn btn-primary" ng-click="edit_food.updateMdRecommendedFood(edit_food.nutrition_food)">Update Food</button></p>
                    <p><a href="javascript:void(0)" ng-click="edit_food.viewFoodDetail()" class="link_clor">View Details</a></p>
                  </td>
                </tr>
              </tbody>
            </table>
            <!-- <div ng-if="!edit_food.foodSearchdata.length || edit_food.foodSearchdata.length==0" class="text-center">Search food to add in recommendation</div>    -->
            <div ng-show="edit_food.edit_food_loader"  class="search_food_loader"></div>
            <br><br><br><br>
          </div>
        </div>
        <!-- END Block Content -->
    </div>
    <!-- END of MD Recommended Food Block -->
     
</div>
<!-- view models -->
<div id="view_food_detail_tmplt" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
  <view-food-detail details="edit_food.food_detail"></view-food-detail>
</div>