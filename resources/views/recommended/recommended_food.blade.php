@extends('layout.app')

@section('content')

<div ng-controller="RecommendationController as vm">  

    <div ui-view></div>

</div>

@endsection

@section('page_scripts')

{!! HTML::script("js/controllers/RecommendationController.js") !!}
{!! HTML::script("js/controllers/EditRecommendedFoodController.js") !!}
{!! HTML::script("js/directives/foodFor.js") !!}
{!! HTML::script("js/directives/viewFoodDetail.js") !!}
@append

@section('jquery_ready')

@append