<div ng-init="add_meal_food.getRecommendedMealItems()">   
      <div class="block-title">
        
        <div class="pull-right">
            <a ng-click="add_meal_food.backToItemPage();" class="btn btn-default"> <i class="gi gi-remove_2"></i>
                     &nbsp;Cancel</a>
         </div>  
        <h2><strong>@{{add_meal_food.item_name}}&nbsp;</strong></h2>
      </div>
    <div class="block">
      <div>
        <form class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label title_blue" for="add_meal">Enter Food</label>
            <div class="col-sm-8 space_col_right_8">
                <div class="input-group">     
                  <input type="text" ng-model="add_meal_food.searchKeyFood" class="form-control">
                  <span class="input-group-addon no_padding">
                    <button type="submit" class="btn btn-info text-white" ng-click="add_meal_food.searchFood()">Search</button>
                  </span>
                </div>
            </div>
            <div class="col-sm-2 no_col_padding">
              <button class="btn btn-warning margin_top_10" ng-click="add_meal_food.clear_logFood_searchResult()">Clear</button>          
            </div>
          </div>
        </form>
      </div>
      <hr/>
      <div id="addrecommeded_food_table">
        <table class="table table-bordered recommeded_table">
        
          <tbody>
            <tr  class="bg-primary">
              <th class="text-center width_35">FOOD NAME</th>
              <th class="text-center">CALORIES</th>
              <th class="text-center">SERVING SIZE</th>
              <th class="text-center">NO. OF SERVING</th>
              <th class="text-center">Item Type</th>
              <th class="text-center">ADD FOOD</th>
            </tr>
          
            <tr ng-repeat="fooddata in add_meal_food.foodSearchdata">
              <td class="text-left" data-title="FOOD NAME">
                <p class="theme_text_p_lg_md">@{{fooddata.name}}
                  <span><a href="javascript:void(0);"></a></span>
                </p>
                <p class="tag_text_normal">
                  @{{fooddata.brand_name}}, 
                  @{{fooddata.nutrition_data.serving_quantity}} @{{fooddata.nutrition_data.serving_size_unit}}
                </p>
              </td>
              <td data-title="CALORIES" class="text-center">
                <p class="food_log_normalbold"
                 ng-bind="add_meal_food.recommended_food[$index].calories" 
                 ng-init="add_meal_food.recommended_food[$index].calories = fooddata.nutrition_data.calories">
                </p>
              </td>


              <td data-title="SERVING SIZE">
                <select class="form-control"
                         ng-init="add_meal_food.recommended_food[$index].serving_string = fooddata.serving_data[0]" 
                         ng-model="add_meal_food.recommended_food[$index].serving_string" 
                         ng-options="serving.label for serving in fooddata.serving_data"
                         ng-change="add_meal_food.calculateServingSize($index, fooddata);">
                  </select>
              </td>
              <td data-title="NO. OF SERVINGS">
               <input type="text"
                   ng-init="add_meal_food.recommended_food[$index].no_of_servings =1"
                    size="6"
                     ng-model="add_meal_food.recommended_food[$index].no_of_servings"
                     ng-change="add_meal_food.calculateServingSize($index, fooddata);"
                     class="form-control"
                    >                     
              </td>
              
              <td data-title="FOOD TYPE">
                 

                  <select class="form-control"
                                     ng-init="add_meal_food.recommended_meal_item[$index].recommended_meal_item_id = add_meal_food.item_id" 
                                     ng-model="add_meal_food.recommended_meal_item[$index].recommended_meal_item_id" 
                                     ng-options="recommended_meal_item.id as recommended_meal_item.item_name for recommended_meal_item in add_meal_food.recommended_meal_items"
                                     >
                 </select>
              </td>
              <td data-title="LOG FOOD" class="text-center">
                <p><button class="btn btn-primary" ng-click="add_meal_food.createRecommendedMeal($index, fooddata)">Add Food</button></p>
                <p><a href="javascript:void(0)" ng-click="add_meal_food.viewFoodDetail(fooddata)" class="link_clor">View Details</a></p>
              </td>
            </tr>
          </tbody>
        </table>
        <!-- <div ng-if="!add_meal_food.foodSearchdata.length || add_meal_food.foodSearchdata.length==0" class="text-center">Search food to add in recommendation</div>    -->
        <div ng-show="add_meal_food.search_loader"  class="search_food_loader"></div>
        <br><br><br><br>
      </div>
    </div>
   
    <!-- END of MD Recommended Food Block -->
     
</div>
<!-- view models -->
<div id="view_food_detail_tmplt" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
  <view-food-detail details="add_meal_food.food_detail"></view-food-detail>
</div>