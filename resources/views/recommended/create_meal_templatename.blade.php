<div class="row block-title patient_title">
  <div class="col-sm-6">
    <h3><strong>
      <span ng-if="!create_templatename.template_id">Create </span>
      <span ng-if="create_templatename.template_id">Edit </span>
      Template Name&nbsp;</strong></h3>
  </div>  
  <div class="col-sm-6 text-alignRight">
  
                 <button class="btn btn-default" 
                                      ng-if="!create_templatename.template_id"
                                      ng-disabled="createTemplateNameForm.$invalid"
                                      ng-click="create_templatename.saveTemplateName()">
                                      <i class="fa fa-angle-right"></i> Create</button>
                  <button class="btn btn-default"
                                      ng-if="create_templatename.template_id"
                                      ng-disabled="createTemplateNameForm.$invalid"
                                      ng-click="create_templatename.updateTemplate()">
                                      <i class="fa fa-angle-right"></i> Update</button>
                 <a ui-sref="template-names" class="btn btn-default"><span>&nbsp;</span>Back</a>
  </div>
</div>

<div class="row">
  <form name="createTemplateNameForm" enctype="multipart/form-data" class="form-horizontal form-bordered">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <!-- Basic Form Elements Block -->
        <div class="block">            

                <div class="form-group">
                    <label class="col-md-3 control-label" for="example-text-input">Template Name<span class="text-danger">&nbsp;*</span></label>
                    <div class="col-md-8">
                        <input type="text" id="example-text-input" 
                                          name="template_name" 
                                          class="form-control" 
                                          placeholder="Text" 
                                          ng-model="create_templatename.template_data.template_name"
                                          required>
         
                    </div>
                    <div class="col-md-offset-3 col-md-9">
                      <span class="text-danger" ng-show="createTemplateNameForm.template_name.$invalid && createTemplateNameForm.template_name.$touched">Please enter title</span>
                    </div>
                </div>
                
                <div class="form-group" ng-if="!create_templatename.template_id">
                    <label class="col-md-3 control-label" for="example-textarea-input">Template Gender</label>
                    <div class="col-md-8">
                        

                      <select class="form-control" name="template_gender"
                      						 ng-model="create_templatename.template_data.template_gender"
                                   ng-init="create_templatename.template_data.template_gender='Male'">
	                    <option value="Male" >Male</option>
	                    <option value="Female">Female</option>
                      </select>
                    </div>
                </div>

            <!-- END Basic Form Elements Content -->
        </div>
        <!-- END Basic Form Elements Block -->
    </div>
  </form>
</div>
