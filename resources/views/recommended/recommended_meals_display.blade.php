<!-- <ul class="breadcrumb breadcrumb-top">
    <li>Pages</li>
    <li><a href="javascript:void(0);">MD Recommended Meals</a></li>
</ul> -->

<!--MD Recommended Food Block -->
 <div class="row block-title patient_title" ng-init="meal.getTemplateNames(); meal.getRecommendedMeals();">
      <div class="col-sm-6">
        <h3><strong>Meals of <span class="text-primary">@{{meal.meal_template.template_name}}</span></strong></h3>
      </div>  
      <div class="col-sm-6 text-alignRight">
        
        <a ui-sref="new-meal-items({new_meal_id: meal.new_meal_id, meal_template_id: meal.meal_template.id})"
            class="btn btn-default" data-original-title="New Meal"><span><i class="fa fa-plus"></i></span>&nbsp;New Meal</a>
        <a ui-sref="template-names" class="btn btn-default"><span>&nbsp;</span>Back</a>
      </div>
  </div>

<div class="block md_recommended_proui" >

 
<!-- 
    <div class="block-title">
      <h2>Meal</h2> 
        <div class="block-options pull-right">
          <a href="#" class="btn btn-alt btn-sm btn-default btn-round" data-toggle="modal" data-target="#addMealModal">
            <i class="fa fa-plus text-success"></i>
          </a>
        </div>
    </div> -->
    <!-- Add meal Modal -->
    <div id="addMealModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-primary">
            <button class="close white_close" type="button" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add Meal To @{{meal.meal_template.template_name}}</h4>
          </div>
          <div class="modal-body">
            <form name="createRecommendedMeal" class="form-horizontal" novalidate="novalidate">
                <div ng-if="meal.meal_exist" class="form-group">
                  <div class="col-sm-4"></div>
                  <div class="col-sm-8 red_font">
                      Meal is already exist in this template on this time
                  </div>
                  
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label title_blue" for="add_meal">Meal Name</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" ng-model="meal.meal_name" required/>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label title_blue" for="add_meal">Meal Time</label>
                  <div class="col-sm-6">                      
                     <time-picker ng-model="meal.meal_time"></time-picker>
                  </div>
                </div>
                <!-- <div class="form-group">
                  
                  <label class="col-sm-4 control-label title_blue" for="add_meal">Template Name</label>
                  <div class="col-sm-6">                      
                      
                      <select name="mySelect" id="mySelect"
                          ng-options="value.id as value.template_name+' ('+value.template_for +')' for value in meal.template_names track by value.id"
                          ng-model="data.selectedOption"
                          class="form-control">
                          <option value="">Select</option>
                      </select>
                  </div>
                </div> -->
                
                <div class="form-group">
                  <div class="col-sm-4"></div>
                  <div class="col-sm-8">
                    <button type="submit" ng-disabled="createRecommendedMeal.$invalid" ng-click="meal.createRecommendedMeal()" class="btn btn-success">Add</button>          

                  </div>
                </div>
            </form>

            <!-- <div class="center_text_for_line">
              <span class="centered_text_of_line">
                Or 
              </span>
            </div> -->

          </div>
          <!-- <div class="text-center">
             <a class="btn btn-success" ng-click="meal.goToCreateNewTemplateName()">Create New Template</a>      
          </div> -->
          <div class="saperator_5"></div>
        </div>
      </div>
    </div>
    <!-- End of Add Meal Modal -->    

    <div class="md_rec_food_class" id="mdrecommeded_meal_table">
      <table class="table recommeded_table">
        <thead>
          <tr class="">
            <th class="text-center bg-primary width_60">MEAL NAME</th>
            <th class="text-center bg-primary width_15">MAX CALORIE</th>
            <th class="text-center bg-primary width_10">ACTION</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="recommended_meal in meal.recommended_meals">
             <td data-title="MEAL NAME" class="text-left">
                <!-- <a href="javascript:void(0)" ui-sref="meal-items({recommended_meal_id: recommended_meal.id, recommended_meal_name: recommended_meal.meal_name})">@{{recommended_meal.meal_name}}</a> -->
                <small class="meal_food_name" ng-if="recommended_meal.meal_foods_names.length!=0">
                  &nbsp;
                  [<span ng-repeat="food_name in recommended_meal.meal_foods_names track by $index">
                    <a class="meal_food_name" href="javascript:void(0)" 
                      ui-sref="meal-items({recommended_meal_id: recommended_meal.id, recommended_meal_name: recommended_meal.meal_name})"
                    title="@{{ food_name}}">@{{ food_name }}</a>
                    <span ng-if="recommended_meal.meal_foods_names.length-1!=$index">, </span> 
                  </span>]
                </small>
             </td>
             
             <td data-title="MAX CALORIE" class="text-center">@{{recommended_meal.max_calorie}}</td>
              
              <td data-title="ACTION" class="text-center">
                <div class="btn-group btn-group-xs">
                  <a class="btn btn-default" href="javascript:void(0)" ui-sref="meal-items({recommended_meal_id: recommended_meal.id, recommended_meal_name: recommended_meal.meal_name })" data-original-title="Edit">
                    <i class="fa fa-pencil-square-o"></i>
                  </a>
                  <a class="btn btn-danger" title="" ng-click="meal.deleteRecommendedMeal(recommended_meal)" data-toggle="tooltip" href="javascript:void(0)" data-original-title="Delete">
                    <i class="fa fa-times"></i>
                  </a>
                </div>
              </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!-- END Block Content -->
</div>
<!-- END of MD Recommended Food Block