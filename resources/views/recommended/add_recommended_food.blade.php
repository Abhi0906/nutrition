<div class="row block-title patient_title" ng-init="add_food.getFoodRecommendedTypes();">
      <div class="col-sm-6">
        <h3><strong>Edit @{{add_food.recommended_type_name}}&nbsp;</strong></h3>
      </div>  
      <div class="col-sm-6 text-alignRight">
                  <!--    <a class="btn btn-alt btn-sm btn-default " 
                     title="Back" ng-click="edit_food.backToItemPage();">Back</a> -->

                     <a ui-sref="foods" class="btn btn-default"> <i class="gi gi-remove_2"></i>
                     &nbsp;Cancel</a>

                    <!--  <a class="btn btn-alt btn-sm btn-default " 
                     title="Back" ui-sref="foods">Back</a> -->


      </div>
    </div>

<div >   
   <!--  <ul class="breadcrumb breadcrumb-top">
        <li>Pages</li>
        <li><a href="javascript:void(0);">MD Recommended Foods</a></li>
    </ul> -->
    
    <!--MD Recommended Food Block -->
    <div class="block">
        
        <!-- <div class="row">
          <div class="col-sm-12">
            <h3 class="text-center title_blue text-uppercase">@{{add_food.recommended_type_name}}</h3>
          </div>
          <div class="col-sm-12">
            <p><a class="btn btn-primary" ui-sref="foods">Back</a></p>
          </div>
        </div> -->
        <div class="block">
          <div class="block-title">
            
           <!--  <div class="block-options pull-right">
                    <a class="btn btn-alt btn-sm btn-default " 
                     title="Back" ui-sref="foods">Back</a>
            </div>  
             <h2><strong>@{{add_food.recommended_type_name}}&nbsp;</strong></h2> 
          </div>
 -->

          <div>
            <form class="form-horizontal">
              <div class="form-group"> <br>
                <label class="col-sm-2 control-label title_blue" for="add_meal">Enter Food</label>
                <div class="col-sm-8 space_col_right_8">
                    <div class="input-group">     
                      <input type="text" ng-model="add_food.searchKeyFood" class="form-control">
                      <span class="input-group-addon no_padding">
                        <button type="submit" class="btn btn-info text-white" ng-click="add_food.searchFood()">Search</button>
                      </span>
                    </div>
                </div>
                <div class="col-sm-2 no_col_padding">
                  <button class="btn btn-warning margin_top_10" ng-click="add_food.clear_logFood_searchResult()">Clear</button>          
                </div>
              </div>
            </form>
          </div>
          <hr/>
          <div id="addrecommeded_food_table">
            <table class="table table-bordered recommeded_table add_food_tbl">
              <thead class="bg-primary">
                <tr>
                  <th class="text-center width_30">FOOD NAME</th>
                  <th class="text-center">CALORIES</th>
                  <th class="text-center">Serving Size</th>
                  <th class="text-center">Serving Range</th>
                  <th class="text-center">Serving Default</th>
                  <th class="text-center">Food Type</th>
                  <th class="text-center">Food For</th>
                  <th class="text-center">ADD FOOD</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="fooddata in add_food.foodSearchdata">
                  <td class="text-left" data-title="FOOD NAME">
                    <p class="theme_text_p_lg_md">@{{fooddata.name}}
                      <span><a href="javascript:void(0);"></a></span>
                    </p>
                    <p class="tag_text_normal">@{{fooddata.brand_name}}, 
                      @{{fooddata.nutrition_data.serving_quantity}} @{{fooddata.nutrition_data.serving_size_unit}}
                    </p>
                  </td>
                  <td data-title="CALORIES" class="text-center">
                    <p class="food_log_normalbold"
                     ng-bind="add_food.recommended_food[$index].calories" 
                     ng-init="add_food.recommended_food[$index].calories = fooddata.nutrition_data.calories">
                    </p>
                  </td>

                  <td data-title="SERVING SIZE">
                    <select class="form-control"
                             ng-init="add_food.recommended_food[$index].serving_string = fooddata.serving_data[0]" 
                             ng-model="add_food.recommended_food[$index].serving_string" 
                             ng-options="serving.label for serving in fooddata.serving_data"
                             ng-change="add_food.calculateServingSize($index, fooddata);">
                      </select>
                  </td>

                    <td data-title="SERVING RANGE">
                        <select class="form-control"
                                ng-init="add_food.recommended_food[$index].serving_range = 1"
                                ng-model="add_food.recommended_food[$index].serving_range"
                                ng-options="range for range in add_food.serving_ranges"
                                ng-change="add_food.updateCaloriesRange($index, fooddata);">
                        </select>
                    </td>

                  <td data-title="SERVING DEFAULT">
                   <input type="text"
                       ng-init="add_food.recommended_food[$index].no_of_servings =1"
                        size="6"
                         ng-model="add_food.recommended_food[$index].no_of_servings"
                         ng-change="add_food.calculateServingSize($index, fooddata);"
                         class="form-control"
                        >                     
                  </td>
                  
                  <td data-title="FOOD TYPE">                    

                      <select class="form-control"
                             ng-init="add_food.recommended_food[$index].food_recommendation_type_id = add_food.recommended_type_id" 
                             ng-model="add_food.recommended_food[$index].food_recommendation_type_id" 
                             ng-options="recommended_type.id as recommended_type.name for recommended_type in add_food.recommended_types"
                             >
                     </select>
                  </td>

                  <td data-title="FOOD FOR">
                    <food-for class="form-control" ng-model="add_food.recommended_food[$index].food_for" 
                          ng-init="add_food.recommended_food[$index].food_for = 'Any'"
                          ng-change="add_food.updateDefaultServing($index, fooddata);"
                    ></food-for>
                  </td>

                  <td data-title="LOG FOOD" class="text-center">
                    <p><button class="btn btn-primary" ng-click="add_food.createMdRecommendedFood($index, fooddata)">Add Food</button></p>
                    <p><a href="javascript:void(0)" ng-click="add_food.viewFoodDetail($index,fooddata)" class="link_clor">View Details</a></p>
                  </td>
                </tr>
              </tbody>
            </table>
            <!-- <div ng-if="!add_food.foodSearchdata.length || add_food.foodSearchdata.length==0" class="text-center">Search food to add in recommendation</div>    -->
            <div ng-show="add_food.search_loader"  class="search_food_loader"></div>
            <br><br><br><br>
          </div>
        </div>
        <!-- END Block Content -->
    </div>
    <!-- END of MD Recommended Food Block -->
     
</div>
<!-- view models -->
<div id="view_food_detail_tmplt" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
  <view-food-detail details="add_food.food_detail"></view-food-detail>
</div>