<div ng-init="recommend_food.mdRecommendedFoodsDisplayData() ">
 <!--  <ul class="breadcrumb breadcrumb-top">
      <li>Pages</li>
      <li><a href="javascript:void(0);">MD Recommended Foods</a></li>
  </ul> -->

  <!--MD Recommended Food Block -->
<div class="row block-title patient_title">
      <div class="col-sm-6">
        <h3><strong>MD Recommended Foods</strong></h3>
      </div>  
      <div class="col-sm-6 text-alignRight">
          
      </div>
</div>

  <div class="block">
      <div class="block-title">
      <div class="row padding_top_bottom">
        <div class="col-sm-6">
         <!--  <h2><strong>MD Recommended Foods</strong></h2> -->
        </div>
        <div class="col-sm-6 text-right">
          <div class="form-inline">
                   Sort: <select class="form-control" ng-options="option.value as option.name for option in recommend_food.displayShortRecords"  
                   ng-model="recommend_food.itemDefault"></select>&nbsp;&nbsp;
          </div>
        </div>
      </div>

          
          
      </div>


      <div class="md_rec_food_class" id="mdrecommeded_food_table">

        <table class="table recommeded_table">
          <thead>   
            <tr>
              <td class="no_padding">
                <table class="table no_border margin_bottm_0">
                  <tr>
                    <th class="width_30">&nbsp;</th>
                    <th class="text-center bg-primary width_10">FOOD FOR</th>
                    <th class="text-center bg-primary width_10">Serving Size</th>
                    <th class="text-center bg-primary width_10">Serving Range</th>
                    <th class="text-center bg-primary width_10">Serving Default</th>
                    <th class="text-center bg-primary width_10">CALORIES</th>
                    <th class="text-center bg-primary width_10">FAT</th>
                    <th class="text-center bg-primary width_10">FIBER</th>
                    <th class="text-center bg-primary width_10">CARBS</th>
                    <th class="text-center bg-primary width_10">SODIUM</th>
                    <th class="text-center bg-primary width_10">PROTEIN</th>
                    
                  </tr>
                </table>
              </td>
            </tr>
          </thead>
          <tbody>
            <tr class="tr_no_border" ng-repeat="recom_food in recommend_food.recommended_foods">
              <td class="no_padding">
                <table class="table no_border margin_bottm_0">
                  <tr class="subhead-mdrec">
                    <td colspan="11">
                      <h4 class="text-uppercase">@{{recom_food.recommended_type_name}}</h4>
                      <ul class="list-inline">
                        <li>
                          <i class="fa fa-plus-circle facolor"></i> 
                          <a ui-sref="@{{recommend_food.add_food_url}}({ recommended_type_id : recom_food.recommended_type_id,recommended_type_name: recom_food.recommended_type_name})">
                            Add Food
                          </a> 
                        </li>
                      </ul>
                    </td>
                  </tr>
                  <tr ng-repeat="food in recom_food.recommended_foods | orderBy:recommend_food.itemDefault:false">
                    <td class="width_30" data-title="NAME">
                      <a href="javascript:void(0);" ng-click="recommend_food.deleteRecommendedFood(recom_food, food)">
                        <i class="fa fa-times-circle facolor"></i>
                      </a>
                      <a ui-sref="edit-food({ edit_from: 'md_food',
                                              recommended_type_id : recom_food.recommended_type_id, 
                                              food_data: food,
                                              recommended_types: recommend_food.recommended_types,
                                              recommended_food_id: food.recommended_food_id
                                            })">
                        <!-- <i class="fa fa-pencil-square fa-gray"></i> -->
                        @{{food.food_name}}
                      </a> 
                      
                    </td>
                     <td data-title="FOOD FOR" class="text-center width_10">@{{food.food_for}}</td>
                     <td data-title="SERVING SIZE" class="text-center width_10">@{{food.serving_string}}</td>
                     <td data-title="SERVING RANGE" class="text-center width_10">@{{food.serving_range}}</td>
                     <td data-title="SERVING DEFAULT" class="text-center width_10">@{{food.no_of_servings}}</td>
                     <td data-title="CALORIES" class="text-center width_10">@{{food.calories}}</td>
                     <td data-title="FAT" class="text-center width_10">@{{food.total_fat}}</td>
                     <td data-title="FIBER" class="text-center width_10">@{{food.dietary_fiber}}</td>
                     <td data-title="CARBS" class="text-center width_10">@{{food.total_carb}}</td>
                     <td data-title="SODIUM" class="text-center width_10">@{{food.sodium}}</td>
                     <td data-title="PROTEIN" class="text-center width_10">@{{food.protein}}</td>
                     
                      
                  </tr>
                </table>
              </td>
            </tr>
          </tbody>
          </table>
      </div>
      <!-- END Block Content -->
  </div>
  <!-- END of MD Recommended Food Block -->
</div>