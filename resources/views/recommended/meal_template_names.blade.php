<div class="row block-title patient_title" ng-init="template_name.getTemplateNames();">
    <div class="col-sm-6">
      <h3><strong>Meal Template Names</strong></h3>
    </div>  
    <div class="col-sm-6 text-alignRight">
     
      <a ui-sref="creat-templatename"class="btn btn-default"><span><i class="fa fa-plus"></i></span>&nbsp;New Template Name</a>
      
    </div>
</div>

<div class="block md_recommended_proui" >

    <div class="md_rec_food_class">
      <table class="table recommeded_table">
        <thead>
          <tr class="">            
            <th class="text-center bg-primary width_60">Male Template Name</th>
            <th class="text-center bg-primary width_10">View Meals</th>
            <th class="text-center bg-primary width_10">Default</th>
            <th class="text-center bg-primary width_10">ACTION</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="template in template_name.template_names | orderBy: '-id' | filter : maleTemplateFilter">
             <td data-title="TEMPLATE NAME" class="text-left">
                @{{template.template_name}}                
             </td>
             <td>
                <a ui-sref="meals({ id: template.id, template_name: template.template_name})">View Meals</a>
             </td>
             <td data-title="DEFAULT" class="text-center">
              <input type="radio" ng-model="template.is_default" 
                                  name="male_template" 
                                  value="template.is_default" 
                                  ng-value='true'
                  ng-click="template_name.setDefaultTemplate(template.id)">
                
              </td>       
              <td data-title="ACTION" class="text-center">
                <div class="btn-group btn-group-xs">
                  <a class="btn btn-default" 
                  ui-sref="edit-template({ id: template.id })" data-original-title="Edit">
                    <i class="fa fa-pencil-square-o"></i>
                  </a>
                  <a href="javascript:void(0)" class="btn btn-danger" title="" ng-disabled="template.is_default==1" ng-click="template_name.deleteTemplate(template)" data-toggle="tooltip" data-original-title="Delete">
                    <i class="fa fa-times"></i>
                  </a>
                </div>
              </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="md_rec_food_class">
      <table class="table recommeded_table">
        <thead>
          <tr class="">            
            <th class="text-center bg-primary width_60">Female Template Name</th>
            <th class="text-center bg-primary width_10">View Meals</th>
            <th class="text-center bg-primary width_10">Default</th>
            <th class="text-center bg-primary width_10">ACTION</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="template in template_name.template_names | orderBy: '-id' | filter: femaleTemplateFilter">
             <td data-title="TEMPLATE NAME" class="text-left">
                @{{template.template_name}}                
             </td>
             <td>
                <a ui-sref="meals({ id: template.id, template_name: template.template_name})">View Meals</a>
             </td>
             
             <td data-title="DEFAULT" class="text-center">
                <input type="radio" ng-model="template.is_default"
                    name="female_template" value="template.is_default" 
                    ng-click="template_name.setDefaultTemplate(template.id)"
                    ng-value="true">
              
              </td>
                           
              <td data-title="ACTION" class="text-center">
                <div class="btn-group btn-group-xs">
                  <a class="btn btn-default" 
                  ui-sref="edit-template({ id: template.id })" data-original-title="Edit">
                    <i class="fa fa-pencil-square-o"></i>
                  </a>
                  <a href="javascript:void(0)" class="btn btn-danger" title="" ng-disabled="template.is_default==1" ng-click="template_name.deleteTemplate(template)" data-toggle="tooltip"  data-original-title="Delete">
                    <i class="fa fa-times"></i>
                  </a>
                </div>
              </td>
          </tr>
        </tbody>
      </table>
    </div>
</div>
