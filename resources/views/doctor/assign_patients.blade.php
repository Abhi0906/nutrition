<script type="text/ng-template" id="assign_patients">
	
<div id="no-more-tables">
    <table class="table table-vcenter table-striped">
         <thead>
        <tr>
            <th width="7%" class="text-center"></th>
            <th width="14%"  ng-click="assign_patients.sort('full_name')" class="table_list cursor text-primary">Name<small ng-show="assign_patients.sortKey=='full_name'" ng-class="{'fa fa-caret-up fa-fw':assign_patients.reverse,'fa fa-caret-down fa-fw':!assign_patients.reverse}"></small>
                </th>

            <th width="9%" ng-click="assign_patients.sort('profile.baseline_weight')" class="table_list cursor text-primary">Weight<small ng-show="assign_patients.sortKey=='profile.baseline_weight'" ng-class="{'fa fa-caret-up fa-fw':assign_patients.reverse,'fa fa-caret-down fa-fw':!assign_patients.reverse}"></small></th>

            <th width="9%" ng-click="assign_patients.sort('profile.calories')" class="table_list cursor text-primary">Calories<small ng-show="assign_patients.sortKey=='profile.calories'" ng-class="{'fa fa-caret-up fa-fw':assign_patients.reverse,'fa fa-caret-down fa-fw':!assign_patients.reverse}"></small></th>


            <th width="13%" class="table_list cursor hidden-sm hidden-xs text-primary" ng-click="assign_patients.sort('created_at')">Created<small ng-show="assign_patients.sortKey=='created_at'" ng-class="{'fa fa-caret-up fa-fw':assign_patients.reverse,'fa fa-caret-down fa-fw':!assign_patients.reverse}"></th>
            
            <th class="text-primary table_list cursor" width="8%" ng-click="assign_patients.sort('status')">Status<small ng-show="assign_patients.sortKey=='status'" ng-class="{'fa fa-caret-up fa-fw':assign_patients.reverse,'fa fa-caret-down fa-fw':!assign_patients.reverse}"></small></th>

            <th width="8%" class="table_list text-center text-primary">Actions</th>
        </tr>
   		 </thead>
        <tbody>
         <tr ng-repeat="user in assign_patients.assign_patients|orderBy:assign_patients.sortKey:assign_patients.reverse|filter:search">
            <td class="text-center">
	            <a href="{{URL::to('/patient')}}/@{{user.id}}">
	                <img ng-if = "user.photo != null" ng-src="/uploads/@{{user.photo}}" alt="@{{user.full_name}}" class="img-circle" width="40px" />
	                <img ng-if = "user.photo == null" ng-src="/uploads/no_img.png" alt="@{{user.full_name}}" class="img-circle" width="40px" />
	            </a>
            </td>
            <td data-title="Name">
              <a href="{{URL::to('/patient')}}/@{{user.id}}">@{{user.full_name}}</a><br/>
                @{{user.email }}<br/> 
                <span ng-if="user.phone != '' && user.phone != null ">@{{user.phone}}</span>
            </td>
            <td data-title="Weight">
                <span class="label label-default">@{{user.profile.baseline_weight}}</span>
            </td>
            <td data-title="Calories">
                <span class="label label-default">@{{user.profile.calories}}</span>
            </td>
            
            <td data-title="Created" class="hidden-sm hidden-xs" ng-bind="assign_patients.getDate(user.created_at)"></td>
            <td data-title="Status">
                <span ng-if="user.status=='Active'" class="label label-success">@{{user.status }}</span>
                <span ng-if="user.status=='Inactive'"  class="label label-warning">@{{user.status }}</span>
                <span ng-if="user.status=='Guest'"  class="label label-info">@{{user.status }}</span>
                <span ng-if="user.status=='Incomplete'"  class="label label-danger">@{{user.status }}</span>
            </td>
            <td class="text-center" data-title="Actions">
                <div class="btn-group btn-group-xs">
                    <a href="javascript:void(0)" ng-click="assign_patients.unassignPatientFromDoctor(user);" data-toggle="tooltip" title="Unassign Patient" ><i class="fa fa-user-times text-danger fa-2x"></i></a>
                </div>
            </td>
         </tr>
        </tbody>
    </table>
 </div>


</script>

