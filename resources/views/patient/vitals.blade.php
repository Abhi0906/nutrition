<script type="text/ng-template" id="vitals">
<div class="row">
    <div class="col-xs-12">
        <div class="block">
            <!-- Tickets Title -->
            <div class="block-title">
                <ul class="nav nav-tabs" gene-tabs data-toggle="tabs">
                    <li class="active" style="font-size:16px !important"><a href="#weight"><i style="font-size:20px !important;    vertical-align: sub;" class="wellness-icon-weight text-info vitals_icon"></i> &nbsp;Weight</a></li>
                    <li  style="font-size:16px !important"><a href="#blood_pressure"><i style="font-size:20px !important;    vertical-align: sub;" class="wellness-icon-bloodpresure text-info vitals_icon"></i> &nbsp;Blood Pressure</a></li>
                    <li style="font-size:16px !important"><a href="#pulse_oximeter"><i style="font-size:20px !important;    vertical-align: sub;" class="wellness-icon-pulseox text-info vitals_icon"></i></i> &nbsp;Pulse Oximeter</a></li>
                    <li style="font-size:16px !important"><a href="#blood_sugar"><i style="font-size:20px !important;    vertical-align: sub;" class="wellness-icon-bloodsugar text-info vitals_icon"></i></i> &nbsp;Blood Sugar</a></li>
                </ul>
            </div>
            <!-- END Tickets Title -->

            <!-- Tabs Content -->
            <div class="tab-content">

                <div class="tab-pane active" id="weight">
                    <h4 class="text-warning">Limits</h4>
                    <form action="javascript:void(0);"  class="animation-fadeInQuick" method="post" onsubmit="return false;" >
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn vitals-width">
                                    <span class="btn btn-primary vitals-width-btn">
                                        Baseline
                                    </span>
                                </span>
                                <input type="text"
                                            id="baseline_weight"
                                            name="baseline-weight"
                                            class="form-control"
                                            placeholder="Enter your baseline weight"
                                            maxlength="5"
                                            ng-model="vitals.weight_parameters.baseline_weight">

                                <span class="input-group-btn">
                                    <span class="btn btn-primary">lbs</span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn vitals-width">
                                    <span class="btn btn-primary vitals-width-btn">Max Weight</span>
                                </span>
                                <input type="text"
                                         id="max_weight"
                                         name="max-weight"
                                         class="form-control"
                                         placeholder="Enter your Max weight"
                                         maxlength="5"
                                         ng-model="vitals.weight_parameters.max_weight">
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">lbs</span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn vitals-width">
                                    <span class="btn btn-primary vitals-width-btn">Min Weight</span>
                                </span>
                                <input type="text"
                                         id="min_weight"
                                         name="min-weight"
                                         class="form-control"
                                         placeholder="Enter your Min weight"
                                         maxlength="5"
                                         ng-model="vitals.weight_parameters.min_weight" >
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">lbs</span>
                                </span>
                            </div>
                        </div>
                        <h4 class="text-warning">Daily</h4>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn vitals-width">
                                    <span class="btn btn-primary vitals-width-btn">High Alert &nbsp; >=</span>
                                </span>
                                <input type="text"
                                         id="high_alert_weight"
                                         name="high-alert-weight"
                                         class="form-control"
                                         placeholder="Enter your High alert for daily"
                                         maxlength="5"
                                         ng-model="vitals.weight_parameters.high_alert_lbs">
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">lbs</span>
                                </span>
                            </div>
                            or
                            <div class="input-group">
                                <input type="text"
                                         id="high_alert_weight_per"
                                         name="high-alert-weight-per"
                                         class="form-control"
                                         placeholder="Enter your High alert for daily"
                                         maxlength="5"
                                         ng-model="vitals.weight_parameters.high_alert_per">
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">&nbsp;%&nbsp;</span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn vitals-width">
                                    <span class="btn btn-primary vitals-width-btn">Medium Alert &nbsp; >=</span>
                                </span>
                                <input type="text"
                                         id="medium_alert_weight"
                                         name="medium-alert-weight"
                                         class="form-control"
                                         placeholder="Enter your Medium alert for daily"
                                         maxlength="5"
                                         ng-model="vitals.weight_parameters.med_alert_lbs">

                                         
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">lbs</span>
                                </span>
                            </div>
                            or,
                            <div class="input-group">
                                <input type="text"
                                         id="medium_alert_weight_per"
                                         name="medium-alert-weight-per"
                                         class="form-control"
                                         placeholder="Enter your Medium alert for daily"
                                         maxlength="5"
                                         ng-model="vitals.weight_parameters.med_alert_per">
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">&nbsp%&nbsp</span>
                                </span>
                            </div>
                        </div>
                        <h4 class="text-warning">Weekly</h4>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn vitals-width">
                                    <span class="btn btn-primary vitals-width-btn">Medium Alert &nbsp; >=</span>
                                </span>
                                <input type="text" 
                                        id="medium_alert_weight_weekly"
                                        name="medium-alert-weight-weekly"
                                        class="form-control"
                                        placeholder="Enter your Medium alert for weekly"
                                        maxlength="5"
                                        ng-model="vitals.weight_parameters.med_alert_lbs_weekly">
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">lbs</span>
                                </span>
                            </div>
                            or,
                            <div class="input-group">
                                <input type="text" 
                                        id="medium_alert_weight_weekly_per"
                                        name="medium-alert-weight-weekly-per"
                                        class="form-control"
                                        placeholder="Enter your Medium alert for weekly"
                                        maxlength="5"
                                        ng-model="vitals.weight_parameters.med_alert_per_weekly">
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">&nbsp;%&nbsp;</span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="" style="text-align:center">
                                <button type="button" class="btn btn-sm btn-info" ng-click="vitals.updateVital(vitals.weight_parameters,'weight')">Save</button>
                                <button type="button" class="btn btn-sm btn-warning" ng-click="vitals.reset('weight')">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="tab-pane" id="blood_pressure">
                    <form action="javascript:void(0);"  class="animation-fadeInQuick" method="post" onsubmit="return false;">
                        <h4 class="text-warning">Systolic</h4>
                        <h5 class="text-warning"><strong>Upper Boundary</strong></h5>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn vitals-width">
                                    <span class="btn btn-primary vitals-width-btn">High Alert &nbsp; >=</span>
                                </span>
                                <input type="text"
                                         class="form-control"
                                         id="systolic_high_upper"
                                         placeholder="Enter your High alert for systolic upper boundary"
                                         maxlength="5"
                                         ng-model="vitals.blood_pressure_parameters.systolic_high_ul" >
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">mm Hg</span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn vitals-width">
                                    <span class="btn btn-primary vitals-width-btn">Medium Alert &nbsp; >=</span>
                                </span>
                                <input type="text"
                                         class="form-control"
                                         id="systolic_med_upper"
                                         placeholder="Enter your Medium alert for systolic upper boundary"
                                         maxlength="5"
                                         ng-model="vitals.blood_pressure_parameters.systolic_med_ul" >
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">mm Hg</span>
                                </span>
                            </div>
                        </div>
                        <h5 class="text-warning"><strong>Lower Boundary</strong></h5>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn vitals-width">
                                    <span class="btn btn-primary vitals-width-btn">High Alert &nbsp; <=</span>
                                </span>
                                <input type="text"
                                         class="form-control"
                                         id="systolic_high_lower"
                                         placeholder="Enter your High alert for systolic lower boundary"
                                         maxlength="5"
                                         ng-model="vitals.blood_pressure_parameters.systolic_high_ll" >
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">mm Hg</span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn vitals-width">
                                    <span class="btn btn-primary vitals-width-btn">Medium Alert &nbsp; <=</span>
                                </span>
                                <input type="text"
                                         class="form-control"
                                         id="systolic_med_lower"
                                         placeholder="Enter your Medium alert for systolic lower boundary"
                                         maxlength="5"
                                         ng-model="vitals.blood_pressure_parameters.systolic_med_ll" >
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">mm Hg</span>
                                </span>
                            </div>
                        </div>
                        <h4 class="text-warning">Diastolic</h4>
                        <h5 class="text-warning"><strong>Upper Boundary</strong></h5>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn vitals-width">
                                    <span class="btn btn-primary vitals-width-btn">High Alert &nbsp; >=</span>
                                </span>
                                <input type="text"
                                      class="form-control"
                                      id="diastolic_high_upper"
                                      placeholder="Enter your High alert for diastolic upper boundary"
                                      maxlength="5"
                                      ng-model="vitals.blood_pressure_parameters.diastolic_high_ul" >
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">mm Hg</span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn vitals-width">
                                    <span class="btn btn-primary vitals-width-btn">Medium Alert &nbsp; >=</span>
                                </span>
                                <input type="text"
                                        class="form-control"
                                        id="diastolic_med_upper"
                                        placeholder="Enter your Medium alert for diastolic upper boundary"
                                        ng-model="vitals.blood_pressure_parameters.diastolic_med_ul" >
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">mm Hg</span>
                                </span>
                            </div>
                        </div>
                        <h5 class="text-warning"><strong>Lower Boundary</strong></h5>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn vitals-width">
                                    <span class="btn btn-primary vitals-width-btn">Medium Alert &nbsp; <=</span>
                                </span>
                                <input type="text"
                                         class="form-control"
                                         id="diastolic_med_lower"
                                         placeholder="Enter your Medium alert for diastolic lower boundary"
                                         maxlength="5"
                                         ng-model="vitals.blood_pressure_parameters.diastolic_med_ll" >
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">mm Hg</span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="" style="text-align:center">
                                <button type="button" class="btn btn-sm btn-info" ng-click="vitals.updateVital(vitals.blood_pressure_parameters,'blood_pressure')">Save</button>
                                <button type="button" class="btn btn-sm btn-warning" ng-click="vitals.reset('blood_pressure')">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="tab-pane" id="pulse_oximeter">
                    <h4 class="text-warning">Oxygen Limits</h4>
                    <form action="javascript:void(0);"  class="animation-fadeInQuick" method="post" onsubmit="return false;">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn vitals-width">
                                    <span class="btn btn-primary vitals-width-btn">High Alert &nbsp; <=</span>
                                </span>
                                <input type="text"
                                         id="high_alert_ox"
                                         name="high-alert-ox"
                                         class="form-control"
                                         placeholder="Enter your High alert for Oxygen limits"
                                         maxlength="5"
                                         ng-model="vitals.pulse_ox_parameters.high_alert" >
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">&nbsp;&nbsp;&nbsp;%&nbsp;&nbsp;</span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn vitals-width">
                                    <span class="btn btn-primary vitals-width-btn">Medium Alert &nbsp; <=</span>
                                </span>
                                <input type="text"
                                         id="medium_alert_ox"
                                         name="medium-alert-ox"
                                         class="form-control"
                                         placeholder="Enter your Medium alert for Oxygen limits"
                                         maxlength="5"
                                         ng-model="vitals.pulse_ox_parameters.med_alert">
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">&nbsp;&nbsp;&nbsp;%&nbsp;&nbsp;</span>
                                </span>
                            </div>
                        </div>
                        <h4 class="text-warning">Pulse Limits</h4>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn vitals-width">
                                    <span class="btn btn-primary vitals-width-btn">High Alert &nbsp; >=</span>
                                </span>
                                <input type="text"
                                         id="high_alert_pulse"
                                         name="high-alert-pulse"
                                         class="form-control"
                                         placeholder="Enter your High alert for Pulse limits"
                                         maxlength="5"
                                         ng-model="vitals.pulse_ox_parameters.high_alert_pulse" >
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">bpm</span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn vitals-width">
                                    <span class="btn btn-primary vitals-width-btn">Medium Alert &nbsp; <=</span>
                                </span>
                                <input type="text"
                                         id="medium_alert_pulse"
                                         name="medium-alert-pulse"
                                         class="form-control"
                                         placeholder="Enter your Medium alert for Pulse limits"
                                         maxlength="5"
                                         ng-model="vitals.pulse_ox_parameters.med_alert_pulse" >
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">bpm</span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="" style="text-align:center">
                                <button type="button" class="btn btn-sm btn-info" ng-click="vitals.updateVital(vitals.pulse_ox_parameters,'pulse_ox')">Save</button>
                                <button type="button" class="btn btn-sm btn-warning" ng-click="vitals.reset('pulse_ox')">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="tab-pane" id="blood_sugar">
                    <form action="javascript:void(0);"  class="animation-fadeInQuick" method="post" onsubmit="return false;">
                        <h4 class="text-warning">All Test Types</h4>
                        <h5 class="text-warning"><strong>Lower Boundary</strong></h5>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn vitals-width">
                                    <span class="btn btn-primary vitals-width-btn">High Alert &nbsp; <=</span>
                                </span>
                                <input type="text"
                                         class="form-control"
                                         id="lower_high"
                                         placeholder="Enter your High alert for lower boundary"
                                         maxlength="5"
                                         ng-model="vitals.blood_sugar_parameters.lb_high" >
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">mg/dL</span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn vitals-width">
                                    <span class="btn btn-primary vitals-width-btn">Medium Alert &nbsp; <=</span>
                                </span>
                                <input type="text"
                                    class="form-control"
                                    id="lower_med"
                                    placeholder="Enter your Medium alert for lower boundary"
                                    maxlength="5"
                                    ng-model="vitals.blood_sugar_parameters.lb_med" >
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">mg/dL</span>
                                </span>
                            </div>
                        </div>
                        <h4 class="text-warning">Random Check</h4>
                        <h5 class="text-warning"><strong>Upper Boundary</strong></h5>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn vitals-width">
                                    <span class="btn btn-primary vitals-width-btn">High Alert &nbsp; >=</span>
                                </span>
                                <input type="text"
                                     class="form-control"
                                      id="random_chk_upper_high"
                                      placeholder="Enter your High alert for upper boundary"
                                      maxlength="5"
                                      ng-model="vitals.blood_sugar_parameters.ub_random_chk_high" >
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">mg/dL</span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn vitals-width">
                                    <span class="btn btn-primary vitals-width-btn">Medium Alert &nbsp; >=</span>
                                </span>
                                <input type="text"
                                         id="random_chk_upper_med"
                                         class="form-control"
                                         placeholder="Enter your Medium alert for upper boundary"
                                         maxlength="5"
                                         ng-model="vitals.blood_sugar_parameters.ub_random_chk_med" >
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">mg/dL</span>
                                </span>
                            </div>
                        </div>
                        <h4 class="text-warning">Fasting Value</h4>
                        <h5 class="text-warning"><strong>Upper Boundary</strong></h5>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn vitals-width">
                                    <span class="btn btn-primary vitals-width-btn">Medium Alert &nbsp; >=</span>
                                </span>
                                <input type="text"
                                         id="fasting_upper_med"
                                         class="form-control"
                                         placeholder="Enter your Medium alert for upper boundary"
                                         maxlength="5"
                                         ng-model="vitals.blood_sugar_parameters.ub_fasting_med" >
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">mg/dL</span>
                                </span>
                            </div>
                        </div>
                        <h4 class="text-warning">Post Prandial</h4>
                        <h5 class="text-warning"><strong>Upper Boundary</strong></h5>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn vitals-width">
                                    <span class="btn btn-primary vitals-width-btn">Medium Alert &nbsp; >=</span>
                                </span>
                                <input type="text"
                                       id="pp_upper_med"
                                       class="form-control"
                                       placeholder="Enter your Medium alert for upper boundary"
                                       maxlength="5"
                                       ng-model="vitals.blood_sugar_parameters.ub_pp_med" >
                                <span class="input-group-btn">
                                    <span class="btn btn-primary">mg/dL</span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="" style="text-align:center">
                                <button type="button" class="btn btn-sm btn-info" ng-click="vitals.updateVital(vitals.blood_sugar_parameters,'blood_sugar')">Save</button>
                                <button type="button" class="btn btn-sm btn-warning" ng-click="vitals.reset('blood_sugar')">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Tabs Content -->
        </div>
    </div>
</div>
</script>
@section('inline_scripts')
<script type="text/javascript">
  
</script>
@append