<script type="text/ng-template" id="weight">
<div class="row patient_weight_container" ng-init="weight.userId='{{$user_id}}';weight.loggedWeight(7, 'seven-days'); ">

	<!-- Classic and Bars Chart -->
    
    <div class="col-xs-12">
        <!-- Classic Chart Block -->
        <div class="block full">
            <!-- Classic Chart Title -->
            <div class="block-title">
                <h2>Weight</h2>
            </div>
            <!-- END Classic Chart Title -->
            <div class="col-md-12">
	            <button type="button" id="seven-days" class="btn btn-xs btn-info days-filter" ng-click="weight.loggedWeight(7, 'seven-days');">Last 7 Days</button>
	            <button type="button" id="thirty-days" class="btn btn-xs btn-info days-filter" ng-click="weight.loggedWeight(30, 'thirty-days');">Last 30 Days</button>
	            <button type="button" id="ninety-days" class="btn btn-xs btn-info days-filter" ng-click="weight.loggedWeight(90, 'ninety-days');">Last 90 Days</button>
	            <button type="button" id="one-eighty-days" class="btn btn-xs btn-info days-filter" ng-click="weight.loggedWeight(180, 'one-eighty-days');">Last 180 Days</button>
	            <button type="button" id="year" class="btn btn-xs btn-info days-filter" ng-click="weight.loggedWeight(365, 'year');">Last Year</button>
            </div>
     		<!-- amchart -->
            <div id="chartdiv"></div>
            <!-- end: amchart -->
        </div>
        <!-- END Classic Chart Block -->
    </div>
    
    <!-- END Classic and Bars Chart -->

    
	<div class="col-md-5 col-xs-12">
		<form name="LogWeightForm" novalidate="novalidate" class="animation-fadeInQuick">
			<div class="block full">
				<div class="block-title">
			    	<h3>
				        <i class="fa wellness-icon-weight"></i>&nbsp;&nbsp;Log Weight
				    </h3>
				</div>				
				<div class="widget-extra-full log-weight-form">
					<span ng-show="weight.log_loader"  class="log_weight_loader"></span>
					<div class="form-horizontal form-bordered">
						<div class="form-group">
		                  	{!! HTML::decode(Form::label('current_weight', 'Current Weight<sup class="text-danger">*</sup>', array('class'=>'col-md-4 control-label log-weight-lbl-doctor'))) !!}
		                  	<div class="col-md-8  log-weight-lbl-doctor">
		                     	<div class="input-group">
		                        	<input type="text" numbers-only="numbers-only" 
		                           ng-model="weight.log_weight.current_weight"
		                           maxlength="10"
		                           placeholder="Current Weight" name="current_weight" id="current_weight" class="form-control" required>
		                        	<span class="input-group-btn">
		                        		<span class="btn btn-primary">&nbsp;&nbsp;lbs&nbsp;&nbsp;</span>
		                        	</span>
		                     	</div>
		                     	<p class="text-danger" ng-show="LogWeightForm.current_weight.$invalid && LogWeightForm.current_weight.$touched"><small>You must fill out your valid current weight.</small></p>
		                  	</div>
		               	</div>
		               	<div class="form-group">
		                  	{!! HTML::decode(Form::label('body_fat', 'Body Fat<sup class="text-danger">*</sup>', array('class'=>'col-md-4 control-label'))) !!}
		                  	<div class="col-md-8 log-weight-lbl-doctor">
		                     	<div class="input-group">
		                        	<input type="text" numbers-only="numbers-only" 
		                           		ng-model="weight.log_weight.body_fat"
		                           		maxlength="5"
		                           		ng-blur="weight.validBodyFat(weight.log_weight.body_fat);"
		                           		ng-keyup="weight.validBodyFat(weight.log_weight.body_fat);"
		                           		placeholder="Body Fat" name="body_fat" id="body_fat" class="form-control" required>
		                        	<span class="input-group-btn">
		                        		<span class="btn btn-primary">&nbsp;&nbsp; % &nbsp;&nbsp;</span>
		                        	</span>
		                     	</div>
		                     	<p class="text-danger" ng-show="LogWeightForm.body_fat.$invalid && LogWeightForm.body_fat.$touched"><small>You must fill out your valid current body fat.</small></p>
		                     	<p class="text-danger" ng-bind-html="weight.body_fat_error"></p>
		                  	</div>
		               	</div>
		               	<div class="form-group">
		                  	{!! HTML::decode(Form::label('on', 'On<sup class="text-danger">*</sup>', array('class'=>'col-md-4 control-label log-weight-lbl-doctor'))) !!}
		                  	<div class="col-md-8 log-weight-lbl-doctor">
		                     	<div class="input-group">
		                        	<input-datepicker  format="MM dd, yyyy" date="weight.log_weight.log_date" disabled="disabled"></input-datepicker>

		                     	</div>

		                  	</div>
		               	</div>
		               	<div class="form-group">
		                  	<div class="col-md-4"></div>
		                  	<div class="col-md-8  log-weight-lbl-doctor">
		                     	<div class="input-group">
		                        	<button type="button" class="btn btn-primary" ng-disabled="LogWeightForm.$invalid || weight.log_weight_error" ng-click="weight.checkLoggedWeight();">Log</button>
		                     	</div>
		                  	</div>
		               	</div>
					</div>

	                <div class="row">
	                	<div class="col-xs-12 col-md-3">
		                	
		                </div>
	                </div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-md-7 col-xs-12">
		<div class="block full">
            <div class="block-title">
                <h4>
                   Measurement History
                </h4>
            </div>
            <div class="widget-extra-full">
	            <div id="no-more-tables" class="table-responsive">
		            <table class="table table-striped">
		                <tbody>
		                	<tr>
		                        <td class="hidden-xs text-center">Day</td>
		                        <td class="hidden-xs text-center">Weight</td>
		                        <td class="hidden-xs text-center">Body Fat</td>
		                        <td class="hidden-xs text-center">Source</td>
		                    </tr>
		                    <tr ng-repeat="loggedWeight in weight.user_weights">
		                        <td data-title="Day" class="text-center text-muted">@{{loggedWeight.log_date | date:'EEEE, MMM d'}}</td>
		                        <td data-title="Weight" class="text-center text-muted">@{{loggedWeight.current_weight}} lbs</td>
		                        <td data-title="Body Fat" class="text-center text-muted">@{{loggedWeight.body_fat}} %</td>
		                        <td data-title="Source" class="text-center text-muted">@{{loggedWeight.source}}</td>
		                    </tr>
		                </tbody>
		            </table>
		        </div>
            </div>
        </div> 
	</div>
	
	<div id="log_weight_confirmation" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content">
	 			<!-- Modal Header -->
	 			<div class="modal-header  has-warning text-center custom-modal-header">
	 				<div class="alert custom_alert">
	 					
						<i class="fa fa-exclamation-triangle"></i>
	   					<strong> Warning!</strong>
	  				</div>
	      			<div class="modal-body custom-modal-body">
	          			<p class="custom-warning-text"><strong>
	          			Today&#39;s weight is already logged, do you want to replace?</strong></p>
	      				<p class="text-right">
	      					
		                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">No</button>
		                    <button type="button" class="btn btn-sm btn-primary" ng-click="weight.logWeight()">Yes</button>
		                    
		                </p>
	      			</div>
	      			
	    		</div>
			</div>
		</div>
	</div>
</div>
</script>


<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>