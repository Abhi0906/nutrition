<div id="user-schedule" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="container-fluid modal-dialog" style="width:auto !important;">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal" >
                    <span aria-hidden="true" >&times;</span>
                    <span class="sr-only" >Close</span>
                </button>
                <h4 class="modal-title"><strong>Assign Workouts</strong></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                  <div class="col-md-3 hidden-xs">
                    <simple-block block-title="Find Templates">
                      <div class="form-group">
                        <input type="text" ng-model="schedule.keyword" class="form-control" placeholder="enter search term(s)">
                        <small>Search Results: @{{schedule.templates.length}}</small>
                      </div>
                      <hr>
                       <div class="container-fluid" low-margin slim-scroll>
                          <div  ng-class="{widget: true}" ng-repeat="template in schedule.templates"
                            dnd-draggable="template"
                            dnd-effect-allowed="move" style="border: 1px solid rgba(128, 128, 128, 0.53);">
                                <div class="widget-advanced widget-advanced-alt"> 
                                   <!-- Widget Header -->
                                   <div  class="widget-header text-center">
                                      <img ng-src="@{{template.workouts[0].videos[0].pictures.imageUrl}}" class="widget-background animation-pulseSlow img-responsive" style="width:100%;opacity:0.2">
                                      
                                      <div class="widget-icon themed-background-autumn animation-fadeIn">
                                           <i class="fa fa-file-text"></i>
                                      </div>
                                      <h4 class="widget-content">
                                          <strong><a style="text-decoration:none" href="javascript:void(0);" class="text-warning">@{{template.title}}</a></strong><br>
                                          <small class="text-default"><strong></strong></small>
                                      </h4>
                                   </div>
                                    <!-- END Widget Header --> 
                                    <div class="widget-extra">
                                      <div ng-class="{row:true,'text-center':true, 'themed-background-dark-autumn': true}">
                                          <div class="col-xs-4">
                                              <h3 class="widget-content-light">
                                                  <strong>@{{template.workout_count}}</strong><br>
                                                  <small>Workouts</small>
                                              </h3>
                                          </div>
                                          <div class="col-xs-4">
                                              <h3 class="widget-content-light">
                                                  <strong>@{{template.video_count}}</strong><br>
                                                  <small>Videos</small>
                                              </h3>
                                          </div>
                                          <div class="col-xs-4">
                                              <h3 class="widget-content-light">
                                                  <strong>@{{template.days}}</strong><br>
                                                  <small>Days</small>
                                              </h3>
                                          </div>
                                      </div>
                                    </div>
                                </div>
                          </div>
                       </div>  
                    </simple-block>
                  </div>
                    
                  <div class="col-md-9 col-xs-12">
                        <simple-block block-title="Patient Workouts" right_title>
                          <div class="container block-content"
                                slim-scroll 
                                dnd-list="schedule.user_schedules"
                                dnd-drop="schedule.dropCallback(event, index, item, external, type, 'itemType')"
                                >
                                  <interactive-block closed 
                                        ng-repeat="workout in schedule.user_schedules"
                                        on-remove="schedule.deleteUserWorkout(workout)"
                                        block-title="schedule.getWorkoutTitle(workout)">
                                          <block-content>
                                            <div class="gallery">
                                                <div ng-if="$index%3==0" ng-repeat="xyz in workout.videos" >                     
                                                    <div  class="row row-eq-height" >
                                                        <div ng-if="($index>=$parent.$index) && ($index<=($parent.$index+2))" 
                                                            ng-repeat="video in workout.videos" 
                                                            class="col-sm-4">
                                                            <single-video  video="video" 
                                                             width="250" 
                                                             height="150" 
                                                             not-editable 
                                                             not-deletable
                                                             remove ="vm.deleteVideo(video, workout.id)"></single-video>
                                                             <br>                              
                                                       </div>
                                                        <br>
                                                    </div>                            
                                                </div>
                                            </div> 

                                         </block-content>
                                  </interactive-block>
             
                          </div>
                        </simple-block>
                  </div>
                </div>
            </div>
        </div>
    </div>    
</div>

