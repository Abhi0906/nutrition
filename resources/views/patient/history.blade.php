<?php $today_date=date('F d,Y');   ?>
<script type="text/ng-template" id="history">
<div class="animation-fadeInQuick" ng-init="diary.userId='{{$user_id}}';diary.diary_from='{{$today_date}}';
    diary.diary_to='{{$today_date}}'; diary.getHealthDiaryData(null, 7, 'seven-days'); diary.checkAllFilter();">
    <h4 class="sub-header">Your Diary for Food & Exercise:</h4>
    
    <div class="row">
        <div class="col-md-5">
          <span style="text-align:right;" class="col-md-2 first_alt">Filter:</span>
          <span class="col-md-10 checkbox">
            <span class="">
              <label for="all_log_filter">
                <input id="all_log_filter" ng-model="diary.all_log_filter" type="checkbox" value="all" name="all_log_filter" ng-click="diary.checkAllFilter()">
              All
              </label>
              &nbsp;
            </span>
            <span class="" ng-repeat="(key, filter) in diary.diaryFilters">
              <label for="@{{filter.id}}">
                <input id="@{{filter.id}}" ng-model="filter.selected" type="checkbox" value="food" name="filter.id" ng-click="diary.singleCheckbox();">
              @{{filter.title}}
              </label>
              &nbsp;
            </span>
          </span>
        </div>
    </div>
    <div class="row">
	    <div class="col-md-5">
	        <span class="col-md-2 first_alt" style="text-align:right;">From:</span><span class="col-md-10"><input-datepicker format="MM dd, yyyy" date="diary.diary_from"></input-datepicker></span>
	    </div>
        <div class="col-md-5">
            <span class="col-md-2 first_alt" style="text-align:right;">To:</span><span class="col-md-10"><input-datepicker format="MM dd, yyyy" date="diary.diary_to"></input-datepicker></span>
        </div>
        <div class="col-md-2">
            <input type="button" class="btn btn-primary" ng-click="diary.getHealthDiaryData(null, null, null)" value="Get"/>
            <input type="button" class="btn btn-success" ng-disabled="diary.userHealthDiaryLength == 0"  ng-click="diary.getHealthDiaryData('download')" value="Print"/>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-12">
          <button type="button" id="seven-days" class="btn btn-xs btn-info days-filter" ng-click="diary.getHealthDiaryData(null, 7, 'seven-days');">Last 7 Days</button>
          <button type="button" id="thirty-days" class="btn btn-xs btn-info days-filter" ng-click="diary.getHealthDiaryData(null, 30, 'thirty-days');">Last 30 Days</button>
          <button type="button" id="ninety-days" class="btn btn-xs btn-info days-filter" ng-click="diary.getHealthDiaryData(null, 90, 'ninety-days');">Last 90 Days</button>
          <button type="button" id="one-eighty-days" class="btn btn-xs btn-info days-filter" ng-click="diary.getHealthDiaryData(null, 180, 'one-eighty-days');">Last 180 Days</button>
          <button type="button" id="year" class="btn btn-xs btn-info days-filter" ng-click="diary.getHealthDiaryData(null, 365, 'year');">Last Year</button>
        </div>
      </div>
    <br/><br/>
    <span ng-show="diary.diary_loader"  class="diary_loader"></span>
    <div ng-repeat="(key, health_diary_data) in diary.userHealthDiary">
        <div class="block full">
            <div class="block-title text-center">
                <h2><strong>@{{diary.getDate(key)}}</strong></h2>
            </div>
            <div id="no-more-tables">
                <table class="table table-condensed">
                    <tr>
                        <td class="first_alt">Food Diary</td>
                        <td class="alt hidden-sm hidden-xs">Calories</td>
                        <td class="alt hidden-sm hidden-xs">Carbs</td>
                        <td class="alt hidden-sm hidden-xs">Fat</td>
                        <td class="alt hidden-sm hidden-xs">Protein</td>
                        <td class="alt hidden-sm hidden-xs">Sodium</td>
                        <td class="alt hidden-sm hidden-xs">Sugar</td>
                    </tr>
                    <tbody ng-repeat="(key, food_diary_data) in health_diary_data" ng-if="key == 'BREAKFAST' || key == 'LUNCH' || key == 'DINNER' || key == 'SNACK' ">
                        <tr>
                            <td class="text-warning" colspan="7"><b>@{{key}}</b></td>
                           
                        </tr>
                        <tr ng-repeat="data in food_diary_data">
                            <td data-title="Name">@{{data.nutritions.name}}</br>
                               <small> @{{data.nutritions.nutrition_brand.name}}, (@{{data.nutritions.serving_quantity}} @{{data.nutritions.serving_size_unit}}) </small>
                            </td>
                            <td class="text-center" data-title="Calories">@{{data.calories | number:0}}</td>
                            <td class="text-center"  data-title="Carbs">@{{data.total_carb | number:0 }}</td>
                            <td class="text-center"  data-title="Fat">@{{data.total_fat | number:0 }}</td>
                            <td class="text-center"  data-title="Protein">@{{data.protein | number:0 }}</td>
                            <td class="text-center"  data-title="Sodium">@{{data.sodium | number:0 }}</td>
                            <td class="text-center"  data-title="Sugar">@{{data.sugars | number:0 }}</td>
                        </tr>
                    </tbody>
                    <tr class="total">
                            <td width="40%" data-title="Total">Total</td>
                            <td width="10%" data-title="Calories" class="text-center">@{{health_diary_data.food_total.food_total_calories | number:0}}</td>
                            <td width="10%" data-title="Carbs" class="text-center">@{{health_diary_data.food_total.food_total_carb | number:0 }}</td>
                            <td width="10%" data-title="Fat" class="text-center">@{{health_diary_data.food_total.food_total_fat | number:0 }}</td>
                            <td width="10%" data-title="Protein" class="text-center">@{{health_diary_data.food_total.food_total_protein | number:0 }}</td>
                            <td width="10%" data-title="Sodium" class="text-center">@{{health_diary_data.food_total.food_total_sodium | number:0 }}</td>
                            <td width="10%" data-title="Sugar" class="text-center">@{{health_diary_data.food_total.food_total_sugars | number:0 }}</td>
                        </tr>
                </table>
            </div>
            <div id="no-more-tables">
                <table class="table table-condensed">
                    <tr>
                        <td class="first_alt">Exercise Diary</td>
                        <td class="alt hidden-sm hidden-xs">Time in minutes</td>
                        <td class="alt hidden-sm hidden-xs">Calories burned</td>
                    </tr>
                    <tbody ng-repeat="(key, exercise_diary_data) in health_diary_data" ng-if="key == 'CARDIO' || key == 'STRENGTH' ">
                    <tr>
                        <td class="text-warning" colspan="7"><b>@{{key}}</b></td>
                       
                    </tr>
                    <tr ng-repeat="data in exercise_diary_data">
                        <td>@{{data.exercise.name}}</td>
                            <td class="text-center"  data-title="Time in minutes">@{{data.time}}</td>
                            <td class="text-center"  data-title="Calories burned">@{{data.calories_burned | number:0}}</td>
                    </tr>
                    </tbody>
                    <tr class="total">
                            <td width="40%">Total</td>
                            <td width="30%" data-title="Time in minutes" class="text-center">@{{health_diary_data.exercise_total.total_time}}</td>
                            <td width="30%" data-title="Calories burned" class="text-center">@{{health_diary_data.exercise_total.total_calories_burned | number:0}}</td>
                        </tr>
                </table>
            </div>
            <div id="no-more-tables">
                  <table class="table table-condensed" width="100%">
                      <tbody><tr>
                          <td width="40%" class="first_alt">Weight</td>
                          <td width="20%" class="alt hidden-sm hidden-xs">Current Weight</td>
                          <td width="20%" class="alt hidden-sm hidden-xs">Body Fat</td>
                          <td width="20%" class="alt hidden-sm hidden-xs">Source</td>
                      </tr>
                      </tbody>
                      <tbody>
                        
                        <tr class="total">
                            <td width="40%"></td>
                            <td width="20%" data-title="Current Weight" class="text-center">@{{health_diary_data.weight_data.current_weight}} lbs</td>
                            <td width="20%" data-title="Body Fat" class="text-center">@{{health_diary_data.weight_data.body_fat}} %</td>
                            <td width="20%" data-title="Source" class="text-center">@{{health_diary_data.weight_data.source}}</td>
                        </tr>
                      </tbody>
                  </table>
            </div>
        </div>  
    </div> 

    <div ng-cloak  class="alert alert-info animation-fadeInQuick"  ng-show="diary.userHealthDiary.length <= 0">
        <button data-dismiss="alert" class="close" type="button">
          <i class="ace-icon fa fa-times"></i>
        </button>

        <strong>
          Health Diary information not available!
        </strong>
   </div>
</div>
</script>