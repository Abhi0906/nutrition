<div id="log-weight" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"   data-backdrop="static" data-keyboard="false">
   <div class="container-fluid modal-dialog" style="width:45%;">
      <div class="modal-content">
         <div class="modal-header text-center">
            <button type="button" class="close" data-dismiss="modal" >
            <span aria-hidden="true" >&times;</span>
            <span class="sr-only" >Close</span>
            </button>
            <h5 class="modal-title"><strong>Log Weight</strong></h5>
         </div>
         <div class="modal-body">
            {!! Form::open(array('onsubmit' => 'return false;','method' => 'post','class' => 'form-horizontal animation-fadeInQuick','id' => 'LogWeightForm','name' => 'LogWeightForm')) !!}
            <div class="form-group">
               <label class="col-sm-2 control-label" class="">Weight</label>
               <div class="col-md-3">
                  <div class="input-group">
                     <input type="text"  ng-change="profile_goals.calculateWater()" class="form-control input-xs" 
                        numbers-only="numbers-only"  id="current_weight"
                        name="current_weight" ng-model="profile_goals.current_weight"  required>
                     <span class="input-group-addon btn-primary">lbs</span>
                  </div>
               </div>
               <label class="col-sm-1 control-label">On</label>
               <div class="col-sm-4">
                <input  name="log_date" id="log_date" class="form-control input-datepicker" value="<?php echo date('F d, Y'); ?>" data-date-format="MM dd, yyyy" placeholder="MM DD, YYYY">
               </div>
               <div class="col-sm-2">
                  <button type="button" class="btn btn-primary" ng-disabled="LogWeightForm.$invalid"  ng-click="profile_goals.log_weight();">Log</button>
               </div>
            </div>
           <h5><strong>Measurement History</strong></h5>
            <table class="table table-striped">
                  <tbody> 
                          <tr>
                              <td class="hidden-xs text-center"><strong>Day</strong></td>
                              <td class="hidden-xs text-center"><strong>Weight</strong></td>
                          </tr>
                          <tr ng-repeat="weight in profile_goals.getlogweight">
                              <td data-title="Day" format="dd MM, yyyy" class="text-center">@{{weight.log_date | date:'MMMM dd, y'}}</td>
                              <td data-title="Weight" class="text-center">@{{weight.current_weight}}</td>
                        </tr>
                  </tbody>
            </table>
            {!! Form::close() !!}
         </div>
      </div>
   </div>
</div>
{!! HTML::script("js/directives/inputDatepicker.js") !!}