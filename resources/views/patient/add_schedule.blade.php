<div id="user_add_schedule" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="container-fluid modal-dialog" style="width:auto !important;">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal" >
                    <span aria-hidden="true" >&times;</span>
                    <span class="sr-only" >Close</span>
                </button>
                <h4 class="modal-title"><i class="fa fa-pencil"></i>&nbsp; <strong>Edit Patient Schedule</strong></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-2 hidden-xs">
                        <search-video title="FIND WORKOUTS" videos="listing.videos"></search-video>
                    </div>
                    <div class="col-md-3 hidden-xs">
                        <simple-block block-title="Result">
                            <div class="gallery container"  slim-scroll >
                                <div class="row">                                   
                                        <single-video class="col-xs-12"  video="video" width="220" height="140" 
                                            ng-repeat="video in listing.videos">                            
                                         </single-video>                                 
                                    
                                </div>
                            </div>
                        </simple-block>
                    </div>
                    <div class="col-md-7 col-xs-12">
                        <simple-block block-title="Schedule">
                          <div class="container block-content" slim-scroll>
                             <interactive-block transparent title="schedule.getTitle()">
                                <block-content>
                                    <div class="gallery"  >
                                        <div ng-if="$index%3==0" ng-repeat="xyz in listing.videos" >                     
                                            <div  class="row row-eq-height" >
                                                <single-video ng-if="($index>=$parent.$index) && ($index<=($parent.$index+2))" 
                                                class="col-sm-4" ng-repeat="video in listing.videos" video="video" width="220" height="140" >                            
                                                </single-video>
                                            </div>
                                            <br>
                                        </div>                                 
                                       
                                    </div>
                                </block-content>    
                            </interactive-block>
                          </div>
                        </simple-block>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>

