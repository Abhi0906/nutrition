<script type="text/ng-template" id="medication">
<!-- Typeahead Block -->
<div class="row">
  <div class="col-xs-12">   
    <!-- Typeahead Block -->
    <div class="row">
  		<!-- Typeahead Content -->
        <div class="form-group">
          <div class="col-xs-12 col-md-12"  id="prefetch">
              <input-typeahead  placeholder="Select your medicine" new_medicine="medication.new_medicine" clear-typeahead="true"></input-typeahead>
          </div>
        </div>
  	  <!-- END Typeahead Content -->
    </div>
    <div ng-if="medication.new_medicine">
      {!! Form::open(array('onsubmit' => 'return false;','method' => 'post','class' => 'form-horizontal','id' => 'medicine_id')) !!}
      <div class="row">
        <div class="col-xs-12">
          <simple-block block-title="@{{ medication.new_medicine.generic_name ? medication.new_medicine.name +' &nbsp;[' + medication.new_medicine.generic_name +']'  : medication.new_medicine.name }}">
            <div class="row" style="padding-left:20px;margin-top:-10px;"  >
              <h1><i class="text-muted wellness-icon-rx pull-left">&nbsp;&nbsp;</i><h4 style="margin-top:18px" class="text-warning pull-left">
                  @{{medication.new_medicine.display_medicine_direction}}
              </h4></h1>
            </div>
            <div class="row text-center" >
                <div class="col-xs-6 col-lg-3">
                    <h3>
                        <strong>
                              <input type="text" ng-blur="medication.calculateDays()" ng-model ="medication.new_medicine.config.dispense" id="dispense" name="dispense" class="form-control input-lg" placeholder="" style="margin-left: 60px; max-width: 35%;font-size: 24px;">
                        </strong> <small></small>
                        <small><i class="wellness-icon-medicine"></i> Dispense</small>
                    </h3>
                </div>
                <div class="col-xs-6 col-lg-3">
                    <h3>
                        <strong> 
                            <input type="text" ng-blur="medication.calculateDays()" ng-model ="medication.new_medicine.config.refills" id="refills" name="refills" class="form-control input-lg" placeholder="" style="margin-left: 60px; max-width: 35%;font-size: 24px;">
                        </strong> <small></small>
                        <small><i class="wellness-icon-capsule"></i> Refills</small>
                    </h3>
                </div>
                <div class="col-xs-6 col-lg-3">
                    <h3>
                        <strong> 
                              <input type="text" ng-model ="medication.new_medicine.days" id="days" name="days" class="form-control input-lg" placeholder="" style="margin-left: 60px; max-width: 35%;font-size: 24px;">
                        </strong> <small></small>
                        <small><i class="fa fa-calendar"></i> Days</small>
                    </h3>
                </div>
            </div>

            <div class="row">
              <div class="col-xs-6 col-lg-6">
                <div class="col-xs-6">         
                  <h3><input-datepicker format="MM dd, yyyy" date="medication.new_medicine.start"></input-datepicker>
                  <small class="text-center"><i class="fa fa-calendar"></i> Start</small></h3>
                </div>
                <div class="col-xs-6">                    
                   <h3><input type="text" class="form-control" disabled="" ng-model="medication.new_medicine.end">
                  <small class="text-center"><i class="fa fa-calendar"></i> To </small></h3>
                </div>
              </div>
              <div class="col-xs-6 col-lg-6">
                  <h3><select 
                    chosen="medication.routine_times"
                    data-placeholder="Choose a Time"
                     max_selected_options = "@{{medication.new_medicine.frequency_times}}"
                    ng-model="medication.new_medicine.time" multiple>
                    <option ng-repeat="routine_time in medication.routine_times" value="@{{routine_time.display_name}}">@{{routine_time.display_name}}</option>
                  </select>
                  <small class="text-center"><i class="fa fa-clock-o"></i> Time</small></h3>
              </div>
            </div>
            <br>
            <div class="form-group form-actions">
              <div class="col-md-12">
                <button type="button" class="btn btn-sm btn-warning btn-alt pull-left" ng-click="medication.new_medicine=null">Cancel</button>
                <button type="button" class="btn btn-sm btn-primary btn-alt pull-right" ng-click="medication.saveMedicine()">Save</button>
              </div>
            </div>
          </simple-block>
        </div>
      </div>
      {!! Form::close() !!}
    </div>
    
    <div class="row">
      <div class="col-xs-12" ng-repeat="medicine in medication.user_medicine_list">
        <interactive-block block-title="medication.getMedicationFullHtmlName(medicine)" transparent on-remove="medication.deleteMedicine(medicine)">
            <block-content  > 
              <div class="well well-sm" style="margin-top:-10px;">
                  <div class="row" style="padding-left:20px;margin-top:-10px;"  >
                    <h1><i class="text-muted wellness-icon-rx pull-left">&nbsp;&nbsp;</i><h4 style="margin-top:18px" class="text-warning pull-left">
                        @{{medicine.display_medicine_direction}}
                    </h4></h1>
                  </div>
                  <div class="row text-center" >
                      <div class="col-xs-4 col-md-4 col-lg-2">
                          <h3>
                              <strong>@{{medicine.config.dispense}}</strong> <small></small><br>
                              <small class="text-primary"><i class="wellness-icon-medicine"></i> Dispense</small>
                          </h3>
                      </div>
                      <div class="col-xs-4 col-md-4 col-lg-2">
                          <h3>
                              <strong>@{{medicine.config.refills}}</strong> <small></small><br>
                              <small class="text-primary"><i class="wellness-icon-capsule"></i> Refills</small>
                          </h3>
                      </div>
                      <div class="col-xs-4 col-md-4 col-lg-2">
                          <h3>
                              <strong>@{{medicine.days}}</strong> <small></small><br>
                              <small class="text-primary"><i class="fa fa-calendar"></i> Days</small>
                          </h3>
                      </div>
                      <div class="col-xs-12 col-md-12 col-lg-6">
                          <div class="row">
                            <div class="col-xs-6">
                              <h3>
                                <strong>@{{medicine.start | date : 'MMMM dd, yyyy' }}</strong> <br>
                                <small class="text-primary"><i class="fa fa-calendar"></i> From</small>
                              </h3>
                            </div>
                            <div class="col-xs-6">
                              <h3>
                                <strong>@{{medicine.end | date : 'MMMM dd, yyyy' }}</strong> <br>
                                <small class="text-primary"><i class="fa fa-calendar"></i> To</small>
                              </h3>
                            </div>
                          </div>
                          
                      </div>
                  </div>
                  <div class="row" style="padding-left:20px;">
                    <h5><strong><label class="label label-primary label-as-badge" ng-repeat="time in medicine.time" style="margin-left:7px;"> @{{time}}</label></strong></h5>
                  </div>
                  
              </div>             
            </block-content>                
        </interactive-block>
      </div>
    </div>
    <!-- END Typeahead Block -->
  </div>
</div>
<!-- END Typeahead Block -->
</script>
