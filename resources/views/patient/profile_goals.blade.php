<script type="text/ng-template" id="profile_goals">

<div class="row">
   <div class="col-xs-12">
      <div class="block">
         <div class="block-title ">

            <h2><strong>Profile</strong></h2>
         </div>
         {!! Form::open(array('onsubmit' => 'return false;','method' => 'post','class' => 'form-horizontal animation-fadeInQuick','id' => 'profile_goals_id', 'name' => 'profileForm')) !!}
         
         <div class="form-group">
            <label class="col-xs-12 col-sm-3  col-md-2 col-lg-2 control-label text-left_align" for="age">Age</label>
            <div class="col-xs-12 col-sm-9 col-md-10 col-lg-10">
               <label class="control-label text-primary" for="age"> &nbsp;@{{profile_goals.age }}&nbsp; </label>&nbsp;&nbsp; <span class="text-primary">years</span>
            </div>
         </div>
         <div class="form-group">
            <label class="col-xs-12 col-sm-3 col-md-2 col-lg-2 control-label text-left_align" for="gender">Gender</label>
            <div class="col-xs-12 col-sm-9 col-md-10 col-lg-10">
               <label class="control-label text-primary" for="gender"><strong>@{{ profile_goals.gender }}</strong></label>
            </div>
         </div>
      
         <div class="form-group">
            <label class="col-xs-12 col-sm-3 col-md-2 col-lg-2 control-label text-left_align" for="program_start">Program Start</label>
            <div class="col-xs-12 col-sm-9 col-md-10 col-lg-10">
               <div class="input-group">
                  <input-datepicker format="MM dd, yyyy" date="profile_goals.profile.program_start"></input-datepicker>
                  </div>
            </div>
         </div>

         <div class="form-group" >
            <label class="col-xs-12 col-sm-3 col-md-2 col-lg-2 control-label text-left_align" for="Weight">Last Weight</label>
            <div class="col-xs-12 col-sm-9 col-md-10 col-lg-10">
               <label class="control-label text-primary" for="Last Weight"><strong>@{{profile_goals.user_weight.current_weight}}</strong></label>
                     &nbsp;<span class="text-primary"><b>lbs</span>&nbsp;&nbsp;&nbsp;&nbsp;
                     <strong>Body Fat </strong> &nbsp;&nbsp;&nbsp;&nbsp; 
                     <span class="text-primary">@{{ profile_goals.user_weight.body_fat }} %</span>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;</span>
                     <strong>Last Updated </strong> 
                     <span class="text-primary">&nbsp;&nbsp;&nbsp;&nbsp; @{{ profile_goals.user_weight.created_at}}</span>

             <!--    <a href="javascript:void(0)" style="cursor:pointer; text-decoration: none;"  ng-click="profile_goals.editprofile()" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Add Weight</a>  -->
                     
            </div>
         </div>
         <div class="form-group" >
            <label class="col-xs-12 col-sm-3 col-md-2 col-lg-2 control-label text-left_align" for="height">Height</label>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
               <div class="input-group">
                  <input type="text"
                     id="feet_height"    
                     name="height_in_feet"
                     placeholder="Enter Height in feet"
                     class="form-control"
                     maxlength="5"
                     ng-model="profile_goals.profile.height_in_feet"
                      numbers-only="numbers-only" ng-change="profile_goals.calculateCalories()">
                  <span class="input-group-btn">
                  <span class="btn btn-primary">&nbsp;&nbsp;Feet&nbsp;&nbsp;</span>
                  </span>
               </div>
               <small>Enter numbers only</small> 
            </div>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
               <div class="input-group">
                  <input type="text"
                     id="inches_height"
                     name="height_in_inch"
                     placeholder="Enter Height in inches"
                     class="form-control"
                     maxlength="5"
                     ng-model="profile_goals.profile.height_in_inch"
                     ng-change="profile_goals.calculateCalories()"
                      numbers-only="numbers-only">
                  <span class="input-group-btn">
                  <span class="btn btn-primary">Inches</span>
                  </span>
               </div>
               <small>Enter numbers only</small>
            </div>
         </div>

         {!! Form::close() !!}
         <!-- END Horizontal Form Content -->
      </div>
      <div class="block">
         <!-- Horizontal Form Title -->
         <div class="block-title">
            <h2><strong>Goals</strong></h2>
         </div>
         <!-- END Horizontal Form Title -->
         <!-- Horizontal Form Content -->
         {!! Form::open(array('onsubmit' => 'return false;','method' => 'post','class' => 'form-horizontal animation-fadeInQuick','id' => 'profile_goals_id')) !!}
         <h4><strong>Weight</strong></h4>
         <hr>

         <div class="form-group">
         {!! HTML::decode(Form::label('goal weight', 'Goal Weight<sup class="text-danger"></sup>', array('class'=>'col-md-2'))) !!}            
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
               <div class="input-group">
                  <input type="text"
                     maxlength="5"
                     numbers-only="numbers-only" 
                     ng-model="profile_goals.user_goal.goal_weight" 
                                       placeholder="Goal Weight"
                      ng-change="profile_goals.changeGoalWeight();"
                     class="form-control" required>
                  <span class="input-group-btn">
                  <span class="btn btn-primary">&nbsp;&nbsp;lbs&nbsp;&nbsp;</span>
                  </span>
               </div>
               <small>Enter numbers only</small>
            </div>
         </div>
         <div class="form-group">
            {!! HTML::decode(Form::label('weekly_goal', 'Weekly Goal<sup class="text-danger"></sup>', array('class'=>'col-md-2'))) !!}            
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
               <select  ng-model="profile_goals.user_goal.weekly_goal_id"
                  ng-options="weeklygoal.id as weeklygoal.name for weeklygoal in profile_goals.weeklygoals" 
                  class="form-control" ng-change="profile_goals.changeCalories();"   required >
                  <option value="">Select weekly goal</option>
               </select>
             </div>
         </div>
         <div class="form-group">
            {!! HTML::decode(Form::label('calories', 'Calories<sup class="text-danger"></sup>', array('class'=>'col-md-2'))) !!}            
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
               <input type="text" numbers-only="numbers-only" maxlength="20"  ng-model="profile_goals.profile.calories"  class="form-control"  name="calories"  id="calories"  maxlength="50" placeholder="Calories" required>
            </div>
         </div>
         <h4><strong> Water </strong></h4>
         <hr>
         <div class="form-group">
            {!! HTML::decode(Form::label('water_in_take', 'Water Intake<sup class="text-danger"></sup>', array('class'=>'col-md-2'))) !!}                        
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
               <div class="input-group">
                  <input type="text"
                     id="water_intake"
                     name="water_intake"
                     maxlength="20"
                     class="form-control"
                     placeholder="Enter amount in US Oz."
                     ng-model="profile_goals.profile.water_intake" >
                  <span class="input-group-btn">
                  <span class="btn btn-primary">&nbsp;&nbsp;&nbsp;US Oz.&nbsp;&nbsp;&nbsp;</span>
                  </span>
               </div>
            </div>
         </div>
         <hr>
         <div class="form-group form-actions">
            <div class="col-xs-7 col-xs-offset-4 col-md-7 col-md-offset-5">
               <button type="button" class="btn btn-sm btn-primary" ng-click="profile_goals.saveProfile()">Save</button>&nbsp;&nbsp;&nbsp;&nbsp;
               <button type="button" class="btn btn-sm btn-warning" ng-click="profile_goals.reset()">Reset</button>
            </div>
         </div>
         {!! Form::close() !!}
         <!-- END Horizontal Form Content -->
      </div>
   </div>
</div>
<div id="same_day" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
         <!-- Modal Header -->
         <div class="modal-header  has-warning text-center">
           <div class="alert alert-warning">
               <strong>Warning!</strong>
           </div>
              <div class="modal-body">
                  <p><strong>Today's weight is already logged, Do you want to replace? </strong></p>
              </div>
               <div class="form-group form-actions">
               <div class="col-xs-13">
                  <button class="btn btn-sm btn-primary" ng-click="profile_goals.saveWeight()" type="submit">Yes</button>
                  <button data-dismiss="modal" class="btn btn-sm btn-default" type="button">No</button>
               </div>
            </div>
          </div>
        </div>
      </div>
</div>
<div id="keep_current" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header  has-warning text-center">
                 <div class="alert alert-warning">
                     <strong>Warning!</strong>
                 </div>
                 <div class="modal-body">
                 <p><strong>Current daily calories: &nbsp;&nbsp;
                     <label class="text-primary"><strong>@{{ profile_goals.profile.calories }}</strong></label>
                     <br>
                     New recommended daily calories:  
                     <label class="text-primary"><strong>@{{ profile_goals.new_calories }}</strong></label>
                     <br>
                     Current daily water intake: &nbsp;&nbsp;
                     <label class="text-primary"><strong>@{{ profile_goals.profile.water_intake }}</strong></label> <br>
                     New recommended daily water intake:     
                     <label class="text-primary"><strong>@{{ profile_goals.new_water_intake}}</strong></label> <br>
                     Do you want to update the the calories and water intake, or leave them as they are? 
                    <br></strong>
                  </p>
                 </div>
                  <div class="form-group form-actions">
                  <div class="col-xs-13">
                     <button data-dismiss="modal" class="btn btn-sm btn-primary" type="button">Keep current</button>&nbsp;&nbsp;&nbsp;&nbsp;
                     <button data-dismiss="modal" class="btn btn-sm btn-primary" ng-click="profile_goals.updateCaloriesAndWater();">Update</button>
                  </div>
                  </div>
            </div>
         </div>
      </div>
</div>
<div id="weekly_goal_warning" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
         <!-- Modal Header -->
         <div class="modal-header  has-warning text-center">
           <div class="alert alert-warning">
               <strong>Warning!</strong>
           </div>
              <div class="modal-body">
                  <p><strong>@{{profile_goals}}</strong></p>
              </div>
               <div class="form-group form-actions">
               <div class="col-xs-13">
                  <button class="btn btn-sm btn-primary" ng-click="$('#weekly_goal_warning').modal('hide')">OK</button>
               </div>
            </div>
          </div>
        </div>
      </div>
</div>

</script>

@section('jquery_ready')

@append       