<script type="text/ng-template" id="schedules">
    <div ng-repeat="user_schedule in schedule.user_schedules" class="animation-fadeInQuick">  
        <interactive-block  
            block-title="schedule.getWorkoutTitle(user_schedule)"
            on-remove="schedule.deleteUserWorkout(user_schedule)"
            closed >
            <block-content>
                <div class="gallery">
                    <div ng-if="$index%3==0" ng-repeat="xyz in user_schedule.videos" >                     
                        <div  class="row row-eq-height" >
                            <div ng-if="($index>=$parent.$index) && ($index<=($parent.$index+2))" 
                                ng-repeat="video in user_schedule.videos" 
                                class="col-sm-4">
                                <single-video  video="video" 
                                 width="250" 
                                 height="150" 
                                 not-editable
                                 not-deletable 
                                 remove ="vm.deleteVideo(video, workout.id)"></single-video>
                                 <br>                              
                           </div>
                            <br>
                        </div>                            
                    </div>
                </div> 
            </block-content>
        </interactive-block> 
    </div>
 @include('patient.edit_schedules')           
</script>
@section('jquery_ready')

@append
@section('inline_scripts')

@append


