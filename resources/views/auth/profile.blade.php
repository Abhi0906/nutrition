<div id="mainpanel">
 <div id="innerpage" class="login-healthapp" style="background-image:url(images/Login-app-banner.jpg)">
    <div class="container-fluid">
      <div class="row">
         <div class="col-xs-12 col-lg-offset-8 col-lg-4">
                  <div class="innerpage">
                      <div ng-show="profile.profileLoader" class="profile_loader"></div>

                      <div class="innerpage">
                        <div class="top">
                          <img src="images/genemedics-nutrition.png" alt="logo">
                        </div>
                        <div class="middle">

                          <h4>Patient Profile</h4>
                                                    
                            
                            <form  name="profileForm" class="form-login" ng-submit="profile.updateProfile()" novalidate>
                             
                             
                              <div class="form-group">  
                              
                                   <i class="fa fa-user"></i>
                                   <input type="text" maxlength="25" class="text-box" name="first_name"  placeholder="Firstname" ng-model="profile.about_data.first_name" required>
                                  
                                   <p class="text-danger text-center" ng-show="profileForm.first_name.$invalid && profileForm.first_name.$touched">
                                          <small>You must fill out your first name.</small>
                                   </p>
                              </div>
                      
                              <div class="form-group">  
                                      <i class="fa fa-user"></i>
                                      <input type="text" maxlength="25"   class="text-box" name="last_name"  placeholder="Lastname" ng-model="profile.about_data.last_name" required>
                                  
                                  <p class="text-danger text-center" ng-show="profileForm.last_name.$invalid && profileForm.last_name.$touched">
                                          <small>You must fill out your last name.</small>
                                 </p>
                              </div>
                           
                              <div class="form-group">  
                                
                                    <i class="fa fa-gift"></i> 
                                    <input type="text" id="birth_date" class="text-box input-datepicker" value="<?php echo date('F d, Y'); ?>" data-date-format="MM dd, yyyy" placeholder="MM DD, YYYY">
                                
                                   <p class="text-danger text-center" ng-show="profile.ageValidation">
                                      <small>You must be at least 13 years of age to register</small>
                                   </p>
                              </div>
                              

                                
                              <div class="form-group " ng-init="profile.about_data.gender='male'">

                                     <input type="radio" id="male" ng-model="profile.about_data.gender" value="male" name="gender"  required>
                                      &nbsp;
                                    <label for="male" class="malelbl">Male</label>

                                    <span class="maleback backcolor malefemale" >
                                      <input type="radio" id="female" ng-model="profile.about_data.gender" value="female" name="gender" required>
                                      &nbsp;
                                      <label for="female" class="malelbl">Female</label>
                                    </span>
                                  
                                        
                              </div>
                              
                                <div class="col-xs-6 form-group">  
                                  <div class="input-group">
                                    <i class="fa fa-male"></i> 
                                    <input type="text" class="" maxlength="5"   ng-model="profile.about_data.height_in_feet" required >
                                  <span class="input-group-addon">Feet</span>
                                  </div>
                                </div>
                              
                 
                              <div class="col-xs-6  feetbox topbotspace">
                                <div class="form-group">  
                                <div class="input-group">
                                    <i class="fa fa-male"></i>
                                    <input type="text"  maxlength="5" class="text-box" ng-model="profile.about_data.height_in_inch" required>
                                  <span class="input-group-addon">Inch</span>
                                  </div>
                                </div>
                              </div>
                            
                              <div class="col-xs-12 topbotspace"> 
                                <div class="form-group"> 
                                  <div class="input-group">
                                      <i class="fa fa-gift"></i> 
                                      <input type="text" class="text-box" maxlength="5" placeholder="Weight" ng-model="profile.about_data.baseline_weight" required>
                                      <span class="input-group-addon">lbs</span>

                                  </div>  
                                  </div>
                              </div>

                              <div class="">
                                  
                                <input type="submit" class="btn" value="Continue" ng-disabled="profileForm.$invalid">
                              </div>
                            </form>

                        </div>  
                      </div>  
                  </div>
         </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  
$('.input-datepicker').datepicker({autoclose:true
        });

</script>