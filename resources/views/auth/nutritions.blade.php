@extends('layout.front')
@section('content')
<?php
$email = $customer_data['email'];
$firstname = $customer_data['firstname'];
$lastname = $customer_data['lastname'];
?>
<div class="row" ng-controller="AuthController as auth"
 ng-init="auth.user.email='{{$email}}';
 		  auth.user.first_name='{{$firstname}}';
 		  auth.user.last_name='{{$lastname}}';	
            auth.displaySignUp();
            ">
	<div class="col-xs-12" ng-show="auth.loadLoginPage">
		@include('auth.authlogin')
	</div>
	<div class="col-xs-12" ng-show="auth.loadSignUpPage">
		@include('auth.authsignup')
	</div>
	<div class="col-xs-12" ng-show="auth.loadResetPasswordPage">
		@include('auth.authreset')
	</div>
</div>

@endsection