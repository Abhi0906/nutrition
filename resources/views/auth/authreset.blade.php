<div id="mainpanel3">
  
  <div id="innerpage" class="login-healthapp">
    <div class="container-fluid">
      <div class="row">
         <div class="col-xs-12 col-lg-offset-4 col-lg-4">
               <div class="innerpage">
                  <div class="innerpage">
                        <div class="top">
                          @if(Session::has('organization_id') )
                                 <img src="/organization_logo/{{$organization['logo']}}" alt="mobile-logo" style="height:35px">
                         @else
                            <img src="/images/genemedics-nutrition.png" alt="logo">

                         @endif
                        </div>
                        <div class="middle">

                          <h4>Reset Password</h4>
                          <span ng-show="auth.loader" class="loader"></span>
                          
                          <p ng-show="auth.sentLinkSuccess" class="text-success text-center">@{{auth.sentLinkSuccessMessage}}</p>
                        
                          <form name="resetForm" class="form-login" ng-submit="auth.sendResetLink($event)" novalidate>
                             
                            <div class="form-group">  

                                   <input type="email" class="text-box form-control indexset noborderleft" maxlength="30"  name="email"  show-focus="auth.loadResetPasswordPage"   placeholder="Email" ng-model="auth.email" required> 
                                    <i class="fa fa-envelope-o"></i>

                                    <p class="text-danger text-center text-error" ng-show="resetForm.email.$invalid && resetForm.email.$touched">
                                        <small>You must fill out your valid email.</small>
                                    </p>
                                    <p class="text-danger text-center text-error" ng-show="auth.sentLinkValidation">
                                        <small>@{{auth.sentLinkErrorMessage}}</small>
                                    </p>
                            </div>

                            <div class="form-group">
                                  <input type="submit" class="btn btndesign1" value="Send Reset Link" ng-disabled="resetForm.$invalid">
                              
                            </div>
                            <p>Do you have an account? <a href="javascript:void(0);" ng-click="auth.displayLoginPage();">Login</a></p>  
                          </form>
                        </div>
                  </div>
           </div>
      </div>
    </div>
  </div>
</div>