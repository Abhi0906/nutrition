

<div id="mainpanel2">
  <!-- IF PAGE LIG IN / SIGN UP -->
  <div id="innerpage" class="login-healthapp">
    <div class="container-fluid">

      <div class="row">
         <div class="col-xs-12 col-lg-offset-4 col-lg-4">
                  <div class="innerpage">
                     <span ng-show="auth.loader" class="loader"></span>
                      <div class="innerpage">
                        <div class="top">
                          @if(Session::has('organization_id') )
                                 <img src="/organization_logo/{{$organization['logo']}}" alt="mobile-logo" style="height:35px">
                         @else
                            <img src="/images/genemedics-nutrition.png" alt="logo">

                         @endif
                        </div>
                        <div class="middle">

                          <h4>Sign Up</h4>
                            <div ng-show="auth.signupLoader" class="signup_loader"></div>
                            <form name="signupForm" class="form-login" ng-submit="auth.registerUser($event)" novalidate>
                               
                                <div class="form-group">
                                  <input type="email"  maxlength="50"
                                     name="email" show-message-on-tab
                                     show-focus="auth.loadSignUpPage"
                                     class="email text-box" placeholder="Email" ng-model="auth.user.email" required>
                                  <i class="fa fa-envelope-o"></i>
                                  <p class="text-danger text-center text-error" ng-show="signupForm.email.$invalid && signupForm.email.$dirty">
                                      <small>You must fill out your valid email.</small>
                                  </p>
                                  <p class="text-danger text-center text-error" ng-show="auth.authSignupError">
                                      <small>The email has already been taken.</small>
                                  </p>
                                </div>
                                <div class="form-group">
                                  <input type="password" id="passwordId" maxlength="30"  name="password" class="password text-box" placeholder="Password" ng-model="auth.user.password" ng-minlength="6" required>
                                  <i class="fa fa-asterisk"></i>
                                  <p class="text-danger text-center text-error" ng-show="signupForm.password.$invalid && signupForm.password.$touched">
                                          <small>Password Should Atleast 6 Characters.</small>
                                  </p> 
                                </div>
                                <div class="form-group">
                                  <input type="password"
                                   maxlength="30"
                                     name="password_confirmation"
                                      class="password text-box"
                                       placeholder="Confirm Password"
                                        ng-model="auth.user.password_confirmation"
                                         ng-minlength="6"
                                          required
                                           pw-check='passwordId'
                                          >
                                  <i class="fa fa-asterisk"></i>
                                 
                                    <p class="text-danger text-center text-error" 
                                    ng-show="signupForm.password_confirmation.$touched && signupForm.password_confirmation.$error.pwmatch && signupForm.password_confirmation.$dirty">
                                      <small>Password does not match</small>
                                  </p> 

                                </div>
                                 <div class="form-group">  
                              
                                   <i class="fa fa-user"></i>
                                   <input type="text" maxlength="25" class="text-box" name="first_name"  placeholder="Firstname" ng-model="auth.user.first_name" required>
                                  
                                   <p class="text-danger text-center text-error" ng-show="signupForm.first_name.$invalid && signupForm.first_name.$touched">
                                          <small>You must fill out your first name.</small>
                                    </p>
                                </div>
                      
                                <div class="form-group">  
                                        <i class="fa fa-user"></i>
                                        <input type="text" maxlength="25"   class="text-box" name="last_name"  placeholder="Lastname" ng-model="auth.user.last_name" required>
                                    
                                    <p class="text-danger text-center text-error" ng-show="signupForm.last_name.$invalid && signupForm.last_name.$touched">
                                            <small>You must fill out your last name.</small>
                                   </p>
                                </div>
                                
                                <div class="form-group">
                                    <span class="checkboxthem">
                                       <input type="checkbox" ng-model="auth.user.tos" name="tos" required>
                                    </span> 
                                     
                                    <small>I agree to Genemedics <a href="/terms_and_privacy#tou">Terms of Service</a> and <a href="/terms_and_privacy#privacy"  target="_blank">Privacy Policy</a>
                                    </small>
                                </div>

                                <div class="form-group">
                                    <span class="checkboxthem" ng-init="auth.user.news_letter=true">
                                       <input type="checkbox" ng-model="auth.user.news_letter"></span>
                                    </span>
                                    <small>Yes, send me the Genemedics Newsletter
                                      </small>
                                </div>


                                <div class="form-group">
                                      <input type="submit" class="btn btn-primary btndesign1" value="Register" ng-disabled="signupForm.$invalid">
                                </div>
                                <p>Do you have an account? <a href="javascript:void(0);" ng-click="auth.displayLoginPage();">Login</a></p> 
                            </form>

                          </div>
                          <div class="bottom">
                            <!-- <span class="divider"><hr></span> -->
                            
                          </div> 
                        </div>
                      </div>
                  </div>
           </div>
      </div>

    </div>
  </div>

  <script type="text/javascript">
       
  </script>