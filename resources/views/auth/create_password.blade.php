<style>
.text-box{   background-color: #fff;
    border: 1px solid #eceded;
    border-radius: 3px;
    display: block;
    font-size: 12px;
    height: 51px;
    padding: 0 0 0 30px;
    width: 100%;
  }
.text-error{
  color: #d41c20 !important;
}
</style>




@extends('layout.front')
@section('content')
<div class="row" ng-controller="ResetController as createpassword">
  <div class="col-xs-12">
    
   <div id="mainpanel" ng-init="createpassword.user.email='{{$reset_data->email}}';createpassword.user.token='{{$reset_data->token}}'">
  <!-- IF PAGE LIG IN / SIGN UP -->
  <div id="innerpage" class="login-healthapp" style="background-image:url(/images/Login-app-banner.jpg)">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12 col-lg-offset-8 col-lg-4">
          <div class="innerpage">
            <div class="top">
              <img src="/images/genemedics-nutrition.png" alt="logo">
            </div>
            <div class="middle">

              <h4>RESET PASSWORD</h4>
              <p class="text-danger text-center text-error" ng-show="createpassword.resetServerError">@{{createpassword.resetErrorMsg}}</p>
              <div ng-show="createpassword.resetLoader" class="signup_loader"></div>
              <form name="rpForm" class="form-horizontal" ng-submit="createpassword.createPassword($event)" novalidate>

                <div class="form-group">
                  <input type="email" class="email text-box" name="email" placeholder="Email" ng-model="createpassword.user.email" disabled="disabled"> 

                  <i class="fa fa-envelope-o"></i>
                   <p class="text-danger text-center text-error" ng-show="rpForm.email.$invalid && rpForm.email.$touched">
                                <small>You must fill out your valid email.</small>
                   </p>
                  <p class="text-danger text-center" ng-show="createpassword.authSignupError">
                      <small>The email has already been taken.</small>
                  </p>
                </div>
                <div class="form-group">
                  <input type="password" id="passwordId" class="password text-box" name="password" show-message-on-tab  placeholder="New Password" ng-model="createpassword.user.password" ng-minlength="6" required>
                  <i class="fa fa-asterisk"></i>
                 <p class="text-danger text-center text-error" ng-show="rpForm.password.$invalid && rpForm.password.$dirty">
                    <small>Password Should Atleast 6 Characters.</small>
                 </p> 
                </div>

                   <div class="form-group">
                     <input type="password" 
                                      class="password text-box"
                                      name="password_confirmation"
                                      placeholder="Confirm Password"
                                      ng-model="createpassword.user.password_confirmation" 
                                      ng-minlength="6"
                                      required
                                      pw-check='passwordId'>
                  <i class="fa fa-asterisk"></i>
                 <p class="text-danger text-center text-error" ng-show="rpForm.password_confirmation.$touched && rpForm.password_confirmation.$error.pwmatch && rpForm.password_confirmation.$dirty">
                    <small>Password does not match</small>
                 </p> 
                </div>

                <input type="submit" class="btn btndesign1" value="Reset Password" ng-disabled="rpForm.$invalid">
              </form>
             
            </div>
            <div class="bottom">
              <div class="divider"></div>
              <p>technical support</p>
              <a href="tel:800-277-4041"><strong><i class="fa fa-phone"></i>800-277-4041</strong></a>
              <a href="mailto:info@genemedics.com"><strong><i class="fa fa-envelope"></i>info@genemedics.com</strong></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- LOG IN - HEALTH AND WELLNESS APP END -->


  </div>
 
</div>

@endsection
