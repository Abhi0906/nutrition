@extends('layout.front')
@section('content')
<div id="mainpanel">
    <div id="innerpage" class="login-healthapp admin-login-div">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-lg-offset-4 col-lg-4">

                    <div class="innerpage">
                        <div class="top">
                            @if(Session::has('organization_id') )
                                 <img src="/organization_logo/{{$organization['logo']}}" alt="mobile-logo">
                             @else
                                <img src="/images/genemedics-nutrition.png" alt="logo">

                             @endif

                        </div>
                        <div class="middle">

                            <h4>Admin log in</h4>
                            <!-- Login Form -->
                            @if ($errors->has('email'))
                                    <p class="text-danger text-center text-error">
                                        <small>{{ $errors->first('email') }}</small>
                                    </p>
                            @endif

                            @if ($errors->has('password'))
                                    <p class="text-danger text-center text-error">
                                        <small>{{ $errors->first('password') }}</small>
                                    </p>
                            @endif
                            <form name="loginForm" action="/admin/superadminlogin" method="post" id="form-login" class="form-login">
                                {!! csrf_field() !!}
                                <div class="form-group">
                                    <input type="email" name="email" id="admin_email" class="email text-box" placeholder="Email" maxlength="30" required >
                                    <i class="fa fa-envelope-o"></i>


                                </div>
                                <div class="form-group">
                                    <input type="password" id="password" name="password" maxlength="30" class="password text-box" placeholder="Password" required>
                                    <i class="fa fa-asterisk"></i>
                                </div>
                                <div class="form-group form-actions">
                                    <input type="submit" class="btn" value="Login" ng-disabled="loginForm.$invalid">
                                </div>
                            </form>
                         <!--    <div class="links">
                                <a href="{{URL::to('/auth0/#signup')}}" >Create a new account</a>
                                <i class="fa fa-circle"></i>
                                <a href="{{URL::to('/auth0/#reset')}}">Forgot password?</a>
                            </div>-->
                            <!-- END Login Form -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Login Container -->
</div>

@endsection

@section('inline_scripts')
<script type="text/javascript">
    $( document ).ready(function() {
      $( "#admin_email" ).focus();
    });
</script>

@endsection
