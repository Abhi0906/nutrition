@include('inc.config')
@include('inc.template_start')

<!-- Login Background -->
<div id="login-background">
    <!-- For best results use an image with a resolution of 2560x400 pixels (prefer a blurred image for smaller file size) -->
    {!! HTML::image("img/placeholders/headers/login_header.jpg","Login Background", array('class' => 'animation-pulseSlow')) !!} 
</div>
<!-- END Login Background -->

<!-- Login Container -->
<div id="login-container" class="animation-fadeIn">
    <!-- Login Title -->
    <div class="login-title text-center">
       
        <h1> {!! HTML::image('img/logo.png', 'Genemedics Logo') !!}<br><small><strong>Change Password</strong></small></h1>
    </div>
    <!-- END Login Title -->

    <!-- Login Block -->
    <div class="block push-bit">
        <!-- Login Form -->
        <form class="form-horizontal form-bordered form-control-borderless" role="form" method="POST" action="/password/reset">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="token" value="{{ $token }}">

			<div class="form-group">
				<div class="col-xs-12">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
						<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder='Email'>
					</div>
                </div>
            </div>
			
			<div class="form-group">
                <div class="col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
						<input type="password" class="form-control" name="password" placeholder="Password">
					</div>
                </div>
            </div>

			<div class="form-group">
                <div class="col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
						<input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password">
					</div>
                </div>
            </div>

			<div class="form-group form-actions">
                <div class="col-xs-4">
                   <!--  <label class="switch switch-primary" data-toggle="tooltip" title="Remember Me?">
                       <input type="checkbox" id="login-remember-me" name="login-remember-me" checked>
                       <span></span>
                   </label> -->
                </div>
                <div class="col-xs-8 text-right">
                    <button type="submit" class="btn btn-primary">
						Reset Password
					</button>
                </div>
            </div>
		</form>
        <!-- END Login Form -->        
    </div>
    <!-- END Login Block -->

    <!-- Footer -->
    <footer class="text-muted text-center">
        @if($errors->has())
           @foreach ($errors->all() as $error)
              <div class="alert alert-danger">{{ $error }}</div>
          @endforeach
        @endif
        <small><span id="year-copy"></span> &copy; <a href="http://goo.gl/TDOSuC" target="_blank"><?php echo $template['name'] . ' ' . $template['version']; ?></a></small>
    </footer>
    <!-- END Footer -->
</div>
<!-- END Login Container -->

@include('inc.template_scripts')
@include('inc.template_end')

