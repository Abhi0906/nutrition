@include('inc.config')
@include('inc.template_start')
<body onload="focusOnInput()">
<div id="login-background">
    {!! HTML::image("img_pages/new_bg.png","Login Background", array('class' => 'animation-pulseSlow')) !!} 
</div>
<div id="login-container" class="animation-fadeIn" ng-controller="RegisterController as vm">
    <div class="login-title text-center">
        <h1> {!! HTML::image('img/logo.png', 'Genemedics Logo') !!}<br><small><strong>Register</strong></small></h1>
    </div>
        <span ng-show="vm.loader" class="loader">
        </span>

        <span ng-show="vm.loder_about" class="loader_about">
        </span>
       
    <div class="block push-bit">
        <!-- Register Form -->
        <form id="form-register" class="form-horizontal form-bordered form-control-borderless display-none" 
        name="formRegister" novalidate>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group" >
                <div class="col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                        <input type="email" id="input" autofocus  class="form-control input-lg" name="email" ng-model="vm.user.email" placeholder="Email" required>
                        </div>
                        <div class="input-group">
                        <span class="input-group-addon"><i class=""></i></span>
                        <span ng-show="formRegister.email.$invalid && formRegister.email.$touched">
                        <span class="help-block animation-slideDown" ><span class="text-danger"> Valid Email is Required</span></span>
                        </span>
                        <span class="help-block animation-slideDown"  ng-show="vm.displayServerError"><span class="text-danger">@{{vm.displayServerMessage}}</span></span>
                    </div>
                </div>
            </div>
            <div class="form-group" ng-class="{ 'has-error' : formRegister.password.$invalid && !formRegister.password.$pristine }">
                <div class="col-xs-12">
                    <div class="input-group">
                         <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                         <input type="password" class="form-control input-lg" ng-model="vm.user.password" name="password" placeholder="Password"  ng-minlength="6" required>
                         </div>
                         <div class="input-group">
                         <span class="input-group-addon"><i class=""></i></span>
                         <span ng-show="formRegister.password.$invalid && formRegister.password.$touched">
                         <span class="help-block animation-slideDown"><span class="text-danger">Password Should Atleast 6 Characters</span></span>
                         </span>
                         </div>
                     </div>
            </div>
            <div class="form-group" ng-class="{ 'has-error' : formRegister.password.$invalid && !formRegister.password.$pristine }">
                <div class="col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon">
                        <input type="checkbox" ng-model="vm.user.tos" name="tos" required>
                        </span>
                        <span style="vertical-align: sub;">
                        I agree to the Genemedics 
                        <a href="/terms_and_privacy#tou" target="_blank">Terms of Service</a> 
                        and <a href="/terms_and_privacy#privacy" target="_blank">Privacy Policy</a>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" ng-init="vm.user.news_letter=true">
                            <input type="checkbox" ng-model="vm.user.news_letter"></span>
                            <span style="vertical-align: sub;">
                            Yes, send me the Genemedics Newsletter
                            </span>

                    </div>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-xs-8 col-md-8">
                    <span class="text-danger" ng-hide="formRegister.$valid">&nbsp;&nbsp;</span>
                </div>
                <div class="col-xs-4 col-md-4 text-right">
                        <button type="button" class="btn btn-sm btn-success"  ng-disabled="formRegister.$invalid" ng-click="vm.registerUser()">Continue</button>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12 text-center">
                    <small>Do you have an account?</small> <a href="{{URL::to('/auth/login')}}" id="link-register"><small>Login</small></a>
                </div>
            </div>
        </form>
        <!-- END Register Form -->
        <!-- About you -->
        <form id="form-about_you" class="form-horizontal form-bordered form-control-borderless display-none" name="formAbout" novalidate>
            <div class="form-group">
                <div class="col-xs-12 col-md-6">
                   
                     <div class="input-group">
                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        <input type="text" name="first_name" class="form-control input-lg" placeholder="Firstname" ng-model="vm.about_data.first_name" required>
                    </div>

                     <div class="input-group">
                        <span class="input-group-addon"><i class=""></i></span>
                        <span ng-show="formAbout.first_name.$invalid && formAbout.first_name.$touched">
                        <span class="help-block animation-slideDown" ><span class="text-danger">FirstName is Required</span></span>
                        </span>
                     </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" name="last_name" class="form-control input-lg" placeholder="Lastname" ng-model="vm.about_data.last_name" required>
                    </div>
                   <div class="input-group">
                        <span class="input-group-addon"><i class=""></i></span>
                        <span ng-show="formAbout.last_name.$invalid && formAbout.last_name.$touched">
                        <span class="help-block animation-slideDown" ><span class="text-danger">LastName is Required</span></span>
                        </span>
                     </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-gift"></i></span>
                        <input type="text" id="birth_date" class="form-control input-datepicker" value="<?php echo date('F d, Y'); ?>" data-date-format="MM dd, yyyy" placeholder="MM DD, YYYY">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon"><i class=""></i></span>
                        <div ng-show="vm.ageValidation" class="help-block animation-slideDown"><span class="text-danger">You must be at least 13 years of age to register</span></div>
                        </span>
                    </div>                     
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-transgender"></i></span>
                        <label class="radio-inline" for="male" ng-init="vm.about_data.gender ='male'">
                        <input type="radio" id="male" ng-model="vm.about_data.gender" value="male" name="gender"  required> Male
                        </label>
                        <label class="radio-inline" for="female" >
                        <input type="radio" id="female" ng-model="vm.about_data.gender" value="female" name="gender" required> Female
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12 col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-male"></i></span>
                        <input type="text" name="height_in_feet" class="form-control input-lg" ng-model="vm.about_data.height_in_feet" required>
                        <span class="input-group-btn">
                                 <span class="input-group-addon btn-default">Feet</span>
                        </span>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" name="height_in_inch"  class="form-control input-lg"  ng-model="vm.about_data.height_in_inch" required>
                        <span class="input-group-btn">
                            <span class="input-group-addon btn-default">Inch</span>
                        </span>
                    </div>
                </div>

                     <div class="col-xs-12 col-md-6">
                        <div class="input-group">
                           <span class="input-group-addon"></span>
                           <span ng-show="formAbout.height_in_feet.$invalid && formAbout.height_in_feet.$touched">
                           <span class="help-block animation-slideDown" ><span class="text-danger">Enter valid Height</span></span>
                        </div>
                     </div>
                     <div class="col-xs-12 col-md-6">
                        <div class="input-group">
                           <span class="input-group-addon"></span>
                           <span ng-show="formAbout.height_in_inch.$invalid && formAbout.height_in_inch.$touched">
                           <span class="help-block animation-slideDown" ><span class="text-danger">Enter valid Height</span></span>
                        </div>
                     </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="wellness-icon-weight"></i></span>
                        <input type="text" name="baseline_weight" class="form-control input-lg" placeholder="Weight" ng-model="vm.about_data.baseline_weight" required>
                        <span class="input-group-btn">
                                                      <span class="input-group-addon btn-default">lbs</span>
                         </span>
                    </div>
                </div>
                   <div class="col-xs-12 col-md-6">
                           <div class="input-group">
                              <span class="input-group-addon"></span>
                              <span ng-show="formAbout.baseline_weight.$invalid && formAbout.baseline_weight.$touched">
                              <span class="help-block animation-slideDown" ><span class="text-danger">Weight is Required</span></span>
                           </div>
                        </div>
            </div>
           
            <div class="form-group form-actions">
                <div class="col-xs-9 col-md-8">
                    <span class="text-danger" ng-hide="formAbout.$valid">&nbsp;&nbsp;</span>
                </div>
                <div class="col-xs-3 col-md-4 text-right">
                    <button type="button" class="btn btn-sm btn-success" ng-disabled="formAbout.$invalid" ng-click="vm.updateUserInfo()"><i class="fa fa-plus"></i> Save</button>
                </div>
            </div>
        </form>
        <!-- END About you -->
    </div>

    <footer class="text-muted text-center">
        @if($errors->has())
           @foreach ($errors->all() as $error)
              <div class="alert alert-danger">{{ $error }}</div>
          @endforeach
        @endif
        <small><a href="https://genemedicshealth.com" target="_blank">genemedicshealth.com</a></small>
    </footer>
</div>

@include('inc.template_scripts')
{!! HTML::script("js/pages/login.js") !!}
{!! HTML::script("js/controllers/RegisterController.js") !!}
{!! HTML::script("js/directives/inputDatepicker.js") !!}

<script>$(function(){ $('#form-register').show(); });
function focusOnInput() {
    document.forms["formRegister"]["input"].focus();
}
</script>

@include('inc.template_end')