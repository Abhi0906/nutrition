
<div id="mainpanel1">
  <!-- IF PAGE LIG IN / SIGN UP -->
  <div id="innerpage" class="login-healthapp" style="">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12 col-lg-offset-4 col-lg-4">
          <div class="innerpage">
            <div class="top">
              @if(Session::has('organization_id') )
                                 <img src="/organization_logo/{{$organization['logo']}}" alt="mobile-logo" style="height:35px">
             @else
                <img src="/images/genemedics-nutrition.png" alt="logo">

             @endif
            </div>
            <div class="middle">

              <h4>log in</h4>
              <p class="text-danger text-center text-error" ng-show="auth.invalidCredentials">Invalid credentials</p>
               @if(Session::has('reset_password') )
               <p class="text-danger text-center text-error">{{Session::get('reset_password')}}</p>
               @endif

              <div ng-show="auth.loginLoader" class="login_loader"></div>
              <form name="loginForm" class="form-login" ng-submit="auth.submit($event)">
                <small>@{{auth.message2}}</small>
                <div class="form-group">
                  <input type="email" name="email" 
                  show-focus="auth.loadLoginPage" maxlength="50"  class="email text-box" placeholder="Email"  show-message-on-tab ng-model="auth.email" required>
                  <i class="fa fa-envelope-o"></i>
                  <p class="text-danger text-center text-error" ng-show="loginForm.email.$invalid && loginForm.email.$dirty">
                    <small>You must fill out your valid email.</small>
                  </p>
                </div>
                <div class="form-group">
                  <input type="password" name="password" class="password text-box" maxlength="30" placeholder="Password" ng-model="auth.password" required>
                  <i class="fa fa-asterisk"></i>
                  <p class="text-danger text-center text-error" ng-show="loginForm.password.$invalid && loginForm.password.$touched">
                     <small>You must fill out your password.</small>
                  </p>
                </div>
                <input type="submit" class="btn btndesign1" value="LOG IN" ng-disabled="loginForm.$invalid">
              </form>
              <div class="links">
                <a href="javascript:void(0);"  ng-click="auth.displaySignUp();" >Create a new account</a>
                <i class="fa fa-circle"></i>
                <a href="javascript:void(0);" ng-click="auth.displayResetPassword();">Forgot password?</a>
              </div>
            </div>
            <div class="bottom">
              <div class="divider"></div>
              <p>technical support</p>
              <a href="tel:800-277-4041"><strong><i class="fa fa-phone"></i>&nbsp;800-277-4041</strong></a>
              <a href="mailto:info@genemedics.com"><strong><i class="fa fa-envelope"></i>&nbsp;info@genemedics.com</strong></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  <!-- LOG IN - HEALTH AND WELLNESS APP END -->