@extends('layout.front')
@section('content')
<div class="row" ng-controller="AuthController as auth">
	<div class="col-xs-12" ng-show="auth.loadLoginPage">
		@include('auth.authlogin')
	</div>
	<div class="col-xs-12" ng-show="auth.loadSignUpPage">
		@include('auth.authsignup')
	</div>
	<div class="col-xs-12" ng-show="auth.loadResetPasswordPage">
		@include('auth.authreset')
	</div>
</div>

@endsection