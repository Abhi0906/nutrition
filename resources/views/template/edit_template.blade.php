<div class="row block-title patient_title">
    <div class="col-sm-6"> 
        <h3><strong>@{{edit_template.selectedTemplate.title}}</strong></h3>
    </div>  
    <div class="col-sm-6 text-alignRight">
        <h3>

            <div class="form-group" ng-show="edit_template.Edit_Form_Details">
              <a ng-click="edit_template.editformShowHide()" class="btn btn-default"> 
              <span><i class="fa fa-pencil-square-o"></i></span>&nbsp;Edit</a>
              <a ui-sref="list-workout-plans" class="btn btn-default"> 
              <span><i class="gi gi-remove_2"></i></span>&nbsp;Cancel</a>
            </div> 
            <div class="form-group" ng-show="edit_template.Edit_Form">
              <a ng-disabled="templateEditForm.$invalid" ng-click="edit_template.updateTemplate(templateEditForm.$valid)" class="btn btn-default"> 
              <span><i class="fa fa-pencil-square-o"></i></span>&nbsp;Update</a>
              <a ui-sref="list-workout-plans" class="btn btn-default"> 
              <span><i class="gi gi-remove_2"></i></span>&nbsp;Cancel</a>
            </div> 

        </h3>
    </div>
</div>
<div class="row" ng-show="edit_template.Edit_Form_Details">
    <div class="col-md-5 hidden-xs">
        <div class="row">
            <div class="col-md-6">
                <div class="block full">
                    <div class="block-title"> 
                        <div class="block-options pull-right">
                                <a  data-toggle="button"
                                 class="btn btn-alt btn-xs btn-default enable-tooltip"
                                  href="javascript:void(0)"
                                   ng-click="edit_template.clearWorkoutFilter()" 
                                   data-original-title="Clear Filter">Clear Filter</a>
                        </div>       
                        <h2><strong style="font-size:14px !important">FIND WORKOUTS</strong></h2>
                    </div>
                    <div  class="container-fluid">
                        <div class="row-fluid">
                            <div class="" style="border:none">
                                <label for="keywords" class="space-top">Keyword(s)</label>
                                <input type="text" ng-model="workout_keyword" class="form-control" placeholder="enter search term(s)">
                            </div>  
                        </div>
                        <div class="row-fluid">
                                <div class="" style="border:none">
                                    <label class="space-top">Body Parts</label> 
                                        <br>
                                        <div >       
                                            <select 
                                            chosen="vm.body_parts"
                                            data-placeholder="Choose a Body Parts"
                                            ng-options="body.name as body.name group by body.parent_name for body in vm.body_parts"
                                            ng-model="body_part" >
                                          </select>
                                        </div>
                                </div>      
                        </div>
                        <div class="row-fluid">
                                <div class="" style="border:none">
                                    <label class="space-top">Experience Levels</label> 
                                        <br>
                                        <div >       
                                            <select 
                                            chosen="vm.experiences"
                                            data-placeholder="Choose Experience Level"
                                            ng-options="value for (key,value) in vm.experiences"
                                            ng-model="experience" >
                                        </select>
                                        </div>
                                </div>      
                        </div>
                        <div class="form-group">  
                            <div class="experience space-top">
                              <legend><strong>Experience level legend</strong></legend>
                              <div style="margin-left:15px">
                                <span>
                                <i class="fa fa-square text-primary"></i>&nbsp;&nbsp;
                                <label for="beginner">Beginner</label>
                               </span>
                                <br />
                                <span>
                                <i class="fa fa-square text-warning"></i>&nbsp;&nbsp;
                                <label for="intermediate">Intermediate</label>
                                </span>
                                <br />
                                <span>
                                <i class="fa fa-square text-success"></i>&nbsp;&nbsp;
                                <label for="advanced">Advanced</label> 
                                </span>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 ">
                <simple-block block-title="Workout (@{{edit_template.workouts.length}})">
                    <div class="gallery container" low-margin slim-scroll >
                        <div class="row"
                         ng-repeat="workout in edit_template.workouts"
                         dnd-draggable="workout"
                         dnd-effect-allowed="move"
                         >
                        <div class="col-xs-12 edit_widget">
                            <div class="widget widget-hover-effect1">
                            <div class="widget-simple edit_template  themed-background-dark">
                                <div ng-class="{'widget-icon':true,'pull-left':true,'animation-fadeIn':true,'themed-background ' : workout.experience == 'Beginner','themed-background-autumn' : workout.experience == 'Intermediate','themed-background-spring' : workout.experience == 'Advanced'}" >
                                    <i class="flaticon-stick5"></i>
                                </div>
                                <h5 class="widget-content text-right animation-pullDown">
                                    <strong class="tillana-font" style="color: #D4D4D4;">@{{workout.title}}</strong><br>
                                    <small class="tillana-font" style="color: #D4D4D4;">@{{workout.body_part}}</small>
                                </h5>
                            </div> 
                            </div>
                        </div>                                 
                        </div>
                    </div>
                </simple-block>
            </div>
        </div>
    </div>
    
    <div class="col-md-7 col-xs-12">
        <simple-block 
         block-title="@{{edit_template.selectedTemplate.title}}&nbsp;&nbsp;&nbsp;<small>Duration in days:</small> <small><strong>@{{edit_template.selectedTemplate.days}}</strong></small>,
        <small>Workouts:</small> <small><strong>@{{edit_template.selectedTemplate.workout_count}}</strong></small>" right_title>
           <div class="container-fluid block-content" 
           style="min-height: 250px;padding-left: 0px;"
           dnd-list="edit_template.assignWorkouts"
           dnd-inserted="edit_template.workoutInserted(event, index, item, edit_template.selectedTemplate.id)"
           >
             <interactive-block  
                    block-title="edit_template.getWorkoutTitle(workout)" 
                    ng-repeat = "workout in edit_template.assignWorkouts"
                    on-remove="edit_template.deleteWorkout(workout,edit_template.selectedTemplate.id)"
                    closed
                    >
                <block-content>
                <form name="editTemplateForm" novalidate="novalidate">
                    <div class="row">
                        <div class="col-xs-6" style="border-right:2px solid rgba(0,0,0,0.1)">
                            <div class="row">
                               <div class="col-xs-12">
                                    <label for="example-duration">Duration in day(s)</label>
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group" ng-class="{ 'has-error' : editTemplateForm.start.$invalid && !editTemplateForm.start.$pristine }">
                                        <div class="input-group">
                                            <span class="input-group-addon btn btn-primary">From</span>
                                            <input type="text"
                                              class="form-control"
                                              name="start"
                                              min = "1"
                                              max="@{{workout.pivot.end}}"
                                              ng-model = "workout.pivot.start"
                                              string-to-number
                                              ng-blur ="edit_template.updateSetting(workout,editTemplateForm.$valid)">
                                       </div>
                                  </div>  
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group" ng-class="{ 'has-error' : editTemplateForm.end.$invalid && !editTemplateForm.end.$pristine }">
                                        <div class="input-group">
                                            <span class="input-group-addon btn btn-primary">To</span>
                                            <input type="text" 
                                            name="end"
                                            class="form-control" 
                                            ng-model = "workout.pivot.end"
                                            ng-blur = "edit_template.updateSetting(workout,editTemplateForm.$valid)"
                                            min="@{{workout.pivot.start}}"
                                            string-to-number
                                            required>
                                        </div>
                                    </div>  
                                </div>
                            </div>
                           
                        </div>
                        <div class="col-xs-6" >
                            <div class="row">
                                <div class="col-xs-12">
                                    <label for="example-nf-email">week day(s)</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                <label style="margin-left:10px" for="example-inline-checkbox1" class="checkbox-inline" ng-repeat= "day in edit_template.week">
                                 <input type="checkbox" 
                                        checklist-value="day.dayNumber"
                                        checklist-model ="workout.pivot.config"
                                        checklist-change = "edit_template.updateSetting(workout,editTemplateForm.$valid)"> @{{day.dayName}}
                                </label>
                            </div>
                            </div>
                        </div>
                    </div>
                   
                   
                    <p class="text-danger" ng-show="editTemplateForm.end.$error.min" class="help-block">
                    Workout end duration day(s) not minimum start day(s).</p>

                   <p class="text-danger" ng-show="editTemplateForm.start.$error.max" class="help-block">
                    Workout start duration day(s) not maximam end day(s).</p>


                    <p class="text-danger" ng-show="editTemplateForm.start.$error.min" class="help-block">
                    Workout start duration day(s) is greater than 0. </p>

                 
                </form>
                     <hr>
                    <div class="gallery">
                        <div ng-if="$index%3==0" ng-repeat="xyz in workout.exercise_list" >                     
                            <div  class="row row-eq-height">
                                <div ng-if="($index>=$parent.$index) && ($index<=($parent.$index+2))" 
                                    ng-repeat="exercise in workout.exercise_list" 
                                    class="col-sm-4">
                                    
                                    <single-video-image  not-editable
                                                         not-deletable
                                         view = "edit_template.viewExercise(exercise)"
                                         videoimage="exercise"
                                         width="250" height="140">                            
                                      </single-video-image>

                                    <br>
                                   
                                </div>
                                <br>
                            </div>                            
                        </div>
                    </div>
                </block-content>    
             </interactive-block>
          </div>

        </simple-block>
    </div>
</div>


<div class="row" ng-show="edit_template.Edit_Form">
    <form name="templateEditForm" enctype="multipart/form-data" class="form-horizontal form-bordered">
            <div class="col-md-6">
                <div class="block">
                    <div class="block-title">
                       <h2><strong>Workout Routine Information</strong></h2>
                    </div>
                    
                   <div class="form-group" ng-class="{ 'has-error' : templateEditForm.title.$invalid && !templateEditForm.title.$pristine }">
                        <label for="title">Title
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text"
                               placeholder="Enter Title.."
                               class="form-control" 
                               name="title"
                               ng-model="edit_template.selectedTemplate.title" 
                               id="title" required>
                        
                        <span class="text-danger text-center" ng-show="templateEditForm.title.$invalid && templateEditForm.title.$touched">
                        Please enter title
                        </span>
                    </div>
                    <div class="form-group">
                            <label for="description">Description<span class="text-danger"></span></label>
                              <textarea placeholder="Content.."
                               class="form-control" 
                               rows="3" 
                               name="description" 
                               ng-model="edit_template.selectedTemplate.description"
                               id="description"></textarea>
                    </div>
                </div>
            </div>
            
          <div class="col-md-6">
           <div class="block">
              <div class="block-title"><h2><strong>Workout Routine Image</strong></h2>
              </div>
              <div class="form-group">
                  <div class="col-md-12">
                      <div class="row">
                        <div class="dropzone col-sm-6" 
                              upload-image-url="/api/v3/template_image/upload"
                              remove-image-url="/api/v3/template_image/remove/"
                              file="edit_template.selectedTemplate.image" 
                              upload-image-dropzone 
                              old-file-name = '@{{edit_template.selectedTemplate.image}}'>
                             <div class="dz-default dz-message"><span>Drop Image here to upload</span></div>                     
                        </div>
                        <div class="col-sm-6">
                            <img ng-src="/template-images/@{{edit_template.selectedTemplate.image}}" style=" object-fit: contain; height:auto; width:100%" alt="image">
                        </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </form>
</div> 