<div class="row block-title patient_title" ng-init="list_template.showAddTemplate=true">
    <div class="col-sm-2">
        <h3><strong>Workout Routines</strong></h3>
    </div>  
    <div class="col-sm-10 text-alignRight">
        <h3>
          <form class="form-inline">
                <div class="form-group">
                   <!-- <a  class="btn btn-default" ata-original-title="New Workout Plan"  ng-click="list_template.showAddTemplate = !list_template.showAddTemplate;list_template.addTemplate()"><span><i class="fa fa-plus"></i></span>&nbsp;New Workout Plan</a> -->


                  <a ui-sref="add-workout-plans" class="btn btn-default" ata-original-title="New Workout Plan"><span><i class="fa fa-plus"></i></span>&nbsp;New Workout Routine</a>

                </div>
          </form>
        </h3>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3">
      <form name="templateForm" novalidate="novalidate">
        <div class="block" ng-init="list_template.showAddTemplate=true">
            <div class="block-title">
                <div class="block-options pull-right">
                  <a ng-show="!list_template.showAddTemplate" href="javascript:void(0)"
                    class="btn btn-alt btn-sm btn-default"
                    data-toggle="tooltip" 
                    title=""
                    data-original-title="Find Template"
                    ng-click="list_template.showAddTemplate = !list_template.showAddTemplate;list_template.addTemplate()">
                    <i class="fa fa-search"></i>
                  </a>
                  <!-- <a ng-show="!!list_template.showAddTemplate" href="javascript:void(0)"
                    class="btn btn-alt btn-sm btn-default"
                    data-toggle="tooltip" 
                    title=""
                    data-original-title="Add Template"
                    ng-click="list_template.showAddTemplate = !list_template.showAddTemplate;">
                    <i class="fa fa-plus"></i>
                  </a> -->
                </div>
                <h2 ng-show="!!list_template.showAddTemplate" class="uppercase"><i class="fa fa-search fa-fw"></i> <strong>Find Workout Routines</strong></h2>
                <h2 ng-show="!list_template.showAddTemplate" class="uppercase"><i class="fa fa-tasks fa-fw"></i> <strong>SAVE Workout Routines</strong></h2>
            </div>
            
            <div class="container-fluid">
              <!-- Add Workout -->
                <div ng-show="!list_template.showAddTemplate">
                  <div class="form-group" ng-class="{ 'has-error' : templateForm.title.$invalid && !templateForm.title.$pristine }">
                      <label >Title <sup class="text-danger">*</sup></label>
                      <input type="text" name="title" placeholder="Enter Title.." class="form-control" ng-model="list_template.selectedTemplate.title" ng-minlength="3" required>
                      <p ng-show="templateForm.title.$invalid && !templateForm.title.$pristine" class="help-block">Template title is required Min. 3 characters required</p>
                  </div>
                  <div class="form-group">
                      <label >Description</label>
                      <textarea 
                               placeholder="Content.."
                               class="form-control"
                               rows="3"
                               ng-model="list_template.selectedTemplate.description">
                      </textarea>
                  </div> 
                  <div class="form-group" ng-if="list_template.selectedTemplate.days">
                      <label for="example-nf-duration">Duration in Days
                       <span class="pull-right text-info">&nbsp;
                            <span>::</span>
                              @{{list_template.selectedTemplate.days}}
                        </span></label>
                                                       
                  </div>  
                  <div class="form-group form-actions text-center" >
                      <button class="btn btn btn-default" type="button" ng-click="list_template.saveTemplate()" ng-disabled="templateForm.$invalid">Save</button>
                      <button class="btn btn btn-default" type="button" ng-click="list_template.selectedTemplate = null;list_template.showAddTemplate = !list_template.showAddTemplate;">Cancel</button>
                  </div>
                  <span class="text-danger" ng-hide="templateForm.$valid">&nbsp;&nbsp;(Fields with * are mandatory.)</span>
                </div>
              <!-- End Add Workout-->

              <!-- all workouts  -->
              	<div ng-show ="!!list_template.showAddTemplate">
                  	<div class="form-group">
                      <label for="keyword">Keyword(s)</label>
                      <input type="text" id="keywords" name="keywords" ng-model="keyword" class="form-control" placeholder="enter search term(s)">
                  	</div>
                  	<div class="form-group form-actions">
                      <div class="col-md-9 col-md-offset-3">
                          <button class="btn btn btn-default" type="button" ng-click="list_template.clearFiler()"><i class="fa fa-repeat"></i> &nbsp;&nbsp;Clear Filter</button>
                      </div>
                  	</div>
              	</div>
              <!-- END all workouts -->

            </div>
             <div class="row-fluid"><br></div>
        </div>
      </form>
    </div>
    <!-- workout details List -->
    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
      <div ng-if="list_template.isloading">
        @include('layout.loading')
      </div>
      <div ng-show="list_template.templates.length > 0" ng-if="$index%2==0" ng-repeat="xyz in list_template.templates" >                     
        <div  class="row row-eq-height" >
          <div ng-if="($index>=$parent.$index) && ($index<=($parent.$index+1))" class="col-lg-6" ng-repeat="template in list_template.templates">
              <!-- <div  ng-click="list_template.editTemplate(template, 'edit')" ng-class="{widget: true,'selected-border':template==list_template.selectedTemplate}" style="box-sizing: border-box !important;"> -->
              <div  ng-class="{widget: true,'selected-border':template==list_template.selectedTemplate}" style="box-sizing: border-box !important;">
                  <div class="widget-advanced widget-advanced-alt"> 
                     <!-- Widget Header -->
                    <div  class="widget-header text-center">
                        <img ng-src="@{{template.workouts[0].videos[0].pictures.imageUrl}}" class="widget-background animation-pulseSlow img-responsive" style="width:100%;opacity:0.2">
                        
                        <div class="widget-options-left">
                            <button class="btn btn-primary btn-xs" ng-click="list_template.editTemplate(template)" data-toggle="tooltip" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></button>
                        </div>
                        <div class="widget-options">
                            <button class="btn btn-danger btn-xs " data-toggle="tooltip" title="" ng-click="list_template.deleteTemplate(template)" data-original-title="Delete"><i class="fa fa-times"></i></button>
                        </div>

                        <div ng-click="list_template.editTemplate(template, 'edit')" class="widget-icon themed-background-autumn animation-fadeIn">
                             <i class="fa fa-file-text"></i>
                        </div>
                        <h4  class="widget-content">
                            <strong><a ng-click="list_template.editTemplate(template, 'edit')" class=" @{{template==list_template.selectedTemplate ? 'text-danger':'text-warning'}}" style="text-decoration:none" href="javascript:void(0);">@{{template.title}}</a></strong><br>
                            <small class="text-default"><strong></strong></small>
                        </h4>
                    </div>
                      <!-- END Widget Header --> 
                    <div class="widget-extra">
                        <div ng-class="{row:true,'text-center':true, 'themed-background-dark-autumn': true}">
                            <div class="col-xs-3">
                                <h4 class="widget-content-light">
                                    <strong>@{{template.workout_count}}</strong><br>
                                    <small>Workouts</small>
                                </h4>
                            </div>
                            <div class="col-xs-3">
                                <h4 class="widget-content-light">
                                    <strong>@{{template.video_count}}</strong><br>
                                    <small>Videos</small>
                                </h4>
                            </div>
                             <div class="col-xs-3">
                                <h4 class="widget-content-light">
                                    <strong>@{{template.exercise_count}}</strong><br>
                                    <small>Exercise Images</small>
                                </h4>
                            </div>
                            <div class="col-xs-3">
                                <h4 class="widget-content-light">
                                    <strong>@{{template.days}}</strong><br>
                                    <small>Days</small>
                                </h4>
                            </div>
                        </div>
                    </div>
                  </div>
              </div>
          </div>
        </div><br>
      </div> 
      <div class="alert alert-danger animation-fadeInQuick" ng-cloak ng-show="list_template.templates.length <= 0">
        <button data-dismiss="alert" class="close" type="button">
          <i class="ace-icon fa fa-times"></i>
        </button>

        <strong>
          No Templates available!
        </strong>
      </div>
  	</div>
</div>