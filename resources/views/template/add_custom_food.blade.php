<div class="container-fluid modal-dialog">
		<span ng-show="diary_index.log_newfoodloader"  class="log_newfood_loader"></span>
      <div class="modal-content">
         <div class="modal-header text-center">
            <button type="button" class="close" data-dismiss="modal" >
	            <span aria-hidden="true" >&times;</span>
	            <span class="sr-only" >Close</span>
            </button>
            <h4 class="modal-title"><strong>Add Custom Food</strong></h4>
         </div>
         <div class="modal-body">
         	<div class="row custom-scroll">
	            <form name="customFoodForm" novalidate="novalidate" class="animation-fadeInQuick" ng-submit="diary_index.add_new_food()">

	            	<div class="form-group">
		               <label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Food Name<sup class="text-danger">*</sup></label>
		               
		                  <div class="col-md-6 col-sm-6 col-xs-6 input-group">
		                      <input type="text" maxlength="30" name="food_name" id="custom_food_name" autofocus="true" class="form-control input-xs" ng-model="diary_index.custom_food.food_name"  required>
			                  <span class="text-danger" ng-show="customFoodForm.food_name.$invalid && customFoodForm.food_name.$touched">
			                  	<small>Please enter food name</small>
			                  </span>
		                  </div>
		                  
		            </div>

		            <div class="form-group">
		               <label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Brand Name<sup class="text-danger">*</sup></label>
		               
		                  <div class="col-md-6 col-sm-6 col-xs-6 input-group">
		                     <input type="text" maxlength="30" name="brand_name" class="form-control input-xs" ng-model="diary_index.custom_food.brand_name"  required>
		                     <span class="text-danger" ng-show="customFoodForm.brand_name.$invalid && customFoodForm.brand_name.$touched">
			                  	<small>Please enter brand name</small>
			                 </span>
		                  </div>

		            </div>

		            <div class="form-group">
		               <label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Calories<sup class="text-danger">*</sup></label>
		               
	                  <div class="col-md-6 col-sm-6 col-xs-6 input-group">
	                     <input type="text" number-validation="number-validation"   
		                        	maxlength="8"  name="calories" class="form-control input-xs" ng-model="diary_index.custom_food.calories" required>
	                     <span class="text-danger" ng-show="customFoodForm.calories.$invalid && customFoodForm.calories.$touched">
		                  	<small>Please enter calories</small>
		                 </span>
	                  </div>
		            </div>

		            <div class="form-group">
		               <label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Serving quantity<sup class="text-danger">*</sup></label>
		               
		                  <div class="col-md-6 col-sm-6 col-xs-6 input-group">
		                     <input type="text" number-validation="number-validation"   
		                        	maxlength="4" name="serving_quantity" class="form-control input-xs" ng-model="diary_index.custom_food.serving_quantity" required>
		                     <span class="text-danger" ng-show="customFoodForm.serving_quantity.$invalid && customFoodForm.serving_quantity.$touched">
			                  	<small>Please enter serving quantity</small>
			                 </span>
		                  </div>
		            </div>

		            <div class="form-group">
		               <label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Serving Size Unit</label>
		               
		                  <div class="col-md-6 col-sm-6 col-xs-6 input-group">
		                     <input type="text" class="form-control input-xs" ng-model="diary_index.custom_food.serving_size_unit" >
		                  </div>
		            </div>

		            <div class="form-group">
		               <label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Serving Size Weight</label>
		               
		                  <div class="col-md-6 col-sm-6 col-xs-6 input-group">
		                     <input type="text" class="form-control input-xs" ng-model="diary_index.custom_food.serving_size_weight" >
		                     <span class="input-group-addon btn-primary">g</span>
		                  </div>
		            </div>

		            <div class="form-group">
		               <label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Servings</label>
		               
		                  <div class="col-md-6 col-sm-6 col-xs-6 input-group">
		                     <input type="text" class="form-control input-xs" ng-model="diary_index.custom_food.servings" >
		                  </div>
		            </div>

		            <div class="form-group">
		               <label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Calcium</label>
		               
		                  <div class="col-md-6 col-sm-6 col-xs-6 input-group">
		                     <input type="text" class="form-control input-xs" ng-model="diary_index.custom_food.calcium" >
		                  </div>
		            </div>

		            <div class="form-group">
		               <label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Calories From Fat</label>
		               
		                  <div class="col-md-6 col-sm-6 col-xs-6 input-group">
		                     <input type="text" class="form-control input-xs" ng-model="diary_index.custom_food.calories_from_fat" >
		                     <span class="input-group-addon btn-primary">g</span>
		                  </div>
		            </div>

		            <div class="form-group">
		               <label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Cholesterol</label>
		               
		                  <div class="col-md-6 col-sm-6 col-xs-6 input-group">
		                     <input type="text" class="form-control input-xs" ng-model="diary_index.custom_food.cholesterol">
		                     <span class="input-group-addon btn-primary">mg</span>
		                  </div>
		            </div>

		            <div class="form-group">
		               <label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Dietary Fiber</label>
		               
		                  <div class="col-md-6 col-sm-6 col-xs-6 input-group">
		                     <input type="text" class="form-control input-xs" ng-model="diary_index.custom_food.dietary_fiber" >
		                     <span class="input-group-addon btn-primary">g</span>
		                  </div>
		            </div>

		            <div class="form-group">
		               <label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Iron</label>
		               
		                  <div class="col-md-6 col-sm-6 col-xs-6 input-group">
		                     <input type="text" class="form-control input-xs" ng-model="diary_index.custom_food.iron" >
		                  </div>
		            </div>

		            <div class="form-group">
		               <label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Metric Unit</label>
		               
		                  <div class="col-md-6 col-sm-6 col-xs-6 input-group">
		                     <input type="text" class="form-control input-xs" ng-model="diary_index.custom_food.metric_unit" >
		                  </div>
		            </div>

		            <div class="form-group">
		               <label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Potassium</label>
		               
		                  <div class="col-md-6 col-sm-6 col-xs-6 input-group">
		                     <input type="text" class="form-control input-xs" ng-model="diary_index.custom_food.potassium" >
		                     <span class="input-group-addon btn-primary">mg</span>
		                  </div>
		            </div>

		            <div class="form-group">
		               <label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Protein</label>
		               
		                  <div class="col-md-6 col-sm-6 col-xs-6 input-group">
		                     <input type="text" class="form-control input-xs" ng-model="diary_index.custom_food.protein"  >
		                     
		                  </div>
		            </div>

		            <div class="form-group">
		               <label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Saturated Fat</label>
		               
		                  <div class="col-md-6 col-sm-6 col-xs-6 input-group">
		                     <input type="text" class="form-control input-xs" ng-model="diary_index.custom_food.saturated_fat" >
		                     <span class="input-group-addon btn-primary">g</span>
		                  </div>
		            </div>

		            <div class="form-group">
		               <label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Sodium</label>
		               
		                  <div class="col-md-6 col-sm-6 col-xs-6 input-group">
		                     <input type="text" class="form-control input-xs" ng-model="diary_index.custom_food.sodium">
		                     <span class="input-group-addon btn-primary">mg</span>
		                  </div>
		            </div>

		            <div class="form-group">
		               <label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Sugars</label>
		               
		                  <div class="col-md-6 col-sm-6 col-xs-6 input-group">
		                     <input type="text" class="form-control input-xs" ng-model="diary_index.custom_food.sugars" >
		                     <span class="input-group-addon btn-primary">g</span>
		                  </div>
		            </div>

		            <div class="form-group">
		               <label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Total Carbohydrate</label>
		               
		                  <div class="col-md-6 col-sm-6 col-xs-6 input-group">
		                     <input type="text" class="form-control input-xs" ng-model="diary_index.custom_food.carbohydrate" >
		                     <span class="input-group-addon btn-primary">g</span>
		                  </div>
		            </div>

		            <div class="form-group">
		               <label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Total Fat</label>
		               
		                  <div class="col-md-6 col-sm-6 col-xs-6 input-group">
		                     <input type="text" class="form-control input-xs" ng-model="diary_index.custom_food.total_fat">
		                     <span class="input-group-addon btn-primary">g</span>
		                  </div>
		            </div>

		            <div class="form-group">
		               <label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Trans Fat</label>
		               
		                  <div class="col-md-6 col-sm-6 col-xs-6 input-group">
		                     <input type="text" class="form-control input-xs" ng-model="diary_index.custom_food.trans_fat">
		                     <span class="input-group-addon btn-primary">g</span>
		                  </div>
		            </div>

		            <div class="form-group">
		               <label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Vitamin A</label>
		               
		                  <div class="col-md-6 col-sm-6 col-xs-6 input-group">
		                     <input type="text" class="form-control input-xs" ng-model="diary_index.custom_food.vitamin_a" >
		                  </div>
		            </div>

		            <div class="form-group">
		               <label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Vitamin C</label>
		               
		                  <div class="col-md-6 col-sm-6 col-xs-6 input-group">
		                     <input type="text" class="form-control input-xs" ng-model="diary_index.custom_food.vitamin_c" >
		                  </div>
		            </div>

		            <div class="form-group">
		            	<div class="col-md-4 col-sm-4 col-xs-4"></div>
		            	<div class="col-md-6 col-sm-6 col-xs-6 input-group">
		            		<input type="submit" class="btn btn-primary" value="Add Food" ng-disabled="customFoodForm.$invalid">
		            		<input type="button" class="btn btn-custom" value="cancel" data-dismiss="modal">
		            	</div>
		            </div>

	            </form>
	        </div>
         </div>
      </div>
   </div>