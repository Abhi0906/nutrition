<div class="container-fluid modal-dialog">
	<span ng-show="dash_new.log_waterloader"  class="log_waterloader"></span>
	<div class="modal-content">
	 <div class="modal-header text-center">
	    <button type="button" class="close" data-dismiss="modal" >
	        <span aria-hidden="true" >&times;</span>
	        <span class="sr-only" >Close</span>
	    </button>
	    <h4 class="modal-title"><strong>Add Water Log</strong></h4>
	 </div>
	 <div class="modal-body">
	 	<div class="row">
	        <form name="LogWaterIntake" novalidate="novalidate" class="animation-fadeInQuick" ng-submit="dash_new.logWaterIntake()">

	        	<div class="form-group">
					<label class="col-md-4 col-sm-4 col-xs-4 control-label" class="">Water Amount <sup class="text-danger">*</sup></label>

					<div class="col-md-6 col-sm-6 col-xs-6 input-group">

						<input type="text" maxlength="30" ng-model="dash_new.water_intake" 
						size="4" name="water_intake" id="water_intake" 
						autofocus="true" class="form-control input-xs"  required>
						<span class="input-group-addon btn-primary">US Oz</span>
			
					</div>
	                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-4"></div>
  					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6" style="padding:0">
  						<span class="text-danger clearfix " ng-show="LogWaterIntake.water_intake.$invalid && LogWaterIntake.water_intake.$touched">
  							<small>Please enter water amount</small>
  						</span>  
  					</div>	
	            </div>

	            <div class="form-group">
	            	<div class="col-md-4 col-sm-4 col-xs-4"></div>
	            	<div class="col-md-6 col-sm-6 col-xs-6 input-group">
	            		<input type="submit" class="btn btn-primary" value="Log It" ng-disabled="LogWaterIntake.$invalid">
	            		<input type="button" class="btn btn-custom" value="cancel" data-dismiss="modal">
	            	</div>
	            </div>

	        </form>
	    </div>
	 </div>
	</div>
</div>