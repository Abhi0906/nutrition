<div class="row block-title patient_title">
      <div class="col-sm-6">
        <h3><strong>Create Workout Routine&nbsp;</strong></h3>
      </div>  
      <div class="col-sm-6 text-alignRight">
                    <button class="btn btn-default" ng-disabled="templateForm.$invalid"
                     ng-click="add_template.saveTemplate(templateForm.$valid)">
                     <i class="fa fa-angle-right"></i> Save & Close</button>
                     <a ui-sref="list-workout-plans" class="btn btn-default"> <i class="gi gi-remove_2"></i>&nbsp;Cancel</a>
      </div>
</div>
<div class="row">
    <form name="templateForm" enctype="multipart/form-data" class="form-horizontal form-bordered">
            <div class="col-md-6">
                <div class="block">
                    <div class="block-title">
                       <h2><strong>Workout Routine Information</strong></h2>
                    </div>
                    
                   <div class="form-group" ng-class="{ 'has-error' : templateForm.title.$invalid && !templateForm.title.$pristine }">
                        <label for="title">Title
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text"
                               placeholder="Enter Title.."
                               class="form-control" 
                               name="title"
                               ng-model="add_template.selectedTemplate.title" 
                               id="title" required>
                        
                        <span class="text-danger text-center" ng-show="templateForm.title.$invalid && templateForm.title.$touched">
                        Please enter title
                        </span>
                    </div>
                    <div class="form-group">
                            <label for="description">Description<span class="text-danger"></span></label>
                              <textarea placeholder="Content.."
                               class="form-control" 
                               rows="3" 
                               name="description" 
                               ng-model="add_template.selectedTemplate.description"
                               id="description"></textarea>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="block">
                    <div class="block-title">
                        <h2><strong>Workout Routine Image</strong></h2>
                    </div>
                    <div class="dropzone" 
                           upload-image-url="/api/v3/template_image/upload"
                           remove-image-url="/api/v3/template_image/remove/"
                           file="add_template.selectedTemplate.image" 
                           upload-image-dropzone >
                            <div class="dz-default dz-message"><span>Drop Image here to upload</span></div>                     
                    </div>
                </div>
            </div>
    </form>
</div>