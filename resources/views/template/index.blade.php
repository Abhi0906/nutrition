@extends('layout.app')

@section('content')

<div ng-controller="TemplateController as vm">   
 <!--  <ul class="breadcrumb breadcrumb-top">
      <li>Pages</li>
      <li><a href="javascript:void(0);">Templates</a></li>
  </ul> -->
     <div ui-view></div>
  
</div>
@endsection

@section('page_scripts')
{!! HTML::script("js/controllers/TemplateController.js") !!}
{!! HTML::script("js/controllers/TemplateListController.js") !!}
{!! HTML::script("js/controllers/TemplateEditController.js") !!}
{!! HTML::script("js/controllers/TemplateAddController.js") !!}
{!! HTML::script("js/directives/chosen.js") !!}
{!! HTML::script("js/directives/stringToNumber.js") !!}
{!! HTML::script("js/directives/singleVideoImage.js") !!}
{!! HTML::script("js/directives/uploadImageDropZone.js") !!}

@append

@section('jquery_ready')
@append