<link type="text/css" href="css/style_pdf.css">
<link type="text/css" href="css/genemedics.css">
<div class="logo_div">
  <div class="col-sm-3 col-sm-offset-3">
     <img src="img/logo.png" alt="Genemedics Health" height="30" width="200"> 
  </div>
</div>
<div id="page-content">
  <div class="row"   class="animation-fadeInQuick">
   <div class="col-xs-12 animation-fadeInQuick">
      
      <h2>Reports: <font size="5" class="text-warning">{{$user['full_name']}}</font> </h2>

      @foreach($result->toArray() as $key => $dates)
      <div class="row">
          <div class="block full">
              <div class="block-title text-center">
                  <h2><strong>{{date('F d, Y', strtotime($key))}}</strong></h2>
              </div>
              
              @if(in_array('all_filter', $filter) || in_array('food_filter', $filter))
                <div id="no-more-tables">
                  <table class="table table-condensed" width="100%">
                    <tbody>
                      <tr>
                        <td width="40%" class="first_alt">Food</td>
                        <td width="10%" class="alt hidden-sm hidden-xs">Calories</td>
                        <td width="10%" class="alt hidden-sm hidden-xs">Carbs</td>
                        <td width="10%" class="alt hidden-sm hidden-xs">Fat</td>
                        <td width="10%" class="alt hidden-sm hidden-xs">Protein</td>
                        <td width="10%" class="alt hidden-sm hidden-xs">Sodium</td>
                        <td width="10%" class="alt hidden-sm hidden-xs">Sugar</td>
                      </tr>
                    </tbody>

                    @foreach($dates as $key2 => $schedule_times)

                      @if($key2 == 'BREAKFAST' || $key2 == 'LUNCH' || $key2 == 'DINNER' || $key2 == 'SNACKS')
                        <tbody>
                          <tr>
                            <td colspan="7" class="text-warning">
                              <b>
                               {{$key2}}
                              </b>
                            </td>
                          </tr>

                          @foreach($schedule_times as $key3 => $values)
                            <tr>
                              <td width="40%" data-title="Name">{{$values['nutritions']['name']}}<br></td>
                              <td width="10%" data-title="Calories" class="text-center">{{number_format($values['calories'])}}</td>
                              <td width="10%" data-title="Carbs" class="text-center">{{number_format($values['total_carb'])}}</td>
                              <td width="10%" data-title="Fat" class="text-center">{{number_format($values['total_fat'])}}</td>
                              <td width="10%" data-title="Protein" class="text-center">{{number_format($values['protein'])}}</td>
                              <td width="10%" data-title="Sodium" class="text-center">{{number_format($values['sodium'])}}</td>
                              <td width="10%" data-title="Sugar" class="text-center">{{number_format($values['sugars'])}}</td>
                            </tr>
                          @endforeach
                        </tbody>
                      @endif       
                      @endforeach

                    <tr class="total">
                      <td width="40%" data-title="Name">Total</td>
                      <td width="10%" data-title="Calories" class="text-center">{{number_format($dates['food_total']['food_total_calories'])}}</td>
                      <td width="10%" data-title="Carbs" class="text-center">{{number_format($dates['food_total']['food_total_carb'])}}</td>
                      <td width="10%" data-title="Fat" class="text-center">{{number_format($dates['food_total']['food_total_fat'])}}</td>
                      <td width="10%" data-title="Protein" class="text-center">{{number_format($dates['food_total']['food_total_protein'])}}</td>
                      <td width="10%" data-title="Sodium" class="text-center">{{number_format($dates['food_total']['food_total_sodium'])}}</td>
                      <td width="10%" data-title="Sugar" class="text-center">{{number_format($dates['food_total']['food_total_sugars'])}}</td>
                    </tr>                    
                  </table>
                </div>
              @endif

              @if(in_array('all_filter', $filter) || in_array('exercise_filter', $filter))
                <div id="no-more-tables">
                  <table class="table table-condensed" width="100%">
                    <tbody>
                      <tr>
                          <td width="40%" class="first_alt">Exercise</td>
                          <td width="30%" class="alt hidden-sm hidden-xs">Time in minutes</td>
                          <td width="30%" class="alt hidden-sm hidden-xs">Calories burned</td>
                      </tr>
                    </tbody>

                    @foreach ($dates as $key4 => $schedule_times)
                      @if($key4 == 'CARDIO' || $key4 == 'STRENGTH')
                        <tbody>
                          <tr>
                              <td colspan="7" class="text-warning"><b>{{$key4}}</b></td>
                          </tr>
                          @foreach ($schedule_times as $key5 => $values)
                          <tr>
                             <td width="40%">{{$values['exercise']['name']}}</td>
                             <td width="30%" data-title="Time in minutes" class="text-center">{{$values['time']}}</td>
                             <td width="30%" data-title="Calories burned" class="text-center">{{number_format($values['calories_burned'])}}</td>
                          </tr>
                          @endforeach
                        </tbody>
                      @endif
                    @endforeach
                      
                    <tr class="total">
                       <td width="40%">Total</td>
                       <td width="30%" data-title="Time in minutes" class="text-center">{{$dates['exercise_total']['total_time']}}</td>
                       <td width="30%" data-title="Calories burned" class="text-center">{{number_format($dates['exercise_total']['total_calories_burned'])}}</td>
                    </tr>
                  </table>
                </div>
              @endif

              @if(in_array('all_filter', $filter) || in_array('weight_filter', $filter))
                <div id="no-more-tables">
                  <table class="table table-condensed" width="100%">
                    <tbody>
                      <tr>
                          <td width="40%" class="first_alt">Weight</td>
                          <td width="20%" class="alt hidden-sm hidden-xs">Current Weight</td>
                          <td width="20%" class="alt hidden-sm hidden-xs">Body Fat</td>
                          <td width="20%" class="alt hidden-sm hidden-xs">Source</td>
                      </tr>
                    </tbody>

                    <tbody>
                      <tr class="total">
                        <td width="40%"></td>
                        <td width="20%" data-title="Current Weight" class="text-center">{{$dates['weight_data']['current_weight']}} lbs</td>
                        <td width="20%" data-title="Body Fat" class="text-center">{{$dates['weight_data']['body_fat']}} %</td>
                        <td width="20%" data-title="Source" class="text-center">{{$dates['weight_data']['source']}}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              @endif
          </div>  
      </div>
      @endforeach
   
   </div>
  </div>
</div>

  


