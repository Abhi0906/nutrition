<div class="row block-title patient_title" ng-init="meal_plan.getMealPlans();">
    <div class="col-sm-6">
      <h3><strong>Complete Plan Templates</strong></h3>
    </div>  
    <div class="col-sm-6 text-alignRight">
     
      <a href="javascript:void(0);" ng-click="meal_plan.addMealPlanModal();" class="btn btn-default"><span><i class="fa fa-plus"></i></span>&nbsp;New Complete Plan</a>
      
    </div>
</div>

<!-- Add Meal Plan Modal -->
    <div id="addMealPlanModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-primary">
            <button class="close white_close" type="button" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">@{{meal_plan.action}} Complete Plan</h4>
            
          </div>
          <div class="modal-body">
            <form name="createMealPlanForm" class="form-horizontal" novalidate="novalidate">
                
                <div class="form-group">
                  <label class="col-sm-4 control-label title_blue" for="add_meal">Meal Plan Name</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" ng-model="meal_plan.plan_name" required/>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label title_blue" for="add_meal">Gender</label>
                  <div class="col-sm-6">                      
                     
                      <select class="form-control" name="template_gender"
                                   ng-model="meal_plan.gender"
                                   ng-init="meal_plan.gender='Male'">
                      <option value="Male" >Male</option>
                      <option value="Female">Female</option>
                      </select>

                  </div>
                </div>
                               
                <div class="form-group">
                  <div class="col-sm-4"></div>
                  <div class="col-sm-8">
                    <button type="submit" ng-disabled="createMealPlanForm.$invalid" ng-click="meal_plan.createMealPlan()" class="btn btn-success">@{{meal_plan.action}}</button>          

                  </div>
                </div>
            </form>

            

          </div>
          
          <div class="saperator_5"></div>
        </div>
      </div>
    </div>
<!-- End of Add Meal Plan Modal --> 

<div class="block md_recommended_proui" >

    <div class="md_rec_food_class">
      <table class="table recommeded_table">
        <thead>
          <tr class="">            
            <th class="text-center bg-primary width_50">Male Plan Name</th>
            <th class="text-center bg-primary ">Created At</th>
            <th class="text-center bg-primary ">Updated At</th>
            <th class="text-center bg-primary width_10">View Plans</th>
            <th class="text-center bg-primary width_10">ACTION</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="template in meal_plan.meal_plans | orderBy: '-id' | filter : maleTemplateFilter">
             <td data-title="TEMPLATE NAME" class="text-left">
                @{{template.name}}                
             </td>

             <td data-title="Created At" class="text-left">
                 @{{template.created_at | custom_date_format: 'dd-MMM-yy hh:mm a'}}                
             </td>

             <td data-title="Updated At" class="text-left">
                @{{template.updated_at | custom_date_format: 'dd-MMM-yy hh:mm a'}}                
             </td>
             <td>
                <a ui-sref="view_plan({id: template.id})" >View Plans</a>
             </td>
             
              <td data-title="ACTION" class="text-center">
                <div class="btn-group btn-group-xs">
                  <a class="btn btn-default" href="javascript:void(0);"
                      ng-click="meal_plan.updateMealPlanModal(template);"
                   data-original-title="Edit">
                    <i class="fa fa-pencil-square-o"></i>
                  </a>
                  <a href="javascript:void(0)" class="btn btn-danger" title="" ng-disabled="template.is_default==1" ng-click="meal_plan.deleteMealPlan(template)" data-toggle="tooltip" data-original-title="Delete">
                    <i class="fa fa-times"></i>
                  </a>
                </div>
              </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="md_rec_food_class">
      <table class="table recommeded_table">
        <thead>
          <tr class="">            
            <th class="text-center bg-primary width_50">Female Plan Name</th>
            <th class="text-center bg-primary">Created At</th>
            <th class="text-center bg-primary">Updated At</th>
            <th class="text-center bg-primary width_10">View Plans</th>
            <th class="text-center bg-primary width_10">ACTION</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="template in meal_plan.meal_plans | orderBy: '-id' | filter: femaleTemplateFilter">
             <td data-title="TEMPLATE NAME" class="text-left">
                @{{template.name}}                
             </td>
             <td data-title="Created At" class="text-left">
                 @{{template.created_at | custom_date_format: 'dd-MMM-yy hh:mm a'}}                
             </td>

             <td data-title="Updated At" class="text-left">
                @{{template.updated_at | custom_date_format: 'dd-MMM-yy hh:mm a'}}                
             </td>
             <td>
                <a ui-sref="view_plan({id: template.id})" >View Plans</a>
             </td>
             
                                     
              <td data-title="ACTION" class="text-center">
                <div class="btn-group btn-group-xs">
                   <a class="btn btn-default" href="javascript:void(0);"
                      ng-click="meal_plan.updateMealPlanModal(template);"
                   data-original-title="Edit">
                    <i class="fa fa-pencil-square-o"></i>
                  </a>
                  <a href="javascript:void(0)" class="btn btn-danger" title="" ng-disabled="template.is_default==1" ng-click="meal_plan.deleteMealPlan(template)" data-toggle="tooltip"  data-original-title="Delete">
                    <i class="fa fa-times"></i>
                  </a>
                </div>
              </td>
          </tr>
        </tbody>
      </table>
    </div>
</div>
