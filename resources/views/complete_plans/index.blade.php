@extends('layout.app')

@section('content')

<div>   
    <div ui-view></div>     
</div>


@endsection

@section('page_scripts')

{!! HTML::script("js/controllers/MealPlansController.js") !!}
{!! HTML::script("js/controllers/ViewPlanController.js") !!}
{!! HTML::script("js/filters/dateFormatFilter.js") !!}
{!! HTML::script("js/directives/checklist-model.js") !!}
{!! HTML::script("js/directives/singleVideoImage.js") !!}
{!! HTML::script("js/directives/timePicker.js") !!}


@append

@section('jquery_ready')

@append