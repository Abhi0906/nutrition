 <div class="row block-title patient_title">
      <div class="col-sm-6">
        <h3><strong><span class="text-primary">@{{view_plan.meal_plan_name}}</span></strong></h3>
      </div>
      <div class="col-sm-6 text-alignRight">

        <a href="javascript:void(0);" ng-click="view_plan.savePlan();"
            class="btn btn-default" data-original-title="Save Plan"><span><i class="fa gi gi-disk_saved"></i></span>&nbsp;Save</a>
        <a ui-sref="complete_plans" class="btn btn-default"><span>&nbsp;</span>Cancel</a>
      </div>
  </div>

 @include('common/select_complete_plan')