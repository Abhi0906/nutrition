
<form class="col-sm-3" id="contact">
    <div class="alert alert-success" id="contact_msg">
      <strong>Details submited successfully!</strong> 
    </div>
    <h3 class="lead">Contact</h3>
        <p class="row"> 
            Name:<sup class="red">*</sup>
            <input type="text" id="name" name="name" class="form-control form-control_height" required>
        </p>
        <p class="row"> 
            Email:<sup class="red">*</sup>
            <input type="email" id="email" name="email" class="form-control form-control_height" required>
        </p>
        <p class="row"> 
            Message:
            <textarea id="message" name="message" rows="2" class="form-control" ></textarea>
        </p>
        <p>
            <button type="submit" class="btn btn-md btn-info pull-right" id="send"> Send</button>
        </p>
</form>