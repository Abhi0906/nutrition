<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Genemedics</title>
        <meta name="apple-mobile-web-app-capable" content="yes" />
       <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=no"/>
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="csrf-token" content="{!! csrf_token() !!}">
        <link rel="shortcut icon" href="/img/favicon.png">
        {!! HTML::style("flat-ui/bootstrap/css/bootstrap.css") !!}
        {!! HTML::style("flat-ui/css/flat-ui.css") !!}
        <!-- Using only with Flat-UI (free)-->
        {!! HTML::style("common-files/css/icon-font.css") !!}
        <!-- end -->
        {!! HTML::style("css/style_pages.css") !!}

         <style type="text/css">
            .error{
              color: red !important;
            }
        </style>
    </head>

    <body>
        <div class="page-wrapper">
            <!-- header-2 -->
             @if ($layout)
            <header class="inner header-2">
                <div class="container">
                    <div class="row">
                        <div class="navbar col-sm-12 navbar-fixed-top" role="navigation">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle"></button>
                                <a class="brand" href="https://genemedicshealth.com/">
                                    <img src="img/logo.png" width="242" height="36" alt="" style="margin-top: 5px;"> </a>
                            </div>
                            <div class="collapse navbar-collapse">
                                <ul class="nav pull-right">
                                    <li>
                                        <a href="/" class="text-center">Home</a>
                                    </li>
                                    <li>
                                        <a href="/why_us" class="active text-center">Why Us</a>
                                    </li>
                                    <li>
                                        <a href="#contact" class="text-center">Contact</a>
                                    </li>
									<li>
									<a href="{{URL::to('/auth0/#login')}}" class="text-center">Login</a>
									</li>
                                     <li>
                                     <a href="{{URL::to('/auth0/#signup')}}" class="text-center">Sign up</a>
                                    </li>
                                </ul>
                                <ul class="subnav">
                                    <li>
                                        <a href="#">Privacy</a>
                                    </li>
                                    <li>
                                        <a href="#">Terms</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            @endif
            <section class="inner-section header-2-sub bg-midnight-blue ">
                <div class="background_term">
                    &nbsp;
                </div>
            </section>

            <!-- content-10  -->
            <section class="content-10 service">
                <div class="container">
					<div class="row">
						<div class="col-sm-12">
							<span class="display_top">
							<h2>Why Choose Genemedics Health Institute?</h2><BR/>
								<div class="gene-health-institue-page">
									<h3>Why Choose Genemedics Health Institute?</h3>
									<ul>
										<li>
										Because we are the best. Our team of anti-aging physicians, led by the nationally renowned <span style="text-decoration: underline;"><a href="http://www.genemedics.com/about-george-shanlikian-m-d/">Dr. George Shanlikian, M.D.</a></span>, includes the most highly trained specialists in <a href="http://www.genemedics.com/bioidentical-hormones/">natural bioidentical hormone replacement therapy (BHRT)</a> for <a href="http://www.genemedics.com/hormone-therapy-for-men/">men</a> and <a href="http://www.genemedics.com/hormone-therapy-for-women/">women</a>. Our <a href="http://www.genemedics.com/cost-of-our-hormone-therapy-program/">BHRT programs</a> are individually customized to your lab results and symptoms and complement your <a href="http://www.genemedics.com/nutritional-supplements/">personalized nutrition</a> and <a href="http://www.genemedics.com/exercise/">exercise program.</a></li>
									</ul>
									<ul>
										<li>We <a href="http://www.genemedics.com/cost-of-our-hormone-therapy-program/">offer</a> the most comprehensive health assessment of any clinic in the country which includes:
										<ul>
											<li><a href="http://www.genemedics.com/lab-testing/" title="Lab testing">Lab testing:</a> We offer the most thorough lab testing available, including <em>comprehensive hormone testing</em>, as well as the <em>full</em> range of preventative health markers (i.e., cholesterol levels, liver enzymes, kidney markers, inflammation, blood glucose, advanced markers of heart risk, vitamin D status, etc.).</li>
											<li>Body composition testing: We use the most advanced equipment and rigorous standards to measure body fat, muscle mass, and water weight. This information is then used to design your personalized nutrition and fitness program. Genemedics’ focus on combining BHRT with the same body fat calculations used by professional athletes ensures that your total health makeover will yield results.</li>
											<li>Resting metabolic rate: We use the most advanced, FDA-approved method to determine how many calories your body burns at rest over a 24-hour period. We use this information to design your custom exercise and nutrition program.</li>
											<li>Fitness testing: Our FDA-approved, gold standard test very accurately measures your cardio-respiratory fitness level. This helps us determine your baseline fitness level and guides us in customizing an exercise program for optimal heart and lung health.</li>
											<li>Flexibility testing: Flexibility is a key component of youthful health. We teach our patients proper stretching techniques to improve flexibility.</li>
											<li>Muscle endurance testing: We track this important component of overall health and longevity throughout your treatment program.</li>
											<li>Muscle strength testing: Your core exercise program is designed to make you stronger, with more powerful muscles, which will increase your resting metabolic rate and protect your joints and bones.</li>
										</ul>
										</li>
										<li>Unlike many anti-aging clinics, we stress the importance of whole-body nutrition through supplementation and diet. Your nutrition program will be designed specifically to nourish and support your youthful body.</li>
										<li>You will have access to our exercise video library, with advanced workout routines and instructions on how to perform each exercise. Your complete workout will be set at the pace and intensity recommended for your customized exercise routine and based on your specific goals.</li>
										<li>We provide you with your own patient portal, with complete access to your medical chart (including completed and signed lab results, medications, etc.), nutrition plan, nutritional supplement regimen and exercise routine.</li>
										<li>We offer the highest quality, pharmaceutical-grade supplements available!</li>
										<li>You can always reach us, and if we aren’t available, we’ll call you back promptly.</li>
										<li>We are one of the few anti-aging clinics in the country that accepts insurance. We also offer the most affordable prices for all of our programs including: weight loss, <a href="http://www.genemedics.com/bioidentical-hormones/">bioidentical hormone therapy</a>, men’s health and women’s health.</li>
									</ul>
									<h3>Find an Expert Bioidentical Hormone Replacement Therapy Doctor Near You!</h3>
									<p>Finding a highly qualified <a href="http://www.genemedics.com/hormone-replacement-therapy-arizona/">bioidentical hormone replacement therapy physician</a> is one of the most important decisions you will ever make! Would you let your primary care physician, urologist or OB/GYN physician perform your heart surgery? Of course not! The same applies for bioidentical hormone replacement therapy, or BHRT. Unfortunately, many physicians prescribe hormones despite not having the proper training and knowledge or knowing how to monitor your levels. Levels that are too low will result in limited benefits. Levels that are too high can result in unwanted side effects.</p>
									<p>All Genemedics Health Institute medical doctors are national leaders in the specialized field of bioidentical hormone replacement therapy. Our physicians have thousands of hours of training in administering bioidentical hormone therapy. They are board-certified and Advanced Fellows in Anti-Aging and Regenerative Medicine.</p>
									<p><a href="http://www.genemedics.com/bioidentical-hormones/" title="Hormone replacement therapy">Hormone replacement therapy</a> has been proven to greatly enhance the quality of one’s life, while possibly lengthening one’s lifespan! Patients using hormone replacement therapy truly feel like they have discovered the fountain of youth! When it comes to your health, only trust the best!</p>
									<p>Schedule a visit to our <a href="http://www.genemedics.com/hormone-replacement-therapy-florida/">Hormone Replacement Therapy centre</a> now.
									</p>
								</div>
							</span>
						</div>
					</div>
				</div>
         	</section>

            <!-- footer-1 -->
             @if ($layout)
            <footer class="footer-1 bg-midnight-blue">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="row social">
                                <ul >
                                    <li class="fb">
                                        <a target="_blank" rel="nofollow" href="https://www.facebook.com/Genemedics" title="Facebook" class="fui-facebook"></a>
                                    </li>
                                    <li class="tw">
                                        <a target="_blank" rel="nofollow" href="https://twitter.com/genemedics" title="Twitter" class="fui-twitter"></a>
                                    </li>
                                   <li class="googleplus">
                                        <a target="_blank" rel="nofollow" href="https://plus.google.com/+Genemedics/" title="GooglePlus" class="fui-googleplus"></a>
                                    </li>
                                    <li class="linkedin">
                                        <a target="_blank" rel="nofollow" href="https://www.linkedin.com/in/genemedics" title="Linkedin" class="fui-linkedin"></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="row">
                                <br/><br/>
                                <div class="">
                                <a class="btn btn-danger col-xs-12 col-sm-6" href="https://itunes.apple.com/us/app/genemedicshealth-health-tracker/id1061451973?mt=8">Download App</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">     
                        
                        </div>
                         @include('pages.contact_form')
                    </div>
                    <div class="row">
                        <br/>
                        <div class="additional-links">
                             <a href="/terms_and_privacy?layout=true#tou">Terms of Service</a>
                            <a href="/terms_and_privacy?layout=true#privacy">Privacy Policy</a>
                        </div>
                    </div>
                </div>
            </footer>
            @endif
        </div>
        

        <!-- Placed at the end of the document so the pages load faster -->
        {!! HTML::script("common-files/js/jquery-1.10.2.min.js") !!}
        {!! HTML::script("flat-ui/js/bootstrap.min.js") !!}
        {!! HTML::script("common-files/js/modernizr.custom.js") !!}
        {!! HTML::script("common-files/js/jquery.sharrre.min.js") !!}
        {!! HTML::script("common-files/js/startup-kit.js") !!}
        {!! HTML::script("js/jquery.validate.min.js") !!}
        {!! HTML::script("js/PriorityQueue.js") !!}
        {!! HTML::script("js/common.js") !!}
        {!! HTML::script("js/page.js") !!}
    </body>
</html>