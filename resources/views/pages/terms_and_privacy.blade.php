<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Genemedics</title>
        <meta name="apple-mobile-web-app-capable" content="yes" />
       <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=no"/>
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
          <meta name="csrf-token" content="{!! csrf_token() !!}">
        <link rel="shortcut icon" href="/img/favicon.png">
        {!! HTML::style("flat-ui/bootstrap/css/bootstrap.css") !!}
        {!! HTML::style("flat-ui/css/flat-ui.css") !!}
        <!-- Using only with Flat-UI (free)-->
        {!! HTML::style("common-files/css/icon-font.css") !!}
        <!-- end -->
        {!! HTML::style("css/style_pages.css") !!}

         <style type="text/css">
            .error{
              color: red !important;
            }
        </style>
    </head>

    <body>
        <div class="page-wrapper">
            <!-- header-2 -->
            @if ($layout)
            <header class="inner header-2">
                <div class="container">
                    <div class="row">
                        <div class="navbar col-sm-12 navbar-fixed-top" role="navigation">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle"></button>
                                <a class="brand" href="https://genemedicshealth.com/"><img src="img/logo.png" width="242" height="36" alt="" style="margin-top: 5px;" alt=""> </a>
                            </div>
                            <div class="collapse navbar-collapse">
                                <ul class="nav pull-right">
                                    <li>
                                        <a href="/" class="text-center">Home</a>
                                    </li>
                                    <li>
                                        <a href="/why_us" class="text-center">Why Us</a>
                                    </li>
                                    <li>
                                        <a href="#contact" class="text-center">Contact</a>
                                    </li>
                                    <li>
                                    <a href="{{URL::to('/auth0/#login')}}" class="text-center">Login</a>
                                    </li>
                                     <li>
                                     <a href="{{URL::to('/auth0/#signup')}}" class="text-center">Sign up</a>
                                    </li>
                                </ul>
                                <ul class="subnav">
                                    <li>
                                        <a href="#">Privacy</a>
                                    </li>
                                    <li>
                                        <a href="#">Terms</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            @endif
            <section class="inner-section header-2-sub bg-midnight-blue ">
                <div class="background_term">
                    &nbsp;
                </div>
            </section>

            <!-- content-10  -->
            <section class="content-10 service">
                <div class="container">
					<div class="row">
						<div class="col-sm-12">
						<ul class="terms">
					  <li><a href="#tou">Terms of Use</a></li>
					  <li><a href="#privacy">Privacy Policy</a></li>
						</ul>
                        <span class="anchor" id="tou"></span>
							  <h1>GENEMEDICSHEALTH Terms of Use</h1>
						<p>We may provide a translated version of this Agreement, the Privacy Policy or any other operating rules, policies and procedures regarding the Services, in other languages for your convenience. Please note that the English language version of these documents is the version that governs your use of the Services and in the event of any conflict between the English language version and a translated version, the English language version will control.</p>
						<ol class="first">
						  <li>
							<h2>AGREEMENT</h2>
							<p>These Terms of Use (the "<strong>Agreement</strong>") constitute a legally binding agreement by and between Genemedics Health Institute, P.C. (hereinafter, "<strong>GENEMEDICSHEALTH</strong>") and you ("<strong>You</strong>" or "<strong>Your</strong>") concerning Your use of GENEMEDICSHEALTH's website located at http://www.GENEMEDICSHEALTH.com/ (the "<strong>Website</strong>") and GENEMEDICSHEALTH mobile phone applications (the "<strong>Applications</strong>") and the services available through the Website and Applications (the "<strong>Services</strong>"). By using the Services, You represent and warrant that You have read and understand, and agree to be bound by, this Agreement and GENEMEDICSHEALTH's <a href="#privacy">Privacy Policy</a>, which is incorporated herein by reference and made part of this Agreement. IF YOU DO NOT UNDERSTAND THIS AGREEMENT, OR DO NOT AGREE TO BE BOUND BY THESE TERMS, YOU MAY NOT USE THE SERVICES.</p>
						  </li>
						  <li>
							<h2>PRIVACY POLICY</h2>
							<p>By using the Services, You consent to the collection and use of certain information about You, as specified in the <a href="#privacy">Privacy Policy</a>.  Use of information we collect now is subject to the Privacy Policy in effect at the time such information is used.</p>
						  </li>
						  <li>
							<h2>CHANGES TO AGREEMENT</h2>
							<p>GENEMEDICSHEALTH RESERVES THE RIGHT TO CHANGE THIS AGREEMENT AT ANY TIME UPON NOTICE TO YOU, TO BE GIVEN BY: (I) THE POSTING OF A NEW VERSION; AND/OR (II) A CHANGE NOTICE ON THE WEBSITE OR APPLICATION. IT IS YOUR RESPONSIBILITY TO REVIEW THIS AGREEMENT PERIODICALLY. You will be deemed to have agreed to any such modification or amendment by Your decision to continue using the Services following the date in which the modified or amended Agreement is posted.</p>
						  </li>
						  <li>
							<h2>ELIGIBILITY</h2>
							<p>BY USING THE SERVICES, YOU REPRESENT AND WARRANT THAT YOU ARE AT LEAST 18 YEARS OLD AND ARE OTHERWISE LEGALLY QUALIFIED TO ENTER INTO AND FORM CONTRACTS UNDER APPLICABLE LAW. This Agreement is void where prohibited.</p>
						  </li>
						  <li>
							<h2>LICENSE</h2>
							<p>Subject to Your compliance with the terms and conditions of this Agreement, GENEMEDICSHEALTH grants You a non-exclusive, non-sublicensable, revocable, non-transferable license to use the Services through the Website or by downloading and installing our Applications. THE SERVICES ARE FOR YOUR PERSONAL AND NON-COMMERCIAL USE. The Services, including the Website and the Applications, or any portion thereof, may not be reproduced, duplicated, copied, modified, sold, resold, distributed, visited, or otherwise exploited for any commercial purpose without the express written consent of GENEMEDICSHEALTH. Except as expressly set forth herein, this Agreement grants You no rights in or to the intellectual property of GENEMEDICSHEALTH or any other party. The license granted in this section is conditioned on Your compliance with the terms and conditions of this Agreement. In the event that You breach any provision of this Agreement, Your rights under this section will immediately terminate.</p>
							<p>When accessing the Services through the Applications that were downloaded from an app store or app distribution platform, such as the Apple App Store or Google Play, (the &quot;App Provider&quot;), You acknowledge and agree that: (a) this Agreement are concluded between us, and not with the App Provider, and that we are solely responsible for the Applications (not the App Provider); (b) the App Provider has no obligation to furnish any maintenance and support services with respect to the Applications; (c) in the event of any failure of the Applications to conform to any applicable warranty, (i) you may notify the App Provider and the App Provider will refund the purchase price for the Applications to you (if applicable), (ii) to the maximum extent permitted by applicable law, the App Provider will have no other warranty obligation whatsoever with respect to the Applications, and (iii) any other claims, losses, liabilities, damages, costs or expenses attributable to any failure to conform to any warranty will be our responsibility; (d) the App Provider is not responsible for addressing any claims you have or any claims of any third party relating to the Applications or your possession and use of the Applications, including, but not limited to: (i) product liability claims; (ii) any claim that the app fails to conform to any applicable legal or regulatory requirement; and (iii) claims arising under consumer protection or similar legislation; (e) in the event of any third party claim that the Applications or your possession and use of that Applications infringes that third party&#39;s intellectual property rights, we will be responsible for the investigation, defense, settlement and discharge of any such intellectual property infringement claim to the extent required by this Agreement; (f) the App Provider, and its subsidiaries, are third party beneficiaries of this Agreement as it relates to your license of the Applications, and that, upon your acceptance of the terms and conditions of this Agreement, the App Provider will have the right (and will be deemed to have accepted the right) to enforce this Agreement as related to your license of the Applications against you as a third party beneficiary thereof; and (g) you must also comply with all applicable third party terms of service when using the Applications.</p>
						  </li>
						  <li>
							<h2>THE SERVICE DOES NOT PROVIDE PROFESSIONAL MEDICAL SERVICES OR ADVICE; NO DOCTOR-PATIENT RELATIONSHIP</h2>
							<p>GENEMEDICSHEALTH provides the Services for informational purposes only. THE SERVICES DO NOT CONTAIN OR CONSTITUTE, AND SHOULD NOT BE INTERPRETED AS, MEDICAL ADVICE OR OPINION. GENEMEDICSHEALTH is not a medical professional, and GENEMEDICSHEALTH does not provide medical services or render medical advice. The Services are not a substitute for the advice of a medical professional, and the information made available on or through the Services should not be relied upon when making medical decisions, or to diagnose or treat a medical or health condition. If you require medical advice or services, You should consult a medical professional. YOUR USE OF THE SERVICES DOES NOT CREATE A DOCTOR-PATIENT RELATIONSHIP BETWEEN YOU AND GENEMEDICSHEALTH.</p>
							<p>YOU HEREBY AGREE THAT, BEFORE USING THE SERVICES, YOU SHALL CONSULT YOUR PHYSICIAN, PARTICULARLY IF YOU ARE AT RISK FOR PROBLEMS RESULTING FROM EXERCISE OR CHANGES IN YOUR DIET.</p>
						  </li>
						  <li>
							<h2>FOOD DATABASE AND NUTRITIONAL INFORMATION</h2>
							<p>GENEMEDICSHEALTH's food database contains a combination of nutritional information entered directly by GENEMEDICSHEALTH and nutritional information entered by GENEMEDICSHEALTH members ("<strong>Food Database</strong>"). Any GENEMEDICSHEALTH member can contribute nutritional information to the Food Database, as well as edit existing nutritional information. Please be advised that nutritional information found in GENEMEDICSHEALTH's Food Database has not been reviewed by persons with the expertise required to provide You with complete, accurate, or reliable information. GENEMEDICSHEALTH DOES NOT (I) GUARANTEE THE ACCURACY, COMPLETENESS, OR USEFULNESS OF ANY NUTRITIONAL INFORMATION IN THE FOOD DATABASE; OR (II) ADOPT, ENDORSE OR ACCEPT RESPONSIBILITY FOR THE ACCURACY OR RELIABILITY OF ANY SUCH NUTRITIONAL INFORMATION. UNDER NO CIRCUMSTANCES WILL GENEMEDICSHEALTH BE RESPONSIBLE FOR ANY LOSS OR DAMAGE RESULTING FROM YOUR RELIANCE ON NUTRITIONAL INFORMATION. You are solely responsible for ensuring that any nutritional information in the Food Database is accurate, complete and useful. Nutritional information in the Food Database may not be reproduced, duplicated, copied, modified, sold, resold, distributed, visited, or otherwise exploited for any commercial purpose without the express written consent of GENEMEDICSHEALTH.</p>
						  </li>
						  <li>
							<h2>RELIANCE ON THIRD-PARTY CONTENT</h2>
							<p>Opinions, advice, statements, or other information, including, without limitation, food, nutrition and exercise data, made available by means of the Services by third parties, are those of their respective authors, and should not necessarily be relied on. Such authors are solely responsible for such content. GENEMEDICSHEALTH DOES NOT: (I) GUARANTEE THE ACCURACY, COMPLETENESS, OR USEFULNESS OF ANY THIRD-PARTY INFORMATION ON THE SERVICE; OR (II) ADOPT, ENDORSE OR ACCEPT RESPONSIBILITY FOR THE ACCURACY OR RELIABILITY OF ANY OPINION, ADVICE OR STATEMENT MADE BY A THIRD-PARTY BY MEANS OF THE SERVICES. UNDER NO CIRCUMSTANCES WILL GENEMEDICSHEALTH BE RESPONSIBLE FOR ANY LOSS OR DAMAGE RESULTING FROM YOUR RELIANCE ON INFORMATION OR OTHER CONTENT POSTED ON THE SERVICES OR TRANSMITTED TO OR BY ANY THIRD-PARTY. You also understand that by accessing and using the Services, You may encounter information, materials and subject matter that You or others may deem offensive, indecent, or objectionable. You agree to use the Services at Your sole risk and that GENEMEDICSHEALTH and its affiliates, partners, suppliers and licensors shall have no liability to You for information, material or subject matter that is found to be offensive, indecent, or objectionable.</p>
						  </li>
						  <li>
							<h2>RISK ASSUMPTION</h2>
							<p>YOU KNOWINGLY AND FREELY ASSUME ALL RISK WHEN USING THE WEBSITE, APPLICATIONS AND SERVICES. YOU, ON BEHALF OF YOURSELF, YOUR PERSONAL REPRESENTATIVES AND YOUR HEIRS, HEREBY VOLUNTARILY AGREE TO RELEASE, WAIVE, DISCHARGE, HOLD HARMLESS, DEFEND AND INDEMNIFY GENEMEDICSHEALTH AND ITS OFFICERS, DIRECTORS, EMPLOYEES, AGENTS, AFFILIATES, REPRESENTATIVES, SUBLICENSEES, SUCCESSORS, AND ASSIGNS FROM ANY AND ALL CLAIMS, ACTIONS OR LOSSES FOR BODILY INJURY, PROPERTY DAMAGE, WRONGFUL DEATH, EMOTIONAL DISTRESS, LOSS OF SERVICES OR OTHER DAMAGES OR HARM, WHETHER TO YOU OR TO THIRD PARTIES, WHICH MAY RESULT FROM YOUR USE OF THE SERVICES.</p>
						  </li>
						  <li>
							<h2>USER INFORMATION; PASSWORD PROTECTION</h2>
							<p>In connection with Your use of certain Services, You are required to complete a registration form. You represent and warrant that all user information You provide on the registration form or otherwise in connection with Your use of the Services will be current, complete and accurate, and that You will update that information as necessary to maintain its completeness and accuracy by visiting your personal profile. For additional information, see the section concerning "User Ability to Access, Update, and Correct Personal Information" in GENEMEDICSHEALTH's <a href="#privacy">Privacy Policy</a>.</p>
							<p>You will also be asked to provide a user name and password in connection with Your use of certain of the Services. You are entirely responsible for maintaining the confidentiality of Your password. You may not use the account, user name, or password of any other Member at any time. You agree to notify GENEMEDICSHEALTH immediately of any unauthorized use of Your account, user name, or password. GENEMEDICSHEALTH shall not be liable for any loss that You incur as a result of someone else using Your password, either with or without Your knowledge. You may be held liable for any losses incurred by GENEMEDICSHEALTH, its affiliates, officers, directors, employees, consultants, agents, and representatives due to someone else&#39;s use of Your account or password.</p>
						  </li>
						  <li>
							<h2>PUBLIC PROFILES; INFORMATION PROVIDED BY MEMBERS</h2>
							<p>As part of registration, members must create public profiles, which may contain certain indentifying information (such as age, location, total weight loss, ethnicity, marital status, religion, etc.). In addition, members have the option to post photographs, videos and other information (such as likes and dislikes) on their public profiles. GENEMEDICSHEALTH relies on its members to provide current and accurate information, and GENEMEDICSHEALTH does not, and cannot, investigate information contained in member public profiles. Accordingly, GENEMEDICSHEALTH must assume that information contained in each member public profile is current and accurate. GENEMEDICSHEALTH DOES NOT REPRESENT, WARRANT OR GUARANTEE THE CURRENCY OR ACCURACY OF PUBLIC PROFILE INFORMATION, AND HEREBY DISCLAIMS ALL RESPONSIBILITY AND LIABILITY FOR ANY INFORMATION PROVIDED BY MEMBERS BY MEANS OF PUBLIC PROFILES OR OTHERWISE IN CONNECTION WITH THE SERVICES.</p>
						  </li>
						  <li>
							<h2>YOUR INTERACTIONS WITH OTHER MEMBERS</h2>
							<p>YOU ARE SOLELY RESPONSIBLE FOR YOUR INTERACTIONS WITH OTHER MEMBERS. YOU ACKNOWLEDGE AND UNDERSTAND THAT GENEMEDICSHEALTH HAS NOT, AND DOES NOT, IN ANY WAY: (A) SCREEN ITS MEMBERS; (B) INQUIRE INTO THE BACKGROUNDS OF ITS MEMBERS; OR (C) REVIEW OR VERIFY THE STATEMENTS OF ITS MEMBERS, INCLUDING WITHOUT LIMITATION INFORMATION OR REPRESENTATIONS CONTAINED IN PUBLIC PROFILES. YOU HEREBY AGREE TO EXERCISE REASONABLE PRECAUTION IN ALL INTERACTIONS WITH OTHER MEMBERS, PARTICULARLY IF YOU DECIDE TO MEET ANOTHER MEMBER IN PERSON. GENEMEDICSHEALTH DOES NOT REPRESENT, WARRANT, ENDORSE OR GUARANTEE THE CONDUCT OF ITS MEMBERS OR THEIR COMPATIBILITY WITH YOU. IN NO EVENT SHALL GENEMEDICSHEALTH BE LIABLE FOR INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF OR RELATING TO ANY MEMBER&#39;S CONDUCT IN CONNECTION WITH SUCH MEMBER&#39;S USE OF THE SERVICES, INCLUDING, WITHOUT LIMITATION, BODILY INJURY, PROPERTY DAMAGE, WRONGFUL DEATH, EMOTIONAL DISTRESS, LOSS OF SERVICES OR ANY OTHER DAMAGES RESULTING FROM COMMUNICATIONS OR MEETINGS BETWEEN MEMBERS. YOU KNOWINGLY AND FREELY ASSUME ALL RISK WHEN USING THE WEBSITE, APPLICATIONS AND SERVICES.</p>
						  </li>
						  <li>
							<h2>MEMBER DISPUTES</h2>
							<p>GENEMEDICSHEALTH reserves the right, but disclaims any perceived, implied or actual duty, to monitor disputes between members. You agree to hold GENEMEDICSHEALTH harmless in connection with any dispute or claim You make against any other member.</p>
						  </li>
						  <li>
							<h2>CONSENT TO RECEIVE EMAIL FROM GENEMEDICSHEALTH</h2>
							<p>In providing the Services, You may receive periodic email communications regarding the Services, new product offers and information regarding the Services, which are part of the Services and which You cannot opt out of receiving. You may also receive periodic promotions and other offers or materials GENEMEDICSHEALTH believes might be of interest to You. You can opt-out of receiving these promotional messages at any time by (a) following the unsubscribe instructions contained in each newsletter; or (b) changing the email preferences in Your account.</p>
						  </li>
						  <li>
							<h2>CONSENT TO RECEIVE EMAIL COMMUNICATIONS FROM MEMBERS</h2>
							<p>By finding and connecting with Your friends and other members, You may receive electronic communications, including email and instant messages from other Website members. You can manage the messages You receive from other members by changing the messaging preferences in Your account.</p>
						  </li>
						  <li>
							<h2>THIRD-PARTY WEBSITES</h2>
							<p>The Service includes links to websites of third parties ("<strong>Third-Party Websites</strong>"), some of whom may have established relationships with GENEMEDICSHEALTH and some of whom may not. GENEMEDICSHEALTH does not have control over the content and performance of Third-Party Websites. GENEMEDICSHEALTH HAS NOT REVIEWED, AND CANNOT REVIEW OR CONTROL, ALL OF THE MATERIAL, INCLUDING COMPUTER SOFTWARE OR OTHER GOODS OR SERVICES, MADE AVAILABLE ON OR THROUGH THIRD-PARTY WEBSITES. ACCORDINGLY, GENEMEDICSHEALTH DOES NOT REPRESENT, WARRANT OR ENDORSE ANY THIRD-PARTY WEBSITE, OR THE ACCURACY, CURRENCY, CONTENT, FITNESS, LAWFULNESS OR QUALITY OF THE INFORMATION MATERIAL, GOODS OR SERVICES AVAILABLE THROUGH THIRD-PARTY WEBSITES. GENEMEDICSHEALTH DISCLAIMS, AND YOU AGREE TO ASSUME, ALL RESPONSIBILITY AND LIABILITY FOR ANY DAMAGES OR OTHER HARM, WHETHER TO YOU OR TO THIRD PARTIES, RESULTING FROM YOUR USE OF THIRD-PARTY WEBSITES.</p>
							<p>YOU AGREE THAT, WHEN LINKING TO OR OTHERWISE ACCESSING OR USING A THIRD-PARTY WEBSITE, YOU ARE RESPONSIBLE FOR: (I) TAKING PRECAUTIONS AS NECESSARY TO PROTECT YOU AND YOUR COMPUTER SYSTEMS FROM VIRUSES, WORMS, TROJAN HORSES, MALICIOUS CODE AND OTHER HARMFUL OR DESTRUCTIVE CONTENT; (II) ANY DOWNLOADING, USE OR PURCHASE OF MATERIAL THAT IS OBSCENE, INDECENT, OFFENSIVE, OR OTHERWISE OBJECTIONABLE OR UNLAWFUL, OR THAT CONTAINS TECHNICAL INACCURACIES, TYPOGRAPHICAL MISTAKES AND OTHER ERRORS; (III) ANY DOWNLOADING, USE OR PURCHASE OF MATERIAL THAT VIOLATES THE PRIVACY OR PUBLICITY RIGHTS, OR INFRINGES THE INTELLECTUAL PROPERTY AND OTHER PROPRIETARY RIGHTS OF THIRD PARTIES, OR THAT IS SUBJECT TO ADDITIONAL TERMS AND CONDITIONS, STATED OR UNSTATED; (IV) ALL FINANCIAL CHARGES OR OTHER LIABILITIES TO THIRD PARTIES RESULTING FROM TRANSACTIONS OR OTHER ACTIVITIES; AND (V) READING AND UNDERSTANDING ANY TERMS OF USE OR PRIVACY POLICIES THAT APPLY TO THOSE THIRD-PARTY WEBSITES.</p>
						  </li>
						  <li>
							<h2>THIRD-PARTY SERVICES</h2>
							<p>Certain features, aspects, products and services offered through the Services are provided, in whole or in part, by third parties ("<strong>Third-Party Services</strong>" as provided by "<strong>Third-Party Service Providers</strong>"). In order to use Third-Party Services, You may be required to enter into additional terms and conditions with Third-Party Service Providers. IF YOU DO NOT UNDERSTAND OR DO NOT AGREE TO BE BOUND BY THOSE ADDITIONAL TERMS AND CONDITIONS, YOU MAY NOT USE THE RELATED THIRD-PARTY SERVICES. In the event of any inconsistency between terms and conditions relating to Third-Party Services and the terms and conditions of this Agreement, those additional terms and conditions will control, although only with respect to such Third-Party Services. The providers of Third-Party Service Providers may collect and use certain information about you, as specified in the Third-Party Service Providers&#39; privacy policies. Prior to providing information to any Third-Party Service Provider, you should review their privacy policy. IF YOU DO NOT UNDERSTAND OR DO NOT AGREE TO THE TERMS OF A THIRD-PARTY SERVICE PROVIDER&#39;S PRIVACY POLICY, YOU SHOULD NOT USE THE RELATED THIRD-PARTY SERVICES. GENEMEDICSHEALTH HEREBY DISCLAIMS ALL RESPONSIBILITY AND LIABILITY FOR ANY OF YOUR INFORMATION COLLECTED OR USED BY THIRD-PARTY SERVICE PROVIDERS.</p>
						  </li>
						  <li>
							<h2>USER CONTENT</h2>
							<p>"<strong>User Content</strong>" is any content, materials or information (e.g., any text, information, photos, images, video, and other content and material, including nutritional information contributed to the Food Database) that You upload or post to, or transmit, display, perform or distribute by means of, the Services, whether in connection with Your use of Website, Application, or through the use of any Third Party Websites or Third Party Services or otherwise. You hereby grant GENEMEDICSHEALTH and its officers, directors, employees, agents, affiliates, representatives, service providers, partners, sublicensees, successors, and assigns (collectively, the "<strong>GENEMEDICSHEALTH Parties</strong>") a perpetual, fully paid-up, worldwide, sublicensable, irrevocable, assignable license to copy, distribute, publish, transmit, publicly display or perform, edit, modify, translate, reformat and otherwise use User Content in connection with the operation of the Services or any other similar services or related business, in any medium now existing or later devised, including without limitation in advertising and publicity. You further agree that this license includes the right for the GENEMEDICSHEALTH Parties to publish, display or otherwise use and make available your User Content and possibly your name and/or any user name of yours in connection with their exercise of the license granted under this section. You agree to waive, and hereby waive, any claims arising from or relating to the exercise by the GENEMEDICSHEALTH Parties of the rights granted under this section, including without limitation any claims relating to your rights of personal privacy and publicity. You will not be compensated for any exercise of the license granted under this section.</p>
							<p>You hereby represent and warrant that You own all rights, title and interest in and to User Content or are otherwise authorized to grant the rights provided the GENEMEDICSHEALTH Parties under this section. You represent and warrant that, when using the Website, Applications and Services, You will obey the law and respect the intellectual property rights of others. Your use of the Website and Services is at all times governed by and subject to laws regarding copyright ownership and use of intellectual property generally. You agree not to upload, post, transmit, display, perform or distribute any content, information or other materials in violation of any third party&#39;s copyrights, trademarks, or other intellectual property or proprietary rights. YOU SHALL BE SOLELY RESPONSIBLE FOR ANY VIOLATIONS OF ANY LAWS AND FOR ANY INFRINGEMENTS OF THIRD-PARTY RIGHTS CAUSED BY YOUR USE OF THE WEBSITE, APPLICATIONS AND SERVICES. YOUR BEAR THE SOLE BURDEN OF PROVING THAT CONTENT, INFORMATION OR OTHER MATERIALS DO NOT VIOLATE ANY LAWS OR THIRD-PARTY RIGHTS.</p>
							<p>GENEMEDICSHEALTH reserves the right to (i) remove, suspend, edit or modify any User Content in its sole discretion, including without limitation any User Content at any time, without notice to you and for any reason (including, but not limited to, upon receipt of claims or allegations from third parties or authorities relating to such User Content or if GENEMEDICSHEALTH is concerned that you may have violated these Terms of Use), or for no reason at all and (ii) to remove, suspend or block any User Content submissions. GENEMEDICSHEALTH also reserves the right to access, read, preserve, and disclose any information as GENEMEDICSHEALTH reasonably believes is necessary to (i) satisfy any applicable law, regulation, legal process or governmental request, (ii) enforce these Terms of Use, including investigation of potential violations hereof, (iii) detect, prevent, or otherwise address fraud, security or technical issues, (iv) respond to user support requests, or (v) protect the rights, property or safety of GENEMEDICSHEALTH, its users and the public.</p>
						  </li>

						  <li>
							<h2>YOUR RESPONSIBILITY FOR DEFAMATORY COMMENTS</h2>
							<p>You agree and understand that you may be held legally responsible for damages suffered by other members or third parties as the result of Your remarks, information, feedback or other content posted or made available on the Services that is deemed defamatory or otherwise legally actionable. Under the Federal Communications Decency Act of 1996, GENEMEDICSHEALTH is not legally responsible, nor can it be held liable for damages of any kind, arising out of or in connection to any defamatory or otherwise legally actionable remarks, information, feedback or other content posted or made available on the Services.</p>
						  </li>
						  <li>
							<h2>OBJECTIONABLE CONTENT</h2>
							<p>You represent and warrant that you shall not use the Services to upload, post, transmit, display, perform or distribute any content, information or materials that: (a) are libelous, defamatory, abusive, or threatening, excessively violent, harassing, obscene, lewd, lascivious, filthy, or pornographic; (b) constitute child pornography; (c) solicit personal information; (d) incite, encourage or threaten physical harm against another; (e) promote or glorify racial intolerance, use hate and/or racist terms, or signify hate towards any person or group of people; (f) glamorize the use of hard core illegal substances and drugs; (g) advertise or otherwise solicit funds or constitute a solicitation for goods or services; (h) violate any provision of this Agreement or any other GENEMEDICSHEALTH agreement, guidelines or policy; or (i) is generally offensive or in bad taste, as determined by GENEMEDICSHEALTH (collectively, "<strong>Objectionable Content</strong>"). GENEMEDICSHEALTH DISCLAIMS ANY PERCEIVED, IMPLIED OR ACTUAL DUTY TO MONITOR THE CONTENTS OF THE SERVICES AND SPECIFICALLY DISCLAIMS ANY RESPONSIBILITY OR LIABILITY FOR INFORMATION PROVIDED HEREON. Without limiting any of its other remedies, GENEMEDICSHEALTH reserves the right to terminate Your use of the Services or Your uploading, posting, transmission, display, performance or distribution of Objectionable Content. GENEMEDICSHEALTH, in its sole discretion, may delete any Objectionable Content from its servers. GENEMEDICSHEALTH intends to cooperate fully with any law enforcement officials or agencies in the investigation of any violation of this Agreement or of any applicable laws.</p>
						  </li>
						  <li>
							<h2>PROHIBITED USES</h2>
							<p>GENEMEDICSHEALTH imposes certain restrictions on Your use of the Services. You represent and warrant that you will not: (a) "stalk" or otherwise harass any person, or contact any person who has requested not to be contacted; (b) provide false, misleading or inaccurate information to GENEMEDICSHEALTH or any other member; (c) impersonate, or otherwise misrepresent affiliation, connection or association with, any person or entity; (d) create more than one unique public profile; (e) harvest or otherwise collect information about GENEMEDICSHEALTH users, including email addresses and phone numbers; (f) use or attempt to use any engine, software, tool, agent, or other device or mechanism (including without limitation browsers, spiders, robots, avatars, or intelligent agents) to harvest or otherwise collect information from the Website for any use, including without limitation use on third-party websites; (g) access content or data not intended for You, or log onto a server or account that You are not authorized to access; (h) attempt to probe, scan, or test the vulnerability of the Services, the Website, the Applications, or any associated system or network, or breach security or authentication measures without proper authorization; (i) interfere or attempt to interfere with the use of the Website, the Applications or the Services by any other user, host or network, including, without limitation by means of submitting a virus, overloading, "flooding," "spamming," "mail bombing," or "crashing"; (j) use the Services to send unsolicited e-mail, including without limitation promotions or advertisements for products or services; (k) forge any TCP/IP packet header or any part of the header information in any e-mail or in any uploading or posting to, or transmission, display, performance or distribution by means of, the Services; (l) post or transmit any unsolicited advertising, promotional materials, "junk mail", "spam," "chain letters," "pyramid schemes" or any other form of solicitation or any non-resume information such as opinions or notices, commercial or otherwise; or (m) attempt to modify, reverse-engineer, decompile, disassemble or otherwise reduce or attempt to reduce to a human-perceivable form any of the source code used by the GENEMEDICSHEALTH Parties in providing the Website or Applications. Any violation of this section may subject You to civil and/or criminal liability.</p>
						  </li>
						  <li>
							<h2>INTELLECTUAL PROPERTY</h2>
							<ol class="second">
							  <li>
								<h3>Software</h3>
								<p>You acknowledge and agree that the Applications and all intellectual property rights associated therewith are, and shall remain, the property of GENEMEDICSHEALTH. Furthermore, You acknowledge and agree that the source and object code of the Applications and the format, directories, queries, algorithms, structure and organization of the Applications are the intellectual property and proprietary and confidential information of GENEMEDICSHEALTH and its affiliates, licensors and suppliers. Except as expressly stated in this Agreement, You are not granted any intellectual property rights in or to the Applications by implication, estoppel or other legal theory, and all rights in and to the Applications not expressly granted in this Agreement are hereby reserved and retained by GENEMEDICSHEALTH.</p>
								<p>The Applications may utilize or include third party software that is subject to third party license terms ("Third Party Software"). You acknowledge and agree that Your right to use such Third Party Software as part of the Applications is subject to and governed by the terms and conditions of the third party license applicable to such Third Party Software. In the event of a conflict between the terms of this Agreement and the terms of such third party licenses, the terms of the third party licenses shall control with regard to Your use of the relevant Third Party Software.</p>
							  </li>
							  <li>
								<h3>Trademarks</h3>
								<p>GENEMEDICSHEALTH, GENEMEDICSHEALTH.com and the GENEMEDICSHEALTH logo (collectively, the "<strong>GENEMEDICSHEALTH Marks</strong>") are trademarks or registered trademarks of Genemedics Health Institute, P.C.. Other trademarks, service marks, graphics, logos and domain names appearing on the Services may be the trademarks of third-parties. Neither Your use of the Services, nor this Agreement, grant You any right, title or interest in or to, or any license to reproduce or otherwise use, the GENEMEDICSHEALTH Marks or any third-party trademarks, service marks, graphics, logos or domain names. You agree that any goodwill in the GENEMEDICSHEALTH Marks generated as a result of Your use of the Services will inure to the benefit of Genemedics Health Institute, P.C., and You agree to assign, and hereby do assign, all such goodwill to Genemedics Health Institute, P.C.. You shall not at any time, nor shall You assist others to, challenge Genemedics Health Institute, P.C.&#39;s right, title, or interest in or to, or the validity of, the GENEMEDICSHEALTH Marks.</p>
							  </li>
							  <li>
								<h3>Copyrighted Materials; Copyright Notice</h3>
								<p>All content and other materials available through the Website and Services, including without limitation the GENEMEDICSHEALTH logo, design, text, graphics, and other files, and the selection, arrangement and organization thereof, are either owned by Genemedics Health Institute, P.C. or are the property of GENEMEDICSHEALTH&#39;s licensors and suppliers. Except as explicitly provided, neither Your use of the Services nor this Agreement grant You any right, title or interest in or to any such materials.</p>
							  </li>
							  <li>
								<h3>DMCA Policy</h3>
								<p>If you have evidence, know, or have a good faith belief that your rights or the rights of a third party have been violated and you want GENEMEDICSHEALTH to delete, edit, or disable the material in question, you must provide GENEMEDICSHEALTH with all of the following information: (a) a physical or electronic signature of a person authorized to act on behalf of the owner of the exclusive right that is allegedly infringed; (b) identification of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works are covered by a single notification, a representative list of such works; (c) identification of the material that is claimed to be infringed or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and information reasonably sufficient to permit GENEMEDICSHEALTH to locate the material; (d) information reasonably sufficient to permit GENEMEDICSHEALTH to contact you, such as an address, telephone number, and if available, an electronic mail address at which you may be contacted; (e) a statement that you have a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law; and (f) a statement that the information in the notification is accurate, and under penalty of perjury, that you are authorized to act on behalf of the owner of an exclusive right that is allegedly infringed. For this notification to be effective, you must provide it to GENEMEDICSHEALTH&#39;s designated agent at:</p>
								<ul class="third agent">
								  <li>Attn: Copyright Agent</li>
								  <li>Genemedics Health Institute, P.C.</li>
								  <li>280 N Old Woodward Ave Ste 210 </li>
								  <li>Birmingham, MI 48009-5324</li>
								  <li>md@genemedics.com</li>
								</ul>
							  </li>
							</ol>
						  </li>
						  <li>
							<h2>DISCLAIMERS; LIMITATION OF LIABILITY</h2>
							<ol class="second">
							  <li>
								<h3>NO WARRANTIES.</h3>
								<p>GENEMEDICSHEALTH, ON BEHALF OF ITSELF AND ITS THIRD-PARTY SERVICE PROVIDERS, LICENSORS AND SUPPLIERS, HEREBY DISCLAIMS ALL WARRANTIES. THE WEBSITE, APPLICATIONS AND SERVICES ARE PROVIDED "AS IS" AND "AS AVAILABLE." TO THE MAXIMUM EXTENT PERMITTED BY LAW, GENEMEDICSHEALTH, ON BEHALF OF ITSELF AND ITS THIRD-PARTY SERVICE PROVIDERS, LICENSORS AND SUPPLIERS, EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED, REGARDING THE WEBSITE, INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. NEITHER GENEMEDICSHEALTH NOR ITS THIRD-PARTY SERVICE PROVIDERS, LICENSORS OR SUPPLIERS WARRANTS THAT THE WEBSITE, APPLICATIONS OR THE SERVICES WILL MEET YOUR REQUIREMENTS, THAT THE OPERATION OF THE WEBSITE, APPLICATIONS OR THE SERVICES WILL BE UNINTERRUPTED OR ERROR-FREE.</p>
							  </li>
							  <li>
								<h3>YOUR RESPONSIBILITY FOR LOSS OR DAMAGE; BACKUP OF DATA</h3>
								<p>YOU AGREE THAT YOUR USE OF THE WEBSITE, APPLICATIONS AND SERVICES IS AT YOUR SOLE RISK. YOU WILL NOT HOLD GENEMEDICSHEALTH OR ITS THIRD-PARTY SERVICE PROVIDERS, LICENSORS AND SUPPLIERS, AS APPLICABLE, RESPONSIBLE FOR ANY LOSS OR DAMAGE THAT RESULTS FROM YOUR ACCESS TO OR USE OF THE SERVICES, INCLUDING WITHOUT LIMITATION ANY LOSS OR DAMAGE TO ANY OF YOUR COMPUTERS OR DATA. THE WEBSITE, APPLICATIONS AND SERVICES MAY CONTAIN BUGS, ERRORS, PROBLEMS OR OTHER LIMITATIONS.</p>
								<p>IMPORTANTLY, YOU HEREBY ACKNOWLEDGE THAT A CATASTROPHIC SERVER FAILURE OR OTHER EVENT COULD RESULT IN THE LOSS OF ALL OF THE DATA RELATED TO YOUR ACCOUNT. YOU AGREE AND UNDERSTAND THAT IT IS YOUR RESPONSIBILITY TO BACKUP YOUR DATA TO YOUR PERSONAL COMPUTER OR EXTERNAL STORAGE DEVICE AND TO ENSURE SUCH BACKUPS ARE SECURE.</p>
							  </li>
							  <li>
								<h3>LIMITATION OF LIABILITY</h3>
								<p>THE LIABILITY OF GENEMEDICSHEALTH AND ITS THIRD-PARTY SERVICE PROVIDERS, LICENSORS AND SUPPLIERS IS LIMITED. TO THE MAXIMUM EXTENT PERMITTED BY LAW, IN NO EVENT SHALL GENEMEDICSHEALTH OR ITS THIRD-PARTY SERVICE PROVIDERS, LICENSORS OR SUPPLIERS BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, LOST PROFITS, LOST DATA OR CONFIDENTIAL OR OTHER INFORMATION, LOSS OF PRIVACY, COSTS OF PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES, FAILURE TO MEET ANY DUTY INCLUDING WITHOUT LIMITATION OF GOOD FAITH OR OF REASONABLE CARE, NEGLIGENCE, OR OTHERWISE, REGARDLESS OF THE FORESEEABILITY OF THOSE DAMAGES OR OF ANY ADVICE OR NOTICE GIVEN TO GENEMEDICSHEALTH OR ITS THIRD-PARTY SERVICE PROVIDERS, LICENSORS AND SUPPLIERS ARISING OUT OF OR IN CONNECTION WITH YOUR USE OF THE WEBSITE, APPLICATIONS OR SERVICES. THIS LIMITATION SHALL APPLY REGARDLESS OF WHETHER THE DAMAGES ARISE OUT OF BREACH OF CONTRACT, TORT, OR ANY OTHER LEGAL THEORY OR FORM OF ACTION. ADDITIONALLY, THE MAXIMUM LIABILITY OF GENEMEDICSHEALTH AND ITS THIRD-PARTY SERVICE PROVIDERS, LICENSORS AND SUPPLIERS TO YOU UNDER ALL CIRCUMSTANCES WILL BE $50.00. YOU AGREE THAT THIS LIMITATION OF LIABILITY REPRESENTS A REASONABLE ALLOCATION OF RISK AND IS A FUNDAMENTAL ELEMENT OF THE BASIS OF THE BARGAIN BETWEEN GENEMEDICSHEALTH AND YOU. THE WEBSITE, APPLICATIONS AND SERVICES WOULD NOT BE PROVIDED WITHOUT SUCH LIMITATIONS.</p>
							  </li>
							  <li>
								<h3>APPLICATION</h3>
								<p>THE ABOVE DISCLAIMERS, WAIVERS AND LIMITATIONS DO NOT IN ANY WAY LIMIT ANY OTHER DISCLAIMER OF WARRANTIES OR ANY OTHER LIMITATION OF LIABILITY IN THIS AGREEMENT, ANY OTHER AGREEMENT BETWEEN YOU AND GENEMEDICSHEALTH OR BETWEEN YOU AND ANY OF GENEMEDICSHEALTH&#39;S THIRD-PARTY SERVICE PROVIDERS, LICENSORS AND SUPPLIERS. SOME JURISDICTIONS MAY NOT ALLOW THE EXCLUSION OF CERTAIN IMPLIED WARRANTIES OR THE LIMITATION OF CERTAIN DAMAGES, SO SOME OF THE ABOVE DISCLAIMERS, WAIVERS AND LIMITATIONS OF LIABILITY MAY NOT APPLY TO YOU. UNLESS LIMITED OR MODIFIED BY APPLICABLE LAW, THE FOREGOING DISCLAIMERS, WAIVERS AND LIMITATIONS SHALL APPLY TO THE MAXIMUM EXTENT PERMITTED, EVEN IF ANY REMEDY FAILS ITS ESSENTIAL PURPOSE. GENEMEDICSHEALTH&#39;S THIRD-PARTY SERVICE PROVIDERS LICENSORS AND SUPPLIERS ARE INTENDED THIRD-PARTY BENEFICIARIES OF THESE DISCLAIMERS, WAIVERS AND LIMITATIONS. NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU THROUGH THE SERVICES OR OTHERWISE SHALL ALTER ANY OF THE DISCLAIMERS OR LIMITATIONS STATED IN THIS SECTION.</p>
							  </li>
							</ol>
						  </li>
						  <li>
							<h2>YOUR REPRESENTATIONS AND WARRANTIES</h2>
							<p>You represent and warrant that Your use of the Website, Applications and Services will be in accordance with this Agreement and any other GENEMEDICSHEALTH policies and guidelines, and with any applicable laws or regulations.</p>
						  </li>
						  <li>
							<h2>INDEMNITY BY YOU</h2>
							<p>Without limiting any indemnification provision of this Agreement, You agree to defend, indemnify and hold harmless GENEMEDICSHEALTH and its officers, directors, employees, agents, affiliates, representatives, sublicensees, successors, assigns, and Third-Party Service Providers (collectively, the "<strong>Indemnified Parties</strong>") from and against any and all claims, actions, demands, causes of action and other proceedings (collectively, "<strong>Claims</strong>"), including but not limited to legal costs and fees, arising out of or relating to: (i) Your breach of this Agreement, including without limitation any representation or warranty contained in this Agreement; (ii) Your access to or use of the Website, Applications or Services; (iii) Your provision to GENEMEDICSHEALTH or any of the Indemnified Parties of information or other data; (iv) Your violation or alleged violation of any foreign or domestic, federal, state or local law or regulation; or (v) Your violation or alleged violation of any third party&#39;s copyrights, trademarks, or other intellectual property or proprietary rights.</p>
							<p>The Indemnified Parties will have the right, but not the obligation, to participate through counsel of their choice in any defense by You of any Claim as to which You are required to defend, indemnify or hold harmless the Indemnified Parties. You may not settle any Claim without the prior written consent of the concerned Indemnified Parties.</p>
						  </li>
						  <li>
							<h2>GOVERNING LAW; JURISDICTION AND VENUE</h2>
							<p>This Agreement, including without limitation this Agreement's interpretation, shall be treated as though this Agreement were executed and performed in San Francisco, California and shall be governed by and construed in accordance with the laws of the State of California without regard to its conflict of law principles. ANY CAUSE OF ACTION BY YOU ARISING OUT OF OR RELATING TO THE SERVICES, OR THIS AGREEMENT MUST BE INSTITUTED WITHIN ONE (1) YEAR AFTER THE CAUSE OF ACTION AROSE OR BE FOREVER WAIVED AND BARRED. ALL ACTIONS SHALL BE SUBJECT TO THE LIMITATIONS SET FORTH IN ABOVE. The language in this Agreement shall be interpreted in accordance with its fair meaning and not strictly for or against either party.</p>
							<ol class="second">
							  <li>
								<h3>Requirement of Arbitration.</h3>
								<p>You agree that any dispute, of any nature whatsoever, between You and GENEMEDICSHEALTH arising out of or relating to this Agreement, shall be decided by neutral, binding arbitration before a representative of JAMS in San Francisco, California (unless You and GENEMEDICSHEALTH mutually agree to a different arbitrator), who shall render an award in accordance with the substantive laws of California and JAMS&#39; Streamlined Arbitration Rules &amp; Procedures. A final judgment or award by the arbitrator may then be duly entered and recorded by the prevailing party in the appropriate court as final judgment. The arbitrator shall award costs (including, without limitation, the JAMS&#39; fee and reasonable attorney&#39;s fees) to the prevailing party.</p>
							  </li>
							  <li>
								<h3>Remedies in Aid of Arbitration; Equitable Relief.</h3>
								<p>This agreement to arbitrate will not preclude You or GENEMEDICSHEALTH from seeking provisional remedies in aid of arbitration, including without limitation orders to stay a court action, compel arbitration or confirm an arbitral award, from a court of competent jurisdiction. Furthermore, this agreement to arbitrate will not preclude You or GENEMEDICSHEALTH from applying to a court of competent jurisdiction for a temporary restraining order, preliminary injunction, or other interim or conservatory relief, as necessary. THE PROPER VENUE FOR ANY ACTION PERMITTED UNDER THIS SUBSECTION REGARDING &quot;EQUITABLE RELIEF&quot; WILL BE THE FEDERAL AND STATE COURTS LOCATED IN SAN FRANCISCO, CALIFORNIA; THE PARTIES HEREBY WAIVE ANY OBJECTION TO THE VENUE AND PERSONAL JURISDICTION OF SUCH COURTS.</p>
							  </li>
							</ol>
						  </li>
						  <li>
							<h2>TERMINATION</h2>
							<ol class="second">
							  <li>
								<h3>Termination; Survival</h3>
								<p>Either party may terminate this Agreement and its rights hereunder at any time, for any or no reason at all, by providing to the other party notice of its intention to do so in accordance with this Agreement. This Agreement shall automatically terminate in the event that You breach any of this Agreement&#39;s representations, warranties or covenants. Such termination shall be automatic, and shall not require any action by GENEMEDICSHEALTH. Upon termination, all rights, licenses and obligations created by this Agreement will terminate, except that Sections 1-4, 6-13, 16-30 will survive any termination of this Agreement.</p>
							  </li>
							  <li>
								<h3>Effect of Termination</h3>
								<p>Any termination of this Agreement automatically terminates all rights and licenses granted to You under this Agreement, including all rights to use the Website, Applications and Services. Subsequent to termination, GENEMEDICSHEALTH reserves the right to exercise whatever means it deems necessary to prevent Your unauthorized use of the Website,, Applications and Services, including without limitation technological barriers such as IP blocking and direct contact with Your Internet Service Provider.</p>
							  </li>
							  <li>
								<h3>Legal Action</h3>
								<p>If GENEMEDICSHEALTH, in GENEMEDICSHEALTH&#39;s discretion, takes legal action against You in connection with any actual or suspected breach of this Agreement, GENEMEDICSHEALTH will be entitled to recover from You as part of such legal action, and You agree to pay, GENEMEDICSHEALTH&#39;s reasonable costs and attorneys&#39; fees incurred as a result of such legal action. The GENEMEDICSHEALTH Parties will have no legal obligation or other liability to You or to any third party arising out of or relating to any termination of this Agreement.</p>
							  </li>
							</ol>
						  </li>
						  <li>
							<h2>NOTICES</h2>
							<p>All notices required or permitted to be given under this Agreement must be in writing. GENEMEDICSHEALTH shall give any notice by email sent to the most recent email address, if any, provided by You to GENEMEDICSHEALTH. You agree that any notice received from GENEMEDICSHEALTH electronically satisfies any legal requirement that such notice be in writing. YOU BEAR THE SOLE RESPONSIBILITY OF ENSURING THAT YOUR EMAIL ADDRESS ON FILE WITH GENEMEDICSHEALTH IS ACCURATE AND CURRENT, AND NOTICE TO YOU SHALL BE DEEMED EFFECTIVE UPON THE SENDING BY GENEMEDICSHEALTH OF AN EMAIL TO THAT ADDRESS. You shall give any notice to GENEMEDICSHEALTH by means of: (1) U.S. mail, postage prepaid, to Genemedics Health Institute, P.C., 280 N Old Woodward Ave Ste 210 , Birmingham, MI 48009-5324; or (2) email to: md@genemedics.com Notice to GENEMEDICSHEALTH shall be effective upon receipt of notice by GENEMEDICSHEALTH.</p>
						  </li>
						  <li>
							<h2>GENERAL</h2>
							<p>This Agreement constitutes the entire agreement between GENEMEDICSHEALTH and You concerning Your use of the Services. This Agreement may only be modified by a written amendment signed by an authorized executive of GENEMEDICSHEALTH or by the unilateral amendment of this Agreement by GENEMEDICSHEALTH and by the posting by GENEMEDICSHEALTH of such amended version. If any part of this Agreement is held invalid or unenforceable, that part will be construed to reflect the parties&#39; original intent, and the remaining portions will remain in full force and effect. A waiver by either party of any term or condition of this Agreement or any breach thereof, in any one instance, will not waive such term or condition or any subsequent breach thereof. GENEMEDICSHEALTH may assign or transfer this Agreement at any time, with or without notice to You. This Agreement and all of Your rights and obligations hereunder will not be assignable or transferable by You without the prior written consent of GENEMEDICSHEALTH. This Agreement will be binding upon and will inure to the benefit of the parties, their successors and permitted assigns. You and GENEMEDICSHEALTH are independent contractors, and no agency, partnership, joint venture or employee-employer relationship is intended or created by this Agreement. Except for the GENEMEDICSHEALTH Parties and the Indemnified Parties as and to the extent set forth in Sections 18, 21, 25 and 28(c), and GENEMEDICSHEALTH&#39;s licensors and suppliers as and to the extent expressly set forth in Section 23, there are no third-party beneficiaries to this Agreement. You acknowledge and agree that any actual or threatened breach of this Agreement or infringement of proprietary or other third party rights by You would cause irreparable injury to GENEMEDICSHEALTH and GENEMEDICSHEALTH&#39;s licensors and suppliers, and would therefore entitle GENEMEDICSHEALTH or GENEMEDICSHEALTH&#39;s licensors or suppliers, as the case may be, to injunctive relief. The headings in this Agreement are for the purpose of convenience only and shall not limit, enlarge, or affect any of the covenants, terms, conditions or provisions of this Agreement.</p>
						  </li>
						</ol>
                        <span class="anchor" id="privacy"></span>
						<h1>GENEMEDICSHEALTH Privacy Policy</h1>
						<p>Genemedics Health Institute, P.C. (hereinafter, "<strong>GENEMEDICSHEALTH</strong>") is committed to respecting the privacy rights of users of the Website and GENEMEDICSHEALTH mobile phone applications (the "<strong>Applications</strong>"). GENEMEDICSHEALTH created this Privacy Policy to explain its information collection and use practices and the protection of Your information when You visit and use the Website and Applications. Any terms capitalized but not otherwise defined herein shall have the respective meanings set forth in the GENEMEDICSHEALTH <a href="#tou">Terms of Use</a>.</p>
						<p>This Privacy Policy is only applicable to the Website and Applications and does not apply to any websites or applications that are owned or operated by third parties ("<strong>Third-Party Websites/Applications</strong>"), which may have data collection, storage and use practices and policies that differ materially from this Privacy Policy. For additional information, see the section concerning Third-Party Websites/Applications, below.</p>
						<p>BY USING THE WEBSITE AND/OR APPLICATIONS, YOU REPRESENT AND WARRANT THAT YOU HAVE READ AND UNDERSTOOD, AND AGREE TO THE TERMS OF, THIS PRIVACY POLICY. IF YOU DO NOT UNDERSTAND OR DO NOT AGREE TO BE BOUND BY THIS PRIVACY POLICY, YOU MAY NOT USE THE WEBSITE AND/OR APPLICATION.</p>
						<ol class="first">
						  <li>
							<h2>INFORMATION COLLECTION PRACTICES</h2>
							<ol class="second">
							  <li>
								<p><strong>Traffic Data</strong>. GENEMEDICSHEALTH automatically gathers information of the sort that browsers automatically make available, including: (i) IP addresses; (ii) domain servers; (iii) types of computers accessing the Website; and (iv) types of Web browsers accessing the Website (collectively "<strong>Traffic Data</strong>"). Traffic Data is anonymous information that does not personally identify You.</p><p>
							  </p></li>
							  <li>
								  <p><strong>Cookies</strong>. A "<strong>Cookie</strong>" is a string of information that a website stores on a user&#39;s computer, and that the user's browser provides to the website each time the user submits a query to the website. The purpose of a Cookie is to identify the user as a unique user of the Website. GENEMEDICSHEALTH uses Cookies to customize Your experience on the Website to Your interests, to ensure that You do not see the same advertisements or informational messages repeatedly, and to store Your password so You do not have to re-enter it each time You visit the Website. In addition, GENEMEDICSHEALTH may use Cookies, and other methods, to store and retrieve data from Your web browser. For additional information on GENEMEDICSHEALTH&#39;s uses of Cookies, see the section concerning Information Use and Disclosure Practices/Traffic Data and Information Gathered Using Cookies, below.</p>
								  <p>IF YOU DO NOT WISH TO HAVE COOKIES PLACED ON YOUR COMPUTER, YOU SHOULD SET YOUR BROWSERS TO REFUSE COOKIES BEFORE ACCESSING THE WEBSITE, WITH THE UNDERSTANDING THAT CERTAIN OF THE SERVICES AND CERTAIN FEATURES OF THE WEBSITE MAY NOT FUNCTION PROPERLY WITHOUT THE AID OF COOKIES.</p>
							  </li>
							  <li>
								<p><strong>Web Beacons</strong>. "<strong>Web Beacons</strong>" (also known as clear gifs, pixel tags or web bugs) are tiny graphics with a unique identifier, similar in function to Cookies, and are used to track the online movements of web users or to access Cookies. Unlike Cookies which are stored on the user&#39;s computer hard drive, Web Beacons are embedded invisibly on web pages (or in e-mail). Web Beacons may be used to deliver or communicate with Cookies, to count users who have visited certain pages and to understand usage patterns. For additional information on GENEMEDICSHEALTH&#39;s uses of Web Beacons, see the section concerning Information Use and Disclosure Practices/Traffic Data and Information Gathered Using Cookies &amp; Web Beacons, below.</p>
							  </li>
							  <li>
								<p><strong>Mobile Device Data</strong>. When users download, install and use the Applications on their mobile phone or other wireless telecommunications device, GENEMEDICSHEALTH automatically gathers information, including without limitation, carrier providers, a unique device identifier, geo-location information (if allowed by the user), the types of mobile devices accessing the Application, and the types of operating systems accessing the Applications (collectively, "<strong>Mobile Device Data</strong>"). A unique device identifier is a string of alphanumeric characters (similar to a serial number) used to uniquely identify and distinguish each mobile phone or other wireless communications device. Location services can be enabled or disabled at anytime, through the mobile device settings. GENEMEDICSHEALTH uses Mobile Device Data to understand usage patterns and to improve the Applications. For additional information on GENEMEDICSHEALTH&#39;s uses of Mobile Device Data, see the section concerning Information Use and Disclosure Practices/Traffic Data and Information Gathered Using Cookies, Web Beacons, and Mobile Device Data, below.</p>
							  </li>
							  <li>
								<p><strong>Ad Serving and Usage Analytics</strong>. GENEMEDICSHEALTH uses third-parties for advertising and usage analytics for the Website and Applications. These third-parties may place Cookies on Your machine, use Web Beacons, gather Personal Information (as defined below), Traffic Data and Mobile Device Data, and log data to collect traffic and activity data in order to deliver relevant metrics, content, and advertising. We do not share your non-public Personal Information with these parties, but we may share anonymized or aggregated information with them to improve the relevancy of the ads you see at GENEMEDICSHEALTH. The collection of this information by third-parties is subject to such third-parties&#39; privacy policies. For additional information, see the section concerning Third-Party Websites/Applications, below. With regards to advertising on the Website, to learn more about behavioral advertising practices or to opt-out of this type of advertising, please email md@genemedics.com</p>
							  </li>
							  <li>
								<p><strong>Personal Information</strong>. You will be asked to provide GENEMEDICSHEALTH certain information related to You ("<strong>Personal Information</strong>"). Personal Information includes, without limitation: (1) "<strong>Contact Data</strong>" (personally identifiable information about You, such as Your name and email address, as well as Your friends and contacts, if you enable access to Your contacts and address book information); and (2) "<strong>Demographic Data</strong>" (personal information about You (but doesn&#39;t specifically identify You), such as Your gender, birthday, zip code, country, height, weight, lifestyle and exercise frequency); and (3) "<strong>Fitness Data</strong>" (information about Your use of the Services (but doesn&#39;t specifically identify You), such as Your caloric intake, nutritional statistics, fitness activity, and weight loss/gain). The Personal Information you provide is used for such purposes as allowing you to set up a user account and profile, providing the Website and/or Applications and related services, monitoring and improving the content and usage of the Website and/or Applications, interacting with the Website and/or Applications and with your friends and other members, to find and connect with Your friends and other members (when instructed by You), customizing the advertising and content you see, communicating with you about new features and other news, and any other purpose for which the information was collected. We may also draw upon this Personal Information in order to adapt the Website and/or Applications to your needs, to research the effectiveness of our Website and/or Applications, and to develop new tools for our members.</p>
								<p>You are under no obligation to provide Personal Information, however, not providing this information may prevent You from using certain features of the Services.</p>
							  </li>
							  <li>
								<p><strong>Third Party Services</strong>.  If you create an account using Your login/ID from a Third Party Service, like Facebook, GENEMEDICSHEALTH will use Your login/ID to access and collect the information that Your privacy settings on that Third Party Service permits GENEMEDICSHEALTH to access so that GENEMEDICSHEALTH can create Your account and connect You with Your friends.</p>
							  </li>
							  <li>
								<p><strong>Information You Make Public</strong>. Certain portions of the Website and Applications are open to any viewer, such as our community forums and Your personal profile. Any information You post in these locations may be available and accessible to other members of the Website. In addition, the Website contains features that permit You to upload, post, transmit, display, perform or distribute content, information or other data, including Your Personal Information. Any information that You choose to disclose by means of such features becomes public information. You should exercise caution when deciding to disclose Your Personal Information by means of such features, and You agree to assume all responsibility for doing so.</p>
							  </li>
							  <li>
								<p><strong>Transfer of HealthKit Data</strong>. You can choose to connect and share your GENEMEDICSHEALTH information with HealthKit and your HealthKit information with GENEMEDICSHEALTH. The information you provide to HealthKit is then governed by the Apple Terms and Conditions and Privacy Policy. The unique information you choose to send from HealthKit is not used by GENEMEDICSHEALTH for marketing and advertising or transferred by GENEMEDICSHEALTH to third parties for marketing and advertising.</p>
							  </li>
							</ol>
						  </li>
						  <li>
							<h2>INFORMATION USE AND DISCLOSURE PRACTICES</h2>
							<ol class="second">
							  <li>
								<h3>Traffic Data and Information Gathered Using Cookies, Web Beacons &amp; Mobile Device Data.</h3>
								<p>GENEMEDICSHEALTH analyzes Traffic Data and information gathered using Cookies, Web Beacons and Mobile Device Data to help GENEMEDICSHEALTH better understand who is using the Website and/or Applications and how they are using the Website and/or Applications. By identifying patterns and trends in usage, GENEMEDICSHEALTH is able to better design the Website and Applications to improve Your experience, and to serve You more relevant and interesting content and advertisements. From time to time, GENEMEDICSHEALTH may release Traffic Data and information gathered using Cookies, Web Beacons and Mobile Device Data in the aggregate, such as by publishing a report on trends in the usage of the Website. In addition, GENEMEDICSHEALTH may share Traffic Data and Mobile Device Data and information gathered using Cookies and Web Beacons with: (1) third-party analytics companies so that such third-parties may help GENEMEDICSHEALTH better understand who is using the Website and/or Applications and how they are using the Website and/or Applications; (2) advertisers and marketing partners so that such third-parties may provide You with advertisements tailored to Your interests; and (3) service providers so that we can personalize, provide and improve our Services to Your use and interests.</p>
							  </li>
							  <li>
								<h3>Personal Information</h3>
								<ol class="third personal">
								  <li>
									<p><strong>Contact Data</strong>. GENEMEDICSHEALTH uses Your Contact Data to send You information about GENEMEDICSHEALTH and GENEMEDICSHEALTH&#39;s products and services, and to contact You when necessary in connection with the Services. GENEMEDICSHEALTH uses Your Contact Data (when instructed by You) to connect You with Your friends and other members with whom You want to connect with, according to the preferences set in Your account.</p>
								  </li>
								  <li>
									<p><strong>Demographic Data</strong>. GENEMEDICSHEALTH uses Your Demographic Data to customize and tailor Your experience on the Website and/or Applications. As with Traffic Data and Mobile Device Data and information gathered using Cookies and Web Beacons, from time to time GENEMEDICSHEALTH may release or share Demographic Data with third parties in anonymous form and/or in the aggregate, for industry analysis, research, demographic profiling and other similar purposes. In addition, GENEMEDICSHEALTH shares anonymized Demographic Data with its advertisers and marketing partners so that such third-parties may provide You with advertisements tailored to Your interests. To opt-out of sharing your anonymized and Demographic Data with our advertising and marketing partners, please email md@genemedics.com</p>
								  </li>
								  <li>
									<p><strong>Fitness Data</strong>. GENEMEDICSHEALTH uses Your Fitness Data to provide, personalize and improve Your experience on the Website and/or Applications.  As with Demographic Data and other data gathered through the Service, GENEMEDICSHEALTH may release or share Fitness Data with third parties in anonymous form and/or in the aggregate, for statistical analysis, research, demographic profiling and other similar purposes. In addition, GENEMEDICSHEALTH may share your Fitness Data along with some of Your Demographic Data, in anonymous form and in the aggregate, with other GENEMEDICSHEALTH users so that You and other users can compare their own personal fitness, health and wellness situation relative to the entire GENEMEDICSHEALTH community.</p>
								  </li>
								</ol>
							  </li>
							  <li>
								<h3>Other Disclosure Practices</h3>
								<p>Except as otherwise provided in this Privacy Policy, and when You otherwise give permission or under the following circumstances, GENEMEDICSHEALTH will not share Your Personal Information with third parties.</p>
								<ul class="third disclosure">
								  <li>
									<p><strong>Disclosure in Connection with Services</strong>. GENEMEDICSHEALTH discloses Personal Information to those who help it provide services, including those who perform technical, administrative and data processing tasks such as hosting, billing, fulfillment, and data storage and security.</p>
								  </li>
								  <li>
									<p><strong>By Law or to Protect Rights</strong>. GENEMEDICSHEALTH discloses Personal Information when required to do so by law, or in response to a subpoena or court order, or when GENEMEDICSHEALTH believes in its sole discretion that disclosure is reasonably necessary to protect the property or rights of GENEMEDICSHEALTH, third-parties or the public at large.</p>
								  </li>
								  <li>
									<p><strong>Business Transfers; Bankruptcy</strong>. GENEMEDICSHEALTH reserves the right to transfer all Personal Information in its possession to a successor organization in the event of a merger, acquisition, or bankruptcy or other sale of all or a portion of GENEMEDICSHEALTH&#39; assets. Other than to the extent ordered by a bankruptcy or other court, the use and disclosure of all transferred Personal Information will be subject to this Privacy Policy, or to a new privacy policy if You are given notice of that new privacy policy and an opportunity to affirmatively opt-out of it. Personal Information submitted or collected after a transfer, however, may be subject to a new privacy policy adopted by GENEMEDICSHEALTH&#39;s successor organization.</p>
								  </li>
								</ul>
							  </li>
							</ol>
						  </li><li>
							<h2>EMAIL COMMUNICATIONS FROM GENEMEDICSHEALTH</h2>
							<p>In providing the Services, You may receive periodic email communications from GENEMEDICSHEALTH regarding the Services, such as new product offers and other information regarding the Services, which are part of the Services and which You cannot opt out of receiving. You may also receive periodic promotions and other offers or materials GENEMEDICSHEALTH believes might be of interest to You. You can opt-out of receiving these promotional messages at any time by (a) following the unsubscribe instructions contained in each message; or (b) changing the messaging preferences in Your account.</p>
						  </li>
						  <li>
							<h2>EMAIL COMMUNICATIONS FROM MEMBERS</h2>
							<p>By finding and connecting with Your friends and other members, You may receive electronic communications, including email and personal messages from other GENEMEDICSHEALTH members. You can manage the messages You receive from other members by changing the messaging preferences in Your account.</p>
						  </li>
						  <li>
							<h2>SECURITY OF PERSONAL INFORMATION</h2>
							<p>GENEMEDICSHEALTH has implemented and maintains reasonable security procedures and practices designed to protect against the unauthorized access, use, modification, destruction or disclosure of Your Personal Information, however, despite these efforts, no security measures are perfect or impenetrable and no method of data transmission can be guaranteed against any interception or other type of misuse. In the event that Your Personal Information is compromised as a result of a breach of security, GENEMEDICSHEALTH will promptly notify You if Your Personal Information has been compromised as required by applicable law.</p>
						  </li>
						  <li>
							<h2>USER ABILITY TO ACCESS, UPDATE, AND CORRECT PERSONAL INFORMATION</h2>
							<p>If You have an account, You can access and modify Your Personal Information through Your account, at any time.  If you completely delete all such information You will not be able to access or use the Website and/or Applications correctly.  If You would like GENEMEDICSHEALTH to delete Your Personal Information from its system, please contact us at md@genemedics.com with a request that we delete Your Personal Information from its database. GENEMEDICSHEALTH will use commercially reasonable efforts to honor Your request; however, GENEMEDICSHEALTH may retain an archived copy of Your records as required by law or for other legitimate business purposes.</p>
							<p>GENEMEDICSHEALTH wants Your Personal Information to be complete and accurate. By using the Website and/or Applications, You represent and warrant that all information You provide on any registration form or otherwise in connection with Your use of the Website and/or Applications and Services will be complete and accurate, and that You will update that information as necessary to maintain its completeness and accuracy. To confirm the completeness and accuracy of, or make changes to, Your Personal Information, please visit Your personal profile.</p>
						  </li>
						  <li>
							<h2>THIRD-PARTY WEBSITES/APPLICATIONS</h2>
							<p>The Website and Applications contain links to Third-Party Websites/Applications. GENEMEDICSHEALTH does not own or control these Third-Party Websites/Applications. GENEMEDICSHEALTH is not responsible for the privacy and security practices and policies of those Third-Party Websites/Applications and this Privacy Policy does not apply to any Third-Party Websites/Applications, except with respect to Personal Information provided directly to them by GENEMEDICSHEALTH. Before visiting or providing Personal Information to a Third-Party Website, You should inform Yourself of the privacy policies and practices (if any) of that Third-Party Website, and should take those steps necessary to, in Your discretion, protect Your privacy.</p>
						  </li>
						  <li>
							<h2>UPDATES AND CHANGES TO PRIVACY POLICY</h2>
							<p>GENEMEDICSHEALTH reserves the right to, update, change or modify this Privacy Policy. Any material changes to this policy will be posted on the Website and/or Applications (or You may be notified by email or other notification), and will indicate when such changes will become effective. You will be deemed to have agreed to any such modification or amendment by Your decision to continue using the Website and/or Application following the date in which the modified or amended Privacy Policy is posted on the Website. Use of information we collect now is subject to the Privacy Policy in effect at the time such information is used.</p>
						  </li>
						  <li>
							<h2>CHILDREN'S PRIVACY</h2>
							<p>GENEMEDICSHEALTH does not solicit or knowingly collect personally identifiable information from children under the age of 13. If GENEMEDICSHEALTH obtains actual knowledge that it has collected personally identifiable information from a child under the age of 13, GENEMEDICSHEALTH will promptly delete such information from its database.</p>
						  </li>
						  <li>
							<h2>INTERNATIONAL USERS</h2>
							<p>If you are located outside the United States and choose to provide Your information to GENEMEDICSHEALTH, Your information will be transferred to the United States and processed there (or any other country where GENEMEDICSHEALTH operates). By providing Your information and using the Website and/or Applications, You agree and consent to the collection, transfer, use, storage and disclosure of Your information as described in this Privacy Policy.</p>
						  </li>
						</ol>
						<h1>Effective Date: Dec 1st, 2015</h1>
					</div>
				</div>
			</div>
         </section>

         
             @if ($layout)
            <!-- footer-1 -->
            <footer class="footer-1 bg-midnight-blue">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="row social">
                                <ul >
                                    <li class="fb">
                                        <a target="_blank" rel="nofollow" href="https://www.facebook.com/Genemedics" title="Facebook" class="fui-facebook"></a>
                                    </li>
                                    <li class="tw">
                                        <a target="_blank" rel="nofollow" href="https://twitter.com/genemedics" title="Twitter" class="fui-twitter"></a>
                                    </li>
                                   <li class="googleplus">
                                        <a target="_blank" rel="nofollow" href="https://plus.google.com/+Genemedics/" title="GooglePlus" class="fui-googleplus"></a>
                                    </li>
                                    <li class="linkedin">
                                        <a target="_blank" rel="nofollow" href="https://www.linkedin.com/in/genemedics" title="Linkedin" class="fui-linkedin"></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="row">
                                <br/><br/>
                                <div class="">
                                <a class="btn btn-danger col-xs-12 col-sm-6" href="https://itunes.apple.com/us/app/genemedicshealth-health-tracker/id1061451973?mt=8">Download App</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">     
                        
                        </div>
                         @include('pages.contact_form')
                    </div>
                    <div class="row">
                        <br/>
                        <div class="additional-links">
                             <a href="/terms_and_privacy?layout=true#tou">Terms of Service</a>
                            <a href="/terms_and_privacy?layout=true#privacy">Privacy Policy</a>
                        </div>
                    </div>
                </div>
            </footer>
            @endif
        </div>
        

        <!-- Placed at the end of the document so the pages load faster -->
        {!! HTML::script("common-files/js/jquery-1.10.2.min.js") !!}
        {!! HTML::script("flat-ui/js/bootstrap.min.js") !!}
        {!! HTML::script("common-files/js/modernizr.custom.js") !!}
        {!! HTML::script("common-files/js/jquery.sharrre.min.js") !!}
        {!! HTML::script("common-files/js/startup-kit.js") !!}

        {!! HTML::script("js/jquery.validate.min.js") !!}
        {!! HTML::script("js/PriorityQueue.js") !!}
        {!! HTML::script("js/common.js") !!}
        {!! HTML::script("js/page.js") !!}
    </body>
</html>