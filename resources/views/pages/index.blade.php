<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Genemedics</title>
        <meta name="apple-mobile-web-app-capable" content="yes" />
       <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=no"/>
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="csrf-token" content="{!! csrf_token() !!}">
        <link rel="shortcut icon" href="/img/favicon.png">
        {!! HTML::style("flat-ui/bootstrap/css/bootstrap.css") !!}
        {!! HTML::style("flat-ui/css/flat-ui.css") !!}
        <!-- Using only with Flat-UI (free)-->
        {!! HTML::style("common-files/css/icon-font.css") !!}
        <!-- end -->
        {!! HTML::style("css/style_pages.css") !!}

        <style type="text/css">
            .error{
              color: red !important;
            }
            .carousel-control.left {
                background-image: none !important;
            }
            .carousel-control.right {
                background-image: none !important;
            }
        </style>
    </head>

    <body>
        <div class="page-wrapper">
            <!-- header-2 -->
            <header class="header-2">
                <div class="container">
                    <div class="row">
                        <div class="navbar col-sm-12 navbar-fixed-top" role="navigation">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle"></button>
                                <a class="brand" href="https://genemedicshealth.com/">
                                    {!! HTML::image('img/logo.png', 'Genemedics Logo', []) !!} </a>
                            </div>
                            <div class="collapse navbar-collapse">
                                <ul class="nav pull-right">
                                    <li>
                                        <a href="/" class="active text-center">Home</a>
                                    </li>
                                    <li>
                                        <a href="/why_us?layout=true" class="text-center">Why Us</a>
                                    </li>
                                    <li>
                                        <a href="#contact" class="text-center">Contact</a>
                                    </li>
									<li>
									<a href="{{URL::to('/auth0/#login')}}" class="text-center">Login</a>
									</li>
                                    <li>
                                     <a href="{{URL::to('/auth0/#signup')}}" class="text-center">Sign up</a>
                                    </li>
                                </ul>
								
								<ul class="subnav">
                                    <li>
                                        <a href="#">Privacy</a>
                                    </li>
                                    <li>
                                        <a href="#">Terms</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <section class="header-2-sub bg-midnight-blue">
                <div class="background">
                    &nbsp;
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <div class="hero-unit">
                                <h1>Lose Weight and be fit with Genemedics Health Tracker. Easy to use calorie
                                <br/>
                                counter and excercise entry. With a large food database of over 200,000 foods it is very easy to manage a health diary!</h1>
                            </div>
                            <div class="btns">
                                <a class="btn btn-danger" href="https://itunes.apple.com/us/app/genemedicshealth-health-tracker/id1061451973?mt=8">Download from Appstore</a>
                                <a class="btn btn-inverse" href="{{URL::to('/auth0/#signup')}}">Sign up</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- content-10  -->
            <section class="content-10 home">
                <div class="container">
                    <div class="row">
                           <div class="col-sm-6">
                            <h3 class="title">Genemedics Health Tracker</h3>
                            <p>
                             You have the app, we make sure you remain fit
                            </p>
                            <div class="row">
                                <div class="col-sm-6 info">
                                    <h6>Health Diary</h6>
                                   Manage food and exercise diary and stay on track with your weight goal
                                </div>
                                <div class="col-sm-6 info">
                                    <h6>Medication reminders</h6>
                                    Take your medicines on time and never miss a dose
                                </div>
								<div class="col-sm-6 info">
                                    <h6>Goal tracking</h6>
                                    Track your weight loss goal easily with a holistic approach 
                                </div>
                                <div class="col-sm-6 info">
                                    <h6>Professional video workouts</h6>
                                    Search from our huge workout videos library prepared by world class professionals and add it to your daily routine
                                </div>
								<div class="col-sm-6 info">
                                    <h6>Connectivity with devices</h6>
                                    Connect with Apple Watch, Fitbit, Withings and many many more
                                </div>
                                <div class="col-sm-6 info">
                                    <h6>Personalized Supplements</h6>
                                    Get advice for supplements tailored to your needs
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-4 col-sm-offset-2 text-center">
                            <div id="c-10_myCarousel" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active">
                                        {!! HTML::image('img_pages/mob_img.png', '', ['width' => '302','height' => '635']) !!}
                                    </div>
                                    <div class="item">
                                        {!! HTML::image('img_pages/mob_img2.png', '', ['width' => '302','height' => '635']) !!}
                                    </div>
                                    <div class="item">
                                        {!! HTML::image('img_pages/mob_img3.png', '', ['width' => '302','height' => '635']) !!}
                                    </div>
                                </div>
                                <a class="left carousel-control text-info" href="#c-10_myCarousel" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control text-info" href="#c-10_myCarousel" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                                <ol class="carousel-indicators">
                                    <li data-target="#c-10_myCarousel" data-slide-to="0" class="active"></li>
                                    <li data-target="#c-10_myCarousel" data-slide-to="1"></li>
                                    <li data-target="#c-10_myCarousel" data-slide-to="2"></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- logos -->
            <section class="logos">
                <div class="container">
                    <div class="col-sm-3 col-sm-offset-3">
                        <a href="http://www.genemedics.com/">
                        {!! HTML::image('img_pages/logos/Institute.png', 'Genemedics Health', ['width' => '200','height' => '35']) !!}</a></div>
                    <div class="col-sm-3">
                        <a href="https://genemedicsnutrition.com/">
                        {!! HTML::image('img_pages/logos/Nutrition.png', 'Genemedics Nutrition', ['width' => '200','height' => '35']) !!}</a></div>
                </div>
            </section>

            <!-- footer-1 -->
            <footer class="footer-1 bg-midnight-blue">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="row social">
                                <ul >
                                    <li class="fb">
                                        <a target="_blank" rel="nofollow" href="https://www.facebook.com/Genemedics" title="Facebook" class="fui-facebook"></a>
                                    </li>
                                    <li class="tw">
                                        <a target="_blank" rel="nofollow" href="https://twitter.com/genemedics" title="Twitter" class="fui-twitter"></a>
                                    </li>
                                   <li class="googleplus">
                                        <a target="_blank" rel="nofollow" href="https://plus.google.com/+Genemedics/" title="GooglePlus" class="fui-googleplus"></a>
                                    </li>
                                    <li class="linkedin">
                                        <a target="_blank" rel="nofollow" href="https://www.linkedin.com/in/genemedics" title="Linkedin" class="fui-linkedin"></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="row">
                                <br/><br/>
                                <div class="">
                                <a class="btn btn-danger col-xs-12 col-sm-6" href="https://itunes.apple.com/us/app/genemedicshealth-health-tracker/id1061451973?mt=8">Download App</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">     
                        
                        </div>
                          @include('pages.contact_form')
                    </div>
                    <div class="row">
                        <br/>
                        <div class="additional-links">
                            <a href="/terms_and_privacy?layout=true#tou">Terms of Service</a>
                            <a href="/terms_and_privacy?layout=true#privacy">Privacy Policy</a>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        

        <!-- Placed at the end of the document so the pages load faster -->
        {!! HTML::script("common-files/js/jquery-1.10.2.min.js") !!}
        {!! HTML::script("flat-ui/js/bootstrap.min.js") !!}
        {!! HTML::script("common-files/js/modernizr.custom.js") !!}
        {!! HTML::script("common-files/js/jquery.sharrre.min.js") !!}
        {!! HTML::script("common-files/js/startup-kit.js") !!}
        {!! HTML::script("js/jquery.validate.min.js") !!}
        {!! HTML::script("js/PriorityQueue.js") !!}
        {!! HTML::script("js/common.js") !!}
        {!! HTML::script("js/page.js") !!}

        
    </body>


</html>