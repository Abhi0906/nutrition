<div class="wrapper hidden-xs">
  <div class="top">
    <div class="container ">
      <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
        <ul id="myTab" class="nav nav-tabs nav-tabs-responsive mon-disp" role="tablist">
          <?php if((strpos(strtolower(Request::path()), strtolower('whyChooseUs')) === false) && (Request::path() != "/")){
            $accountActive = "active";
            $healthActive = "next";
          }else{
            $accountActive = "next";
            $healthActive = "active";
          }
          ?>
          <li role="presentation" class="<?php echo $accountActive;?>">
            <a href="https://genemedicsnutrition.com/patient/account/index/" role="tab" id="home-tab" aria-expanded="true" aria-controls="home" class="top-tab nav-btn-hover">
              <span class="text">GENEMEDICS ACCOUNT</span>
            </a>
          </li>

          <li role="presentation" class="<?php echo $healthActive;?>">
            <a href="https://genemedicshealth.com" role="tab" id="home-tab" aria-expanded="true" aria-controls="home" aria-expanded="true" class="top-tab nav-btn-hover">
              <span class="text">HEALTH APP</span>
            </a>
          </li>

          <li role="presentation">
            <a href="https://genemedicsnutrition.com/" role="tab" id="profile-tab" aria-controls="profile" class="top-tab nav-btn-hover">
              <span class="text">NUTRITION SUPPLEMENTS</span>
            </a>
          </li>
          <li role="presentation">
            <a href="https://www.genemedics.com/" id="samsa-tab" role="tab" aria-controls="samsa" class="top-tab nav-btn-hover">
              <span class="text">HORMONE REPLACEMENT THERAPY</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div id="myTabContent" class="tab-content">
  <div role="tabpanel" class="tab-pane fade " id="home" aria-labelledby="home-tab">
      <p>
          GENEMEDICS APP
      </p>
  </div>
  <div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">
      <p>
          GENEMEDICS NUTRITION
      </p>
  </div>

  <!-- Top Tabs -->
  <div role="tabpanel" class="tab-pane fade in active" id="samsa" aria-labelledby="samsa-tab">
      <!-- Header start here -->
      <header id="header">
          <div id="trueHeader">
              <div class="wrapper hidden-xs ">
                  <div class="container">
                      <div class="row">
                          <div class="col-lg-6 col-sm-4 pull-left"><div class="logo"><a href="{{ url('/') }}"><img id="scroll-logo" src="images/scroll-logo.png" alt="Genemedics Health Institute" title=""></a></div></div>
                          <!--<div class="col-lg-3 col-sm-4 col-xs-10 padd-30">
                              <div class="select-box">
                                  <select id="location" class="selectbox1">
                                      <option class="option-drop">Find Your Location</option>
                                      <option class="option-drop">option</option>
                                      <option class="option-drop">option2</option>
                                  </select>
                              </div>
                          </div>-->
                          <div class="col-lg-3 col-sm-4 padd-30">
                              <a href="tel:800-277-4041" class="tel-email top-link">800-277-4041</a>&nbsp;&nbsp;
                          </div>
                          <div class="col-lg-3 col-sm-4 padd-30 pull-right">
                              @if (Route::has('login'))
                                  <span class="pull-right ">
                                      @auth
                                          <a href="{{ url('/dashboard') }}" class="top-link">Welcome <?php echo \Auth::user()->first_name ?></a><span class="top-line-des"> / </span><a href="{{ route('logout') }}" class="top-link">Logout</a>
                                      @else
                                          <a href="{{ route('login') }}" class="top-link">Log-In</a><span class="top-line-des"> / </span><a href="{{ route('login') }}" class="top-link">Sign-Up</a>
                                      @endauth
                                  </span>
                              @endif
                              <!-- <span class="pull-right ">
                                  <?php if(\Auth::check()):?>
                                    <a href="{{ url('/dashboard') }}" class="top-link">Welcome <?php echo \Auth::user()->first_name ?></a><span class="top-line-des"> / </span><a href="{{ url('/logout') }}" class="top-link">Logout</a>
                                  <?php else:?>
                                    <a href="{{ url('/login') }}" class="top-link">Log-In</a><span class="top-line-des"> / </span><a href="{{ url('/auth/signup') }}" class="top-link">Sign-Up</a>
                                  <?php endif; ?>
                              </span>
                              -->
                          </div>
                      </div><!-- /.container -->
                  </div>
                  <!-- Header over here -->
                  <!-- nav start here -->
                  <div class="wrapper-nav">
                    <div class="container">
                      <div class="row">
                  		  <nav id="site-navigation" class="navbar white navbar-default" role="navigation" aria-label="Primary Menu">
                  				<div class="container-fluid">
                  				  <ul id="menu-header-menu" class="nav navbar-nav">
                              <?php if((strpos(strtolower(Request::path()), strtolower('whyChooseUs')) === false) && (Request::path() != "/")):?>
                                <li id="menu-item-245" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-245">
                                  <a href="https://genemedicsnutrition.com/patient/account/index">MY ACCOUNT</a>
                                </li>
                                <li id="menu-item-3428" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3428">
                  							  <a href="https://genemedicsnutrition.com/patient/info/index">PATIENT INFO</a>
                  						  </li>
                              <?php else:?>
                                <li id="menu-item-245" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-245">
                                  <a href="{{ url('/') }}">HOME</a>
                                </li>
                                <li id="menu-item-3428" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3428">
                  							  <a href="{{ url('/whyChooseUs') }}">WHY CHOOSE US</a>
                  						  </li>
                              <?php endif;?>
                              <?php if(\Auth::check()):?>
                              <?php if((strpos(strtolower(Request::path()), strtolower('whyChooseUs')) === false) && (Request::path() != "/")):?>
                              <li id="menu-item-3282" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3282:">
                  							<a href="{{ url('/dashboard') }}">DASHBOARD</a>
                  						</li>
                              <li id="menu-item-2798" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2798">
                                <a href="{{ url('/diary') }}">DIARY</a>
                              </li>
                              <li id="menu-item-3248" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3248">
                                <a href="{{ url('/workout_routines') }}">WORKOUTS</a>
                              </li>
                              <li id="menu-item-3232" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3232">
                  						  <a href="{{ url('/goals') }}">GOALS</a>
                  						</li>
                              <li id="menu-item-2232" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2232" style="display:none">
                                <a href="" id="dynamicLI"></a>
                              </li>
                              <li id="menu-item-2796" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2796 dropdown">
                                <a target="_blank" href="#" class="dropdown-toggle" data-toggle="dropdown">MORE...</a>
                                <ul  class="sub-menu dropdown-menu">
                                <li id="menu-item-32822" class="mainele menu-item menu-item-type-post_type menu-item-object-page menu-item-32822">
                    						  <a href="{{ url('/weight') }}">WEIGHT</a>
                    						</li>
                                <li id="menu-item-324" class="mainele menu-item menu-item-type-post_type menu-item-object-page menu-item-324">
                    							<a href="{{ url('/sleep') }}">SLEEP</a>
                    						</li>
                    						<li id="menu-item-529" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-529">
                    						  <a href="{{ url('/bp_pulse') }}">BP & PULSE</a>
                    						</li>
                                <li id="menu-item-1206" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1206">
                    							<a href="{{ url('/reports') }}">REPORTS</a>
                    						</li>

                                <li id="menu-item-530" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-530">
                    							<a href="{{ url('/deals_list_front') }}">DEALS</a>
                    						</li>
                                <li id="menu-item-525" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-525">
                    							<a href="{{ url('/challenges_list_front') }}">CHALLENGES</a>
                    						</li>
                    						<li id="menu-item-526" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-526">
                    						  <a href="{{ url('/event') }}">EVENTS</a>
                    						</li>
                                <li id="menu-item-528" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-528">
                    							  <a href="{{ url('/my_plans') }}">MY PLANS</a>
                    						</li>
                                <?php endif; ?>
                                <?php endif; ?>
                                </ul>
                              </li>
                  					</ul>
                  				</div>
                  			</nav><!-- .main-navigation -->
                      </div>
                    </div>
                  </div>
                </div>
                <div class="wrapper hidden-lg hidden-sm hidden-md">
                  <div id="wrapper">
                    <a href="{{ url('/') }}"><img id="scroll-logo" src="images/scroll-logo.png" alt="Genemedics Health Institute" title=""></a>
                    <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
                      <button type="button" class="menu-toggle hamburger is-closed" data-toggle="offcanvas">
                        <span class="hamb-top"></span>
                        <span class="hamb-middle"></span>
                        <span class="hamb-bottom"></span>
                      </button>
                      <ul id="menu-mobile-menu" class="nav sidebar-nav mob">
                        <li class="mobmenu-logo">
                          <a href="{{ url('/') }}"><img id="scroll-logo" src="images/scroll-logo.png" alt="Genemedics Health Institute" title=""></a>
                        </li>
                        <li id="menu-item-245" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-245">
                          <a href="{{ url('/') }}">HOME</a>
                        </li>
                        <li id="menu-item-3428" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3428">
                          <a href="{{ url('/whyChooseUs') }}">WHY CHOOSE US</a>
                        </li>
                        <?php if(\Auth::check()):?>
                        <li id="menu-item-3282" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3282">
                          <a href="{{ url('/dashboard') }}">DASHBOARD</a>
                        </li>
                        <li id="menu-item-2798" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2798">
                          <a href="{{ url('/diary') }}">DIARY</a>
                        </li>
                        <li id="menu-item-3248" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3248">
                          <a href="{{ url('/workout_routines') }}">WORKOUTS</a>
                        </li>
                        <li id="menu-item-3232" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3232">
                          <a href="{{ url('/goals') }}">GOALS</a>
                        </li>
                        <li id="menu-item-2796" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2796 dropdown">
                          <a href="" class="dropdown-toggle" data-toggle="dropdown">MORE...</a>
                          <span class="arrow"></span>
                          <ul class="sub-menu dropdown-menu">
                          <li id="menu-item-32822" class="mainele menu-item menu-item-type-post_type menu-item-object-page menu-item-32822">
                            <a href="{{ url('/weight') }}">WEIGHT</a>
                          </li>
                          <li id="menu-item-324" class="mainele menu-item menu-item-type-post_type menu-item-object-page menu-item-324">
                            <a href="{{ url('/sleep') }}">SLEEP</a>
                          </li>
                          <li id="menu-item-529" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-529">
                            <a href="{{ url('/bp_pulse') }}">BP & PULSE</a>
                          </li>
                          <li id="menu-item-1206" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1206">
                            <a href="{{ url('/reports') }}">REPORTS</a>
                          </li>

                          <li id="menu-item-530" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-530">
                            <a href="{{ url('/deals_list_front') }}">DEALS</a>
                          </li>
                          <li id="menu-item-525" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-525">
                            <a href="{{ url('/challenges_list_front') }}">CHALLENGES</a>
                          </li>
                          <li id="menu-item-526" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-526">
                            <a href="{{ url('/event') }}">EVENTS</a>
                          </li>
                          <li id="menu-item-528" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-528">
                              <a href="{{ url('/my_plans') }}">MY PLANS</a>
                          </li>
                          <?php endif; ?>
                          </ul>
                        </li>
                        <li class="nav-link-num1">
                          <a href="tel:800-277-4041" class="tel-email top-link">800-277-4041</a>
                          <span class="pull-right">
                            <?php if(\Auth::check()):?>
                              <a href="{{ url('/dashboard') }}" class="top-link">Welcome <?php echo \Auth::user()->first_name ?></a><span class="top-line-des"> / </span><a href="{{ url('/auth0/logout') }}" class="top-link">Logout</a>
                            <?php else:?>
                              <a href="{{ url('/auth/login') }}" class="top-link">Log-In</a><span class="top-line-des"> / </span><a href="{{ url('/auth/signup') }}" class="top-link">Sign-Up</a>
                            <?php endif; ?>
                          </span>
                        </li>
                        <li><a class="nav-link nav-l1 tab-mob-link" href="https://www.genemedicshealth.com/">GENEMEDICS HEALTH INSTITUTE</a></li>
                        <li><a class="nav-link tab-mob-link" href="https://genemedicsnutrition.com/">GENEMEDICS NUTRITION</a></li>
                        <li><a class="nav-link tab-mob-link" href="https://genemedics.com">GENEMEDICS APP</a></li>
                      </ul>
                    </nav>
                  </div>
                  <div class="row">
                    <div class="col-md-6 col-xs-6 pull-left text-center ">
                      <select class="selectbox1 find-loction">
                        <option class="option-drop">FIND YOUR LOCTION</option>
                        <option class="option-drop">option</option>
                        <option class="option-drop">option2</option>
                      </select>
                    </div>
                    <div class="col-md-6 col-xs-6 nav-btn1 pull-right text-center">
                      <a href="#">FREE CONSULTATION</a>
                    </div>
                  </div>
                </div>
            </div>
      </header>
  </div>
</div>
@section('js')
{!! HTML::script("js/controllers/DashboardController.js") !!}
@append
