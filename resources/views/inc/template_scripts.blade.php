<?php
/**
 * template_scripts.php
 *
 * Author: pixelcave
 *
 * All vital JS scripts are included here
 *
 */

?>
<script type="text/javascript">
	var Auth0Domain ="{{{config('auth0.domain')}}}";
	var Auth0ClientID ="{{{config('auth0.clientID')}}}";
	var Auth0Connection ="{{{config('auth0.connection')}}}";
	var Auth0CallbackUrl ="{{{config('laravel-auth0.redirect_uri')}}}";
</script>

<!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->

{!! HTML::script("js/vendor/bootstrap.min.js") !!}

{!! HTML::script("js/ng-tags-input.min.js") !!}
{!! HTML::script("js/angular-cookies.min.js") !!}


{!! HTML::script("js/plugins.js") !!}
<!--  <script src="//cdn.auth0.com/w2/auth0-6.7.js"></script> -->

 <script src="https://cdn.auth0.com/js/auth0/9.5.1/auth0.min.js"></script>

{!! HTML::script("js/app.js") !!}
{!! HTML::script("js/app.core.js") !!}
{!! HTML::script("js/app.routes.js") !!}
{!! HTML::script("js/app.services.js") !!}

{!! HTML::script("js/PriorityQueue.js") !!}
{!! HTML::script("js/common.js") !!}
{!! HTML::script("js/sweetalert-dev.js") !!}
{!! HTML::script("js/typeahead.bundle.min.js") !!}

<!-- {!! HTML::script("js/jquery-gauge.min.js") !!}
{!! HTML::script("js/guage-meter-genemedics.js") !!} -->
{!! HTML::script("js/bootstrap-datepicker.js") !!}
<!-- {!! HTML::script("js/amcharts.js") !!}
{!! HTML::script("js/light.js") !!}
{!! HTML::script("js/serial.js") !!}
{!! HTML::script("js/mainchart.js") !!} -->
{!! HTML::script("js/datetime-picker.min.js") !!}

  <!-- Services -->
{!! HTML::script("js/services/getData.fct.js") !!}
{!! HTML::script("js/services/storeData.fct.js") !!}
{!! HTML::script("js/services/utility.fct.js") !!}
  <!-- Controllers -->

{!! HTML::script("js/controllers/UserController.js") !!}

<!-- Directives -->
{!! HTML::script("js/directives/elif.js") !!}
{!! HTML::script("js/directives/angular-drag-and-drop-lists.js") !!}
{!! HTML::script("js/directives/configureBlock.js") !!}
{!! HTML::script("js/directives/interactiveBlock.js") !!}
{!! HTML::script("js/directives/singleVideo.js") !!}
{!! HTML::script("js/directives/checklist-model.js") !!}
{!! HTML::script("js/directives/slimScroll.js") !!}
{!! HTML::script("js/directives/weekdaySchedule.js") !!}
{!! HTML::script("js/directives/geneTabs.js") !!}
{!! HTML::script("js/directives/slider.js") !!}

{!! HTML::script("js/angular-ui-router.min.js") !!}

{!! HTML::script("js/directives/numbersOnly.js") !!}
{!! HTML::script("js/directives/numberValidation.js") !!}

{!! HTML::script("js/directives/calendar.js") !!}


<!-- {!! HTML::script("js/directives/dirPagination.js") !!} -->
  @if (Auth::check() && Auth::User()->user_type == 'patient')
<script type="text/javascript">


	/*var auth0 = new Auth0({
	    domain:       Auth0Domain,
	    clientID:     Auth0ClientID,
	    callbackURL:  Auth0CallbackUrl,
	    callbackOnLocationHash: true
	  });

	function checkSSOAuth(){

	    auth0.getSSOData(function(err, data) {
	        // if there is still a session, do nothing
	        console.log(data);
	        if (err || (data && data.sso)) return;

	        // if we get here, it means there is no session on Auth0,
	        // then remove the token and redirect to #login

	        window.location.href = '/auth0/logout';

	    });
	}


	checkSSOAuth();*/
	/*setInterval(function() {
	    checkSSOAuth();
	}, 5000);*/

</script>
@endif
