<nav>
	  <div class="navigation">
  		<ul id="MainMenu">
        <li><a href="{{URL::to('/')}}" class="homemenu">HOME</a></li>
        <!-- <li><span class="menuline hidden-sm hidden-xs hidden-md">|</span></li> -->
        <li>
        
        {!! HTML::image('images/line.png', '', ['class' => 'menuline-img visible-lg']) !!}
        </li>
    		<li class="opendrop1"><a href="javascript:void(0);">WHY CHOOSE US</a>
          <span> {!! HTML::image('images/dropdown.png', '', ['class' => 'icondown']) !!}
          </span>
          <span class="glyphicon formenudownicon glyphicon-chevron-down hidden-lg"></span>
          <ul class="dropdown drop1">
                <li><a href="">Menuitem 1</a></li>
                <li><a href="">Menuitem 2</a></li>
                <li><a href="">Menuitem 3</a></li>     
          </ul>
        </li>
    		<li>{!! HTML::image('images/line.png', '', ['class' => 'menuline-img visible-lg']) !!}
</li>
    		<li class="opendrop2"><a href="javascript:void(0);">MEN'S HORMONES</a><span> {!! HTML::image('images/dropdown.png', '', ['class' => 'icondown']) !!}</span>
            <span class="glyphicon formenudownicon glyphicon-chevron-down hidden-lg"></span>
            <ul class="dropdown drop2">
                <li><a href="">Menuitem 1</a></li>
                <li><a href="">Menuitem 2</a></li>
                <li><a href="">Menuitem 3</a></li>     
            </ul>
        </li>
    		<li>        {!! HTML::image('images/line.png', '', ['class' => 'menuline-img visible-lg']) !!}
</li>
    	   <li class="opendrop3"><a href="javascript:void(0);" tabindex="1">WOMNEN'S HORMONES</a><span> {!! HTML::image('images/dropdown.png', '', ['class' => 'icondown']) !!}</span><span class="glyphicon formenudownicon glyphicon-chevron-down hidden-lg"></span>
              <ul class="dropdown drop3">
        				<li><a href="">Services - 1</a></li>
        				<li><a href="">Services - 2</a></li>
        				<li><a href="">Services - 3</a></li>		 
      				</ul>
    		</li>
    		<li>        {!! HTML::image('images/line.png', '', ['class' => 'menuline-img visible-lg']) !!}
</li>
    		<li class="opendrop4"><a href="javascript:void(0);" tabindex="1">BIODENTICAL HORMONES</a><span>  {!! HTML::image('images/dropdown.png', '', ['class' => 'icondown']) !!}</span>
          <span class="glyphicon formenudownicon glyphicon-chevron-down hidden-lg"></span>
      			<ul class="dropdown drop4">
        			<li><a href="">Works - 1</a></li>
        			<li><a href="">Works - 2</a></li>
        			<li><a href="">Works - 3</a></li>
      			</ul>
    		</li>
    		<li>        {!! HTML::image('images/line.png', '', ['class' => 'menuline-img visible-lg']) !!}
</li>
    		<li class="opendrop5"><a href="javascript:void(0);">ANTI-AGIN SERVICES</a><span> {!! HTML::image('images/dropdown.png', '', ['class' => 'icondown']) !!}</span>
          <span class="glyphicon formenudownicon glyphicon-chevron-down hidden-lg"></span>
          <ul class="dropdown drop5">
                <li><a href="">Menuitem 1</a></li>
                <li><a href="">Menuitem 2</a></li>
                <li><a href="">Menuitem 3</a></li>     
            </ul>
        </li>
    		<li>        {!! HTML::image('images/line.png', '', ['class' => 'menuline-img visible-lg']) !!}
</li>
    		<li class="opendrop6"><a href="javascript:void(0);">SUCCESS STORIES</a><span> {!! HTML::image('images/dropdown.png', '', ['class' => 'icondown']) !!}</span>
          <span class="glyphicon formenudownicon glyphicon-chevron-down hidden-lg"></span>
          <ul class="dropdown drop6">
                <li><a href="">Menuitem 1</a></li>
                <li><a href="">Menuitem 2</a></li>
                <li><a href="">Menuitem 3</a></li>     
            </ul>
        </li>
    		<li>        {!! HTML::image('images/line.png', '', ['class' => 'menuline-img visible-lg']) !!}
</li>
    		<li><a href="javascript:void(0);" class="contactmenu">CONTACT US</a></span></li>
         @if (Auth::check())
            <li><a href="{{URL::to('/'.auth()->user()->user_type.'/'.auth()->user()->id.'/demographics')}}" class="contactmenu hidden-lg">PROFILE</a></span></li>
            <li><a href="{{URL::to('/auth0/logout')}}" class="contactmenu hidden-lg">LOG OUT</a></span></li>
  		   @endif
      </ul>
		</div>
		<div class="nav_bg">
  		<div class="nav_bar"> <span></span> <span></span> <span></span></div>
		</div>
</nav>
