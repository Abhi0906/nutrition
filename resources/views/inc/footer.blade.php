
 <?php /* <div class="footer-top-menu">
    <div class="container-fluid">
      <div class="footer-top-wrap">
        <div class="row">
          <div class="col-xs-12 col-lg-6">
            <div class="row">
              <div class="col-xs-12 col-sm-4">
                <h4>PATIENT FORMS</h4>
              </div>
                          <div class="col-xs-12 col-sm-8 side-border-left">
                <h4>CONNECT WITH US:</h4>
                <div class="medias">
                  <ul class="list-inline list-unstyled">
                                      <li><a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                      <li><a href="https://www.twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                      <li><a href="https://plus.google.com/collections/featured" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                      <li><a href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube"></i></a></li>
                                      <li>
                      <div id="share-buttons">
                        <img src="/images/share-btn.png" alt="share">
                        <div class="cont">
                          <!-- Email -->
                          <a href="mailto:?Subject=Genemedics&amp;Body=I%20saw%20this%20and%20thought%20of%20you!%20 http://demowebsite1.net/genemedics/health-wellness-app/log-in-sign-up/">
                            Mail
                          </a>
                          <!-- Facebook -->
                          <a href="http://www.facebook.com/sharer.php?u=http://demowebsite1.net/genemedics/health-wellness-app/log-in-sign-up/" target="_blank">
                            Facebook
                          </a>
                          <!-- Google+ -->
                          <a href="https://plus.google.com/share?url=http://demowebsite1.net/genemedics/health-wellness-app/log-in-sign-up/" target="_blank">
                            google+
                          </a>
                          <!-- LinkedIn -->
                          <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://demowebsite1.net/genemedics/health-wellness-app/log-in-sign-up/" target="_blank">
                            linkedin
                          </a>
                          <!-- Reddit -->
                          <a href="http://reddit.com/submit?url=http://demowebsite1.net/genemedics/health-wellness-app/log-in-sign-up/&amp;title=Genemedics" target="_blank">
                            reddit
                          </a>
                          <!-- Tumblr-->
                          <a href="http://www.tumblr.com/share/link?url=http://demowebsite1.net/genemedics/health-wellness-app/log-in-sign-up/&amp;title=Genemedics" target="_blank">
                            tumblr
                          </a>
                          <!-- Twitter -->
                          <a href="https://twitter.com/share?url=http://demowebsite1.net/genemedics/health-wellness-app/log-in-sign-up/&amp;text=Genemedics&amp;hashtags=genemedics" target="_blank">
                            twitter
                          </a>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
                        </div>
          </div>
          <div class="col-xs-12 col-lg-6 side-border-left">
            <div class="row">
              <div class="col-xs-12 col-sm-4">
                <h4>PARTNER WITH US:</h4>
              </div>
              <div class="col-xs-12 col-sm-8">
                <ul id="footer-top" class="list-unstyled list-inline"><li id="menu-item-2820" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2820"><a href="#">Affiliate Partner</a></li>
                  <li id="menu-item-2821" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2821"><a href="#">Sales</a></li>
                  <li id="menu-item-2822" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2822"><a href="#">Health Care Practitioner</a></li>
                  </ul>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> */ ?>

<div class="footer-top-menu">
  <div class="container-fluid">
    <div class="footer-top-wrap">
      <div class="row">
            <div class="col-xs-12 col-lg-6">
                <div class="row">
                    <!-- <div class="col-xs-12 col-sm-4">
                      <h4>PATIENT FORMS</h4>
                    </div> -->
                        <div class="col-xs-12 col-md-7 col-sm-8 col-xl-12 col-lg-12"> <!-- side-border-left -->
                              <h4>CONNECT WITH US:</h4>
                              <div class="medias">
                                      <ul class="list-inline list-unstyled">
                                                          <li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                                          <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                                          <li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                                          <li><a href="#" target="_blank"><i class="fa fa-youtube"></i></a></li>
                                                          <li>
                                                        <div id="share-buttons">
                                                          <img src="/images/share-btn.png" alt="share">
                                                          <div class="cont">
                                                            <!-- Email -->
                                                            <a href="#">
                                                              Mail
                                                            </a>
                                                            <!-- Facebook -->
                                                            <a href="#" target="_blank">
                                                              Facebook
                                                            </a>
                                                            <!-- Google+ -->
                                                            <a href="#" target="_blank">
                                                              google+
                                                            </a>
                                                            <!-- LinkedIn -->
                                                            <a href="#" target="_blank">
                                                              linkedin
                                                            </a>
                                                            <!-- Reddit -->
                                                            <a href="#" target="_blank">
                                                              reddit
                                                            </a>
                                                            <!-- Tumblr-->
                                                            <a href="#" target="_blank">
                                                              tumblr
                                                            </a>
                                                            <!-- Twitter -->
                                                            <a href="#" target="_blank">
                                                              twitter
                                                            </a>
                                                          </div>
                                                        </div>
                                                  </li>
                                      </ul>
                              </div>
                        </div>
                </div>
            </div>
            <div class="col-xs-12 col-lg-6 side-border-left">
              <div class="row">
                <div class="col-xs-12 col-sm-4">
                  <h4>PARTNER WITH US:</h4>
                </div>
                  <div class="col-xs-12 col-sm-8">
                    <ul id="footer-top" class="list-unstyled list-inline">
                      <li id="menu-item-2820" class="menu-item menu-item-type-custom menu-item-object-custom menu-  item-2820"><a href="https://genemedicsnutrition.com/vip-membership.html/">Affiliate Partner</a></li>
                </ul>
            </div>
              </div>
            </div>
      </div>
    </div>
  </div>
</div>


  <footer id="footer">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-7 hidden-xs">
              <div class="inner right">
<!--                 <ul id="menu" class="main-menu strong header-menu list-inline"><li id="menu-item-134" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-134"><a href="http://demowebsite1.net/genemedics/privacy-policy/">Privacy Policy</a></li>
                  <li id="menu-item-137" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-137"><a href="http://demowebsite1.net/genemedics/sitemap/">Sitemap</a></li>
 -->                </ul>
              </div>
            </div>
        <div class="footer-content">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="left-col">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-7">
                  <h4>ABOUT GENEMEDICS HEALTH INSTITUTE</h4>
                  <p>Genemedics® Health Institute is a global premier institute dedicated to revolutionizing health and medicine through healthy lifestyle education, guidance and accountability in harmony with functional medicine. Our physician-supervised health programs are personally customized to help you reach your health and fitness goals while looking and feeling better than ever.</p>
                  <h4>CONTACT US</h4>
                  <p>Phone : 800-277-4041<br />
                  Email : info@genemedics.com</p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-offset-1 col-lg-4">
                  <div class="footer-menu">
                    <h4>OUR SERVICES</h4>
                    <div class="row">
                      <div class="col-xs-12">
                        <ul id="footer-menu" class="list-unstyled"><li id="menu-item-2374" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2374"><a href="{{ env('LIVE_URL') }}services/hormone-imbalance-treatment/hormone-replacement-for-men/">Hormone Replacement For Men</a></li>
                          <li id="menu-item-2375" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2375"><a href="{{ env('LIVE_URL') }}services/hormone-imbalance-treatment/hormone-replacement-for-women/">Hormone Replacement For Women</a></li>
                          <li id="menu-item-2379" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2379"><a href="{{ env('LIVE_URL') }}services/anti-aging/weight-loss/">Medical Weight Loss</a></li>
                          <li id="menu-item-2371" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2371"><a href="{{ env('LIVE_URL') }}services/anti-aging/weight-loss/the-hcg-diet/">HCG Diet</a></li>
                          <li id="menu-item-2380" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2380"><a href="{{ env('LIVE_URL') }}services/anti-aging/lab-testing/">Lab Testing</a></li>
                          <li id="menu-item-2368" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2368"><a href="{{ env('LIVE_URL') }}services/anti-aging/nutrition-consulting/">Nutrition Consulting</a></li>
                          <li id="menu-item-2367" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2367"><a href="{{ env('LIVE_URL') }}services/anti-aging/exercise/">Exercise Consulting</a></li>
                        </ul>
                        <ul id="footer-menu" class="list-unstyled"><li id="menu-item-2381" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2381"><a href="{{ env('LIVE_URL') }}services/anti-aging/nutritional-supplement/">Nutritional Supplement</a></li>
                          <li id="menu-item-2382" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2382"><a href="{{ env('LIVE_URL') }}services/anti-aging/supplement-consulting/">Supplement Consulting</a></li>
                          <li id="menu-item-2383" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2383"><a href="{{ env('LIVE_URL') }}services/iv-nutrition/">IV Nutrition</a></li>
                          <li id="menu-item-2384" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2384"><a href="{{ env('LIVE_URL') }}services/gourmet-foods/">Gourmet Foods</a></li>
                          <li id="menu-item-2385" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2385"><a href="{{ env('LIVE_URL') }}health-wellness-app/home/">Health and Wellness App</a></li>
                          <li id="menu-item-2386" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2386"><a href="https://genemedicsnutrition.com/vip-membership.html/">Become a VIP Member</a></li>
                          <li id="menu-item-2387" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-2387"><a href="{{ env('LIVE_URL') }}category/health-library/">Health Library</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 side-border-left">
            <div class="right-col">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                  <h4><u>About Genemedics Health App</u></h4>
                  <p>The Genemedics® Health App is the ultimate, one-of-a-kind health and wellness app, offering more features than any other health and wellness app in the world. The Genemedics® Health App is also the most flexible health app available today, seamlessly connecting with virtually every smart device on the market. <a href="{{ env('LIVE_URL') }}health-wellness-app/home/">Read More</a></p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 right-menu">
                  <h4>Log In / Sign Up</h4>
                  <a href="{{ env('LIVE_URL') }}health-wellness-app/home/">About us</a>
                  <a href="{{ env('LIVE_URL') }}health-wellness-app/why-choose-us/">Why Choose Us</a>
                  <a href="{{ env('LIVE_URL') }}health-wellness-app/contact-us/">Contact Us</a>
                  <a href="">Become a VIP Member</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="copyright">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-12 col-md-12 ">
            <!-- col-lg-5 -->
            <div class="inner left">
              ©  Copyright 2015 Genemedics Health Institute. All Rights Reserved          </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
</div>
<!-- <div id="banner-form">
  <div class="inner">
    <h2>FREE <br><span>CONSULTATION</span></h2>
    <div class="subtitle">Completely Confidential. No Obligation.</div>
    <div class="frm_forms  with_frm_style frm_style_formidable-style" id="frm_form_4_container">
      <form enctype="multipart/form-data" method="post" class="frm-show-form " id="form_2zzaq7" >
        <div class="frm_form_fields ">
          <fieldset>
            <legend class="frm_hidden">Free Consultation 2</legend>

            <input type="hidden" name="frm_action" value="create" />
            <input type="hidden" name="form_id" value="4" />
            <input type="hidden" name="frm_hide_fields_4" id="frm_hide_fields_4" value="" />
            <input type="hidden" name="frm_helpers_4" id="frm_helpers_4" value="[]" />
            <input type="hidden" name="form_key" value="2zzaq7" />
            <input type="hidden" name="item_meta[0]" value="" />
            <input type="hidden" id="frm_submit_entry_4" name="frm_submit_entry_4" value="39913a8f69" /><input type="hidden" name="_wp_http_referer" value="/genemedics/health-wellness-app/log-in-sign-up/" />
            <div id="frm_field_27_container" class="frm_form_field form-field  frm_required_field frm_none_container">
                <label for="field_zok965" class="frm_primary_label">First Name
                    <span class="frm_required"></span>
                </label>
                <input type="text" id="field_zok965" name="item_meta[27]" value=""  placeholder="First Name"  />
            </div>
            <div id="frm_field_120_container" class="frm_form_field form-field  frm_required_field frm_none_container">
                <label for="field_4bqbxe" class="frm_primary_label">Last Name
                    <span class="frm_required"></span>
                </label>
                <input type="text" id="field_4bqbxe" name="item_meta[120]" value=""  placeholder="Last Name"  />
            </div>
            <div id="frm_field_29_container" class="frm_form_field form-field  frm_none_container frm_first frm_half">
                <label for="field_ic21vs" class="frm_primary_label">Age
                    <span class="frm_required"></span>
                </label>
                <input type="text" id="field_ic21vs" name="item_meta[29]" value=""  placeholder="Age"  />
            </div>
            <div id="frm_field_30_container" class="frm_form_field form-field  frm_none_container frm_half">
                <label for="field_3ag6k8" class="frm_primary_label">Gender
                    <span class="frm_required"></span>
                </label>
                <input type="text" id="field_3ag6k8" name="item_meta[30]" value=""  placeholder="Gender"  />
            </div>
            <div id="frm_field_31_container" class="frm_form_field form-field  frm_none_container">
                <label for="field_s29ein" class="frm_primary_label">Phone
                    <span class="frm_required"></span>
                </label>
                <input type="text" id="field_s29ein" name="item_meta[31]" value=""  placeholder="Phone"  />
            </div>
            <div id="frm_field_32_container" class="frm_form_field form-field  frm_required_field frm_none_container">
                <label for="field_c29no9" class="frm_primary_label">Email Address
                    <span class="frm_required"></span>
                </label>
                <input type="email" id="field_c29no9" name="item_meta[32]" value=""  placeholder="Email"  />
            </div>
            <div id="frm_field_33_container" class="frm_form_field form-field  frm_none_container">
                <label for="field_be59cw" class="frm_primary_label">Comment
                    <span class="frm_required"></span>
                </label>
                <textarea name="item_meta[33]" id="field_be59cw" rows="9"  placeholder="Comments"  ></textarea>
            </div>
            <div id="frm_field_35_container" class="frm_form_field form-field  frm_none_container">
                <label class="frm_primary_label">reCAPTCHA
                    <span class="frm_required"></span>
                </label>
                <div id="field_v9kjcm" class="g-recaptcha" data-sitekey="6LfMIBITAAAAAJ97UUfkoJjUuJQ69ZolpRNFrmwX" data-size="default"></div>
            </div>
            <input type="hidden" name="item_key" value="" />
            <div class="frm_submit">
              <input type="submit" value="Submit"  />
              <img class="frm_ajax_loading" src="/images/ajax_loader.gif" alt="Sending"/>
            </div>
          </fieldset>
        </div>
      </form>
    </div>
  </div>
  <span id="formActive" class="slideToggle">FREE CONSULTATION</span>
  <span id="formBtn" class="slideToggle">FREE CONSULTATION</span>
  <span id="exitBtn" class="fa fa-close"></span>
</div> -->
<div class="back-top">
  <i class="fa fa-chevron-up"></i>
</div>
<style type="text/css">
@media screen {
  #share-buttons {
      position: relative;
  }
  #share-buttons img {
      padding-bottom: 5px;
  }
  #share-buttons .cont {
      background-color: rgba(0, 0, 0, 0.2);
      display: none;
      padding: 10px;
      position: absolute;
      z-index: 9;
  }
  #share-buttons .cont:hover {
      display: block;
  }
  #share-buttons:hover .cont {
      display: block;
  }
  #share-buttons .cont a {
      font-size: 15px;
      line-height: 18px;
      margin: 0;
  }


  #footer-top li {
      border-right: 1px solid #fff;
      padding: 0 10px;
  }
  #footer-top li a {
      font-size: 15px;
      line-height: 29px;
  }
  #footer-top li:last-child {
      border-right: medium none;
      padding-right: 0;
  }
  .footer-top-menu .medias {
      position: absolute;
      right: 0;
      top: 0;
  }
}

@media (min-width: 1400px) {
  .footer-top-menu h4, .footer-top-menu a {
      font-size: 24px;
  }
  #footer-top li a {
      font-size: 14px;
      line-height: 34px;
  }
  }
  @media (min-width: 1600px) {
  #footer .left, #footer .main-menu > li > a {
      font-size: 13px;
  }

  .footer-top-menu h4, .footer-top-menu a {
      font-size: 30px;
  }
  .footer-top-menu h4 {
      line-height: 45px;
  }
  #footer-top li a {
      font-size: 20px;
      line-height: 45px;
  }
  .footer-top-menu .medias li .fa {
      font-size: 23px;
      line-height: 38px;
      width: 38px;
  }
}

@media (min-width: 2400px) {
  .medias li {
      padding-right: 10px;
  }
  #share-buttons img {
      padding-bottom: 16px;
      width: 140px;
  }
  #share-buttons .cont a {
      font-size: 22px;
      line-height: 30px;
  }
  .footer-top-menu .medias li .fa {
      font-size: 33px;
      line-height: 60px;
      width: 60px;
  }

  .footer-top-menu .footer-top-wrap {
      padding: 0 51px;
  }
  #footer-top li a {
      font-size: 31px;
      line-height: 55px;
  }
}

@media only screen and (max-width: 768px){
  .footer-top-menu .medias {
      position: relative;
  }
}
@media only screen and (max-width: 360px){
  .footer-top-menu .medias {
      position: relative;
  }

  .footer-top-menu .medias li .fa {
      font-size: 15px;
      line-height: 26px;
      width: 26px;
  }
}

</style>