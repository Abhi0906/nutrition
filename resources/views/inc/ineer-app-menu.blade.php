
@if(Auth::check())
		            <div class="no_radius navbar<?php if ($template['header_navbar']) { echo ' ' . $template['header_navbar']; } ?><?php if ($template['header']) { echo ' '. $template['header']; } ?>">

		                <?php
		                if ( $template['header_content'] == 'horizontal-menu' ) { // Horizontal Menu Header Content ?>
		                    <!-- Navbar Header -->
		                    <!-- <div class="navbar-header"> -->
		                        <!-- Horizontal Menu Toggle + Alternative Sidebar Toggle Button, Visible only in small screens (< 768px) -->
		                        <!-- <ul class="nav navbar-nav-custom pull-left visible-xs">
		                            <li>
		                                <a href="javascript:void(0)" data-toggle="collapse" data-target="#horizontal-menu-collapse">Menu</a>
		                            </li>
		                        </ul>  -->
		                        <!-- END Horizontal Menu Toggle + Alternative Sidebar Toggle Button -->



		                        <!-- Main Sidebar Toggle Button -->
		                        <!-- <ul class="nav navbar-nav-custom">
		                            <li>
		                                <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
		                                    <i class="fa fa-bars fa-fw"></i>
		                                </a>
		                            </li>
		                        </ul>
		                        <!-- END Main Sidebar Toggle Button -->
		                    <!-- </div> -->
		                    <!-- END Navbar Header -->


		                    <!-- Alternative Sidebar Toggle Button, Visible only in large screens (> 767px) -->

		                    <!-- END Alternative Sidebar Toggle Button -->

		                    <!-- Horizontal Menu + Search -->
		                    <div id="" class="">
		                    @if($primary_nav )

		                        <!-- Sidebar Navigation -->
		                        <ul class="nav navbar-nav admin_nav_horiz">
		                        <?php
		                            foreach( $primary_nav as $key => $link ) {
		                                $link_class = '';
		                                $li_active  = '';
		                                $menu_link  = '';

		                                // Get 1st level link's vital info
		                                $url        = (isset($link['url']) && $link['url']) ? $link['url'] : '#';
		                                $active     = (isset($link['url']) && ($template['active_page'] == $link['url']) || (isset($link['url']) && strpos($link['url'], $template['active_page']) !==false) ) ? ' active' : '';
		                                $icon       = (isset($link['icon']) && $link['icon']) ? '<i class="' . $link['icon'] . ' sidebar-nav-icon font_14"></i>' : '';

		                                // Check if the link has a submenu
		                                if (isset($link['sub']) && $link['sub']) {
		                                    // Since it has a submenu, we need to check if we have to add the class active
		                                    // to its parent li element (only if a 2nd or 3rd level link is active)
		                                    foreach ($link['sub'] as $sub_link) {
		                                        if (in_array($template['active_page'], $sub_link)) {
		                                            $li_active = ' class="active"';
		                                            break;
		                                        }

		                                        // 3rd level links
		                                        if (isset($sub_link['sub']) && $sub_link['sub']) {
		                                            foreach ($sub_link['sub'] as $sub2_link) {
		                                                if (in_array($template['active_page'], $sub2_link)) {
		                                                    $li_active = ' class="active"';
		                                                    break;
		                                                }
		                                            }
		                                        }
		                                    }

		                                    $menu_link = 'dropdown';
		                                }

		                                // Create the class attribute for our link
		                                if ($menu_link || $active) {
		                                    $link_class = ' class="'. $menu_link . $active .'"';
		                                }
		                            ?>
		                            <?php
		                                if ($url == 'header') { // if it is a header and not a link ?>

		                                <li class="sidebar-header">
		                                <?php
		                                 if (isset($link['opt']) && $link['opt']) { // If the header has options set
		                                ?>
		                                <span class="sidebar-header-options clearfix"><?php echo $link['opt']; ?></span>
		                                <?php } ?>
		                                <span class="sidebar-header-title"><?php echo $link['name']; ?></span>
		                                </li>

		                            <?php
		                                } else { // If it is a link
		                            ?>
		                                <li <?php echo $li_active; ?> <?php echo $link_class; ?> >
		                                    <?php
		                                    if (isset($link['sub']) && $link['sub']) {
		                                    ?>
		                                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
		                                    <?php
		                                    }else{
		                                    ?>
																						<?php echo "<a href = '/admin".$url."'>"; ?>
		                                    <?php
		                                    }
		                                    ?>

		                                    <?php
		                                        echo $icon;
		                                    ?>  &nbsp;
		                                        <span class="sidebar-nav-mini-hide"><?php echo $link['name']; ?></span>
		                                        <?php
		                                        if (isset($link['sub']) && $link['sub']) {
		                                        // if the link has a submenu
		                                    ?>  &nbsp;
		                                        <!-- <i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i> -->
		                                        <i class="fa fa-angle-down"></i>
		                                    <?php
		                                        }

		                                    ?>
		                                        </a>
		                                    <?php if (isset($link['sub']) && $link['sub']) { // if the link has a submenu ?>

		                                    <ul class="dropdown-menu">
		                                        <?php foreach ($link['sub'] as $sub_link) {
		                                            $link_class = '';
		                                            $li_active = '';
		                                            $submenu_link = '';

		                                            // Get 2nd level link's vital info
		                                            $url        = (isset($sub_link['url']) && $sub_link['url']) ? $sub_link['url'] : '#';
		                                            $active     = (isset($sub_link['url']) && ($template['active_page'] == $sub_link['url'])) ? ' active' : '';
		                                            $sub_link_icon       = (isset($sub_link['icon']) && $sub_link['icon']) ? '<i class="' . $sub_link['icon'] . ' sidebar-nav-icon"></i>' : '';

		                                            // Check if the link has a submenu
		                                            if (isset($sub_link['sub']) && $sub_link['sub']) {
		                                                // Since it has a submenu, we need to check if we have to add the class active
		                                                // to its parent li element (only if a 3rd level link is active)
		                                                foreach ($sub_link['sub'] as $sub2_link) {
		                                                    if (in_array($template['active_page'], $sub2_link)) {
		                                                        $li_active = ' class="active"';
		                                                        break;
		                                                    }
		                                                }

		                                                $submenu_link = 'dropdown-submenu';
		                                            }

		                                            if ($submenu_link || $active) {
		                                                $link_class = ' class="'. $submenu_link . $active .'"';
		                                            }
		                                        ?>
		                                        <li<?php echo $li_active; ?> <?php echo $link_class; ?>>

		                                            <?php echo "<a href = '/admin".$url."'>"; ?>
		                                                <?php echo $sub_link_icon; ?>
		                                                <?php if (isset($sub_link['sub']) && $sub_link['sub']) { ?><i class="fa fa-angle-left sidebar-nav-indicator"></i><?php } echo $sub_link['name']; ?>
		                                            </a>
		                                            <?php if (isset($sub_link['sub']) && $sub_link['sub']) { ?>
		                                                <ul>
		                                                    <?php foreach ($sub_link['sub'] as $sub2_link) {
		                                                        // Get 3rd level link's vital info
		                                                        $url    = (isset($sub2_link['url']) && $sub2_link['url']) ? $sub2_link['url'] : '#';
		                                                        $active = (isset($sub2_link['url']) && ($template['active_page'] == $sub2_link['url'])) ? ' class="active"' : '';
		                                                    ?>
		                                                    <li>
																														<?php echo "<a href = '/admin".$url."' ".$active." ".$sub2_link['name'].">"; ?>
		                                                    </li>
		                                                    <?php } ?>
		                                                </ul>
		                                            <?php } ?>
		                                        </li>
		                                        <?php } ?>
		                                    </ul>
		                                    <?php } ?>
		                                </li>
		                            <?php
		                               }
		                            }
		                            ?>
		                        </ul>
		                        <!-- END Sidebar Navigation -->

		                    @endif
		                    <!-- <form action="page_ready_search_results.php" class="navbar-form navbar-left" role="search">
		                        <div class="form-group">
		                            <input type="text" class="form-control" placeholder="Search..">
		                        </div>
		                    </form> -->
		                    </div>
		                    <!-- END Horizontal Menu + Search -->

		                <?php
		                } else { // Default Header Content
		                    ?>
		                    <!-- Left Header Navigation -->
		                    <ul class="nav navbar-nav-custom pull-right">
		                        <!-- Main Sidebar Toggle Button -->
		                        <li>
		                            <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
		                                <i class="fa fa-bars fa-fw"></i>
		                            </a>
		                        </li>
		                        <!-- END Main Sidebar Toggle Button -->

		                        <!-- Template Options -->
		                        <!-- Change Options functionality can be found in js/app.js - templateOptions() -->
		                        <!-- <li class="dropdown">
		                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
		                                <i class="gi gi-settings"></i>
		                            </a>
		                            <ul class="dropdown-menu dropdown-custom dropdown-options">
		                                <li class="dropdown-header text-center">Header Style</li>
		                                <li>
		                                    <div class="btn-group btn-group-justified btn-group-sm">
		                                        <a href="javascript:void(0)" class="btn btn-primary" id="options-header-default">Light</a>
		                                        <a href="javascript:void(0)" class="btn btn-primary" id="options-header-inverse">Dark</a>
		                                    </div>
		                                </li>
		                                <li class="dropdown-header text-center">Page Style</li>
		                                <li>
		                                    <div class="btn-group btn-group-justified btn-group-sm">
		                                        <a href="javascript:void(0)" class="btn btn-primary" id="options-main-style">Default</a>
		                                        <a href="javascript:void(0)" class="btn btn-primary" id="options-main-style-alt">Alternative</a>
		                                    </div>
		                                </li>
		                            </ul>
		                        </li> -->
		                        <!-- END Template Options -->
		                    </ul>
		                    <!-- END Left Header Navigation -->

		                    <!-- Search Form -->
		                    <!--  <form action="page_ready_search_results.php" method="post" class="navbar-form-custom" role="search">
		                        <div class="form-group">
		                            <input type="text" id="top-search" name="top-search" class="form-control" placeholder="Search..">
		                        </div>
		                    </form> -->
		                    <!-- END Search Form -->
		                <?php
		                }
		                ?>
		            </div>
		        @endif