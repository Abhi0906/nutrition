@if(Auth::check())
  <ul class="sub-menu patient-login text-uppercase">
    <li><a href="/dashboard">Dashboard</a></li>
    <li><a href="/goals">Goals</a></li>
    <!-- <li><a href="/food">Food</a></li>
    <li><a href="/exercise">Exercise</a></li> -->
    <li><a href="/diary">Diary</a></li>
    <li><a href="/weight">Weight</a></li>
    <li><a href="/sleep">Sleep</a></li>
    <li><a href="/bp_pulse">BP & Pulse</a></li>
    <li><a href="/reports">Reports</a></li>
    <li><a href="/workout_routines">Workouts</a></li>
    <li><a href="/deals_list_front">Deals</a></li>
    <li><a href="/challenges_list_front">Challenges</a></li>
    <li><a href="/event">Events</a></li>
    <li><a href="/my_plans">My Plans</a></li>
    <li class="hidden-lg"><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#Refer_friend" >Refer a friend</a></li>
  </ul>
  @else
  <ul class="sub-menu">
    <li><a href="{{ env('LIVE_URL') }}health-wellness-app/home/">About Us</a></li>
    <li><a href="{{ env('LIVE_URL') }}health-wellness-app/why-choose-us/">Why Choose Us</a></li>
    <li class="current-menu-item"><a href="{{URL::to('/auth/login')}}">Log In / Sign Up</a></li>
    <li><a href="{{ env('LIVE_URL') }}health-wellness-app/contact-us/">Contact Us</a></li>
  </ul>
@endif