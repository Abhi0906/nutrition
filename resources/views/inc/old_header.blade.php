<!-- Header Container of the Page -->

<div class="container-fluid headerlogin">

    <!-- Header for Skiky  -->
 	<header class="fluidpadd">
 	    
      <!-- This is First Login/search/phone numer header -->
 			<div class="row uperheaderlogin none-marginleftright visible-lg">
 			  <div class="col-md-4 text-left">
 				 <ul class="list-inline headfirst">
 					  <li>{!! HTML::image('images/icon-phone.png') !!}</li>
 				 	  <li class="phonetext"><strong>800-277-4041</strong></li>
                         <li><a class="btn freeconsult" href="http://dev.genemedicsnutrition.com/sso/saml/login" target="_blank">Genemedics Nutrition</a></li> 
 				 </ul>
 			  </div>
 			  <div class="col-md-4 text-center">
     				<form role="form" class="form-inline space_search_login">
     					<div class="form-group">
     						<label for="search" class="sr-only">Search</label>
     						<div class="input-group search">
     							<input type="text" class="form-control search" placeholder="Zip Code - Find doctor near you">
     							<span class="input-group-btn"><button class="btn go">Go !</button></span>
     						</div>
     					</div>
     				</form>
 			  </div>
 			  <div class="col-md-4 text-right">
            
   				<ul class="list-inline space_search_login hideafterlogin">
                    <li><button class="btn freeconsult">FREE CONSULT</button></li>
   					<li>
                        <a href="javascript:void(0);">{!! HTML::image('images/login.png') !!}</a>
   						<strong style="color:#000; vertical-align: middle;" class="text-uppercase"><a href="#/login" class="loginjoin">Patient Login/Sign up</a>&nbsp;&nbsp;
                        {!! HTML::image('images/fa-angle-up.png',null,['class'=>'arrow-petientlogin-signup']) !!}

                      </strong>
                    </li>
   				</ul>
                 
                <!-- Right Header Navigation -->
                  @if (Auth::check())
					<ul class="list-inline login_user_dropdown pull-right">
                        <li><button class="btn freeconsult">FREE CONSULT</button></li>
                        <li class="dropdown">
                                
                                
                                <!-- {!! HTML::image("uploads/".auth()->user()->photo,"avatar") !!} --> 
                               
                                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                {!! HTML::image('images/login.png') !!}
                                <span style="color:#000; vertical-align: middle;" class="text-uppercase">{{auth()->user()->full_name}} &nbsp;
                                
                                
                                 {!! HTML::image('images/fa-angle-up.png',null,['class'=>'arrow-petientlogin-signup']) !!}
                                </span>
                                </a>
                                <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                                    <!-- <li class="user-loggedin-name">
                                        <div class="text-center">
                                            {{auth()->user()->full_name}}
                                        </div>
                                    </li>
                                    <li class="divider"></li> -->
                                    <li>
                                        <a href="{{URL::to('/'.auth()->user()->user_type.'/'.auth()->user()->id.'/demographics')}}">
                                            <i class="fa fa-user fa-fw pull-right"></i>
                                            Profile
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="{{URL::to('/auth0/logout')}}"><i class="gi gi-exit fa-fw pull-right"></i> Logout</a>
                                    </li>
                                </ul>
                        </li>
                    </ul>
                    
                  @endif
                    <!-- END Right Header Navigation -->
 			  </div>
 		  </div>
        <!-- End of Login/search/phone number header -->

        <!-- This is for Second Header for Menu -->
     		<div class="row menuborder none-marginleftright">

     			<div class="col-lg-3 col-md-3 col-sm-10 col-xs-10">
               {!! HTML::image('images/login_nutrition_logo.png', 'Genemedics Health', ['class' => 'img-responsive']) !!}
     			</div>

     			<!-- The Menu Container -->
     			<div class="col-lg-9 col-md-9 col-sm-2 col-xs-2">
     				<!-- For Menu --> 
     				@include('inc.nav')
       			<!-- End of Menu -->
        
     			</div>
     			<!-- End of Menu Container -->

     		</div>
        <!-- End of Second Header Menu -->
        <!-- In the PHP version you can set the following options from inc/config file -->
            <!--
                Available header.navbar classes:

                'navbar-default'            for the default light header
                'navbar-inverse'            for an alternative dark header

                'navbar-fixed-top'          for a top fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar())
                    'header-fixed-top'      has to be added on #page-container only if the class 'navbar-fixed-top' was added

                'navbar-fixed-bottom'       for a bottom fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar()))
                    'header-fixed-bottom'   has to be added on #page-container only if the class 'navbar-fixed-bottom' was added
            -->
             @if(Auth::check())
            <div class="navbar<?php if ($template['header_navbar']) { echo ' ' . $template['header_navbar']; } ?><?php if ($template['header']) { echo ' '. $template['header']; } ?>">

                <?php 
                if ( $template['header_content'] == 'horizontal-menu' ) { // Horizontal Menu Header Content ?>
                    <!-- Navbar Header -->
                    <!-- <div class="navbar-header"> -->
                        <!-- Horizontal Menu Toggle + Alternative Sidebar Toggle Button, Visible only in small screens (< 768px) -->
                        <!-- <ul class="nav navbar-nav-custom pull-left visible-xs">
                            <li>
                                <a href="javascript:void(0)" data-toggle="collapse" data-target="#horizontal-menu-collapse">Menu</a>
                            </li>
                        </ul>  -->
                        <!-- END Horizontal Menu Toggle + Alternative Sidebar Toggle Button -->

                        

                        <!-- Main Sidebar Toggle Button -->
                        <!-- <ul class="nav navbar-nav-custom">
                            <li>
                                <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                                    <i class="fa fa-bars fa-fw"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- END Main Sidebar Toggle Button -->
                    <!-- </div> -->
                    <!-- END Navbar Header -->


                    <!-- Alternative Sidebar Toggle Button, Visible only in large screens (> 767px) -->

                    <!-- END Alternative Sidebar Toggle Button -->

                    <!-- Horizontal Menu + Search -->
                    <div id="" class="">
                    @if($primary_nav )

                        <!-- Sidebar Navigation -->
                        <ul class="nav navbar-nav">                        
                        <?php 
                            foreach( $primary_nav as $key => $link ) {
                                $link_class = '';
                                $li_active  = '';
                                $menu_link  = '';
                                
                                // Get 1st level link's vital info
                                $url        = (isset($link['url']) && $link['url']) ? $link['url'] : '#';
                                $active     = (isset($link['url']) && ($template['active_page'] == $link['url']) || (isset($link['url']) && strpos($link['url'], $template['active_page']) !==false) ) ? ' active' : '';
                                $icon       = (isset($link['icon']) && $link['icon']) ? '<i class="' . $link['icon'] . ' sidebar-nav-icon"></i>' : '';

                                // Check if the link has a submenu
                                if (isset($link['sub']) && $link['sub']) {
                                    // Since it has a submenu, we need to check if we have to add the class active
                                    // to its parent li element (only if a 2nd or 3rd level link is active)
                                    foreach ($link['sub'] as $sub_link) {
                                        if (in_array($template['active_page'], $sub_link)) {
                                            $li_active = ' class="active"';
                                            break;
                                        }

                                        // 3rd level links
                                        if (isset($sub_link['sub']) && $sub_link['sub']) {
                                            foreach ($sub_link['sub'] as $sub2_link) {
                                                if (in_array($template['active_page'], $sub2_link)) {
                                                    $li_active = ' class="active"';
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    $menu_link = 'dropdown';
                                }

                                // Create the class attribute for our link
                                if ($menu_link || $active) {
                                    $link_class = ' class="'. $menu_link . $active .'"';
                                }
                            ?>
                            <?php 
                                if ($url == 'header') { // if it is a header and not a link ?>
                            
                                <li class="sidebar-header">
                                <?php
                                 if (isset($link['opt']) && $link['opt']) { // If the header has options set 
                                ?>
                                <span class="sidebar-header-options clearfix"><?php echo $link['opt']; ?></span>
                                <?php } ?>
                                <span class="sidebar-header-title"><?php echo $link['name']; ?></span>
                                </li>

                            <?php 
                                } else { // If it is a link 
                            ?>
                                <li <?php echo $li_active; ?> <?php echo $link_class; ?> >
                                    <?php 
                                    if (isset($link['sub']) && $link['sub']) {
                                    ?>
                                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                    <?php
                                    }else{
                                    ?>
                                        <a href="<?php echo $url; ?>">
                                    <?php
                                    }
                                    ?>
                                    
                                    <?php 
                                        echo $icon;
                                    ?>  &nbsp;
                                        <span class="sidebar-nav-mini-hide"><?php echo $link['name']; ?></span>
                                        <?php
                                        if (isset($link['sub']) && $link['sub']) { 
                                        // if the link has a submenu 
                                    ?>  &nbsp;
                                        <!-- <i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i> -->
                                        <i class="fa fa-angle-down"></i>
                                    <?php 
                                        } 
                                         
                                    ?>
                                        </a>
                                    <?php if (isset($link['sub']) && $link['sub']) { // if the link has a submenu ?>
                                    
                                    <ul class="dropdown-menu">
                                        <?php foreach ($link['sub'] as $sub_link) {
                                            $link_class = '';
                                            $li_active = '';
                                            $submenu_link = '';

                                            // Get 2nd level link's vital info
                                            $url        = (isset($sub_link['url']) && $sub_link['url']) ? $sub_link['url'] : '#';
                                            $active     = (isset($sub_link['url']) && ($template['active_page'] == $sub_link['url'])) ? ' active' : '';
                                            $sub_link_icon       = (isset($sub_link['icon']) && $sub_link['icon']) ? '<i class="' . $sub_link['icon'] . ' sidebar-nav-icon"></i>' : '';

                                            // Check if the link has a submenu
                                            if (isset($sub_link['sub']) && $sub_link['sub']) {
                                                // Since it has a submenu, we need to check if we have to add the class active
                                                // to its parent li element (only if a 3rd level link is active)
                                                foreach ($sub_link['sub'] as $sub2_link) {
                                                    if (in_array($template['active_page'], $sub2_link)) {
                                                        $li_active = ' class="active"';
                                                        break;
                                                    }
                                                }

                                                $submenu_link = 'dropdown-submenu';
                                            }

                                            if ($submenu_link || $active) {
                                                $link_class = ' class="'. $submenu_link . $active .'"';
                                            }
                                        ?>
                                        <li<?php echo $li_active; ?> <?php echo $link_class; ?>>

                                            <a href="<?php echo $url; ?>">
                                                <?php echo $sub_link_icon; ?>
                                                <?php if (isset($sub_link['sub']) && $sub_link['sub']) { ?><i class="fa fa-angle-left sidebar-nav-indicator"></i><?php } echo $sub_link['name']; ?>
                                            </a>
                                            <?php if (isset($sub_link['sub']) && $sub_link['sub']) { ?>
                                                <ul>
                                                    <?php foreach ($sub_link['sub'] as $sub2_link) {
                                                        // Get 3rd level link's vital info
                                                        $url    = (isset($sub2_link['url']) && $sub2_link['url']) ? $sub2_link['url'] : '#';
                                                        $active = (isset($sub2_link['url']) && ($template['active_page'] == $sub2_link['url'])) ? ' class="active"' : '';
                                                    ?>
                                                    <li>
                                                        <a href="<?php echo $url; ?>"<?php echo $active ?>><?php echo $sub2_link['name']; ?></a>
                                                    </li>
                                                    <?php } ?>
                                                </ul>
                                            <?php } ?>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                    <?php } ?>
                                </li>
                            <?php
                               }
                            } 
                            ?>
                        </ul>
                        <!-- END Sidebar Navigation -->

                    @endif
                    <!-- <form action="page_ready_search_results.php" class="navbar-form navbar-left" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search..">
                        </div>
                    </form> -->
                    </div>
                    <!-- END Horizontal Menu + Search -->

                <?php 
                } else { // Default Header Content
                    ?>
                    <!-- Left Header Navigation -->
                    <ul class="nav navbar-nav-custom pull-right">
                        <!-- Main Sidebar Toggle Button -->
                        <li>
                            <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                                <i class="fa fa-bars fa-fw"></i>
                            </a>
                        </li>
                        <!-- END Main Sidebar Toggle Button -->

                        <!-- Template Options -->
                        <!-- Change Options functionality can be found in js/app.js - templateOptions() -->
                        <!-- <li class="dropdown">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="gi gi-settings"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-custom dropdown-options">
                                <li class="dropdown-header text-center">Header Style</li>
                                <li>
                                    <div class="btn-group btn-group-justified btn-group-sm">
                                        <a href="javascript:void(0)" class="btn btn-primary" id="options-header-default">Light</a>
                                        <a href="javascript:void(0)" class="btn btn-primary" id="options-header-inverse">Dark</a>
                                    </div>
                                </li>
                                <li class="dropdown-header text-center">Page Style</li>
                                <li>
                                    <div class="btn-group btn-group-justified btn-group-sm">
                                        <a href="javascript:void(0)" class="btn btn-primary" id="options-main-style">Default</a>
                                        <a href="javascript:void(0)" class="btn btn-primary" id="options-main-style-alt">Alternative</a>
                                    </div>
                                </li>
                            </ul>
                        </li> -->
                        <!-- END Template Options -->
                    </ul>
                    <!-- END Left Header Navigation -->

                    <!-- Search Form -->
                    <!--  <form action="page_ready_search_results.php" method="post" class="navbar-form-custom" role="search">
                        <div class="form-group">
                            <input type="text" id="top-search" name="top-search" class="form-control" placeholder="Search..">
                        </div>
                    </form> -->
                    <!-- END Search Form -->
                <?php 
                } 
                ?>
            </div>
           @endif
            <!-- END Header -->
 	</header>

    <!-- End of Header Stiky -->
	 
</div>
 
 <!-- End of The Header Container -->

