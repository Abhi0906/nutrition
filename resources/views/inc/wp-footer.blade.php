<div class="contact-bg p-t">
    <div class="container">
        <div class="col-md-8 col-sm-8 cont-app post ">
            <p class="con-title-sub">Download the app</p>
            <div class="mob-gen">
                Make the most of your day by getting more out of your night. Surge automatically monitors how long and how well you sleep, syncs those stats to your smartphone and computer, and displays your trends as detailed charts on the Fitbit dashboard
            </div>
            <div class="row"><a target="_blank" href="https://itunes.apple.com/in/app/genemedicshealth-health-tracker/id1061451973"><img src="images/app-store.png" alt="" title="" class="app-store app-bhn-m-l"></a></div>
        </div>
        <div class=" col-md-4 col-sm-4  post ">
            <img src="images/genemedics-mob.png" alt="" title="" class="mob-pos-gen">
        </div>
    </div>
</div>
<div class="title-ani">
    <div class="full-content1 line-full">
        <div class="container padd-40">
            <div class="col-md-3 col-sm-12 text-center  padd-10"><a href="index.html"><img src="images/scroll-logo.png" alt="" title="" /></a></div>
            <div class="col-md-4 col-sm-4 addre-cmp-padd">
                <p class="addre-cmp">280 North Old Wordward Ave. Suite 210Birmingham, MI 48009, USA</p>
                <p class="addre-cmp"><a href="tel:800-277-4041">800-277-4041</a></p>
                <p class="addre-cmp"><a href="mail:info@genemedics.com">info@genemedics.com</a></p>
            </div>
            <div class="col-md-2 col-sm-3">
                <div class="foot-bot-link">
                    <ul>
                        <li><a href="#" class="bottom-link">Affiliate Partner</a></li>
                        <li><a href="#" class="bottom-link">Affiliate Doctor</a></li>
                        <li><a href="#" class="bottom-link">Affiliate Consultant</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2 col-sm-5 padd-25 text-center">
                <ul class="social-network social-circle ">
                    <li><a href="https://www.facebook.com/Genemedics" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/genemedics" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://plus.google.com/+Genemedics/videos" class="icoGoogle" title="Google +"><i class="fa fa-youtube-play"></i></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="full-content1 padd-10">
        <div class="container">
            <div class="col-md-6 col-sm-6">
                <p class="copy-right"> &copy; Copyright Genemedics Health Institute. All Rights Reserved. Privacy Policy.</p>
            </div>
            <div class="col-md-6 col-sm-6">
                <a href="javascript:void(0);" id="scroll" title="Scroll to Top"><span>Back to Top</span></a>
            </div>
        </div>
    </div>

    <!--footer bottom-->
</div>
