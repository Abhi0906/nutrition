<?php
/**
 * template_start.php
 *
 * Author: pixelcave
 *
 * The first block of code used in every page of the template
 *
 */
?>
<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>{!! $template['title'] !!}</title>

        <meta name="description" content="<?php echo $template['description'] ?>">
        <meta name="author" content="<?php echo $template['author'] ?>">
        <meta name="robots" content="<?php echo $template['robots'] ?>">

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
        <meta name="csrf-token" content="{!! csrf_token() !!}">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="/img/favicon.png">
        <link rel="apple-touch-icon" href="/img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="/img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="/img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="/img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="/img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="/img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="/img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="/img/icon180.png" sizes="180x180">
        <!-- END Icons -->
        {!! HTML::script("js/vendor/jquery-1.11.3.min.js") !!}
        {!! HTML::script("js/vendor/angular.min.js") !!}
         {!! HTML::script("js/vendor/angular-route.min.js") !!}
        {!! HTML::script("js/vendor/angular-animate.min.js") !!}
        {!! HTML::script("js/vendor/angular-sanitize.min.js") !!}
        {!! HTML::script("js/ui-bootstrap-tpls-1.3.3.min.js") !!}

        <!-- Stylesheets -->
        {!! HTML::style("css/styles.css") !!}
        {!! HTML::style("css/sweetalert.css") !!}
        {!! HTML::style("css/genemedics.css") !!}
        {!! HTML::style("css/my_plans.css") !!}
        
        {!! HTML::style("css/flaticon/flaticon.css") !!}
        {!! HTML::style("css/ng-tags-input.min.css") !!}
        <link href='https://fonts.googleapis.com/css?family=Tillana:400,600,700,800' rel='stylesheet' type='text/css'>
        <!-- END Stylesheets -->

        <!-- {!! HTML::style("css/bootstrap.min.css") !!} -->
         <!-- V -->
        <!-- {!! HTML::style("css/BootstrapXL.css") !!}
        {!! HTML::style("css/jquery-gauge.css") !!}
        {!! HTML::style("css/genemedicshealthgoal.css") !!}
        {!! HTML::style("css/mediaquery-genemedics.css") !!} -->
        <!-- End -->
        <!-- END Stylesheets -->
<!-- 
        {!! HTML::style("css/nav.css") !!}
        {!! HTML::style("css/responsive.css") !!}
        {!! HTML::style("css/responsive_extended.css") !!}  -->

        <!-- Related styles of various icon packs and plugins -->
        <!-- <link rel="stylesheet" href="css/plugins.css"> -->
        
        <!-- new header stylesheets -->
        <!-- {!! HTML::style("css/main.css") !!} -->
        

        <!-- Modernizr (browser feature detection library) & Respond.js (enables responsive CSS code on browsers that don't support it, eg IE8) -->
        {!! HTML::script("js/vendor/modernizr-respond.min.js") !!}
        
       

        @yield('base')
       
    </head>
    <body ng-app="wellnessApp">