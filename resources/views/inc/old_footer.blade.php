 <div class="container-fluid footerback">
	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-12 text-left">
			<p class="small copyright">
			&copy;&nbsp;2015 GenemedicsNutrition.com. All rights reserved. Gendemedics Health Institute, LLC.
			</p>    
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12 text-right">

			<p class="text-uppercase imgspace">
				<span class="stay-connect-footer" style="">Stay Connected</span>
				<a href="#" title="20" data-toggle="popover" data-trigger="hover" data-content="Facebook">{!! HTML::image('images/footer_fafacebooksquare.png') !!}</a>
				<a href="#" title="10" data-toggle="popover" data-trigger="hover" data-content="Twitter">{!! HTML::image('images/footer_fawittersquare.png') !!}</a>
				<a href="#" title="35" data-toggle="popover" data-trigger="hover" data-content="Google Plus">{!! HTML::image('images/footer_fagoogleplussquare.png') !!}</a>
				<a href="#" title="45" data-toggle="popover" data-trigger="hover" data-content="You Tube">{!! HTML::image('images/footer_youtub.png') !!}</a>
				<a href="#" title="10" data-toggle="popover" data-trigger="hover" data-content="Plus">{!! HTML::image('images/footer_Plus.png') !!}</a>
			</p>
		</div>
	</div> 
</div>