

 <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <div id="sidepanel">
      <div class="inner">
        <div class="row">
            <div class="col-sm-4">
                <header>
                  @if(Session::has('organization_id') )
                     <img class="img-responsive padding_headlogo" src="/organization_logo/{{$organization['logo']}}" alt="mobile-logo">
                  @else
                     <img class="img-responsive padding_headlogo" src="/images/logo-genemedics.png" alt="mobile-logo">
                  @endif
                 <span class="exit pull-right glyphicon glyphicon-chevron-right"></span>
                </header>

                <div class="menu"></div>
            </div>
            <div class="col-sm-4 text-center">
                <div class="saperator_5"></div>
                 <span>
                    @if(Session::has('super_admin_user') )
                    <a class="btn btn-danger" href="/organization/stopImpersonate/{{session('super_admin_user')}}">&nbsp;Back to Admin</a>
                    @endif
                </span>

            </div>
            <div class="col-sm-4">

                <ul class="nav navbar-nav-custom pull-right white_back">

                    <!-- User Dropdown -->
                    <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void(0)" aria-expanded="false">
                    Welcome {{auth()->user()->first_name = strlen(auth()->user()->first_name) > 27 ? (ucfirst(substr(auth()->user()->first_name, 0, 27)) . '...') : ucfirst(auth()->user()->first_name)}}
                    <i class="fa fa-angle-down"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                    <li class="dropdown-header text-center">Account</li>

                    <li>
                    <?php
                      $routeName = \Route::currentRouteName();
                    //  $this->userType = explode(".", $routeName)[0];
                    ?>
                    <?php /*<a href="{{URL::to('/api/v2/admin/'.$this->userType.'/demographics/'.auth()->user()->id)}}"> */?>
                    <i class="fa fa-user fa-fw pull-right"></i>
                    Profile
                    </a>

                    <a title="Log Out" href="{{URL::to('/logout')}}"><i class="fa fa-ban fa-fw pull-right"></i> Logout</a>
                    </li>

                    </ul>
                    </li>
                    <!-- END User Dropdown -->
                </ul>
            </div>
        </div>

      </div>
    </div>
