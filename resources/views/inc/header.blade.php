

 <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <div id="sidepanel">
      <div class="inner">
        <header>
          <img class="img-responsive" src="/images/logo-genemedics.png" alt="mobile-logo">
          <span class="exit pull-right glyphicon glyphicon-chevron-right"></span>
        </header>
        <div class="menu"></div>
      </div>
    </div>
    <header id="header" style="background-image: url(/images/menu-bg_01.jpg)">
      <div class="top transition hidden-xs hidden-sm hidden-md">
        <div class="container-fluid">
          <div class="row">
            <div class="col-xs-12">
              <div class="list-item">
                <div class="item left">
                  <a class="tel strong regular" href="tel:8002774041"><img class="icon" src="/images/icon-phone.png" width="41" height="41">800-277-4041</a>
                </div>
                <!-- <div class="item middle">
                  <form class="search-doctor" method="post" action="{{ env('LIVE_URL') }}locations/">
                    <div class="form-group">
                      <input class="form-control" name="zip" placeholder="Zip Code - Find doctor near you" />
                      <input type="submit" class="btn" value="GO">
                    </div>
                  </form>
                </div> -->
                <div class="item right">
                  <style>
                      /*======= Toplinks =======*/
                      .toplinks {
                        padding:0;
                        float:right;
                        margin-left:15px;
                        line-height:normal;
                        padding-top:1px;

                      }
                      .toplinks .links {

                      }
                      .toplinks .links li {
                        display:inline;
                        text-align:right;
                        float:left
                      }
                      .toplinks ul.links li a {
                        color: black;
                        display: inline-block;
                        padding:0px;
                        border-right: 0px solid #cacaca;
                        color:#777 !important;
                        font-size:13px;
                        padding-left:25px
                      }
                      .toplinks ul.links li a:hover {
                        color: #2FDAB8;
                      }
                      .toplinks ul.links li.first a {
                        padding-left: 0;
                      }
                      .toplinks ul.links li.last a {
                        border: none;
                        padding-right:0
                      }
                      .toplinks>ul>li.wishlist>a:before {
                        content:'\f004';
                        font-family: FontAwesome;
                        font-size:13px;
                        padding-right:6px
                      }
                      .toplinks>ul>li.login>a:before {
                        content:'\f13e';
                        font-family: FontAwesome;
                        font-size:14px;
                        padding-right:6px
                      }
                      .toplinks>ul>li.logout>a:before {
                        content:'\f09c';
                        font-family: FontAwesome;
                        font-size:14px;
                        padding-right:6px
                      }
                      .toplinks .links li .click-nav {
                      }
                      .toplinks .links li .click-nav ul {
                        padding:0;
                        margin:0
                      }
                      .toplinks .links li .click-nav ul li {
                        position:relative;
                        list-style:none;
                        cursor:pointer;
                      }
                      .toplinks .links li .click-nav ul li ul {
                        position:absolute;
                        left:0px;
                        right:0;
                        z-index:999
                      }
                      .toplinks .links li .click-nav ul .clicker {
                        background:none;
                        color:#777;
                        text-shadow:none !important;
                        font-size:13px;
                        padding:0px;
                        padding-left:25px;
                        line-height:19px;
                        width:auto;
                        border-bottom:none
                      }
                      .toplinks .links li .click-nav ul .clicker:hover {
                        background:none;
                        color:#eee
                      }
                      .toplinks .links li .click-nav ul .clicker:hover, .click-nav ul .active {
                        color:#333 !important;
                      }
                      .click-nav img {
                        position:absolute;
                        top:9px;
                        left:12px;
                      }
                      .toplinks .links li .click-nav ul li a {
                        transition:background-color 0.2s ease-in-out;
                        -webkit-transition:background-color 0.2s ease-in-out;
                        -moz-transition:background-color 0.2s ease-in-out;
                        display:block;
                        padding:10px 10px;
                        color:#fff;
                        text-decoration:none;
                        font-size:12px;
                        text-align:left;
                        border-bottom:1px #000 solid;
                        width:130px
                      }
                      .toplinks .links li .click-nav ul li a:hover {
                        color:#eee !important;
                      }
                      .toplinks .links li .click-nav .no-js ul {
                        display:none;
                        z-index:99999;
                        width:150px
                      }
                      .toplinks .links li .click-nav .no-js:hover ul {
                        display:block;
                        background:#222;
                        width:150px;
                        margin-left:0px;
                        border:1px #111 solid;
                        padding:0;
                      }
                      .toplinks .links li .click-nav .no-js:hover ul li a {
                        color:#666;
                      }
                      .toplinks .links li .click-nav .no-js:hover ul li a:hover {
                        color:#333;
                      }
                      .toplinks .links li .click-nav ul li ul.link {
                        padding:8px 0;
                        border-top:1px #111 solid
                      }
                      .toplinks .links li .click-nav ul li ul.link li {
                        padding:0px 0
                      }
                      .caret:after {
                        content: "\f107";
                        display: inline-block;
                        vertical-align: top;
                        width: 5px;
                        margin:1px 0px 0 4px;
                        font-family: 'FontAwesome';
                      }
                      .toplinks .links div {
                        display: inline;
                        text-align: right;
                        float: left;
                      }
                      .toplinks div.links div a {
                        color: black;
                        display: inline-block;
                        padding: 0px;
                        border-right: 0px solid #cacaca;
                        color: #fff;
                        font-size: 19px;
                        padding-left: 25px;
                        font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
                         font-weight: 700;
                         text-rendering: optimizeLegibility !important;
                        -webkit-font-smoothing: antialiased !important;
                        -moz-osx-font-smoothing: grayscale;
                      }
                      .toplinks div.links div a:hover {
                        color: #2FDAB8;
                      }
                      .toplinks div.links div.first a {
                        padding-left: 0;
                      }
                      .toplinks div.links div.last a {
                        border: none;
                        padding-right: 0;
                      }
                      .toplinks>div>div.login>a:before {
                        content: '\f007';
                        font-family: FontAwesome;
                        font-size: 18px;
                        padding-right: 6px;
                      }

                      .toplinks>div>div.login>a:after {
                        font-family: FontAwesome;
                          content: "\f107";
                          padding-left: 10px;
                      }
                      .toplinks>div>div.logout>a:before {
                        content: '\f09c';
                        font-family: FontAwesome;
                        font-size: 14px;
                        padding-right: 6px;
                      }
                      .toplinks .links div .click-nav {
                      }
                      .toplinks .links div .click-nav ul {
                        padding: 0;
                        margin: 0;
                      }
                      .toplinks .links div .click-nav ul li {
                        position: relative;
                        list-style: none;
                        cursor: pointer;
                      }
                      .toplinks .links div .click-nav ul li ul {
                        position: absolute;
                        left: 0px;
                        right: 0;
                        z-index: 999;
                      }
                      .toplinks .links div .click-nav ul .clicker {
                        background: none;
                        color: #777;
                        text-shadow: none!important;
                        font-size: 13px;
                        padding: 0px;
                        padding-left: 25px;
                        line-height: 19px;
                        width: auto;
                        border-bottom: none;
                        padding-bottom:10px
                      }
                      .toplinks .links div .click-nav ul .clicker:hover {
                        background: none;
                        color: #2FDAB8;
                      }
                      .toplinks .links div .click-nav ul .clicker:hover, .click-nav ul .active {
                      }
                      .click-nav img {
                        position: absolute;
                        top: 9px;
                        left: 12px;
                      }
                      .toplinks .links div .click-nav ul li a {
                        transition:background-color 0.2s ease-in-out;
                        -webkit-transition:background-color 0.2s ease-in-out;
                        -moz-transition:background-color 0.2s ease-in-out;
                        display:block;
                        padding:6px 15px;
                        color:#fff;
                        text-decoration:none;
                        font-size:13px;
                        text-align:left;
                      }
                      .toplinks .links div .click-nav ul li a:hover {
                        color: #2FDAB8!important;
                      }
                      .toplinks .links div .click-nav .no-js ul {
                        display: none;
                        z-index: 99999;
                        width: 150px;
                      }
                      .toplinks .links div .click-nav .no-js:hover ul {
                        display:block;
                        background:#222;
                        border-top:2px #ffe51e solid;
                        padding:10px 0px;
                        width:140px;
                        border-bottom:4px #ffe51e solid;
                        margin-left:0px;
                        top:25px
                      }
                      .toplinks .links div .click-nav .no-js:hover ul li a {
                        color: #fff;
                      }
                      .toplinks .links div .click-nav .no-js:hover ul li a:hover {
                        color: #fff;
                      }
                      .toplinks .links div .click-nav ul li ul.link {
                        padding: 8px 0;
                        border-top: 2px #000 solid;
                      }
                      .toplinks .links div .click-nav ul li ul.link li {
                        padding: 0px 0;
                      }
                      .mob-links{ display:none}

                      #primary_nav_wrap .ul-subcontainer li a:hover
                      {
                        color:#FFF !important;
                      }
                      /*======= End Toplinks =======*/
                  </style>


                  <style>
                    #primary_nav_wrap.drop2{margin:0;}
                    
                    
                    #primary_nav_wrap.drop2 .arrow-log{
                      color: #002766;
                      font-size: 18px;
                      /*padding: 10px 0px;*/
                    }
                    #primary_nav_wrap.drop2 .arrow-log:before{
                        content: '\f007';
                      font-family: FontAwesome;
                      font-size: 18px;
                      padding-right: 6px;
                    }
                    #primary_nav_wrap.drop2 .user-logged{
                          color: #1c77bd;
                        display: inline-block;
                        position: absolute;
                        top: 18px;
                        left: 0;
                        width: 150px;
                        font-size: 20px;
                        font-weight: bold;
                    }
                    
                    #primary_nav_wrap.drop2 .inner-drop:before{
                    content: url(images/dropdown-login-bg-tri.png);
                        position: absolute;
                    top: -25px;
                    right: 50px;

                    
                    }
                    
                    #primary_nav_wrap.drop2 .inner-drop{
                      width: 400px;
                      /*height: 315px !important;*/ 
                      box-shadow: 1px 5px 15px 1px #565656 !important;
                      border: 8px solid #1f7bc1;
                      border-radius: 5px;
                      
                    }
                    
                    #primary_nav_wrap.drop2 .inner-drop li a{
                        padding: 5px 15px !important;
                    }
                    
                    #primary_nav_wrap.drop2 .inner-drop li a:hover{
                        color:white; 
                    }
                    
                    .div-myaccount-container{
                      margin-top: 30px;
                      float: right !important;
                    }
                    
                    .ul-subcontainer{
                        width: 180px;
                      display: inline-block;
                      float: none;
                      vertical-align: top;
                      text-align: left !important;
                    }
                    
                    .ul-subcontainer li{
                      width:100% !important;
                      
                    }
                    .div-aff-container { margin-left: 25px;}
                    .div-aff-container p{
                      background:url(https://genemedicsnutrition.com/skin/frontend/rwd/innova/images/aff-login.png) no-repeat;
                      background-position:left;
                      padding-left:30px;
                    }

                    .arrow-log:after{
                       font-family: FontAwesome;
                        content: "\f107";
                      padding-left: 10px;

                    }

                    #primary_nav_wrap.drop2 .inner-drop > lable{
                      max-width: 0px !important;
                    }

                    .user-logged {
                      max-width: none !important;
                    }
                    
                  </style>
                  @if (Auth::check())
                  <div class="toplinks">
                    <div class="links">
                      <!-- <div class="blog-magik"><a href="https://genemedicsnutrition.com/blog/" title="Blog"><span>Blog</span></a></div> -->
                            
                      <div id="primary_nav_wrap" class="drop2">
                      <ul>
                        <li>
                          <a href="#" class="arrow-log">

                            Welcome {{auth()->user()->first_name = strlen(auth()->user()->first_name) > 27 ? (ucfirst(substr(auth()->user()->first_name, 0, 27)) . '...') : ucfirst(auth()->user()->first_name)}}

                          </a>
                          <ul class="inner-drop">
                          
                            <div style="width:100%;border-bottom:1px solid #c9deee;">
                              <span style="float:left;text-align: left;position: relative; color:#2a5e7a;">
                                Welcome
                                <label class="user-logged">
                                  {{auth()->user()->full_name = strlen(auth()->user()->full_name) > 27 ? (substr(auth()->user()->full_name, 0, 27) . '...') : auth()->user()->full_name}}                           
                                </label>
                              </span> 
                              <a class="float:right" title="Log Out" href="{{URL::to('/auth0/logout')}}">
                                <img src="images/logout.png">
                              </a>
                            </div>
                          
                            <div class="div-myaccount-container">
                              <div class="ul-subcontainer">
                                <p style="padding-left: 15px;">My Store Account</p>
                                <li><a title="My Account" href="https://genemedicsnutrition.com/customer/account/index"><span>Account Dashboard</span></a></li>
                                <li><a title="Checkout" href="https://genemedicsnutrition.com/checkout/"><span>Checkout</span></a></li>
                                <li><a title="My Account" href="https://genemedicsnutrition.com/wishlist/"><span>Wishlist</span></a></li>
                                <li><a title="My Account" href="https://genemedicsnutrition.com/sales/order/history/"><span>Order History</span></a></li>
                                <li>
                                  <a title="My Account" href="https://genemedicshealth.com/dashboard"><span>Health &amp; Wellness App</span></a>
                                </li>
                              </div>
                            </div>
                          </ul>
                          </li>
                      </ul>
                      </div>
                         
                      <!-- Company links static block -->
                    </div> <!--links-->

                  </div> <!--toplinks-->
                  @else
                    <div class="login">
                      <a href="{{URL::to('/auth/login')}}" class="drop regular strong text-uppercase"><i class="fa fa-user"></i>&nbsp; Log in / Sign Up</a>
                      <!-- <button class="freeconsultbtn"> 
                        Free Consult -->
                    </div>
                  
                  @endif
                
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="bot transition">
        <div class="container-fluid">
          <div class="row">
            <div class="col-xs-12">
              <div class="list-item">
                <div class="item left">
                  <a class="logo" href="{{ env('LIVE_URL') }}">
                                    <img src="/images/genemedics-nutrition.png" width="252" height="34" class="img-responsive">
                   </a>
                  <button type="button" class="mobile-menu navbar-toggle visible-xs visible-sm visible-md" data-toggle="collapse" data-target="sidepanel">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                </div> 
                <div class="item right hidden-xs hidden-sm hidden-md">
                  <div class="menu-main-menu-container">
                    <ul id="menu-main-menu" class="main-menu">

                      @if(Auth::check())
                        @if(Auth::user()->user_type == 'patient')
                          <li id="menu-item-2240" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2240"><a href="https://genemedicsnutrition.com/customer/account/index">My<br> Account</a></li>
                          <li id="menu-item-2239" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-2239"><a href="{{URL::to('/dashboard')}}">Health &#038;<br>Wellness App</a></li>
                          <li id="menu-item-2240" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2240"><a href="https://genemedicsnutrition.com/genemedics_customerhealthinfo">Patient<br> Info</a></li>
                          <li id="menu-item-1736" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1736"><a href="https://genemedicsnutrition.com">Nutritional<br>Supplements</a>
                            <ul class="sub-menu">
                              <li id="menu-item-1738" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1738"><a href="https://genemedicsnutrition.com">Why Choose Us</a></li>
                              <li id="menu-item-1739" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1739"><a href="https://genemedicsnutrition.com/all-products.html/">All Products</a></li>
                              <li id="menu-item-1740" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1740"><a href="https://genemedicsnutrition.com/package-programs.html/">Package Programs</a></li>
                              <li id="menu-item-1741" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1741"><a href="https://genemedicsnutrition.com/hormone-balance.html/">Hormone Balance</a></li>
                              <li id="menu-item-1742" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1742"><a href="https://genemedicsnutrition.com/top-10-products.html/">Top 10 Products</a></li>
                              <li id="menu-item-2596" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2596"><a href="{{ env('LIVE_URL') }}about-ghi/become-a-vip-member/">Become a VIP Member</a></li>
                              <li id="menu-item-1744" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1744"><a href="https://genemedicsnutrition.com/contact-us/">Contact Genemedics Nutrition</a></li>
                            </ul>
                          </li>
                          <!-- <li id="menu-item-2223" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2223"><a href="{{ env('LIVE_URL') }}services/gourmet-foods/">Gourmet<br>Foods</a></li>
                          <li id="menu-item-2226" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2226"><a href="{{ env('LIVE_URL') }}about-ghi/why-choose-ghi/">About<br>GHI</a>
                            <ul class="sub-menu">
                              <li id="menu-item-1166" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1166"><a href="{{ env('LIVE_URL') }}about-ghi/why-choose-ghi/">Why Choose GHI</a></li>
                              <li id="menu-item-2655" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2655"><a href="{{ env('LIVE_URL') }}about-ghi/bioidentical-hormone-replacement-therapy/">Our Services</a>
                                <ul class="sub-menu">
                                  <li id="menu-item-2654" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2654"><a href="{{ env('LIVE_URL') }}about-ghi/bioidentical-hormone-replacement-therapy/">Bioidentical Hormone Replacement Therapy</a></li>
                                  <li id="menu-item-2611" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2611"><a href="{{ env('LIVE_URL') }}about-ghi/lab-testing/">Lab Testing</a>
                                    <ul class="sub-menu">
                                      <li id="menu-item-2612" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2612"><a href="{{ env('LIVE_URL') }}about-ghi/lab-testing/blood-lab-testing/">Blood Lab Testing</a></li>
                                      <li id="menu-item-2613" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2613"><a href="{{ env('LIVE_URL') }}about-ghi/lab-testing/blood-spot-lab-testing/">Blood Spot Lab Testing</a></li>
                                      <li id="menu-item-2609" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2609"><a href="{{ env('LIVE_URL') }}about-ghi/lab-testing/saliva-lab-testing/">Saliva Lab Testing</a></li>
                                    </ul>
                                  </li>
                                  <li id="menu-item-2644" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2644"><a href="{{ env('LIVE_URL') }}about-ghi/weight-loss/">Weight Loss</a></li>
                                  <li id="menu-item-2647" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2647"><a href="{{ env('LIVE_URL') }}about-ghi/the-hcg-diet/">The HCG Diet</a></li>
                                  <li id="menu-item-2637" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2637"><a href="{{ env('LIVE_URL') }}about-ghi/iv-nutrition/">IV Nutrition</a></li>
                                  <li id="menu-item-2650" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2650"><a href="{{ env('LIVE_URL') }}about-ghi/gourmet-foods/">Gourmet Foods</a></li>
                                  <li id="menu-item-2636" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2636"><a href="{{ env('LIVE_URL') }}about-ghi/cutting-edge-health-fitness-app/">Cutting-Edge Health &#038; Fitness App</a></li>
                                  <li id="menu-item-2651" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2651"><a href="{{ env('LIVE_URL') }}about-ghi/life-style-consulting/">Life Style consulting</a>
                                    <ul class="sub-menu">
                                      <li id="menu-item-2652" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2652"><a href="{{ env('LIVE_URL') }}about-ghi/life-style-consulting/nutrition-consulting/">Nutrition Consulting</a></li>
                                      <li id="menu-item-2656" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2656"><a href="{{ env('LIVE_URL') }}about-ghi/life-style-consulting/exercise-consulting/">Exercise Consulting</a></li>
                                      <li id="menu-item-2653" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2653"><a href="{{ env('LIVE_URL') }}about-ghi/life-style-consulting/supplement-consulting/">Supplement Consulting</a></li>
                                    </ul>
                                  </li>
                                </ul>
                              </li>
                              <li id="menu-item-1167" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1167"><a href="{{ env('LIVE_URL') }}about-ghi/ghi-locations/">GHI Locations</a>
                                <ul class="sub-menu">
                                  <li id="menu-item-787" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-787"><a href="http://www.genemedics.com/hormone-replacement-therapy-arizona/">Arizona</a>
                                    <ul class="sub-menu">
                                      <li id="menu-item-876" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-876"><a href="http://www.genemedics.com/hormone-replacement-therapy-arizona/phoenix/">Phoenix</a></li>
                                      <li id="menu-item-877" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-877"><a href="http://www.genemedics.com/hormone-replacement-therapy-arizona/scottsdale/">Scottdale</a></li>
                                      <li id="menu-item-878" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-878"><a href="http://www.genemedics.com/hormone-replacement-therapy-arizona/tucson/">Tucson</a></li>
                                    </ul>
                                  </li>
                                  <li id="menu-item-879" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-879"><a href="http://www.genemedics.com/hormone-replacement-therapy-california/">California</a></li>
                                  <li id="menu-item-883" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-883"><a href="http://www.genemedics.com/hormone-replacement-therapy-florida/">Florida</a></li>
                                  <li id="menu-item-884" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-884"><a href="http://www.genemedics.com/hormone-replacement-therapy-georgia/">Georgia</a></li>
                                  <li id="menu-item-885" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-885"><a href="http://www.genemedics.com/hormone-replacement-therapy-illinois/">Illinois</a></li>
                                  <li id="menu-item-886" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-886"><a href="http://www.genemedics.com/hormone-replacement-therapy-michigan/">Michigan</a></li>
                                  <li id="menu-item-887" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-887"><a href="http://www.genemedics.com/hormone-replacement-therapy-oregon/">Oregon</a></li>
                                  <li id="menu-item-888" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-888"><a href="http://www.genemedics.com/hormone-replacement-therapy-texas/">Texas</a></li>
                                </ul>
                              </li>
                              <li id="menu-item-1168" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1168"><a href="{{ env('LIVE_URL') }}about-ghi/ghi-success-stories/">GHI Success Stories</a>
                                <ul class="sub-menu">
                                  <li id="menu-item-1169" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1169"><a href="{{ env('LIVE_URL') }}about-ghi/ghi-success-stories/womens-success-stories/">Womens Success Stories</a></li>
                                  <li id="menu-item-1170" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1170"><a href="{{ env('LIVE_URL') }}about-ghi/ghi-success-stories/mens-success-stories/">Mens Success Stories</a></li>
                                </ul>
                              </li>
                              <li id="menu-item-1172" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1172"><a href="{{ env('LIVE_URL') }}about-ghi/contact-ghi/">Contact GHI</a></li>
                            </ul>
                          </li>
                          <li id="menu-item-1462" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-1462"><a href="{{ env('LIVE_URL') }}category/health-library/">Health<br>Library</a>
                            <ul class="sub-menu">
                              <li id="menu-item-1478" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-1478"><a href="{{ env('LIVE_URL') }}category/health-library/health-wikipedia/">Health Wikipedia</a>
                                <ul class="sub-menu">
                                  <li id="menu-item-1487" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-1487"><a href="{{ env('LIVE_URL') }}category/health-library/health-wikipedia/hormones/">Hormones</a>
                                    <ul class="sub-menu">
                                      <li id="menu-item-1515" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1515"><a href="{{ env('LIVE_URL') }}category/health-library/health-wikipedia/hormones/testosterone/">Testosterone</a></li>
                                      <li id="menu-item-1490" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1490"><a href="{{ env('LIVE_URL') }}category/health-library/health-wikipedia/hormones/estrogen/">Estrogen</a></li>
                                      <li id="menu-item-1533" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1533"><a href="{{ env('LIVE_URL') }}category/health-library/health-wikipedia/hormones/progesterone/">Progesterone</a></li>
                                      <li id="menu-item-1516" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1516"><a href="{{ env('LIVE_URL') }}category/health-library/health-wikipedia/hormones/thyroid/">Thyroid</a></li>
                                      <li id="menu-item-1491" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1491"><a href="{{ env('LIVE_URL') }}category/health-library/health-wikipedia/hormones/human-growth-hormoneigf-1/">Human Growth Hormone/IGF-1</a></li>
                                      <li id="menu-item-1488" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1488"><a href="{{ env('LIVE_URL') }}category/health-library/health-wikipedia/hormones/cortisol/">Cortisol</a></li>
                                      <li id="menu-item-1489" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1489"><a href="{{ env('LIVE_URL') }}category/health-library/health-wikipedia/hormones/dhea/">DHEA</a></li>
                                      <li id="menu-item-1513" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1513"><a href="{{ env('LIVE_URL') }}category/health-library/health-wikipedia/hormones/pregnenolone/">Pregnenolone</a></li>
                                      <li id="menu-item-1517" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1517"><a href="{{ env('LIVE_URL') }}category/health-library/health-wikipedia/hormones/vitamin-d/">Vitamin D</a></li>
                                    </ul>
                                  </li>
                                  <li id="menu-item-1479" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-1479"><a href="{{ env('LIVE_URL') }}category/health-library/health-wikipedia/hormone-imbalance-conditions/">Hormone Imbalance Conditions</a>
                                    <ul class="sub-menu">
                                      <li id="menu-item-1481" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1481"><a href="{{ env('LIVE_URL') }}category/health-library/health-wikipedia/hormone-imbalance-conditions/andropause-low-t-in-men/">Andropause &#8211; Low T in Men</a></li>
                                      <li id="menu-item-1484" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1484"><a href="{{ env('LIVE_URL') }}category/health-library/health-wikipedia/hormone-imbalance-conditions/menopause-transition-in-women/">Menopause Transition in Women</a></li>
                                      <li id="menu-item-1644" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1644"><a href="{{ env('LIVE_URL') }}category/health-library/health-wikipedia/hormone-imbalance-conditions/growth-hormone-defeciency/">Growth Hormone Defeciency</a></li>
                                      <li id="menu-item-1714" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1714"><a href="{{ env('LIVE_URL') }}category/health-library/health-wikipedia/hormone-imbalance-conditions/hypothyroidism/">Hypothyroidism</a></li>
                                      <li id="menu-item-1713" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1713"><a href="{{ env('LIVE_URL') }}category/health-library/health-wikipedia/hormone-imbalance-conditions/adrenal-fatigue/">Adrenal Fatigue</a></li>
                                    </ul>
                                  </li>
                                </ul>
                              </li>
                              <li id="menu-item-1670" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-1670"><a href="{{ env('LIVE_URL') }}category/health-library/health-blog/">Health Blog</a>
                                <ul class="sub-menu">
                                  <li id="menu-item-1673" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-1673"><a href="{{ env('LIVE_URL') }}category/health-library/health-blog/hormones-health-blog/">Hormones</a>
                                    <ul class="sub-menu">
                                      <li id="menu-item-1694" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1694"><a href="{{ env('LIVE_URL') }}category/health-library/health-blog/hormones-health-blog/testosterone-hormones-health-blog/">Testosterone</a></li>
                                      <li id="menu-item-1680" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1680"><a href="{{ env('LIVE_URL') }}category/health-library/health-blog/hormones-health-blog/estrogen-hormones-health-blog/">Estrogen</a></li>
                                      <li id="menu-item-1693" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1693"><a href="{{ env('LIVE_URL') }}category/health-library/health-blog/hormones-health-blog/progesterone-hormones-health-blog/">Progesterone</a></li>
                                      <li id="menu-item-1685" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1685"><a href="{{ env('LIVE_URL') }}category/health-library/health-blog/hormones-health-blog/human-growth-hormoneigf-1-hormones-health-blog/">Human Growth Hormone/IGF-1</a></li>
                                      <li id="menu-item-1695" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1695"><a href="{{ env('LIVE_URL') }}category/health-library/health-blog/hormones-health-blog/thyroid-hormones-health-blog/">Thyroid</a></li>
                                      <li id="menu-item-1683" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1683"><a href="{{ env('LIVE_URL') }}category/health-library/health-blog/hormones-health-blog/cortisol-hormones-health-blog/">Cortisol</a></li>
                                      <li id="menu-item-1684" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1684"><a href="{{ env('LIVE_URL') }}category/health-library/health-blog/hormones-health-blog/dhea-hormones-health-blog/">DHEA</a></li>
                                      <li id="menu-item-1697" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1697"><a href="{{ env('LIVE_URL') }}category/health-library/health-blog/hormones-health-blog/pregnenolone-hormones-health-blog/">Pregnenolone</a></li>
                                      <li id="menu-item-1696" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1696"><a href="{{ env('LIVE_URL') }}category/health-library/health-blog/hormones-health-blog/vitamin-d-hormones-health-blog/">Vitamin D</a></li>
                                    </ul>
                                  </li>
                                  <li id="menu-item-1672" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-1672"><a href="{{ env('LIVE_URL') }}category/health-library/health-blog/hormone-imbalance-conditions-health-blog/">Hormone Imbalance Conditions</a>
                                    <ul class="sub-menu">
                                      <li id="menu-item-1677" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1677"><a href="{{ env('LIVE_URL') }}category/health-library/health-blog/hormone-imbalance-conditions-health-blog/andropause-low-t-in-men-hormone-imbalance-conditions-health-blog/">Andropause &#8211; Low T in Men</a></li>
                                      <li id="menu-item-1678" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1678"><a href="{{ env('LIVE_URL') }}category/health-library/health-blog/hormone-imbalance-conditions-health-blog/menopause-transition-in-women-hormone-imbalance-conditions-health-blog/">Menopause Transition in Women</a></li>
                                      <li id="menu-item-1711" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1711"><a href="{{ env('LIVE_URL') }}category/health-library/health-blog/hormone-imbalance-conditions-health-blog/growth-hormone-defeciency-hormone-imbalance-conditions-health-blog/">Growth Hormone Defeciency</a></li>
                                      <li id="menu-item-1712" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1712"><a href="{{ env('LIVE_URL') }}category/health-library/health-blog/hormone-imbalance-conditions-health-blog/hypothyroidism-hormone-imbalance-conditions-health-blog/">Hypothyroidism</a></li>
                                      <li id="menu-item-1710" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1710"><a href="{{ env('LIVE_URL') }}category/health-library/health-blog/hormone-imbalance-conditions-health-blog/adrenal-fatigue-hormone-imbalance-conditions-health-blog/">Adrenal Fatigue</a></li>
                                    </ul>
                                  </li>
                                  <li id="menu-item-1674" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1674"><a href="{{ env('LIVE_URL') }}category/health-library/health-blog/nutrition/">Nutrition</a></li>
                                  <li id="menu-item-1675" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1675"><a href="{{ env('LIVE_URL') }}category/health-library/health-blog/nutritional-supplements/">Nutritional Supplements</a></li>
                                  <li id="menu-item-1671" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1671"><a href="{{ env('LIVE_URL') }}category/health-library/health-blog/exercise/">Exercise</a></li>
                                </ul>
                              </li>
                            </ul>
                          </li> -->
                          <li><a href="{{URL::to('/'.auth()->user()->user_type.'/'.auth()->user()->id.'/demographics')}}" class="contactmenu hidden-lg">PROFILE</a></span></li>
                          <li><a href="{{URL::to('/auth0/logout')}}" class="contactmenu hidden-lg">LOG OUT</a></span></li>
                        @else
                          <li id="menu-item-2239" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-2239"><a href="{{URL::to('/dashboard')}}">Health &#038;<br>Wellness App</a></li>
                        @endif
                      @else
                      
                          <li id="menu-item-1346" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1346"><a href="{{ env('LIVE_URL') }}health-wellness-app/home/">About Us</a></li>
                          <li id="menu-item-1350" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1350"><a href="{{ env('LIVE_URL') }}health-wellness-app/why-choose-us/">Why Choose Us</a></li>
                          <li id="menu-item-1643" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-1639 current_page_item menu-item-1643"><a href="{{URL::to('/auth/login')}}">Log In / Sign Up</a></li>
                          <li id="menu-item-1352" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1352"><a href="{{ env('LIVE_URL') }}health-wellness-app/contact-us/">Contact Us</a></li>
                   

                      @endif

                    </ul>
                  </div>                
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    
    