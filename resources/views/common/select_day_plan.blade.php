<div class="block md_recommended_proui" ng-init="view_plan.getMDTemplate();">
     <div id="all_templates_div">

             <div class="row text-center main-top">

                 <div class="col-md-6">
                     <div class="block">
                         <div class="block-title">
                             <p class="select-create-plan">Select/Create Day Plan</p>
                         </div>

                         <div class="row">
                             <div class="col-md-12">
                                <div class="row"><p class="sub-title">MEAL TEMPLATES</p></div>

                                 <div class="row">
                                     <div class="table-responsive">
                                         <table class="table table-bordered">
                                             <tbody ng-repeat="m_temp in view_plan.master_templates | filter:{parent:'1'}">
                                             <tr>
                                                 <th colspan="3" class="table-title">@{{m_temp.template_name}}</th>
                                             </tr>
                                             <tr>
                                                 <td class="table-sub">
                                                   
                                                     <select class="form-control"
                                                              ng-init="view_plan.mas_temp[m_temp.id].meal_id = 0"    
                                                              ng-model="view_plan.mas_temp[m_temp.id].meal_id"
                                                              style="width:220px;"
                                                              ng-if="m_temp.type == 'Meal' && view_plan.meal_templates.length"
                                                              ng-options="mt.id as mt.template_name for mt in view_plan.meal_templates"
                                                                ng-change="view_plan.changedMeal(m_temp);">
                                                    </select>

                                                      <select class="form-control"
                                                          ng-init="view_plan.mas_temp[m_temp.id].workout_id = 0"
                                                          ng-model="view_plan.mas_temp[m_temp.id].workout_id"
                                                          ng-if="m_temp.type == 'Workout' && view_plan.workouts_templates.length" 
                                                          style="width:220px;"
                                                          ng-options="wt.id as wt.template_name for wt in view_plan.workouts_templates"
                                                          ng-change="view_plan.changedWorkout(m_temp);"
                                                            >
                                                    </select>   

                                                 </td>
                                                 <td class="table-sub">
                                                     <a href="md_meals#/create-meal-items?new_meal_id=@{{view_plan.new_meal_id}}"  ng-if="m_temp.type == 'Meal'">Create Meal</a>

                                                     <a href="workout#/add-workout" ng-if="m_temp.type == 'Workout'">Create Workout</a>
                                                 </td>
                                                 <td class="table-sub">
                                                   <!--   <a href="javascript:void(0);" ng-click="view_plan.openDayTimeModel(m_temp,$index);">Select Times &amp; Days</a> -->
                                                     <time-picker
                                                      ng-model="view_plan.mas_temp[m_temp.id].meal_time"
                                                        ng-change="view_plan.timeDisplayOnCal(m_temp);"
                                                      ></time-picker>
                                                 </td>
                                             </tr>
                                             
                                             </tbody>
                                         </table>
                                     </div><!--end of .table-responsive-->
                                 </div>

                             </div>
                         </div>
                     </div>
                 </div>
                 <div class="col-md-6 ">
                     <div class="preview">
                         Preview
                     </div>


                     <div class="row ">
                         <div ui-calendar="view_plan.uiConfig.calendar"
                              ng-model="view_plan.eventSources"
                              calendar="nutrition_calendar"
                              >
                         </div>

                     </div>
                 </div>
             </div>

     </div>

 </div>


 @include('common/meal_details')
@include('common/workout_details')