<div class="block md_recommended_proui" ng-init="view_plan.getMDTemplate();">
    <div id="all_templates_div">

        <div class="row text-center main-top">

            <div class="col-md-6">
                <div class="block">
                    <div class="block-title">
                        <p class="select-create-plan">Select/Create Complete Plan</p>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="row"><p class="sub-title">COMPLETE PLAN TEMPLATES</p></div>

                            <div class="row plan-tem-bg">

                                <div class="col-md-5 col-sm-5 col-xs-12"><p class="choose-plan">Choose a Plan Template</p>
                                </div>

                                <div class="col-md-7 col-sm-7 col-xs-12 mrg-top-10">
                                    <select class="form-control"
                                            ng-if="view_plan.meal_plans.length"
                                            ng-options="mp.id as mp.name for mp in view_plan.meal_plans"
                                            ng-model="view_plan.selected_plan_id"
                                            ng-change="view_plan.changedPlan();"
                                    >
                                    </select>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 select-day">
                                    <div>Day Plan Template</div>
                                </div>

                                <div class="row" ng-repeat="sdp in view_plan.select_day_plans">
                                    <div class="col-sm-12 col-md-12">
                                    <div class="col-md-5 mrg-top-10 ">
                                        <select class="form-control"
                                                ng-if="view_plan.day_plan_templates.length"
                                                ng-model="sdp.day_plan_id"
                                                ng-options="dp.id as dp.name for dp in view_plan.day_plan_templates"

                                                style="width:220px;">

                                        </select>
                                    </div>
                                    <div class="col-md-3 mrg-top-10 ">
                                        <a href="/admin/day_meal_plans">Create Day Plan</a>
                                    </div>
                                    <div class="col-md-3 mrg-top-10 ">
                                        <a href="javascript:void(0);" ng-click="view_plan.openDayModel(sdp);">Select
                                            Days</a>
                                    </div>
                                    <div class="col-md-1">
                                             <span  ng-show="view_plan.select_day_plans.length >=2 && $index >=1">
                                             <button class="btn btn-sm btn-danger" type="button"
                                                     ng-click="view_plan.removeDayPlan(sdp);">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                       </span>
                                    </div>
                                    </div>





                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12 pull-right">
                                        <div class="col-md-12">
                                        <button class="btn btn-sm btn-primary pull-right" type="button"
                                                ng-click="view_plan.addDayPlan();">Add Day Plan
                                        </button>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row"><p class="sub-title">MEAL TEMPLATES</p></div>

                            <div class="row">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tbody ng-repeat="m_temp in view_plan.master_templates | filter:{parent:'1'}">
                                        <tr>
                                            <th colspan="3" class="table-title">@{{m_temp.template_name}}</th>
                                        </tr>
                                        <tr>
                                            <td class="table-sub">

                                                <select class="form-control"
                                                        ng-init="view_plan.mas_temp[m_temp.id].meal_id = 0"
                                                        ng-model="view_plan.mas_temp[m_temp.id].meal_id"
                                                        style="width:220px;"
                                                        ng-if="m_temp.type == 'Meal' && view_plan.meal_templates.length"
                                                        ng-options="mt.id as mt.template_name for mt in view_plan.meal_templates"
                                                        ng-change="view_plan.changedMeal(m_temp);"
                                                >

                                                </select>

                                                <select class="form-control"
                                                        ng-init="view_plan.mas_temp[m_temp.id].workout_id = 0"
                                                        ng-model="view_plan.mas_temp[m_temp.id].workout_id"
                                                        ng-if="m_temp.type == 'Workout' && view_plan.workouts_templates.length"
                                                        style="width:220px;"
                                                        ng-options="wt.id as wt.template_name for wt in view_plan.workouts_templates"
                                                        ng-change="view_plan.changedWorkout(m_temp);"
                                                >
                                                </select>

                                            </td>
                                            <td class="table-sub">
                                                <a href="/admin/md_meals#/create-meal-items?new_meal_id=@{{view_plan.new_meal_id}}"
                                                   ng-if="m_temp.type == 'Meal'">Create Meal</a>

                                                <a href="/admin/workout#/add-workout" ng-if="m_temp.type == 'Workout'">Create
                                                    Workout</a>
                                            </td>
                                            <td class="table-sub">
                                                <a href="javascript:void(0);"
                                                   ng-click="view_plan.openDayTimeModel(m_temp,$index);">Select Times
                                                    &amp; Days</a>


                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div><!--end of .table-responsive-->
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="preview">
                    Preview
                </div>


                <div class="row ">
                    <div ui-calendar="view_plan.uiConfig.calendar"
                         ng-model="view_plan.eventSources"
                         calendar="nutrition_calendar"
                    >
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>

@include('common/days_and_time')
@include('common/meal_details')
@include('common/workout_details')
@include('common/select_days')