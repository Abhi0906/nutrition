<!-- Workout details  Modal -->
    <div id="workoutDetailsModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog" style="width: 920px;">
        <div class="modal-content">
          <div class="modal-header bg-primary">
            <button class="close white_close" type="button" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">@{{view_plan.template_name}} - @{{view_plan.template_time}}</h4>
            
          </div>
          <div class="modal-body">
             <div class="block full">
                  <div class="block-title">
                      <h2><strong ng-bind-html="view_plan.getWorkoutTitle(view_plan.workout);"></strong></h2>
                      <div class="block-options pull-right">
                  </div>
                  </div>
                  <div class="gallery container-fluid">
                          <div class="row">
                                <div class="col-xs-12 col-sm-4" ng-repeat="exercise in view_plan.workout.exercise_lists">                     
                                  
                                  <single-video-image  not-editable not-deletable
                                    view = "vm.viewExercise(exercise)"
                                    videoimage="exercise"
                                    width="250" height="150"
                                    >                            
                                  </single-video-image>
                          </div>
                          </div>
                  </div>
            </div>
          </div>
          
          <div class="saperator_5"></div>
        </div>
      </div>
    </div>
<!--  Workout details Modal --> 