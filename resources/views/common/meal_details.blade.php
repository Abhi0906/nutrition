<!-- Meal details  Modal -->
    <div id="mealDetailsModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog" style="width: 920px;">
        <div class="modal-content">
          <div class="modal-header bg-primary">
            <button class="close white_close" type="button" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">@{{view_plan.template_name}} - @{{view_plan.template_time}}</h4>
            
          </div>
          <div class="modal-body">
            <div class="md_rec_food_class" id="mdrecommeded_meal_table">
              <table class="table recommeded_table">
                <thead>   
                  <tr>
                    <td class="no_padding">
                      <table class="table no_border margin_bottm_0">
                        <tr>
                          <th class="width_34">&nbsp;</th>
                          <th class="text-center bg-primary width_11">CALORIES</th>
                          <th class="text-center bg-primary width_11">SERVING SIZE</th>
                          <th class="text-center bg-primary width_11">NO. Of SERVING</th>
                          <th class="text-center bg-primary width_11">FAT</th>
                          <th class="text-center bg-primary width_11">FIBER</th>
                          <th class="text-center bg-primary width_11">CARBS</th>
                          <th class="text-center bg-primary width_11">SODIUM</th>
                          <th class="text-center bg-primary width_11">PROTEIN</th>
                               
                        </tr>
                      </table>
                    </td>
                  </tr>
                </thead>
                <tbody>
                  <tr ng-repeat="mealItem in view_plan.meal_item_data" class="tr_no_border">
                    <td class="no_padding">
                      <table class="table no_border margin_bottm_0">
                        <tr class="subhead-mdrec">
                          <td data-title=" " colspan="9">
                            <h4><span class="uppercase">@{{mealItem.item_name}}</span></h4>
                            <ul class="list-inline">
                              
                          </td>
                        </tr>
                        <tr ng-repeat="food in mealItem.meal_item_foods">
                          <td class="width_34" data-title="NAME">
                            
                            <a href="javascript:void(0);">
                              <!-- <i class="fa fa-pencil-square fa-gray"></i> -->
                              @{{food.food_name}}
                            </a>                         
                          </td>
                          <td data-title="CALORIES" class="text-center width_11">@{{food.calories}}</td>
                           <td data-title="SERVING SIZE" class="text-center width_11">@{{food.serving_quantity}}</td> 
                           <td data-title="NO. Of SERVING" class="text-center width_11">@{{food.no_of_servings}}</td> 
                           <td data-title="FAT" class="text-center width_11">@{{food.total_fat}}</td>
                           <td data-title="FIBER" class="text-center width_11">@{{food.dietary_fiber}}</td>
                           <td data-title="CARBS" class="text-center width_11">@{{food.total_carb}}</td>
                           <td data-title="SODIUM" class="text-center width_11">@{{food.sodium}}</td>
                           <td data-title="PROTEIN" class="text-center width_11">@{{food.protein}}</td> 
                           
                          
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td class="no_padding">
                      <table class="table no_border margin_bottm_0">
                        <tr>
                          <td class="width_34" data-title="NAME">
                            <a href="javascript:void(0);">
                              <strong>Total</strong>
                            </a>                         
                          </td>
                          <td data-title="CALORIES" class="text-center width_11">@{{view_plan.totalMealItemData.total_calories}}</td>
                          <td data-title="SERVING SIZE" class="text-center width_11">-</td> 
                          <td data-title="SERVING SIZE" class="text-center width_11">-</td> 
                           <td data-title="SERVING SIZE" class="text-center width_11">@{{view_plan.totalMealItemData.total_fat}}</td> 
                           <td data-title="NO. Of SERVING" class="text-center width_11">@{{view_plan.totalMealItemData.total_fiber}}</td> 
                           <td data-title="FAT" class="text-center width_11">@{{view_plan.totalMealItemData.total_carb}}</td>
                           <td data-title="FIBER" class="text-center width_11">@{{view_plan.totalMealItemData.total_sodium}}</td>
                           <td data-title="CARBS" class="text-center width_11">@{{view_plan.totalMealItemData.total_protein}}</td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          
          <div class="saperator_5"></div>
        </div>
      </div>
    </div>
<!--  Meal details Modal --> 