<!-- Select Days and Time Modal -->
    <div id="daysModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-primary">
            <button class="close white_close" type="button" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Day Plan Template</h4>
            
          </div>
          <div class="modal-body">
            
              <form  method="post" class="form-horizontal" onsubmit="return false;" _lpchecked="1">
                   <div class="form-group">
                    <label class="checkbox-custom-label"> Select Days</label>
                   </div>
                    <div class="form-group">

                      <span  ng-repeat= "(key, week_day) in view_plan.week">
                          <input id="checkbox-@{{key}}"
                                 class="checkbox-custom"
                                 name="checkbox-@{{key}}"
                                 type="checkbox"
                                 checklist-value="week_day.dayNumber"
                                 checklist-model="view_plan.selectDayPlanObj.day_plan_days"
                                 >
                          <label for="checkbox-@{{key}}" class="checkbox-custom-label"> @{{week_day.dayName}}</label>
                      </span>
                    </div>  
                    
                    <div class="form-group form-actions">
                        <div class="col-xs-12 col-md-6 col-md-offset-3">
                           <button type="button" class="btn btn-md btn-default" ng-click="view_plan.displayDayPlanCal(view_plan.selectDayPlanObj);">OK</button>
                           <button type="button" class="btn btn-md btn-default" data-dismiss="modal">Cancel</button>

                        </div>
                    </div>
                </form>
            

          </div>
          
          <div class="saperator_5"></div>
        </div>
      </div>
    </div>
<!--  Select Days and Time Modal --> 
