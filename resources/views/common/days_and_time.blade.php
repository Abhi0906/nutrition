<!-- Select Days and Time Modal -->
    <div id="daysTimeModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-primary">
            <button class="close white_close" type="button" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">@{{view_plan.selectTemplate.template_name}}</h4>
            
          </div>
          <div class="modal-body">
            
              <form  method="post" class="form-horizontal" onsubmit="return false;" _lpchecked="1">
                   
                   <div class="form-group" >
                   
                     <div class="col-md-12"  ng-show="view_plan.selectTemplate.tech_name == 'pre_workout_meal'">
                      
                      <input id="checkbox-pre_workout_meal"
                                       class="checkbox-custom"
                                       name="checkbox-pre_workout_meal"
                                       type="checkbox"
                                       ng-model="view_plan.atuo_timer_pre_workout"
                                        ng-true-value="1" ng-false-value="0"
                       >  

                        <label for="checkbox-@{{view_plan.selectTemplate.tech_name}}" class="checkbox-custom-label"> Auto Timer</label>
                     </div>

                     <div class="col-md-12"  ng-show="view_plan.selectTemplate.tech_name == 'meal_1'">
                      
                      <input id="checkbox-meal_1"
                                       class="checkbox-custom"
                                       name="checkbox-meal_1"
                                       type="checkbox"
                                       ng-model="view_plan.atuo_timer_meal_1"
                                        ng-true-value="1" ng-false-value="0" style="z-index: 99999;"
                       >  

                        <label for="checkbox-@{{view_plan.selectTemplate.tech_name}}" class="checkbox-custom-label"> Auto Timer</label>
                     </div>
                    
                     
                   </div>

                    <div class="form-group" ng-repeat= "(key, week_day) in view_plan.masterTemplateWeeks">
                        <div class="col-md-3">
                         
                           <input id="checkbox-@{{week_day.dayName}}"
                                       class="checkbox-custom"
                                       name="checkbox-@{{week_day.dayName}}"
                                       type="checkbox"
                                       checklist-value="week_day.dayNumber"
                                       ng-init="view_plan.template[week_day.master_template_id].meal_day = view_plan.meal_days[week_day.master_template_id]" 
                                       checklist-model="view_plan.template[week_day.master_template_id].meal_day"
                                       ng-click="view_plan.checkAll(week_day.dayNumber);"
                                       >
                         
                          <label for="checkbox-@{{week_day.dayName}}" class="checkbox-custom-label"> @{{week_day.dayName}}</label>
                        </div>
                        <div class="col-md-5">
                            
                            <time-picker ng-init="view_plan.template[week_day.master_template_id][week_day.dayNumber].meal_time = view_plan.meal_time[week_day.master_template_id][week_day.dayNumber]"
                                         ng-model="view_plan.template[week_day.master_template_id][week_day.dayNumber].meal_time"
                                         ng-change="view_plan.changeAllTime(week_day.dayNumber);"
                                         ></time-picker>
                        </div>
                        <div class="col-md-4">
                          

                          <label class="switch">
                          <input  id="notification-@{{week_day.dayName}}"
                                  
                                  name="notification-@{{week_day.dayName}}" 
                                  type="checkbox"
                                  checklist-value="week_day.dayNumber"
                                  ng-init="view_plan.template[week_day.master_template_id].notification = view_plan.selectNotifications[week_day.master_template_id]"
                                  checklist-model="view_plan.template[week_day.master_template_id].notification"
                                  ng-click="view_plan.checkAllNotification(week_day.dayNumber);">
                            <span class="slider round"></span>
                          </label>
                        </div>
                    </div>
                    
                    <div class="form-group form-actions">
                        <div class="col-xs-12 col-md-6 col-md-offset-3">
                           <button type="button" class="btn btn-md btn-default" ng-click="view_plan.creatTemplateData();">OK</button>
                           <button type="button" class="btn btn-md btn-default" data-dismiss="modal">Cancel</button>

                        </div>
                    </div>
                </form>
            

          </div>
          
          <div class="saperator_5"></div>
        </div>
      </div>
    </div>
<!--  Select Days and Time Modal --> 
