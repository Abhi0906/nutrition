<div class="col-xs-12">
<div class="row block-title patient_title">
            <div class="col-sm-2">
                <h3><strong> Exercise Image </strong></h3>
            </div>  
            <div class="col-sm-10 text-alignRight">
             <h3>
               <div class="form-group">
                   <button class="btn btn-default" ng-disabled="exerciseForm.$invalid" 
                   type="button" ng-click="add_exercise.saveExerciseImage();">
                        <i class="fa gi gi-disk_saved"></i> Save & Close
                   </button>
                    <a ui-sref="list-exercise-images" class="btn btn-default"> <i class="gi gi-remove_2"></i>
                    &nbsp;Cancel</a>
                </div>
             </h3>
           </div>
</div>
    <div class="block">
    	<!-- Updates Title -->
    	
    	<!-- END Updates Title -->

    		
    	<div class="row">
            <div class="col-md-6">
                <!-- Basic Form Elements Block -->
                <div class="block">
                    <!-- Basic Form Elements Title -->
                    <div class="block-title">
                       
                        <h2><strong>Information</strong></h2>
                    </div>
                    <!-- END Form Elements Title -->

                    <!-- Basic Form Elements Content -->
                    <form name="exerciseForm" novalidate class="form-bordered" enctype="multipart/form-data">
                       
                        <div class="form-group">
                            <label for="exercise_name_id">Exercise Name
    							<span class="text-danger">*</span>
                            </label>
                            <input type="text"
                                   placeholder="Exercise Name"
                                   class="form-control" 
                            		name="exercise_name"
                                    ng-model="add_exercise.name" 
                                    id="exercise_name_id" required>
                            <span class="text-danger" ng-show="exerciseForm.exercise_name.$invalid && exerciseForm.exercise_name.$touched">
                            You must fill out your valid exercise name.</span>
                        </div>
                        
                        <div class="form-group">
                            <label for="exercise_type_id">Exercise Category
                                <span class="text-danger">*</span>
                            </label>
                            <select 
                            chosen="vm.exercise_categories"
                            data-placeholder="Choose a Exercise Category"
                            ng-options="value for (key,value) in vm.exercise_categories"
                            ng-model="add_exercise.exercise_type"
                            id="exercise_type_id"
                            name="exercise_type"
                            required>
                          </select>
                            <span class="text-danger" ng-show="exerciseForm.exercise_type.$invalid && exerciseForm.exercise_type.$touched">
                            You must select your exercise catories.</span>
                        </div>

                        <div class="form-group">
                            <label for="exercise_minute_id">Exercise Time in Minutes
    						<!-- <span class="text-danger">*</span> -->	
                            </label>
                            <input type="text"
                                    ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/"
                                    placeholder="Exercise Minutes"
                                    class="form-control"
                                    name="exercise_time"
                                    ng-model="add_exercise.exercise_time"
                                    number-validation="number-validation"  
                                    id="exercise_minute_id"
                                  
                                    >
                            <span class="text-danger"
                                  ng-show="exerciseForm.exercise_time.$invalid && exerciseForm.exercise_time.$touched"  
                             >You must fill out your valid exercise minutes.</span>
                        </div>

                        <div class="form-group">
                            <label for="calorie_burn_id">Calorie Burn
    							<!-- <span class="text-danger">*</span> -->
                            </label>
                            <input type="text"
                                    ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/"
                                    placeholder="Calorie Burn"
                                    class="form-control"
                                    name="calorie_burn"
                                    ng-model="add_exercise.calorie_burn"
                                    number-validation="number-validation"
                                    id="calorie_burn_id"
                                    >
                            <span class="text-danger"
                                   ng-show="exerciseForm.calorie_burn.$invalid && exerciseForm.calorie_burn.$touched">
                                 You must fill out your valid exercise calorie burn.
                            </span>
                        </div>

                         <div class="form-group">
                            <label for="exercise_body_part_id">Body Parts</label>
                            <span class="text-danger">*</span>
    						<select 
    			        	chosen="vm.body_parts"
    	                    data-placeholder="Choose a Body Parts"
    	                    ng-options="body.name as body.name group by body.parent_name for body in vm.body_parts"
    	                    ng-model="add_exercise.body_part"
                            id="exercise_body_part_id"
                            name="body_part"
                            required>
    	                  </select>		
                            
                            <span class="text-danger"
                                   ng-show="exerciseForm.body_part.$invalid && exerciseForm.body_part.$touched">
                                 You must select your body part.
                            </span>

                        </div>

                        <div class="form-group">
                            <label for="exercise_level_id">Experience Levels</label>
    						<select 
    			                    chosen="vm.experiences"
    			                    data-placeholder="Choose Experience Level"
    			                    ng-options="value for (key,value) in vm.experiences"
    			                    ng-model="add_exercise.experience"
                                    id="exercise_level_id">
    	                	</select>		
                            <span class="help-block hide">Please enter your password</span>
                        </div>

                         <div class="form-group">
                            <label for="exercise_equipments_id">Equipments</label>
    						<select 
    		                    chosen="vm.equipments"
    		                    data-placeholder="Choose a Equipment"
    		                    ng-options="equipment.display_name as equipment.display_name for equipment in vm.equipments"
    		                    ng-model="add_exercise.equipment"
                                id="exercise_equipments_id" >
    		                </select>		
                            <span class="help-block hide">Please enter your password</span>
                        </div>

                         <div class="form-group">
                            <label for="exercise_desc_id">Description</label>
    						  <textarea placeholder="Description.."
                               class="form-control" 
                               rows="5" 
                               name="example-textarea-input" 
                               ng-model="add_exercise.description"
                               id="exercise_desc_id"></textarea>
                              <span class="help-block hide">Please enter your password</span>
                        </div>
                       
                        
                    </form>
                    <!-- END Basic Form Elements Content -->
                </div>
                <!-- END Basic Form Elements Block -->
            </div>
            <div class="col-md-6">
                <div class="block">
                    <div class="block-title">
                        <h2><strong>Exercise Image</strong></h2>
                    </div>
    				<div class="dropzone" file="add_exercise.image" exercise-dropzone >
                         <div class="dz-default dz-message"><span>Drop Image here to upload</span></div>                     
                    </div>
                   
                </div>

                <div class="block">
                    <div class="block-title">
                    <h2><strong>Steps</strong></h2>
                    </div>
                     <form  class="form-bordered">
                        <div class="form-group" ng-repeat="step in add_exercise.steps">
                        	<label for="exercise_step">Step-@{{$index+1}}</label>
                            <input type="text" ng-model="step.desc" class="form-control" name="">
                             <button class="btn btn-sm btn-danger"
                              type="button"
    						  ng-show="$last" ng-click="add_exercise.removeStep();"	

                              ><i class="fa fa-minus"></i></button>
                        </div>
    					<div class="form-group form-actions">
                            <button class="btn btn-sm btn-primary" type="button" ng-click="add_exercise.addNewStep();">Add Step</button>
                        </div>	
    				</form>
                </div>

                
            </div>
        </div>

    	<div class ="row">
            <div class="col-md-12">
                <div class="form-bordered">
                    <div class="form-group form-actions">
                        <div class="col-md-12 col-md-offset-5">
                            <button class="btn btn-default" ng-disabled="exerciseForm.$invalid" type="button" ng-click="add_exercise.saveExerciseImage();">
                                <i class="fa fa-angle-right"></i> Save
                            </button>
                            <button class="btn btn-default" type="button" ng-click="demographics.reset()">
                                <i class="fa fa-repeat"></i> Reset
                            </button>
                            <span class="text-danger" ng-hide="demographicsForm.$valid">&nbsp;&nbsp;(Fields with * are mandatory.)</span>
                        </div>
                    </div>
               </div>
            </div> 

        </div>
    </div>
</div>