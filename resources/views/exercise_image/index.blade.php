@extends('layout.app')

@section('content')
    <div ng-controller="ExerciseImageController as vm">
      <!-- Main Row -->
    <div class="row" >
        <div class="col-xs-12">
            <div class="row">
              <div ui-view></div>
            </div>
        </div>
    </div>
    
</div>

    
    <!-- END Main Row -->


@endsection
@section('page_scripts')
{!! HTML::script("js/controllers/ExerciseImageController.js") !!}
{!! HTML::script("js/controllers/ExerciseImageListController.js") !!}
{!! HTML::script("js/controllers/ExerciseImageAddController.js") !!}
{!! HTML::script("js/controllers/ExerciseImageEditController.js") !!}

{!! HTML::script("js/directives/chosen.js") !!}
{!! HTML::script("js/directives/exerciseDropzone.js") !!}
{!! HTML::script("js/directives/singleImage.js") !!}


@append

@section('jquery_ready')

$('.scroll_column').slimScroll({
    height: $(window).height()*.63,

});

@append