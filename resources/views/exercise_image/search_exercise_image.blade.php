<div class="block">
	<div class="block-title">
	      <div class="block-options pull-right">
                    <a title="" data-toggle="button" class="btn btn-alt btn-xs btn-default enable-tooltip" href="javascript:void(0)" ng-click="list_exercise.clearFilter()" data-original-title="Clear Filter">Clear Filter</a>
          </div> 
	    <h2><i class="fa fa-search fa-fw"></i> <strong>FIND EXERCISE</strong></h2>
	</div>
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="" style="border:none">
	            <label for="keywords">Exercise Name</label>
	            <input type="text"
						id="keywords"
						name="keywords"
						ng-model="keyword"
						class="form-control"
						placeholder="enter search term(s)"
				>
	        </div>	
		</div>

		<div class="row-fluid">
			<div class="" style="border:none">
	            <label class="space-top">Exercise Categories</label> 
	                <br>
	                <div>       
	                   <select 
                            chosen="vm.exercise_categories"
                            data-placeholder="Choose a Exercise Category"
                            ng-options="value for (key,value) in vm.exercise_categories"
                            ng-model="exercise_type"
                            id="exercise_type_id"
                            name="exercise_type"
                            ></select>
		            </div>
		    </div>		
		</div>
		<div class="row-fluid">
					<div class="" style="border:none">
			            <label class="space-top">Exercise time minutes</label>
			            <div >             
			            	<div class="col-xs-6">            		
						            <small>Minimum</small>
						            <input type="text" 
						              ng-model="minimun_exercise_time"
						             id="min" 
						             name="min" 
						             class="form-control"
						              placeholder="">		        
			            	</div>
			            	<div class="col-xs-6">            		
						        <small>Maximum</small>
						        <input type="text"
						         id="max" 
						         name="max"
						          ng-model="maximum_exercise_time" 
						          class="form-control"
						           placeholder="">		        
			            	</div>
			            </div>	
			        </div>		
		</div>	
		<div class="row-fluid">
			<div class="" style="border:none">
	            <label class="space-top">Calorie burn</label> 
	            <div >           
	            	<div class="col-xs-6">            		
			            <small>Minimum</small>
			            <input type="text"
			             id="minCal"
			              name="minCal"
			               ng-model="minimun_calorie_burn" 
			               class="form-control"
			                placeholder="">	        
	            	</div>
	            	<div class="col-xs-6">            		
				        <small>Maximum</small>
				        <input type="text"
				         id="maxCal"
				          name="maxCal" 
				          ng-model="maximum_calorie_burn" 
				          class="form-control"
				           placeholder="">	        
	            	</div>
	            </div>	
	        </div>		
		</div>
		<div class="row-fluid">
			<div class="" style="border:none">
	            <label class="space-top">Body Parts</label> 
	                <br>
	                <div>       
			        	<select 
			        	chosen="vm.body_parts"
	                    data-placeholder="Choose a Body Parts"
	                    ng-options="body.name as body.name group by body.parent_name for body in vm.body_parts"
	                    ng-model="body_part" >
	                  </select>
		            </div>
		    </div>		
		</div>
		<div class="row-fluid">
			<div class="" style="border:none">
	            <label class="space-top">Experience Levels</label> 
	                <br>
	                <div >       
			        	<select 
	                    chosen="vm.experiences"
	                    data-placeholder="Choose Experience Level"
	                    ng-options="value for (key,value) in vm.experiences"
	                    ng-model="experience" >
	                </select>
		            </div>
		    </div>		
		</div>
		<div class="row-fluid">
			<div class="" style="border:none">
	            <label class="space-top">Equipments</label>
				<br>
	            <div>    
	                <select 
	                    chosen="vm.equipments"
	                    data-placeholder="Choose a Equipment"
	                    ng-options="equipment.display_name as equipment.display_name for equipment in vm.equipments"
	                    ng-model="equipment" >
	                </select>
	           	</div>
	        </div>		
		</div>
		<div  class="row-fluid"><br></div>
    </div>
</div>
