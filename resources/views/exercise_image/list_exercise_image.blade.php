<div class="block-title patient_title"  ng-init="list_exercise.fetchExercise(1);">
    <div class="col-sm-2">
        <h3><strong> &nbsp; &nbsp;  Exercise Images</strong></h3>
    </div>  
    <div class="col-sm-10 text-alignRight">
        <h3>
          <form class="form-inline">
            <div class="form-group">
              <a ui-sref="add-exercise-image" class="btn btn-default" ata-original-title="New Exercise Image"><span><i class="fa fa-plus"></i></span>&nbsp;New Image</a>
            </div>
          </form>
        </h3>
    </div>
</div>
<div class="col-xs-12 col-sm-5 col-md-4 col-lg-3">
    @include('exercise_image.search_exercise_image')
</div>
<div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
    <div class="block">
    	<!-- Updates Title -->
    	<div class="block-title">
        <div class="block-options">
          <div class="row padding_top_bottom">
            <div class="col-sm-5">
               <h2><strong><span style="font-size: 13px!important;">Exercise Images @{{list_exercise.from}} to @{{list_exercise.to}} of @{{list_exercise.total_count}}</span></strong></h2>
            </div> 
            <div class="col-sm-7 text-right">
               <div class="form-inline">
                  <div class="form-group">
                    <ul uib-pagination
                      total-items="list_exercise.total_count"
                      ng-model="list_exercise.current_page"
                       max-size="5" 
                       class="pagination-sm margin_top_no"
                       boundary-links="false"
                       boundary-link-numbers="true" 
                       rotate="false"
                       items-per-page ="list_exercise.itemPerPage"
                       ng-change="list_exercise.pageChanged()"
                       >
                  </ul>
                  </div>
                  <div class="form-group">
                    <label><b>Show:</b></label>
                    <select class="form-control" ng-options="value for value in list_exercise.displayRecords" ng-change="list_exercise.pageChanged();" ng-model="list_exercise.itemPerPage">  
                    </select>
                  </div>
               </div>
            </div>
          </div>
        </div>
    	</div>
    	<!-- END Updates Title -->
      <!-- Lightbox Gallery with Options Content -->
                <div class="gallery container-fluid animation-fadeInQuick">
                    <div ng-if="list_exercise.isloading">
                      @include('layout.loading')
                    </div>
                    <div ng-show="list_exercise.exercise_images.length > 0" ng-if="$index%3==0" ng-repeat="xyz in list_exercise.exercise_images" >                     
                        <div  class="row row-eq-height" >
                            <single-image edit="list_exercise.editExercise(exercise)"
                                         remove ="list_exercise.deleteExercise(exercise)"
                                         view ="list_exercise.viewExercise(exercise)"
                              ng-if="($index >= $parent.$index) && ($index <= ($parent.$index+2))" 
                            class="col-sm-4" ng-repeat="exercise in list_exercise.exercise_images" exercise="exercise" width="250" height="150" >                            
                            </single-image>
                        </div>
                        <br>
                    </div>                         
                </div>
                <!-- END Lightbox Gallery with Options Content -->
          <hr/>
          <div class="row">
             <div class="col-sm-3">
                <h2><strong><span style="font-size: 13px!important; color: #1BBAE1 !important;">
                Exercise Images @{{list_exercise.from}} to @{{list_exercise.to}} of 
                @{{list_exercise.total_count}}</span>
                </strong></h2> 
             </div>  
             <div class="col-sm-9" style="text-align:center; font-size: 13px!important; ">
                     <ul uib-pagination
                        total-items="list_exercise.total_count"
                        ng-model="list_exercise.current_page"
                         max-size="5" 
                         class="pagination-sm"
                         boundary-links="true"
                         boundary-link-numbers="true" 
                         rotate="false"
                         items-per-page ="list_exercise.itemPerPage"
                         ng-change="list_exercise.pageChanged()"
                         >
                      </ul>
             </div>
          </div>
    </div>
    <!-- view models -->
    <div id="view_exercise_image" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"         data-backdrop="static" data-keyboard="false">
              @include('exercise_image.view_exercise_detail')
    </div>
</div>