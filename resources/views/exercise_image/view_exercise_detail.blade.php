<div class="modal-dialog" style="width:650px;">
  
   <div class="panel panel-primary">
          <div class="panel-heading panel_head_background">
            <div class="row">
              <div class="col-xs-12">
              	<button type="button" class="close white_close" data-dismiss="modal" >
      			        <span aria-hidden="true" >&times;</span>
      			        <span class="sr-only" >Close</span>
  			        </button>
                <h4>
                	<strong>@{{list_exercise.selectedExercise.name}}</strong>
                	<p class="tag_text_normal"></p>
                </h4>
              </div>
            </div>
          </div>
          <div class="panel-body">
            
              <div class="row">
                <div class="col-sm-8" style="border-right:1px solid #f2f2f2;">
                   <img ng-src="/exercise-images/@{{list_exercise.selectedExercise.image_name}}" style="object-fit: contain; height:280px; width:100%" alt="image"
                   onerror="this.src = '/exercise-images/no_image_big.png'">
                </div>
                <div class="col-sm-4">
                  <div class="details-exec">
                    <h5>Calorie Burn &nbsp;
                    <span>
                    @{{list_exercise.selectedExercise.calorie_burn}}</span></h5>
                    
                    <h5>Minutes &nbsp;
                    <span>
                    @{{list_exercise.selectedExercise.exercise_time}}</span></h5>

                    <h5>Exercise Category &nbsp;
                    <span>
                    @{{list_exercise.selectedExercise.exercise_type}}</span></h5>
                    
                    <h5>Body Part  &nbsp;
                    <span>
                    @{{list_exercise.selectedExercise.body_part}}</span></h5>

                    <h5>Experience Level  &nbsp;
                    <span>
                    @{{list_exercise.selectedExercise.experience}}</span></h5>
                    
                    <h5>Equipment Needed  &nbsp;
                    <span>
                    @{{list_exercise.selectedExercise.equipment}}</span></h5>

                    
                  </div>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-xs-12">
                  <h4>Exercise Details</h4>
                  <p> @{{list_exercise.selectedExercise.description}}</p>
                  <div ng-repeat="(key, value) in list_exercise.selectedExercise.steps" class="exe_details_bot">
                    <h5>Step @{{$index+1}}</h5>
                    <p>@{{value}}</p>
                  </div>
                </div>
              </div>
          </div>
   </div>
</div>