<div class="row block-title patient_title" ng-init="edit_deal.getDealData()">
  <div class="col-sm-6">
    <h3><strong>Edit Deal&nbsp;</strong></h3>
  </div>  
  <div class="col-sm-6 text-alignRight">
    
                 <button class="btn btn-default" 
                                      ng-disabled="editDealForm.$invalid || edit_deal.date_error==true"
                                      ng-click="edit_deal.updateDealData()">
                                      <i class="fa fa-angle-right"></i> Update</button>
                 <button class="btn btn-default" type="button" ng-click="edit_deal.deleteDeal(edit_deal.deal_data.id);">
                  <i class="fa gi gi-bin"></i> Delete
                 </button>
                 <a ng-click="edit_deal.gotoDealListPage()" class="btn btn-default"> <i class="gi gi-remove_2"></i>
                 &nbsp;Cancel</a>
  </div>
</div>

<div class="row">
  <form name="editDealForm" enctype="multipart/form-data" class="form-horizontal form-bordered">
    <div class="col-md-6">
        <!-- Basic Form Elements Block -->
        <div class="block">
            <div class="block-title">
              <h2>
              <strong>Edit Deal Information</strong>
              </h2>
            </div>
            <!-- Basic Form Elements Content -->
                <div class="form-group">
                  <label class="col-md-3 control-label" for="example-text-input">Is Expired</label>
                  <div class="col-md-5">
                      <select class="form-control" ng-model="edit_deal.deal_data.is_expired">
                        <option value="0" ng-selected="edit_deal.deal_data.is_expired==0">No</option>
                        <option value="1"  ng-selected="edit_deal.deal_data.is_expired==1">Yes</option>
                      </select>
                    
                  </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="example-text-input">Title<span class="text-danger">&nbsp;*</span></label>
                    <div class="col-md-8">
                        <input type="text" id="example-text-input" 
                                          name="title" 
                                          class="form-control" 
                                          placeholder="Text" 
                                          ng-model="edit_deal.deal_data.title"
                                          required>
         
                    </div>
                    <div class="col-md-offset-3 col-md-9">
                      <span class="text-danger" ng-show="editDealForm.title.$invalid && editDealForm.title.$touched">Please enter title</span>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-3 control-label" for="example-textarea-input">Description<span class="text-danger">&nbsp;*</span></label>
                    <div class="col-md-8">
                        <textarea id="example-textarea-input" 
                                  name="description" 
                                  rows="4" 
                                  class="form-control" 
                                  placeholder="Content.."
                                  ng-model="edit_deal.deal_data.description"
                                  required></textarea>
                    </div>
                    <div class="col-md-offset-3 col-md-9">
                      <span class="text-danger" ng-show="editDealForm.description.$invalid && editDealForm.description.$touched">Please enter description</span>
                    </div>
                </div>                

                <div class="form-group">
                    <label class="col-md-3 control-label" for="example-text-input">Start<span class="text-danger">&nbsp;*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                          <input type="text" class="form-control" 
                                  name="start_time"
                                  datetime-picker="dd MMM yyyy hh:mm a" 
                                  ng-model="edit_deal.deal_data.start_datetime"
                                  is-open="edit_deal.isOpen_startDate"  
                                  when-closed="edit_deal.dateCompareValidation(edit_deal.deal_data.start_datetime, edit_deal.deal_data.end_datetime)"
                                  required/>
                          <span class="input-group-btn">
                              <button type="button" class="btn btn-default" ng-click="edit_deal.openCalendar_startDate($event, prop)">
                                <i class="fa fa-calendar"></i></button>
                          </span>
                        </div>
                    </div>
                    <div class="col-md-offset-3 col-md-9">
                      <span class="text-danger">@{{edit_deal.errMessage_startdate}}</span>
                      <br>
                      <span class="text-danger" ng-show="editDealForm.start_time.$invalid && editDealForm.start_time.$touched">Please select start time</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="example-text-input">End<span class="text-danger">&nbsp;*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                          <input type="text" class="form-control" 
                                  name="end_time"
                                  datetime-picker="dd MMM yyyy hh:mm a" 
                                  ng-model="edit_deal.deal_data.end_datetime"
                                  is-open="edit_deal.isOpen_endDate" 
                                  when-closed="edit_deal.dateCompareValidation(edit_deal.deal_data.start_datetime, edit_deal.deal_data.end_datetime)" 
                                  required/>
                          <span class="input-group-btn">
                              <button type="button" class="btn btn-default" ng-click="edit_deal.openCalendar_endDate($event, prop)">
                                <i class="fa fa-calendar"></i></button>
                          </span>
                        </div>
                    </div>
                    <div class="col-md-offset-3 col-md-9">
                      <span class="text-danger">@{{edit_deal.errMessage_enddate}}</span>
                      <br>
                      <span class="text-danger" ng-show="editDealForm.end_time.$invalid && editDealForm.end_time.$touched">Please select end time</span>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-3 control-label" for="example-text-input">Coupon Code<span class="text-danger">&nbsp;*</span></label>
                    <div class="col-md-5">
                        <input type="text" id="example-text-input" 
                                            name="coupon_code" 
                                            class="form-control" 
                                            placeholder="Text"
                                            ng-model="edit_deal.deal_data.coupon_code"
                                            required>
                      
                    </div>
                    <div class="col-md-offset-3 col-md-9">
                      <span class="text-danger" ng-show="editDealForm.coupon_code.$invalid && editDealForm.coupon_code.$touched">Please enter coupon code</span>
                    </div>
                </div>

            <!-- END Basic Form Elements Content -->
        </div>
        <!-- END Basic Form Elements Block -->
    </div>
    <div class="col-md-6">
      <div class="block">
        <div class="block-title">
          <h2>
          <strong> Edit Deal Image</strong>
          </h2>
        </div>
        
        <div class="form-group">
            <div class="col-md-12">
                <div class="row">
                  <div class="dropzone col-sm-6" 
                        upload-image-url="/api/v3/deal_image/upload"
                        remove-image-url="/api/v3/deal_image/remove/"
                        file="edit_deal.deal_data.image" 
                        upload-image-dropzone 
                        old-file-name = '@{{edit_deal.deal_data.image}}'>
                       <div class="dz-default dz-message"><span>Drop Image here to upload</span></div>                     
                  </div>
                  <div class="col-sm-6">
                      <img ng-src="/deal-images/@{{edit_deal.deal_data.image}}" style=" object-fit: contain; height:auto; width:100%" alt="image">
                  </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </form>
</div>
