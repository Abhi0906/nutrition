<!-- Deal Block -->
 <div class="row block-title patient_title" ng-init="deal.getDealList();">
      <div class="col-sm-6">
        <h3><strong>Deals</strong></h3>
      </div>  
      <div class="col-sm-6 text-alignRight">
        <a ui-sref="create-deal" class="btn btn-default"><span><i class="fa fa-plus"></i></span>&nbsp;New Deal</a>
      </div>
  </div>

<div class="block md_recommended_proui" >

    <div class="md_rec_food_class" id="mdrecommeded_meal_table">
      <table class="table recommeded_table">
        <thead>
          <tr class="">
            <th class="text-center bg-primary" style="width: 8%">IMAGE</th>
            <th class="text-center bg-primary" style="width: 22%">DEAL</th>
            <th class="text-center bg-primary" style="width: 28%">DESCRIPTION</th>
            <th class="text-center bg-primary" style="width: 10%">COUPON CODE</th>
            <th class="text-center bg-primary" style="width: 8%">START</th>
            <th class="text-center bg-primary" style="width: 8%">END</th>
            <th class="text-center bg-primary" style="width: 8%">IS EXPIRED</th>
            <th class="text-center bg-primary" style="width: 8%">ACTION</th>
          </tr>
        </thead>
        <tbody>
        <tr ng-repeat="deal_data in deal.deallist | orderBy:'-start'">
          <td data-title="Image" class="text-center">
            <img height="60" width="60" 
                              alt="avatar" 
                              class="" 
                              ng-src="/deal-images/@{{deal_data.image}}">
          </td>
          <td data-title="DEAL" class="text-left">
              @{{deal_data.title}}
          </td>
          <td data-title="DESCRIPTION" class="text-left">@{{deal_data.description}}</td>
          <td data-title="COUPON CODE" class="text-center">
            @{{deal_data.coupon_code}}
          </td>

          <td data-title="START" class="text-center">
            <span ng-if="deal_data.start">
              @{{deal_data.start | custom_date_format: 'dd-MMM-yy hh:mm a' }}
            </span>
          </td>

          <td data-title="END" class="text-center">
            <span ng-if="deal_data.end">
              @{{deal_data.end | custom_date_format: 'dd-MMM-yy hh:mm a'}}
            </span>
          </td>

          <td data-title="IS EXPIRED" class="text-center">
            <span ng-if="deal_data.is_expired==0">No</span>
            <span ng-if="deal_data.is_expired==1">Yes</span>
          </td>
          <td data-title="ACTION" class="text-center">
            <div class="btn-group btn-group-xs">
              <a class="btn btn-default" href="javascript:void(0)" ui-sref="edit-deal({deal_id: deal_data.id})" data-original-title="Edit">
                <i class="fa fa-pencil-square-o"></i>
              </a>
              <a class="btn btn-danger" title="" ng-click="deal.deleteDeal(deal_data)" data-toggle="tooltip" href="javascript:void(0)" data-original-title="Delete">
                <i class="fa fa-times"></i>
              </a>
            </div>
          </td>
        </tr>
        <tr class="text-center" ng-if="deal.deallist.length==0">
          <td colspan="8">
            No deal added yet
          </td>
        </tr>
        </tbody>
      </table>
    </div>
    <!-- END Block Content -->
</div>
<!-- END of MD Recommended Food Block