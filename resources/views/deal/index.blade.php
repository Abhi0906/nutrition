@extends('layout.app')

@section('content')

<div ng-controller="DealIndexController as deal_index">   
    <div ui-view></div>     
</div>


@endsection

@section('page_scripts')
{!! HTML::script("js/controllers/DealIndexController.js") !!}
{!! HTML::script("js/controllers/DealController.js") !!}
{!! HTML::script("js/controllers/CreateDealController.js") !!}
{!! HTML::script("js/controllers/EditDealController.js") !!}

{!! HTML::script("js/directives/uploadImageDropZone.js") !!}
{!! HTML::script("js/filters/dateFormatFilter.js") !!}

@append

@section('jquery_ready')

@append