@extends('layout.master')

@section('title')
  Deals
@endsection

<!-- add Internal css -->
@section('custom-css')
   <style type="text/css">
   
  </style>
@endsection
 

@section('content') 


<section class="section-page">

	<div class="row">
	<!-- <div ng-repeat="deal_data in deal.deals">@{{deal_data.title}}</div> -->
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center ng-scope">
			<strong><h2 class="text-uppercase"><span class="title_head_line ">Deals</span></h2></strong>
		</div>
	</div>

	<div class="saperator"></div>
  	<div class="saperator"></div>

	<div class="row" ng-controller="DealFrontController as deal" ng-init="deal.userId='{{Auth::user()->id}}';  deal.getDeals()">

		<div class="col-lg-12" >
			<div class="row" ng-repeat="deal_data in deal.deals | orderBy:'-id'">
				<div class="col-lg-12" >
					<div class="row">
						<div class="col-md-3">					
							 <img height="200" width="300" 
		                              alt="avatar" 
		                              class="" 
		                              ng-src="/deal-images/@{{deal_data.image}}">
						</div>
						<div class="col-md-9">
							<h4>@{{deal_data.title}}</h4>
							<p>						
								<strong>Valid From: </strong>@{{deal_data.start | custom_date_format: 'dd-MMM-yyyy hh:mm a'}}
								&nbsp;
								<strong>To: </strong>@{{deal_data.end | custom_date_format: 'dd-MMM-yyyy hh:mm a'}}						
							</p>

							<p class="text-justify ">@{{deal_data.description}}</p>

							<p><strong>Coupon Code: </strong>@{{deal_data.coupon_code}}</p>
						</div>
					</div>
				</div>
				<div class="col-lg-12"> 
					<hr>
				</div>
				
				 
			</div>

		</div>

	</div>
</section>




@endsection
@section('js')
<!-- {!! HTML::script("js/jquery.sameheight.js") !!} -->

{!! HTML::script("js/controllers/DealFrontController.js") !!}

{!! HTML::script("js/filters/stringLimitFilter.js") !!}

@append
@section('custom-js')
 

@endsection