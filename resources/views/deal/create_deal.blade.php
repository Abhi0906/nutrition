<div class="row block-title patient_title">
  <div class="col-sm-6">
    <h3><strong>Create Deal&nbsp;</strong></h3>
  </div>  
  <div class="col-sm-6 text-alignRight">
    
                 <button class="btn btn-default" 
                                      ng-disabled="createDealForm.$invalid || create_deal.date_error==true"
                                      ng-click="create_deal.saveDeal()">
                                      <i class="fa fa-angle-right"></i> Submit</button>
                 <a ng-click="create_deal.gotoDealListPage()" class="btn btn-default"> <i class="gi gi-remove_2"></i>
                 &nbsp;Cancel</a>
  </div>
</div>

<div class="row">
  <form name="createDealForm" enctype="multipart/form-data" class="form-horizontal form-bordered">
    <div class="col-md-6">
        <!-- Basic Form Elements Block -->
        <div class="block">
            <div class="block-title">
              <h2>
              <strong>Deal Information</strong>
              </h2>
            </div>
            <!-- Basic Form Elements Content -->
            

                <div class="form-group">
                    <label class="col-md-3 control-label" for="example-text-input">Title<span class="text-danger">&nbsp;*</span></label>
                    <div class="col-md-8">
                        <input type="text" id="example-text-input" 
                                          name="title" 
                                          class="form-control" 
                                          placeholder="Text" 
                                          ng-model="create_deal.deal_data.title"
                                          required>
         
                    </div>
                    <div class="col-md-offset-3 col-md-9">
                      <span class="text-danger" ng-show="createDealForm.title.$invalid && createDealForm.title.$touched">Please enter title</span>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-3 control-label" for="example-textarea-input">Description<span class="text-danger">&nbsp;*</span></label>
                    <div class="col-md-8">
                        <textarea id="example-textarea-input" 
                                  name="description" 
                                  rows="4" 
                                  class="form-control" 
                                  placeholder="Content.."
                                  ng-model="create_deal.deal_data.description"
                                  required></textarea>
                    </div>
                    <div class="col-md-offset-3 col-md-9">
                      <span class="text-danger" ng-show="createDealForm.description.$invalid && createDealForm.description.$touched">Please enter description</span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="example-text-input">Start<span class="text-danger">&nbsp;*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                          <input type="text" class="form-control" 
                                  name="start_time"
                                  datetime-picker="dd MMM yyyy hh:mm a" 
                                  ng-model="create_deal.deal_data.start_datetime"
                                  ng-init = "create_deal.deal_data.start_datetime = create_deal.today_datetime"
                                  is-open="create_deal.isOpen_startDate"  
                                  when-closed="create_deal.dateCompareValidation(create_deal.deal_data.start_datetime, create_deal.deal_data.end_datetime)"
                                  required/>
                          <span class="input-group-btn">
                              <button type="button" class="btn btn-default" ng-click="create_deal.openCalendar_startDate($event, prop)">
                                <i class="fa fa-calendar"></i></button>
                          </span>
                        </div>
                    </div>
                    <div class="col-md-offset-3 col-md-9">
                      <span class="text-danger">@{{create_deal.errMessage_startdate}}</span>
                      <br>
                      <span class="text-danger" ng-show="createDealForm.start_time.$invalid && createDealForm.start_time.$touched">Please select start time</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="example-text-input">End<span class="text-danger">&nbsp;*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                          <input type="text" class="form-control" 
                                  name="end_time"
                                  datetime-picker="dd MMM yyyy hh:mm a" 
                                  ng-model="create_deal.deal_data.end_datetime" 
                                  ng-init = "create_deal.deal_data.end_datetime = create_deal.today_datetime"
                                  is-open="create_deal.isOpen_endDate"
                                  when-closed="create_deal.dateCompareValidation(create_deal.deal_data.start_datetime, create_deal.deal_data.end_datetime)"
                                  required/>
                          <span class="input-group-btn">
                              <button type="button" class="btn btn-default" ng-click="create_deal.openCalendar_endDate($event, prop)">
                                <i class="fa fa-calendar"></i></button>
                          </span>
                        </div>
                    </div>
                    <div class="col-md-offset-3 col-md-9">
                      <span class="text-danger">@{{create_deal.errMessage_enddate}}</span>
                      <br>
                      <span class="text-danger" ng-show="createDealForm.end_time.$invalid && createDealForm.end_time.$touched">Please select end time</span>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-3 control-label" for="example-text-input">Coupon Code<span class="text-danger">&nbsp;*</span></label>
                    <div class="col-md-5">
                        <input type="text" id="example-text-input" 
                                            name="coupon_code" 
                                            class="form-control" 
                                            placeholder="Text"
                                            ng-model="create_deal.deal_data.coupon_code"
                                            required>
                      
                    </div>
                    <div class="col-md-offset-3 col-md-9">
                      <span class="text-danger" ng-show="createDealForm.coupon_code.$invalid && createDealForm.coupon_code.$touched">Please enter coupon code</span>
                    </div>
                </div>

            <!-- END Basic Form Elements Content -->
        </div>
        <!-- END Basic Form Elements Block -->
    </div>
    <div class="col-md-6">
      <div class="block">
        <div class="block-title">
          <h2>
          <strong>Deal Image</strong>
          </h2>
        </div>
        <div class="form-group">
            <div class="col-md-12">

                <div class="dropzone" 
                      upload-image-url="/api/v3/deal_image/upload"
                      remove-image-url="/api/v3/deal_image/remove/"
                      file="create_deal.deal_data.image" 
                      upload-image-dropzone >
                     <div class="dz-default dz-message"><span>Drop Image here to upload</span></div>                     
                </div>
            </div>
        </div>
      </div>
    </div>
  </form>
</div>
