<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
	<strong><h2 class="text-uppercase">YOUR Ex<span class="title_head_line ">ercis</span>e DIARY</h2></strong>
</div>
 <div class="saperator"></div>

   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9" ng-init="exercise_diary.getExerciseDiary();">
         @include('front.exercise.exercise-diary')
        <!-- monthly exercise calendar -->
        <div class="saperator-50"></div>
      
        <div class="saperator"></div>
         @include('front.exercise.exercise-monthly-summary')
        <div class="saperator-50"></div>
        <!-- end monthly exercise calendar -->
   </div>