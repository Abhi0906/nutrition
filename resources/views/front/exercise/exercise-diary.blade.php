<div class="saperator"></div>
<div class="row">
   <div class="col-md-12 col-lg-12 text-right">
   <div class="text-right datepicker_contain_part">
      <ul class="list-inline btn-ul-date datepicker_ul">
        
        <li>
              <button ng-click="exercise_diary.prevExerciseDairy();" class="btn btn-primary btndesign btn-arrow">
                    <span class="fa fa-chevron-left"></span>
              </button>
        </li>
        <li>
              <div class="input-group date sleepboxdatepicker big-date-width">
                 <input type="text"
                   class="form-control" 
                   uib-datepicker-popup = "@{{exercise_diary.format}}"
                    ng-model="exercise_diary.log_date"
                    is-open="exercise_diary.exercise_date_popup.opened"
                    datepicker-options="exercise_diary.dateOptions"
                    ng-required="true"
                    ng-change="exercise_diary.changeDate();"
                    show-button-bar=false
                    close-text="Close" />
                     <span class="input-group-addon caledar_custom" ng-click="exercise_diary.exercise_date_open()"> 
                        <i aria-hidden="true" class="fa fa-calendar"></i>
                      </span>
              </div>
        </li>
        <li><button ng-click="exercise_diary.nextExerciseDairy();" class="btn btn-primary btndesign btn-arrow"><span class="fa fa-chevron-right"></span></button></li>
        
      </ul>
    </div>
  </div>
</div>
<div class="food_dairy_block"> 
  <div id="exercise_1st_table_responsive">
    <table class="table table-bordered borderless_table">
      <thead>
        <tr class="toptr-border">
          <th class="withborder td_width_30"></th>
          <th class="calorie-subhead-td withborder td_width_20">CALORIES BURNED</th>
          <th class="calorie-subhead-td withborder td_width_50">MINUTES</th>      
        </tr>
      </thead>
        <tbody>
            <tr class="text-center toptr-border">
              <td id="exec_1st_td" data-title=" " colspan="3" class="sidecolor text-leftside withborder head_slide_exec_tbl_td">
                <h4 id="cardio_focus_id">CARDIOVASCULAR</h4>
                <ul class="list-inline nobottom-margin">
                  <li>
                    <i class="fa fa-plus-circle facolor"></i> 
                     <a ui-sref="log_exercise({ exercise_type : 'cardio'})" class="add_exercise" >
                      Add Exercise
                    </a> 
                  </li>
                </ul>
              </td>
            </tr>
            <tr ng-repeat="(key, value) in exercise_diary.cardioExercises">
             <td data-title="Exercise"  class="first-td-border">
               <a href="javascript:void(0);" ng-click="exercise_diary.deleteExercise(value)">
                 <i class="fa fa-times-circle facolor"></i>
               </a>
                 @{{value.name}}
               
             </td>
             <td class="text-center black_text td-border-none" data-title="CALORIES BURNED"> @{{value.calories_burned}}</td>
             <td class="text-center black_text td-border-none" data-title="MINUTES"> @{{value.time}}</td>
           </tr>
          
           
            <tr class="total_tr">
             <td class="first-td-border first_td">TOTAL</td>
             <td class="text-center  td-border-none ng-binding" data-title="CALORIES BURNED">@{{exercise_diary.totalCardio.calories_burned || 0}}</td>

             <td class="text-center  td-border-none ng-binding" data-title="MINUTES">@{{exercise_diary.totalCardio.time || 0}}</td>
           </tr>
        
      </tbody>
    </table>
  </div>

  <div id="exercise_2st_table_responsive">
      <table class="table table-bordered borderless_table">
        <thead>
          <tr class="toptr-border">
            <td class="withborder text-center td_width_30">
              &nbsp;
            </td>
            <td class="calorie-subhead-td withborder td_width_20">CALORIES BURNED</td>
            <td class="calorie-subhead-td withborder td_width_12_5">MINUTES</td>
            <td class="calorie-subhead-td withborder td_width_12_5">SETS</td>
            <td class="calorie-subhead-td withborder td_width_12_5">REPS</td>
            <td class="calorie-subhead-td withborder td_width_12_5">WEIGHT(lbs)</td>
          </tr>
        </thead>
        <tbody>
          <tr class="text-center toptr-border">
            <td data-title="" id="exec_2st_td2" colspan="6" class="sidecolor text-leftside withborder text-center head_slide2_exec2_tbl_td">
              <h4 id="resistance_focus_id">RESISTANCE TRAINING</h4>
              <span class="text_font_size">
                         (Categories: Olympic Weightlifting, Plyometrics, Powerlifting
                          Strength, Stretching, Strongman) 
                  </span>
                  
              <ul class="list-inline nobottom-margin">
               
                <li style="padding-top:5px;">

                   <i class="fa fa-plus-circle facolor"></i> 
                    <a ui-sref="log_exercise({ exercise_type : 'resistance'})" class="add_exercise" >
                      Add Exercise
                    </a>
               
                </li>
               
              </ul>
            </td>
          </tr>
          <tr class="first-td-border" ng-repeat="(key, value) in exercise_diary.resistanceExercises">
           <td data-title="Exercise" class="first-td-border first_td">
              <a href="javascript:void(0);" ng-click="exercise_diary.deleteExercise(value)">
                   <i class="fa fa-times-circle facolor"></i>
                 </a>
                   @{{value.name}}
           </td>
           <td class="text-center  black_text td-border-none" data-title="CALORIES BURNED"> @{{value.calories_burned}}</td>
           <td class="text-center  black_text td-border-none" data-title="MINUTES"> @{{value.time}}</td>
           <td class="text-center  black_text td-border-none" data-title="SETS"> @{{value.sets}}</td>
           <td class="text-center  black_text td-border-none" data-title="REPS"> @{{value.reps}}</td>
           <td class="text-center black_text  td-border-none" data-title="WEIGHT(lbs)"> @{{value.weight_lbs}}</td>
          </tr>
          <tr class="total_tr">
           <td class="first-td-border first_td td_width_30">TOTAL</td>
           <td class="text-center  td-border-none"  data-title="CALORIES BURNED" >@{{exercise_diary.totalResistance.calories_burned || 0}}</td>
           <td class="text-center  td-border-none"  data-title="MINUTES" >@{{exercise_diary.totalResistance.time || 0}}</td>
           <td class="text-center  td-border-none"  data-title="SETS" >@{{exercise_diary.totalResistance.sets || 0}}</td>
           <td class="text-center  td-border-none"  data-title="REPS" >@{{exercise_diary.totalResistance.reps || 0}}</td>
           <td class="text-center  td-border-none"  data-title="WEIGHT(lbs)" >@{{exercise_diary.totalResistance.weight_lbs || 0}}</td>
          </tr>
        </tbody>
      </table>
  </div>
  <div id="exercise_2st_table_responsive_2part">
    <table class="table table-bordered borderless_table no_margin">
      <thead>
        <tr class="toptr-border">
          <th class="withborder td_width_30"></th>
          <th class="calorie-subhead-td withborder td_width_20">CALORIES BURNED</th>
          <th class="calorie-subhead-td withborder td_width_50">MINUTES</th>      
        </tr>
      </thead>
        <tbody>
            <tr class="text-center toptr-border">
              <td id="exec_1st_td" data-title=" " colspan="3" class="sidecolor text-leftside withborder head_slide_exec_tbl_td">
                <h4 id="cardio_focus_id">APPLE HEALTH</h4>
                <span class="text_font_size">
                  (Note: Exercise data received from Apple Health through Genemedics iPhone app) 
                </span>
                <!-- <ul class="list-inline nobottom-margin">
                  <li>
                    <i class="fa fa-plus-circle facolor"></i> 
                     <a ui-sref="log_exercise({ exercise_type : 'cardio'})" class="add_exercise" >
                      Add Exercise
                    </a> 
                  </li>
                </ul> -->
              </td>
            </tr>
            <tr ng-repeat="(key, value) in exercise_diary.appleHealthExercises">
             <td data-title="Exercise"  class="first-td-border">
               
               @{{value.name}}
               
             </td>
             <td class="text-center black_text td-border-none" data-title="CALORIES BURNED">@{{value.calories_burned}}</td>
             <td class="text-center black_text td-border-none" data-title="MINUTES">@{{value.time}}</td>
            </tr>
            
            <tr class="total_tr">
             <td class="first-td-border first_td">TOTAL</td>
             <td class="text-center  td-border-none ng-binding" data-title="CALORIES BURNED">@{{exercise_diary.totalAppleHealth.calories_burned || 0}}</td>

             <td class="text-center  td-border-none ng-binding" data-title="MINUTES">@{{exercise_diary.totalAppleHealth.time || 0}}</td>
           </tr>
        
      </tbody>
    </table>
  </div>

  <div id="exercise_3st_table_responsive">
    <table class="table table-bordered borderless_table">
      <tbody>
        <tr class="toptr-border">
          <td class="text-right withborder head_slide_foodtbl_td td_width_30" data-title=" "><h4>YOUR DAILY GOAL</h4></td>
          <td class="sidecolor withborder ng-binding text-center">@{{exercise_diary.exerciseSummary.calorie_burned_goal || 0}}</td>
          <td class="sidecolor withborder" >&nbsp;</td>
          <td class="sidecolor withborder" >&nbsp;</td>
          <td class="sidecolor withborder" >&nbsp;</td>
          <td class="sidecolor withborder" >&nbsp;</td>
        </tr>
        <tr class="toptr-border">
          <td class="text-right withborder head_slide_foodtbl_td" data-title=" "><h4>TOTAL BURNED</h4></td>
          <td class="sidecolor withborder ng-binding text-center" data-title="CALORIES">@{{exercise_diary.exerciseSummary.total_burned || 0}}</td>
          <td class="sidecolor withborder" >&nbsp;</td>
          <td class="sidecolor withborder" >&nbsp;</td>
          <td class="sidecolor withborder" >&nbsp;</td>
          <td class="sidecolor withborder" >&nbsp;</td>
        </tr>
        <tr class="toptr-border">
          <td class="text-right withborder head_slide_foodtbl_td" data-title=" "><h4>% OF GOAL</h4></td>
          <td class="sidecolor withborder ng-binding text-center" >@{{exercise_diary.exerciseSummary.calorie_burned_percentage || 0}}%</td>
          <td class="sidecolor withborder" >&nbsp;</td>
          <td class="sidecolor withborder" >&nbsp;</td>
          <td class="sidecolor withborder" >&nbsp;</td>
          <td class="sidecolor withborder" >&nbsp;</td>
        </tr>
        <tr class="toptr-border">
          <td class="text-right withborder head_slide_foodtbl_td"><h4>TOTAL REMAINING</h4></td>
          <td class="sidecolor withborder ng-binding text-center">@{{exercise_diary.exerciseSummary.total_remaining || 0}}</td>
          <td class="sidecolor withborder" >&nbsp;</td>
          <td class="sidecolor withborder" >&nbsp;</td>
          <td class="sidecolor withborder" >&nbsp;</td>
          <td class="sidecolor withborder" >&nbsp;</td>
        </tr>
      </tbody>
    </table>
  </div>

   
</div>

<div class="row food_last_row headrow-samecolumnheight">
  <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
    <div class="row footnoterow">
      <div class="col-xs-12 col-sm-12 col-md-12">
        <h4>
          TODAY'S EXERCISE NOTES  
          <!-- <small class="pull-right"><a href="#">Edit Note <i class="fa fa-pencil"></i></a></small> -->
        </h4>
        <!-- <textarea class="form-control textcolor" ng-disabled="true" rows="4"></textarea>  -->
          <a href="javascript:void(0);" editable-textarea="exercise_diary.exercise_notes" e-rows="20" e-cols="40" onaftersave="exercise_diary.update_notes();">
          <pre class="food_notes">@{{ exercise_diary.exercise_notes || 'no description' }}</pre>
        </a> 
      </div>
     </div>
    </div>
  <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 second_row_foot">
  </div>
</div>