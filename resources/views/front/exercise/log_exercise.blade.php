<div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12" 
 ng-init="add_exercise.allLoggedExercise()"
>
 <strong>
    <h2 class="text-light-blue text-uppercase">
      <a class="pull-left btn btn-primary btndesign" ui-sref="diary">Back</a>
      LOG <span class="title_head_line" ng-if="add_exercise.exercise_type =='cardio'">CARDIOVASCULAR</span>
         <span class="title_head_line" ng-if="add_exercise.exercise_type =='resistance'">RESISTANCE TRAINING</span>
      </h2>
  </strong>
  <div class="saperator"></div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
  
 <div class="panel panel-primary weight_body_panel weight_diary_panel_size goalsummary_tbl1">
        <div class="panel-heading panel_head_background panel_head_log_food">
          <div class="row">
            <div class="col-xs-5">

              <h4>LOG EXERCISE</h4></div>
            <div class="col-xs-7 text-right">
              
            </div>
          </div>
        </div>
        <div class="panel-body panel-contain_div_logfood"> 
            
            <p class="theme_text_p_lg">ENTER EXERCISE</p>

            <div class="row">
              <div class="col-sm-10">
                <div class="btn_group_search">
                  
                    <input style="width:86%"
                     type="text"
                      ng-keypress="add_exercise.searchExercise()" 
                      ng-model="add_exercise.searchKeyExercise"
                       class="form-control formcontrol_height"
                         aria-describedby="basic-addon2">
                </div>
              </div>
              <div class="space_between visible-xs"></div>

              <div class="col-sm-2">
                <button class="btn btn-primary btndesign no_margin" ng-click="add_exercise.clearSearchResult();">Clear</button>
              </div>
            </div>
          
        </div>
        <div class="panel-body">
          <div class="logfood_tab_continer">
            <div id="log_food_tab" class="tabs_container">
              <ul class="nav nav-tabs" id="tabs_logfood" ng-show="add_exercise.exercise_tab_display">
                <li id="feq_tab" class="active one"><a href="/log_exercise/#fre_log_tab" data-toggle="tab">Frequent</a></li>
                <li id="rec_tab"><a href="/log_exercise/#rec_log_tab" data-toggle="tab">Recent</a></li>
                <li id="selected_tab" ng-show="add_exercise.is_selected"><a href="/log_exercise/#selected_log_tab" data-toggle="tab">Selected</a></li>

              </ul>
              <div class="tab-content logfood_tab_details">
               
                 <div id="fre_log_tab" class="tab-pane fade in active" > 
                    <div class="tbl_tab_food_log">
                      <table class="table table-bordered tbl_log_food text-center">
                        <thead>
                          <tr>
                           <th><h4>EXERCISE NAME</h4></th>
                            <th><h4>CALORIES BURNED</h4></th>
                            <th><h4>MINUTES</h4></th>
                            <th  ng-if="add_exercise.exercise_type =='resistance'"><h4>SETS</h4></th>
                            <th  ng-if="add_exercise.exercise_type =='resistance'"><h4>REPS</h4></th>
                            <th  ng-if="add_exercise.exercise_type =='resistance'"><h4>WEIGHT(lbs)</h4></th>
                            <th><h4>EXERCISE LOG</h4></th>
                          </tr>
                        </thead>
                        <tbody>
                          
                          <tr ng-repeat="exdata in add_exercise.frequent_exercises">
                              <td class="text-left" data-title="EXERCISE NAME">
                                <p class="theme_text_p_lg">@{{exdata.name}}</p>
                                <p class="food_log_normalbold">@{{exdata.time}}, 
                                  minutes @{{exdata.calories_burned}} calc</p>
                              </td>
                              <td data-title="CALORIES BURNED">
                              <p class="food_log_normalbold"
                                 ng-bind="add_exercise.freq_log[$index].calories_burned" 
                                 ng-init="add_exercise.freq_log[$index].calories_burned =exdata.calories_burned"
                               ></p></td>
                              
                              <td data-title="MINUTES">
                                <input type="text"
                                       ng-init="add_exercise.freq_log[$index].time =exdata.time"
                                       size="6"
                                       ng-model="add_exercise.freq_log[$index].time"
                                       ng-change="add_exercise.calculateCaloriesBurned($index,exdata,'frequent');"
                                   >
                              </td>
                              <td ng-if="add_exercise.exercise_type =='resistance'" data-title="SETS">
                                <input type="text" size="6"
                                  ng-init="add_exercise.freq_log[$index].sets =exdata.sets" 
                                  ng-model="add_exercise.freq_log[$index].sets">
                              </td>

                              <td ng-if="add_exercise.exercise_type =='resistance'" data-title="REPS">
                                <input type="text" size="6"
                                   ng-init="add_exercise.freq_log[$index].reps =exdata.reps"
                                   ng-model="add_exercise.freq_log[$index].reps">
                              </td>

                              <td ng-if="add_exercise.exercise_type =='resistance'" data-title="WEIGHT(lbs)">
                                <input type="text" size="6"
                                  ng-init="add_exercise.freq_log[$index].weight_lbs =exdata.weight_lbs" 
                                  ng-model="add_exercise.freq_log[$index].weight_lbs">
                              </td>
                              <td data-title="EXERCISE LOG">
                                <button class="btn btn-primary btndesign" ng-click="add_exercise.logExercise(exdata,$index,'frequent')">Log Exercise</button>
                              </td>
                          </tr>

                        </tbody>
                      </table>
                    </div>
                 </div>
                 <div id="rec_log_tab" class="tab-pane fade in"> 
                    <div class="tbl_tab_food_log">
                      <table class="table table-bordered tbl_log_food text-center">
                        <thead>
                          <tr>
                           <th><h4>EXERCISE NAME</h4></th>
                            <th><h4>CALORIES BURNED</h4></th>
                            <th><h4>MINUTES</h4></th>
                            <th  ng-if="add_exercise.exercise_type =='resistance'"><h4>SETS</h4></th>
                            <th  ng-if="add_exercise.exercise_type =='resistance'"><h4>REPS</h4></th>
                            <th  ng-if="add_exercise.exercise_type =='resistance'"><h4>WEIGHT(lbs)</h4></th>
                            <th><h4>EXERCISE LOG</h4></th>
                          </tr>
                        </thead>
                        <tbody>
                          
                          <tr ng-repeat="exdata in add_exercise.recent_exercises">
                              <td class="text-left" data-title="EXERCISE NAME">
                                <p class="theme_text_p_lg">@{{exdata.name}}</p>
                                <p class="food_log_normalbold">@{{exdata.time}}, 
                                  minutes @{{exdata.calories_burned}} calc</p>
                              </td>
                              <td data-title="CALORIES BURNED">
                              <p class="food_log_normalbold"
                                 ng-bind="add_exercise.recent_log[$index].calories_burned" 
                                 ng-init="add_exercise.recent_log[$index].calories_burned =exdata.calories_burned"
                               ></p></td>
                              
                              <td data-title="MINUTES">
                                <input type="text"
                                       ng-init="add_exercise.recent_log[$index].time =exdata.time"
                                       size="6"
                                       ng-model="add_exercise.recent_log[$index].time"
                                       ng-change="add_exercise.calculateCaloriesBurned($index,exdata,'recent');"
                                   >
                              </td>
                              <td ng-if="add_exercise.exercise_type =='resistance'" data-title="SETS">
                                <input type="text" size="6"
                                  ng-init="add_exercise.recent_log[$index].sets =exdata.sets" 
                                 ng-model="add_exercise.recent_log[$index].sets">
                              </td>

                              <td ng-if="add_exercise.exercise_type =='resistance'" data-title="REPS">
                                <input type="text" size="6"
                                 ng-init="add_exercise.recent_log[$index].reps =exdata.reps" 
                                 ng-model="add_exercise.recent_log[$index].reps">
                              </td>

                              <td ng-if="add_exercise.exercise_type =='resistance'" data-title="WEIGHT(lbs)">
                                <input type="text" size="6"
                                 ng-init="add_exercise.recent_log[$index].weight_lbs =exdata.weight_lbs"
                                 ng-model="add_exercise.recent_log[$index].weight_lbs">
                              </td>
                              <td data-title="EXERCISE LOG">
                                <button class="btn btn-primary btndesign" ng-click="add_exercise.logExercise(exdata,$index,'recent')">Log Exercise</button>
                              </td>
                          </tr>

                        </tbody>
                      </table>
                    </div>
                 </div>
                 <div id="searched_exercise_tab" class="tab-pane fade in"> 
                    <div class="tbl_tab_food_log">
                      <table ng-show="add_exercise.search_result" class="table table-bordered tbl_log_food text-center">
                        <thead>
                          <tr>
                            <th><h4>EXERCISE NAME</h4></th>
                            <th><h4>CALORIES BURNED</h4></th>
                            <th><h4>MINUTES</h4></th>
                            <th  ng-if="add_exercise.exercise_type =='resistance'"><h4>SETS</h4></th>
                            <th  ng-if="add_exercise.exercise_type =='resistance'"><h4>REPS</h4></th>
                            <th  ng-if="add_exercise.exercise_type =='resistance'"><h4>WEIGHT(lbs)</h4></th>
                            <th><h4>EXERCISE LOG</h4></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr ng-repeat="exdata in add_exercise.exerciseSearchdata">
                              <td class="text-left" data-title="EXERCISE NAME">
                                <p class="theme_text_p_lg">@{{exdata.name}}</p>
                                <p class="food_log_normalbold">@{{exdata.time}}, 
                                  minutes @{{exdata.calories_burned}} calc</p>
                              </td>
                              <td data-title="CALORIES BURNED">
                              <p class="food_log_normalbold"
                                 ng-bind="add_exercise.search_log[$index].calories_burned" 
                                 ng-init="add_exercise.search_log[$index].calories_burned =exdata.calories_burned"
                               ></p></td>
                              
                              <td data-title="MINUTES">
                                <input type="text"
                                       ng-init="add_exercise.search_log[$index].time =exdata.time"
                                       size="6"
                                       ng-model="add_exercise.search_log[$index].time"
                                       ng-change="add_exercise.calculateCaloriesBurned($index,exdata,'search');"
                                   >
                              </td>
                              <td ng-if="add_exercise.exercise_type =='resistance'" data-title="SETS">
                                <input type="text" size="6" ng-model="add_exercise.search_log[$index].sets">
                              </td>

                              <td ng-if="add_exercise.exercise_type =='resistance'" data-title="REPS">
                                <input type="text" size="6" ng-model="add_exercise.search_log[$index].reps">
                              </td>

                              <td ng-if="add_exercise.exercise_type =='resistance'" data-title="WEIGHT(lbs)">
                                <input type="text" size="6" ng-model="add_exercise.search_log[$index].weight_lbs">
                              </td>
                              <td data-title="EXERCISE LOG">
                                <button class="btn btn-primary btndesign" ng-click="add_exercise.logExercise(exdata,$index,'search')">Log Exercise</button>
                              </td>
                          </tr>
                        </tbody>
                      </table>
                      <div ng-show="add_exercise.searchNodata" class="text-center">No data found</div>
                      <div ng-show="add_exercise.search_loader"  class="search_food_loader"></div>
                    </div>
                 </div>
                 <div id="selected_log_tab"  ng-show="add_exercise.is_selected" class="tab-pane fade in"> 
                    <div class="tbl_tab_food_log">
                      <table class="table table-bordered tbl_log_food text-center">
                        <thead>
                          <tr>
                           <th><h4>EXERCISE NAME</h4></th>
                            <th><h4>CALORIES BURNED</h4></th>
                            <th><h4>MINUTES</h4></th>
                            <th  ng-if="add_exercise.selectedExrcise.exercise_type =='Resistance'"><h4>SETS</h4></th>
                            <th  ng-if="add_exercise.selectedExrcise.exercise_type =='Resistance'"><h4>REPS</h4></th>
                            <th  ng-if="add_exercise.selectedExrcise.exercise_type =='Resistance'"><h4>WEIGHT(lbs)</h4></th>
                            <th><h4>EXERCISE LOG</h4></th>
                          </tr>
                        </thead>
                        <tbody>
                          
                          <tr>
                              <td class="text-left">
                                <p class="theme_text_p_lg">@{{add_exercise.selectedExrcise.name}}</p>
                                <p class="food_log_normalbold">@{{add_exercise.selectedExrcise.time}}, 
                                  minutes @{{add_exercise.selectedExrcise.calories_burned}} calc</p>
                              </td>
                              <td>
                              <p class="food_log_normalbold"
                                 ng-bind="add_exercise.select_log.calories_burned" 
                                 ng-init="add_exercise.select_log.calories_burned =add_exercise.selectedExrcise.calories_burned"
                               ></p></td>
                              
                              <td>
                                <input type="text"
                                       ng-init="add_exercise.select_log.time =add_exercise.selectedExrcise.time"
                                       size="6"
                                       ng-model="add_exercise.select_log.time"
                                       ng-change="add_exercise.calculateCaloriesBurned($index,add_exercise.selectedExrcise,'selected');"
                                   >
                              </td>
                              <td ng-if="add_exercise.selectedExrcise.exercise_type =='Resistance'">
                                <input type="text" size="6"
                                  ng-init="add_exercise.select_log.sets =add_exercise.selectedExrcise.sets" 
                                 ng-model="add_exercise.select_log.sets">
                              </td>

                              <td ng-if="add_exercise.selectedExrcise.exercise_type =='Resistance'">
                                <input type="text" size="6"
                                 ng-init="add_exercise.select_log.reps =add_exercise.selectedExrcise.reps" 
                                 ng-model="add_exercise.select_log.reps">
                              </td>

                              <td ng-if="add_exercise.selectedExrcise.exercise_type =='Resistance'">
                                <input type="text" size="6"
                                 ng-init="add_exercise.select_log.weight_lbs =add_exercise.selectedExrcise.weight_lbs"
                                 ng-model="add_exercise.select_log.weight_lbs">
                              </td>
                              <td>
                                <button class="btn btn-primary btndesign" ng-click="add_exercise.logExercise(add_exercise.selectedExrcise,$index,'selected')">Log Exercise</button>
                              </td>
                          </tr>

                        </tbody>
                      </table>
                    </div>
                 </div> 

              </div>
            </div>
          </div>
        </div>
 </div>
</div>  

