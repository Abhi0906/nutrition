<div class="row">
    <div class="col-md-12 col-lg-12">
      <div class="exe_synbtn_contain">  
        <ul class="list-inline">
          <li>
            <button class="btn btn-primary btndesign btn-sm">View Full Report</button>
          </li>
          <li>
            <button class="btn btn-primary btndesign btn-sm"><i class="fa fa-print"></i> Print Full Report</button>
          </li>
        </ul>
      </div>
    </div>
</div>

<div class="row text-center">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <strong><h2>MONTHLY EX<span class="title_head_line">ERCIS</span>E SUMMARY</h2></strong>
    </div>
</div>
<div class="saperator"></div>
<div class="row calander_container_portion">
  <div class="col-xs-6 col-sm-10 col-md-10 col-lg-10 calendar_portion">
   <div ng-model="exercise_diary.eventSources"
    ui-calendar="exercise_diary.uiConfig.calendar" 
    calendar="exerciseCalendar" 
    class="exercise_monthly_calender angular-calender hidden-xs"
   ></div>
    

    <div class="visible-xs">
      <table class="table table-bordered calendar_blockequal left_side_table_cale">
        <thead>
          <tr class="tab_firsthead">
            <th class="calendar_table_head">Month Average</th>
          </tr>
        </thead>
        <tbody>
          <tr class="tab_colnum1 tablecell_blockqual">
            <td>
              <p>First Week Average</p>   
            </td>
          </tr>
          <tr class="tablecell_blockqual">
            <td>
              <p>Second Week Average</p>   
            </td>
          </tr>
          <tr class="tablecell_blockqual">
            <td>
              <p>Third Week Average</p>   
            </td>
          </tr>
          <tr class="tablecell_blockqual">
            <td>
              <p>Fourth Week Average</p>   
            </td>
          </tr>
          <tr class="tablecell_blockqual">
            <td>
              <p>Fifth Week Average</p>   
            </td>
          </tr>
          <tr class="tablecell_blockqual">
            <td>
              <p>Sixth Week Average</p>   
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 calendar_week_avg exe_top_cal">
    <table id="week_average" class="table table-bordered calendar_blockequal right_side_table_cale">
      <thead>
        <tr class="tab_firsthead">
          <th class="calendar_table_head">Week Average</th>
        </tr>
      </thead>
      <tbody>
        <tr class="tablecell_blockqual">
          <td>
            <div class="tabletd-div calendar-week-cell week_1">
              &nbsp;
            </div>
          </td>
        </tr>
        <tr class="tablecell_blockqual">
          <td>            
              <div class="tabletd-div calendar-week-cell week_2">
            </div>
          </td>
        </tr>
        <tr class="tablecell_blockqual">
          <td>
            <div class="tabletd-div calendar-week-cell week_3">
            </div>
          </td>
        </tr>
        <tr class="tablecell_blockqual">
          <td>
            <div class="tabletd-div calendar-week-cell week_4">
            </div>
          </td>
        </tr>
        <tr class="tablecell_blockqual">
          <td>
            <div class="tabletd-div calendar-week-cell week_5">
            </div>
          </td>
        </tr>
        <tr class="tablecell_blockqual">
          <td>
            <div class="tabletd-div calendar-week-cell week_6">
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
  <div class="row">
  <div class="col-xs-6 col-sm-10 col-md-10 col-lg-10 footercal_side">
    <div class="monthly_calendar_footer columnfooterdiv" id="monthly_left">
      <p class="footer_para">Month Average</p>
    </div>
  </div>
 <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 footercal_side footercal_side_right">
    <div class="monthly_calendar_footer monthly_rightside columnfooterdiv" id="monthly_right">
        <p ng-bind-html="exercise_diary.calories" class="no_margin"></p>
    </div>
  </div>
</div>
</div>