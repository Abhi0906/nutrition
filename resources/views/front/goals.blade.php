@extends('layout.master')
@section('title')
  Goals Summary
@endsection
@section('custom-css')
 <style type="text/css">
    .table_fixed_height
    {
      /*height: 335px;*/
      height: auto;
      margin-bottom: 0px;
    }
    .table_fixed_height_water{
      /*height: 120px;*/
      height: auto;
      margin-bottom: 0px;
    }
   .table.table_fixed_height tr td, .table_fixed_height_water tr td
    {
      /*border: 1px solid #f0f8ff!important;*/
    }
    .table.table_fixed_height tr td,
    .table.table_fixed_height_water tr td
    {
      vertical-align: middle;
      text-align: center;
    }
    .goal_log_summary .control-label
    {
      padding: 0px;
      font-size: 12px;
    }
    .form_contain_td
    {
      background: #F9F9F9;
    }
    .table_fixed_bottom
    {
      height: 15%;
    }
    .table .fixed_colum_width
    {
      width: 25%;
    }
    .table_width_half
    {
      width: 50%;
    }
    .nopadding_column
    {
      padding-left: 0px;
      padding-right: 0px;
    }
    .full_table_size
    {
      width: 100%;
      height: 100%;
      margin-bottom: 0px;
    }
    .btn.btn-primary.btndesign.fa.fa-refresh
    {
      height: 35px !important;
    }

    @media only screen and (max-width: 480px){
    .label_responsive{
       margin-left: 14px;
      }
    }
    span.btndesign
    {
      background: #0071BE;
      color: #fff;
    }

</style>

<style type="text/css">
    ul.progress_loadbar {
    list-style:none;
    width:auto;
    display: inline-block;
    margin:0 auto;
    }
    ul.progress_loadbar li {
    float:left;
    position:relative;
    width:4px;
    height:37px;
    margin-left:4px;
    /*border-left:1px solid #E5E5E5; border-top:1px solid #E5E5E5; border-right:1px solid #E5E5E5; border-bottom:1px solid #E5E5E5;*/
    background:#E5E5E5;
    }

    ul.progress_loadbar li:first-child { margin-left:0; }
    .bar {
    width:4px;
    height:37px;
    opacity:0;
    -webkit-animation:fill .5s linear forwards;
    -moz-animation:fill .5s linear forwards;
    }

    .one.bar{
      background-color:#BF1F2E;
    }
    .two.bar{
      background-color:#EF3F37;
    }
    .three.bar{
      background-color:#F1582A;
    }

    .four.bar{
      background-color:#F7931D;
    }
    .five.bar{
      background-color:#FBB040;
    }
    .sixth.bar{
      background-color:#F9F372;
    }
    .seven.bar{
      background-color:#D6DF22;
    }
    .eight.bar{
      background-color:#8DC640;
    }
    .nine.bar{
      background-color:#7bb135;
    }
    .ten.bar{
      background-color:#527623;
    }


    @-moz-keyframes fill {
    0%{ opacity:0; }
    100%{ opacity:1; }
    }

    @-webkit-keyframes fill {
    0%{ opacity:0; }
    100%{ opacity:1; }
    }

    .progress_ul
    {
      padding: 0px !important;
      margin-bottom: 2px;
      margin-left: 0px;
    }
    .progress_ul li ul
    {
      padding: 0px;
      margin: 0 2px;
    }
    .progress_ul li
    {
      vertical-align: middle;
      display: inline-table !important;
    }
</style>

@endsection
@section('content')
<!-- Start Main Page -->
<section class="section-panel custom-section-panel" ng-controller="GoalsController as goals"  ng-init="goals.userId='{{$user_id}}';goals.getPatientGoals();">
   <!-- header title -->
   <div class="row">
      <div class="col-xs-12 text-center">
         <h2 class="text-primary head_title_main">GOAL<span class="title_head_line_pulse_bp">S SUM</span>MARY</h2>
      </div>
   </div>
   <div class="saperator"></div>
   <!-- End of header title -->
   <!-- calories tables row -->
   <div class="row tab_chart_row goal_log_summary"> 
      <!-- CALORIE and body fat portion -->
       @include('front.goals.calorie')

      <!-- end of CALORIE portion -->
      <!-- body measurement portion -->
        <!-- @include('front.goals.body_measurement') -->
        @include('front.goals.bp_pulse')
      <!-- end of body measurement portion -->
   </div>
   <!-- end of calories tables -->
   <!-- weight tables row -->
   <div class="row tab_chart_row goal_log_summary">
      <!-- weight and body fat portion -->
         @include('front.goals.weight')
      <!-- end of weight portion -->
      <!-- bp & pulse portion -->
       <!-- @include('front.goals.bp_pulse') -->
        @include('front.goals.body_measurement')
      <!-- end of bp & pulse portion -->
   </div>
   <!-- end of weight tables -->
    <!-- weight tables row -->
   <div class="row tab_chart_row goal_log_summary">
      <!-- Sleep portion -->
       @include('front.goals.sleep')
      <!-- end of Sleep portion -->
      <!-- Water portion -->
      @include('front.goals.water')
      <!-- end Water portion -->
   </div>
   <!-- end of weight tables -->

</section>


<!-- End Main Page -->
@endsection

@section('js')
 {!! HTML::script("js/controllers/GoalsController.js") !!}
 {!! HTML::script("js/directives/inputDatepicker.js") !!}
 {!! HTML::script("js/directives/progressBar.js") !!}
 {!! HTML::script("js/jquery.sameheight.js") !!}
@append

@section('custom-js')
  <script type="text/javascript">

    $(document).ready(function(){

      if($(window).width() >= 768){
        $('.table_fixed_height').sameheight();
        $('.table_fixed_height_water').sameheight();

      }

      if($(window).width() >= 1200){
        $('.goalsummary_tbl1').sameheight();
        $('.weightgolas_bppulse_tbl2').sameheight();
        $('.sleepgoal_watergoal_tbl3').sameheight();
      }
    });

  </script>

@endsection