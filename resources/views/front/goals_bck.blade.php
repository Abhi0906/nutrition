@extends('layout.app')
@section('content') 
<div ng-controller="GoalsController as goals"  ng-init="goals.userId='{{$user_id}}';goals.getPatientGoals();">
   <div class="block health-goal">
      <div class="row">
         <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
            <div class="contain-block-goal">
               <table class="table table-bordered">
                  <th class="block-title" colspan="2">
                     <h4>Weight Goal</h4>
                  </th>
                  <tbody>
                     <tr>
                        <td>
                           <div class="gauge1 guagemeterdemo"></div>
                           <div>
                              <h3 class="gauge-subtitle">75% <span class="goal-text">of GOAL</span></h3>
                           </div>
                        </td>
                        <td>
                           <form class="form-horizontal" role="form">
                              <div class="form-group">
                                 <label class="control-label col-sm-5 col-md-5 col-lg-5 col-xl-5 col-xs-5" for="current_weight">
                                 Current Weight&nbsp;*
                                 </label>
                                 <div class="col-sm-7 col-md-7 col-lg-7 col-xl-7 col-xs-7">
                                    <input type="text" class="form-control" name="current_weight" />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="control-label col-sm-5 col-md-5 col-lg-5 col-xl-5 col-xs-5" for="goal_weight">
                                 Goal Weight&nbsp;*
                                 </label>
                                 <div class="col-sm-7 col-md-7 col-lg-7 col-xl-7 col-xs-7">
                                    <input type="text" class="form-control" name="goal_weight" />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="control-label col-sm-5 col-md-5 col-lg-5 col-xl-5 col-xs-5" for="goal_body_fat">
                                 Goal Body Fat&nbsp;*
                                 </label>
                                 <div class="col-sm-7 col-md-7 col-lg-7 col-xl-7 col-xs-7">
                                    <input type="text" class="form-control" name="goal_body_fat" />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="control-label col-sm-5 col-md-5 col-lg-5 col-xl-5 col-xs-5" for="current_weight">
                                 Weekly Goal&nbsp;*
                                 </label>
                                 <div class="col-sm-7 col-md-7 col-lg-7 col-xl-7 col-xs-7">
                                    <input type="text" class="form-control" name="weekly_Goal" />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-offset-5 col-sm-7 text-right">
                                    <input type="button" class="btn btn-primary" value="Save Changes" />
                                 </div>
                              </div>
                           </form>
                        </td>
                     </tr>
                     <tr>
                        <td colspan="2" class="text-center">
                           <button class="btn btn-default">Log Weight</button>
                           <button class="btn btn-default">Weight History</button>
                        </td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <div class="contain-block-goal">
               <table class="table table-bordered">
                  <th colspan="2" class="block-title">
                     <h4>Water Goal </h4>
                  </th>
                  <tbody>
                     <tr>
                        <td>
                           <img width="20" ng-src="img/cup.png" src="img/cup.png">
                           <img width="20" ng-src="img/cup.png" src="img/cup.png">
                           <img width="20" ng-src="img/cup.png" src="img/cup.png">
                           <img width="20" ng-src="img/cup.png" src="img/cup.png"><br>
                           <img width="20" ng-src="img/cup.png" src="img/cup.png">
                           <img width="20" ng-src="img/cup.png" src="img/cup.png">
                           <img width="20" ng-src="img/cup.png" src="img/cup.png">
                           <img width="20" ng-src="img/cup.png" src="img/cup.png"><br>
                           <img width="20" ng-src="img/cup.png" src="img/cup.png">
                           <img width="20" ng-src="img/null_cup.png" src="img/null_cup.png">
                           <img width="20" ng-src="img/null_cup.png" src="img/null_cup.png">
                           
                           <div>
                              <h3 class="gauge-subtitle">85% <span class="goal-text">of GOAL</span></h3>
                           </div>
                        </td>
                        <td>
                           <form class="form-horizontal" role="form">
                              <div class="form-group">
                                 <label class="control-label col-sm-5 col-md-5 col-lg-5 col-xl-5 col-xs-5" for="Water Intake">
                                 Water Intake &nbsp;*
                                 </label>
                                 <div class="col-sm-7">
                                    <input type="text" class="form-control" name="water_intake" />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-offset-5 col-sm-7 text-right">
                                    <input type="button" class="btn btn-primary" value="Save Changes" />
                                 </div>
                              </div>
                           </form>
                        </td>
                     </tr>
                     <tr>
                        <td colspan="2" class="text-center">
                           <button class="btn btn-default">Log Water Intake</button>
                           <button class="btn btn-default">Water History</button>
                        </td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
         <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
            <div class="contain-block-goal">
               <table class="table table-bordered">
                  <th class="block-title" colspan="2">
                     <h4>Body Measurements</h4>
                  </th>
                  <tbody>
                     <tr>
                        <td>
                            <div class="col-xl-12 col-lg-6 col-md-6 col-sm-6 col-xs-12 health-sub-contain lastchartcontain">
                              <div id="chartdiv3"></div> 
                            
                              <div class="form-group">
                                 <label class="col-sm-12 col-md-2 col-lg-2 col-xl-2 col-xs-2" for="current_weight">
                                 Neck&nbsp;*
                                 </label>
                                 <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 col-xs-4 ">
                                    <input type="text" placeholder="" class="form-control">
                                 </div>
                                 <label class="col-sm-2 col-md-2 col-lg-2 col-xl-2 col-xs-2" for="current_weight">
                                 Hips&nbsp;*
                                 </label>
                                 <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 col-xs-4">
                                    <input type="text" placeholder="" class="form-control">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 col-md-2 col-lg-2 col-xl-2 col-xs-2" for="current_weight">
                                 Arms&nbsp;*
                                 </label>
                                 <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 col-xs-4">
                                    <input type="text" placeholder="" class="form-control">
                                 </div>
                                 <label class="col-sm-12 col-md-2 col-lg-2 col-xl-2 col-xs-2" for="current_weight">
                                 Hips&nbsp;*
                                 </label>
                                 <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4 col-xs-4">
                                    <input type="text" placeholder="" class="form-control">
                                 </div>
                              </div>

                              <div class="form-group">
                                 <label class="col-sm-12 col-md-2 col-lg-2 col-xl-2 col-xs-2" for="current_weight">
                                 Legs&nbsp;*
                                 </label>
                                 <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 col-xs-4">
                                    <input type="text" placeholder="" class="form-control">
                                 </div>
                                
                                    <div class="text-right">
                                       <input style="" type="button" class="btn btn-primary" value="Save Changes" />
                                    </div>
                                 
                              </div>  
                           </div>
                        </td>
                     </tr>
                     <tr>
                        <td colspan="2" class="text-center">
                           <button class="btn btn-default">Log Measurements</button>
                           <button class="btn btn-default">Body History</button>
                        </td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <div class="contain-block-goal">
               <table class="table table-bordered">
                  <th class="block-title" colspan="2">
                     <h4>Nutrition Goal</h4>
                  </th>
                  <tbody>
                     <tr>
                        <td>
                           <div class="gauge2 guagemeterdemo"></div>
                           <div>
                              <h3 class="gauge-subtitle">75% <span class="goal-text">of GOAL</span></h3>
                           </div>
                        </td>
                        <td>
                           <form class="form-horizontal" role="form">
                              <div class="form-group">
                                 <label class="control-label col-sm-5 col-md-5 col-lg-5 col-xl-5 col-xs-5" for="current_weight">
                                 Calories&nbsp;*
                                 </label>
                                 <div class="col-sm-7">
                                    <input type="text" class="form-control" name="current_weight" />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-offset-5 col-sm-7 text-right">
                                    <input type="button" class="btn btn-primary" value="Save Changes" />
                                 </div>
                              </div>
                           </form>
                        </td>
                     </tr>
                     <tr>
                        <td colspan="2" class="text-center">
                           <button class="btn btn-default">Log Calories</button>
                           <button class="btn btn-default">View History</button>
                        </td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
         <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
            <div class="contain-block-goal">
               <table class="table table-bordered">
                  <th class="block-title" colspan="2">
                     <h4>Blood Pressure Goal</h4>
                  </th>
                  <tbody>
                     <tr>
                        <td>
                           <div class="gauge1 guagemeterdemo"></div>
                           <div>
                              <h3 class="gauge-subtitle">75% <span class="goal-text">of GOAL</span></h3>
                           </div>
                        </td>
                        <td>
                           <form class="form-horizontal" role="form">
                              <div class="form-group">
                                 <label class="control-label col-sm-12 col-md-5 col-lg-5 col-xl-5 col-xs-5" for="current_weight">
                                 Systolic&nbsp;*
                                 </label>
                                 <div class="col-sm-7">
                                    <input type="text" class="form-control" name="current_weight" />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="control-label col-sm-12 col-md-5 col-lg-5 col-xl-5 col-xs-5" for="goal_weight">
                                 Diastolic&nbsp;*
                                 </label>
                                 <div class="col-sm-7">
                                    <input type="text" class="form-control" name="goal_weight" />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-offset-5 col-sm-7 text-right">
                                    <input type="button" class="btn btn-primary" value="Save Changes" />
                                 </div>
                              </div>
                           </form>
                        </td>
                     </tr>
                     <tr>
                        <td colspan="2" class="text-center">
                           <button class="btn btn-default">Log BP</button>
                           <button class="btn btn-default">BP History</button>
                        </td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <div class="contain-block-goal">
               <table class="table table-bordered">
                  <th class="block-title" colspan="2">
                     <h4>Heart Rate</h4>
                  </th>
                  <tbody>
                     <tr>
                        <td>
                           <div class="gauge1 guagemeterdemo"></div>
                           <div>
                              <h3 class="gauge-subtitle">75% <span class="goal-text">of GOAL</span></h3>
                           </div>
                        </td>
                        <td>
                           <form class="form-horizontal" role="form">
                              <div class="form-group">
                                 <label class="control-label col-sm-5 col-md-5 col-lg-5 col-xl-5 col-xs-5" for="current_weight">
                                 Heart Rate&nbsp*
                                 </label>
                                 <div class="col-sm-7">
                                    <input type="text" class="form-control" name="current_weight" />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-offset-5 col-sm-7 text-right">
                                    <input type="button" class="btn btn-primary" value="Save Changes" />
                                 </div>
                              </div>
                           </form>
                        </td>
                     </tr>
                     <tr>
                        <td colspan="2" class="text-center">
                           <button class="btn btn-default">Log Heart Rate</button>
                           <button class="btn btn-default">View History</button>
                        </td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <div class="contain-block-goal">
               <table class="table table-bordered">
                  <th class="block-title" colspan="2">
                     <h4>Sleep Goal</h4>
                  </th>
                  <tbody>
                     <tr>
                        <td>
                           <div class="gauge1 guagemeterdemo"></div>
                           <div>
                              <h3 class="gauge-subtitle">75% <span class="goal-text">of GOAL</span></h3>
                           </div>
                        </td>
                        <td>
                           <form class="form-horizontal" role="form">
                              <div class="form-group">
                                 <label class="control-label col-sm-5 col-md-5 col-lg-5 col-xl-5 col-xs-5" for="current_weight">
                                 Sleep&nbsp;*
                                 </label>
                                 <div class="col-sm-7">
                                    <input type="text" class="form-control" name="weekly_Goal" />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-offset-5 col-sm-7 text-right">
                                    <input type="button" class="btn btn-primary" value="Save Changes" />
                                 </div>
                              </div>
                           </form>
                        </td>
                     </tr>
                     <tr>
                        <td colspan="2" class="text-center">
                           <button class="btn btn-default">Log Sleep</button>
                           <button class="btn btn-default">Sleep History</button>
                        </td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection   
@section('page_scripts')
{!! HTML::script("js/controllers/GoalsController.js") !!}
{!! HTML::script("js/directives/inputDatepicker.js") !!}
@append