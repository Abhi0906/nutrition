@extends('layout.app')
@section('content') 
<div class="row" ng-controller="SleepController as weight" ng-init="weight.userId='{{$user_id}}';weight.loggedWeight(); ">
   <div class="col-sm-8">
      <div class="block full">
         <div class="block-title">
            <h4>Sleep Graph</h4>
         </div>
         <div class="col-2">
              <div id="chartdiv"></div> 
         </div>
      </div>
      <div class="block full">
      <div class="block-title">
            <h4 >Sleep Log</h4>
      </div>
		
		<p style="margin-left: 9px;"><strong>Create a new sleep record </strong> </p>
         <form name="sleepform">
            <div class="row">
               <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                  <h5 class="sleep_h5">Entered bed</h5>
                  <div class="header">
                     <ul class="sleeplog-ul">
                        <li class="sleeplog-li">
                           <input type="text" class="form-control sleep">
                        </li>
                        <li class="sleeplog-li"> : </li>
                        <li class="sleeplog-li">
                           <input type="text" class="form-control sleep">
                        </li>
                        <li class="sleeplog-li">
                           <select class="form-control">
                              <option>pm</option>
                           </select>
                        </li>
                     </ul>
                  </div>
                  <br><br>
                  <div class="form-group">
                     {!! HTML::decode(Form::label('on','<span class="on_date_left">On</span><sup class="text-danger ">*</sup>', array('class'=>'col-md-1 control-label'))) !!}
                     <div class="col-md-8">
                        <div class="input-group" style="margin-left: 20px;">
                           <input-datepicker  format="MM dd, yyyy" date="weight.log_weight.log_date" disabled="disabled"></input-datepicker>
                        </div> 
                     </div>
                  </div>
                  <br><br><br>
                  <div class="form-group" style="margin-left: -7px!important;" ng-class="{ 'has-error' : goalsForm.weekly_goal.$invalid && !goalsForm.weekly_goal.$pristine }">
                        {!! HTML::decode(Form::label('weekly_goal', 'on the<sup class="text-danger">*</sup>', array('class'=>'col-md-4 control-label'))) !!}
                        <div class="col-md-6">
                           <select ng-model="goals.weekly_goal_id" name="weekly_goal" id="weekly_goal" 
                              		class="form-control" style="margin-left: -72px!important;" ng-change="goals.changeCalories();" required >
                           <option value="">Previous day</option>
                           </select>
                        </div>
                  </div>
				  <br>
                  <div>
                     <ul class="sleeplog-ul">
                        <li class="sleeplog-li">
                           &nbsp;&nbsp; <mark class="mark">12 hr</mark> &nbsp;&nbsp;
                        </li>
                        <li class="sleeplog-li color_hr"> 24 hr </li>
                     </ul>
                  </div>
                  <br>
                  <div>
                     <ul class="sleeplog-ul">
                        <li class="sleeplog-li">
                           <div class="input-group">
                              <button class="btn btn-primary sleep_h5" type="button" >Log Sleep</button>
                           </div>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                  <h5 class="sleep_h5">Left bed</h5>
                  <ul class="sleeplog-ul">
                     <li class="sleeplog-li">
                        <input type="text" class="form-control sleep">
                     </li>
                     <li class="sleeplog-li"> : </li>
                     <li class="sleeplog-li">
                        <input type="text" class="form-control sleep">
                     </li>
                     <li class="sleeplog-li">
                        <select class="form-control">
                           <option>am</option>
                        </select>
                     </li>
                  </ul>
               </div>
            </div>
         </form>
      </div>
   </div>
   <div class="col-sm-4">
      <div class="block">
         <div class="block-title">
            <h4>Sleep Stats</h4>
         </div>
         <p>Time a sleep over the past 30 days in hours
            <br> 2.50
            <br> 1.50
            <br> 0.50
            <br> &nbsp; &nbsp; &nbsp; &nbsp;Mar 20 &nbsp;&nbsp; Mar 26&nbsp;&nbsp; Apr 01 &nbsp;&nbsp;Apr 07 &nbsp; &nbsp;Apr 13 &nbsp;&nbsp;Apr 19
            <br><br>
            Times awake or restless over the past 30 days
            <br> &nbsp;&nbsp;&nbsp; &nbsp; 5
            <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3
            <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1
            <br> &nbsp; &nbsp; &nbsp; &nbsp;Mar 20 &nbsp;&nbsp; Mar 26&nbsp;&nbsp; Apr 01 &nbsp;&nbsp;Apr 07 &nbsp; &nbsp;Apr 13 &nbsp;&nbsp;Apr 19
            <br><br>
            Total time a sleep over the past 30 days in hours
            <br> 2.50
            <br> 1.50
            <br> 0.50
            <br> &nbsp; &nbsp; &nbsp; &nbsp;Mar 20 &nbsp;&nbsp; Mar 26&nbsp;&nbsp; Apr 01 &nbsp;&nbsp;Apr 07 &nbsp; &nbsp;Apr 13 &nbsp;&nbsp;Apr 19
            <br><br> 
            Total time a sleep over the past 30 days in hours
            <br> 2.50
            <br> 1.50
            <br> 0.50
            <br> &nbsp; &nbsp; &nbsp; &nbsp;Mar 20 &nbsp;&nbsp; Mar 26&nbsp;&nbsp; Apr 01 &nbsp;&nbsp;Apr 07 &nbsp; &nbsp;Apr 13 &nbsp;&nbsp;Apr 19
            <br><br>
            Total time a sleep over the past 30 days in hours
            <br> 2.50
            <br> 1.50
            <br> 0.50
            <br> &nbsp; &nbsp; &nbsp; &nbsp;Mar 20 &nbsp;&nbsp; Mar 26&nbsp;&nbsp; Apr 01 &nbsp;&nbsp;Apr 07 &nbsp; &nbsp;Apr 13 &nbsp;&nbsp;Apr 19
            <br><br>
         </p>
      </div>
   </div>
</div>

@endsection
<!-- Load and execute javascript code used only in this page -->
@section('page_scripts')
{!! HTML::script("js/controllers/SleepController.js") !!}
{!! HTML::script("js/directives/inputDatepicker.js") !!}
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
@append