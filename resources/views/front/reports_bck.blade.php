@extends('layout.master')
@section('title')
  Reports Summary
@endsection 
@section('custom-css')
@endsection 

@section('content') 

<?php $today_date=date('F d, Y');  
$a = $viewreports;  ?>
<div class="block full">
  <div class="block-title">
        <h4 class="sub_header_dairy">Reports</h4>
  </div>
  <div class="row" ng-controller="DiaryController as diary"  class="animation-fadeInQuick" 
    ng-init="diary.userId='{{$user_id}}';diary.diary_from='{{$today_date}}';
    diary.diary_to='{{$today_date}}'; diary.filter= '{{$a}}'; diary.getHealthDiaryData('get', 7, 'seven-days'); diary.checkAllFilter();">

    <div class="col-xs-12 animation-fadeInQuick">
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4">
            <span style="text-align:right;" class="col-md-2 first_alt">Filter:</span>
            <span class="col-md-10 checkbox">
              <span class="">
                <label for="all_log_filter">
                  <input id="all_log_filter" ng-model="diary.all_log_filter" type="checkbox" value="all" name="all_log_filter" ng-click="diary.checkAllFilter()">
                All
                </label>
                &nbsp;&nbsp;
              </span>
              <span class="" ng-repeat="(key, filter) in diary.diaryFilters">
                <label for="@{{filter.id}}">
                  <input id="@{{filter.id}}" ng-model="filter.selected" type="checkbox" value="food" name="filter.id" ng-click="diary.singleCheckbox();">
                @{{filter.title}}
                </label>
                &nbsp;&nbsp;
              </span>
            </span>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4">
          <span style="text-align:right;" class="col-md-2 first_alt">From:</span>
          <span class="col-md-10">
            <input-datepicker format="MM dd, yyyy" date="diary.diary_from" disabled="disabled"></input-datepicker>
          </span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <span style="text-align:right;" class="col-md-2 first_alt">To:</span>
          <span class="col-md-10">
             <input-datepicker format="MM dd, yyyy" date="diary.diary_to" disabled="disabled"></input-datepicker>
          </span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <span style="text-align:right;">
              <input type="button" class="btn btn-primary"  ng-click="diary.getHealthDiaryData(null, null, null)" value="Get"/>
              <input type="button" class="btn btn-success" ng-disabled="diary.userHealthDiaryLength == 0"  ng-click="diary.getHealthDiaryData('download')" value="Print"/>
          </span>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-12">
          <button type="button" id="seven-days" class="btn btn-xs btn-info days-filter" ng-click="diary.getHealthDiaryData(null, 7, 'seven-days');">Last 7 Days</button>
          <button type="button" id="thirty-days" class="btn btn-xs btn-info days-filter" ng-click="diary.getHealthDiaryData(null, 30, 'thirty-days');">Last 30 Days</button>
          <button type="button" id="ninety-days" class="btn btn-xs btn-info days-filter" ng-click="diary.getHealthDiaryData(null, 90, 'ninety-days');">Last 90 Days</button>
          <button type="button" id="one-eighty-days" class="btn btn-xs btn-info days-filter" ng-click="diary.getHealthDiaryData(null, 180, 'one-eighty-days');">Last 180 Days</button>
          <button type="button" id="year" class="btn btn-xs btn-info days-filter" ng-click="diary.getHealthDiaryData(null, 365, 'year');">Last Year</button>
        </div>
      </div>
      <br>
      <span ng-show="diary.diary_loader"  class="diary_loader"></span>
      <div ng-show="diary.display_diary_data" class="row" ng-repeat="(key, health_diary_data) in diary.userHealthDiary" >
          <div class="block full">
              <div class="block-title text-center">
                  <h2><strong>@{{diary.getDate(key)}}</strong></h2>
              </div>
              <div id="no-more-tables" ng-if="diary.all_filter_display || diary.food_filter_display" >
                <table class="table table-condensed" width="100%">
                    <tbody>
                    <tr>
                        <td width="40%" class="first_alt">Food</td>
                        <td width="10%" class="alt hidden-sm hidden-xs">Calories</td>
                        <td width="10%" class="alt hidden-sm hidden-xs">Carbs</td>
                        <td width="10%" class="alt hidden-sm hidden-xs">Fat</td>
                        <td width="10%" class="alt hidden-sm hidden-xs">Protein</td>
                        <td width="10%" class="alt hidden-sm hidden-xs">Sodium</td>
                        <td width="10%" class="alt hidden-sm hidden-xs">Sugar</td>
                    </tr>
                    </tbody>
                    <tbody ng-repeat="(key, food_diary_data) in health_diary_data" ng-if="key == 'BREAKFAST' || key == 'LUNCH' || key == 'DINNER' || key == 'SNACKS' ">
                      <tr>
                            <td colspan="7" class="text-warning"><b>@{{key}}</b></td>
                           
                      </tr>
                      <tr ng-repeat="data in food_diary_data">
                            <td width="40%" data-title="Name">@{{data.nutritions.name}}<br>
                               <small>@{{data.nutritions.nutrition_brand.name}}, (@{{data.nutritions.serving_quantity}} @{{data.nutritions.serving_size_unit}}) </small>
                            </td>
                            <td width="10%" data-title="Calories" class="text-center">@{{data.calories | number:0}}</td>
                            <td width="10%" data-title="Carbs" class="text-center">@{{data.total_carb | number:0 }}</td>
                            <td width="10%" data-title="Fat" class="text-center">@{{data.total_fat | number:0 }}</td>
                            <td width="10%" data-title="Protein" class="text-center">@{{data.protein | number:0 }}</td>
                            <td width="10%" data-title="Sodium" class="text-center">@{{data.sodium | number:0 }}</td>
                            <td width="10%" data-title="Sugar" class="text-center">@{{data.sugars | number:0 }}</td>
                      </tr>
                        
                    </tbody>

                    <tr class="total">
                        <td width="40%" data-title="Total">Total</td>
                        <td width="10%" data-title="Calories" class="text-center">@{{health_diary_data.food_total.food_total_calories | number:0}}</td>
                        <td width="10%" data-title="Carbs" class="text-center">@{{health_diary_data.food_total.food_total_carb | number:0 }}</td>
                        <td width="10%" data-title="Fat" class="text-center">@{{health_diary_data.food_total.food_total_fat | number:0 }}</td>
                        <td width="10%" data-title="Protein" class="text-center">@{{health_diary_data.food_total.food_total_protein | number:0 }}</td>
                        <td width="10%" data-title="Sodium" class="text-center">@{{health_diary_data.food_total.food_total_sodium | number:0 }}</td>
                        <td width="10%" data-title="Sugar" class="text-center">@{{health_diary_data.food_total.food_total_sugars | number:0 }}</td>
                    </tr>
                </table>
              </div>
              <div id="no-more-tables" ng-if="diary.all_filter_display ||  diary.exercise_filter_display" >
                  <table class="table table-condensed" width="100%">
                      <tbody><tr>
                          <td width="40%" class="first_alt">Exercise</td>
                          <td width="30%" class="alt hidden-sm hidden-xs">Time in minutes</td>
                          <td width="30%" class="alt hidden-sm hidden-xs">Calories burned</td>
                      </tr>
                      </tbody>
                      <tbody ng-repeat="(key, exercise_diary_data) in health_diary_data" ng-if="key == 'CARDIO' || key == 'STRENGTH' ">
                        <tr>
                            <td colspan="7" class="text-warning"><b>@{{key}}</b></td>
                        </tr>
                        <tr ng-repeat="data in exercise_diary_data">
                            <td width="40%">@{{data.exercise.name}}</td>
                            <td width="30%" data-title="Time in minutes" class="text-center">@{{data.time}}</td>
                            <td width="30%" data-title="Calories burned" class="text-center">@{{data.calories_burned | number:0}}</td>
                        </tr>
                      </tbody>
                      <tr class="total">
                          <td width="40%">Total</td>
                          <td width="30%" data-title="Time in minutes" class="text-center">@{{health_diary_data.exercise_total.total_time}}</td>
                          <td width="30%" data-title="Calories burned" class="text-center">@{{health_diary_data.exercise_total.total_calories_burned | number:0}}</td>
                      </tr>
                  </table>
              </div>
              <div id="no-more-tables" ng-if="diary.all_filter_display ||  diary.weight_filter_display">
                  <table class="table table-condensed" width="100%">
                      <tbody><tr>
                          <td width="40%" class="first_alt">Weight</td>
                          <td width="20%" class="alt hidden-sm hidden-xs">Current Weight</td>
                          <td width="20%" class="alt hidden-sm hidden-xs">Body Fat</td>
                          <td width="20%" class="alt hidden-sm hidden-xs">Source</td>
                      </tr>
                      </tbody>
                      <tbody>
                        
                        <tr class="total" >
                            <td width="40%"></td>
                            <td width="20%" data-title="Current Weight" class="text-center">@{{health_diary_data.weight_data.current_weight}} lbs</td>
                            <td width="20%" data-title="Body Fat" class="text-center">@{{health_diary_data.weight_data.body_fat}} %</td>
                            <td width="20%" data-title="Source" class="text-center">@{{health_diary_data.weight_data.source}}</td>
                        </tr>
                        
                      </tbody>
                  </table>
              </div>
          </div>  
      </div>

      <div ng-cloak  class="alert alert-info animation-fadeInQuick"  ng-show="diary.userHealthDiary.length <= 0">
        <button data-dismiss="alert" class="close" type="button">
          <i class="ace-icon fa fa-times"></i>
        </button>

        <strong>
          Health Diary information not available!
        </strong>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
{!! HTML::script("js/controllers/DiaryController.js") !!}
{!! HTML::script("js/directives/inputDatepicker.js") !!}
@append

@section('custom-js')
 

@endsection