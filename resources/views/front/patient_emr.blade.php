@extends('layout.master')
@section('title')
  Patient Info
@endsection

@section('custom-css')
<style type="text/css">

</style>
@endsection
@section('content') 
  <!-- scroll tabs -->
  <section class="scroll_tab_section">
    <div class="row">
      <div class="col-md-12">
        <div id="scroll_tabs_dash" class="tab_container_plot hidden-xs">
            <ul class="nav nav-tabs nav-justified main_tabs">
                <li class="active">
                  <a class="scroll_divs_unique" id="form_pdash" data-target="#form_pdash_section" href="javascript:void(0)">FORMS</a>
                </li>
                <li>
                <a class="scroll_divs" id="healthrecords_pdash" data-target="#healthrecords_pdash_section" href="javascript:void(0)">HEALTH RECORDS</a>
                </li>
                <li>
                <a class="scroll_divs" id="appointment_pdash" data-target="#appointment_pdash_section" href="javascript:void(0)">APPOINTMENTS</a>
                </li>
                <li>
                <a class="scroll_divs" id="msg_notification_pdash" data-target="#msg_notification_pdash_section" href="javascript:void(0)">MESSAGES/NOTIFICATIONS<sup class="text_red">12 NEW</sup></a>
                </li>
            </ul>
        </div>
      </div>
    </div>
  </section>
  <!-- end scroll tabs -->

  <section class="forms_pdash__section" id="form_pdash_section">
    <div class="row">
      <div class="col-xs-12 text-center">
        <strong><h2 class="theme_blue_color">F<span class="title_head_line">ORM</span>S</h2></strong>
      </div>
    </div>
    <div class="saperator"></div>
    <div class="row">
      <div class="col-xs-12">
        <div class="patients_table_container" id="patients_dash_tbl1">
          <table class="table table-bordered table-striped">
            <thead>
              <tr class="pdash_tbl_head">
                <th colspan="4"><h4>FORMS</h4></th>
              </tr>
              <tr class="pdash_tbl_subhead">
                <th>FORM NAME</th>
                <th>DATE PROVIDED</th>
                <th>STATUS</th>
                <th>DATE SIGNED</th>
              </tr>
            </thead>
            <tbody>
             @foreach($result['forms'] as $form)
              <tr>
                <td data-title="FORM NAME">
                  <p>{{$form['FORM']}}</p>
                </td>
                <td data-title="DATE PROVIDED">
                  <p>{{$form['PROVIDED']}}</p>
                </td>
                <td data-title="STATUS">
                  <p>{{$form['STATUS']}}</p>
                </td>
                 <td data-title="DATE SIGNED">
                  <p>{{$form['SIGNED']}}</p>
                </td>
              </tr>
              @endforeach

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>

  <section class="healthrecords_pdash__section" id="healthrecords_pdash_section">
    <div class="row">
      <div class="col-xs-12  text-center">
        <strong><h2 class="theme_blue_color">HEAL<span class="title_head_line">TH REC</span>ORDS</h2></strong>
      </div>
    </div>
    <div class="saperator"></div>
    <div class="row">
      <div class="col-xs-12">
        <div class="patients_table_container" id="patients_dash_healtherecord_tbl1">
          <table class="table table-bordered table-striped">
            <thead>
              <tr class="pdash_tbl_head">
                <th colspan="6"><h4>MEDICATIONS-GENEMEDICS</h4></th>
              </tr>
              <tr class="pdash_tbl_subhead">
                <th>DATE PRESCRIBED</th>
                <th>MEDICATIONS</th>
                <th>DIRECTIONS</th>
                <th>LAST REFILL DATE</th>
                <th>QUANTITY</th>
                <th>NEXT REFILL DATE</th>
              </tr>
            </thead>
            <tbody>
              @foreach($result['meds'] as $med)
               <tr>
                <td data-title="DATE PRESCRIBED">
                  <p>{{$med['PRESCRIBED']}}</p>
                </td>
                <td data-title="MEDICATIONS">
                  <p>{{$med['MEDICATION']}}</p>
                </td>
                <td data-title="DIRECTIONS">
                  <p>{{$med['DIRECTION']}}</p>
                </td>
                <td data-title="LAST REFILL DATE">
                  <p></p>
                </td>
                <td data-title="QUANTITY">
                  <p>{{$med['QUANTITY']}}</p>
                </td>
                <td data-title="NEXT REFILL DATE" class="">
                 <p>{{$med['START']}}</p>
                </td>
               </tr>
               @endforeach
            

            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="patients_table_container" id="patients_dash_healtherecord_tbl2">
          <table class="table table-bordered table-striped">
            <thead>
              <tr class="pdash_tbl_head">
                <th colspan="4">
                  <div class="row">
                    <div class="col-xs-6 col-sm-7 col-md-7 col-lg-8 text-right"><h4>OTHER MEDICATIONS</h4></div>
                    <div class="col-xs-6 col-sm-5 col-md-5 col-lg-4 text-right">
                      <h4>
                        <span class="p_icon_text">
                          <span class="text_no_space">
                            <a href="#"><i class="fa fa-plus-circle p_icon" aria-hidden="true"></i></a> 
                            <small class="p_icotext">Add New</small>&nbsp;&nbsp;
                          </span>

                          <span class="text_no_space">
                            <a href="#"><i class="fa fa-plus-circle p_icon" aria-hidden="true"></i></a> 
                            <small class="p_icotext">Edit</small>&nbsp;&nbsp;
                          </span>
                        </span>
                      </h4>
                    </div>
                  </div>
                </th>
              </tr>
              <tr class="pdash_tbl_subhead">
                <th>MEDICATIONS</th>
                <th>START DATE</th>
                <th>DIRECTIONS</th>
                <th>PROVIDER</th>
              </tr>
            </thead>
            <tbody> 
            @foreach($result['othermedications'] as $othermedication)
              <tr>
                <td data-title="MEDICATIONS">
                 <p>{{$othermedication['MEDICATION']}}</p>
                </td>
                <td data-title="START DATE">
                  <p>{{$othermedication['START']}}</p>
                </td>
                <td data-title="DIRECTIONS">
                 <p>{{$othermedication['DIRECTIONS']}}</p>
                </td>
                <td data-title="PROVIDER">
                  <p>{{$othermedication['PROVIDER']}}</p>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <div class="col-md-6">
        <div class="patients_table_container" id="patients_dash_healtherecord_tbl3">
          <table class="table table-bordered table-striped">
            <thead>
              <tr  class="pdash_tbl_head">
                <th colspan="3"><h4>LAB TEST RESULTS</h4></th>
              </tr>
              <tr class="pdash_tbl_subhead">
                <th>DATE</th>
                <th>TEST COMPONENT</th>
                <th>YOUR RESULT</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td data-title="DATE">
                </td>
                <td data-title="TEST COMPONENT">
                </td>
                <td data-title="PROVIDER">
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12">
        <div class="patients_table_container" id="patients_dash_healtherecord_tbl4">
          <table class="table table-bordered table-striped">
            <thead>
              <tr class="pdash_tbl_head">
                <th colspan="6">
                  <div class="row">
                    <div class="hidden-xs col-sm-4"></div>
                    <div class="col-xs-5 col-sm-3 text-center"><h4>SUPPLEMENTS</h4></div>
                    <div class="col-xs-7 col-sm-5 text-right">
                      <h4>
                        <span class="p_icon_text">
                          <span class="text_no_space">
                            <a href="#"><i class="fa fa-plus-circle p_icon" aria-hidden="true"></i></a> 
                            <small class="p_icotext">Add/ Order Supplements</small>&nbsp;&nbsp;
                          </span>

                          <span class="text_no_space">
                            <a href="#"><i class="fa fa-check-circle p_icon text-success" aria-hidden="true"></i></a> <small class="p_icotext">Autoship Active</small>&nbsp;&nbsp;
                          </span>
                        </span>
                      </h4>
                    </div>
                  </div>
                </th>
              </tr>
              <tr class="pdash_tbl_subhead">
                <th>START DATE</th>
                <th>SUPPLEMENTS</th>
                <th>DIRECTIONS</th>
                <th>LAST REFILL DATE</th>
                <th>QUANTITY</th>
                <th>NEXT REFILL DATE</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td data-title="START DATE">
                </td>
                <td data-title="SUPPLEMENTS">
                </td>
                <td data-title="DIRECTIONS">
                </td>
                <td data-title="LAST REFILL DATE">
                </td>
                <td data-title="QUANTITY">
                </td>
                <td data-title="NEXT REFILL DATE" class="pending_status">
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="patients_table_container" id="patients_dash_healtherecord_tbl5">
          <table class="table table-bordered table-striped">
            <thead>
              <tr class="pdash_tbl_head">
                <th colspan="3"><h4>IN-OFFICE PROCEDURES</h4></th>
              </tr>
              <tr class="pdash_tbl_subhead">
                <th>DATE</th>
                <th>PROCEDURES NAME</th>
                <th>STATUS</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td data-title="DATE">
                </td>
                <td data-title="PROCEDURES NAME">
                </td>
                <td data-title="STATUS">
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="col-md-6">
        <div class="patients_table_container" id="patients_dash_healtherecord_tbl6">
          <table class="table table-bordered table-striped">
            <thead>
              <tr class="pdash_tbl_head">
                <th colspan="3"><h4>VITALS</h4></th>
              </tr>
              <tr class="pdash_tbl_subhead">
                <th>VITALS NAME</th>
                <th>VALUE</th>
                <th>LAST UPDATED</th>
              </tr>
            </thead>
            <tbody>
              @foreach($result['vitals'] as $vital)
              <tr>
                <td data-title="VITALS NAME">
                   <p>{{$vital['NAME']}}</p>
                </td>
                <td data-title="VALUE">
                  <p>{{$vital['VALUE']}}</p>
                </td>
                <td data-title="LAST UPDATED">
                   <p>{{$vital['DATE']}}</p>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="patients_table_container" id="patients_dash_healtherecord_tbl7">
          <table class="table table-bordered table-striped">
            <thead>
              <tr class="pdash_tbl_head">
                <th colspan="3">
                  <div class="row">
                    <div class="col-xs-6 col-sm-7 col-md-7 col-lg-8 text-right"><h4>MEDICAL PROBLEMS</h4></div>
                    <div class="col-xs-6 col-sm-5 col-md-5 col-lg-4 text-right">
                      <h4>
                        <span class="p_icon_text">
                          <span class="text_no_space">
                            <a href="#"><i class="fa fa-plus-circle p_icon" aria-hidden="true"></i></a> 
                            <small class="p_icotext">Add New</small>&nbsp;&nbsp;
                          </span>

                          <span class="text_no_space">
                            <a href="#"><i class="fa fa-plus-circle p_icon" aria-hidden="true"></i></a> 
                            <small class="p_icotext">Edit</small>&nbsp;&nbsp;
                          </span>
                        </span>
                      </h4>
                    </div>
                  </div>
                </th>
              </tr>
              <tr class="pdash_tbl_subhead">
                <th>DIAGNOSIS NAME</th>
                <th>START DATE</th>
                <th>PROVIDER</th>
              </tr>
            </thead>
            <tbody>
            @foreach($result['problems'] as $problem)
              <tr>
                <td data-title="DIAGNOSIS NAME">
                   <p>{{$problem['PROBLEM']}}</p>
                </td>
                <td data-title="START DATE">
                   <p>{{$problem['DATE']}}</p>
                </td>
                <td data-title="PROVIDER">
                   <p>{{$problem['PROVIDER']}}</p>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <div class="col-md-6">
        <div class="patients_table_container" id="patients_dash_healtherecord_tbl8">
            <table class="table table-bordered table-striped">
              <thead>
                <tr class="pdash_tbl_head">
                  <th colspan="3">
                    <div class="row">
                      <div class="col-xs-6 col-sm-7 col-md-7 col-lg-8 text-right"><h4>ALLERGIES</h4></div>
                      <div class="col-xs-6 col-sm-5 col-md-5 col-lg-4 text-right">
                        <h4>
                        <span class="p_icon_text">
                          <span class="text_no_space">
                            <a href="#"><i class="fa fa-plus-circle p_icon" aria-hidden="true"></i></a> 
                            <small class="p_icotext">Add New</small>&nbsp;&nbsp;
                          </span>

                          <span class="text_no_space">
                            <a href="#"><i class="fa fa-plus-circle p_icon" aria-hidden="true"></i></a> 
                            <small class="p_icotext">Edit</small>&nbsp;&nbsp;
                          </span>
                        </span>
                      </h4>
                      </div>
                    </div>
                  </th>
                </tr>
                <tr class="pdash_tbl_subhead">
                  <th>ALLERGY</th>
                  <th>REACTION</th>
                  <th>SEVERITY</th>
                </tr>
              </thead>
              <tbody>
                @foreach($result['allergies'] as $allergie)
                <tr>
                  <td data-title="ALLERGY">
                    <p>{{$allergie['NAME']}}</p>
                  </td>
                  <td data-title="REACTION">
                    <p>{{$allergie['REACTION']}}</p>
                  </td>
                  <td data-title="SEVERITY">
                     <p>{{$allergie['SEVERITY']}}</p>
                  </td>
                </tr>
              @endforeach
              </tbody>
            </table>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="patients_table_container" id="patients_dash_healtherecord_tbl9">
          <table class="table table-bordered table-striped">
            <thead>
              <tr class="pdash_tbl_head">
                <th colspan="3">
                    <div class="row">
                      <div class="col-xs-6 col-sm-7 col-md-7 col-lg-8 text-right"><h4>SURGICAL HISTORY</h4></div>
                      <div class="col-xs-6 col-sm-5 col-md-5 col-lg-4 text-right">
                        <h4>
                          <span class="p_icon_text">
                            <span class="text_no_space">
                              <a href="#"><i class="fa fa-plus-circle p_icon" aria-hidden="true"></i></a> 
                              <small class="p_icotext">Add New</small>&nbsp;&nbsp;
                            </span>

                            <span class="text_no_space">
                              <a href="#"><i class="fa fa-plus-circle p_icon" aria-hidden="true"></i></a> 
                              <small class="p_icotext">Edit</small>&nbsp;&nbsp;
                            </span>
                          </span>
                        </h4>
                    </div>
                  </div>
                </th>
              </tr>
              <tr class="pdash_tbl_subhead">
                <th>SURGICAL NAME</th>
                <th>START DATE</th>
                <th>PROVIDER</th>
              </tr>
            </thead>
            <tbody>
             @foreach($result['surgeries'] as $surgery)
              <tr>
                <td data-title="SURGICAL NAME">
                  <p>{{$surgery['NAME']}}</p>
                </td>
                <td data-title="START DATE">
                  <p>{{date('M d, Y',strtotime($surgery['START']))}}</p>
                </td>
                <td data-title="PROVIDER">
                  <p>{{$surgery['PROVIDER']}}</p>
                </td>
              </tr>
             @endforeach
              

            </tbody>
          </table>
        </div>
      </div>
      <div class="col-md-6">
        <div class="patients_table_container" id="patients_dash_healtherecord_tbl10">
            <table class="table table-bordered table-striped">
              <thead>
                <tr class="pdash_tbl_head">
                  <th colspan="2">
                    <div class="row">
                      <div class="col-xs-6 col-sm-7 col-md-7 col-lg-8 text-right"><h4>FAMILY HISTORY</h4></div>
                      <div class="col-xs-6 col-sm-5 col-md-5 col-lg-4 text-right">
                        <h4>
                        <span class="p_icon_text">
                          <span class="text_no_space">
                            <a href="#"><i class="fa fa-plus-circle p_icon" aria-hidden="true"></i></a> 
                            <small class="p_icotext">Add New</small>&nbsp;&nbsp;
                          </span>

                          <span class="text_no_space">
                            <a href="#"><i class="fa fa-plus-circle p_icon" aria-hidden="true"></i></a> 
                            <small class="p_icotext">Edit</small>&nbsp;&nbsp;
                          </span>
                        </span>
                      </h4>
                      </div>
                    </div>
                  </th>
                </tr>
                <tr class="pdash_tbl_subhead">
                  <th>DIAGNOSIS NAME</th>
                  <th>FAMILY MEMBER / AGE OF DIAGNOSIS</th>
                </tr>
              </thead>
              <tbody>
               @foreach($result['familyHistory'] as $family)
                <tr>
                  <td data-title="DIAGNOSIS NAME">
                    <p>{{$family['NAME']}}</p>
                  </td>
                  <td data-title="FAMILY MEMBER/AGE OF DIAGNOSIS">
                    <p>{{$family['DETAIL']}}</p>
                  </td>
                </tr>
                @endforeach
               
                <tr>
                  <td colspan="2">
                    <p>NO FURTHER RECORDS FOUND</p>
                  </td>
                </tr>
              </tbody>
            </table>
        </div>
      </div>
    </div>  
  </section>

  <section class="appointment_pdash__section" id="appointment_pdash_section">
    <div class="row">
      <div class="col-xs-12  text-center">
        <strong><h2 class="theme_blue_color">APP<span class="title_head_line">OINTM</span>ENTS</h2></strong>
      </div>
    </div>
    <div class="saperator"></div>
    <div class="row">
      <div class="col-xs-12">
        <div class="patients_table_container" id="patients_dash_tbl_appoint_tab1">
          <table class="table table-bordered table-striped">
            <thead>
              <tr class="pdash_tbl_head">
                <th colspan="4"><h4>APPOINTMENTS</h4></th>
              </tr>
              <tr class="pdash_tbl_subhead">
                <th>DATE</th>
                <th>TYPE</th>
                <th>PROVIDER</th>
                <th>--</th>
              </tr>
            </thead>
            <tbody>
              <tr class="pending_status">
                <td data-title="DATE">
                </td>
                <td data-title="TYPE">
                </td>
                <td data-title="PROVIDER">
                </td>
                 <td data-title="--">
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>

  <section class="messages_pdash__section" id="msg_notification_pdash_section">
    <div class="row">
      <div class="col-xs-12  text-center">
        <strong><h2 class="theme_blue_color">ME<span class="title_head_line">SSAG</span>ES</h2></strong>
      </div>
    </div>
    <div class="saperator"></div>
    <div class="row">
      <div class="col-xs-12">
        <div class="patients_table_container" id="patients_dash_tbl_appoint_tab1">
          <table class="table table-bordered table-striped">
            <thead>
              <tr class="pdash_tbl_head">
                <th colspan="3"><h4>MESSAGES</h4></th>
              </tr>
              <tr class="pdash_tbl_subhead">
                <th>FROM</th>
                <th>SUBJECT</th>
                <th>DATE</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td data-title="FROM">
                </td>
                <td data-title="SUBJECT">
                </td>
                <td data-title="DATE">
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>

@endsection

@section('js')


@append

@section('custom-js')
<script>
 	$(document).ready(function() {

    /*$('.scroll_tab_section').parent().css('margin-top','120px');*/

    $(".main_tabs li").on('click',function() {
      $('ul.main_tabs').find('li').removeClass('active');
      $(this).addClass('active');
    });

    /*scroll on tabs*/
    //$(".tab_container_plot ul li a").removeClass("scroll_divs");
    //	$(".tab_container_plot ul li a").addClass("scroll_divs_first");
    /*end of scroll tabs*/

    // activate tab on scroll
    if($(window).width()>1199){
      var screen_offset = 210;
    }
    else{
      var screen_offset = 180;
    }
    $('body').scrollspy({ target: '#scroll_tabs_dash', offset: screen_offset });
  	//end: activate tab on scroll



    /*  Scroll Tab Script  */
  	$(".tab_container_plot ul li a").removeClass("scroll_divs");
    $(".tab_container_plot ul li a").addClass("scroll_divs_first");

    $(document).on('click','.scroll_divs_first',function(e){
        e.preventDefault();
        var id = $(this).attr('id');

        if($(window).width()>1199) { 
          $('html, body').animate({
            'scrollTop' : $("#"+id+"_section").position().top - 180
          });  
        } else {
          $('html, body').animate({
            'scrollTop' : $("#"+id+"_section").position().top - 130
          });  
        }

        $(".tab_container_plot ul li a").removeClass("scroll_divs_first");
        $(".tab_container_plot ul li a").addClass("scroll_divs");  
        console.log("first click");
    });

    $(document).on('click','.scroll_divs',function(e){
        e.preventDefault();
        var id = $(this).attr('id');
         
        if($(window).width()>1199) { 
          $('html, body').animate({
            'scrollTop' : $("#"+id+"_section").position().top - 60
          });
        } else { 
          $('html, body').animate({
            'scrollTop' : $("#"+id+"_section").position().top - 130
          });
        } 
        
        console.log("second click");
    }); 

    $(document).on('click','.scroll_divs_unique',function(e){

        e.preventDefault();
        $('.tab_container_plot ul li a').removeClass("scroll_divs");
        $('.tab_container_plot ul li a').addClass("scroll_divs_first");
        console.log("Third click");
    }); 
    /* End of Scroll Tab Script  */


  });
</script>

@append