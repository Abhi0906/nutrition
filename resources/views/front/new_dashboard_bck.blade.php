@extends('layout.app')
@section('content')
    <style type="text/css">
       [class^="flaticon-"]:before, [class*=" flaticon-"]:before, [class^="flaticon-"]:after, [class*=" flaticon-"]:after {
       font-family: Flaticon;
       font-size: 14px;
       font-style: normal;
       margin-left: 20px;
       font-weight: 600 !important;
       }
       .control-label{
       margin-top: 9px !important;
       }


    </style>
    <?php
      $first_name = isset($auth0_user_data['first_name'])?$auth0_user_data['first_name']:"";
      $last_name = isset($auth0_user_data['last_name'])?$auth0_user_data['user_metadata']['last_name']:"";
      $gender = isset($auth0_user_data['gender'])?strtolower($auth0_user_data['gender']):"male";
      $birth_date = isset($auth0_user_data['birth_date'])?date('F d, Y',strtotime($auth0_user_data['birth_date'])):date('F d, Y');


    ?>
<div  ng-controller="DashboardController as dash_new" ng-init="dash_new.userId='{{$user_id}}';">
    <!-- email varification and profile pages -->
    <!-- <div class="alert alert-success" >
        <div class="" style="padding-bottom:10px" ng-hide="myVar">
            <div class="">
                <span id="passwordError" ng-show="dash_new.emailValidation_email">
                    <p><small class="text-danger">@{{dash_new.emailValidationMesg_email}}</small></p>
                </span>

                <span id="passwordSuccess" ng-show="dash_new.emailSuccessMsg_email">
                    <p><small class="text-success">@{{dash_new.emailSuccessMsg_email}}</small></p>
                </span>
            </div>
            <div class="">
                <strong>Please check your email for verification. You can access only for 7 days</strong>
                <button type="button" class="btn btn-primary pull-right" ng-click="dash_new.resend_email()">Resend Email</button>
            </div>
        </div>
    </div>   -->
    @if(isset($auth0_user_data['email_varify']) && !($auth0_user_data['email_varify']))
      <div class="alert alert-success" style="width:100%;" ng-hide="myVar">
        <div class="msg-body">
            <span id="passwordError" ng-show="dash_new.emailValidation_email">
                    <p><small class="text-danger">@{{dash_new.emailValidationMesg_email}}</small></p>
            </span>
            <span id="passwordSuccess" ng-show="dash_new.emailSuccessMsg_email">
                    <p><small class="text-success">@{{dash_new.emailSuccessMsg_email}}</small></p>
            </span>
            <span id="emailerror"><strong>Please check your email for verification. You can access only for 7 days  </strong></span>
        </div>
        <div class="email-resend">
           <button type="button" class="btn btn-primary pull-right" ng-click="dash_new.resend_email()">Resend Email</button>
        </div>
      </div>
    @endif

    <div id="user_incomplete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
     <div class="modal-dialog">
        <div class="modal-content">
           <!-- Modal Header -->
           <div class="modal-header text-center">
              <h1><small><strong>Profile Information</strong></small></h1>
           </div>
           <!-- END Modal Header -->
           <!-- Modal Body -->
           <div class="modal-body">
              <form  id="formAbout" class="form-horizontal form-bordered form-control-borderless" name="formAbout" >
                 <div class="form-group" >
                    <div class="col-xs-12 col-md-6">
                       <div class="input-group" ng-init="dash_new.about_data.first_name ='{{$first_name}}'">
                          <span class="input-group-addon"><i class="gi gi-user"></i></span>
                          <input type="text"
                                 name="first_name"
                                 ng-model="dash_new.about_data.first_name"
                                 ng-minlength="3"
                                 maxlength="30"
                                 class="form-control input-lg"
                                 placeholder="Firstname"
                                 required
                                  >
                       </div>
                       <div class="input-group">
                          <span class="input-group-addon"><i class=""></i></span>
                          <span ng-show="formAbout.first_name.$invalid && formAbout.first_name.$touched">
                          <span class="help-block animation-slideDown" ><span class="text-danger">FirstName is Required</span></span>
                          </span>
                       </div>
                    </div>
                    <div class="col-xs-12 col-md-6" ng-init="dash_new.about_data.last_name ='{{$last_name}}'">
                       <div class="input-group">
                          <span class="input-group-addon"></span>
                          <input type="text"
                              name="last_name"
                              maxlength="30"
                              ng-model="dash_new.about_data.last_name"
                              class="form-control input-lg"
                              placeholder="Lastname"
                              required>
                       </div>
                       <div class="input-group">
                          <span class="input-group-addon"><i class=""></i></span>
                          <span ng-show="formAbout.last_name.$invalid && formAbout.last_name.$touched">
                          <span class="help-block animation-slideDown" ><span class="text-danger">LastName is Required</span></span>
                          </span>
                       </div>
                    </div>
                    <div class="form-group">
                       <div class="col-xs-12">
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-gift"></i></span>
                              <input type="text" id="birth_date" class="form-control input-datepicker" value="<?php echo $birth_date; ?>" data-date-format="MM dd, yyyy" placeholder="MM DD, YYYY">
                          </div>

                          <div class="input-group">
                              <span class="input-group-addon"><i class=""></i></span>
                                <div ng-show="dash_new.ageValidation" class="help-block animation-slideDown"><span class="text-danger">You must be at least 13 years of age to register</span></div>
                              </span>
                          </div>
                       </div>
                    </div>
                    <div class="form-group">
                       <div class="col-xs-12" ng-init="dash_new.about_data.gender ='{{$gender}}'">
                          <div class="input-group">
                             <span class="input-group-addon"><i class="fa fa-transgender"></i></span>
                             <label class="radio-inline" for="male" ng-init="dash_new.about_data.gender ='male'">
                             <input type="radio" id="male"  ng-model="dash_new.about_data.gender"  value="male" name="gender"> Male
                             </label>
                             <label class="radio-inline" for="female" >
                             <input type="radio" id="female" ng-model="dash_new.about_data.gender"   value="female" name="gender"> Female
                             </label>
                          </div>
                          <div class="col-xs-12 col-md-6">
                             <div class="input-group">
                                <span class="input-group-addon"></span>
                                <span  class="text-danger text-center" ng-show="formAbout.gender.$invalid && formAbout.gender.$touched">Select any gender</span>
                             </div>
                          </div>
                       </div>
                    </div>
                    <div class="form-group">
                       <div class="col-xs-12 col-md-6">
                          <div class="input-group">
                             <span class="input-group-addon"><i class="fa fa-male"></i></span>
                             <input type="text" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" number-validation="number-validation"  maxlength="5" name="height_in_feet"  ng-model="dash_new.about_data.height_in_feet" class="form-control input-lg" required>
                             <span class="input-group-btn">
                            <span class="input-group-addon btn-default">Feet</span>
                             </span>
                          </div>
                       </div>
                                <div class="col-xs-12 col-md-6">
                                   <div class="input-group">
                                     <div class="col-md-6">
                                       <div class="input-group ">
                                            <input type="text"  ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" number-validation="number-validation"   maxlength="6" name="height_in_inch" ng-model="dash_new.about_data.height_in_inch"   class="form-control input-lg" required>
                                       </div>
                                     </div>
                                     <span class="input-group-btn">
                                           <span class="input-group-addon btn-default">Inch</span>
                                     </span>
                                   </div>
                                </div>
                          <div class="col-xs-12 col-md-6">
                              <div class="input-group">
                                 <span class="input-group-addon"></span>
                                 <span ng-show="formAbout.height_in_feet.$invalid && formAbout.height_in_feet.$touched">
                                 <span class="help-block animation-slideDown" ><span class="text-danger">Enter valid Height</span></span>
                              </div>
                          </div>
                          <div class="col-xs-12 col-md-6">
                             <div class="input-group">
                                <span class="input-group-addon"></span>
                                <span ng-show="formAbout.height_in_inch.$invalid && formAbout.height_in_inch.$touched">
                                <span class="help-block animation-slideDown" ><span class="text-danger">Enter valid Height</span></span>
                             </div>
                          </div>
                    </div>
                    <div class="form-group">
                       <div class="col-xs-12">
                          <div class="input-group">
                             <span  class="input-group-addon"><i class="wellness-icon-weight"></i></span>
                             <input type="text" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" number-validation="number-validation" maxlength="6" name="baseline_weight"  ng-model="dash_new.about_data.baseline_weight" class="form-control input-lg" placeholder="Weight" required>
                             <span class="input-group-btn">
                             <span class="input-group-addon btn-default">lbs</span>
                             </span>
                          </div>
                          <div class="col-xs-12 col-md-6">
                             <div class="input-group">
                                <span class="input-group-addon"></span>
                                <span ng-show="formAbout.baseline_weight.$invalid && formAbout.baseline_weight.$touched">
                                <span class="help-block animation-slideDown" ><span class="text-danger">Weight is Required</span></span>
                             </div>
                          </div>
                       </div>
                     </div>
                     <div class="form-group form-actions">
                         <div class="col-xs-8 col-md-8">
                            <span class="text-danger" ng-hide="formAbout.$valid" >&nbsp;&nbsp; </span>
                         </div>
                         <div class="col-xs-3 col-md-4 text-right">
                           <button type="button" class="btn btn-sm btn-success"  ng-disabled="formAbout.$invalid"  ng-click="dash_new.updateUserInfo()"><i class="fa fa-plus"></i> Save</button>
                         </div>
                      </div>
                    </form>
                  </div>
                 <!-- END Modal Body -->
              </div>
           </div>
        </div>
    </div>

    <div id="user_verify" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
       <div class="modal-dialog">
           <div class="modal-content">
              <!-- Modal Header -->
             <div class="modal-header  has-warning text-center">
                  <div class="alert alert-warning">
                   <strong>Warning!</strong>
                  </div>
                  <div class="modal-body" style="padding:10px">
                      <span id="passwordError" ng-hide="dash_new.emailValidation_email">
                              <p><large class="text-danger">@{{dash_new.emailValidationMesg_email}} </large></p>
                      </span>
                      <span id="passwordSuccess" ng-show="dash_new.emailSuccessMsg_email">
                              <p><large class="text-success">@{{dash_new.emailSuccessMsg_email}}</large></p>
                      </span>

                      <p><strong>Please, You must verify your email address before you can continue.Please check you email for the verify account.</strong></p>
                  </div>
                  <div>
                    <a class="btn btn-primary btn-sm" href="{{URL::to('/auth0/logout')}}">Logout</a>
                    &nbsp;
                    <button type="button" class="btn btn-primary btn-sm" ng-click="dash_new.resend_email()">Resend Email</button>

                  </div>
              </div>
           </div>
       </div>
    </div>
    <!-- end: email varification and profile pages -->

    <!-- This is for Calorie Log Summary block -->
    <div class="block calorie-log-summary">
        <div class="row">
            <div class="col-xl-12">
                <h1 class="mainheading-form"><strong>CALORIE LOG SUMMARY</strong></h1>
            </div>
        </div>
        <div class="row">
            <!-- This is First block which display horizontally 1280 screen and vertically less than 1280 -->
            <div class="col-md-12 col-lg-12 col-xl-4" ng-init="dash_new.getCaloriesLog();">

              <div class="row"><div class="col-xl-12"><h2 class="mainheading-form-subhead">DAILY</h2></div> </div>
                <!-- Block with Options -->
                <div class="block healthbox">
                    <!-- Hading for First Row -->
                    <div class="block-title block-heading-health calorie-log-heading-first fa-iconcolor">
                        <div class="row headrow-samecolumnheight">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 health-sub-heading">
                                    <a href="javascript:void(0);" ng-click="dash_new.dateNextPrevious('prev')">
                                        <span id="cal_summary_date_prev"  class="fa fa-chevron-left"></span>
                                    </a>
                                    <!-- <span class="fa fa-calendar"></span> -->
                                    <input-datepicker id="cal_summary_datepicker" disabled="disabled" format="MM dd, yyyy" date="dash_new.daily_cal_date"></input-datepicker>
                                    <a href="javascript:void(0);" ng-click="dash_new.dateNextPrevious('next')">
                                        <span id="cal_summary_date_next" class="fa fa-chevron-right"></span>&nbsp;&nbsp;
                                    </a>

                             </div>
                        </div>
                    </div>
                    <!-- END Block Header -->
                    <div class="block-content block-heading-health calorie-log-heading calorielog-row-minus-20">
                        <div class="row headrow-samecolumnheight">
                            <div class="col-xl-5 col-lg-6 col-md-6 col-sm-6 col-xs-5 health-sub-heading">
                                <h3></h3>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-5 health-sub-heading">
                                <h3>Actual</h3>
                            </div>
                            <div class="col-xl-3 col-lg-2 col-md-2 col-sm-2 col-xs-2 health-sub-heading">
                                <h3>Goal</h3>
                            </div>
                        </div>
                    </div>
                    <!-- END Block Header -->
                    <!-- Body Part of The Calorie log summary -->
                    <div class="block-content health-contain health-body-row calorielog-row-minus-20">

                        <!-- This First row for Contain -->
                        <div class="row headrow-samecolumnheight">
                            <div class="col-xl-5 col-lg-6 col-md-6 col-sm-6 col-xs-5 health-sub-contain">
                                <!-- This is for Meter column portion -->
                                <h5><strong>Calories Consumed</strong></h5>

                                <div class="guagemetercontain">
                                <ul class="list-inline">
                                    <li>
                                        <!-- <canvas id="gauge1" class="gaugemeter"></canvas> -->
                                        <!-- <div id="calories-gauge123" meter-value="dash_new.gauge_meter_food_value" class="calorielog-gauge1 guagemeterdemo" gauge-meter></div> -->
                                        <div id="calories-gauge" class="calorielog-gauge1 guagemeterdemo"></div>
                                    </li>
                                    <li>
                                        <h5 class="textcolor"><strong><span class="textfont">@{{dash_new.calories_consume_percent_food | number:0 }} %</span> OF GOAL</strong></h5>
                                    </li>
                                </ul>
                                </div>

                                <div class="lastline left calorielog-left calorieline"></div>
                                <!-- End of Meter Portion -->
                            </div>

                            <!-- Column for button portion last Measured -->

                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-5 health-sub-contain">

                                <h5><strong>Log Food-: @{{dash_new.calories.food }}</strong></h5>

                                <div class="btn-group buttondropdown">
                                     <a href="{{URL::to('/food')}}" class="buttonwidth btn btn-alt btn-info ">Log Food <span class="caret tranformcaret"></span></a>
                                    <ul class="dropdown-menu dropdown-custom text-left">
                                        <li class="dropdown-header">Header</li>
                                        <li>
                                            <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                            <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                        </li>
                                    </ul>
                                </div>
                               <div class="lastline calorieline"></div>
                            </div>
                            <!-- End of Column for button portion last Measured -->
                            <!-- Goal 3 Columns -->
                            <div class="col-xl-3 col-lg-2 col-md-2 col-sm-2 col-xs-2 health-sub-contain borderrightnone ">
                                <h5><strong>@{{dash_new.calories.current_calories }}</strong></h5>
                                <div class="lastline calorielog-last"></div>
                            </div>
                            <!-- End of 3rd Column -->
                            <!-- This portion is only display for <768 screen  -->
                            <!-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 visible-xs subheading-minhead-meter">
                                <h3></h3>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 visible-xs subheading-minhead-chart">
                                <h3>Actual</h3>
                            </div> -->
                            <!-- End of portion is only display for <768 screen  -->
                        </div>
                        <!-- End of First Row Contain -->

                        <!-- This Second row for Contain -->
                        <div class="row headrow-samecolumnheight">
                            <!-- This is for Meter Column -->
                            <div class="col-xl-5 col-lg-6 col-md-6 col-sm-6 col-xs-5 health-sub-contain">
                                <h5><strong>Calories Burned</strong></h5>

                                <div class="guagemetercontain">
                                <ul class="list-inline">
                                    <li><div id="calories-burned"  class="calorielog-gauge2 guagemeterdemo"></div></li>
                                    <li>
                                        <h5 class="textcolor"><strong><span class="textfont">@{{dash_new.calories_consume_percent_exercise | number:0 }}% </span> OF GOAL</strong></h5>
                                    </li>
                                </ul>
                                </div>

                                <div class="lastline left calorielog-left calorieline"></div>
                            </div>
                            <!-- End of This is for Meter Column -->

                            <!-- This is for button column -->
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-5 health-sub-contain">
                                <h5><strong>Exercise-:  @{{dash_new.calories.exercise}}</strong></h5>
                                <div class="btn-group buttondropdown">
                                    <a href="{{URL::to('/exercise')}}"  class="buttonwidth btn btn-alt btn-info">Log Exercise <span class="caret tranformcaret"></span></a>
                                    <ul class="dropdown-menu dropdown-custom text-left">
                                        <li class="dropdown-header">Header</li>
                                        <li>
                                            <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                            <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                        </li>
                                    </ul>
                                </div>
                                <p class="ormargin"><span class="colorblue">or</span></p>
                                <div class="btn-group buttondropdown">
                                    <a href="javascript:void(0)" data-toggle="dropdown" class="buttonwidth btn btn-alt btn-info dropdown-toggle butttonfont">Sync with Device <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-custom text-left">
                                        <li class="dropdown-header">Header</li>
                                        <li>
                                            <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                            <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="lastline calorieline"></div>

                            </div>
                            <!-- End of Button Column -->

                            <!-- Goal Column -->
                            <div class="col-xl-3 col-lg-2 col-md-2 col-sm-2 col-xs-2 health-sub-contain">
                                <h5><strong>-</strong></h5>

                                <div class="lastline calorielog-last"></div>
                            </div>
                            <!-- End of Goal Column -->

                            <!-- This portion is only display for <768 screen  -->
                            <!-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 visible-xs subheading-minhead-meter">
                                <h3></h3>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 visible-xs subheading-minhead-chart">
                                <h3>Actual</h3>
                            </div> -->
                            <!-- End of portion is only display for <768 screen  -->

                        </div>
                        <!-- End of Second Row Contain -->

                        <!-- This Third row for Contain -->
                        <div class="row headrow-samecolumnheight">
                            <!-- Meter Column -->
                            <div class="col-xl-5 col-lg-6 col-md-6 col-sm-6 col-xs-5 health-sub-contain">
                                <h5><strong>Calories Total</strong></h5>

                                <div class="guagemetercontain lastguage">
                                <ul class="list-inline">
                                    <li>
                                        <div  id="calories_total" class="calorielog-gauge3 guagemeterdemo"></div>
                                    </li>
                                    <li>
                                        <h5 class="textcolor"><strong><span class="textfont">@{{dash_new.calories_consume_percent_calories | number:0 }} %</span> OF GOAL</strong></h5>
                                    </li>
                                </ul>
                                </div>
                            </div>
                            <!-- End of Meter Column -->
                            <!-- Button or Number Column -->
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-5 health-sub-contain">
                                <h5><strong>@{{dash_new.actual}}</strong></h5>

                            </div>
                            <!-- End of Button or number column -->

                            <!-- Goal Nubmer column -->
                            <div class="col-xl-3 col-lg-2 col-md-2 col-sm-2 col-xs-2 health-sub-contain">
                                <h5><strong>@{{dash_new.calories.current_calories}}</strong></h5>

                            </div>
                            <!-- End of Goal number column -->

                        </div>
                        <!-- End of Third Row Contain -->

                    </div>
                    <!-- End of Body of Health Goal -->
                    <!-- END Block with Options Content -->
                </div>
                <!-- END Block with Options -->
                <!-- This is for WaterCup block -->
                <div class="block healthbox watercupboxheight">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 health-sub-contain">
                                    <a href="javascript:void(0)" ng-click="dash_new.openLogWaterForm()" ng-if="dash_new.add_log_water_display">Add water</a>
                                    <h5 class="watercup-cup"><strong>@{{dash_new.total_water_to_drink | number:0 }} Cups of Water to Drink </strong>&nbsp;&nbsp;&nbsp;
                                        <small >
                                            <span ng-bind-html="dash_new.cupDisplay"></span>
                                        </small>
                                    </h5>
                        </div>
                    </div>
                </div>
             </div>
             <div class="col-md-12 col-lg-12 col-xl-8">
                <!-- Block with Options -->
                <div class="row"><div class="col-xl-12"><h2 class="mainheading-form-subhead">WEEKLY/MONTHLY</h2></div> </div>
                <div class="block healthbox calender-right">
                    <div class="block-content health-contain health-body-row calorielog-row-minus-20-table">
                            <!-- This Third row for Contain -->
                            <div class="row headrow-samecolumnheight">
                                <!-- Meter Column -->
                                <div class="col-xl-10 col-lg-9 col-md-9 col-sm-9 hidden-xs health-sub-contain forcalender">
                                    <div id="calendar">
                                    </div>
                                </div>
                                <!-- End of Meter Column -->

                                <!-- Goal Nubmer column -->



                                <div class="col-xs-6 visible-xs health-sub-contain weekavg left-weekavg">
                                    <h5><span class="fa fa-chevron-left"></span>&nbsp;<strong>January Month Average</strong>&nbsp;<span class="fa fa-chevron-right"></span></h5>


                                    <table class="table avgpart">
                                        <tr>
                                            <td>
                                                <div class="tabletd-div">
                                                    <p>First Week Average</p>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tabletd-div">
                                                    <p>Second Week Average</p>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tabletd-div">
                                                    <p>Third Week Average</p>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tabletd-div">
                                                    <p>Fourth Week Average</p>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tabletd-div">
                                                    <p>Fifth Week Average</p>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tabletd-div">
                                                    <p>Sixth Week Average</p>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="col-xl-2 col-lg-3 col-md-3 col-sm-3 col-xs-6 health-sub-contain weekavg right-weekavg">
                                    <h5><strong>Week Average</strong></h5>


                                    <table class="table avgpart" id="week_average_calorie">
                                        <tr>
                                            <td>
                                                <div class="tabletd-div calendar-week-cell week_1">

                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tabletd-div calendar-week-cell week_2">

                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tabletd-div calendar-week-cell week_3">

                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tabletd-div calendar-week-cell week_4">

                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tabletd-div calendar-week-cell week_5">

                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tabletd-div calendar-week-cell week_6">

                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!-- End of Goal number column -->
                            </div>
                    </div>
                    <div class="block-content block-heading-health calorie-log-heading-footer calorilog-footer-hieght calorielog-row-minus-20">
                            <div class="row headrow-samecolumnheight">
                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-xs-6 health-sub-contain monthavgpart text-right">
                                    <div class="tabletd-div">
                                        <h5><strong>Month Average</strong></h5>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-6 health-sub-contain lastrow-monthavg forcalorilog-footer-avg">
                                    <div class="tabletd-div month-average-calorie">

                                    </div>
                                </div>
                            </div>
                    </div>
                <!-- END Block with Options Content -->
                </div>
                <!-- END Block with Options -->
            </div>
            <!-- End of second Block Weeklly and Monthlly Calendar block  -->
        </div>
    </div>
    <!-- End of Calorie Log Summary block -->

    <!-- This is for Exercise log Summary Blcok -->
    <div class="block exerciselogbackground exercise-log-summary">
        <div class="row">
            <div class="col-xl-12">
                <h1 class="mainheading-form"><strong>EXERCISE LOG SUMMARY</strong></h1>
                <div class="exe-logbutton">
                        <div class="btn-group buttondropdown">
                        <a href="javascript:void(0)" data-toggle="dropdown" class="buttonwidth exe-button btn btn-alt btn-info dropdown-toggle butttonfont">View Exercise Details <span class="caret tranformcaret"></span></a>
                            <ul class="dropdown-menu dropdown-custom text-left">
                                <li class="dropdown-header">Header</li>
                                <li>
                                    <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                    <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                </li>
                            </ul>
                        </div>
                </div>
        </div>
        </div>
        <div class="row">
            <!-- This is First block which display horizontally 1280 screen and vertically less than 1280 -->
            <div class="col-md-12 col-lg-12 col-xl-4">
                <div class="row"><div class="col-xl-12"><h2 class="mainheading-form-subhead">DAILY</h2></div> </div>
                <!-- Block with Options -->
                <div class="block healthbox heightExe-daily-log">
                    <!-- Hading for First Row -->
                    <div class="block-title block-heading-health calorie-log-heading-first fa-iconcolor">
                        <div class="row headrow-samecolumnheight">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 health-sub-heading">

                                    <span class="fa fa-chevron-left" ></span>
                                    <input type="text" id="example-datepicker" name="example-datepicker4" class="form-control input-datepicker-first firstblock-date" placeholder="day, mm-dd-yyyy">
                                    <span class="fa fa-chevron-right"></span>&nbsp;&nbsp;
                                    <span class="fa fa-calendar"></span>
                             </div>
                        </div>
                    </div>
                    <!-- END Block Header -->

                    <div class="block-content block-heading-health calorie-log-heading calorielog-row-minus-20">
                        <div class="row headrow-samecolumnheight">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-3 health-sub-heading">
                                <h3 class="fristhead">Legs</h3>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-3 health-sub-heading">
                                <h3>Sets</h3>
                            </div>
                            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-3 health-sub-heading">
                                <h3>Weight</h3>
                            </div>
                            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-3 health-sub-heading">
                                <h3>Reps</h3>
                            </div>
                        </div>
                    </div>
                    <!-- END Block Header -->

                    <div class="block-content block-heading-health calorie-log-heading exerciselog-secondhead calorielog-row-minus-20">
                        <div class="row headrow-samecolumnheight">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-3 health-sub-heading">
                                <h3 class="fristhead">Quadriceps</h3>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-3 health-sub-heading">
                                <h3></h3>
                            </div>
                            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-3 health-sub-heading">
                                <h3></h3>
                            </div>
                            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-3 health-sub-heading">
                                <h3>12-15 Reps</h3>
                            </div>
                        </div>
                    </div>

                    <!-- Body Part of The Exercise log summary -->
                    <div class="block-content health-contain health-body-row calorielog-row-minus-20">

                        <!-- This First row for Contain -->
                        <div class="row headrow-samecolumnheight">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-3 health-sub-contain">
                                <!-- This is for Leg Extendsions portion -->

                                <ul class="list-group genemedicsul">
                                    <li class="list-group-item">Leg Extensions</li>
                                    <li class="list-group-item"></li>
                                    <li class="list-group-item"></li>
                                </ul>
                                <!-- End of Leg Extendsions -->
                            </div>

                            <!-- Column for button portion last Measured -->
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-3 health-sub-contain">

                                <ul class="list-group genemedicsul">
                                    <li class="list-group-item"><span class="badge noback hidden-xs"><a href="#">Edit</a></span>1</li>
                                    <li class="list-group-item"><span class="badge noback hidden-xs"><a href="#">Edit</a></span>2</li>
                                    <li class="list-group-item"><span class="badge noback hidden-xs"><a href="#">Edit</a></span>3</li>
                                </ul>
                            </div>
                            <!-- End of Column for button portion last Measured -->
                            <!-- Goal 3 Columns -->
                            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-3 health-sub-contain">

                                <ul class="list-group genemedicsul">
                                    <li class="list-group-item">10 lbs.</li>
                                    <li class="list-group-item">20 lbs.</li>
                                    <li class="list-group-item">40 lbs.</li>
                                </ul>
                            </div>
                            <!-- End of 3rd Column -->

                            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-3 health-sub-contain borderrightnone ">

                                <ul class="list-group genemedicsul">
                                    <li class="list-group-item">15</li>
                                    <li class="list-group-item">12</li>
                                    <li class="list-group-item">15</li>
                                </ul>
                            </div>

                            <!-- This portion is only display for <768 screen  -->
                            <!-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 visible-xs subheading-minhead-meter">
                                <h3></h3>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 visible-xs subheading-minhead-chart">
                                <h3>Actual</h3>
                            </div> -->
                            <!-- End of portion is only display for <768 screen  -->
                        </div>
                        <!-- End of First Row Contain -->

                        <!-- This First row for Contain -->
                        <div class="row headrow-samecolumnheight">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-3 health-sub-contain">
                                <!-- This is for Leg Extendsions portion -->

                                <ul class="list-group genemedicsul">
                                    <li class="list-group-item">Leg Press</li>
                                    <li class="list-group-item"></li>
                                    <li class="list-group-item"></li>
                                </ul>
                                <!-- End of Leg Extendsions -->
                            </div>

                            <!-- Column for button portion last Measured -->
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-3 health-sub-contain">

                                <ul class="list-group genemedicsul">
                                    <li class="list-group-item"><span class="badge noback hidden-xs"><a href="#">Edit</a></span>1</li>
                                    <li class="list-group-item"><span class="badge noback hidden-xs"><a href="#">Edit</a></span>2</li>
                                    <li class="list-group-item"><span class="badge noback hidden-xs"><a href="#">Edit</a></span>3</li>
                                </ul>
                            </div>
                            <!-- End of Column for button portion last Measured -->
                            <!-- Goal 3 Columns -->
                            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-3 health-sub-contain">

                                <ul class="list-group genemedicsul">
                                    <li class="list-group-item">10 lbs.</li>
                                    <li class="list-group-item">20 lbs.</li>
                                    <li class="list-group-item">40 lbs.</li>
                                </ul>
                            </div>
                            <!-- End of 3rd Column -->

                            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-3 health-sub-contain borderrightnone ">

                                <ul class="list-group genemedicsul">
                                    <li class="list-group-item">15</li>
                                    <li class="list-group-item">12</li>
                                    <li class="list-group-item">15</li>
                                </ul>
                            </div>

                            <!-- This portion is only display for <768 screen  -->
                            <!-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 visible-xs subheading-minhead-meter">
                                <h3></h3>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 visible-xs subheading-minhead-chart">
                                <h3>Actual</h3>
                            </div> -->
                            <!-- End of portion is only display for <768 screen  -->
                        </div>
                        <!-- End of First Row Contain -->

                        <!-- This First row for Contain -->
                        <div class="row headrow-samecolumnheight">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-3 health-sub-contain">
                                <!-- This is for Leg Extendsions portion -->

                                <ul class="list-group genemedicsul">
                                    <li class="list-group-item">Hack Squats</li>
                                </ul>
                                <!-- End of Leg Extendsions -->
                            </div>

                            <!-- Column for button portion last Measured -->
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-3 health-sub-contain">

                                <ul class="list-group genemedicsul">
                                    <li class="list-group-item">
                                        <div class="btn-group buttondropdown">
                                            <a href="{{URL::to('/exercise')}}"  class="buttonwidth btn btn-alt btn-info">Log Exercise <span class="caret tranformcaret"></span></a>

                                                <ul class="dropdown-menu dropdown-custom text-left">
                                                    <li class="dropdown-header">Header</li>
                                                    <li>
                                                        <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                                        <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                                    </li>
                                                    <li class="divider"></li>
                                                    <li>
                                                        <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                                    </li>
                                                </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <!-- End of Column for button portion last Measured -->
                            <!-- Goal 3 Columns -->
                            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-3 health-sub-contain">

                                <ul class="list-group genemedicsul">
                                    <li class="list-group-item"></li>
                                </ul>
                            </div>
                            <!-- End of 3rd Column -->

                            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-3 health-sub-contain borderrightnone ">

                                <ul class="list-group genemedicsul">
                                    <li class="list-group-item"></li>
                                </ul>
                            </div>

                            <!-- This portion is only display for <768 screen  -->
                            <!-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 visible-xs subheading-minhead-meter">
                                <h3></h3>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 visible-xs subheading-minhead-chart">
                                <h3>Actual</h3>
                            </div> -->
                            <!-- End of portion is only display for <768 screen  -->
                        </div>
                        <!-- End of First Row Contain -->

                        <!-- gray background second sub-header -->
                        <div class="block-content block-heading-health calorie-log-heading exerciselog-secondhead exerciselog-secondhead-second">
                            <div class="row headrow-samecolumnheight">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-3 health-sub-heading">
                                    <h3 class="fristhead">Hamstrings</h3>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-3 health-sub-heading">
                                    <h3>3 Sets</h3>
                                </div>
                                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-3 health-sub-heading">
                                    <h3></h3>
                                </div>
                                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-3 health-sub-heading">
                                    <h3>12-15 Reps</h3>
                                </div>
                            </div>
                        </div>
                        <!-- End of gray background second sub-header -->

                        <div class="row headrow-samecolumnheight">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-3 health-sub-contain">
                                <!-- This is for Leg Extendsions portion -->

                                <ul class="list-group genemedicsul">
                                    <li class="list-group-item">Let Curis - Lying</li>
                                    <li class="list-group-item">Let Curis - Seated</li>
                                </ul>
                                <!-- End of Leg Extendsions -->
                            </div>

                            <!-- Column for button portion last Measured -->
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-3 health-sub-contain">

                                <ul class="list-group genemedicsul">
                                    <li class="list-group-item">
                                        <div class="btn-group buttondropdown">
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="buttonwidth btn btn-alt btn-info dropdown-toggle butttonfont">Log Sets <span class="caret tranformcaret"></span></a>
                                                <ul class="dropdown-menu dropdown-custom text-left">
                                                    <li class="dropdown-header">Header</li>
                                                    <li>
                                                        <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                                        <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                                    </li>
                                                    <li class="divider"></li>
                                                    <li>
                                                        <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                                    </li>
                                                </ul>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="btn-group buttondropdown">
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="buttonwidth btn btn-alt btn-info dropdown-toggle butttonfont">Log Sets <span class="caret tranformcaret"></span></a>
                                                <ul class="dropdown-menu dropdown-custom text-left">
                                                    <li class="dropdown-header">Header</li>
                                                    <li>
                                                        <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                                        <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                                    </li>
                                                    <li class="divider"></li>
                                                    <li>
                                                        <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                                    </li>
                                                </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <!-- End of Column for button portion last Measured -->
                            <!-- Goal 3 Columns -->
                            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-3 health-sub-contain">

                                <ul class="list-group genemedicsul">
                                    <li class="list-group-item"></li>
                                    <li class="list-group-item"></li>

                                </ul>
                            </div>
                            <!-- End of 3rd Column -->

                            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-3 health-sub-contain borderrightnone ">

                                <ul class="list-group genemedicsul">
                                    <li class="list-group-item"></li>
                                    <li class="list-group-item"></li>
                                </ul>
                            </div>

                            <!-- This portion is only display for <768 screen  -->
                            <!-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 visible-xs subheading-minhead-meter">
                                <h3></h3>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 visible-xs subheading-minhead-chart">
                                <h3>Actual</h3>
                            </div> -->
                            <!-- End of portion is only display for <768 screen  -->
                        </div>

                        <!-- gray background second sub-header -->
                        <div class="block-content block-heading-health calorie-log-heading exerciselog-secondhead exerciselog-secondhead-second">
                            <div class="row headrow-samecolumnheight">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-3 health-sub-heading">
                                    <h3 class="fristhead">Calves</h3>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-3 health-sub-heading">
                                    <h3>3 Sets</h3>
                                </div>
                                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-3 health-sub-heading">
                                    <h3></h3>
                                </div>
                                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-3 health-sub-heading">
                                    <h3>12-15 Reps</h3>
                                </div>
                            </div>
                        </div>
                        <!-- End of gray background second sub-header -->

                        <div class="row headrow-samecolumnheight">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-3 health-sub-contain">
                                <!-- This is for Leg Extendsions portion -->

                                <ul class="list-group genemedicsul">
                                    <li class="list-group-item">Standing Calf Raises</li>
                                    <li class="list-group-item">Seated Calf Raises</li>
                                </ul>
                                <!-- End of Leg Extendsions -->
                            </div>

                            <!-- Column for button portion last Measured -->
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-3 health-sub-contain padd-bottom-exelog">

                                <ul class="list-group genemedicsul">
                                    <li class="list-group-item">
                                        <div class="btn-group buttondropdown">
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="buttonwidth btn btn-alt btn-info dropdown-toggle butttonfont">Log Sets <span class="caret tranformcaret"></span></a>
                                                <ul class="dropdown-menu dropdown-custom text-left">
                                                    <li class="dropdown-header">Header</li>
                                                    <li>
                                                        <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                                        <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                                    </li>
                                                    <li class="divider"></li>
                                                    <li>
                                                        <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                                    </li>
                                                </ul>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="btn-group buttondropdown">
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="buttonwidth btn btn-alt btn-info dropdown-toggle butttonfont">Log Sets <span class="caret tranformcaret"></span></a>
                                                <ul class="dropdown-menu dropdown-custom text-left">
                                                    <li class="dropdown-header">Header</li>
                                                    <li>
                                                        <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                                        <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                                    </li>
                                                    <li class="divider"></li>
                                                    <li>
                                                        <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                                    </li>
                                                </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <!-- End of Column for button portion last Measured -->
                            <!-- Goal 3 Columns -->
                            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-3 health-sub-contain">

                                <ul class="list-group genemedicsul">
                                    <li class="list-group-item"></li>
                                    <li class="list-group-item"></li>

                                </ul>
                            </div>
                            <!-- End of 3rd Column -->

                            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-3 health-sub-contain borderrightnone ">

                                <ul class="list-group genemedicsul">
                                    <li class="list-group-item"></li>
                                    <li class="list-group-item"></li>
                                </ul>
                            </div>

                            <!-- This portion is only display for <768 screen  -->
                            <!-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 visible-xs subheading-minhead-meter">
                                <h3></h3>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 visible-xs subheading-minhead-chart">
                                <h3>Actual</h3>
                            </div> -->
                            <!-- End of portion is only display for <768 screen  -->
                        </div>
                    </div>
                    <!-- End of Body of Exercise Log Summary -->
                    <!-- END Block with Options Content -->
                </div>
                <!-- END Block with Options -->
             </div>
             <div class="col-md-12 col-lg-12 col-xl-8">
                <!-- Block with Options -->
                <div class="row"><div class="col-xl-12"><h2 class="mainheading-form-subhead">WEEKLY/MONTHLY</h2></div> </div>
                <div class="block healthbox calender-right">
                    <div class="block-content health-contain health-body-row calorielog-row-minus-20-table">
                        <!-- This Third row for Contain -->
                        <div class="row headrow-samecolumnheight">
                            <!-- Meter Column -->
                            <div class="col-xl-10 col-lg-9 col-md-9 col-sm-9 hidden-xs health-sub-contain forcalender">
                                <div id="calendar-exerciselog">
                                </div>
                            </div>
                            <!-- End of Meter Column -->

                            <div class="col-xs-6 visible-xs health-sub-contain weekavg left-weekavg">
                                <h5><span class="fa fa-chevron-left"></span>&nbsp;<strong> January Month Average</strong>&nbsp; <span class="fa fa-chevron-right"></span></h5>


                                <table class="table avgpart">
                                    <tr>
                                        <td>
                                            <div class="tabletd-div">
                                                <p>First Week Average</p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="tabletd-div">
                                                <p>Second Week Average</p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="tabletd-div">
                                                <p>Third Week Average</p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="tabletd-div">
                                                <p>Fourth Week Average</p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="tabletd-div">
                                                <p>Fifth Week Average</p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="tabletd-div">
                                                <p>Sixth Week Average</p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <!-- Goal Nubmer column -->
                            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-3 col-xs-6 health-sub-contain weekavg right-weekavg">
                                <h5><strong>Workouts/Week</strong></h5>


                                <table class="table avgpart">
                                    <tr>
                                        <td>
                                            <div class="tabletd-div">
                                            <ul class="list-group genemedicsul">
                                                <li class="list-group-item text-success"><span class="badge noback">0 (0%)</span>SUCCESS</li>
                                                <li class="list-group-item text-warning"><span class="badge noback">0 (0%)</span>ADJUSTED</li>
                                                <li class="list-group-item text-danger"><span class="badge noback">1 (100%)</span>MISSED</li>
                                            </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="tabletd-div">
                                            <ul class="list-group genemedicsul">
                                                <li class="list-group-item text-success"><span class="badge noback">0 (0%)</span>SUCCESS</li>
                                                <li class="list-group-item text-warning"><span class="badge noback">0 (0%)</span>ADJUSTED</li>
                                                <li class="list-group-item text-danger"><span class="badge noback">1 (100%)</span>MISSED</li>
                                            </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="tabletd-div">
                                            <ul class="list-group genemedicsul">
                                                <li class="list-group-item text-success"><span class="badge noback">0 (0%)</span>SUCCESS</li>
                                                <li class="list-group-item text-warning"><span class="badge noback">0 (0%)</span>ADJUSTED</li>
                                                <li class="list-group-item text-danger"><span class="badge noback">1 (100%)</span>MISSED</li>
                                            </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="tabletd-div">
                                            <ul class="list-group genemedicsul">
                                                <li class="list-group-item text-success"><span class="badge noback">0 (0%)</span>SUCCESS</li>
                                                <li class="list-group-item text-warning"><span class="badge noback">0 (0%)</span>ADJUSTED</li>
                                                <li class="list-group-item text-danger"><span class="badge noback">1 (100%)</span>MISSED</li>
                                            </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="tabletd-div">
                                            <ul class="list-group genemedicsul">
                                                <li class="list-group-item text-success"><span class="badge noback">0 (0%)</span>SUCCESS</li>
                                                <li class="list-group-item text-warning"><span class="badge noback">0 (0%)</span>ADJUSTED</li>
                                                <li class="list-group-item text-danger"><span class="badge noback">1 (100%)</span>MISSED</li>
                                            </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="tabletd-div">
                                            <ul class="list-group genemedicsul">
                                                <li class="list-group-item text-success"><span class="badge noback">0 (0%)</span>SUCCESS</li>
                                                <li class="list-group-item text-warning"><span class="badge noback">0 (0%)</span>ADJUSTED</li>
                                                <li class="list-group-item text-danger"><span class="badge noback">1 (100%)</span>MISSED</li>
                                            </ul>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!-- End of Goal number column -->
                        </div>
                    </div>
                    <div class="block-content block-heading-health calorie-log-heading-footer calorielog-row-minus-20">
                        <div class="row headrow-samecolumnheight">
                            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-xs-6 health-sub-contain monthavgpart text-right">
                                <div class="tabletd-div">
                                <h5><strong>Month Average</strong></h5>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-6 health-sub-contain lastrow-monthavg">
                                <div class="tabletd-div">
                                <ul class="list-group genemedicsul">
                                    <li class="list-group-item text-success"><span class="badge noback">0 (0%)</span>SUCCESS</li>
                                    <li class="list-group-item text-warning"><span class="badge noback">0 (0%)</span>ADJUSTED</li>
                                    <li class="list-group-item text-danger"><span class="badge noback">1 (100%)</span>MISSED</li>
                                </ul>
                                </div>
                            </div>
                        </div>

                    <!-- END Block with Options -->
                    </div>
                    <!-- End of second Block Weeklly and Monthlly Calendar block  -->
                </div>
            </div>
        </div>
    </div>
    <!-- End of Exercise log Summary Blcok -->

    <!-- This is block for Health Goal -->
    <div class="block health-goal">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h1 class="mainheading-form"><strong>HEALTH GOALS</strong></h1>
            </div>
        </div>
        <!-- Block with Options Row -->
        <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-6">
                        <!-- Block with Options -->
                        <div class="block healthbox">
                            <!-- Hading for First Row -->
                            <div class="block-title block-heading-health">
                                <div class="row headrow-samecolumnheight">
                                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-6 health-goal health-sub-heading">
                                        <h3></h3>
                                    </div>
                                    <div class="col-xl-3 col-lg-2 col-md-2 col-sm-2 col-xs-6 health-goal health-sub-heading">
                                        <h3>Last Measured</h3>
                                    </div>
                                    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 hidden-xs health-sub-heading">
                                        <h3>Goal</h3>
                                    </div>
                                    <div class="col-xl-5 col-lg-6 col-md-6 col-sm-6 hidden-xs health-sub-heading">
                                        <h3>Trend</h3>
                                        <div class="btn-group buttondropdown">
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="buttonwidth btn btn-alt btn-info dropdown-toggle butttonfont">Monthly (default) <span class="caret tranformcaret"></span></a>
                                            <ul class="dropdown-menu dropdown-custom text-left">
                                                <li class="dropdown-header">Header</li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                                    <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END Block Header -->

                            <!-- Body Part of The Health Goals -->
                            <div class="block-content health-contain health-body-row">

                                <!-- This First row for Contain -->
                                <div class="row headrow-samecolumnheight">
                                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-6 health-goal health-sub-contain">
                                        <!-- This is for Meter column portion -->
                                        <h5><strong>Body Weight</strong></h5>

                                        <div class="guagemetercontain">
                                            <ul class="list-inline">
                                                <li>
                                                    <!-- <canvas id="gauge1" class="gaugemeter"></canvas> -->
                                                    <div class="gauge1 guagemeterdemo"></div>
                                                </li>
                                                <li>
                                                    <h5 class="textcolor"><strong><span class="textfont">75%</span> OF GOAL</strong></h5>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="lastline left"></div>
                                        <!-- End of Meter Portion -->
                                    </div>

                                    <!-- Column for button portion last Measured -->
                                    <div class="col-xl-3 col-lg-2 col-md-2 col-sm-2 col-xs-6 health-goal health-sub-contain">
                                        <h5><strong>250 lbs.</strong><small class="dataside">(1/25/16)</small></h5>
                                        <div class="btn-group buttondropdown">
                                            <a href="{{URL::to('/goals')}}" class="buttonwidth btn btn-alt btn-info dropdown-toggle butttonfont">Log Body Weight <span class="caret tranformcaret"></span></a>
                                            <ul class="dropdown-menu dropdown-custom text-left">
                                                <li class="dropdown-header">Header</li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                                    <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <p class="ormargin"><span class="colorblue">or</span></p>
                                        <div class="btn-group buttondropdown">
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="buttonwidth btn btn-alt btn-info dropdown-toggle butttonfont">Sync with Device <span class="caret"></span></a>
                                            <ul class="dropdown-menu dropdown-custom text-left">
                                                <li class="dropdown-header">Header</li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                                    <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="lastline"></div>

                                    </div>
                                    <!-- End of Column for button portion last Measured -->

                                    <!-- Goal 3 Columns -->
                                    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 hidden-xs health-sub-contain">
                                        <h5><strong>200 lbs.</strong></h5>

                                        <div class="lastline"></div>
                                    </div>
                                    <!-- This is for chart heading that only display in < 768 screen. -->
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 visible-xs subheading-minhead">
                                        <h3 class="displayline-side">Trend</h3>
                                        <div class="btn-group buttondropdown">
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="buttonwidth btn btn-alt btn-info dropdown-toggle butttonfont">Monthly (default) <span class="caret tranformcaret"></span></a>
                                            <ul class="dropdown-menu dropdown-custom text-left">
                                                <li class="dropdown-header">Header</li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                                    <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- This portion is only display in < 768 screen. -->


                                    <!-- Chart Portion -->
                                    <div class="col-xl-5 col-lg-6 col-md-6 col-sm-6 col-xs-12 health-sub-contain borderrightnone">
                                        <div id="chartdiv1"></div>

                                        <div class="dateformatecontain">
                                            <ul class="list-inline verticaltext datepickerno-320">
                                                <li class="verticaltext">
                                                    <div id="datepickerto" class="input-group date" data-date-format="mm-dd-yyyy">
                                                        <input class="form-control calanderinputcolor" type="text" readonly />
                                                        <span class="input-group-addon addonwidth"><i class="fa fa-calendar calandarcolor"></i></span>
                                                    </div>
                                                </li>
                                                <li class="verticaltext"><h5>To</h5></li>
                                                <li class="verticaltext">
                                                    <div id="datepickerfrom" class="input-group date" data-date-format="mm-dd-yyyy">
                                                        <input class="form-control calanderinputcolor" type="text" readonly />
                                                        <span class="input-group-addon addonwidth"><i class="fa fa-calendar calandarcolor"></i></span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="lastline right"></div>

                                    </div>
                                    <!-- End of Chart portion -->


                                    <!-- This portion is only display for <768 screen  -->
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 visible-xs subheading-minhead-meter">
                                        <h3></h3>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 visible-xs subheading-minhead-chart">
                                        <h3>Last Measured</h3>
                                    </div>
                                    <!-- End of portion is only display for <768 screen  -->



                                </div>
                                <!-- End of First Row Contain -->

                                <!-- This Second row for Contain -->
                                <div class="row headrow-samecolumnheight">
                                    <!-- This is for Meter Column -->
                                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-6 health-goal health-sub-contain">
                                        <h5><strong>Body Fat Percentage</strong></h5>

                                        <div class="guagemetercontain">
                                        <ul class="list-inline">
                                            <li><div class="gauge2 guagemeterdemo"></div></li>
                                            <li>
                                                <h5 class="textcolor"><strong><span class="textfont">0%</span> OF GOAL</strong></h5>
                                            </li>
                                        </ul>
                                        </div>

                                        <div class="lastline left"></div>
                                    </div>
                                    <!-- End of This is for Meter Column -->

                                    <!-- This is for button column -->
                                    <div class="col-xl-3 col-lg-2 col-md-2 col-sm-2 col-xs-6 health-goal health-sub-contain">
                                        <h5><strong>25%</strong><small class="dataside">(1/25/16)</small></h5>
                                        <div class="btn-group buttondropdown">
                                            <a href="{{URL::to('/goals')}}"  class="buttonwidth btn btn-alt btn-info dropdown-toggle butttonfont">Log Body Fat <span class="caret tranformcaret"></span></a>
                                            <ul class="dropdown-menu dropdown-custom text-left">
                                                <li class="dropdown-header">Header</li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                                    <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <p class="ormargin"><span class="colorblue">or</span></p>
                                        <div class="btn-group buttondropdown">
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="buttonwidth btn btn-alt btn-info dropdown-toggle butttonfont">Sync with Device <span class="caret"></span></a>
                                            <ul class="dropdown-menu dropdown-custom text-left">
                                                <li class="dropdown-header">Header</li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                                    <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="lastline"></div>

                                    </div>
                                    <!-- End of Button Column -->

                                    <!-- Goal Column -->
                                    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 hidden-xs health-sub-contain">
                                        <h5><strong>5%</strong></h5>

                                        <div class="lastline"></div>
                                    </div>
                                    <!-- End of Goal Column -->



                                    <!-- This is for chart header that only display in < 768 screen. -->
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 visible-xs subheading-minhead">
                                        <h3 class="displayline-side">Trend</h3>
                                        <div class="btn-group buttondropdown">
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="buttonwidth btn btn-alt btn-info dropdown-toggle butttonfont">Monthly (default) <span class="caret tranformcaret"></span></a>
                                            <ul class="dropdown-menu dropdown-custom text-left">
                                                <li class="dropdown-header">Header</li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                                    <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- This portion is only display in < 768 screen. -->

                                    <!-- Chart Column -->
                                    <div class="col-xl-5 col-lg-6 col-md-6 col-sm-6 col-xs-12 health-sub-contain borderrightnone">
                                        <div id="chartdiv2"></div>

                                        <div class="dateformatecontain">
                                            <ul class="list-inline verticaltext datepickerno-320">
                                                <li class="verticaltext">
                                                    <div id="datepickerto2" class="input-group date" data-date-format="mm-dd-yyyy">
                                                        <input class="form-control calanderinputcolor" type="text" readonly />
                                                        <span class="input-group-addon addonwidth"><i class="fa fa-calendar calandarcolor"></i></span>
                                                    </div>
                                                </li>
                                                <li class="verticaltext"><h5>To</h5></li>
                                                <li class="verticaltext">
                                                    <div id="datepickerfrom2" class="input-group date" data-date-format="mm-dd-yyyy">
                                                        <input class="form-control calanderinputcolor" type="text" readonly />
                                                        <span class="input-group-addon addonwidth"><i class="fa fa-calendar calandarcolor"></i></span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="lastline right"></div>
                                    </div>
                                    <!-- End of Chart Column -->


                                    <!-- This portion is only display for <768 screen  -->
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 visible-xs subheading-minhead-meter">
                                        <h3></h3>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 visible-xs subheading-minhead-chart">
                                        <h3>Last Measured</h3>
                                    </div>
                                    <!-- End of portion is only display for <768 screen  -->

                                </div>
                                <!-- End of Second Row Contain -->

                                <!-- This Third row for Contain -->
                                <div class="row headrow-samecolumnheight">
                                    <!-- Meter Column -->
                                    <div class="col-xl-3 col-md-3 col-sm-3 col-xs-6 health-goal health-sub-contain">
                                        <h5><strong>Body Measurements</strong></h5>

                                        <div class="guagemetercontain lastguage">
                                        <ul class="list-inline">
                                            <li>
                                                <div class="gauge3 guagemeterdemo"></div>
                                            </li>
                                            <li>
                                                <h5 class="textcolor"><strong><span class="textfont">100%</span> OF GOAL</strong></h5>
                                            </li>
                                        </ul>
                                        </div>

                                    </div>
                                    <!-- End of Meter Column -->

                                    <!-- Button or Number Column -->
                                    <div class="col-xl-3 col-lg-2 col-md-2 col-sm-2 col-xs-6 health-goal health-sub-contain">

                                        <div class="body-measure-labels">
                                            <ul class="list-unstyled">
                                                <li>Neck</li>
                                                <li>Arms</li>
                                                <li>Waits</li>
                                                <li>Hips</li>
                                                <li>Legs</li>
                                            </ul>
                                        </div>

                                        <div class="body-measurelast-labels">
                                            <ul class="list-unstyled">
                                                <li>16</li>
                                                <li>18</li>
                                                <li>32</li>
                                                <li>34</li>
                                                <li>28</li>
                                            </ul>
                                        </div>

                                    </div>
                                    <!-- End of Button or number column -->

                                    <!-- Goal Nubmer column -->
                                    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 hidden-xs health-sub-contain">

                                        <div class="body-measurelast-labels-goal">
                                            <ul class="list-unstyled">
                                                <li>17</li>
                                                <li>19</li>
                                                <li>30</li>
                                                <li>30</li>
                                                <li>30</li>
                                            </ul>
                                        </div>

                                    </div>
                                    <!-- End of Goal number column -->


                                    <!-- This is for chart header that only display in < 768 screen. -->
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 visible-xs subheading-minhead">
                                        <h3 class="displayline-side">Trend</h3>
                                        <div class="btn-group buttondropdown">
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="buttonwidth btn btn-alt btn-info dropdown-toggle butttonfont">Monthly (default) <span class="caret tranformcaret"></span></a>
                                            <ul class="dropdown-menu dropdown-custom text-left">
                                                <li class="dropdown-header">Header</li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                                    <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- This portion is only display in < 768 screen. -->



                                    <!-- Chart column -->
                                    <div class="col-xl-5 col-lg-6 col-md-6 col-sm-6 col-xs-12 health-sub-contain lastchartcontain">
                                        <div id="chartdiv3"></div>

                                        <div class="dateformatecontain">
                                            <ul class="list-inline verticaltext datepickerno-320">
                                                <li class="verticaltext">
                                                    <div id="datepickerto3" class="input-group date" data-date-format="mm-dd-yyyy">
                                                        <input class="form-control calanderinputcolor" type="text" readonly />
                                                        <span class="input-group-addon addonwidth"><i class="fa fa-calendar calandarcolor"></i></span>
                                                    </div>
                                                </li>
                                                <li class="verticaltext"><h5>To</h5></li>
                                                <li class="verticaltext">
                                                    <div id="datepickerfrom3" class="input-group date" data-date-format="mm-dd-yyyy">
                                                        <input class="form-control calanderinputcolor" type="text" readonly />
                                                        <span class="input-group-addon addonwidth"><i class="fa fa-calendar calandarcolor"></i></span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>
                                    <!-- End of chart column -->

                                </div>
                                <!-- End of Third Row Contain -->

                            </div>
                            <!-- End of Body of Health Goal -->
                        </div>
                        <!-- END Block with Options -->
                    </div>

                    <div class="col-md-12 col-lg-12 col-xl-6">
                        <!-- Block with Options -->
                        <div class="block healthbox">
                            <!-- Hading for First Row -->
                            <div class="block-title block-heading-health">
                                <div class="row headrow-samecolumnheight">
                                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-6 health-goal health-sub-heading">
                                        <h3></h3>
                                    </div>
                                    <div class="col-xl-3 col-lg-2 col-md-2 col-sm-2 col-xs-6 health-goal health-sub-heading">
                                        <h3>Last Measured</h3>
                                    </div>
                                    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 hidden-xs health-sub-heading">
                                        <h3>Goal</h3>
                                    </div>
                                    <div class="col-xl-5 col-lg-6 col-md-6 col-sm-6 hidden-xs health-sub-heading">
                                        <h3>Trend</h3>
                                        <div class="btn-group buttondropdown">
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="buttonwidth btn btn-alt btn-info dropdown-toggle butttonfont">Monthly (default) <span class="caret tranformcaret"></span></a>
                                            <ul class="dropdown-menu dropdown-custom text-left">
                                                <li class="dropdown-header">Header</li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                                    <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END Block Header -->

                            <!-- Body Part of The Health Goals -->
                            <div class="block-content health-contain health-body-row">

                                <!-- This First row for Contain -->
                                <div class="row headrow-samecolumnheight">
                                    <div class="colxl-3 col-lg-3 col-md-3 col-sm-3 col-xs-6 health-goal health-sub-contain">
                                        <!-- This is for Meter column portion -->
                                        <h5><strong>Blood Pressure</strong></h5>

                                        <div class="guagemetercontain">
                                        <ul class="list-inline">
                                            <li>
                                                <!-- <canvas id="gauge1" class="gaugemeter"></canvas> -->
                                                <div class="gauge2-1 guagemeterdemo"></div>
                                            </li>
                                            <li>
                                                <h5 class="textcolor"><strong><span class="textfont">75%</span> OF GOAL</strong></h5>
                                            </li>
                                        </ul>
                                        </div>

                                        <div class="lastline left"></div>
                                        <!-- End of Meter Portion -->
                                    </div>

                                    <!-- Column for button portion last Measured -->
                                    <div class="col-xl-3 col-lg-2 col-md-2 col-sm-2 col-xs-6 health-goal health-sub-contain">
                                        <h5><strong>126/84</strong><small class="dataside">(1/25/16)</small></h5>
                                        <div class="btn-group buttondropdown">
                                            <a href="{{URL::to('/goals')}}"  class="buttonwidth btn btn-alt btn-info dropdown-toggle butttonfont">Log BP <span class="caret tranformcaret"></span></a>
                                            <ul class="dropdown-menu dropdown-custom text-left">
                                                <li class="dropdown-header">Header</li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                                    <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <p class="ormargin"><span class="colorblue">or</span></p>
                                        <div class="btn-group buttondropdown">
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="buttonwidth btn btn-alt btn-info dropdown-toggle butttonfont">Sync with Device <span class="caret"></span></a>
                                            <ul class="dropdown-menu dropdown-custom text-left">
                                                <li class="dropdown-header">Header</li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                                    <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="lastline"></div>

                                    </div>
                                    <!-- End of Column for button portion last Measured -->

                                    <!-- Goal 3 Columns -->
                                    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 hidden-xs health-sub-contain goal124">
                                        <h5><strong>124/84</strong></h5>

                                        <div class="lastline"></div>
                                    </div>
                                    <!-- End of 3rd Column -->


                                    <!-- This is for chart header that only display in < 768 screen. -->
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 visible-xs subheading-minhead">
                                        <h3 class="displayline-side">Trend</h3>
                                        <div class="btn-group buttondropdown">
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="buttonwidth btn btn-alt btn-info dropdown-toggle butttonfont">Monthly (default) <span class="caret tranformcaret"></span></a>
                                            <ul class="dropdown-menu dropdown-custom text-left">
                                                <li class="dropdown-header">Header</li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                                    <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- This portion is only display in < 768 screen. -->



                                    <!-- Chart Portion -->
                                    <div class="col-xl-5 col-lg-6 col-md-6 col-sm-6 col-xs-12 health-sub-contain borderrightnone">
                                        <div id="chartdiv2-1"></div>

                                        <div class="dateformatecontain">
                                            <ul class="list-inline verticaltext datepickerno-320">
                                                <li class="verticaltext">
                                                    <div id="datepickerto2-1" class="input-group date" data-date-format="mm-dd-yyyy">
                                                        <input class="form-control calanderinputcolor" type="text" readonly />
                                                        <span class="input-group-addon addonwidth"><i class="fa fa-calendar calandarcolor"></i></span>
                                                    </div>
                                                </li>
                                                <li class="verticaltext"><h5>To</h5></li>
                                                <li class="verticaltext">
                                                    <div id="datepickerfrom2-1" class="input-group date" data-date-format="mm-dd-yyyy">
                                                        <input class="form-control calanderinputcolor" type="text" readonly />
                                                        <span class="input-group-addon addonwidth"><i class="fa fa-calendar calandarcolor"></i></span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="lastline right"></div>

                                    </div>
                                    <!-- End of Chart portion -->

                                    <!-- This portion is only display for <768 screen  -->
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 visible-xs subheading-minhead-meter">
                                        <h3></h3>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 visible-xs subheading-minhead-chart">
                                        <h3>Last Measured</h3>
                                    </div>
                                    <!-- End of portion is only display for <768 screen  -->


                                </div>
                                <!-- End of First Row Contain -->

                                <!-- This Second row for Contain -->
                                <div class="row headrow-samecolumnheight">
                                    <!-- This is for Meter Column -->
                                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-6 health-goal health-sub-contain">
                                        <h5><strong>Heart Rate</strong></h5>

                                        <div class="guagemetercontain">
                                        <ul class="list-inline">
                                            <li><div class="gauge2-2 guagemeterdemo"></div></li>
                                            <li>
                                                <h5 class="textcolor"><strong><span class="textfont">0%</span> OF GOAL</strong></h5>
                                            </li>
                                        </ul>
                                        </div>

                                        <div class="lastline left"></div>
                                    </div>
                                    <!-- End of This is for Meter Column -->

                                    <!-- This is for button column -->
                                    <div class="col-xl-3 col-lg-2 col-md-2 col-sm-2 col-xs-6 health-goal health-sub-contain">
                                        <h5><strong>72</strong><small class="dataside">(1/25/16)</small></h5>
                                        <div class="btn-group buttondropdown">
                                            <a href="{{URL::to('/goals')}}" class="buttonwidth btn btn-alt btn-info dropdown-toggle butttonfont">Log Body Fat <span class="caret tranformcaret"></span></a>
                                            <ul class="dropdown-menu dropdown-custom text-left">
                                                <li class="dropdown-header">Header</li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                                    <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <p class="ormargin"><span class="colorblue">or</span></p>
                                        <div class="btn-group buttondropdown">
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="buttonwidth btn btn-alt btn-info dropdown-toggle butttonfont">Sync with Device <span class="caret"></span></a>
                                            <ul class="dropdown-menu dropdown-custom text-left">
                                                <li class="dropdown-header">Header</li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                                    <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="lastline"></div>

                                    </div>
                                    <!-- End of Button Column -->

                                    <!-- Goal Column -->
                                    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 hidden-xs health-sub-contain">
                                        <h5><strong>72</strong></h5>

                                        <div class="lastline"></div>
                                    </div>
                                    <!-- End of Goal Column -->


                                    <!-- This is for chart header that only display in < 768 screen. -->
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 visible-xs subheading-minhead">
                                        <h3 class="displayline-side">Trend</h3>
                                        <div class="btn-group buttondropdown">
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="buttonwidth btn btn-alt btn-info dropdown-toggle butttonfont">Monthly (default) <span class="caret tranformcaret"></span></a>
                                            <ul class="dropdown-menu dropdown-custom text-left">
                                                <li class="dropdown-header">Header</li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                                    <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- This portion is only display in < 768 screen. -->


                                    <!-- Chart Column -->
                                    <div class="col-xl-5 col-lg-6 col-md-6 col-sm-6 col-xs-12 health-goal health-sub-contain borderrightnone">
                                        <div id="chartdiv2-2"></div>

                                        <div class="dateformatecontain">
                                            <ul class="list-inline verticaltext datepickerno-320">
                                                <li class="verticaltext">
                                                    <div id="datepickerto2-2" class="input-group date" data-date-format="mm-dd-yyyy">
                                                        <input class="form-control calanderinputcolor" type="text" readonly />
                                                        <span class="input-group-addon addonwidth"><i class="fa fa-calendar calandarcolor"></i></span>
                                                    </div>
                                                </li>
                                                <li class="verticaltext"><h5>To</h5></li>
                                                <li class="verticaltext">
                                                    <div id="datepickerfrom2-2" class="input-group date" data-date-format="mm-dd-yyyy">
                                                        <input class="form-control calanderinputcolor" type="text" readonly />
                                                        <span class="input-group-addon addonwidth"><i class="fa fa-calendar calandarcolor"></i></span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="lastline right"></div>
                                    </div>
                                    <!-- End of Chart Column -->

                                    <!-- This portion is only display for <768 screen  -->
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 visible-xs subheading-minhead-meter">
                                        <h3></h3>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 visible-xs subheading-minhead-chart">
                                        <h3>Last Measured</h3>
                                    </div>
                                    <!-- End of portion is only display for <768 screen  -->

                                </div>
                                <!-- End of Second Row Contain -->

                                <!-- This Third row for Contain -->
                                <div class="row headrow-samecolumnheight">
                                    <!-- Meter Column -->
                                    <div class="col-xl-3 col-md-3 col-sm-3 col-xs-6 health-goal health-sub-contain">
                                        <h5><strong>Sleep</strong></h5>

                                        <div class="guagemetercontain">
                                        <ul class="list-inline">
                                            <li>
                                                <div class="gauge2-3 guagemeterdemo"></div>
                                            </li>
                                            <li>
                                                <h5 class="textcolor"><strong><span class="textfont">0%</span> OF GOAL</strong></h5>
                                            </li>
                                        </ul>
                                        </div>

                                    </div>
                                    <!-- End of Meter Column -->

                                    <!-- Button or Number Column -->
                                    <div class="col-xl-3 col-lg-2 col-md-2 col-sm-2 col-xs-6 health-goal health-sub-contain">
                                      <!-- This is for button column -->
                                        <h5><strong>480 mins</strong><small class="dataside">(1/25/16)</small></h5>
                                        <div class="btn-group buttondropdown">
                                            <a href="{{URL::to('/goals')}}" class="buttonwidth btn btn-alt btn-info dropdown-toggle butttonfont">Log Sleep <span class="caret tranformcaret"></span></a>
                                            <ul class="dropdown-menu dropdown-custom text-left">
                                                <li class="dropdown-header">Header</li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                                    <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <p class="ormargin"><span class="colorblue">or</span></p>
                                        <div class="btn-group buttondropdown">
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="buttonwidth btn btn-alt btn-info dropdown-toggle butttonfont">Sync with Device <span class="caret"></span></a>
                                            <ul class="dropdown-menu dropdown-custom text-left">
                                                <li class="dropdown-header">Header</li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                                    <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>
                                    <!-- End of Button or number column -->

                                    <!-- Goal Nubmer column -->
                                    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 hidden-xs health-sub-contain">
                                        <h5><strong>480 mins</strong></h5>
                                    </div>
                                    <!-- End of Goal number column -->


                                    <!-- This is for chart header that only display in < 768 screen. -->
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 visible-xs subheading-minhead">
                                        <h3 class="displayline-side">Trend</h3>
                                        <div class="btn-group buttondropdown">
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="buttonwidth btn btn-alt btn-info dropdown-toggle butttonfont">Monthly (default) <span class="caret tranformcaret"></span></a>
                                            <ul class="dropdown-menu dropdown-custom text-left">
                                                <li class="dropdown-header">Header</li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-plus pull-right"></i>Action</a>
                                                    <a href="javascript:void(0)"><i class="fa fa-video-camera pull-right"></i>Another Action</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="javascript:void(0)"><i class="fa fa-pencil pull-right"></i>Edit</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- This portion is only display in < 768 screen. -->

                                    <!-- Chart column -->
                                    <div class="col-xl-5 col-lg-6 col-md-6 col-sm-6 col-xs-12 health-sub-contain lastchartcontain">
                                         <div id="chartdiv2-3"></div>

                                        <div class="dateformatecontain">
                                            <ul class="list-inline verticaltext datepickerno-320">
                                                <li class="verticaltext">
                                                    <div id="datepickerto2-3" class="input-group date" data-date-format="mm-dd-yyyy">
                                                        <input class="form-control calanderinputcolor" type="text" readonly />
                                                        <span class="input-group-addon addonwidth"><i class="fa fa-calendar calandarcolor"></i></span>
                                                    </div>
                                                </li>
                                                <li class="verticaltext"><h5>To</h5></li>
                                                <li class="verticaltext">
                                                    <div id="datepickerfrom2-3" class="input-group date" data-date-format="mm-dd-yyyy">
                                                        <input class="form-control calanderinputcolor" type="text" readonly />
                                                        <span class="input-group-addon addonwidth"><i class="fa fa-calendar calandarcolor"></i></span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>
                                    <!-- End of chart column -->

                                </div>
                                <!-- End of Third Row Contain -->

                            </div>
                            <!-- End of Body of Health Goal -->
                        </div>
                        <!-- END Block with Options -->
                    </div>
        </div>
    </div>
    <!-- End of First Block Row -->

    <div id="log_water_tmplt" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false" style="z-index:9999">
      @include('template.add_water_log')
    </div>
</div>

@endsection
@section('page_scripts')
@append
@section('inline_scripts')
<!-- This is For Daily Calendar Control -->
<script type="text/javascript">
    $(function() {
        var dt = $(".input-datepicker-first").datepicker({
           format: "D MM dd, yyyy"
        });

    });
</script>
<!-- End of Daily Calendar Control-->
<!-- This is for data formate For Health Goad -->
<script type="text/javascript">
        $(function () {
            $("#datepickerto,#datepickerto2,#datepickerto3,#datepickerto2-1,#datepickerto2-2,#datepickerto2-3").datepicker({
                autoclose: true,
                todayHighlight: true
            }).datepicker('update', new Date());;
        });


          $(function () {
            $("#datepickerfrom,#datepickerfrom2,#datepickerfrom3,#datepickerfrom2-1,#datepickerfrom2-2,#datepickerfrom2-3").datepicker({
                autoclose: true,
                todayHighlight: true
            }).datepicker('update', new Date());;
        });
</script>
<!-- This is for Calendar Control for Month -->

<script type="text/javascript">

    $('.fc-prev-button').find('span').removeClass('fc-icon fc-icon-left-single-arrow').addClass('fa fa-chevron-left fa-fw');
    $('.fc-next-button').find('span').removeClass('fc-icon fc-icon-right-single-arrow').addClass('fa fa-chevron-right fa-fw');

    /*$('.fc-next-button').append('&nbsp;<span class="fa fa-calendar fa-fw"></span>');*/
</script>

<!-- This is for Exersice log summary calendar -->
<script src="js/pages/compCalendar-exerciselog.js"></script>
<script>$(function(){ CompCalendar_exercise.init(); });</script>
<script type="text/javascript">

$(document).ready(function () {


    /**/
    caloryLogCalendar();

    $( ".fc-prev-button .fc-next-button" ).on( "click", function() {


        caloryLogCalendar();

    });

    function caloryLogCalendar(){

        $('#calendar').fullCalendar({

            header: {
              left:   'title',
              center: '',
              right:  'prev,next'
            },

            height: 760,

            events: function(start, end, timezone, callback) {
                    var d = $('#calendar').fullCalendar('getDate');
                    var today = new Date();
                    var curr_date = new Date(d);
                    var req_month = curr_date.getMonth() + 1;
                    var req_year = curr_date.getFullYear();

                    var param = '{{$user_id}}';

                    if(req_month && req_year){
                      param = param+'?req_month='+req_month+'&req_year='+req_year;
                    }


                    $.ajax({
                        url: 'api/v3/patient/monthly_calories/'+param,
                        dataType: 'json',
                        success: function(doc) {
                            var events = [];

                            var response = doc.response.date_wise;
                            for (var i=0; i<response.length; i++){

                                events[i] = {
                                    calory: response[i].total_consumed,
                                    start: response[i].date, // will be parsed
                                    diff_text_color: response[i].diff_text_color,
                                    diff_text: response[i].diff_text,
                                    diff: response[i].diff,
                                    water_intake : response[i].water_intake
                                };
                            }

                            callback(events);
                        }
                    });

                    $.get("api/v3/patient/average_calories/"+param, function(data, status){


                        $.each(data.response.week_wise_avg, function( index, value ) {

                            var html_data_weekly = "";
                            if(value.total_consumed_weekly_avg!=undefined){
                              html_data_weekly = "<p>Calorie Average</p>"
                                      +"<p><strong>"+ value.total_consumed_weekly_avg+"/day</strong></p>"
                                      +"<p class="+ value.weekly_diff_text_class_avg+">"
                                            + value.weekly_diff_text_avg+ "="
                                            + value.weekly_diff_avg+"</p>"
                                      +"<p>Water = "+value.total_weekly_water_intake_avg+" Cups</p>";
                            }

                            $('#week_average_calorie .week_'+index).html(html_data_weekly);
                        });


                        var monthly_avg = data.response.monthly_avg;
                        var html_data = "<p>Calorie Average</p>"
                                    +"<p><strong>"+ monthly_avg.monthly_consumed_avg+"/day</strong></p>"
                                    +"<p class="+ monthly_avg.monthly_diff_text_class_avg+">"
                                          + monthly_avg.monthly_diff_text_avg+ "="
                                          + monthly_avg.monthly_diff_avg+"</p>"
                                    +"<p>Water = "+monthly_avg.monthly_water_avg+" Cups</p>";

                        $('.month-average-calorie').html(html_data);

                    });
            },

            eventRender: function(event, element, view) {
                     return $(
                              '<div class="tabletd-div cal_sammary_data"><p>Calorie Total</p> <p><b>' +
                                    event.calory +
                                    '</b></p>'+
                                    '<p style="color:'+event.diff_text_color+';">'+event.diff_text+' = ' + event.diff + '</p>'+

                                '<p>Water = '+event.water_intake+' Cups</p></div>');
            }

        });

    }

});



    /*$('.fc-prev-button').find('span').removeClass('fc-icon fc-icon-left-single-arrow').addClass('fa fa-chevron-left fa-fw');
    $('.fc-next-button').find('span').removeClass('fc-icon fc-icon-right-single-arrow').addClass('fa fa-chevron-right fa-fw');*/

    $('.fc-next-button').append('&nbsp;<span class="fa fa-calendar fa-fw"></span>');
</script>

@if(Auth::User()->status == "Incomplete")
   <script type="text/javascript">
          $('#user_incomplete').modal('show');
   </script>
@endif

@if(isset($auth0_user_data['email_varify']) && !($auth0_user_data['email_varify']))
  @if($auth0_user_data['created_at_diff'] > 7)
  <script type="text/javascript">

        $('#user_verify').modal('show');

  </script>
  @endif
@endif

{!! HTML::script("js/controllers/DashboardController.js") !!}
{!! HTML::script("js/directives/inputDatepicker.js") !!}
{!! HTML::script("js/directives/gaugeMeter.js") !!}
@append
