  <div id="all_templates_div" ng-init="my_plan.getMDTemplate();">
    <div class="container">
        <div class="row text-center main-top">

            <div class="col-md-6">
                <p class="select-create-plan">Select/Create Plan</p>
                <div class="row b-r">
                    <div class="col-md-11">
                        <div class="row"><p class="sub-title">PLAN TEMPLATES</p></div>

                        <div class="row plan-tem-bg">

                            <div class="col-md-7 col-sm-7 col-xs-12"><p class="choose-plan">Choose a Plan Template</p>
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-12 mrg-top-10">
                                <select class="form-control" ng-model="my_plan.nutrition_template_id"
                                    ng-change="my_plan.getMealTemplate();" >
                                    <option value="0">Select</option>
                                   <option ng-repeat="nt in my_plan.nutrition_templates" value="@{{nt.id}}">@{{nt.template_name}}</option>
                                </select>
                           </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 select-day">
                                <p>Select Days</p>
                            </div>

                            <div class="col-md-12 col-sm-12 pad-top-10">
                                <span  ng-repeat= "(key, week_day) in my_plan.week">
                                <input id="checkbox-@{{key}}"
                                       class="checkbox-custom"
                                       name="checkbox-@{{key}}"
                                       type="checkbox"
                                       checklist-value="week_day.dayNumber"
                                       checklist-model="my_plan.select_days"
                                       >
                                <label for="checkbox-@{{key}}" class="checkbox-custom-label"> @{{week_day.dayName}}</label>
                                </span>

                            </div>

                            <div class="col-md-12 mrg-top-10 "><a class="blue-btn pull-left sel-btn" href="#"><span>Select Day Plan Template</span></a></div>

                        </div>

                        <div class="row"><p class="sub-title">MEAL TEMPLATES</p></div>

                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <tbody  ng-repeat="m_temp in my_plan.master_templates | filter:{parent:'1'}">
                                        <tr>
                                            <th colspan="3" class="table-title">@{{m_temp.template_name}}</th>
                                        </tr>
                                        <tr>
                                            <td class="table-sub">

                                           <!--  <a href="#">Select Meal Template</a> -->

                                            <select class="form-control" ng-if="m_temp.type == 'Meal'" style="width:220px;">
                                                <option>Select Meal Template</option>
                                                <option ng-repeat="mt in my_plan.meal_templates" value="@{{mt.id}}">@{{mt.template_name}}</option>
                                            </select>

                                            <select class="form-control" ng-if="m_temp.type == 'Workout'" style="width:220px;">
                                                <option>Select Workout Template</option>
                                                <option ng-repeat="wt in my_plan.workouts_templates" value="@{{wt.id}}">@{{wt.template_name}}</option>
                                            </select>

                                            </td>
                                            <td class="table-sub">
                                               <a
                                                  ui-sref="create-meal({ master_template_id: m_temp.id,
                                                                          master_template_name : m_temp.template_name
                                                                      })"

                                                ng-if="m_temp.type == 'Meal'">Create Meal</a>
                                               <a href="#" ng-if="m_temp.type == 'Workout'">Create Workout</a>
                                            </td>
                                            <td class="table-sub">
                                                <a href="javascript:void(0);" ng-click="my_plan.openDayTimeModel(m_temp);">Select Times &amp; Days</a>
                                            </td>
                                        </tr>
                                    </tbody>



                                </table>
                            </div><!--end of .table-responsive-->
                        </div>

                        <div class="row"><p class="sub-title">SUPPLEMENT TEMPLATES</p></div>

                        <div class="row plan-tem-bg1 plan-tem-bg">

                            <div class="col-md-9  col-sm-9 col-xs-12"><p class="choose-plan">Select a Package Programme </p></div>
                            <div class="col-md-3  col-sm-3 col-xs-12 mrg-top-10"><a class="blue-btn" href="#"><span>Select</span></a></div>

                            <div class="col-md-12 col-sm-12 col-xs-12 select-day">
                                <p>Supplements</p>
                            </div>

                            <div class="col-md-12">
                                    <table class="table table-bordered table-hover table-mrg">
                                        <tbody>
                                            <tr>
                                                <td class="table-sub table-sub1"><a href="#">Add Supplement</a></td>
                                                <td class="table-sub table-sub1"><a href="#">Select Times & Days</a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                        </div>

                        <div class="row"><p class="sub-title">MEDICATION</p></div>

                        <div class="row plan-tem-bg2 plan-tem-bg">

                            <div class="col-md-12 col-sm-12 col-xs-12 select-day">
                                <p>Medication</p>
                            </div>

                            <div class="col-md-12">
                                <table class="table table-bordered table-hover table-mrg">
                                    <tbody>
                                        <tr>
                                            <td class="table-sub"><a href="#">Add Medication</a></td>
                                            <td class="table-sub"><a href="#">Select Times & Days</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                </div>
            </div>


            <div class="col-md-6 ">
            <div class="preview">
             Preview
            </div>


            <div class="row calendar_view">
                 <div ui-calendar="my_plan.uiConfig.calendar"
                      ng-model="my_plan.eventSources"
                      calendar="nutrition_calendar"
                      class="angular-calender">
              </div>

           </div>
        </div>
    </div>
  </div>
