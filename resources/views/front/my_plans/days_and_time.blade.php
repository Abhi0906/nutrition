 <!-- Day and Time modal -->
  <div id="days_time_modal" class="modal fade" role="dialog" style="z-index: 9999">
    <div class="modal-dialog">
       <div class="panel panel-default panel_modal_contain_warn">
        <div class="panel-heading panel_modal_contain_warn_head">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title modal_title_head text-center">@{{my_plan.selectTemplate.template_name}}</h4>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <tbody>

                            <tr>
                                <td class="table-sub">
                                    <input id="checkbox-all"
                                       class="checkbox-custom"
                                       name="checkbox-all"
                                       type="checkbox"
                                       checklist-value="week_day.dayNumber"
                                       checklist-model="my_plan.select_days1"
                                       >
                                   <label for="checkbox-all" class="checkbox-custom-label"> All</label>
                                </td>
                                <td class="table-sub">
                                      <time-picker ng-init="my_plan.template['all'].meal_time = my_plan.meal_time"
                                             ng-model="my_plan.template['all'].meal_time"></time-picker>
                                </td>
                            </tr>

                            <tr ng-repeat= "(key, week_day) in my_plan.week">
                                <td class="table-sub">
                                    <input id="checkbox-all"
                                       class="checkbox-custom"
                                       name="checkbox-all"
                                       type="checkbox"
                                       checklist-value="week_day.dayNumber"
                                       checklist-model="my_plan.select_days1"
                                       >
                                   <label for="checkbox-all" class="checkbox-custom-label"> @{{week_day.dayName}}</label>
                                </td>
                                <td class="table-sub">
                                      <time-picker ng-init="my_plan.template[$index].meal_time = my_plan.meal_time"
                                             ng-model="my_plan.template[$index].meal_time"></time-picker>
                                </td>
                            </tr>
                        </tbody>

                    </table>
                </div><!--end of .table-responsive-->
            </div>

        </div>
      </div>
    </div>
  </div>
  <!-- Days and Time modal -->