@extends('layout.master')

@section('title')
  My Plans
@endsection

@section('css-link')
{!! HTML::style("css/my_plans.css") !!}
@endsection

@section('custom-css')

@endsection
@section('content') 

  <div ui-view   ng-init="my_plan.gender='{{$user->gender}}';my_plan.user_id='{{$user->id}}'">
    
  </div> 

@endsection

@section('js')


@append

@section('custom-js')

{!! HTML::script("js/controllers/MyPlanController.js") !!}
{!! HTML::script("js/controllers/UserMealController.js") !!}
{!! HTML::script("js/directives/checklist-model.js") !!}

@append