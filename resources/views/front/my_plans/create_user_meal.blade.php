  
  <section class="section-page custom-section-page custom-section-panel padding_top_0"> 
     
	<div class="row">
	<div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12">
	    <h2 id="log_food_focus_id" class="text-light-blue text-uppercase head_title_main">
	       <span class="title_head_line">Pre Workout Meal</span>
	    </h2>
	</div> 
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" ng-init="add_food.favouriteFood(); add_food.allLoggedFood();add_food.recommended_food_type_id=1; add_food.getFoodRecommendedTypes();add_food.getMdRecommendedFoods();add_food.getMealsByUser();">
	    <a class="btn btn-primary btndesign" ui-sref="/">Back</a>
	    <div class="small-separator"></div>
	</div>
	</div>

	<div class="row">
	   <div class="col-md-12">
		   	  <div class="food_dairy_block custom_food_dairy_block" id="responsive_food_table">             
				    <table class="table table-bordered borderless_table margin_bottom_zero" id="simple-example-table">
				      <thead> 
				        <tr class="toptr-border">
				          <th class="calorie-subhead-td withborder text-uppercase"></th>
				          <th class="calorie-subhead-td withborder text-uppercase"">CALORIES</th>
	                      <th class="calorie-subhead-td withborder text-uppercase"">SERVING SIZE</th>
	                      <th class="calorie-subhead-td withborder text-uppercase"">NO. Of SERVING</th>
	                      <th class="calorie-subhead-td withborder text-uppercase"">FAT</th>
	                      <th class="calorie-subhead-td withborder text-uppercase"">FIBER</th>
	                      <th class="calorie-subhead-td withborder text-uppercase"">CARBS</th>
	                      <th class="calorie-subhead-td withborder text-uppercase"">SODIUM</th>
	                      <th class="calorie-subhead-td withborder text-uppercase"">PROTEIN</th>
				        </tr>
				      </thead>
				      <tbody>

				          <!-- BREAKFAST -->
				          <tr class="text-center toptr-border">
				            <td id="food_1st_td" data-title=" " colspan="9" class="sidecolor text-leftside withborder head_slide_foodtbl_td">
				              <ul class="list-inline">
				                <li>
				                  <h4 id="Protine">PROTEIN</h4>
				                </li>
				                <li>
				                   
				                  <a title="Add Food" ui-sref="log_food({ food_type : 'Breakfast'})" class="add_food" id="add_breakfast"  >
				                    <i class="fa fa-plus-circle facolor add_plus_btn"></i>
				                  </a> 
				                </li>
				               
				              </ul>
				            </td>
				          </tr>

				          
				            </td>
				          </tr>


				          <tr class="text-center toptr-border">
				            <td id="food_1st_td" data-title=" " colspan="9" class="sidecolor text-leftside withborder head_slide_foodtbl_td">
				              <ul class="list-inline">
				                <li>
				                  <h4 id="Protine" style="text-transform: uppercase;">Carbohydrates</h4>
				                </li>
				                <li>
				                   
				                  <a title="Add Food" ui-sref="log_food({ food_type : 'Breakfast'})" class="add_food" id="add_breakfast"  >
				                    <i class="fa fa-plus-circle facolor add_plus_btn"></i>
				                  </a> 
				                </li>
				               
				              </ul>
				            </td>
				          </tr>

				          <tr class="text-center toptr-border">
				            <td id="food_1st_td" data-title=" " colspan="9" class="sidecolor text-leftside withborder head_slide_foodtbl_td">
				              <ul class="list-inline">
				                <li>
				                  <h4 id="Protine" style="text-transform: uppercase;">Vegetables</h4>
				                </li>
				                <li>
				                   
				                  <a title="Add Food" ui-sref="log_food({ food_type : 'Breakfast'})" class="add_food" id="add_breakfast"  >
				                    <i class="fa fa-plus-circle facolor add_plus_btn"></i>
				                  </a> 
				                </li>
				               
				              </ul>
				            </td>
				          </tr>

				          <tr class="text-center toptr-border">
				            <td id="food_1st_td" data-title=" " colspan="9" class="sidecolor text-leftside withborder head_slide_foodtbl_td">
				              <ul class="list-inline">
				                <li>
				                  <h4 id="Protine" style="text-transform: uppercase;">Healthy Fats</h4>
				                </li>
				                <li>
				                   
				                  <a title="Add Food" ui-sref="log_food({ food_type : 'Breakfast'})" class="add_food" id="add_breakfast"  >
				                    <i class="fa fa-plus-circle facolor add_plus_btn"></i>
				                  </a> 
				                </li>
				               
				              </ul>
				            </td>
				          </tr>

				          <tr ng-repeat="(key, value) in diary.breakfastFood | orderBy:'-id' ">
				             <td data-title="Food" class="first-td-border">
				             <a href="javascript:void(0);" ng-click="diary.deleteLoggedFood(value)">
				               <i class="fa fa-times-circle facolor"></i>
				             </a>
				              @{{value.name}}
				             </td>
				             <td data-title="CALORIES" class="text-center black_text">@{{value.calories}}</td>
				             <td data-title="DESCRIPTION" class="text-left black_text">
				                Fat: @{{value.fat}}, Fiber: @{{value.fiber}}, Carb: @{{value.carb}}, Sodium: @{{value.sodium}}, Protein: @{{value.protein}}
				              </td>
				          </tr>
				          <tr class="total_tr">
				            <td  class="first-td-border">TOTAL</td>
				            <td data-title="CALORIES" class="text-center">@{{diary.totalBreakfast.calorie || 0}}</td>
				            <td data-title="CALORIES" class="text-center">@{{diary.totalBreakfast.calorie || 0}}</td>
				            <td data-title="CALORIES" class="text-center">@{{diary.totalBreakfast.calorie || 0}}</td>
				            <td data-title="CALORIES" class="text-center">@{{diary.totalBreakfast.calorie || 0}}</td>
				            <td data-title="CALORIES" class="text-center">@{{diary.totalBreakfast.calorie || 0}}</td>
				            <td data-title="CALORIES" class="text-center">@{{diary.totalBreakfast.calorie || 0}}</td>
				            <td data-title="CALORIES" class="text-center">@{{diary.totalBreakfast.calorie || 0}}</td>
				            <td data-title="CALORIES" class="text-center">@{{diary.totalBreakfast.calorie || 0}}</td>
				          </tr>
				          <!-- end: BREAKFAST -->
				       </tbody>
				    </table>
			  </div>

	   </div>
	</div>

</section>
      
