 <!-- scroll tabs -->
  <section class="scroll_tab_section">
    <div class="row">
      <div class="col-md-12">
        <div id="scroll_tabs_dash" class="hidden-xs">
            <ul class="nav nav-tabs nav-justified main_tabs">
                <li class="active">
                  <a class="scroll_divs" id="all_templates" data-target="#all_templates_div" href="javascript:void(0)">ALL</a>
                </li>
                <li>
                <a class="scroll_divs" id="nutrition_exercise" data-target="#nutrition_exercise_div" href="javascript:void(0)">NUTRITION & EXERCISE</a>
                </li>
                <li>
                <a class="scroll_divs" id="supplements" data-target="#supplements_div" href="javascript:void(0)">SUPPLIMENTS</a>
                </li>
                <li>
                <a class="scroll_divs" id="meds" data-target="#meds_div" href="javascript:void(0)">MEDS</a>
                </li>
            </ul>
        </div>
      </div>
    </div>
  </section>
  <!-- end scroll tabs -->


    
      <!-- all template pages -->
      @include('front/my_plans/all_templates')
      <!-- end all template pages -->
     @include('front/my_plans/days_and_time')
