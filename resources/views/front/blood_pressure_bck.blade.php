@extends('layout.app')
@section('content')
<div class="row" ng-controller="BloodPressureController as weight" ng-init="weight.userId='{{$user_id}}';weight.loggedWeight(150, 'seven-days'); ">
	<div class="col-xs-12">
        <!-- Classic Chart Block -->
        <div class="block full">
            <!-- Classic Chart Title -->
            <div class="block-title">
                <h2>Blood Pressure & Pulse</h2>
            </div>
            <!-- END Classic Chart Title -->
            <div class="col-md-12">
	            <button type="button" id="seven-days" class="btn btn-xs btn-info days-filter" ng-click="weight.loggedWeight(7, 'seven-days');">Last 7 Days</button>
	            <button type="button" id="thirty-days" class="btn btn-xs btn-info days-filter" ng-click="weight.loggedWeight(30, 'thirty-days');">Last 30 Days</button>
	            <button type="button" id="ninety-days" class="btn btn-xs btn-info days-filter" ng-click="weight.loggedWeight(90, 'ninety-days');">Last 90 Days</button>
	            <button type="button" id="one-eighty-days" class="btn btn-xs btn-info days-filter" ng-click="weight.loggedWeight(180, 'one-eighty-days');">Last 180 Days</button>
	            <button type="button" id="year" class="btn btn-xs btn-info days-filter" ng-click="weight.loggedWeight(365, 'year');">Last Year</button>
            </div>
     		<!-- amchart -->
            <div id="chartdiv"></div>
            <!-- end: amchart -->
        </div>
        <!-- END Classic Chart Block -->
    </div>
    <!-- END Classic and Bars Chart -->    
	<div class="col-md-5 col-xs-12">
		<form name="LogWeightForm" novalidate="novalidate" class="animation-fadeInQuick">
			<div class="block full">
				<div class="block-title">
			    	<h3>
				        <i class="fa wellness-icon-weight"></i>&nbsp;&nbsp;Log Blood Pressure & Pulse
				    </h3>
				</div>				
				<div class="widget-extra-full log-weight-form">
					<div class="form-horizontal form-bordered">
						<div class="form-group">
		                  	{!! HTML::decode(Form::label('systolic', 'Systolic<sup class="text-danger">*</sup>', array('class'=>'col-md-4 control-label'))) !!}
		                  	<div class="col-md-8">
		                     	<div class="input-group">
		                        	<input type="text" number-validation="number-validation"   
		                        	maxlength="6" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" 
		                           ng-model="weight.log_weight.systolic" 
		                           placeholder="Systolic" name="systolic" id="systolic" class="form-control" required>
		                        	<span class="input-group-btn">
		                        		<span class="btn btn-primary">&nbsp;&nbsp;mmHg&nbsp;&nbsp;</span>
		                        	</span>
		                     	</div>
		                    </div>
		               	</div>
		               	<div class="form-group">
		                  	{!! HTML::decode(Form::label('diastolic', 'Diastolic<sup class="text-danger">*</sup>', array('class'=>'col-md-4 control-label'))) !!}
		                  	<div class="col-md-8">
		                     	<div class="input-group">
		                        	<input type="text" number-validation="number-validation"
		                           		ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" name="diastolic" id="diastolic"
		                           		ng-model="weight.log_weight.body_fat"  maxlength="5" 
		                           		placeholder="Diastolic" name="body_fat" id="body_fat" class="form-control" required>
		                        	<span class="input-group-btn">
		                        		<span class="btn btn-primary">&nbsp;&nbsp;mmHg &nbsp;&nbsp;</span>
		                        	</span>
		                     	</div>
		                     
		                  	</div>
		               	</div>
		               	<div class="form-group">
		                  	{!! HTML::decode(Form::label('heart_rate', 'Heart Rate<sup class="text-danger">*</sup>', array('class'=>'col-md-4 control-label'))) !!}
		                  	<div class="col-md-8">
		                     	<div class="input-group">
		                        	<input type="text" number-validation="number-validation"
		                           		ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" 
		                           		ng-model="weight.log_weightr"  maxlength="5" 
		                           		placeholder="Heart Rate" name="heart_rate" id="heart_rate" class="form-control" required>
		                        </div>
		                     	<p class="text-danger" ng-bind-html="weight.body_fat_error"></p>
		                  	</div>
		               	</div>
		               	<div class="form-group">
		                  	{!! HTML::decode(Form::label('on', 'On<sup class="text-danger">*</sup>', array('class'=>'col-md-4 control-label'))) !!}
		                  	<div class="col-md-8">
		                     	<div class="input-group">
		                        	<input-datepicker  format="MM dd, yyyy" date="weight.log_weight.log_date" disabled="disabled"></input-datepicker>
								</div>
							</div>
		               	</div>
		               	<div class="form-group">
		                  	<div class="col-md-4"></div>
		                  	<div class="col-md-8">
		                     	<div class="input-group">
		                        	<button type="button" class="btn btn-primary" ng-disabled="LogWeightForm.$invalid || weight.log_weight_error" ng-click="weight.checkLoggedWeight();">Log</button>
		                     	</div>
		                  	</div>
		               	</div>
					</div>

	                <div class="row">
	                	<div class="col-xs-12 col-md-3">
		                	
		                </div>
	                </div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-md-7 col-xs-12">
		<div class="block full">
            <div class="block-title">
                <h4>
                   Blood Pressure & Pulse History
                </h4>
            </div>
            <div class="widget-extra-full">
	            <div id="no-more-tables" class="table-responsive">
		            <table class="table table-striped">
		                <tbody>
		                	<tr>
		                        <td class="hidden-xs text-center">Day</td>
		                        <td class="hidden-xs text-center">Systolic</td>
		                        <td class="hidden-xs text-center">Diastolic</td>
		                        <td class="hidden-xs text-center">Heart Rate</td>
		                    </tr>
		                    <tr>
		                        <td data-title="Day" class="text-center text-muted">Wednesday, Apr 13</td>
		                        <td data-title="Systolic" class="text-center text-muted">35 mmHg</td>
		                        <td data-title="Diastolic" class="text-center text-muted">32 mmHg</td>
		                        <td data-title="HeartRate" class="text-center text-muted">400 </td>
		                    </tr>
		                     <tr>
		                        <td data-title="Day" class="text-center text-muted">Friday, Apr 15</td>
		                        <td data-title="Systolic" class="text-center text-muted">45 mmHg</td>
		                        <td data-title="Diastolic" class="text-center text-muted">55 mmHg</td>
		                        <td data-title="HeartRate" class="text-center text-muted">450 </td>
		                    </tr>
		                </tbody>
		            </table>
		        </div>
            </div>
        </div> 
	</div>
	
	<div id="log_weight_confirmation" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content">
	 			<!-- Modal Header -->
	 			<div class="modal-header  has-warning text-center custom-modal-header">
	 				<div class="alert custom_alert">
	 					
						<i class="fa fa-exclamation-triangle"></i>
	   					<strong> Warning!</strong>
	  				</div>
	      			<div class="modal-body custom-modal-body">
	          			<p class="custom-warning-text"><strong>Today's weight is already logged, do you want to replace?</strong></p>
	      				<p class="text-right">
	      					
		                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">No</button>
		                    <button type="button" class="btn btn-sm btn-primary" ng-click="weight.logWeight()">Yes</button>
		                    
		                </p>
	      			</div>
	      			
	    		</div>
			</div>
		</div>
	</div>
</div>
@endsection
<!-- Load and execute javascript code used only in this page -->
@section('page_scripts')
{!! HTML::script("js/controllers/BloodPressureController.js") !!}
{!! HTML::script("js/directives/inputDatepicker.js") !!}

<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
@append

