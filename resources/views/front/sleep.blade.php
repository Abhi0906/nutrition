@extends('layout.master')
@section('title')
  Sleep
@endsection 

@section('custom-css')
 <style type="text/css">
   @media(max-width: 800px) {
      #sleep_new_reptable table,
       #sleep_new_reptable thead,
       #sleep_new_reptable tbody,
       #sleep_new_reptable th,
       #sleep_new_reptable td,
       #sleep_new_reptable tr 
       {
         display: block;
       }
       #sleep_new_reptable thead tr {
           position: absolute;
           top: -9999px;
           left: -9999px;
       }
       #sleep_new_reptable tr { border: none; }
         #sleep_new_reptable td {
         /* Behave like a "row" */
           border: none !important;
           /*border-bottom: 1px solid #eee;*/
           position: relative;
           padding-left: 45%;
           white-space: normal;
           text-align:left;
         }
         #sleep_new_reptable table tbody tr td:before {
           /* Now like a table header */
           position: absolute;
           /* Top/left values mimic padding */
           top: 6px;
           left: 6px;
           width: 40%;
           padding-right: 10px;
           white-space: normal;
           text-align:left;
           font-weight: bold;
           padding-top: 15px;
           color: #0073BC;
           text-align: right;
         }
         #sleep_new_reptable td:before { content: attr(data-title); }
   }
 </style>

@endsection 

@section('content') 
<!-- Start Weight Diary -->
<section class="section-panel custom-section-panel"
        ng-controller="SleepController as sleep"
         ng-init="sleep.userId='{{$user_id}}'; 
         sleep.calculateTotalSleep(); 
         sleep.userSleep();">
   <!-- header title -->
   <div class="row text-center text-primary">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
         <strong>
            <h2 class="text-uppercase head_title_main">YOUR <span class="title_head_line ">SLEEP</span> DIARY</h2>
         </strong>
      </div>
   </div>
   <div class="saperator"></div>
   <div class="row">
      <!-- <h4 class="col-sm-12 sub_head_blue">SLEEP GOALS</h4> -->
      <p class="col-sm-12 margin_bot"><strong><span class="text-light-blue">Last Log Date: @{{sleep.sleep_progess.last_log_date}} </span></strong></p>
      <p class="col-sm-12 panel_head_text">SLEEP GOALS</p>
      <div class="col-sm-12">
         <div id="sleep_new_reptable">
            <table class="table new_sleepbox box_border_noradius">
               <thead class="box_head_background">
                  <tr>
                     <th>Last SLEEP (@{{sleep.sleep_progess.display_last_log_date}})</th>
                     <th>Last 7 Days</th>
                     <th>Last 30 Days</th>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                     <td data-title="LAST SLEEP">
                         <p><span class="green"><b>@{{sleep.sleep_progess.last_progress}}%</b></span> of goal achieved</p>
                     </td>
                     <td data-title="LAST 7 DAYS"><p><span class="green"><b>@{{sleep.sleep_progess.last_week_count}}</b></span> of <span class="text-primary"><b>7</b></span> times goal achieved.</p></td>
                     <td data-title="LAST 30 DAYS"><p><span class="green"><b>@{{sleep.sleep_progess.last_month_count}}</b></span> of <span class="text-primary"><b>30</b></span> times goal achieved</p></td>
                  </tr>
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <div class="saperator"></div>
   <!-- <div class="row">
      <div class="col-xs-12">
         <span class="text-light-blue">Last Log Date: @{{sleep.sleep_progess.last_log_date}} </span>
      </div>
   </div> -->
   <!-- End of header title -->
   <!-- sleep row -->
   <div class="row tab_chart_row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 wight_body_portion">
         @include('front.sleep.log_sleep')
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 body_measure_portion">
         @include('front.sleep.last_sleep')
      </div>
    </div>
   <!-- end of sleep row -->
   <div class="saperator"></div>
   @include('front.sleep.weekly_sleep')
</section>

<!-- End Weight Diary -->
@endsection
@section('js')
{!! HTML::script("js/amcharts.js") !!}
{!! HTML::script("js/light.js") !!}
{!! HTML::script("js/serial.js") !!}
{!! HTML::script("js/jquery.sameheight.js") !!}
{!! HTML::script("js/controllers/SleepController.js") !!}

@append
@section('custom-js')
<script type="text/javascript">


$(document).ready(function(){

    if($(window).width() < 1200 ){
      $('#reponsive_table_structure tr td.head_slide_click').siblings().slideUp();
      $("#sun").siblings().slideToggle();
      $('#reponsive_table_structure tr td.head_slide_click').click(function(){
        $(this).siblings().slideToggle("slow");
      });
    }

    if($(window).width() >= 1200){
      var he = $('.sleep_panel_sameheight').sameheight();
      console.log('height' + he);
    }
});
</script>

@append
