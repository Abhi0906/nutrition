<style type="text/css">
  .text-excess{
    color: #FF1A1A !important;
  }
  .text-deficit{
    color: #33CC33 !important;
  }
  .text-neutral{
    color: #FDE674 !important;
  }
</style>
<div class="row text-center">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <strong><h2 class="head_title_main">MONTHLY CALORIE SUMMARY</h2></strong>
  </div>
 </div>
<div class="row food_monthly_calander_container">
  <div class="col-xs-6 col-sm-10 col-md-10 col-lg-10 calendar_portion" id="diary_cal_part">
    <div ng-model="diary.eventSources"
     ui-calendar="diary.uiConfig.calendar"
      class="food_monthly_calandar angular-calender hidden-xs"
      calendar="diaryCalendar" 
      ></div>
    

    <div class="visible-xs">
      <table class="table table-bordered calendar_table_side calendar_blockequal left_side_table_cale food_fix_tr_height">
        <thead>
          <tr class="tab_firsthead">
            <th class="calendar_table_head">Month Average</th>
          </tr>
        </thead>
        <tbody>
          <tr class="tab_colnum1 tablecell_blockqual week_1">
            <td>
              <p>First Week Average</p>   
            </td>
          </tr>
          <tr class="tablecell_blockqual">
            <td>
              <p>Second Week Average</p>   
            </td>
          </tr>
          <tr class="tablecell_blockqual">
            <td>
              <p>Third Week Average</p>   
            </td>
          </tr>
          <tr class="tablecell_blockqual">
            <td>
              <p>Fourth Week Average</p>   
            </td>
          </tr>
          <tr class="tablecell_blockqual">
            <td>
              <p>Fifth Week Average</p>   
            </td>
          </tr>
          <tr class="tablecell_blockqual">
            <td>
              <p>Sixth Week Average</p>   
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    
  </div>
  <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 calendar_week_avg" id="diary_cal_part_tbl">
    <table id="week_average" class="table table-bordered calendar_table_side calendar_blockequal right_side_table_cale">
      <thead>
        <tr class="tab_firsthead">
          <th class="calendar_table_head">Week Average</th>
        </tr>
      </thead>
      <tbody>
        <tr class="tablecell_blockqual">
          <td>
            <div class="tabletd-div calendar-week-cell week_1">
                
            </div>
          </td>
        </tr>
        <tr class="tablecell_blockqual">
          <td>
            <div class="tabletd-div calendar-week-cell week_2">
               
            </div>
          </td>
        </tr>
        <tr class="tablecell_blockqual">
          <td>
            <div class="tabletd-div calendar-week-cell week_3">
               
            </div>
          </td>
        </tr>
        <tr class="tablecell_blockqual">
          <td>
            <div class="tabletd-div calendar-week-cell week_4">
              
            </div>
          </td>
        </tr>
        <tr class="tablecell_blockqual">
          <td>
            <div class="tabletd-div calendar-week-cell week_5">
              
            </div>
          </td>
        </tr>
        <tr class="tablecell_blockqual">
          <td>
            <div class="tabletd-div calendar-week-cell week_6">
              
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<div class="row">
  <div class="col-xs-6 col-sm-10 col-md-10 col-lg-10 footercal_side" id="diary_cal_part2">
    <div class="monthly_calendar_footer columnfooterdiv">
      <p class="footer_para no_margin">Month Average</p>
    </div>
  </div>
  <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 footercal_side footercal_side_right" id="diary_cal_part_tbl2">
    <div id="month-average-calorie" class="monthly_calendar_footer monthly_rightside columnfooterdiv">
    
    </div>
  </div>
</div>
<div class="saperator hidden-lg"></div>
