
<div class="tabs_food_foods">
<h3 class="subhead_title_main subhead_title_main_sidebar">FOODS</h3>
<span class="plain-select">
<select ng-model="selected_food_option" class="form-control food_sidedropdown food_sidedropdown_side_bar"
        ng-init="selected_food_option='my_meals'">
  <!-- <option value="md_meals" class="list-group-item">MD Meals</option> -->
  <option value="proteins" class="list-group-item">MD Proteins</option>
  <option value="carbs" class="list-group-item">MD Carbs</option>
  <option value="vegetables" class="list-group-item">MD Vegetables</option>
  <option value="healthy_fats" class="list-group-item">MD Healthy Fats</option>
  <option value="my_meals" class="list-group-item">MY MEALS</option>
  <option value="most_logged" class="list-group-item">MOST LOGGED</option>
  <option value="recent" class="list-group-item">RECENT</option>
  <option value="custom" class="list-group-item">CUSTOM</option>
  <option value="favorite" class="list-group-item">FAVORITE</option>
  
</select>
</span>
<div class="clearboth"></div>
<div id="food_meal_tabs_foods" class="tabs_container diary_sidebar_tabs_container">
  <div class="tab-content tabs_details">
    <div id="meals_tab" class="tab-pane fade in active">
      <table class="table table-hover table-bordered right_tbl_partlist">
        <thead>
          <tr>
            <th>Cals</th>
            <th>Click on an item to log it.</th>
          </tr>
        </thead>
        <tbody>

          <!-- md_meals food -->
          <!-- <tr ng-if="selected_food_option=='md_meals'" ng-repeat="meal in diary_index.md_meals">
            <td>@{{meal.meal_name}}</td>
            <td>
               <a ui-sref="log_food({ selected_from : 'md_meals', selected_food : @{{meal}} })">
                @{{meal.meal_name | wordlimit:true:35:' ...' }}&nbsp;(@{{meal.meal_for}})
              </a>
            </td>
          </tr> -->
          <!-- end: md_meals food -->

          <!-- protein food -->
          <tr ng-if="selected_food_option=='proteins'" ng-repeat="food in diary_index.proteins_foods">
            <td>@{{food.calories}}</td>
            <td>
               <a ui-sref="log_food({ selected_from : 'proteins_foods', selected_food : @{{food}} })">
                @{{food.nutritions.name | wordlimit:true:35:' ...' }}
              </a>
            </td>
          </tr>
          <!-- end: protein food -->
          <!-- carb food -->
          <tr ng-if="selected_food_option=='carbs'" ng-repeat="food in diary_index.carb_foods">
            <td>@{{food.calories}}</td>
            <td>
               <a ui-sref="log_food({ selected_from : 'carb_foods', selected_food : @{{food}} })">
                @{{food.nutritions.name | wordlimit:true:35:' ...' }}
              </a>
            </td>
          </tr>
          <!-- end: carb food -->
          <!-- vegetables food -->
          <tr ng-if="selected_food_option=='vegetables'" ng-repeat="food in diary_index.vegetable_foods">
            <td>@{{food.calories}}</td>
            <td>
               <a ui-sref="log_food({ selected_from : 'vegetable_foods', selected_food : @{{food}} })">
                @{{food.nutritions.name | wordlimit:true:35:' ...' }}
              </a>
            </td>
          </tr>
          <!-- end: vegetables food -->
          <!-- healthy_fats food -->
          <tr ng-if="selected_food_option=='healthy_fats'" ng-repeat="food in diary_index.healthyfat_foods">
            <td>@{{food.calories}}</td>
            <td>
               <a ui-sref="log_food({ selected_from : 'healthy_fats_food', selected_food : @{{food}} })">
                @{{food.nutritions.name | wordlimit:true:35:' ...' }}
              </a>
            </td>
          </tr>
          <!-- end: healthy_fats food -->
          <!-- food meal -->
          <tr ng-if="selected_food_option=='my_meals'" ng-repeat="meal in diary_index.meals">
            <td>@{{meal.total_calories}}</td>
            <td>
               <a ui-sref="log_food({ selected_from : 'meal', selected_food : @{{meal}} })">
                @{{meal.meal_name | wordlimit:true:35:' ...' }}
              </a>
            </td>
          </tr>
          <!-- end: food meal -->

          <!-- food favorite -->
          <tr ng-if="selected_food_option=='favorite'" ng-repeat="fav_food in diary_index.favourite_nutritions">
            <td>@{{fav_food.nutritions.nutrition_data.calories}}</td>
            <td>
                <a ui-sref="log_food({ selected_from : 'favourite', selected_food : @{{fav_food}} })">
                @{{fav_food.nutritions.name | wordlimit:true:35:' ...' }}
              </a>
            </td>
          </tr>    
          <!-- end: favorite -->

          <!-- food custom -->
          <tr  ng-if="selected_food_option=='custom'" ng-repeat="custom_food in diary_index.custom_nutritions">
            <td>@{{custom_food.nutrition_data.calories}}</td>
            <td>
               <a ui-sref="log_food({ selected_from : 'custom', selected_food : @{{custom_food}} })">
                @{{custom_food.name | wordlimit:true:35:' ...' }}
              </a>
            </td>
          </tr>
          <!-- end: food custom -->

          <!-- food frequent -->
          <tr ng-if="selected_food_option=='most_logged'"  ng-repeat="freq_food in diary_index.frequent_nutritions">
            <td>@{{freq_food.calories}}</td>
            <td>
                <a ui-sref="log_food({ selected_from : 'frequent', selected_food : @{{freq_food}} })">
                  @{{freq_food.nutritions.name | wordlimit:true:35:' ...' }}
                </a>
            </td>
          </tr>
          <!-- end: food frequent -->

          <!-- food recent -->
          <tr ng-if="selected_food_option=='recent'"  ng-repeat="rec_food in diary_index.recent_nutritions">
            <td>@{{rec_food.calories}}</td>
            <td>
                <a ui-sref="log_food({ selected_from : 'recent', selected_food : @{{rec_food}} })">
                  @{{rec_food.nutritions.name | wordlimit:true:35:' ...' }}
                </a>
            </td>
          </tr>          
          <!-- end: food recent -->

          <!-- edit food -->
          <tr ng-if="selected_food_option=='my_meals'" class="last_tr_white">
            <td colspan="2"><a ui-sref="add_custom_food" class="a_last_tr">
            <a ui-sref="meals" class="">Edit Meals</a></td>
          </tr>
          <tr ng-if="selected_food_option!='my_meals'" class="last_tr_white">
            <td colspan="2"><a ui-sref="add_custom_food" class="a_last_tr">Create a new food</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <a ui-sref="custom_foods" class="">Edit my foods</a></td>
          </tr>
          <!-- edit food -->
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
<div class="saperator"></div>
<div class="tabs_food_fav">
<h3 class="subhead_title_main subhead_title_main_sidebar">EXERCISE</h3>
<span class="plain-select">
<select ng-model="selected_exercise_option"
        ng-init="selected_exercise_option='most_logged'"
         class="form-control food_sidedropdown food_sidedropdown_side_bar">
  <option value="most_logged" class="list-group-item">MOST LOGGED</option>
  <option value="recent" class="list-group-item">RECENT</option>
</select>
</span>
<div class="clearboth"></div>
<div id="food_meal_tabs_fav" class="tabs_container diary_sidebar_tabs_container">
  <div class="tab-content tabs_details">
     <div id="frequent_tab" class="tab-pane fade in active"> 
        <table class="table table-hover table-bordered right_tbl_partlist">
          <thead>
            <tr>
              <th>Cals</th>
              <th>Click on an item to log it.</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-if="selected_exercise_option=='most_logged'" ng-repeat="freq_exercise in diary_index.frequent_exercises | limitTo:10">
              <td>@{{freq_exercise.calories_burned}}</td>
              <td>
                 <a ui-sref="log_exercise({ selected_from : 'frequent', selected_exercise : @{{freq_exercise}} })"> 
                    @{{freq_exercise.name | wordlimit:true:30:' ...'}}
                 </a>
              </td>
            </tr>
            <tr ng-if="selected_exercise_option=='recent'"  ng-repeat="recent_exercise in diary_index.recent_exercises | limitTo:10">
                <td>@{{recent_exercise.calories_burned}}</td>
                <td>
                    <a ui-sref="log_exercise({ selected_from : 'recent', selected_exercise : @{{recent_exercise}} })">
                      @{{recent_exercise.name | wordlimit:true:30:' ...'}}
                    </a>
                </td>
              </tr>
          </tbody>
        </table>
     </div>
  </div>
</div>
</div>