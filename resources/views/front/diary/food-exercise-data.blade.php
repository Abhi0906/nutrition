
<div class="food_dairy_block custom_food_dairy_block" id="responsive_food_table">             
    <table class="table table-bordered borderless_table margin_bottom_zero" id="simple-example-table">
      <thead> 
        <tr class="toptr-border">
          <th class="calorie-subhead-td withborder text-uppercase"></th>
          <th class="calorie-subhead-td withborder text-uppercase">CALORIES</th>
          <th class="calorie-subhead-td withborder text-left text-uppercase" colspan="5">Description</th>
        </tr>
      </thead>
      <tbody>

          <!-- Meal 1 (Breakfast) -->
          <tr class="text-center toptr-border">
            <td id="food_1st_td" data-title=" " colspan="6" class="sidecolor text-leftside withborder head_slide_foodtbl_td">
              <ul class="list-inline">
                <li>
                  <h4 id="Meal 1(Breakfast)">Meal 1(Breakfast)</h4>
                </li>
                <li>
                   
                  <a title="Add Food" ui-sref="log_food({ food_type : 'meal_1'})" class="add_food" id="add_breakfast"  >
                    <i class="fa fa-plus-circle facolor add_plus_btn"></i>
                  </a> 
                </li>
                <li>
                  
                  <a ng-if="diary.meal_one_food.length > 0" title="Save Meal" ui-sref="create_meal({ food_type: 'meal_1', meal_food: diary.meal_one_food, meal_food_total: diary.meal_one_total })" class="add_food" >
                   <i class="fa fa-cog facolor setting_btn"></i> 
                  </a>
                </li>
              </ul>
            </td>
          </tr>

          <tr ng-repeat="(key, value) in diary.meal_one_food | orderBy:'-id' ">
             <td data-title="Food" class="first-td-border">
             <a href="javascript:void(0);" ng-click="diary.deleteLoggedFood(value)">
               <i class="fa fa-times-circle facolor"></i>
             </a>
              @{{value.name}}
             </td>
             <td data-title="CALORIES" class="text-center black_text">@{{value.calories}}</td>
             <td data-title="DESCRIPTION" class="text-left black_text">
                Fat: @{{value.fat}}, Fiber: @{{value.fiber}}, Carb: @{{value.carb}}, Sodium: @{{value.sodium}}, Protein: @{{value.protein}}
              </td>
          </tr>
          <tr class="total_tr">
            <td  class="first-td-border">TOTAL</td>
            <td data-title="CALORIES" class="text-center">@{{diary.meal_one_total.calorie || 0}}</td>
            <td data-title="DESCRIPTION" class="" >&nbsp;</td>
          </tr>
          <!-- end: Meal 1 (Breakfast) -->

          <!-- Meal 2 (Snack) -->
          <tr class="text-center toptr-border">
            <td data-title=" " colspan="7" class="sidecolor text-leftside withborder head_slide_foodtbl_td">
              
              <ul class="list-inline">
                <li>
                  <h4 id="Meal 2(Snack)">Meal 2 (Snack)</h4>
                </li>
                <li>
                   
                  <a title="Add Food" ui-sref="log_food({ food_type : 'meal_2'})" class="add_food" id="add_snack"  >
                    <i class="fa fa-plus-circle facolor add_plus_btn"></i>
                  </a> 
                </li>
                <li>                  
                  <a ng-if="diary.meal_two_food.length > 0" title="Save Meal" ui-sref="create_meal({ food_type: 'meal_2', meal_food: diary.meal_two_food, meal_food_total: diary.meal_two_total })" class="add_food" >
                   <i class="fa fa-cog facolor setting_btn"></i> 
                  </a>
                </li>
               <!--  <li><i class="fa fa-cog facolor"></i> Quick Tools</li> -->
              </ul>
            </td>
          </tr>
          <tr  id="Snacks_focus_id" ng-repeat="(key, value) in diary.meal_two_food  | orderBy:'-id'">
             <td data-title="Food" class="first-td-border">
             <a href="javascript:void(0);" ng-click="diary.deleteLoggedFood(value)">
               <i class="fa fa-times-circle facolor"></i>
             </a>
              @{{value.name}}
             </td>
             <td data-title="CALORIES" class="text-center black_text">@{{value.calories}}</td>
             <td data-title="DESCRIPTION" class="text-left black_text">
                Fat: @{{value.fat}}, Fiber: @{{value.fiber}}, Carb: @{{value.carb}}, Sodium: @{{value.sodium}}, Protein: @{{value.protein}}
              </td>
          </tr>
          <tr class="total_tr">
             <td class="first-td-border">TOTAL</td>
             <td data-title="CALORIES" class="text-center">@{{diary.meal_two_total.calorie || 0}}</td>
             <td class="" data-title="DESCRIPTION">&nbsp;</td>
          </tr>
          <!-- end: Meal 2 (Snack) -->

          <!-- Meal 3 (Lunch) -->
          <tr class="text-center toptr-border">
            <td data-title=" " colspan="7" class="sidecolor text-leftside withborder head_slide_foodtbl_td">
              
              <ul class="list-inline">
                <li>
                  <h4 id="Meal 3(Lunch)">Meal 3 (Lunch)</h4>
                </li>
                <li>
                   
                  <a title="Add Food" ui-sref="log_food({ food_type : 'meal_3'})" class="add_food" id="add_lunch"  >
                    <i class="fa fa-plus-circle facolor add_plus_btn"></i>
                  </a> 
                </li>
                <li>
                  
                  <a ng-if="diary.meal_three_food.length > 0" title="Save Meal" ui-sref="create_meal({ food_type: 'meal_3', meal_food: diary.meal_three_food, meal_food_total: diary.meal_three_total })" class="add_food">
                   <i class="fa fa-cog facolor setting_btn"></i> 
                  </a>
                </li>
              </ul>
            </td>
          </tr>
          <tr ng-repeat="(key, value) in diary.meal_three_food | orderBy:'-id'">
             <td data-title="Food" class="first-td-border">
             <a href="javascript:void(0);" ng-click="diary.deleteLoggedFood(value)">
               <i class="fa fa-times-circle facolor"></i>
             </a>
              @{{value.name}}
             </td>
             <td data-title="CALORIES" class="text-center black_text">@{{value.calories}}</td>
             <td data-title="DESCRIPTION" class="text-left black_text">
                Fat: @{{value.fat}}, Fiber: @{{value.fiber}}, Carb: @{{value.carb}}, Sodium: @{{value.sodium}}, Protein: @{{value.protein}}
              </td>
          </tr>         
          <tr class="total_tr">
           <td  class="first-td-border">TOTAL</td>
           <td  data-title="CALORIES" class="text-center">@{{diary.meal_three_total.calorie || 0}}</td>
           <td class="" data-title="DESCRIPTION">&nbsp;</td>
          </tr>
          <!-- end: Meal 3 (Lunch) -->

          <!-- Meal 4 (Snack) -->
          <tr class="text-center toptr-border">
            <td data-title=" " colspan="7" class="sidecolor text-leftside withborder head_slide_foodtbl_td">
              
              <ul class="list-inline">
                <li>
                  <h4 id="Meal 4 (Snack)">Meal 4 (Snack)</h4>
                </li>
                <li>
                   
                  <a title="Add Food" ui-sref="log_food({ food_type : 'meal_4'})" class="add_food" id="add_dinner"  >
                    <i class="fa fa-plus-circle facolor add_plus_btn"></i>
                  </a> 
                </li>
                <li>
                  
                  <a ng-if="diary.meal_four_food.length > 0" ui-sref="create_meal({ food_type: 'meal_4', meal_food: diary.meal_four_food, meal_food_total: diary.meal_four_total })" class="add_food"  >
                   <i class="fa fa-cog facolor setting_btn"></i> 
                  </a>
                </li>
               <!--  <li><i class="fa fa-cog facolor"></i> Quick Tools</li> -->
              </ul>
            </td>
          </tr>
          <tr ng-repeat="(key, value) in diary.meal_four_food | orderBy:'-id'">
           <td data-title="Food" class="first-td-border">
           <a href="javascript:void(0);" ng-click="diary.deleteLoggedFood(value)">
             <i class="fa fa-times-circle facolor"></i>
           </a>
            @{{value.name}}
           </td>
           <td data-title="CALORIES" class="text-center black_text">@{{value.calories}}</td>
           <td data-title="DESCRIPTION" class="text-left black_text">
              Fat: @{{value.fat}}, Fiber: @{{value.fiber}}, Carb: @{{value.carb}}, Sodium: @{{value.sodium}}, Protein: @{{value.protein}}
            </td>
          </tr>
          <tr class="total_tr">
           <td  class="first-td-border">TOTAL</td>
           <td  data-title="CALORIES" class="text-center">@{{diary.meal_four_total.calorie || 0}}</td>
           <td class="" data-title="DESCRIPTION">&nbsp;</td>
          </tr>

          <!-- Meal 5 (Dinner) -->
          <tr class="text-center toptr-border">
              <td data-title=" " colspan="7" class="sidecolor text-leftside withborder head_slide_foodtbl_td">

                  <ul class="list-inline">
                      <li>
                          <h4 id="Meal 5 (Dinner)">Meal 5 (Dinner)</h4>
                      </li>
                      <li>

                          <a title="Add Food" ui-sref="log_food({ food_type : 'meal_5'})" class="add_food" id="add_dinner"  >
                              <i class="fa fa-plus-circle facolor add_plus_btn"></i>
                          </a>
                      </li>
                      <li>

                          <a ng-if="diary.meal_five_food.length > 0" ui-sref="create_meal({ food_type: 'meal_5', meal_food: diary.meal_five_food, meal_food_total: diary.meal_five_total })" class="add_food"  >
                              <i class="fa fa-cog facolor setting_btn"></i>
                          </a>
                      </li>
                      <!--  <li><i class="fa fa-cog facolor"></i> Quick Tools</li> -->
                  </ul>
              </td>
          </tr>
          <tr ng-repeat="(key, value) in diary.meal_five_food | orderBy:'-id'">
              <td data-title="Food" class="first-td-border">
                  <a href="javascript:void(0);" ng-click="diary.deleteLoggedFood(value)">
                      <i class="fa fa-times-circle facolor"></i>
                  </a>
                  @{{value.name}}
              </td>
              <td data-title="CALORIES" class="text-center black_text">@{{value.calories}}</td>
              <td data-title="DESCRIPTION" class="text-left black_text">
                  Fat: @{{value.fat}}, Fiber: @{{value.fiber}}, Carb: @{{value.carb}}, Sodium: @{{value.sodium}}, Protein: @{{value.protein}}
              </td>
          </tr>
          <tr class="total_tr">
              <td  class="first-td-border">TOTAL</td>
              <td  data-title="CALORIES" class="text-center">@{{diary.meal_five_total.calorie || 0}}</td>
              <td class="" data-title="DESCRIPTION">&nbsp;</td>
          </tr>

          <!-- Meal 6 (Snack - Optional) -->
          <tr class="text-center toptr-border">
              <td data-title=" " colspan="7" class="sidecolor text-leftside withborder head_slide_foodtbl_td">

                  <ul class="list-inline">
                      <li>
                          <h4 id="Meal 6 (Snack - Optional)">Meal 6 (Snack - Optional)</h4>
                      </li>
                      <li>

                          <a title="Add Food" ui-sref="log_food({ food_type : 'meal_6'})" class="add_food" id="add_dinner"  >
                              <i class="fa fa-plus-circle facolor add_plus_btn"></i>
                          </a>
                      </li>
                      <li>

                          <a ng-if="diary.meal_six_food.length > 0" ui-sref="create_meal({ food_type: 'meal_6', meal_food: diary.meal_six_food, meal_food_total: diary.meal_six_total })" class="add_food"  >
                              <i class="fa fa-cog facolor setting_btn"></i>
                          </a>
                      </li>
                      <!--  <li><i class="fa fa-cog facolor"></i> Quick Tools</li> -->
                  </ul>
              </td>
          </tr>
          <tr ng-repeat="(key, value) in diary.meal_six_food | orderBy:'-id'">
              <td data-title="Food" class="first-td-border">
                  <a href="javascript:void(0);" ng-click="diary.deleteLoggedFood(value)">
                      <i class="fa fa-times-circle facolor"></i>
                  </a>
                  @{{value.name}}
              </td>
              <td data-title="CALORIES" class="text-center black_text">@{{value.calories}}</td>
              <td data-title="DESCRIPTION" class="text-left black_text">
                  Fat: @{{value.fat}}, Fiber: @{{value.fiber}}, Carb: @{{value.carb}}, Sodium: @{{value.sodium}}, Protein: @{{value.protein}}
              </td>
          </tr>
          <tr class="total_tr">
              <td  class="first-td-border">TOTAL</td>
              <td  data-title="CALORIES" class="text-center">@{{diary.meal_six_total.calorie || 0}}</td>
              <td class="" data-title="DESCRIPTION">&nbsp;</td>
          </tr>

          <!-- Pre Workout Meal -->
          <tr class="text-center toptr-border">
              <td data-title=" " colspan="7" class="sidecolor text-leftside withborder head_slide_foodtbl_td">

                  <ul class="list-inline">
                      <li>
                          <h4 id="Pre Workout Meal">Pre Workout Meal</h4>
                      </li>
                      <li>

                          <a title="Add Food" ui-sref="log_food({ food_type : 'pre_workout_meal'})" class="add_food" id="add_dinner"  >
                              <i class="fa fa-plus-circle facolor add_plus_btn"></i>
                          </a>
                      </li>
                      <li>

                          <a ng-if="diary.pre_workout_food.length > 0" ui-sref="create_meal({ food_type: 'pre_workout_meal', meal_food: diary.pre_workout_food, meal_food_total: diary.pre_workout_total })" class="add_food"  >
                              <i class="fa fa-cog facolor setting_btn"></i>
                          </a>
                      </li>
                      <!--  <li><i class="fa fa-cog facolor"></i> Quick Tools</li> -->
                  </ul>
              </td>
          </tr>
          <tr ng-repeat="(key, value) in diary.pre_workout_food | orderBy:'-id'">
              <td data-title="Food" class="first-td-border">
                  <a href="javascript:void(0);" ng-click="diary.deleteLoggedFood(value)">
                      <i class="fa fa-times-circle facolor"></i>
                  </a>
                  @{{value.name}}
              </td>
              <td data-title="CALORIES" class="text-center black_text">@{{value.calories}}</td>
              <td data-title="DESCRIPTION" class="text-left black_text">
                  Fat: @{{value.fat}}, Fiber: @{{value.fiber}}, Carb: @{{value.carb}}, Sodium: @{{value.sodium}}, Protein: @{{value.protein}}
              </td>
          </tr>
          <tr class="total_tr">
              <td  class="first-td-border">TOTAL</td>
              <td  data-title="CALORIES" class="text-center">@{{diary.pre_workout_total.calorie || 0}}</td>
              <td class="" data-title="DESCRIPTION">&nbsp;</td>
          </tr>

          <!-- Post Workout Meal -->
          <tr class="text-center toptr-border">
              <td data-title=" " colspan="7" class="sidecolor text-leftside withborder head_slide_foodtbl_td">

                  <ul class="list-inline">
                      <li>
                          <h4 id="Post Workout Meal">Post Workout Meal</h4>
                      </li>
                      <li>

                          <a title="Add Food" ui-sref="log_food({ food_type : 'post_workout_meal'})" class="add_food" id="add_dinner"  >
                              <i class="fa fa-plus-circle facolor add_plus_btn"></i>
                          </a>
                      </li>
                      <li>

                          <a ng-if="diary.dinnerFood.length > 0" ui-sref="create_meal({ food_type: 'post_workout_meal', meal_food: diary.post_workout_food, meal_food_total: diary.post_workout_total })" class="add_food"  >
                              <i class="fa fa-cog facolor setting_btn"></i>
                          </a>
                      </li>
                      <!--  <li><i class="fa fa-cog facolor"></i> Quick Tools</li> -->
                  </ul>
              </td>
          </tr>
          <tr ng-repeat="(key, value) in diary.post_workout_food | orderBy:'-id'">
              <td data-title="Food" class="first-td-border">
                  <a href="javascript:void(0);" ng-click="diary.deleteLoggedFood(value)">
                      <i class="fa fa-times-circle facolor"></i>
                  </a>
                  @{{value.name}}
              </td>
              <td data-title="CALORIES" class="text-center black_text">@{{value.calories}}</td>
              <td data-title="DESCRIPTION" class="text-left black_text">
                  Fat: @{{value.fat}}, Fiber: @{{value.fiber}}, Carb: @{{value.carb}}, Sodium: @{{value.sodium}}, Protein: @{{value.protein}}
              </td>
          </tr>
          <tr class="total_tr">
              <td  class="first-td-border">TOTAL</td>
              <td  data-title="CALORIES" class="text-center">@{{diary.post_workout_total.calorie || 0}}</td>
              <td class="" data-title="DESCRIPTION">&nbsp;</td>
          </tr>

         <!-- start: exercise code -->

          <!-- CARDIOVASCULAR -->
          <tr class="text-center toptr-border">
            <td data-title=" " colspan="7" class="sidecolor text-leftside withborder head_slide_foodtbl_td">
              
              <ul class="list-inline">
                <li><h4 id="cardio">EXERCISE CARDIOVASCULAR</h4> </li>
                <li>
                  <a title="Add Exercise" ui-sref="log_exercise({ exercise_type : 'cardio'})" class="add_exercise">
                    <i class="fa fa-plus-circle facolor add_plus_btn"></i> 
                  </a>
                  </li>
              </ul>
            </td>
          </tr>
          <tr ng-repeat="(key, value) in diary.cardioExercises | orderBy:'-id'">
             <td data-title="Exercise" class="first-td-border">
             <a href="javascript:void(0);" ng-click="diary.deleteExercise(value)">
               <i class="fa fa-times-circle facolor"></i>
             </a>
              @{{value.name}}
             </td>
             <td data-title="CALORIES" class="text-center black_text ">@{{value.calories_burned}}</td>
             <td data-title="DESCRIPTION" class="text-left black_text">
                Minutes: @{{value.time}}
              </td>
          </tr>
          <tr class="total_tr">
           <td  class="first-td-border">TOTAL</td>
           <td  data-title="CALORIES" class="text-center">@{{diary.totalCardio.calories_burned || 0}}</td>
           <td data-title="DESCRIPTION">&nbsp;</td>
          </tr>
          <!-- end: CARDIOVASCULAR -->

          <!-- RESISTANCE TRAINING -->
          <tr class="text-center toptr-border">
            <td data-title=" " colspan="7" class="sidecolor text-leftside withborder head_slide_foodtbl_td">

              <ul class="list-inline">
                <li>
                    <h4 id="resistance">EXERCISE RESISTANCE TRAINING</h4>
                    
                </li>
                <li>
                  <a title="Add Exercise" ui-sref="log_exercise({ exercise_type : 'resistance'})" class="add_exercise">
                    <i class="fa fa-plus-circle facolor add_plus_btn"></i> 
                  </a>
                  </li>
              </ul>
              <span class="text_font_size">
                           (Categories: Olympic Weightlifting, Plyometrics, Powerlifting
                            Strength, Stretching, Strongman) 
                    </span>
            </td>
          </tr>
          <tr ng-repeat="(key, value) in diary.resistanceExercises | orderBy:'-id'">
             <td data-title="Exercise" class="first-td-border">
             <a href="javascript:void(0);" ng-click="diary.deleteExercise(value)">
               <i class="fa fa-times-circle facolor"></i>
             </a>
               @{{value.name}}
             </td>
             <td data-title="CALORIES" class="text-center black_text">@{{value.calories_burned}}</td>
             <td data-title="DESCRIPTION" class="text-left black_text">
                Minutes: @{{value.time}}, SETS: @{{value.sets}}, REPS: @{{value.reps}}, Weight(lbs): @{{value.weight_lbs}}
              </td>
          </tr>
          <tr class="total_tr">
           <td  class="first-td-border">TOTAL</td>
           <td  data-title="CALORIES" class="text-center">@{{diary.totalResistance.calories_burned || 0}}</td>
           <td data-title="DESCRIPTION">&nbsp;</td>
          </tr>
          <!-- end: RESISTANCE TRAINING -->

        <!-- end: exercise code -->
        
      </tbody>
    </table>
</div>

<div class="row" >
  <div class="col-xs-12 text-center">
    When you are finished logging all foods and exercise for this day, click here:<br/>
    <button class="btn btn-primary btndesign"  ng-click="diary.addFoodCompleted()">Complete This Entry</button>
  </div>
</div>

<div class="row food_last_row headrow-samecolumnheight">
  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
    <div class="row">
      <div class="col-xs-9 col-sm-9 col-md-9">
          <h4>WATER CONSUMPTION</h4>
            We recommend that you drink at least 8 cups of water a day. <br/>
            Click the arrows to add or subtract cups of water.
      </div>
      <div class="col-xs-3 col-sm-3 col-md-3 text-right">
        <ul class="list-unstyled centertext margin_bot">
          
          <li>
            <div style="width:93px">
              <div>
                <a href="javascript:void(0)" ng-click="diary.addWaterIntake()"><i class="fa fa-caret-up fa-2x"></i></a>
              </div>
              <div>
              <span id="water_intake" ng-bind="diary.water_intake"></span>
              <img id="glass" src="images/glass.jpg" width="" height="">
              </div>
              <div>
                <i ng-if="diary.water_intake<=0" class="fa fa-caret-down fa-2x fa-caret-up-down-disable"></i>
                <a ng-if="diary.water_intake>0" href="javascript:void(0)" ng-click="diary.deleteWaterIntake()"><i class="fa fa-caret-down fa-2x"></i></a>
              
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
    
  </div>
  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 second_row_foot">
    <div class="row footnoterow">
      <div class="col-xs-12 col-sm-12 col-md-12">
        <h4>
          TODAY'S NOTES  
          <small class="pull-right"><a href="#">Edit Note <i class="fa fa-pencil"></i></a></small>
        </h4>
        <!-- <textarea class="form-control textcolor" ng-disabled="true" rows="4"></textarea>  -->
          <a href="javascript:void(0);" editable-textarea="diary.notes" e-rows="20" e-cols="40" onaftersave="diary.update_notes();">
          <pre class="food_notes">@{{ diary.notes || 'no description' }}</pre>
        </a> 
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 text-center">
    <button class="btn btn-primary btndesign">View Full Report</button>
    <button class="btn btn-primary btndesign"><i class="fa fa-print"></i> Print Full Report</button>
  </div>
</div>


 

  