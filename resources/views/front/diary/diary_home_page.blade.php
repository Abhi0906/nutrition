<div class="row food_exe_stick">
	@include('front.diary.diary-sticky-top')
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
	  
  @include('front.diary.food-exercise-data')

  <div class="saperator"></div>
   
  @include('front.diary.calorie-summary')
</div>