<div class="col-lg-9">
	<div class="row row_flex_disp">
	    <div class="col-sm-7 text-left">
	    	<span ng-if="diary.diary_summary_loader" class="diary_summary_loader"></span>
	      <ul class="list-inline foodExe_diayUL">
	          <li>
	            <h5>@{{diary.foodSummary.calorie_goal * 1 || 0}}<br><small>Goal</small></h5>
	          </li>
	          <li>
	            <h5>-</h5>
	          </li>
	          <li>
	            <h5>@{{diary.foodSummary.total_consumed * 1 || 0}}<br><small>Calories</small></h5>
	          </li>
	          <li>
	            <h5>+</h5>
	          </li>
	          <li>
	            <h5>@{{diary.exerciseSummary.total_burned * 1 || 0}}<br><small>Exercise</small></h5>
	          </li>
	          <li>
	            <h5>=</h5>
	          </li>
	          <li>
	            <h5><span class="text-primary">@{{ (diary.foodSummary.calorie_goal * 1) - (diary.foodSummary.total_consumed * 1)  + (diary.exerciseSummary.total_burned ) || 0 }}</span><br><small>Remaining</small></h5>
	          </li>
	      </ul>
	    </div>
	    <div class="col-sm-5 align_Vend">
	      <div class="datepicker_contain_part">
	        <ul class="list-inline btn-ul-date datepicker_ul custom-datepicker_ul">
	          <li><button class="btn btn-primary btndesign btn-arrow" ng-click="diary.prevFoodDairy();"><span class="fa fa-chevron-left"></span></button></li>
	          <li>
	            
	              <div class="input-group date sleepboxdatepicker big-date-width">
	                       <input type="text"
	                         class="form-control" 
	                         uib-datepicker-popup = "@{{diary.format}}"
	                          ng-model="diary.log_date"
	                          is-open="diary.food_date_popup.opened"
	                          datepicker-options="diary.dateOptions"
	                          ng-required="true"
	                          ng-change="diary.changeDate();"
	                          show-button-bar=false
	                          close-text="Close" />
	                           <span class="input-group-addon caledar_custom" ng-click="diary.food_date_open()"> 
	                              <i aria-hidden="true" class="fa fa-calendar"></i>
	                            </span>
	                    </div> 
	          </li>
	          <li><button class="btn btn-primary btndesign btn-arrow" ng-click="diary.nextFoodDairy();"><span class="fa fa-chevron-right"></span></button></li>
	        </ul>
	      </div>
	    </div>
  	</div> 
</div>
<div class="col-lg-3">
</div>