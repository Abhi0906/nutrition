@extends('layout.app')

@section('content')
<div class="row" ng-controller="FoodController as food" ng-init="food.userId='{{$user_id}}';food.loggedFood();food.getWaterIntake();">
	<div class="col-md-6 col-xs-12">
		<form name="LogFoodForm" novalidate="novalidate" class="animation-fadeInQuick">
		<div class="block full">
			<div class="block-title">
			
					<h3 class="widget-options pull-right">
				        <div class="btn-group btn-group-xs">
				            <a href="/diary?viewreports=food" class="btn btn-default" data-toggle="tooltip" title="" data-original-title="Reports">View Reports</a>
				        </div> 
				    </h3>
			    	<h3>
				        <i class="gi gi-cutlery"></i>&nbsp;&nbsp;Log Food
				    </h3>
			</div>
			<div class="widget-extra">
			    <div class="row">
			       <div class="col-xs-12">
                        <h5><label for="search_food" class="text-primary">Enter food:</label> 
                        	<a href="javascript:void(0)" class="clear-search-btn pull-right" ng-click="food.openCustomFoodForm()" >Create Custom Food</a>
                        </h5>
                    	
                        <form name="searchFoodForm">
	                        <div class="input-group">
	                            <input type="text" size="37" ng-model="food.searchKeyFood" id="search_food" class="form-control" placeholder="Search food" autocomplete="off">
	                            <span class="input-group-btn">
	                                <button type="submit" class="btn btn-primary" ng-click="food.searchFood();"><i class="fa fa-search"></i>&nbsp;Search</button>
	                            </span>
	                            <a href="javascript:void(0)" class="clear-search-btn pull-right" ng-click="food.clear_logFood_searchResult()" >Clear</a>
	                        </div>
                          </form>
                        <div class="dd-menu" ng-show="food.searchMenu">
	                        <ul class="dd-dataset">
				                <li ng-repeat="data in food.foodSearchdata | filter:food.new_nutrition.searchFood">
				                    <p ng-click="food.selectedFood(data, true);" class="dd-suggestion"><strong>@{{data.name}} </strong> <br><small>@{{data.brand_name}},@{{data.nutrition_data.serving_quantity}}  @{{data.nutrition_data.serving_size_unit}} ,@{{data.nutrition_data.calories}} cal</small>
				                    </p>
				                </li>
				                <li ng-show="food.foodSearchdata.length==0">
				                	<p class="text-center text-primary"><strong>No Results found.</strong></p>
				                </li>
				            </ul>
			        	</div>
                   		<h5></h5>
                   		<div class="alert alert-success animation-fadeInQuick" ng-show="food.showMessage">
                   			<span>Food was added to your food log below.</span>
                   		</div>
                    </div>
                    <div class="col-xs-12" ng-show="food.searchFoodTabs" id="freq_rec_tabs">
                    	<!-- <ul class="nav nav-tabs nav-justified" > -->
	                    <ul class="nav nav-tabs logfood-tab">
                    	  <li role="presentation" id="favourite_btn" ng-click="food.frequent_recent_toggle('favourite')" class="active frequent">
					  	    <a href="javascript:void(0)">Favourite</a></li>
						  <li role="presentation" id="frequent_b	tn" ng-click="food.frequent_recent_toggle('frequent')">
						  	<a href="javascript:void(0)">Frequent</a></li>
						  <li role="presentation" id="recent_btn" ng-click="food.frequent_recent_toggle('recent')">
						  	<a href="javascript:void(0)" >Recent</a></li>
						  <li role="presentation" id="custom_btn" ng-click="food.frequent_recent_toggle('custom')">
						  	<a href="javascript:void(0)" >Custom</a></li>
						</ul>
						
                        <div id="favourite_logged_food" ng-show="food.favourite_logged_div" >
                        	<table class="table" >
				        		<tbody>
									<tr ng-repeat="favourite_food in food.logged_food.favourite">
										<td>
											<div  ng-click="food.selectedFood(favourite_food.nutritions, true);" class="dd-suggestion">
				                        		<strong>@{{favourite_food.nutritions.name}} </strong> <br><small>@{{favourite_food.nutritions.nutrition_brand.name}}, @{{favourite_food.nutritions.serving_quantity}}  @{{favourite_food.nutritions.serving_size_unit}}
								            </div>
										</td>
									</tr>
									<tr ng-show="food.logged_food.favourite.length==0">
										<td>
											<div class="alert alert-info" >
												<span>No favourite food available</span>
											</div>											
										</td>
									</tr>
								</tbody>
				        	</table>
                        </div>

                        <div id="frequently_logged_food" ng-show="food.frequently_logged_div" >
                        	<table class="table" >
				        		<tbody>
									<tr ng-repeat="frequent_food in food.logged_food.frequent">
										<td>
											<div  ng-click="food.selectedFood(frequent_food, false);" class="dd-suggestion">
				                        		<strong>@{{frequent_food.name}} </strong> <br><small>@{{frequent_food.brand_name}}, @{{frequent_food.serving_quantity}}  @{{frequent_food.serving_size_unit}}
								            </div>
										</td>
									</tr>
									<tr ng-show="food.logged_food.frequent.length==0">
										<td>
											<div class="alert alert-info" >
												<span>No frequent food available</span>
											</div>											
										</td>
									</tr>
								</tbody>
				        	</table>
                        </div>
	                    
	                    <div id="recently_logged_food" ng-show="food.recently_logged_div" >
							<table class="table"  >
								<tbody>
									<tr ng-repeat="recent_food in food.logged_food.recent">
										<td>
											<div ng-click="food.selectedFood(recent_food, false);" class="dd-suggestion">
											<strong>@{{recent_food.name}} </strong> <br><small>@{{recent_food.brand_name}}, @{{recent_food.serving_quantity}}  @{{recent_food.serving_size_unit}}
											</div>
										</td>
									</tr>
									<tr ng-show="food.logged_food.recent.length==0">
										<td>
											<div class="alert alert-info" >
												<span>No recent food available</span>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
	                    </div>

	                    <div id="custom_logged_food" ng-show="food.custom_logged_div" >
							<table class="table"  >
								<tbody>
									<tr ng-repeat="custom_food in food.logged_food.custom">
										<td>
											<div ng-click="food.selectedFood(custom_food.nutritions, true);" class="dd-suggestion">
											<strong>@{{custom_food.nutritions.name}} </strong> <br><small>@{{custom_food.nutritions.nutrition_brand.name}}, @{{custom_food.nutritions.serving_quantity}}  @{{custom_food.nutritions.serving_size_unit}}
											</div>
										</td>
									</tr>
									<tr ng-show="food.logged_food.custom.length==0">
										<td>
											<div class="alert alert-info" >
												<span>No custom food available</span>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
	                    </div>
                    </div>
			    </div>
				<span ng-show="food.log_loader"  class="log_food_loader"></span>
			    <div class="row" ng-show="food.displayFoodsection">
			    	<div class="col-xs-12">
				    	<div class="well well-sm">
				    		<span>@{{food.new_nutrition.name}}</span><br>
				    		<small>@{{food.new_nutrition.brand_name}}, @{{food.new_nutrition.nutrition.nutrition_data.serving_quantity}}&nbsp;@{{food.new_nutrition.nutrition.nutrition_data.serving_size_unit}}</small>
				        </div>
				    </div>
			    </div>
			    <div class="row" ng-show="food.displayFoodsection">
	                <div class="col-xs-6 col-md-3">
	                	<label class="text-primary">Serving Size:</label>
	                </div>
	                <div class="col-xs-6 col-md-4">
	                	<span>@{{food.new_nutrition.nutrition.nutrition_data.serving_quantity}}&nbsp;@{{food.new_nutrition.nutrition.nutrition_data.serving_size_unit}}</span>
	                </div>
	            </div>
	            <h5></h5>
	            <div class="row" ng-show="food.displayFoodsection">
	            	<div class="col-xs-6 col-md-3">
	            		<label class="text-primary">Number of Servings</label>
	            	</div>
	               
	                <div class="col-xs-6 col-md-4">
						    <input type="text"  ng-blur="food.valid_num_of_serving_food(food.new_nutrition.no_of_servings);"
		                           				ng-keyup="food.valid_num_of_serving_food(food.new_nutrition.no_of_servings);"
		                           				maxlength="4"		                           				  
							                    numbers-only="numbers-only"
							                    class="form-control input-xs" ng-model="food.new_nutrition.no_of_servings" required>
							<span class="text-danger" ng-if="food.log_food_error">
								<small>&nbsp; Please enter valid amount</small>
							</span>
	                </div>
                </div>
                <h5></h5>       
				<div class="row" ng-show="food.displayFoodsection">
	                <div class="col-xs-6 col-md-3"><label class="text-primary">Select a meal</label></div>
	                <div class="col-xs-6 col-md-4">
	                	<select ng-model="food.new_nutrition.schedule_time" ng-options="time.name as time.name for time in food.schedule_times" class="form-control" required>            
	                    </select>
	                </div>
                </div>
                
                <h5></h5>       
				<div class="row" ng-show="food.displayFoodsection">
	                <div class="col-xs-6 col-md-3"><label class="text-primary">Select Food taken date</label></div>
	                <div class="col-xs-6 col-md-4">
	                	<input-datepicker format="MM dd, yyyy" date="food.new_nutrition.serving_date"></input-datepicker>
	                </div>
	                
                </div>
                <h5></h5>       
				<div class="row" ng-show="food.displayFoodsection">
					<div class="col-xs-12 col-md-12">
	                	<button type="button" class="btn btn-primary pull-right" ng-disabled="LogFoodForm.$invalid || food.log_food_error" ng-click="food.logFood();">Log Food</button>
	                	<span class="pull-right">&nbsp;&nbsp;</span>
	                	<button type="button" class="btn btn-primary pull-right btn-success"  ng-click="food.logFavouriteFood();" ng-if="!food.is_favourite">Add To Favourite</button>
	                	<button type="button" class="btn btn-primary pull-right btn-danger"  ng-click="food.removeFoodFromFav();" ng-if="food.is_favourite">Remove From Favourite</button>
	                </div> 
				</div>
			    <h5></h5>
			    <div class="row" ng-show="food.displayFoodsection">
			    	<div class="col-xs-12 animation-fadeInQuick">
			                <h5 class="text-center uppercase">Nutrition Facts</h5>
		                <div class="row">
							<table class="table table-striped table-vcenter" id="nutrition-facts">
								<colgroup>
									<col class="col-4">
									<col class="col-2">
									<col class="col-4">
									<col class="col-2">
								</colgroup>
								<tbody>
									<tr>
										<td class="col-4">Calories (g)</td>
										<td class="col-2">
											<span>
												@{{food.nutrition_info.nutrition_data.calories * food.new_nutrition.no_of_servings}}
											</span>
											
										</td>
										<td class="col-4">Monounsaturated (g)</td>
										<td class="col-2">
											<span>
												@{{food.nutrition_info.nutrition_data.monounsaturated}}
											</span>
										</td>
									</tr>
									<tr>
										<td class="col-4">Total Fat (g)</td>
										<td class="col-2">
											<span>
												@{{food.nutrition_info.nutrition_data.total_fat}}
											</span>
										</td>
										<td class="col-4">Calcium</td>
										<td class="col-2">
											<span>
												@{{food.nutrition_info.nutrition_data.calcium_dv}}
											</span>
										</td>
									</tr>
									<tr>
										<td class="col-4">Saturated Fat (g)</td>
										<td class="col-2">
											<span>
												@{{food.nutrition_info.nutrition_data.saturated_fat}}
											</span>
											
										</td>
										<td class="col-4">Trans Fat (g)</td>
										<td class="col-2">
											<span>
												@{{food.nutrition_info.nutrition_data.trans_fat}}
											</span>
										</td>
									</tr>

									<tr>
										<td class="col-4 sub">Cholesterol (mg)</td>
										<td class="col-2">
											<span>
												@{{food.nutrition_info.nutrition_data.cholesterol}}
											</span>
										</td>
										<td class="col-4">Sodium (mg)</td>
										<td class="col-2">
											<span>
												@{{food.nutrition_info.nutrition_data.sodium }}
											</span>
											
										</td>
									</tr>

									<tr>
										<td class="col-4">Potassium (mg)</td>
										<td class="col-2">
											<span>
												@{{food.nutrition_info.nutrition_data.potassium}}
											</span>
											
										</td>
										<td class="col-4">Total Carbohydrates (g)</td>
										<td class="col-2">
											<span>
												@{{food.nutrition_info.nutrition_data.total_carb}}
											</span>
											
										</td>
									</tr>

									<tr>
										<td class="col-4">Dietary fiber (g)</td>
										<td class="col-2">
											<span>
												@{{food.nutrition_info.nutrition_data.dietary_fiber}}
											</span>
											
										</td>
										<td class="col-4">Sugars (g)</td>
										<td class="col-2">
											<span>
												@{{food.nutrition_info.nutrition_data.sugars}}
											</span>
											
										</td>
									</tr>

									<tr>
										<td class="col-4 sub">Protein</td>
										<td class="col-2">
											<span>
												@{{food.nutrition_info.nutrition_data.protein}}
											</span>
											
										</td>
										<td class="col-4">Vitamin A</td>
										<td class="col-2">
											<span>
												@{{food.nutrition_info.nutrition_data.vitamin_a}}
											</span>
											
										</td>
									</tr>

									<tr class="last">
										<td class="col-4">Vitamin C</td>
										<td class="col-2">
											<span>
												@{{food.nutrition_info.nutrition_data.vitamin_c}}
											</span>
											
										</td>
										<td class="col-4">Iron</td>
										<td class="col-2">
											<span>
												@{{food.nutrition_info.nutrition_data.iron_dv}}
											</span>
											
										</td>
									</tr>
								</tbody>
							</table>

							<div class="col-xs-12 col-md-12">
							  <small><sup class="text-danger">*</sup>Percent Daily Values are based on a 2000 calorie diet. Your daily values may be higher or lower depending on your calorie needs.</small>
							</div>
		                </div>
		            </div>
			    </div>

			    <!-- <div class="row">
			    	<div class="form-group form-actions">
			                            <div class="col-md-5 col-md-offset-7">
			                                <a href="#nutrition-info" data-toggle="modal" class="btn btn-success">Nutrition info</a>
			                                <button type="button" class="btn btn-primary" ng-click="food.logFood();">Log Food</button>
			                            </div>
			                        </div>
			    	&nbsp;&nbsp;
			    </div> -->
			</div>
		</div>
		</form>
	</div>
	<div class="col-md-6 col-xs-12">
		<div class="block full">
            <div class="block-title">
                <h4>
                   Daily Totals
                </h4>
            </div>
            <div class="widget-extra-full">
	            <div id="no-more-tables" class="table-responsive">
		            <table class="table table-striped">
		                <tbody>
		                	<tr>
		                        <td class="hidden-xs text-center">Calories</td>
		                        <td class="hidden-xs text-center">Fat</td>
		                        <td class="hidden-xs text-center">Fiber</td>
		                        <td class="hidden-xs text-center">Carbs</td>
		                        <td class="hidden-xs text-center">Sodium</td>
		                        <td class="hidden-xs text-center">Protein</td>
		                        <!-- <td class="hidden-xs text-center">Water</td> -->
		                    </tr>
		                    <tr>
		                        <td data-title="Calories" class="text-center text-muted">@{{food.daily_total.calories}}</td>
		                        <td data-title="Fat" class="text-center text-muted">@{{food.daily_total.calories_from_fat}}</td>
		                        <td data-title="Fiber" class="text-center text-muted">@{{food.daily_total.dietary_fiber}}</td>
		                        <td data-title="Carbs" class="text-center text-muted">@{{food.daily_total.total_carb}}</td>
		                        <td data-title="Sodium" class="text-center text-muted">@{{food.daily_total.sodium}}</td>
		                        <td data-title="Protein" class="text-center text-muted">@{{food.daily_total.protein}}</td>
		                        <!-- <td data-title="Water" class="text-center text-muted">5</td> -->

		                    </tr>
		                    <!-- <tr>
		                    	<td colspan="7">
		                    	<span class="text-muted">Daily Calorie Composition: 0% from carbs, 0% from fat and 0% from protein.</span>
		                    	</td>
		                    </tr> -->
		                </tbody>
		            </table>
		        </div>
            </div>
        </div>
		<div class="block full">
			<div class="block-title">
			    <!-- <div class="widget-options">
			        <div class="btn-group btn-group-xs">
			            <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="" data-original-title="Date"><i class="fa fa-calendar"></i></a>
			            <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="" data-original-title="Reports"><i class="fa fa-bar-chart"></i></a>
			        </div> 
			    </div> -->
			    <h3>
			        <i class="gi gi-cutlery"></i>&nbsp;&nbsp;Logged Foods Today
			    </h3>
			</div>
			<div class="widget-extra">
				<div class="col-xs-12" ng-cloak>
				    <br>
					<div class="alert alert-info" ng-cloak  ng-hide="food.logged_food.break_fast.length > 0 || food.logged_food.lunch.length > 0 || food.logged_food.dinner.length > 0 || food.logged_food.snacks.length > 0">
						<span>Food information not available.</span>
					</div>
				</div>
			    <div class="row">
			    	<div class="form-group col-xs-12" ng-show="food.logged_food.break_fast.length > 0">
                        <h4 class=""><label class="text-primary">Breakfast</label></h4>
                        <div id="no-more-tables" class="table-responsive">
			                <table class="table">
				                <tbody>
				                	<tr>
				                        <td class="hidden-xs text-center text-primary" width="40%">Food</td>
				                        <td class="hidden-xs text-center text-primary">Serving(s)</td>
				                        <td class="hidden-xs text-center text-primary">Calories</td>
				                        <td class="hidden-xs text-center text-primary">Fat</td>
				                        <td class="hidden-xs text-center text-primary">Fiber</td>
				                        <td class="hidden-xs text-center text-primary">Carbs</td>
				                        <td class="hidden-xs text-center text-primary">Sodium</td>
				                        <td class="hidden-xs text-center text-primary">Protein</td>
									</tr>
				                    <tr ng-repeat="(key, value) in food.logged_food.break_fast">
									    <td data-title="Food" class="text-left">@{{value.name}}<br><small>@{{value.brand_name}}, (@{{value.serving_quantity}} @{{value.serving_size_unit}})</small></td>
									    <td data-title="Serving(s)" class="text-center ">@{{value.no_of_servings}}</td>
				                        <td data-title="Calories" class="text-center ">@{{value.calories}}</td>
				                        <td data-title="Fat" class="text-center ">@{{value.calories_from_fat}}</td>
				                        <td data-title="Fiber" class="text-center ">@{{value.dietary_fiber}}</td>
				                        <td data-title="Carbs" class="text-center ">@{{value.total_carb}}</td>
				                        <td data-title="Sodium" class="text-center ">@{{value.sodium}}</td>
				                        <td data-title="Protein" class="text-center ">@{{value.protein}}</td>   
				                    </tr>
				                </tbody>
				            </table>
			        	</div>
                    </div>
                    <div class="form-group col-xs-12" ng-show="food.logged_food.lunch.length > 0">
                        <h4 class=""><label class="text-primary">Lunch</label></h4>
                        <div id="no-more-tables" class="table-responsive">
	                        <table class="table">
			                	<tbody>
				                	<tr>
				                        <td class="hidden-xs text-center text-primary" width="40%">Food</td>
				                        <td class="hidden-xs text-center text-primary">Serving(s)</td>
				                        <td class="hidden-xs text-center text-primary">Calories</td>
				                        <td class="hidden-xs text-center text-primary">Fat</td>
				                        <td class="hidden-xs text-center text-primary">Fiber</td>
				                        <td class="hidden-xs text-center text-primary">Carbs</td>
				                        <td class="hidden-xs text-center text-primary">Sodium</td>
				                        <td class="hidden-xs text-center text-primary">Protein</td>
									</tr>
				                    <tr ng-repeat="(key, value) in food.logged_food.lunch">
									    <td data-title="Food" class="text-left">@{{value.name}}<br><small>@{{value.brand_name}}, (@{{value.serving_quantity}} @{{value.serving_size_unit}})</small></td>
									    <td data-title="Serving(s)" class="text-center ">@{{value.no_of_servings}}</td>
				                        <td data-title="Calories" class="text-center ">@{{value.calories}}</td>
				                        <td data-title="Fat" class="text-center ">@{{value.calories_from_fat}}</td>
				                        <td data-title="Fiber" class="text-center ">@{{value.dietary_fiber}}</td>
				                        <td data-title="Carbs" class="text-center ">@{{value.total_carb}}</td>
				                        <td data-title="Sodium" class="text-center ">@{{value.sodium}}</td>
				                        <td data-title="Protein" class="text-center ">@{{value.protein}}</td>      
				                    </tr>
			                	</tbody>
		            		</table>
	        			</div>
                    </div>
                    <div class="form-group col-xs-12" ng-show="food.logged_food.dinner.length > 0">
                        <h4 class=""><label class="text-primary">Dinner</label></h4>
                        <div id="no-more-tables" class="table-responsive">
	                        <table class="table">
			                	<tbody>
				                	<tr>
				                        <td class="hidden-xs text-center text-primary" width="40%">Food</td>
				                        <td class="hidden-xs text-center text-primary">Serving(s)</td>
				                        <td class="hidden-xs text-center text-primary">Calories</td>
				                        <td class="hidden-xs text-center text-primary">Fat</td>
				                        <td class="hidden-xs text-center text-primary">Fiber</td>
				                        <td class="hidden-xs text-center text-primary">Carbs</td>
				                        <td class="hidden-xs text-center text-primary">Sodium</td>
				                        <td class="hidden-xs text-center text-primary">Protein</td>
									</tr>
				                    <tr ng-repeat="(key, value) in food.logged_food.dinner">
									    <td data-title="Food" class="text-left">@{{value.name}}<br><small>@{{value.brand_name}}, (@{{value.serving_quantity}} @{{value.serving_size_unit}})</small></td>
									    <td data-title="Serving(s)" class="text-center ">@{{value.no_of_servings}}</td>
				                        <td data-title="Calories" class="text-center ">@{{value.calories}}</td>
				                        <td data-title="Fat" class="text-center ">@{{value.calories_from_fat}}</td>
				                        <td data-title="Fiber" class="text-center ">@{{value.dietary_fiber}}</td>
				                        <td data-title="Carbs" class="text-center ">@{{value.total_carb}}</td>
				                        <td data-title="Sodium" class="text-center ">@{{value.sodium}}</td>
				                        <td data-title="Protein" class="text-center ">@{{value.protein}}</td>         
				                    </tr>
				                </tbody>
		            		</table>
	        			</div>
                    </div>
                    <div class="form-group col-xs-12" ng-show="food.logged_food.snacks.length > 0">
                        <h4 class=""><label class="text-primary">Snacks</label></h4>
                        <div id="no-more-tables" class="table-responsive">
	                        <table class="table">
			                	<tbody>
				                	<tr>
				                        <td class="hidden-xs text-center text-primary" width="40%">Food</td>
				                        <td class="hidden-xs text-center text-primary">Serving(s)</td>
				                        <td class="hidden-xs text-center text-primary">Calories</td>
				                        <td class="hidden-xs text-center text-primary">Fat</td>
				                        <td class="hidden-xs text-center text-primary">Fiber</td>
				                        <td class="hidden-xs text-center text-primary">Carbs</td>
				                        <td class="hidden-xs text-center text-primary">Sodium</td>
				                        <td class="hidden-xs text-center text-primary">Protein</td>
									</tr>
				                    <tr ng-repeat="(key, value) in food.logged_food.snacks">
									    <td data-title="Food" class="text-left">@{{value.name}}<br><small>@{{value.brand_name}}, (@{{value.serving_quantity}} @{{value.serving_size_unit}})</small></td>
									    <td data-title="Serving(s)" class="text-center ">@{{value.no_of_servings}}</td>
				                        <td data-title="Calories" class="text-center ">@{{value.calories}}</td>
				                        <td data-title="Fat" class="text-center ">@{{value.calories_from_fat}}</td>
				                        <td data-title="Fiber" class="text-center ">@{{value.dietary_fiber}}</td>
				                        <td data-title="Carbs" class="text-center ">@{{value.total_carb}}</td>
				                        <td data-title="Sodium" class="text-center ">@{{value.sodium}}</td>
				                        <td data-title="Protein" class="text-center ">@{{value.protein}}</td>        
				                    </tr>
				                </tbody>
				            </table>
	        			</div>
                    </div>
			    </div>
			</div>
		</div>
		<div class="block full">
			<div class="block-title">
				    <h3> <i class="fa fa-tint"></i>&nbsp;&nbsp;Water Log</h3>
			</div>	
			  <form name="LogWaterIntake">
				<div class="row">
						<div class="col-md-3 col-lg-3  col-sm-3 col-xs-12">
							<div><img width="130" ng-src=@{{food.img}} /></div>
						</div>

						<div class="col-md-6 col-lg-6  col-sm-6 col-xs-12">
							<h5  ng-show ="food.total_current_water"><b><span class="text-primary" >@{{food.total_current_water | number:2}} @{{food.total_unit}} &nbsp;</span></b> of water consumed </h5>
									<ul class="list-inline">
										<li>
											<div class="input-group">
							                    <input type="text"
							                     id="water_intake"
							                     ng-blur="food.valid_water_intake(food.water);"
		                           				 ng-keyup="food.valid_water_intake(food.water);"
							                     name="water_intake"
							                     ng-model="food.water" 
							                     class="form-control"
							                     maxlength="5"
							                     numbers-only="numbers-only" 
							                     placeholder="@{{food.placeholder}}" required>
							            	</div>
							            </li>
										<li><button class="btn btn-sm btn-primary log_it_button_Waterintake"  ng-disabled="!LogWaterIntake.$valid || food.log_water_error"  ng-click="food.water_log()" type="submit">Log it</button> 
										</li>
											<span class="text-danger" ng-if="food.log_water_error">
												<small><br> &nbsp; Please enter valid number</small>
											</span>										
						   			</ul>
						   			<ul class="list-inline">
						   				<li>
						   					<button class="btn btn-info unit_btn" id="unit_ml"  ng-click="food.log_oz('ml')"  type="button">ml</button>&nbsp;
						   		  			<button class="btn btn-info unit_btn" id="unit_cup"  ng-click="food.log_oz('cup')" type="button">cup</button> &nbsp;
						   		  			<button class="btn btn-info unit_btn active" id="unit_oz"  ng-click="food.log_oz('oz')"  type="button">US oz</button>
						   				</li>
						   			</ul>
						</div>

						<div class="col-md-3 col-lg-2  col-sm-3 col-xs-12">
								<span class="text-primary"><b>Target:- </b> </span> @{{food.water_intake | number:2 }}&nbsp;@{{food.total_unit}} </br></br>
					               	<?php /*<span class="text-primary"><b>Remaining:</b></span> @{{food.remaining_waterIntake}}&nbsp;@{{food.total_unit}} </br></br> */?>
				               		<span class="text-primary"><b>Logged:-</b></span>
				               		<div ng-repeat="line in food.waterlogged track by $index">
			        				  	   	@{{line.log_water | number:2 }} @{{line.unit}}
				                </div>
						</div>
			    </div>
			  </form>
	    </div>

	</div>
	<div id="log_weight_confirmation" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
		@include('template.add_custom_food')
	</div>
</div>
@endsection
@section('page_scripts')
{!! HTML::script("js/controllers/FoodController.js") !!}
{!! HTML::script("js/directives/inputDatepicker.js") !!}
@append