@extends('layout.master')
@section('title')
Challenges
@endsection 
@section('custom-css')
 <style type="text/css">
 

</style>
@endsection 
@section('content') 
<section class="section-panel custom-section-panel">
	<div class="row text-center text-primary">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h2 class="text-uppercase head_title_main">Your Ch<span class="title_head_line">alleng</span>es</h2>
      </div>
   	</div>
   	<div class="saperator"></div>
   	<div class="panel panel-primary weight_body_panel" 
   			ng-controller="ChallengeFrontController as challenge" ng-init="challenge.userId='{{Auth::user()->id}}'; challenge.getChallenges()">
   	   <div class="panel-heading panel_head_background"><strong>CHALLENGES</strong>
   	   </div>
   	   <div class="panel-body panel_Bodypart">
   			<div class="deal_box" ng-repeat="challenge_data in challenge.challenges | orderBy:'start_date'">
			   	<div class="row">
					<div class="col-sm-4 col-md-3">
				   		<div class="deal_img_box">
				   			<img ng-src="/challenge-images/@{{challenge_data.image}}"
				   				class="img-responsive img_deal">
				   		</div>
				   	</div>
				   	<div class="col-sm-8 col-md-9">
				   		<div class="deals_details">
				   			<h2 class="red_font">@{{challenge_data.title}} 
				   				<small ng-if="challenge_data.is_expired==1" class="black_text"><i>(Expired)</i></small>
				   			</h2>
				   			<p class="deals_descrpt">
				   				@{{challenge_data.description}} 
				   			</p>
				   			<div class="clearboth"></div>
				   			<hr class="hr_deal_chall">
				   			<div> 
				   				<div class="col-sm-2">
				   					<p class="chalanges_device">Allowed Devices:</p>
				   				</div>
				   				<div class="col-sm-10">
				   					<span class="chalanges_device_name" ng-repeat="allowed_devices in challenge_data.allowed_devices_arr">@{{allowed_devices}}</span>

				   				</div>
				   			</div>
				   			<div class="clearboth"></div>
				   			<hr class="hr_deal_chall">
				   			<div class="deals_tbl">
					   			<table class="table">
					   				<thead>
					   					<tr>
						   					<th>START DATE</th>
						   					<th>END DATE</th>
					   					</tr>
					   				</thead>
					   				<tbody>
					   					<tr>
					   						<td>@{{challenge_data.start_date | custom_date_format: 'dd-MMM-yyyy hh:mm a'}}</td>
					   						<td>@{{challenge_data.end_date | custom_date_format: 'dd-MMM-yyyy hh:mm a'}}</td>
					   					</tr>
					   				</tbody>
					   			</table>
				   			</div>
				   		</div>
				   	</div>
			   	</div>
				<hr class="hr_lightgray">
		   	</div>
	   </div>
   	</div>
</section>
@endsection
@section('js')
{!! HTML::script("js/controllers/ChallengeFrontController.js") !!}
@append
@section('custom-js')
@append