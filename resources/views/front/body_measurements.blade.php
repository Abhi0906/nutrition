@extends('layout.app')
@section('content')
<div class="row" ng-controller="BodyMeasurementsController as weight" ng-init="weight.userId='{{$user_id}}';weight.loggedWeight(150, 'seven-days'); ">
	<!-- Classic and Bars Chart -->
    <div class="col-xs-12">
        <!-- Classic Chart Block -->
        <div class="block full">
            <!-- Classic Chart Title -->
            <div class="block-title">
                <h2>Body Measurements</h2>
            </div>
            <!-- END Classic Chart Title -->
            <div class="col-md-12">
	            <button type="button" id="seven-days" class="btn btn-xs btn-info days-filter" ng-click="weight.loggedWeight(7, 'seven-days');">Last 7 Days</button>
	            <button type="button" id="thirty-days" class="btn btn-xs btn-info days-filter" ng-click="weight.loggedWeight(30, 'thirty-days');">Last 30 Days</button>
	            <button type="button" id="ninety-days" class="btn btn-xs btn-info days-filter" ng-click="weight.loggedWeight(90, 'ninety-days');">Last 90 Days</button>
	            <button type="button" id="one-eighty-days" class="btn btn-xs btn-info days-filter" ng-click="weight.loggedWeight(180, 'one-eighty-days');">Last 180 Days</button>
	            <button type="button" id="year" class="btn btn-xs btn-info days-filter" ng-click="weight.loggedWeight(365, 'year');">Last Year</button>
            </div>
     		<!-- amchart -->
            <div id="chartdiv"></div>
            <!-- end: amchart -->
        </div>
        <!-- END Classic Chart Block -->
    </div>
    <!-- END Classic and Bars Chart -->
	<div class="col-md-5 col-xs-12">
		<form name="LogWeightForm" novalidate="novalidate" class="animation-fadeInQuick">
			<div class="block full">
				<div class="block-title">
			    	<h3>
				        <i class="fa wellness-icon-weight"></i>&nbsp;&nbsp;Log Body Measurements Goal
				    </h3>
				</div>				
				<div class="widget-extra-full log-weight-form">
					<span ng-show="weight.log_loader"  class="log_weight_loader"></span>
					<div class="form-horizontal form-bordered">
						<div class="form-group">
		                  	{!! HTML::decode(Form::label('neck', 'Neck<sup class="text-danger">*</sup>', array('class'=>'col-md-4 control-label'))) !!}
		                  	<div class="col-md-8">
		                     	<div class="input-group">
		                        	<input type="text" number-validation="number-validation"   
		                        	maxlength="6" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" 
		                           ng-model="weight.log_weight.neck" 
		                           placeholder="Neck" name="neck" id="neck" class="form-control" required>
		                        	<span class="input-group-btn">
		                        		<span class="btn btn-primary">&nbsp;&nbsp;Inches&nbsp;&nbsp;</span>
		                        	</span>
		                     	</div>
		                     	<p class="text-danger" ng-show="LogWeightForm.neck.$invalid && LogWeightForm.neck.$touched"><small>Please enter your Current Weight</small></p>
		                  	</div>
		               	</div>
		               	<div class="form-group">
		                  	{!! HTML::decode(Form::label('arms', 'Arms<sup class="text-danger">*</sup>', array('class'=>'col-md-4 control-label'))) !!}
		                  	<div class="col-md-8">
		                     	<div class="input-group">
		                        	<input type="text" number-validation="number-validation"
		                           		ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" 
		                           		ng-model="weight.log_weight.arms"  maxlength="5" 
		                           		placeholder="Arms" name="arms" id="arms" class="form-control" required>
		                        	<span class="input-group-btn">
		                        		<span class="btn btn-primary">&nbsp;&nbsp; Inches &nbsp;&nbsp;</span>
		                        	</span>
		                     	</div>
		                     	<p class="text-danger" ng-show="LogWeightForm.arms.$invalid && LogWeightForm.arms.$touched"></p>
		                     	<p class="text-danger" ng-bind-html="weight.body_fat_error"></p>
		                  	</div>
		               	</div>

		               	<div class="form-group">
		                  	{!! HTML::decode(Form::label('waist', 'Waist<sup class="text-danger">*</sup>', array('class'=>'col-md-4 control-label'))) !!}
		                  	<div class="col-md-8">
		                     	<div class="input-group">
		                        	<input type="text" number-validation="number-validation"
		                           		ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" 
		                           		ng-model="weight.log_weight.waist"  maxlength="5" 
		                           		placeholder="Waist" name="waist" id="waist" class="form-control" required>
		                        	<span class="input-group-btn">
		                        		<span class="btn btn-primary">&nbsp;&nbsp; Inches&nbsp;&nbsp;</span>
		                        	</span>
		                     	</div>
		                     	<p class="text-danger" ng-show="LogWeightForm.waist.$invalid && LogWeightForm.waist.$touched"></p>
		                     	<p class="text-danger" ng-bind-html="weight.waist"></p>
		                  	</div>
		               	</div>

		               	<div class="form-group">
		                  	{!! HTML::decode(Form::label('hips', 'Hips<sup class="text-danger">*</sup>', array('class'=>'col-md-4 control-label'))) !!}
		                  	<div class="col-md-8">
		                     	<div class="input-group">
		                        	<input type="text" number-validation="number-validation"
		                           		ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" 
		                           		ng-model="weight.log_weight.hips"  maxlength="5" 
		                           		
		                           		placeholder="Hips" name="hips" id="hips" class="form-control" required>
		                        	<span class="input-group-btn">
		                        		<span class="btn btn-primary">&nbsp;&nbsp; Inches&nbsp;&nbsp;</span>
		                        	</span>
		                     	</div>
		                     	<p class="text-danger" ng-show="LogWeightForm.hips.$invalid && LogWeightForm.hips.$touched"></p>
		                     	<p class="text-danger" ng-bind-html="weight.body_fat_error"></p>
		                  	</div>
		               	</div>
		               	<div class="form-group">
		                  	{!! HTML::decode(Form::label('legs', 'Legs<sup class="text-danger">*</sup>', array('class'=>'col-md-4 control-label'))) !!}
		                  	<div class="col-md-8">
		                     	<div class="input-group">
		                        	<input type="text" number-validation="number-validation"
		                           		ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" 
		                           		ng-model="weight.log_weight.legs"  maxlength="5" 
		                           		placeholder="Legs" name="legs" id="legs" class="form-control" required>
		                        	<span class="input-group-btn">
		                        		<span class="btn btn-primary">&nbsp;&nbsp; Inches&nbsp;&nbsp;</span>
		                        	</span>
		                     	</div>
		                     	<p class="text-danger" ng-show="LogWeightForm.legs.$invalid && LogWeightForm.legs.$touched"></p>
		                     	<p class="text-danger" ng-bind-html="weight.legs"></p>
		                  	</div>
		               	</div>
		               	<div class="form-group">
		                  	{!! HTML::decode(Form::label('on', 'On<sup class="text-danger">*</sup>', array('class'=>'col-md-4 control-label'))) !!}
		                  	<div class="col-md-8">
		                     	<div class="input-group">
		                        	<input-datepicker  format="MM dd, yyyy" date="weight.log_weight.log_date" disabled="disabled"></input-datepicker>
								</div>
							</div>
		               	</div>
		               	<div class="form-group">
		                  	<div class="col-md-4"></div>
		                  	<div class="col-md-8">
		                     	<div class="input-group">
		                        	<button type="button" class="btn btn-primary" ng-disabled="LogWeightForm.$invalid || weight.log_weight_error" ng-click="weight.checkLoggedWeight();">Log</button>
		                     	</div>
		                  	</div>
		               	</div>
					</div>

	                <div class="row">
	                	<div class="col-xs-12 col-md-3">
		                	
		                </div>
	                </div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-md-7 col-xs-12">
		<div class="block full">
            <div class="block-title">
                <h4>
                   Body Measurement History
                </h4>
            </div>
            <div class="widget-extra-full">
	            <div id="no-more-tables" class="table-responsive">
		            <table class="table table-striped">
		                <tbody>
		                	<tr>
		                        <td class="hidden-xs text-center">Day</td>
		                        <td class="hidden-xs text-center">Neck</td>
		                        <td class="hidden-xs text-center">Arms</td>
		                        <td class="hidden-xs text-center">Waist</td>
		                        <td class="hidden-xs text-center">Hips</td>
		                        <td class="hidden-xs text-center">Legs</td>
		                    </tr>
		                    <tr>
		                    	<td data-title="Day" class="text-center text-muted">Wednesday, Apr 13</td>
		                        <td data-title="Neck" class="text-center text-muted">4 Inches</td>
		                        <td data-title="Arms" class="text-center text-muted">5 Inches</td>
		                        <td data-title="Waist" class="text-center text-muted">4 Inches</td>
		                        <td data-title="Hips" class="text-center text-muted">6 Inches</td>
		                        <td data-title="Legs" class="text-center text-muted">5 Inches</td>
		                    </tr>
		                     <tr>
		                    	<td data-title="Day"   class="text-center text-muted">Friday, Apr 15</td>
		                        <td data-title="Neck"  class="text-center text-muted">3 Inches</td>
		                        <td data-title="Arms"  class="text-center text-muted">2 Inches</td>
		                        <td data-title="Waist" class="text-center text-muted">4 Inches</td>
		                        <td data-title="Hips"  class="text-center text-muted">4 Inches</td>
		                        <td data-title="Legs"  class="text-center text-muted">2 Inches</td>
		                    </tr>
		                </tbody>
		            </table>
		        </div>
            </div>
        </div> 
	</div>
	
	<div id="log_weight_confirmation" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content">
	 			<!-- Modal Header -->
	 			<div class="modal-header  has-warning text-center custom-modal-header">
	 				<div class="alert custom_alert">
	 					<i class="fa fa-exclamation-triangle"></i>
	   					<strong> Warning!</strong>
	  				</div>
	      			<div class="modal-body custom-modal-body">
	          			<p class="custom-warning-text"><strong>Today's weight is already logged, do you want to replace?</strong></p>
	      				<p class="text-right">
	      				    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">No</button>
		                    <button type="button" class="btn btn-sm btn-primary" ng-click="weight.logWeight()">Yes</button>
		                </p>
	      			</div>
	      		</div>
			</div>
		</div>
	</div>
</div>
@endsection
<!-- Load and execute javascript code used only in this page -->
@section('page_scripts')
{!! HTML::script("js/controllers/BodyMeasurementsController.js") !!}

{!! HTML::script("js/directives/inputDatepicker.js") !!}

<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
@append