@extends('layout.master')
@section('title')
 Workouts
@endsection
@section('custom-css')
<style type="text/css">
</style>
@endsection
@section('content')
<section class="section-panel custom-section-panel" ng-controller="UserWorkoutIndexController as user_workout_index"
										ng-init="user_workout_index.userId='{{Auth::user()->id}}';
												user_workout_index.user_templates_info=<?php echo htmlspecialchars(json_encode($user_templates_info)); ?>;
												user_workout_index.getLoggedUserWorkoutRoutines();
												user_workout_index.getUserWorkoutRoutines();
												user_workout_index.getMdRecommendedWorkout();
												user_workout_index.getMdRecommendedRoutines()">
		<div ui-view></div>
</section>
@endsection
@section('js')
{!! HTML::script("js/controllers/UserWorkoutIndexController.js") !!}
{!! HTML::script("js/controllers/UserWorkoutController.js") !!}
{!! HTML::script("js/controllers/UserWorkoutDetailController.js") !!}
{!! HTML::script("js/controllers/UserRoutineDetailController.js") !!}
{!! HTML::script("js/filters/dateFormatFilter.js") !!}
{!! HTML::script("js/directives/checklist-model.js") !!}
@append
@section('custom-js')
@append