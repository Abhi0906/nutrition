<div class="panel panel-primary weight_body_panel workout_details_panel" ng-init="workout_details.getWorkoutdetails()">
	<div class="panel-heading panel_head_background">
		<div class="row work_pay_pausebtn_row">
			<div class="col-xs-4">
				<a class="btn btn-primary btndesign btn-sm" ui-sref="list-workout">Back</a>
			</div>
			<div class="col-xs-4 text-center">
				<h2 class="text-uppercase head_title_main font_light_blue">@{{workout_details.workout.title}}</h2>
			</div>
		</div>
		<div class="small-separator"></div>
	</div>
	<div class="panel-body panel_Bodypart">
		<div ng-repeat="(key, exercise) in workout_details.workout.exercise_lists">
         <div class="row workout_details_first">
				<div class="col-sm-4">
					<div class="workout_img_box">
						<img ng-src="/exercise-images/@{{exercise.image_name}}" 
                        class="img-responsive work_img"
                        onerror="this.src = '/exercise-images/no_image_big.png'">

					</div>
					<hr class="visible-xs">
				</div>	

				<!-- <div class="col-sm-4 text-center">
					<ul class="nav nav-justified nav_workout_details">
						<li>
							<table class="table set_tbl_workout">
								<thead><tr><th>SETS</th></tr></thead>
								<tbody>
									<tr>
										<td><i class="fa fa-chevron-up theme_blue_color"></i></td>
									</tr>
									<tr>
										<td><input type="text" name="" class="form-control" value="4"></td>
									</tr>
									<tr>
										<td><i class="fa fa-chevron-down theme_blue_color"></i></td>
									</tr>
								</tbody>
							</table>
						</li>
						<li>
							<table class="table set_tbl_workout">
								<thead><tr><th>REPS</th></tr></thead>
								<tbody>
									<tr>
										<td><i class="fa fa-chevron-up theme_blue_color"></i></td>
									</tr>
									<tr>
										<td><input type="text" name="" class="form-control" value="10"></td>
									</tr>
									<tr>
										<td><i class="fa fa-chevron-down theme_blue_color"></i></td>
									</tr>
								</tbody>
							</table>
						</li>
						<li>
							<table class="table set_tbl_workout">
								<thead><tr><th>WEIGHT(lbs)</th></tr></thead>
								<tbody>
									<tr>
										<td><i class="fa fa-chevron-up theme_blue_color"></i></td>
									</tr>
									<tr>
										<td><input type="text" name="" class="form-control" value="8"></td>
									</tr>
									<tr>
										<td><i class="fa fa-chevron-down theme_blue_color"></i></td>
									</tr>
								</tbody>
							</table>
						</li>
					</ul>
				</div> -->
				<div class="col-sm-8">
					<div class="workout_detail_tbl">
   					<table class="table">
   						<thead>
   							<tr><th>@{{exercise.name}}</th></tr>
   						</thead>
   						<tbody>
   							<tr>
   								<td>
   									<h5>Calories<span class="pull-right">@{{exercise.calorie_burn}}</span></h5>
   								</td>
   							</tr>
   							<tr>
   								<td>
   									<h5>Minutes<span class="pull-right">@{{exercise.exercise_time}}</span></h5>
   								</td>
   							</tr>
   							<tr>
   								<td>
   									<h5>Experience Level<span class="pull-right">@{{exercise.experience}}</span></h5>
   								</td>
   							</tr>
   							<tr>
   								<td>
   									<h5>Equipment Needed<span class="pull-right">@{{exercise.equipment}}</span></h5>
   								</td>
   							</tr>
                        <tr>
                           <td>
                              <h4 class="panel-heading cursor_pointer" data-toggle="collapse" href="#collapse@{{key}}">View Steps</h4>
                           </td>
                        </tr>
   						</tbody>
   					</table>
					</div>
				</div>
			</div>
			<div class="saperator"></div>
			<div class="row">
				<div class="col-sm-12">
					<div class="workout_detail_tbl panel-collapse collapse" id="collapse@{{key}}">
   					<table class="table">
   						<thead>
   							<tr><th>STEPS</th></tr>
   						</thead>
   						<tbody>
   							<tr ng-repeat="(key, step) in exercise.steps">
   								<td>
   									<p><strong>@{{key}}</strong></p>
   									<p>@{{step}}</p>
   								</td>
   							</tr>
   							
   						</tbody>
   					</table>
					</div>
				</div>
			</div>
         <hr ng-hide="$last" class="hr_lightgray">
      </div>
      <div style="margin-left: 20px" ng-if="workout_details.workout.exercise_lists.length==0">
         <h5>No Exercises assigned yet</h5>
      </div>
	</div>
</div>
