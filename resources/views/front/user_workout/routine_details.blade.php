<div id="workout_routines_tab_id" ng-init="routine_detail.getTemplateWorkouts()">
   <div class="row">
      <div class="col-xs-4">
         <a class="btn btn-primary btndesign btn-sm" ui-sref="list-workout">Back</a>
      </div>
      <div class="col-xs-4 text-center">
         <h2 class="text-uppercase head_title_main font_light_blue">@{{routine_detail.selectedTemplate.title}}</h2>
      </div>
         
   </div>
   <div class="saperator"></div> 
   <div class="row workoutbox_details"  
         ng-repeat="workout in routine_detail.assignWorkouts">
      <div class="col-sm-3">
         <div class="deal_img_box">
            <img ng-src="/workout-images/@{{workout.image}}" 
               class="img-responsive img_deal recommended_workout_img"
               onerror="this.src = '/workout-images/no_image_big.png'">
         </div>
      </div>
      <div class="col-sm-9">
         <h3>@{{workout.title}}</h3>
         <p><strong>Description:</strong> &nbsp;@{{workout.description}}</p>
         <p><strong>Body Part:</strong> &nbsp;@{{workout.body_part}}</p>
         <p><strong>Exercises:</strong> &nbsp;@{{workout.exercise_count}}</p>
         <p><strong>Total Minutes:</strong> &nbsp;@{{workout.exercise_total_mins}}</p>

         <a ui-sref="workout-details({ id : workout.id, from: 'routine'})" ng-disabled="workout.exercise_count==0" class="btn btn-primary btnborder_blue">View Detail</a>
         
      </div>
      <div class="saperator col-sm-12"></div>
   </div>
   <div style="margin-left: 20px" ng-if="routine_detail.assignWorkouts.length==0">
      <h5>No Workouts assigned yet</h5>
   </div>
</div>