<div class="modal-dialog workout_routine_modal">
   <!-- Modal content-->
   <div class="modal-content">
      <form name="AddRoutineToMyWorkoutForm" novalidate="novalidate" class="animation-fadeInQuick" ng-submit="user_workouts.addRoutineToMyWorkout()">
         <div class="modal-header">
            <h4 class="modal-title display_inline_block"><strong class="text-primary">@{{user_workouts.selectedRoutine.title}}</strong></h4>          
            <span class="hidden-xs">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </span>
            <div class="form-inline display_inline_block">
               <div class="form-group">
                  <label class="control-label">Start Date:- </label>
                  <div class="input-group">
                     
                     <input type="text" 
                        uib-datepicker-popup = "@{{user_workouts.format}}"                        
                        ng-model="user_workouts.my_routine_start_date"
                        is-open="user_workouts.routine_start_date_popup.opened"
                        datepicker-options="user_workouts.dateOptions"
                        ng-required="true"
                        show-button-bar=false
                        close-text="Close"   class="form-control"
                        ng-change="user_workouts.setEndDate();"
                        required
                     >   
                     <span class="input-group-addon" ng-click="user_workouts.routine_start_date_open()"> <i aria-hidden="true" class="fa fa-calendar"></i>
                     </span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="control-label">&nbsp; End Date:- </label>
                  <div class="input-group">
                     <!-- <input type="text" uib-datepicker-popup = "@{{user_workouts.format}}"
                        ng-model="user_workouts.my_routine_end_date"                       
                        datepicker-options="user_workouts.dateOptions"
                        show-button-bar=false
                        close-text="Close"   class="form-control"
                        > -->   
                     <input type="text" 
                        ng-model="user_workouts.my_routine_end_date" 
                        ng-readonly="true"
                         class="form-control"
                        >
                     <span class="input-group-addon"> <i aria-hidden="true" class="fa fa-calendar"></i>
                     </span>
                  </div>
               </div>
            </div>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            
         </div>
         <div class="modal-body">
            <div class="row" ng-repeat="(w_key, workout) in user_workouts.selectedRoutine.workouts">
               <input type="text" style="display: none;" 
                              ng-model="user_workouts.myRoutineWorkout[w_key].workout_id" 
                              ng-init="user_workouts.myRoutineWorkout[w_key].workout_id = workout.id" />
               <div class="col-sm-3">
                  <label for="example-nf-email">&nbsp;@{{workout.title}}</label>
                  <div class="deal_img_box">
                     <img ng-src="/workout-images/@{{workout.image}}" 
                        class="routine_workouts_thumb img-responsive img_deal recommended_workout_img"
                        onerror=" this.src = '/workout-images/no_image_big.png' ">
                  </div>
               </div>
               
               <div class="col-sm-6">
                  <label for="example-nf-email">&nbsp; Week day(s)</label>
                  <br/>
                  <label style="margin-left:10px" 
                           for="example-inline-checkbox1" 
                           class="checkbox-inline" 
                           ng-repeat= "(key, week_day) in user_workouts.week">
                        <input type="checkbox"
                              checklist-value="week_day.dayNumber"
                              checklist-model="workout.pivot.config"
                              ng-click="user_workouts.checkWeekSelected()"> @{{week_day.dayName}}
                  </label><br><br>

                  <label style="margin-left: 10px">Duration in day(s)</label><br>
                  <span style="margin-left: 10px">Start: @{{workout.pivot.start}}</span>
                  <span style="margin-left: 30px">End: @{{workout.pivot.end}}</span>

                  <div class="clearboth"></div>
                  <hr class="visible-xs">
               </div>
               <div class="col-sm-3">
                  <label class="control-label">Time*</label>
                  <div class="clearboth"></div>
                  <uib-timepicker ng-model="user_workouts.myRoutineWorkout[w_key].time" show-meridian=false required></uib-timepicker>
               </div>
               <div ng-if="!$last" class="col-sm-12">
               <hr/>
               </div>
            </div>           
         </div>
         <div class="modal-footer content-center">
            <button type="submit" class="btn btn-primary btndesign1" ng-disabled="AddRoutineToMyWorkoutForm.$invalid">Save To My Workout</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
         </div>
      </form>
   </div>
</div>