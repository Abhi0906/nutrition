<div class="container-row-track-hist">
   				<!-- <div class="col-sm-12 text-center">
   					<ul class="list-inline">
   						<li>
   							<img src="images/workout-images/dumbell (1).png">
   						</li>
   						<li>
   							<h4>Track New Workout</h4>
   						</li>
   						<li>
   							<h4><button class="btn btn-primary btn-logn btndesign">START</button></h4>
   						</li>
	   				</ul>
   				</div> -->
   				<div class="">
   					<div class="last_workout_box_tbl">
	   						<table class="table"> 
	   							<thead>
	   								<tr><th colspan="4">LAST WORKOUT</th></tr>
	   							</thead>
	   							<tbody>
	   								<tr class="text-center" ng-repeat="loggedUserWorkout in user_workout_index.loggedUserWorkoutRoutines | orderBy:'-log_date'  | limitTo:1 ">
	   									<td>
	   										<ul class="list-inline margin_bot">
	   											<li><img src="images/workout-images/fire (2).png"></li>
	   											<li><h3>@{{loggedUserWorkout.total_calories}}</h3></li>
	   											<li><small>CALORIES</small></li>
	   										</ul> 
	   									</td>
	   									<td>
	   										<ul class="list-inline margin_bot">
	   											<li><img src="images/workout-images/weightlifting (6).png"></li>
	   											<li><h3>@{{loggedUserWorkout.total_weight_lifted}}</h3></li>
	   											<li><small>LBS LIFTED</small></li>
	   										</ul> 
	   									</td>
	   									<td>
	   										<ul class="list-inline margin_bot">
	   											<li><img src="images/workout-images/distance-to-travel-between-two-points (2).png"></li>
	   											<li><h3>@{{loggedUserWorkout.total_miles}}</h3></li>
	   											<li><small>MILES</small></li>
	   										</ul> 
	   									</td>
	   									<td>
	   										<a href="/reports?go_to=workout">View All History</a>
	   									</td>
	   								</tr>
	   							</tbody>
	   						</table>
	   				</div> 
   				</div>
</div>
<div class="clearboth"></div>
<div class="saperator"></div>