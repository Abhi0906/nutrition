<div class="row text-center text-primary"  >
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h2 class="text-uppercase head_title_main">Your W<span class="title_head_line">ORKOU</span>T ROUTINES</h2>
      </div>
</div>
<div class="saperator"></div>
<div class="panel panel-primary weight_body_panel">
	<div class="panel-heading panel_head_background"><strong>WORKOUT ROUTINES</strong></div>
	<div class="panel-body panel_Bodypart">
			
		<div class="myroutiens_workout_box">
			@include('front.user_workout.my_routines')
		</div>
		<div class="saperator"></div>
	<div class="md_recomm_workout">
			@include('front.user_workout.md_recommended')
		</div>
		<div class="clearboth"></div>
	</div>
</div>