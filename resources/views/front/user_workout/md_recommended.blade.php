<div class="custom-hr"></div>
<div class="row">
   <div class="col-xs-12 paddingSide_15">
      <p class="subhead_panel_inner myroutine_p">MD Recommended </p>
   </div>
</div>
<div class="saperator"></div>
<ul class="nav nav-tabs">
   <li class="active responsive_systolic text-uppercase"><a data-toggle="tab" href="#workout_routines_tab_id">WORKOUT ROUTINES</a></li>
   <li class="text-uppercase"><a data-toggle="tab" href="#workouts_tab_id">WORKOUTS</a></li>
</ul>
<div class="tab-content">

   <div class="saperator"></div>
   <div id="workout_routines_tab_id" class="tab-pane fade in active">
      <div class="row workoutbox_details"  
            ng-repeat="mdRecommendedRoutine in user_workout_index.mdRecommendedRoutines">
         <div class="col-sm-3">
            <div class="deal_img_box">
               <img ng-src="/workout-images/@{{mdRecommendedRoutine.image}}" 
                  class="img-responsive img_deal recommended_workout_img"
                  onerror="this.src = '/workout-images/no_image_big.png'">
            </div>
         </div>
         <div class="col-sm-9">
            <h3>@{{mdRecommendedRoutine.title}}</h3>
            <p><strong>Description:</strong> &nbsp;@{{mdRecommendedRoutine.description}}</p>
            <p><strong>Workouts:</strong> &nbsp;@{{mdRecommendedRoutine.workout_count}}</p>
            <p><strong>Days:</strong> &nbsp;@{{mdRecommendedRoutine.days}}</p>

            <button ng-disabled="mdRecommendedRoutine.workout_count==0" class="btn btn-primary btndesign1" ng-click="user_workouts.setMdRecommendedRoutineDetails(mdRecommendedRoutine);" data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#AddRoutineWorkoutModal">Add To My Workouts</button>
            <a ng-disabled="mdRecommendedRoutine.workout_count==0" ui-sref="routine-details({ id : mdRecommendedRoutine.id})" class="btn btn-primary btnborder_blue">View Detail</a>
            <p>&nbsp;</p>
            <p ng-if="mdRecommendedRoutine.info">
               <strong>Last Added:</strong> @{{mdRecommendedRoutine.info.updated_at | custom_date_format: 'dd-MMM-yyyy'}}
               <strong>&nbsp;| &nbsp;Start:</strong> @{{mdRecommendedRoutine.info.start_date | custom_date_format: 'dd-MMM-yyyy'}}
               &nbsp;
               <strong>End:</strong> @{{mdRecommendedRoutine.info.end_date | custom_date_format: 'dd-MMM-yyyy'}}
            </p>
         </div>
         <div class="saperator col-sm-12"></div>
      </div>
      <div style="margin-left: 20px" ng-if="user_workout_index.mdRecommendedRoutines.length==0">
         <h5>No MD Recommended Routines added yet</h5>
      </div>
   </div>
   <div id="workouts_tab_id" class="tab-pane fade">
      <div class="row workoutbox_details" ng-repeat="MdWorkout_data in user_workout_index.MdRecommendedWorkout | orderBy:'id'">
         <div class="col-sm-3">
            <div class="deal_img_box">
               <img ng-src="/workout-images/@{{MdWorkout_data.image}}" class="img-responsive img_deal recommended_workout_img">
            </div>
         </div>
         <div class="col-sm-9">
            <h3>@{{MdWorkout_data.title}}</h3>
            <p><strong>Description:</strong> &nbsp;@{{MdWorkout_data.description}}</p>
            <p><strong>Body Part:</strong> &nbsp;@{{MdWorkout_data.body_part}}</p>
            <p><strong>Exercises:</strong> &nbsp;@{{MdWorkout_data.exercise_list.length}}</p>
            <p><strong>Total Minutes:</strong> &nbsp;@{{MdWorkout_data.total_mins}}</p>

            <button ng-disabled="MdWorkout_data.exercise_list.length==0" class="btn btn-primary btndesign1" ng-click="user_workouts.getMdRecommendedWorkoutDetails(MdWorkout_data);" data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#AddWorkoutModal">Add To My Workouts</button>
            <a ng-disabled="MdWorkout_data.exercise_list.length==0" class="btn btn-primary btnborder_blue" ui-sref="workout-details({ id : MdWorkout_data.id})">View Detail</a>
         </div>
         <div class="saperator col-sm-12"></div>
      </div>
      <div style="margin-left: 20px" ng-if="user_workout_index.MdRecommendedWorkout.length==0">
         <h5>No MD Recommended Workouts added yet</h5>
      </div>
   </div>
</div>

<div class="container">
   <!-- Modals -->
   <div class="modal fade" id="AddRoutineWorkoutModal" role="dialog">
      @include("front.user_workout.routine_workouts_modal")
   </div>

   <div class="modal fade" id="AddWorkoutModal" role="dialog">
      <div class="modal-dialog fix_height">
         <!-- Modal content-->
         <div class="modal-content">
            <form name="AddToMyRoutineForm" novalidate="novalidate" class="animation-fadeInQuick" ng-submit="user_workouts.addToMyRoutine()">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"><strong>@{{user_workouts.selectedWorkout.title}}</strong></h4>
               </div>
               <div class="modal-body">
                  <div class="row">
                     <div class="col-xs-12">
                        <label for="example-nf-email">&nbsp; Week day(s)</label>
                     </div>
                     <div class="col-xs-12">
                        <label style="margin-left:10px" for="example-inline-checkbox1" class="checkbox-inline" ng-repeat= "(key, week_day) in user_workouts.week">
                           <input type="checkbox" 
                              ng-model="user_workouts.myRoutine.days[key]"
                              ng-click="user_workouts.checkWeekSelected()"> @{{week_day.dayName}}
                        </label>
                     </div>
                  </div><br>
                        
                  <div class="form-group">
                           <label class="control-label col-xs-4 col-sm-2 text-right custom_class">Time*</label>
                           <div class="col-xs-8 col-sm-7">
                               <uib-timepicker ng-model="user_workouts.myRoutine.time" show-meridian=false required></uib-timepicker>
                           </div>
                  </div>

                  <div class="form-inline">
                     <div class="form-group">
                        <label class="control-label">Start Date:- </label>
                        <div class="input-group">
                           <input type="text" uib-datepicker-popup = "@{{user_workouts.format}}"
                              ng-model="user_workouts.myRoutine.start_date"
                              is-open="user_workouts.start_date_popup.opened"
                              datepicker-options="user_workouts.dateOptions"
                              ng-required="true"
                              show-button-bar=false
                              close-text="Close"   class="form-control"
                              required>   
                           <span class="input-group-addon" ng-click="user_workouts.start_date_open()"> <i aria-hidden="true" class="fa fa-calendar"></i>
                           </span>
                        </div>
                     </div>
                  </div>
                  <div class="saperator"></div>
                  <div class="form-inline"> &nbsp;
                     <div class="form-group">
                        <label class="control-label">End Date:- </label>
                        <div class="input-group">
                           <input type="text" uib-datepicker-popup = "@{{user_workouts.format}}"
                              ng-model="user_workouts.myRoutine.end_date"
                              is-open="user_workouts.end_date_popup.opened"
                              datepicker-options="user_workouts.dateOptions"
                              ng-required="true"
                              show-button-bar=false
                              close-text="Close"   class="form-control"
                              required>   
                           <span class="input-group-addon" ng-click="user_workouts.end_date_open()"> <i aria-hidden="true" class="fa fa-calendar"></i>
                           </span>
                        </div>
                     </div>
                  </div>      
               
               </div>
               <div class="modal-footer">
                  <button type="submit" class="btn btn-default" ng-disabled="AddToMyRoutineForm.$invalid || user_workouts.sel_day_counter==0">Save</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>