<div> 
 <div class="row">
		<div class="col-xs-6 paddingSide_15">
			<p class="subhead_panel_inner myroutine_p">MY WORKOUTS </p>
		</div>
		<div class="col-xs-2 paddingSide_15">
			<button class="btn btn-primary btndesig" ng-click="user_workouts.clearAllMyWorkouts()" ng-disabled="user_workout_index.all_workouts.length==0"> Clear My Workouts </button>
		</div>
		<div class="col-xs-4 paddingSide_15">
			<div class="workout-next-prev-div">
				<strong>
					@{{user_workout_index.week_start | custom_date_format: 'MMM-dd-yyyy'}} <span class="text-primary">To</span> @{{user_workout_index.week_end | custom_date_format: 'MMM-dd-yyyy'}}
				</strong> 	
				<button class="btn btn-primary btndesig" ng-click="user_workout_index.getUserWorkoutRoutines('previous')"> <span class="fa fa-chevron-left"></span> </button>
				
 				<button class="btn btn-primary btndesig" ng-click="user_workout_index.getUserWorkoutRoutines('next')"> <span class="fa fa-chevron-right"></span> </button>
			</div>
		</div>
	</div>

  <div class="custom-hr"></div>
   <div class="panel-group" id="accordion">
		<div class="panel panel-default">
	    	<div class="panel-heading" href="#collapse9" data-toggle="collapse">
	    		<strong>All Days</strong> 
	    		- @{{user_workout_index.all_workouts.length || 'No'}} workouts
	      	</div>
	    	<div id="collapse9" class="panel-collapse collapse">
	      		<div class="panel-body">
	      			<div class="workout_routiens_details" ng-repeat="my_routines_data in user_workout_index.all_workouts">
			      		<h4 class="font_light_blue">
			      			@{{my_routines_data.title}}
			      		</h4>
			      		<p><strong>Exercises:</strong>&nbsp;@{{my_routines_data.workout_exercise.length}}</p>
			      		<a class="btn btn-primary btnborder_blue" ui-sref="workout-details({ id : my_routines_data.id})">View Detail</a>
			   			<button class="btn btn-primary btndesig" ng-click="user_workouts.removeMyWorkout(my_routines_data.id, 'curr_week')"> Delete </button>
			   		</div>
	      		</div>
	    	</div>
	  	</div>
	  	
	  	<div class="panel panel-default">
	    	<div class="panel-heading" data-toggle="collapse" href="#collapse1">
	    	<strong>Monday (@{{user_workout_index.monday_routine.date | custom_date_format: 'MMM-dd-yyyy'}})</strong>
	    	- @{{user_workout_index.monday_routine.data.length || 'No'}} workouts
	    	</div>
	    	<div id="collapse1" class="panel-collapse collapse" ng-class="{in: user_workout_index.monday_routine.data.length>0}">
		      <div class="panel-body">
		      	<div class="workout_routiens_details" ng-repeat="my_routines_data in user_workout_index.monday_routine.data track by $index">
			      	
			      	<h4 class="font_light_blue">
			      		@{{my_routines_data.title}}
			      	</h4>
			      	<p><strong>Exercises:</strong>&nbsp;@{{my_routines_data.workout_exercise.length}}</p>
			      	<a class="btn btn-primary btnborder_blue" ui-sref="workout-details({ id : my_routines_data.id})">View Detail</a>
			      	<button class="btn btn-primary btndesig" ng-click="user_workouts.removeMyWorkout(my_routines_data.id, user_workout_index.monday_routine.date)"> Delete </button>
			   	</div>
			  </div>
	    	</div>
	  	</div>
		<div class="panel panel-default">
	    	<div class="panel-heading" data-toggle="collapse" href="#collapse2">
	    	<strong>Tuesday (@{{user_workout_index.tuesday_routine.date | custom_date_format: 'MMM-dd-yyyy'}})</strong>
	    	- @{{user_workout_index.tuesday_routine.data.length || 'No'}} workouts
	    	</div>
	    	<div id="collapse2" class="panel-collapse collapse" ng-class="{in: user_workout_index.tuesday_routine.data.length>0}">
		      <div class="panel-body">
		      	<div class="workout_routiens_details" ng-repeat="my_routines_data in user_workout_index.tuesday_routine.data track by $index">
			      	<h4 class="font_light_blue">
			      		@{{my_routines_data.title}}
			      	</h4>
			      	<p><strong>Exercise:</strong>&nbsp;@{{my_routines_data.workout_exercise.length}}</p>
			      	<a class="btn btn-primary btnborder_blue" ui-sref="workout-details({ id : my_routines_data.id})">View Detail</a>
			      	<button class="btn btn-primary btndesig" ng-click="user_workouts.removeMyWorkout(my_routines_data.id, user_workout_index.tuesday_routine.date)"> Delete </button>
			   	</div>
			  </div>
	    	</div>
	  	</div>
	  	<div class="panel panel-default">
	    	<div class="panel-heading" data-toggle="collapse" href="#collapse3">
	    	<strong>Wednesday (@{{user_workout_index.wednesday_routine.date | custom_date_format: 'MMM-dd-yyyy'}})</strong>
	    	- @{{user_workout_index.wednesday_routine.data.length || 'No'}} workouts
	    	</div>
	    	<div id="collapse3" class="panel-collapse collapse" ng-class="{in: user_workout_index.wednesday_routine.data.length>0}">
		      <div class="panel-body">
		      	<div class="workout_routiens_details" ng-repeat="my_routines_data in user_workout_index.wednesday_routine.data track by $index">
			      	<h4 class="font_light_blue">
			      		@{{my_routines_data.title}}
			      	</h4>
			      	<p><strong>Exercise:</strong>&nbsp;@{{my_routines_data.workout_exercise.length}}</p>
			      	<a class="btn btn-primary btnborder_blue" ui-sref="workout-details({ id : my_routines_data.id})">View Detail</a>
			      	<button class="btn btn-primary btndesig" ng-click="user_workouts.removeMyWorkout(my_routines_data.id, user_workout_index.wednesday_routine.date)"> Delete </button>
			   	</div>
			  </div>
	    	</div>
	  	</div>
		<div class="panel panel-default">
	    	<div class="panel-heading" data-toggle="collapse" href="#collapse4">
	    	<strong>Thursday (@{{user_workout_index.thursday_routine.date | custom_date_format: 'MMM-dd-yyyy'}})</strong>
	    	- @{{user_workout_index.thursday_routine.data.length || 'No'}} workouts
	    	</div>
	    	<div id="collapse4" class="panel-collapse collapse"  ng-class="{in: user_workout_index.thursday_routine.data.length>0}">
		      <div class="panel-body">
		      	<div class="workout_routiens_details" ng-repeat="my_routines_data in user_workout_index.thursday_routine.data track by $index">
			      	<h4 class="font_light_blue">
			      		@{{my_routines_data.title}}
			      	</h4>
			      	<p><strong>Exercise:</strong>&nbsp;@{{my_routines_data.workout_exercise.length}}</p>
			      	<a class="btn btn-primary btnborder_blue" ui-sref="workout-details({ id : my_routines_data.id})">View Detail</a>
			      	<button class="btn btn-primary btndesig" ng-click="user_workouts.removeMyWorkout(my_routines_data.id, user_workout_index.thursday_routine.date)"> Delete </button>
			   	</div>
			  </div>
	    	</div>
	  	</div>
		<div class="panel panel-default">
	      <div class="panel-heading" data-toggle="collapse" href="#collapse5">
	        <strong>Friday (@{{user_workout_index.friday_routine.date | custom_date_format: 'MMM-dd-yyyy'}})</strong>
	    	- @{{user_workout_index.friday_routine.data.length || 'No'}} workouts
	    	</div>
	    	<div id="collapse5" class="panel-collapse collapse" ng-class="{in: user_workout_index.friday_routine.data.length>0}">
		      <div class="panel-body">
		      	<div class="workout_routiens_details" ng-repeat="my_routines_data in user_workout_index.friday_routine.data
		      	track by $index">
			      	<h4 class="font_light_blue">
			      		@{{my_routines_data.title}}
			      	</h4>
			      	<p><strong>Exercise:</strong>&nbsp;@{{my_routines_data.workout_exercise.length}}</p>
			      	<a class="btn btn-primary btnborder_blue" ui-sref="workout-details({ id : my_routines_data.id})">View Detail</a>
			      	<button class="btn btn-primary btndesig" ng-click="user_workouts.removeMyWorkout(my_routines_data.id, user_workout_index.friday_routine.date)"> Delete </button>
			   	</div>
			  </div>
	    	</div>
	  	</div>
		<div class="panel panel-default">
	      <div class="panel-heading" data-toggle="collapse" href="#collapse6">
	        <strong>Saturday (@{{user_workout_index.saturday_routine.date | custom_date_format: 'MMM-dd-yyyy'}})</strong>
	    	- @{{user_workout_index.saturday_routine.data.length || 'No'}} workouts
	    	</div>
	    	<div id="collapse6" class="panel-collapse collapse" ng-class="{in: user_workout_index.saturday_routine.data.length>0}">
		      <div class="panel-body">
		      	<div class="workout_routiens_details" ng-repeat="my_routines_data in user_workout_index.saturday_routine.data track by $index"> 
			      	<h4 class="font_light_blue">
			      		@{{my_routines_data.title}}
			      	</h4>
			      	<p><strong>Exercise:</strong>&nbsp;@{{my_routines_data.workout_exercise.length}}</p>
			      	<a class="btn btn-primary btnborder_blue" ui-sref="workout-details({ id : my_routines_data.id})">View Detail</a>
			      	<button class="btn btn-primary btndesig" ng-click="user_workouts.removeMyWorkout(my_routines_data.id, user_workout_index.saturday_routine.date)"> Delete </button>
			   	</div>
			  </div>
	    	</div>
	  	</div>
	  	<div class="panel panel-default">
	    	<div class="panel-heading" data-toggle="collapse" href="#collapse7">
	        <strong>Sunday (@{{user_workout_index.sunday_routine.date | custom_date_format: 'dd-MMM-yyyy'}})</strong>
	    	- @{{user_workout_index.sunday_routine.data.length || 'No'}} workouts
	    	</div>
	    	<div id="collapse7" class="panel-collapse collapse" ng-class="{in: user_workout_index.sunday_routine.data.length>0}">
		      <div class="panel-body">
		      	<div class="workout_routiens_details" ng-repeat="my_routines_data in user_workout_index.sunday_routine.data track by $index">
			      	<h4 class="font_light_blue">
			      		@{{my_routines_data.title}}
			      	</h4>
			      	<p><strong>Exercise:</strong>&nbsp;@{{my_routines_data.workout_exercise.length}}</p>
			      	<a class="btn btn-primary btnborder_blue" ui-sref="workout-details({ id : my_routines_data.id})">View Detail</a>
			      	<button class="btn btn-primary btndesig" ng-click="user_workouts.removeMyWorkout(my_routines_data.id, user_workout_index.sunday_routine.date)"> Delete </button>
	
			   	</div>
			  </div>
	    	</div>
	  	</div>
		
	</div>
</div>