@extends('layout.master')

@section('title')
  My Dashboard
@endsection

@section('custom-css')
<style type="text/css">
  .header-top-fixed-menu {
    top:-122px !important;
  }
  .mainpanel-scroll{
    margin-top: 62px !important;
  }
  .content-start-after-scroll{
    margin-top: 110px !important;
  }

  .nopadding-side-panel
  {
    padding: 15px 0px 0px 0px;
  }
  #exec_responsive_table_daily table.exe_table_daily
  {
    margin-bottom: 0px;
  }
  #exec_responsive_table_daily table.exe_table_daily tr td
  {
    border-top: none;
  }
  #exec_responsive_table_daily table.exe_table_daily tr td
  {
    border-right: 1px solid #ccc;
    padding: 2px 8px;
  }
  #exec_responsive_table_daily table.exe_table_daily tr td:last-child
  {
    border-right: none;
  }

  @media only screen and (max-width: 767px) {
    #exec_responsive_table_daily table,
    #exec_responsive_table_daily thead,
    #exec_responsive_table_daily tbody,
    #exec_responsive_table_daily th,
    #exec_responsive_table_daily td,
    #exec_responsive_table_daily tr
    {
      display: block;
    }
    #exec_responsive_table_daily thead tr
    {
      position: absolute;
      top: -9999px;
      left: -9999px;
    }
    #exec_responsive_table_daily tr
    {
      border: 1px solid #ddd;
    }
    #exec_responsive_table_daily td
    {
    /* Behave like a "row" */
      border: none !important;
      border-bottom: 1px solid #eee !important;
      position: relative;
      padding-left: 50% !important;
      white-space: normal;
      text-align:left;
    }
    #exec_responsive_table_daily table tbody tr td:before
    {
      /* Now like a table header */
      position: absolute;
      /* Top/left values mimic padding */
      top: 6px;
      left: 6px;
      width: 45%;
      padding-right: 10px;
      white-space: nowrap;
      text-align:left;
      font-weight: bold;
      color: #1679C1;
    }
    #exec_responsive_table_daily td:before
    { content: attr(data-title); }

  }
</style>
@endsection
@section('content')
  <!-- scroll tabs -->
  <section class="scroll_tab_section">
    <div class="row">
      <div class="col-md-12">
        <div id="scroll_tabs_dash" class="hidden-xs">
            <ul class="nav nav-tabs nav-justified main_tabs">
                <li class="active">
                  <a class="scroll_divs" id="calorie_log" data-target="#calorie_log_div" href="javascript:void(0)">CALORIE LOG SUMMARY</a>
                </li>
                <li>
                <a class="scroll_divs" id="exercise_log" data-target="#exercise_log_div" href="javascript:void(0)">EXERCISE LOG SUMMARY</a>
                </li>
                <li>
                <a class="scroll_divs" id="health_goal" data-target="#health_goal_div" href="javascript:void(0)">HEALTH GOALS</a>
                </li>
            </ul>
        </div>
      </div>
    </div>
  </section>
  <!-- end scroll tabs -->

  <!-- calorie log summary -->
  @include('front/dashboard/dashboard_calorie')
  <!-- end calorie log summary -->

  <!-- Exercise log summary -->
  @include('front/dashboard/dashboard_exercise')
  <!-- end Exercise log summary -->

  <!-- Health Goals -->
  @include('front/dashboard/dashboard_health')
  <!-- end Health Goals -->


@endsection

@section('js')
{!! HTML::script("js/controllers/DashboardController.js") !!}
{!! HTML::script("js/jquery-gauge.min.js") !!}
{!! HTML::script("js/bootstrap-datepicker.js") !!}
{!! HTML::script("js/amcharts.js") !!}
<!-- {!! HTML::script("js/light.js") !!} -->
{!! HTML::script("js/serial.js") !!}
<!-- {!! HTML::script("js/mainchart.js") !!} -->

@append

@section('custom-js')
<script>
  $(document).ready(function() {

    $(".main_tabs li").on('click',function() {
      $('ul.main_tabs').find('li').removeClass('active');
      $(this).addClass('active');
    });

    /*scroll on tabs*/

    if($(window).width()>1199){
      $(".scroll_divs").on('click',function() {
      var id = $(this).attr('id');
        $('html, body').animate({
            'scrollTop' : $("#"+id+"_div").position().top - 130
        });
      });
    }
    else
    {
      $(".scroll_divs").on('click',function() {
      var id = $(this).attr('id');
        $('html, body').animate({
            'scrollTop' : $("#"+id+"_div").position().top - 90
        });
      });
    }



    /*end of scroll tabs*/

  });
</script>

<script type="text/javascript">

    /*for change the icon of next and previous of calendar*/
     $(document).ready(function(){

      $('.calorie_celendar_angular.angular-calender .fc-row.fc-week.ui-widget-content').addClass("tablecell_blockqual");

      $('.exericse_celendar_angular.angular-calender .fc-row.fc-week.ui-widget-content').addClass("tablecell_blockqual_2");

        /*script for equal height of two elements For Calander*/
        $('.calorie_log_calendar_panel').each(function(){
          var highestBox = 0;
          $('.tablecell_blockqual, .tablecell_blockqual_2', this).each(function(){
            if($(this).height() > highestBox) {
              highestBox = $(this).height();
            }
          });
          $('.tablecell_blockqual, .tablecell_blockqual_2', this).height(highestBox);
        });
        /*End of script for equal height of two elements*/


       /*angular js canldar*/
       $(".calorie_celendar_angular.angular-calender .fc-prev-button").find("span").removeClass("ui-icon ui-icon-circle-triangle-w").addClass("fa fa-chevron-left fa-fw");

       $(".calorie_celendar_angular.angular-calender .fc-next-button").find("span").removeClass("ui-icon ui-icon-circle-triangle-w").addClass("fa fa-chevron-right fa-fw");

       $(".exericse_celendar_angular.angular-calender .fc-prev-button").find("span").removeClass("ui-icon ui-icon-circle-triangle-w").addClass("fa fa-chevron-left fa-fw");

       $(".exericse_celendar_angular.angular-calender .fc-next-button").find("span").removeClass("ui-icon ui-icon-circle-triangle-w").addClass("fa fa-chevron-right fa-fw");
       /*end of angular js canldar*/


       /*$(".exericse_celendar_angular.angular-calender .fc-toolbar").css("left","7%");
       $(".exericse_celendar_angular.angular-calender .fc-prev-button.ui-button.ui-state-default.ui-corner-left").css("left","78%");*/




        // activate tab on scroll
        if($(window).width()>1199){
          var screen_offset = 210;
        }
        else{
          var screen_offset = 180;
        }
        $('body').scrollspy({ target: '#scroll_tabs_dash', offset: screen_offset })
        //end: activate tab on scroll

        if($(window).width() <= 767 )
        {
          //$('#exec_responsive_table_daily tr td.slide_exec_resp_daily_td').siblings().slideUp();
          //$("#exec_1st_daily").siblings().slideToggle();
          /*$('#exec_responsive_table_daily tr td.slide_exec_resp_daily_td').click(function(){
            $(this).siblings().slideToggle("slow");
            console.log($(this).attr('id'));
          });*/
        }
          var highestBox = 0;
          $('.calorie_panel').each(function(){


            $(this).find('.box_height_same').each(function(){
                if($(this).height() > highestBox){
                    highestBox = $(this).height();
                }
            })

            $(this).find('.box_height_same').height(highestBox);
          });


     });



</script>

@if($auth0_user_data['status'] == "Incomplete")
   <script type="text/javascript">
          $('#profile_modal').modal('show');
   </script>
@endif

@if(isset($auth0_user_data['email_varify']) && !($auth0_user_data['email_varify']))
  @if($auth0_user_data['created_at_diff'] > 7)
  <script type="text/javascript">
      //$('#warning_modal').modal('show');

  </script>
  @endif
@endif

@append