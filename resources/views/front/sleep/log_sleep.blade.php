 <div class="panel panel-primary weight_body_panel weight_diary_panel_size">
    <div class="panel-heading text-center panel_head_background">SLEEP</div>
    <div class="panel-body panel-container sleep_panel_sameheight panel_Bodypart">
       <div class="row weight_body_fat_row">
          <div class="col-xs-12 col-sm-5 col-md-4 col-lg-5 datebox weight_body_part1">
             <table class="table sleep-first-table body_weight_goal_table w_goal_m_othertable ">
                <tbody>
                   <tr>
                      <td>
                         <form class="form-horizontal no_padding_top_lbl" name="SleepForm">
                            <div class="form-group margin_bot">
                               <label class="col-xs-12 text-center">
                                  <p class="theme_blue_color">WENT TO BED</p>
                               </label>
                            </div>
                            <div class="form-group margin_bot">
                               <label class="control-label col-sm-4 col-xs-3 text-right">
                                  <p>DATE:</p>
                               </label>
                               <div class="col-sm-7 col-xs-9">

                                  <div class="input-group date sleepboxdatepicker">
                                   <input type="text"
                                     class="form-control" 
                                     uib-datepicker-popup = "@{{sleep.format}}"
                                     ng-model="sleep.log_sleep.went_to_date"
                                     is-open="sleep.went_tobed_date_popup.opened"
                                      datepicker-options="sleep.dateOptions"
                                      ng-required="true"
                                      ng-change="sleep.changeDate('went_to_bed');"
                                      show-button-bar=false
                                      close-text="Close" />
                                       <span class="input-group-addon caledar_custom" ng-click="sleep.went_tobed_date_open()"> 
                                          <i aria-hidden="true" class="fa fa-calendar"></i>
                                     </span>
                                </div>
                               </div>
                            </div>
                            <div class="form-group margin_bot">
                               <label class="control-label col-sm-4 col-xs-3 text-right">
                                  <p class="time_bottom">TIME:</p>
                               </label>
                               <div class="col-sm-7 col-xs-9">
                                  <uib-timepicker ng-model="sleep.log_sleep.went_to" ng-change="sleep.calculateTotalSleep();"></uib-timepicker>
                               </div>
                            </div>
                            <hr class="hr_color hr_medium">
                            <div class="form-group margin_bot">
                               <label class="control-label col-sm-9 col-xs-12 text-center">
                                  <p class="theme_blue_color"> RISE FROM BED</p>
                               </label>
                            </div>
                            <div class="form-group margin_bot">
                               <label class="control-label col-sm-4 col-xs-3 text-right">
                                  <p>DATE:</p>
                               </label>
                               <div class="col-sm-7 col-xs-9">
                    
                                   <div class="input-group date sleepboxdatepicker">
                                   <input type="text"
                                    class="form-control" 
                                    uib-datepicker-popup = "@{{sleep.format}}"
                                     ng-model="sleep.log_sleep.rise_from_date"
                                     is-open="sleep.rise_tobed_date_popup.opened"
                                      datepicker-options="sleep.dateOptions"
                                       ng-required="true"
                                       show-button-bar=false
                                       ng-change="sleep.changeDate('rise_from_bed');"
                                        close-text="Close" />
                                       <span class="input-group-addon caledar_custom" ng-click="sleep.rise_tobed_date_open()"> 
                                          <i aria-hidden="true" class="fa fa-calendar"></i>
                                     </span>
                                </div>   


                               </div>
                            </div>
                            <div class="form-group margin_bot">
                               <label class="control-label col-sm-4 col-xs-3 text-right">
                                  <p class="time_bottom">TIME:</p>
                               </label>
                               <div class="col-sm-7 col-xs-9">
                                  <uib-timepicker ng-model="sleep.log_sleep.rise_from"  ng-change="sleep.calculateTotalSleep();"></uib-timepicker>
                               </div>
                            </div>
                            <hr class="hr_color hr_medium">
                            <br>
                            <div class="form-group margin_bot">
                               <label class="control-label col-sm-4 col-xs-4 text-right">
                                  <p class="">Minutes To Fall asleep:</p>
                               </label>
                               <div class="col-sm-7 col-xs-8">
                                  <div class="row">
                                     <div class="col-xs-12 col-sm-12">
                                        <input class="form-control col-sm-6"
                                          ng-model="sleep.log_sleep.minutes_fall_alseep"
                                          ng-change="sleep.calculateTotalSleep();" 
                                            type="text" placeholder="Mins">
                                     </div>
                                    
                                  </div>
                               </div>
                            </div>
                            <hr class="hr_color hr_medium">
                             <br>
                            <div class="form-group margin_bot">
                               <label class="control-label col-sm-4 col-xs-4 text-right">
                                  <p>Times Awakened:</p>
                               </label>
                               <div class="col-sm-7 col-xs-8">
                                  <div class="row">
                                     <div class="col-xs-6 col-sm-6">
                                        <input class="form-control col-sm-6"
                                          ng-model="sleep.log_sleep.times_awakened"
                                          ng-change="sleep.enableTimeAwakened();" 
                                            type="text" placeholder="No.">
                                     </div>
                                     <div class="col-xs-6 col-sm-6">
                                        <input class="form-control col-sm-6"
                                          ng-model="sleep.log_sleep.times_awakened_minutes"
                                          type="text"
                                          placeholder="Mins"
                                          ng-disabled="sleep.times_awakened_num"
                                          ng-change="sleep.calculateTotalSleep();">
                                     </div>
                                  </div>
                               </div>
                            </div>
                            

                            <div class="form-group margin_bot">
                               <label class="control-label col-sm-4 col-xs-4 text-right">
                                  <p class="text_upper">TOTAL SLEEP:</p>
                               </label>
                               <div class="col-sm-7 col-xs-8">
                                  <p class="text-center text-primary text_left_total_sleep">
                                     <span ng-bind="sleep.log_sleep.total_sleep_hours"></span>
                                         <span>Hrs.</span>
                                      <span ng-bind="sleep.log_sleep.total_sleep_mins"></span>
                                         <span>Mins.</span>    
                                  </p>

                               </div>
                            </div>
                            <div class="row">
                               <div class="col-xs-12 text-primary text-center text-uppercase">
                                  <button class="btn btn-primary btndesign1" ng-disabled="SleepForm.$invalid || sleep.log_Sleep_error" ng-click="sleep.logSleep();">Log</button>
                               </div>
                            </div>
                         </form>
                      </td>
                   </tr>
                </tbody>
             </table>
          </div>
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2 weight_body_part2">
             <table class="table body_weight_goal_table sleep-second-table">
                <tbody>
                   <tr class="last-border_top">
                      <th class="sleep-second-table">
                         <p class="theme_blue_color">GOAL DATE:</span></p>
                         <p class="black-text" ng-if="sleep.sleep_progess != null">@{{sleep.sleep_progess.goal_date}}</p>
                      </th>
                   </tr>
                   <tr>
                      <td class="sleep-second-table">
                         <p class="theme_blue_color">GOAL SLEEP:</p>
                         <p class="black-text">@{{sleep.sleep_progess.display_goal}}</p>
                      </td>
                   </tr>
                   <!-- <tr class="last-border_bottom">
                      <td>
                         <p class="weight_mesaure_redlabel last_btnbottom"></p>
                      </td>
                   </tr> -->
                </tbody>
             </table>
          </div>
          <div class="col-xs-12 col-sm-4 col-md-5 col-lg-5 text-center weight_body_part3">
             <table class="table body_weight_goal_table w_goal_m_othertable sleep-third-main-table">
                <tr>
                   <th>
                      <p class="text-center theme_blue_color">HISTORY (LAST 4 LOGS)</p>
                      <table class="table table-bordered history_table sleep-third-table">
                         <tr>
                            <th>Date</th>
                            <th>Actual Sleep</th>
                         </tr>
                         <tr ng-repeat="history in sleep.sleep_history ">
                             <td>@{{history.display_date }}</td>
                             <td><span>@{{history.acutal_hours }}</span> Hrs.<span>@{{history.acutal_minutes }}</span> Mins</td>
                         </tr>
                         
                      </table>
                   </th>
                </tr>
                <tr>
                   <td class="last_btnbottom history_btn">
                      <button class="btn btn-primary btndesign">See All</button>
                   </td>
                </tr>
             </table>
          </div>
       </div>
    </div>
</div>