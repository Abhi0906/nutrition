 <div class="panel panel-primary body_measure_panel weight_body_panel weight_diary_panel_size">
   <div class="panel-heading text-center text-primary panel_head_background">LAST SLEEP (@{{sleep.sleep_progess.display_last_log_date}})</div>
   <div class="panel-body panel-container last-sleep sleep_panel_sameheight panel_Bodypart">
      <div class="row">
         <div class="col-xs-12">
            <div class="row ">
               <div class="col-xs-12 col-sm-2">
                  <div class="text-center sleep-bad">
                     <i class="fa fa-3x fa-bed text-primary" aria-hidden="true"></i>
                  </div>
                  <h4 class="text-center text-primary">@{{sleep.toal_sleep.last_efficiency}}%</h4>
                  <p class="text-center text-muted"><b>SLEEP EFFICIENCY</b></p>
               </div>
               <div id="" class="col-xs-12 col-sm-10">
                  <div id="last-sleep" class="firstdiv last-sleep-graph"></div>
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-xs-12 l-r-padding-none">
            <div class="reponsive_table_structure">
               <table class="table nobottom-margin last_sleep_tast_tbl">
                  <tbody>
                     <tr ng-repeat="(key, value) in sleep.last_sleep">
                        <td class="last_btnbottom">
                           <p class="text-center"><b>You went to bed at </b></p>
                           <p class="text-center text-primary"><b>@{{value.you_went_to_bed_at}}</b></p>
                        </td>
                        <td class="last_btnbottom">
                           <p class="text-center"><b>Time to fall asleep</b></p>
                           <p class="text-center text-primary"><b>@{{value.minutes_fall_alseep}}MIN</b></p>
                        </td>
                     
                        <td class="last_btnbottom">
                           <p class="text-center"><b>Time awakened</b></p>
                           <p class="text-center text-primary"><b>@{{value.times_awakened || 0}}</b></p>
                        </td>
                      
                        <td class="last_btnbottom">
                           <p class="text-center"><b>You were in bed for</b></p>
                           <p class="text-center text-primary"><b>@{{value.total_bed}}</b></p>
                        </td>
                       
                        <td class="last_btnbottom">
                           <p class="text-center"><b>Actual sleep time</b></p>
                           <p class="text-center text-primary green"><b>@{{value.actual_sleep}}</b></p>
                        </td>
                       
                     </tr>
                     <tr>
                        <td colspan="2">TOTAL SLEEP</td>
                        <td class="text-left text-primary" colspan="3">@{{sleep.toal_sleep.total_sleep || 0}}</td>
                     </tr>

                      <tr>
                        <td colspan="2">TOTAL ACTUAL SLEEP</td>
                        <td colspan="3" class="text-left text-primary">@{{sleep.toal_sleep.total_acutal_sleep || 0}}</td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
 </div>