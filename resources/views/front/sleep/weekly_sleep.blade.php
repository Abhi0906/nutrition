 <div class="row text-center text-primary">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
         <strong>
            <h2 class="text-uppercase head_title_main">Your W<span class="title_head_line">EEKLY</span> SLEEP</h2>
         </strong>
      </div>
</div>
<div class="saperator hidden-lg"></div>
<div class="row">
   <div class="col-sm-12 col-md-5 col-lg-5">
      <div class="exe_synbtn_contain">
         <ul class="list-inline">
            <li>
               <button class="btn btn-primary btndesign btn-sm">View Full Report</button>
            </li>
            <li>
               <button class="btn btn-primary btndesign btn-sm"><i class="fa fa-print"></i> Print Full Report</button>
            </li>
         </ul>
      </div>
   </div>
   <div class="col-sm-12 col-md-7 col-lg-7">
      <div class="exe_datepicker_contain">
         <ul class="list-inline datepicker_ul">
            <li class="datapicker_width_100_480 datapicker_first_label"><label class="data_label">Date or Date Range: </label></li>
            <li>
               <div class="form-group">
                  <div class="input-group date datepicker_box">
                     <input type="text"
                                     class="form-control" 
                                     uib-datepicker-popup = "@{{sleep.format}}"
                                     ng-model="sleep.weekly_sleep.from_date"
                                     is-open="sleep.weekly_sleep.from_opened"
                                     datepicker-options="sleep.dateOptions"
                                     ng-required="true"
                                     ng-change="sleep.getWeeklySleep('from');"
                                     show-button-bar=false
                                     close-text="Close" />
                     <span class="input-group-addon caledar_custom" ng-click="sleep.weekly_from_open()"> 
                        <i aria-hidden="true" class="fa fa-calendar"></i>
                   </span>
                  </div>
               </div>
            </li>
            <li><label class="data_label">To</label></li>
            <li>
               <div class="form-group">
                  <div class="input-group date datepicker_box">
                      <input type="text"
                                     class="form-control" 
                                     uib-datepicker-popup = "@{{sleep.format}}"
                                     ng-model="sleep.weekly_sleep.to_date"
                                     is-open=sleep.weekly_sleep.to_opened
                                     datepicker-options="sleep.dateOptions"
                                     ng-required="true"
                                     ng-change="sleep.getWeeklySleep('to');"
                                     show-button-bar=false
                                     close-text="Close" />
                     <span class="input-group-addon caledar_custom" ng-click="sleep.weekly_to_open()"> 
                        <i aria-hidden="true" class="fa fa-calendar"></i>
                   </span>
                  </div>
               </div>
            </li>
         </ul>
      </div>
   </div>
</div>
<div class="row weekly_sleep_row" ng-init="sleep.getWeeklySleep();">
   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div id="reponsive_table_structure">
         <table class="table table-bordered text-center sleep_weekly_table">
            <thead>
               <tr class="panel_head_background">
                  <th>Day/<br>DATE</th>
                  <th style="width:30%">GRAPH</th>
                  <th>TIME TO BED</th>
                  <th>TIME TO RISE</th>
                  <th>TIME TO FALL<br> ASLEEP</th>
                  <th>TIMES<br> AWAKENED</th>
                  <th>TIME IN BED</th>
                  <th>ACTUAL SLEEP <br>TIMES</th>
                  <th>SLEEP <br>EFFICIENCY</th>
               </tr>
            </thead>
            <tbody>
               <tr ng-repeat="(key, history) in sleep.weekly_history">
                  <td id="sun" data-title="   " class="head_slide_click">
                  @{{history.display_day}}<br>
                  @{{history.display_date}}</td>
                  <td data-title="GRAPH" class="last_sleep_small_chart_td">
                     <div id="last_sleep_@{{$index}}" class="firstdiv last-sleep-graph" ng-init="sleep.displayweeklyChart($index,history);"></div>
                  </td>
                  <td data-title="TIME TO BED">@{{history.went_to_bed}}</td>
                  <td data-title="TIME TO RISE">@{{history.rise_from_bed}}</td>
                  <td data-title="TIME TO FALL ASLEEP">@{{history.total_minutes_fall_alseep}}MIN</td>
                  <td data-title="TIME TO AWAKENED">@{{history.total_times_awakened}}</td>
                  <td data-title="TIMES IN BED">@{{history.total_sleep_times}}</td>
                  <td data-title="ACTUAL SLEEP TIMES" class="green big">@{{history.acutal_sleep_times}}</td>
                  <td data-title="SLEEP EFFICIENCY" class="green exbig">@{{history.sleep_efficiency}}%</td>
               </tr>
              
            </tbody>
         </table>
      </div>
   </div>
</div>