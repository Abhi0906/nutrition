@extends('layout.master')
@section('title')
 Deals
@endsection
@section('custom-css')
 <style type="text/css">


</style>
@endsection
@section('content')
<section class="section-panel custom-section-panel">
	<div class="row text-center text-primary">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h2 class="text-uppercase head_title_main">Your D<span class="title_head_line">eal</span>s</h2>
      </div>
   	</div>
   	<div class="saperator"></div>
   	<div class="panel panel-primary weight_body_panel" ng-controller="DealFrontController as deal" ng-init="deal.userId='{{Auth::user()->id}}';  deal.getDeals()">
   		<div class="panel-heading panel_head_background"><strong>DEALS</strong></div>
   		<div class="panel-body panel_Bodypart">
   			<div class="deal_box"  ng-repeat="deal_data in deal.deals | orderBy:'start'">
			   	<div class="row">
				   	<div class="col-sm-4 col-md-3">
				   		<div class="deal_img_box">
				   			<img ng-src="/deal-images/@{{deal_data.image}}"
				   				class="img-responsive img_deal">
				   		</div>
				   	</div>
				   	<div class="col-sm-8 col-md-7">
				   		<div class="deals_details">
				   			<h2 class="text-danger">@{{deal_data.title}}
				   				<small ng-if="deal_data.is_expired==1" class="black_text"><i>(Expired)</i></small>
				   			</h2>
				   			<p class="deals_descrpt">
				   				@{{deal_data.description}}
				   			</p>

				   			<div class="deals_tbl">
					   			<table class="table">
					   				<thead>
					   					<tr>
						   					<th>COUPON CODE</th>
						   					<th>START</th>
						   					<th>END</th>
					   					</tr>
					   				</thead>
					   				<tbody>
					   					<tr>
					   						<td>@{{deal_data.coupon_code}}</td>
					   						<td>@{{deal_data.start | custom_date_format: 'dd-MMM-yyyy hh:mm a'}}</td>
					   						<td>@{{deal_data.end | custom_date_format: 'dd-MMM-yyyy hh:mm a'}}</td>
					   					</tr>
					   				</tbody>
					   			</table>
				   			</div>
				   		</div>
				   	</div>
			   	</div>

			   	<hr class="hr_lightgray">
		   	</div>

   		</div>
   	</div>
</section>

@endsection
@section('js')
{!! HTML::script("js/controllers/DealFrontController.js") !!}

@append
@section('custom-js')
@append
