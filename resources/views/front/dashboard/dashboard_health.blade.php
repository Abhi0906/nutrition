<style>
.health_charts{
  height: 185px;
}

.health_charts_body_measurement{
  height: 325px;
}
</style>
<section class="health_log_summary_section" ng-controller="DashboardController as dash_health" ng-init="dash_health.userId='{{$user_id}}'; dash_health.getUserhealthData(); dash_health.displayCharts('last_week', 'all');">
  <div id="health_goal_div" class="panel panel-default out-row-panel">
   <div class="panel-body">
     <div class="row">
        <div class="col-sm-12">
          <h1 class="text-center text-primary text-uppercase visible-xs"><strong>Health Goals</strong></h1>
        </div> 
      </div>

      <div class="row health_height_change">

        <!-- left : health goals -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
          <div class="panel panel-primary calorie_panel">
            <div class="panel-heading calorie_panel_head"></div>
              <div class="panel-body calorie_panel_body health_goal_panel_body">
                <div class="row row-eq-height headrow-samecolumnheight daily_subhead health_daily_subhead">
                  <div class="col-xs-6 col-sm-4 health-sub-heading">
                    &nbsp;
                  </div>
                  <div class="col-xs-6 col-sm-3 health-sub-heading">
                    <h4>Last Measured</h4>
                  </div>
                  <div class="hidden-xs col-sm-1 health-sub-heading">
                    <h4>Goal</h4>
                  </div>
                  <div class="hidden-xs col-sm-4 health-sub-heading">
                    <h4>Trend&nbsp;
                      <select class="medium_font"
                             ng-init="dash_health.outer_measurement = dash_health.graph_range[0].id" 
                             ng-model="dash_health.outer_measurement" 
                             ng-change="dash_health.displayCharts(dash_health.outer_measurement, 'outer_measurements');">
                        <option ng-repeat="range in dash_health.graph_range" value="@{{range.id}}">@{{range.title}}</option>
                      </select>
                    </h4>
                  </div>
                </div>
               
                <div class="row row-eq-height headrow-samecolumnheight">
                  <div class="col-xs-6 col-sm-4 health-sub-contain">
                    <p class="boldclass">Body Weight</p>
                    <div class="guagemetercontain">
                      <ul class="list-inline">
                          <li>
                              <div id="bodyweight-gauge" class="calorielog-gauge1 guagemeterdemo"></div>
                          </li>
                          <li>
                              <h5 class="guage_meter_head_text"><strong><span class="textbig">@{{dash_health.weight_percent}} %</span> OF GOAL</strong></h5>
                          </li>
                      </ul>
                    </div>
                    <p class="boldclass visible-xs">Goal : @{{dash_health.weight_goal}} lbs.</p>
                    <div class="bottom_line"></div>
                  </div>
                  <div class="col-xs-6 col-sm-3 health-sub-contain">
                    <p class="boldclass">@{{dash_health.last_weight}} lbs. <small class="pull-right">(@{{dash_health.last_weight_fat_date  | custom_date_format: 'MM-dd-yy'}})</small></p>
                    <p>
                      <a href="/weight" class="btn btn-sm btn-default btn-design">Log body Weight&nbsp;<span class="fa fa-caret-right"></span></a>
                    </p>
                    <label>or</label>
                    <p>
                      <button type="button" class="btn btn-sm btn-default btn-design">Sync with Device&nbsp;<span class="fa fa-caret-down"></span></button>
                    </p>
                    <div class="bottom_line"></div>
                  </div>
                  <div class="col-xs-12 visible-xs daily_subhead health_daily_subhead_2">
                    <h4>Trend&nbsp;
                      <select class="medium_font">
                        <option>Last week</option>
                        <option>Last Month</option>
                        <option>Last Quarter</option>
                        <option>Last 6 months</option>
                        <option>Last year</option>
                      </select>

                    </h4>
                  </div>
                  <div class="hidden-xs col-sm-1 health-sub-contain">
                    <p class="boldclass">@{{dash_health.weight_goal}} lbs.</p>
                    <div class="bottom_line smallline"></div>
                  </div>
                  <div class="col-xs-12 col-sm-4 health-sub-contain">
                    <div id="weight-chart" class="health_charts"></div>
                    <p>
                    <!-- <div class='datepicker input-group date datepicker_box' id='example1'>
                        <input type='text' class="form-control" placeholder="click to show datepicker"  id="" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    to
                    <div class='datepicker input-group date datepicker_box' id='example2'>
                        <input type='text' class="form-control" placeholder="click to show datepicker"  id="" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div> -->
                    </p>

                    <div class="bottom_line"></div>
                  </div>
                </div>
                
                <div class="row daily_subhead health_daily_subhead_2 visible-xs">
                  <div class="col-xs-6 health-sub-heading"><h4></h4></div>
                  <div class="col-xs-6 health-sub-heading"><h4>Last Measured</h4>
                  </div>
                </div>
               
                <div class="row row-eq-height headrow-samecolumnheight">
                  <div class="col-xs-6 col-sm-4 health-sub-contain">
                    <p class="boldclass">Body Fat</p>
                    <div class="guagemetercontain">    
                      <p><ul class="list-inline">
                          <li>
                              <div id="bodyfat-gauge" class="calorielog-gauge1 guagemeterdemo"></div>
                          </li>
                          <li>
                              <h5 class="guage_meter_head_text"><strong><span class="textbig">@{{dash_health.fat_percent}} %</span> OF GOAL</strong></h5>
                          </li>
                      </ul>
                      </p>
                      <div class="bottom_line"></div>
                    </div>
                    <p class="boldclass visible-xs">Goal : @{{dash_health.fat_goal}}%</p>
                  </div>
                  <div class="col-xs-6 col-sm-3 health-sub-contain">
                    <p class="boldclass"> @{{dash_health.last_fat}}% <small class="pull-right">(@{{dash_health.last_weight_fat_date | custom_date_format: 'MM-dd-yy'}})</small></p>
                    <p>
                      <a href="/weight" class="btn btn-sm btn-default btn-design">Log body Fat&nbsp;<span class="fa fa-caret-right"></span></a>
                    </p>
                    
                    <div class="bottom_line"></div>
                  </div>
                  <div class="col-xs-12 visible-xs daily_subhead health_daily_subhead_2">
                    <h4>Trend&nbsp;
                        <select class="medium_font">
                        <option>Last Week</option>
                        <option>Last Month</option>
                        <option>Last Quarter</option>
                        <option>Last 6 months</option>
                        <option>Last Year</option>
                      </select>
                    </h4>
                  </div>
                  <div class="hidden-xs col-sm-1 health-sub-contain">
                    <p class="boldclass">@{{dash_health.fat_goal}}%</p>
                    <div class="bottom_line smallline"></div>
                  </div>
                  <div class="col-xs-12 col-sm-4 health-sub-contain">
                    <div id="body-fat-chart" class="health_charts"></div>
        
                    <div class="bottom_line"></div>
                  </div>
                </div>

                <div class="row daily_subhead health_daily_subhead_2 visible-xs">
                  <div class="col-xs-6 health-sub-heading"><h4></h4></div>
                  <div class="col-xs-6 health-sub-heading"><h4>Last Measured</h4>
                  </div>
                </div>

                <div class="row row-eq-height headrow-samecolumnheight">
                  <div class="col-xs-6 col-sm-4 health-sub-contain">
                    <p class="boldclass">Body Measurement</p>
                    <div class="guagemetercontain bodymeasure box_height_same">
                      <ul class="list-inline ul_borderBott_meter">
                          <li class="block_display">
                              <div id="neck-gauge" class="guagemeterdemo-measure"></div>
                          </li>
                          <li class="block_display">
                              <h5 class="guage_meter_head_text"><strong><span class="textbig">@{{dash_health.body_measurements_goal.neck_percentage}} %</span> OF NECK</strong></h5>
                          </li>
                      </ul>
                      <p class="visible-xs no_padding">Goal: @{{dash_health.body_measurements_goal.neck_g}}</a></p>
                    </div>
                    <div class="guagemetercontain bodymeasure box_height_same">
                      <ul class="list-inline ul_borderBott_meter">
                          <li class="block_display">
                              <div id="arms-gauge" class="guagemeterdemo-measure"></div>
                          </li>
                          <li class="block_display">
                              <h5 class="guage_meter_head_text"><strong><span class="textbig">@{{dash_health.body_measurements_goal.arms_percentage}} %</span> OF ARMS</strong></h5>
                          </li>
                      </ul>
                      <p class="visible-xs no_padding">Goal: @{{dash_health.body_measurements_goal.arms_g}}</a></p>
                    </div>
                    <div class="guagemetercontain bodymeasure box_height_same">
                      <ul class="list-inline ul_borderBott_meter">
                          <li class="block_display">
                              <div id="waist-gauge" class="guagemeterdemo-measure"></div>
                          </li>
                          <li class="block_display">
                              <h5 class="guage_meter_head_text"><strong><span class="textbig">@{{dash_health.body_measurements_goal.waist_percentage}} %</span> OF WAIST</strong></h5>
                          </li>
                      </ul>
                      <p class="visible-xs no_padding">Goal: @{{dash_health.body_measurements_goal.waist_g}}</a></p>
                    </div>
                    <div class="guagemetercontain bodymeasure box_height_same">
                      <ul class="list-inline ul_borderBott_meter">
                          <li class="block_display">
                              <div id="hips-gauge" class="guagemeterdemo-measure"></div>
                          </li>
                          <li class="block_display">
                              <h5 class="guage_meter_head_text"><strong><span class="textbig">@{{dash_health.body_measurements_goal.hips_percentage}} %</span> OF HIPS</strong></h5>
                          </li>
                      </ul>
                      <p class="visible-xs no_padding">Goal: @{{dash_health.body_measurements_goal.hips_g}}</a>
                    </div>
                    <div class="guagemetercontain bodymeasure box_height_same">
                      <ul class="list-inline">
                          <li class="block_display">
                              <div id="legs-gauge" class="guagemeterdemo-measure"></div>
                          </li>
                          <li class="block_display">
                              <h5 class="guage_meter_head_text"><strong><span class="textbig">@{{dash_health.body_measurements_goal.legs_percentage}} %</span> OF LEGS</strong></h5>
                          </li>
                      </ul>
                      <p class="visible-xs no_padding">Goal: @{{dash_health.body_measurements_goal.legs_g}}</a>
                    </div>

                    <!-- <p class="boldclass visible-xs">Goal :
                        <ul class="list-group visible-xs">
                            <li class="list-group-item">Neck <span class="pull-right">17</span></li>
                            <li class="list-group-item">Arms <span class="pull-right">19</span></li>
                            <li class="list-group-item">Waist <span class="pull-right">30</span></li>
                            <li class="list-group-item">Hips <span class="pull-right">30</span></li>
                            <li class="list-group-item">Legs <span class="pull-right">30</span></li>
                        </ul>
                    </p> -->
                  </div>
                  <div class="col-xs-6 col-sm-3 health-sub-contain">
                    <div class="body-measure-labels pos_relative">
                      <p>&nbsp;</p>
                      <ul class="list-group no-shadow">
                          <li class="list-group-item box_height_same measure_lbl_meter">Neck <span class="pull-right">@{{dash_health.last_body_measurements.neck}}</span></li>
                          <li class="list-group-item box_height_same measure_lbl_meter">Arms <span class="pull-right">@{{dash_health.last_body_measurements.arms}}</span></li>
                           
                          <li class="list-group-item box_height_same measure_lbl_meter">Waist <span class="pull-right">@{{dash_health.last_body_measurements.waist}}</span></li>
                           
                          <li class="list-group-item box_height_same measure_lbl_meter">Hips <span class="pull-right">@{{dash_health.last_body_measurements.hips}}</span></li>
                           
                          <li class="list-group-item box_height_same measure_lbl_meter">Legs <span class="pull-right">@{{dash_health.last_body_measurements.legs}}</span></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-xs-12 visible-xs daily_subhead health_daily_subhead_2">
                    <h4>Trend&nbsp;
                      <select class="medium_font">
                        <option>Last week</option>
                        <option>Last Month</option>
                        <option>Last Quarter</option>
                        <option>Last 6 months</option>
                        <option>Last year</option>
                      </select></h4>
                  </div>
                  <div class="hidden-xs col-sm-1 health-sub-contain">
                    <div class="body-measurelast-labels-goal pos_relative_last">
                        <p>&nbsp;</p>
                        <ul class="list-group no-shadow">
                            <li class="list-group-item box_height_same">@{{dash_health.body_measurements_goal.neck_g}}</li>
                            <li class="list-group-item box_height_same">@{{dash_health.body_measurements_goal.arms_g}}</li>
                            <li class="list-group-item box_height_same">@{{dash_health.body_measurements_goal.waist_g}}</li>
                            <li class="list-group-item box_height_same">@{{dash_health.body_measurements_goal.hips_g}}</li>
                            <li class="list-group-item box_height_same">@{{dash_health.body_measurements_goal.legs_g}}</li>
                        </ul>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4 health-sub-contain">
                    <div id="all-bm-chart" class="health_charts_body_measurement"></div>
                    
                  </div>
                </div>
              </div>
          </div>
        </div>
        <!-- end left : health goals -->

        <!-- right : health goals -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
          <div class="panel panel-primary calorie_panel health_padding_bottom">
            <div class="panel-heading calorie_panel_head"></div>
              <div class="panel-body calorie_panel_body health_goal_panel_body">
              <div class="row row-eq-height headrow-samecolumnheight daily_subhead health_daily_subhead">
                <div class="col-xs-6 col-sm-4 health-sub-heading">
                  &nbsp;
                </div>
                <div class="col-xs-6 col-sm-3 health-sub-heading">
                  <h4>Last Measured</h4>
                </div>

                <div class="hidden-xs col-sm-1 health-sub-heading">
                  <h4>Goal</h4>
                </div>
                <div class="hidden-xs col-sm-4 health-sub-heading">
                  <h4>Trend&nbsp;
                      <!-- <select class="medium_font">
                        <option>Last week</option>
                        <option>Last Month</option>
                        <option>Last Quarter</option>
                        <option>Last 6 months</option>
                        <option>Last year</option>
                      </select> -->

                      <select class="medium_font"
                             ng-init="dash_health.inner_measurement = dash_health.graph_range[0].id" 
                             ng-model="dash_health.inner_measurement" 
                             ng-change="dash_health.displayCharts(dash_health.inner_measurement, 'inner_measurements');">
                        <option ng-repeat="range in dash_health.graph_range" value="@{{range.id}}">@{{range.title}}</option>
                      </select>
                  </h4>
                </div>
              </div>
              <div class="row row-eq-height headrow-samecolumnheight">
                <div class="col-xs-6 col-sm-4 health-sub-contain">
                  <p class="boldclass">Blood Pressure</p>
                  <div class="guagemetercontain bodymeasure">    
                    <ul class="list-inline">
                        <li>
                            
                            <div id="bloodpresure-systolic-gauge" class="guagemeterdemo-measure"></div>
                        </li>
                        <li>
                            <h5 class="guage_meter_head_text"><strong><span class="textbig">@{{dash_health.bp_goal.systolic_percentage}} %</span> OF SYSTOLIC</strong></h5>
                        </li>
                    </ul>
                  </div>
                  <hr class="hr_medium">
                  <div class="guagemetercontain bodymeasure">    
                    <ul class="list-inline">
                        <li>
                            
                            <div id="bloodpresure-diastolic-gauge" class="guagemeterdemo-measure"></div>
                        </li>
                        <li>
                            <h5 class="guage_meter_head_text"><strong><span class="textbig">@{{dash_health.bp_goal.diastolic_percentage}} %</span> OF DIASTOLIC</strong></h5>
                        </li>
                    </ul>
                  </div>

                 <!--  <div class="guagemetercontain bodymeasure">    
                      <ul class="list-inline">
                          <li>
                              <div id="neck-gauge" class="guagemeterdemo-measure"></div>
                          </li>
                          <li>
                              <h5 class="guage_meter_head_text"><strong><span class="textbig">@{{dash_health.body_measurements_goal.neck_percentage}} %</span> OF NECK</strong></h5>
                          </li>
                      </ul>
                      <p class="visible-xs">Goal: @{{dash_health.body_measurements_goal.neck_g}}</a></p>
                    </div> -->



                  <p class="boldclass visible-xs">Goal : @{{dash_health.bp_goal.systolic_g}} /@{{dash_health.bp_goal.diastolic_g}}</p>
                  <div class="bottom_line"></div>
                </div>
                <div class="col-xs-6 col-sm-3 health-sub-contain">
                  <p class="boldclass">@{{dash_health.last_blood_pressure.systolic}}/@{{dash_health.last_blood_pressure.diastolic}} <small class="pull-right">(@{{dash_health.last_blood_pressure.log_date | custom_date_format: 'MM-dd-yy' }})</small></p>
                  <p>
                      <a href="/bp_pulse" class="btn btn-sm btn-default btn-design">Log BP&nbsp;<span class="fa fa-caret-right"></span></a>
                    </p>
                    <label>or</label>
                    <p>
                      <button type="button" class="btn btn-sm btn-default btn-design">Sync with Device&nbsp;<span class="fa fa-caret-down"></span></button>
                    </p>
                    <div class="bottom_line"></div>
                </div>
                <div class="col-xs-12 visible-xs daily_subhead health_daily_subhead_2">
                    <h4>Trend&nbsp; 
                        <select class="medium_font">
                        <option>Last week</option>
                        <option>Last Month</option>
                        <option>Last Quarter</option>
                        <option>Last 6 months</option>
                        <option>Last year</option>
                      </select>
                    </h4>
                </div>
                <div class="hidden-xs col-sm-1 health-sub-contain">
                  <p class="boldclass">@{{dash_health.bp_goal.systolic_g}} /@{{dash_health.bp_goal.diastolic_g}}</p>
                  <div class="bottom_line smallline"></div>
                </div>
                <div class="col-xs-12 col-sm-4 health-sub-contain">
                  <div id="bp-chart" class="health_charts"></div>
                   
                  <div class="bottom_line"></div>
                </div>
              </div>

              <div class="row daily_subhead health_daily_subhead_2 visible-xs">
                  <div class="col-xs-6 health-sub-heading"><h4></h4></div>
                  <div class="col-xs-6 health-sub-heading"><h4>Last Measured</h4>
                  </div>
              </div>

              <div class="row row-eq-height headrow-samecolumnheight">
                <div class="col-xs-6 col-sm-4 health-sub-contain">
                  <p class="boldclass">Heart Rate</p>
                  <div class="guagemetercontain">    
                    <ul class="list-inline">
                        <li>
                            <!-- <canvas id="gauge1" class="gaugemeter"></canvas> -->
                            <!-- <div id="calories-gauge123" meter-value="dash_new.gauge_meter_food_value" class="calorielog-gauge1 guagemeterdemo" gauge-meter></div> -->
                            <div id="hartrate-gauge" class="calorielog-gauge1 guagemeterdemo"></div>
                        </li>
                        <li>
                            <h5 class="guage_meter_head_text"><strong><span class="textbig">@{{dash_health.bp_goal.pulse_percentage}} %</span> OF GOAL</strong></h5>
                        </li>
                    </ul>
                  </div>
                  <p class="boldclass visible-xs">Goal : @{{dash_health.bp_goal.heart_rate_g}}</p>
                  <div class="bottom_line"></div>
                </div>
                <div class="col-xs-6 col-sm-3 health-sub-contain">
                  <p class="boldclass">@{{dash_health.last_pulse.pulse}} <small class="pull-right">(@{{dash_health.last_pulse.log_date | custom_date_format: 'MM-dd-yy'}})</small></p>

                  <p>
                      <a href="/bp_pulse" class="btn btn-sm btn-default btn-design">Log Pulse&nbsp;<span class="fa fa-caret-right"></span></a>
                    </p>
                    <label>or</label>
                    <p>
                      <button type="button" class="btn btn-sm btn-default btn-design">Sync with Device&nbsp;<span class="fa fa-caret-down"></span></button>
                    </p>
                   <div class="bottom_line"></div>
                </div>
                <div class="col-xs-12 visible-xs daily_subhead health_daily_subhead_2">
                    <h4>Trend&nbsp;
                      <select class="medium_font">
                        <option>Last week</option>
                        <option>Last Month</option>
                        <option>Last Quarter</option>
                        <option>Last 6 months</option>
                        <option>Last year</option>
                      </select></h4>
                </div>
                <div class="hidden-xs col-sm-1 health-sub-contain">
                  <p class="boldclass">@{{dash_health.bp_goal.heart_rate_g}}</p>
                  <div class="bottom_line smallline"></div>
                </div>
                <div class="col-xs-12 col-sm-4 health-sub-contain">
                  <div id="pulse-chart" class="health_charts"></div>
                  
                  <div class="bottom_line"></div>
                </div>
              </div>

              <div class="row daily_subhead health_daily_subhead_2 visible-xs">
                  <div class="col-xs-6 health-sub-heading"><h4></h4></div>
                  <div class="col-xs-6 health-sub-heading"><h4>Last Measured</h4>
                  </div>
              </div>

              <div class="row row-eq-height headrow-samecolumnheight">
                <div class="col-xs-6 col-sm-4 health-sub-contain">
                  <p class="boldclass">Sleep</p>
                  <div class="guagemetercontain">    
                    <ul class="list-inline">
                        <li>
                            <!-- <canvas id="gauge1" class="gaugemeter"></canvas> -->
                            <!-- <div id="calories-gauge123" meter-value="dash_new.gauge_meter_food_value" class="calorielog-gauge1 guagemeterdemo" gauge-meter></div> -->
                            <div id="sleep-gauge" class="calorielog-gauge1 guagemeterdemo"></div>
                        </li>
                        <li>
                            <h5 class="guage_meter_head_text"><strong><span class="textbig">@{{dash_health.last_sleep.last_progress| number:0}} %</span> OF GOAL</strong></h5>
                        </li>
                    </ul>
                  </div>
                  <p class="boldclass visible-xs">Goal : @{{dash_health.last_sleep.display_goal}}</p>
                </div>
                <div class="col-xs-6 col-sm-3 health-sub-contain">
                  <p class="boldclass">@{{dash_health.last_sleep.last_sleep_measurement}} <small class="pull-right">(@{{dash_health.last_sleep.display_last_log_date}})</small></p>
                  <p>
                    <a href="/sleep" class="btn btn-sm btn-default btn-design">Log Sleep&nbsp;<span class="fa fa-caret-right"></span></a>
                    </p>
                </div>
                <div class="col-xs-12 visible-xs daily_subhead health_daily_subhead_2">
                    <h4>Trend&nbsp;
                        <select class="medium_font">
                        <option>Last week</option>
                        <option>Last Month</option>
                        <option>Last Quarter</option>
                        <option>Last 6 months</option>
                        <option>Last year</option>
                      </select>
                    </h4>
                </div>
                <div class="hidden-xs col-sm-1 health-sub-contain">
                  <p class="boldclass">@{{dash_health.last_sleep.display_goal}}</p>
                </div>
                <div class="col-xs-12 col-sm-4 health-sub-contain">
                  <div id="sleep-chart" class="health_charts" style="overflow: hidden; text-align: left;"></div>
                  
                </div>
              </div>
              </div>
          </div>
        <!-- right : health goals -->
        </div>
      </div>
    </div>
  </div>    
</section>