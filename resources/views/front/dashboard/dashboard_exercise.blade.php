<section class="exercise_log_summary_section" ng-controller="DashboardController as dash_exercise" ng-init="dash_exercise.userId='{{$user_id}}'; dash_exercise.getUserLoggedExercise(null);">
  <div id="exercise_log_div" class="panel panel-default out-row-panel">
   <div class="panel-body">
      <div class="row">
        <div class="col-sm-12">
          <h1 class="text-center text-primary text-uppercase visible-xs"><strong>Exercise LOG SUMMARY</strong></h1>
        </div> 
      </div>

      <div class="row exercise_height_change">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
          <div class="panel panel-primary calorie_panel exer_goal_panel exercise_padding_bottom">
            <div class="panel-heading text-center calorie_panel_head">DAILY</div>
            <div class="panel-body calorie_panel_body nopadding-side-panel">
                <div class="row">
                  <div class="col-md-12">
                      <div class="text-center datepicker_contain">
                        <!-- <span class="fa fa-chevron-left"></span>
                        <input type='text' class="datepicker_calorie" id='exercise_daily_datepicker' />
                        <span class="fa fa-chevron-right"></span>
                        <span class="fa fa-calendar"></span>
                         -->
                        <ul class="list-inline btn-ul-date">
                        
                        <li>
                          <a href="javascript:void(0);" ng-click="dash_exercise.dateNextPrevious('prev', dash_exercise.daily_calorie_exercise_date, 'exercise' )">
                              <span id="cal_summary_date_prev"  class="fa fa-chevron-left"></span> 
                          </a> 
                          <input type="text"
                                class="datepicker_calorie" 
                                uib-datepicker-popup = "@{{dash_exercise.format}}"
                                ng-model="dash_exercise.daily_calorie_exercise_date"
                                is-open="dash_exercise.daily_calorie_exercise_date_popup.opened"
                                datepicker-options="dash_exercise.dateOptions"
                                ng-required="true"
                                show-button-bar=false
                                close-text="Close"
                                ng-change="dash_exercise.getUserLoggedExercise(dash_exercise.daily_calorie_exercise_date)" />
                        </li>
                        <li>
                          <a href="javascript:void(0);" ng-click="dash_exercise.dateNextPrevious('next', dash_exercise.daily_calorie_exercise_date, 'exercise')">
                              <span id="cal_summary_date_next" class="fa fa-chevron-right"></span>&nbsp;&nbsp;
                          </a>
                          <span class="caledar_custom fa fa-calendar" ng-click="dash_exercise.daily_calorie_exercise_date_open()"> 
                          </span>
                        </li> 
                      
                      </div>
                  </div>
                </div>
                <div id="exec_responsive_table_daily">
                    <table class="table exe_table_daily">
                      <thead>
                        <tr class="daily_subhead">
                          <td><h4>Type</h4></td>
                          <td><h4>Calories burned</h4></td>
                          <td><h4>Minutes</h4></td>
                        </tr>
                      </thead>
                      <tbody>
                        <tr id="quadriceps_exec_tr" class="second_sub_head">
                          <td id="exec_1st_daily" class="slide_exec_resp_daily_td"><p>Cardiovascular</p></td>
                          <td class="hidden-xs"><p>&nbsp;</p></td>
                          <td class="hidden-xs"><p>&nbsp;</p></td>
                        </tr>
                        <tr class="quadriceps_exec_1" ng-repeat="cardioData in dash_exercise.cardio">
                          <td data-title="Name"><p>@{{cardioData.name}} </p></td>
                          <td data-title="Calories burned">
                            <p>
                              @{{cardioData.calories_burned}}
                            </p>
                          </td>
                          <td data-title="Minutes">
                            <p>
                              @{{cardioData.time}}
                            </p> 
                          </td>
                        </tr>
                        <tr ng-if="!dash_exercise.cardio.length">
                          <td class="slide_exec_resp_daily_td  text-center" colspan=3><p>No data exist</p></td>
                        </tr>
                        <tr id="harmstring_exec_tr" class="second_sub_head">
                          <td class="slide_exec_resp_daily_td"><p>Resistance Training</p></td>
                          <td class="hidden-xs"><p>&nbsp;</p></td>
                          <td class="hidden-xs"><p>&nbsp;</p></td>
                        </tr>
                        <tr ng-repeat="resistanceData in dash_exercise.resistance">
                          <td data-title="Name">
                            <p> @{{resistanceData.name}}</p>
                          </td>
                          <td data-title="Calories burned">
                           <p> @{{resistanceData.calories_burned}}</p>
                          </td>
                          <td data-title="Minutes">
                            <p>
                               @{{resistanceData.time}}
                            </p> 
                          </td>
                        </tr>
                        <tr ng-if="!dash_exercise.resistance.length">
                          <td class="slide_exec_resp_daily_td text-center" colspan=3><p>No data exist</p></td>
                        </tr>
                        <tr class="top_border">
                          <td colspan="3" class="padding-8">
                            <p>
                              <!-- <button type="button" class="btn btn-sm btn-default btn-design">Log Exercise&nbsp;<span class="fa fa-caret-right"></span></button> -->
                              <a class="btn btn-sm btn-default btn-design" href="/exercise">Log Exercise&nbsp;<span class="fa fa-caret-right"></span></a>
                            </p>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                </div>

            </div>
          </div>

          <!-- <div class="panel panel-primary calorie_panel exer_goal_panel exercise_padding_bottom">
            <div class="panel-heading text-center calorie_panel_head">DAILY</div>
            <div class="panel-body calorie_panel_body nopadding-side-panel">
                <div class="row">
                  <div class="col-md-12">
                      <div class="text-center datepicker_contain">
                        <span class="fa fa-chevron-left"></span>
                        <input type='text' class="datepicker_calorie" id='exercise_daily_datepicker' />
                        <span class="fa fa-chevron-right"></span>
                        <span class="fa fa-calendar"></span>
                      
                      </div>
                  </div>
                </div>

                <div id="exec_responsive_table_daily">
                    <table class="table exe_table_daily">
                      <thead>
                        <tr class="daily_subhead">
                          <td><h4>Legs</h4></td>
                          <td><h4>Sets</h4></td>
                          <td><h4>Weight</h4></td>
                          <td><h4>Reps</h4></td>
                        </tr>
                      </thead>
                      <tbody>
                        <tr id="quadriceps_exec_tr" class="second_sub_head">
                          <td id="exec_1st_daily" class="slide_exec_resp_daily_td"><p>Quadriceps</p></td>
                          <td class="hidden-xs"><p>&nbsp;</p></td>
                          <td class="hidden-xs"><p>&nbsp;</p></td>
                          <td class="hidden-xs"><p>12-15 Reps</p></td>
                        </tr>
                        <tr class="quadriceps_exec_1">
                          <td data-title="Legs"><p>Leg Extensions</p></td>
                          <td data-title="Sets">
                            <p>
                              <ul class="list-unstyled">
                                <li>1 <span class="pull-right"><a href="#">Edit</a></span></li>
                                <li>2 <span class="pull-right"><a href="#">Edit</a></span></li>
                                <li>3 <span class="pull-right"><a href="#">Edit</a></span></li>
                                <li>1 <span class="pull-right"><a href="#">Edit</a></span></li>
                                <li>2 <span class="pull-right"><a href="#">Edit</a></span></li>
                                <li>3 <span class="pull-right"><a href="#">Edit</a></span></li>
                              </ul>
                            </p>
                          </td>
                          <td data-title="Weight">
                            <p>
                              <ul class="list-unstyled">
                                <li>10 lbs.</li>
                                <li>20 lbs.</li>
                                <li>40 lbs.</li>
                              </ul>
                            </p> 
                          </td>
                          <td data-title="Reps">
                            <p>
                              <ul class="list-unstyled">
                                <li>15</li>
                                <li>12</li>
                                <li>15</li>
                              </ul>
                            </p>  
                          </td>
                        </tr>
                        <tr class="quadriceps_exec_2">
                          <td data-title="Legs"><p>Leg Press</p></td>
                          <td data-title="Sets">
                            <p>
                              <ul class="list-unstyled">
                                <li>1 <span class="pull-right"><a href="#">Edit</a></span></li>
                                <li>2 <span class="pull-right"><a href="#">Edit</a></span></li>
                                <li>3 <span class="pull-right"><a href="#">Edit</a></span></li>
                              </ul>
                            </p>
                          </td>
                          <td data-title="Weight">
                            <p>
                              <ul class="list-unstyled">
                                <li>10 lbs.</li>
                                <li>20 lbs.</li>
                                <li>40 lbs.</li>
                              </ul>
                            </p> 
                          </td>
                          <td data-title="Reps">
                            <p>
                              <ul class="list-unstyled">
                                <li>15</li>
                                <li>12</li>
                                <li>15</li>
                              </ul>
                            </p>  
                          </td>
                        </tr>
                        <tr class="quadriceps_exec_3">
                          <td data-title="Legs"><p>Hack Squats</p></td>
                          <td data-title="Sets">
                            <p>
                              <button type="button" class="btn btn-sm btn-default btn-design">Log Food&nbsp;<span class="fa fa-caret-right"></span></button>
                            </p>
                          </td>
                          <td data-title="Weight">
                            <p>
                              &nbsp;
                            </p> 
                          </td>
                          <td data-title="Reps">
                            <p>
                              &nbsp;
                            </p>  
                          </td>
                        </tr>
                        <tr id="harmstring_exec_tr" class="second_sub_head">
                          <td class="slide_exec_resp_daily_td"><p>Hamstrings</p></td>
                          <td class="hidden-xs"><p>3 Sets</p></td>
                          <td class="hidden-xs"><p>&nbsp;</p></td>
                          <td class="hidden-xs"><p>12-15 Reps</p></td>
                        </tr>
                        <tr>
                          <td data-title="Legs"><p>Let Curis - Lying</p></td>
                          <td data-title="Sets">
                            <p>
                              <button type="button" class="btn btn-sm btn-default btn-design">Log Sets&nbsp;<span class="fa fa-caret-right"></span></button>
                            </p>
                          </td>
                          <td data-title="Weight">
                            <p>
                              &nbsp;
                            </p> 
                          </td>
                          <td data-title="Reps">
                            <p>
                              &nbsp;
                            </p>  
                          </td>
                        </tr>
                        <tr>
                          <td data-title="Legs"><p>Let Curis - Seated</p></td>
                          <td data-title="Sets">
                            <p>
                              <button type="button" class="btn btn-sm btn-default btn-design">Log Sets&nbsp;<span class="fa fa-caret-right"></span></button>
                            </p>
                          </td>
                          <td data-title="Weight">
                            <p>
                              &nbsp;
                            </p> 
                          </td>
                          <td data-title="Reps">
                            <p>
                              &nbsp;
                            </p>  
                          </td>
                        </tr>
                        <tr id="calves_exec_tr" class="second_sub_head">
                          <td class="slide_exec_resp_daily_td"><p>Calves</p></td>
                          <td><p>3 Sets</p></td>
                          <td><p>&nbsp;</p></td>
                          <td><p>12-15 Reps</p></td>
                        </tr>
                        <tr>
                          <td data-title="Legs"><p>Standing Calf Raises</p></td>
                          <td data-title="Sets">
                            <p>
                              <button type="button" class="btn btn-sm btn-default btn-design">Log Sets&nbsp;<span class="fa fa-caret-right"></span></button>
                            </p>
                          </td>
                          <td data-title="Weight">
                            <p>
                              &nbsp;
                            </p> 
                          </td>
                          <td data-title="Reps">
                            <p>
                              &nbsp;
                            </p>  
                          </td>
                        </tr>
                        <tr>
                          <td data-title="Legs"><p>Seated Calf Raises</p></td>
                          <td data-title="Sets">
                            <p>
                              <button type="button" class="btn btn-sm btn-default btn-design">Log Sets&nbsp;<span class="fa fa-caret-right"></span></button>
                            </p>
                          </td>
                          <td data-title="Weight">
                            <p>
                              &nbsp;
                            </p> 
                          </td>
                          <td data-title="Reps">
                            <p>
                              &nbsp;
                            </p>  
                          </td>
                        </tr>
                      </tbody>
                    </table>
                </div>

            </div>
          </div> -->
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
          <div class="panel panel-primary calorie_panel exer_goal_panel">
            <div class="panel-heading text-center calorie_panel_head">WEEKLY/MONTHLY</div>    
            <div class="panel-body calorie_panel_body calorie_log_calendar_panel">
              <div class="row">
                <div class="col-xs-6 col-sm-10 col-md-10 calendar_column_calorie">
                  <!-- <div id="exercise-log-calendar" class="hidden-xs"></div> -->
                  <div ng-model="dash_exercise.eventSourcesExercise"
                   ui-calendar="dash_exercise.uiConfigExercise.calendar" 
                   class="exericse_celendar_angular angular-calender hidden-xs"
                     calendar="exerciseCalendar"
                   ></div>
                  <div class="visible-xs">
                    <table class="table table-bordered calendar_blockequal left_side_table_cale exercise_fix_tr_height">
                     <thead class="table_head_colorie">
                       <tr>
                         <th>Month Average</th>
                       </tr>
                     </thead>
                     <tbody>
                       <tr class="tablecell_blockqual_2">
                         <td>
                           <p>First Week Average</p>
                            
                         </td>
                       </tr>
                       <tr class="tablecell_blockqual_2">
                         <td>
                           <p>Second Week Average</p>
                            
                         </td>
                       </tr>
                       <tr class="tablecell_blockqual_2">
                         <td>
                           <p>Third Week Average</p>
                            
                         </td>
                       </tr>
                       <tr class="tablecell_blockqual_2">
                         <td>
                           <p>Forth Week Average</p>
                            
                         </td>
                       </tr>
                       <tr class="tablecell_blockqual_2">
                         <td>
                           <p>Fifth Week Average</p>
                            
                         </td>
                       </tr>
                       <tr class="tablecell_blockqual_2">
                         <td>
                           <p>Sixth Week Average</p>  
                         </td>
                       </tr>
                     </tbody>
                    </table>
                  </div>
                </div>
                <div class="col-xs-6 col-sm-2 col-md-2 table_column_colorie exer_tbl_cal exelog_cal colorie_tbl new_size_exerice">
                  <table id="week_average_exercise" class="table table-bordered nomarginbot calendar_blockequal right_side_table_cale">
                     <thead class="table_head_colorie">
                       <tr>
                         <th class="caloriehead">Workouts/Week</th>
                       </tr>
                     </thead>
                     <tbody>
                       <tr class="tablecell_blockqual_2">
                         <td>
                          <div class="tabletd-div calendar-week-cell week_1">
                          </div>
                           <!-- <p><span class="success_green_text">SUCCESS</span><span class="pull-right">0 (0%)</span></p>
                           <p><span class="warn_orange_text">ADJUSTED</span><span class="pull-right">0 (0%)</span></p>
                           <p><span class="error_red_text">MISSED</span><span class="pull-right">1 (100%)</span></p> -->
                         </td>
                       </tr>
                       <tr class="tablecell_blockqual_2">
                         <td>
                            <div class="tabletd-div calendar-week-cell week_2">
                            </div>
                         </td>
                       </tr>
                       <tr class="tablecell_blockqual_2">
                         <td>
                            <div class="tabletd-div calendar-week-cell week_3">
                            </div>
                         </td>
                       </tr>
                       <tr class="tablecell_blockqual_2">
                         <td>
                           <div class="tabletd-div calendar-week-cell week_4">
                           </div>
                         </td>
                       </tr>
                       <tr class="tablecell_blockqual_2">
                         <td>
                           <div class="tabletd-div calendar-week-cell week_5">
                           </div>
                         </td>
                       </tr>
                       <tr class="tablecell_blockqual_2">
                         <td>
                           <div class="tabletd-div calendar-week-cell week_6">
                           </div>
                         </td>
                       </tr>
                     </tbody>
                  </table>
                </div>
              </div>
              <div class="row calorie_footer_table">
                <div class="col-xs-6 ol-sm-9 col-md-9 left_footer_col"><p class="boldclass">Month Average</p></div>
                <div class="col-xs-6 col-sm-3 col-md-3 right_footer_col">
                  
                  <!-- <p><span class="success_green_text">SUCCESS</span><span class="pull-right">0 (0%)</span></p>
                   <p><span class="warn_orange_text">ADJUSTED</span><span class="pull-right">0 (0%)</span></p>
                   <p><span class="error_red_text">MISSED</span><span class="pull-right">1 (100%)</span></p> -->
                   <p ng-bind-html="dash_exercise.monthlyExerciseCalories"></p>
                </div>
              </div>                   
            </div>
          </div>
        </div>
      </div>
    </div> 
  </div>
</section>