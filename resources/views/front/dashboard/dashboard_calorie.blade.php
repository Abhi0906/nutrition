<?php
      $first_name = isset($auth0_user_data['first_name'])?$auth0_user_data['first_name']:"";
      $last_name = isset($auth0_user_data['last_name'])?$auth0_user_data['last_name']:"";
      $gender = isset($auth0_user_data['gender'])?strtolower($auth0_user_data['gender']):"male";
      $birth_date = isset($auth0_user_data['birth_date'])?date('F d, Y',strtotime($auth0_user_data['birth_date'])): date("F d, Y", strtotime("-14 year", time()));
?>
<section class="calorie_log_summary_section" ng-controller="DashboardController as dash_calorie" ng-init="dash_calorie.userId='{{$user_id}}';dash_calorie.user_birth_date='{{$birth_date}}';  dash_calorie.getCaloriesLog(null);">
  <div id="calorie_log_div" class="panel panel-default out-row-panel">
   <div class="panel-body">
      <div class="row" >
        <div class="col-xs-12 col-sm-12 col-lg-12 col-lg-12">
          <div class="saperator_div"></div>
          <h1 class="text-center text-primary visible-xs"><strong>CALORIE LOG SUMMARY</strong></h1>
          @if(isset($auth0_user_data['email_varify']) && !($auth0_user_data['email_varify']))
          <div class="alert alert-info alert_box_info ">
            <div class="row">
              <div>
                <span id="passwordError" ng-hide="dash_calorie.emailValidation_email">
                  <p><large class="text-danger">@{{dash_calorie.emailValidationMesg_email}} </large></p>
                </span>
                <span id="passwordSuccess" ng-show="dash_calorie.emailSuccessMsg_email">
                  <p><large class="text-success">@{{dash_calorie.emailSuccessMsg_email}}</large></p>
                </span>
              </div>
              <div class="col-sm-9">
                Please check your email for verification. You can access only for 7 days.
              </div>
              <div class="col-sm-3 text-right">
                <button class="btn btn-primary btndesign">Resend Email</button>
              </div>
            </div>
          </div>
          @endif
        </div>
      </div>
      <div class="row">
       <!--  <div class="col-xs-12 text-right">
          <button class="btn btn-primary btndesign" data-toggle="modal" data-target="#profile_modal">Profile</button>
          <button class="btn btn-primary btndesign" data-toggle="modal" data-target="#warning_modal">Warning</button>
        </div> -->

        <!-- profile modal -->
        <div id="profile_modal" class="modal fade flat_inputbox flat_modalbox"  tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false" style="z-index:9999">
          <div class="modal-dialog">
             <div class="panel panel-default panel_modal_contain">
                <div class="panel-heading panel_modal_head box_head_background">
                    <h4 class="modal-title modal_title_head text-center">PROFILE INFORMATION</h4>
                </div>
                <div class="panel-body">
                  <form name="userProfileForm" class="form-horizontal profile_modal_dash" role="form" novalidate="novalidate">
                    <div class="form-group" ng-init="dash_calorie.profile_data.first_name ='{{$first_name}}'">
                      <label class="control-label col-sm-3" for="firstname">Firstname*:</label>
                      <div class="col-sm-9">
                        <!-- <input type="email" class="form-control" id="firstname" placeholder="Enter firstname"> -->
                        <input type="text"
                                 name="first_name"
                                 ng-model="dash_calorie.profile_data.first_name"
                                 ng-minlength="3"
                                 maxlength="30"
                                 class="form-control"
                                 placeholder="Enter firstname"
                                 required
                                  >

                                <span ng-show="userProfileForm.first_name.$invalid && userProfileForm.first_name.$touched">
                                <span class="help-block animation-slideDown" ><span class="text-danger">FirstName is Required</span></span>
                      </div>
                      <!-- <div class="input-group">
                        <span class="input-group-addon"><i class=""></i></span>
                        <span ng-show="userProfileForm.first_name.$invalid && userProfileForm.first_name.$touched">
                        <span class="help-block animation-slideDown" ><span class="text-danger">FirstName is Required</span></span>
                        </span>
                     </div> -->
                    </div>
                    <div class="form-group"  ng-init="dash_calorie.profile_data.last_name ='{{$last_name}}'">
                      <label class="control-label col-sm-3" for="lastname">Lastname*:</label>
                      <div class="col-sm-9">
                        <!-- <input type="text" class="form-control" id="lastname" placeholder="Enter lastname"> -->
                        <input type="text"
                              name="last_name"
                              maxlength="30"
                              ng-model="dash_calorie.profile_data.last_name"
                              class="form-control"
                              placeholder="Enter lastname"
                              required>

                              <span ng-show="userProfileForm.last_name.$invalid && userProfileForm.last_name.$touched">
                              <span class="help-block animation-slideDown" ><span class="text-danger">LastName is Required</span></span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-sm-3" for="lastname">Birth Date*:</label>
                      <div class="col-sm-9">
                        <div id="profile_bd_datepicker" class="input-group date sleepboxdatepicker">
                              <ul class="list-inline btn-ul-date">
                                <li>
                                  <input type="text"
                                      class="form-control"
                                      uib-datepicker-popup = "MM-dd-yyyy"
                                      ng-model="dash_calorie.birth_date"
                                      is-open="dash_calorie.profile_data_birth_date_popup.opened"
                                      datepicker-options="dash_calorie.dateOptions"
                                      ng-required="true"
                                      show-button-bar=false
                                      close-text="Close"
                                      placeholder="MM-DD-YYYY"
                                      name="birth_date"
                                      id="birth_date"  />
                                  </li>
                                  <li>
                                  <span class="caledar_custom fa fa-calendar" ng-click="dash_calorie.profile_data_birth_date_open()">
                                  </span>
                                </li>
                              </ul>

                             <div ng-show="userProfileForm.birth_date.$invalid && userProfileForm.birth_date.$touched" class="help-block animation-slideDown"><span class="text-danger">Enter your Birth date</span></div>
                             <div ng-show="dash_calorie.ageValidation" class="help-block animation-slideDown"><span class="text-danger">You must be at least 13 years of age to register</span></div>
                        </div>
                        <!-- <div class="input-group">
                              <span class="input-group-addon"><i class=""></i></span>
                              <div ng-show="dash_calorie.ageValidation" class="help-block animation-slideDown"><span class="text-danger">You must be at least 13 years of age to register</span></div>
                            </span>
                        </div> -->
                      </div>
                    </div>
                    <div class="form-group" ng-init="dash_calorie.profile_data.gender ='{{$gender}}'">
                      <label class="control-label col-sm-3" for="gender">Gender*:</label>
                      <div class="radio col-sm-9">
                        <label ng-init="dash_calorie.profile_data.gender ='male'">
                          <input type="radio" id="male"  ng-model="dash_calorie.profile_data.gender"  value="male" name="gender">
                          Male
                        </label>&nbsp;&nbsp;
                        <label>
                          <input type="radio" id="female" ng-model="dash_calorie.profile_data.gender"   value="female" name="gender">
                          Female</label>

                          <span  class="text-danger text-center" ng-show="userProfileForm.gender.$invalid && userProfileForm.gender.$touched">Select any gender</span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-sm-3" for="height">Height*:</label>
                      <div class="col-sm-9">
                        <div class="row">
                          <div class="col-xs-6 col-sm-6">
                            <!-- <input type="text" class="form-control" id="height" placeholder="Inch">   -->

                            <div class="input-group">
                                <input type="text" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/"
                                                step="0.01" number-validation="number-validation"
                                                maxlength="5" name="height_in_feet"
                                                ng-model="dash_calorie.profile_data.height_in_feet"
                                                class="form-control" required>
                                                <span class="input-group-addon btndesign">Feet</span>
                            </div>

                            <span ng-show="userProfileForm.height_in_feet.$invalid && userProfileForm.height_in_feet.$touched">
                            <span class="help-block animation-slideDown" ><span class="text-danger">Enter valid Height</span></span>


                          </div>
                          <div class="col-xs-6 col-sm-6">
                            <!-- <input type="text" class="form-control" id="height1" placeholder="Feet"> -->
                            <div class="input-group">
                              <input type="text"  ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/"
                                                step="0.01"
                                                number-validation="number-validation"
                                                maxlength="6"
                                                name="height_in_inch"
                                                ng-model="dash_calorie.profile_data.height_in_inch"
                                                class="form-control" required>
                                                <span class="input-group-addon btndesign">Inch</span>
                            </div>
                            <span ng-show="userProfileForm.height_in_inch.$invalid && userProfileForm.height_in_inch.$touched">
                            <span class="help-block animation-slideDown" ><span class="text-danger">Enter valid Height</span></span>
                          </div>

                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-sm-3" for="weight">Weight*:</label>
                      <div class="col-sm-9">
                        <!-- <input type="text" class="form-control" id="weight" placeholder="Enter Weight"> -->
                        <div class="input-group">
                        <input type="text" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/"
                                            step="0.01"
                                            number-validation="number-validation"
                                            maxlength="6"
                                            name="baseline_weight"
                                            ng-model="dash_calorie.profile_data.baseline_weight"
                                            class="form-control"
                                            placeholder="Enter Weight" required>
                                          <span class="input-group-addon btndesign">lbs</span>
                        </div>

                                            <span ng-show="userProfileForm.baseline_weight.$invalid && userProfileForm.baseline_weight.$touched">
                                            <span class="help-block animation-slideDown" ><span class="text-danger">Weight is Required</span></span>
                      </div>


                    </div>

                    <div class="form-group">
                      <div class="col-sm-offset-3 col-sm-9">
                        <button type="submit" class="btn btn-primary btndesign1"  ng-disabled="userProfileForm.$invalid"  ng-click="dash_calorie.updateUserInfo()">Save</button>
                      </div>
                    </div>
                  </form>
                </div>
            </div>
          </div>
        </div>
        <!-- end of profile modal -->

        <!-- warning modal -->
        <div id="warning_modal" class="modal fade" role="dialog" style="z-index:9999">
          <div class="modal-dialog">
             <div class="panel panel-default panel_modal_contain_warn">
              <div class="panel-heading panel_modal_contain_warn_head">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title modal_title_head text-center">WARNING !</h4>
              </div>
              <div class="panel-body">
                  <span id="passwordError" ng-hide="dash_calorie.emailValidation_email">
                    <p><large class="text-danger">@{{dash_calorie.emailValidationMesg_email}} </large></p>
                  </span>
                  <span id="passwordSuccess" ng-show="dash_calorie.emailSuccessMsg_email">
                    <p><large class="text-success">@{{dash_calorie.emailSuccessMsg_email}}</large></p>
                  </span>
                 <div class="warn_message_modal text-center">Please, you must verify your email address before you can continue. Please check you email for the verify account.
                 </div>

                 <div class="warn_btn_modal text-center">
                   <a class="btn btn-primary btndesign" href="{{URL::to('/auth0/logout')}}">Logout</a>&nbsp;
                   <button class="btn btn-primary btndesign" ng-click="dash_calorie.resend_email()">Resend Email</button>
                 </div>
              </div>
            </div>
          </div>
        </div>
        <!-- warning modal -->

      </div>

      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">

          <!-- calorie log summary daily -->
          <div class="panel panel-default calorie_panel colorie_padding_bottom">
            <div class="panel-heading text-center calorie_panel_head">DAILY</div>
            <div class="panel-body calorie_panel_body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="text-center datepicker_contain">
                      <ul class="list-inline btn-ul-date">
                        <li>
                          <a href="javascript:void(0);" ng-click="dash_calorie.dateNextPrevious('prev', dash_calorie.daily_calorie_date, 'calorie')">
                              <span id="cal_summary_date_prev"  class="fa fa-chevron-left"></span>
                          </a>
                          <input type="text"
                                class="datepicker_calorie"
                                uib-datepicker-popup = "@{{dash_calorie.format}}"
                                ng-model="dash_calorie.daily_calorie_date"
                                is-open="dash_calorie.daily_calorie_date_popup.opened"
                                datepicker-options="dash_calorie.dateOptions"
                                ng-required="true"
                                show-button-bar=false
                                close-text="Close"
                                ng-change="dash_calorie.getCaloriesLog(dash_calorie.daily_calorie_date)" />
                        </li>
                        <li>
                          <a href="javascript:void(0);" ng-click="dash_calorie.dateNextPrevious('next', dash_calorie.daily_calorie_date, 'calorie')">
                              <span id="cal_summary_date_next" class="fa fa-chevron-right"></span>&nbsp;&nbsp;
                          </a>
                          <span class="caledar_custom fa fa-calendar" ng-click="dash_calorie.daily_calorie_date_open()">
                          </span>
                        </li>
                      </ul>
                    </div>
                  </div>

                </div>
                <div class="row row-eq-height bg-info headrow-samecolumnheight daily_subhead">
                  <div class="col-xs-6 col-sm-4 health-sub-heading">

                  </div>
                  <div class="col-xs-6 col-sm-4 health-sub-heading">
                    <h4>Actual</h4>
                  </div>
                  <div class="hidden-xs col-sm-4 health-sub-heading">
                    <h4>Goal</h4>
                  </div>
                </div>

                <div class="row row-eq-height headrow-samecolumnheight">
                  <div class="col-xs-6 col-sm-4 health-sub-contain">
                      <p class="boldclass">Calories Consumed</p>
                      <div class="guagemetercontain">
                        <ul class="list-inline">
                            <li>
                                <div id="calories-gauge" class="calorielog-gauge1 guagemeterdemo"></div>
                            </li>
                            <li>
                                <h5 class="textcolor"><strong><span class="textfont">@{{dash_calorie.calories_consumed_persent}} %</span> OF GOAL</strong></h5>
                            </li>
                        </ul>
                      </div>
                      <p class="boldclass visible-xs">Goal : @{{dash_calorie.calorie_goal}}</p>
                      <div class="bottom_line"></div>
                  </div>
                  <div class="col-xs-6 col-sm-4 health-sub-contain">
                    <p class="boldclass">Log Food-: @{{dash_calorie.calories_consumed}}</p>
                    <br/>
                    <a class="btn btn-sm btn-default btn-design" href="/food">Log Food&nbsp;<span class="fa fa-caret-right"></span></a>

                    <div class="bottom_line"></div>
                  </div>
                  <div class="hidden-xs col-sm-4 health-sub-contain borderrightnone">
                    <p class="boldclass">@{{dash_calorie.calorie_goal}}</p>

                    <div class="bottom_line"></div>
                  </div>
                </div>

                <div class="row row-eq-height headrow-samecolumnheight">
                  <div class="col-xs-6 col-sm-4 health-sub-contain">
                    <p class="boldclass">Calories Burned</p>
                    <div class="guagemetercontain">
                        <ul class="list-inline">
                            <li>
                                <!-- <canvas id="gauge1" class="gaugemeter"></canvas> -->
                                <!-- <div id="calories-gauge123" meter-value="dash_calorie.gauge_meter_food_value" class="calorielog-gauge1 guagemeterdemo" gauge-meter></div> -->
                                <div id="calories-burned" class="calorielog-gauge1 guagemeterdemo"></div>
                            </li>
                            <li>
                                <h5 class="textcolor"><strong><span class="textfont">@{{dash_calorie.calories_burned_persent}} %</span> OF GOAL</strong></h5>
                            </li>
                        </ul>
                      </div>
                      <p class="boldclass visible-xs">Goal : @{{dash_calorie.exercise_goal}}</p>
                      <div class="bottom_line"></div>
                  </div>
                  <div class="col-xs-6 col-sm-4 health-sub-contain">
                    <p class="boldclass">Exercise-: @{{dash_calorie.calories_burned}}</p>
                    <br/>
                    <a class="btn btn-sm btn-default btn-design" href="/exercise">Log Exercise&nbsp;<span class="fa fa-caret-right"></span></a>
                    <!-- <p>or</p>
                    <button type="button" class="btn btn-sm btn-default btn-design">Sync with Device&nbsp;<span class="fa fa-caret-down"></span></button> -->
                    <div class="bottom_line"></div>
                  </div>
                  <div class="hidden-xs col-sm-4 health-sub-contain">
                    <p class="boldclass">@{{dash_calorie.exercise_goal}} </p>
                    <div class="bottom_line"></div>
                  </div>
                </div>
                <div class="row row-eq-height headrow-samecolumnheight">
                  <div class="col-xs-6 col-sm-4 health-sub-contain">
                    <p class="boldclass">Calories Total</p>
                    <div class="guagemetercontain">
                        <ul class="list-inline">
                            <li>
                                <!-- <canvas id="gauge1" class="gaugemeter"></canvas> -->
                                <!-- <div id="calories-gauge123" meter-value="dash_calorie.gauge_meter_food_value" class="calorielog-gauge1 guagemeterdemo" gauge-meter></div> -->
                                <div id="calories_total" class="calorielog-gauge1 guagemeterdemo"></div>
                            </li>
                            <li>
                                <h5 class="textcolor"><strong><span class="textfont">@{{dash_calorie.total_calories_persent}} %</span> OF GOAL</strong></h5>
                            </li>
                        </ul>
                      </div>
                      <p class="boldclass visible-xs">Goal : @{{dash_calorie.total_calorie_goal}}</p>
                  </div>
                  <div class="col-xs-6 col-sm-4 health-sub-contain">
                    <p class="boldclass">@{{dash_calorie.total_calories}}</p>
                  </div>
                  <div class="hidden-xs col-sm-4 health-sub-contain">
                    <p class="boldclass">@{{dash_calorie.total_calorie_goal}}</p>
                  </div>
                </div>
            </div>
          </div>
          <!-- end of calorie log summary  -->

          <!-- Add Water Cup of calorie log summary -->
          <div class="panel panel-default calorielog_footer_blockqual">
            <div class="panel-body">
              <div class="row">
                <div class="col-sm-12">
                  <p class="boldclass">@{{dash_calorie.total_water_to_drink | number:0 }} Cup of Water to Drink&nbsp;&nbsp;
                    <!-- <span class="glyphicon glyphicon-glass"></span>-->
                    <span ng-bind-html="dash_calorie.cupDisplay"></span>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <!-- end of Add water cup -->

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
          <!-- calarie log summary calendar -->
          <div class="panel panel-primary calorie_panel">
            <div class="panel-heading text-center calorie_panel_head">WEEKLY/MONTHLY</div>
            <div class="panel-body calorie_panel_body calorie_log_calendar_panel">
              <div class="row">
                <div class="col-xs-6 col-sm-10 col-md-10 calendar_column_calorie">
                  <!-- <div id="calorie-log-calendar" class="hidden-xs"></div> -->
                  <div ng-model="dash_calorie.eventSources"
                   ui-calendar="dash_calorie.uiConfig.calendar"
                    class="calorie_celendar_angular angular-calender hidden-xs"
                      calendar="calorieCalendar"
                    ></div>
                  <div class="visible-xs">
                    <table class="table table-bordered calendar_blockequal left_side_table_cale calorie_fix_tr_height">
                     <thead class="table_head_colorie">
                       <tr>
                         <th>Month Average</th>
                       </tr>
                     </thead>
                     <tbody>
                       <tr class="tablecell_blockqual">
                         <td>
                           <p>First Week Average</p>

                         </td>
                       </tr>
                       <tr class="tablecell_blockqual">
                         <td>
                           <p>Second Week Average</p>

                         </td>
                       </tr>
                       <tr class="tablecell_blockqual">
                         <td>
                           <p>Third Week Average</p>

                         </td>
                       </tr>
                       <tr class="tablecell_blockqual">
                         <td>
                           <p>Forth Week Average</p>

                         </td>
                       </tr>
                       <tr class="tablecell_blockqual">
                         <td>
                           <p>Fifth Week Average</p>

                         </td>
                       </tr>
                       <tr class="tablecell_blockqual">
                         <td>
                           <p>Sixth Week Average</p>

                         </td>
                       </tr>
                     </tbody>
                    </table>
                  </div>
                </div>
                <div class="col-xs-6 col-sm-2 col-md-2 table_column_colorie colorie_tbl new_size_colorie">
                  <table id="week_average" class="table table-bordered calendar_blockequal right_side_table_cale">
                     <thead class="table_head_colorie">
                       <tr>
                         <th class="caloriehead">Week Average</th>
                       </tr>
                     </thead>
                     <tbody>
                       <tr class="tablecell_blockqual">
                         <td>
                            <div class="tabletd-div calendar-week-cell week_1">
                              &nbsp;
                            </div>
                         </td>
                       </tr>
                       <tr class="tablecell_blockqual">
                         <td>
                           <div class="tabletd-div calendar-week-cell week_2">
                              &nbsp;
                            </div>
                         </td>
                       </tr>
                       <tr class="tablecell_blockqual">
                         <td>
                           <div class="tabletd-div calendar-week-cell week_3">
                              &nbsp;
                            </div>
                         </td>
                       </tr>
                       <tr class="tablecell_blockqual">
                         <td>
                           <div class="tabletd-div calendar-week-cell week_4">
                              &nbsp;
                            </div>
                         </td>
                       </tr>
                       <tr class="tablecell_blockqual">
                         <td>
                           <div class="tabletd-div calendar-week-cell week_5">
                              &nbsp;
                            </div>
                         </td>
                       </tr>
                       <tr class="tablecell_blockqual">
                         <td>
                           <div class="tabletd-div calendar-week-cell week_6">
                              &nbsp;
                            </div>
                         </td>
                       </tr>
                     </tbody>
                  </table>
                </div>
              </div>
              <div class="row calorie_footer_table calorielog_footer_blockqual">
                <div class="col-xs-6 col-sm-9 col-md-9 left_footer_col"><p class="boldclass">Month Average</p></div>
                <div id="month-average-calorie" class="col-xs-6 col-sm-3 col-md-3 right_footer_col">
                  <!-- <p>Calorie Average</p>
                   <p class="boldclass">1000/day</p>
                   <p class="successcolor">Defict = -500</p>
                   <p>Water = 8 Cups</p> -->
                </div>
              </div>
            </div>
          </div>
          <!-- end of calorie log summary -->

        </div>
      </div>
    </div>
  </div>
</section>