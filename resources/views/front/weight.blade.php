@extends('layout.master')
@section('title')
  Weight and Body Measurement
@endsection 


@section('custom-css')
 <style type="text/css">
  .weight_mesaure_greenlabel
    {
      color: #006600;
      font-weight: bold;
      font-size: 14px;
      text-align: center;
      vertical-align: middle !important;
    }
    .left_align{
      text-align: left!important;
    }
    .no-padd-right
    {
      padding-right: 0px !important;
    }
</style>
@endsection 
@section('content') 

<!-- Start Weight page -->
<section ng-controller="WeightController as weight" ng-init="weight.userId='{{$user_id}}'; weight.loggedWeight(); weight.loggedBodyMeasurements()" class="section-panel custom-section-panel">
   <!-- header title main -->
   
   <div class="row main_title_all_row">
      <div class="col-xs-12 text-center">
         <h2 class="text-primary head_title_main">YOUR W<span class="title_head_line">EIGHT</span> DIARY</h2>
      </div>     
   </div>
   <div class="row">
     <div class="col-xs-12">
       <span class="text-light-blue">Last Log Date: @{{weight.last_log_date | date:'MMM-dd-yy'}} </span>
       <a href="/withings_callback" class="btn btn-primary btndesign logdate_btn"><i class="fa fa-refresh"></i>&nbsp; Sync with device</a>
      </div>
   </div>

   <!-- End of header main title -->
   <!-- weight diary tables row -->
   <div class="row tab_chart_row">
      <!-- Weight portion start-->
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 wight_body_portion">
         <div class="panel panel-primary weight_body_panel weight_diary_panel_size">
            <div class="panel-heading text-center panel_head_background">WEIGHT & BODY FAT %</div>
            <div class="panel-body panel-container panel_Bodypart">
               <div class="row weight_body_fat_row">
                  <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 datebox weight_body_part1">
                     <form name="LogWeightForm" novalidate="novalidate" class="animation-fadeInQuick">
                     <table class="table body_weight_goal_table w_goal_m_othertable">
                        <tbody>
                           <tr>
                              <th>
                                 <p class="text-primary theme_blue_color">DATE:</p>
                                 <div class="form-group">
                                    <div id="datepicker1" class="input-group date white_datepickterbox">
                                             <input type="text"
                                              class="form-control" 
                                              uib-datepicker-popup = "@{{weight.format}}"
                                               ng-model="weight.log_weight.log_date"
                                               is-open="weight.weight_log_date_popup.opened"
                                               datepicker-options="weight.dateOptions"
                                               ng-required="true"
                                               show-button-bar=false
                                               close-text="Close" />
                                                <span class="input-group-addon caledar_custom" ng-click="weight.weight_log_date_open()"> 
                                                   <i aria-hidden="true" class="fa fa-calendar"></i>
                                                </span>
                                    </div>
                                 </div>
                                 <div class="saperator"></div>
                                 <p class="text-primary theme_blue_color">WEIGHT:</p>
                                    <div class="input-group date datepicker_new_design">
                                    <input type="text" number-validation="number-validation"   
                                       maxlength="6" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" 
                                       ng-model="weight.log_weight.current_weight" 
                                       placeholder="Weight" name="current_weight" id="current_weight" class="form-control" required>
                                       <span class="input-group-addon">lbs</span>
                                        </div>


                                 <div class="saperator"></div>
                                 <p class="text-primary theme_blue_color">BODY FAT:</p>
                                 <div class="input-group date datepicker_new_design">
                                    <input type="text" number-validation="number-validation"
                                       ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" 
                                       ng-model="weight.log_weight.body_fat"  maxlength="5" 
                                       ng-blur="weight.validBodyFat(weight.log_weight.body_fat);"
                                       ng-keyup="weight.validBodyFat(weight.log_weight.body_fat);"
                                       placeholder="Body Fat" name="body_fat" id="body_fat" class="form-control" required>
                                      <span class="input-group-addon">%</span>
                                      </div>
                                 <div class="saperator"></div>
                              </th>
                           </tr>
                           <tr>
                                 <td class="last_btnbottom"><button class="btn btn-primary btn-block btndesign1" ng-disabled="LogWeightForm.$invalid || weight.log_weight_error" ng-click="weight.logWeight();">Log</button></td>
                           </tr>
                        </tbody>
                     </table>
                     </form>
                  </div>
                  <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 weight_body_part2">
                     <table class="table body_weight_goal_table">
                        <tbody>
                           <tr>
                              <th>
                                 <p class="text-primary theme_blue_color">GOAL DATE:</p>
                                 <p>@{{weight.weight_goal_date}}</p>
                              </th>
                           </tr>
                           <tr>
                              <td>
                                 <p class="text-primary theme_blue_color">GOAL WEIGHT:</p>
                                 <p>@{{weight.weight_goal | number:0}} LBS</p>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <p class="text-primary theme_blue_color">GOAL BODY FAT:</p>
                                 <p>@{{weight.body_fat_goal}} %</p>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <p class="weight_mesaure_redlabel last_btnbottom">@{{weight.weight_goal_days_left}} days left</p>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center weight_body_part3">
                     <table class="table body_weight_goal_table w_goal_m_othertable not-full_tbl_height">
                        <tr>
                           <th>
                              <p class="text-primary text-center theme_blue_color">HISTORY (LAST 4 LOGS)</p>
                              <table class="table table-bordered history_table">
                                <thead>
                                 <tr>
                                    <th>Date</th>
                                    <th>Weight</th>
                                    <th>Body Fat %</th>
                                 </tr>
                                 </thead>
                                 <tbody>
                                 <tr ng-repeat="loggedWeight in weight.user_weights_history | orderBy:'log_date':true | limitTo:4 ">
                                    <td>@{{loggedWeight.log_date | date:'MM-dd-yy'}}</td>
                                    <td>@{{loggedWeight.current_weight | number:0}} <span ng-if="loggedWeight.current_weight">lbs</span></td>
                                    <td>@{{loggedWeight.body_fat | number:0}} <span ng-if="loggedWeight.body_fat">%</span></td>
                                 </tr>
                                 </tbody>
                              </table>
                           </th>
                        </tr>
                        <tr>
                           <td class="last_btnbottom history_btn">
                              <button class="btn btn-primary btndesign">See All</button>
                           </td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- end of weight fat portion page -->
      <!-- body measurement portion  -->
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-7 body_measure_portion">
         <div class="panel panel-primary body_measure_panel weight_body_panel weight_diary_panel_size">
            <div class="panel-heading text-center text-primary panel_head_background">BODY MEASUREMENTS</div>
            <div class="panel-body panel-container panel_Bodypart">
               <div class="row body_measure_row">
                  <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2 datebox body_meausre_part1">
                     <form name="BodyMeasurementForm" novalidate="novalidate" class="animation-fadeInQuick">
                     <table class="table body_weight_goal_table w_goal_m_othertable">
                        <tbody>
                           <tr>
                              <th>
                                 <p class="text-primary theme_blue_color">DATE:</p>
                                 <p>
                                 <div class="form-group">
                                    <div id="" class="input-group date white_datepickterbox">
                                       <input type="text"
                                              class="form-control" 
                                              uib-datepicker-popup = "@{{weight.format}}"
                                               ng-model="weight.body_measurements.log_date"
                                               is-open="weight.body_measurements_date_popup.opened"
                                               datepicker-options="weight.dateOptions"
                                               ng-required="true"
                                               show-button-bar=false
                                               close-text="Close" />
                                                <span class="input-group-addon caledar_custom" ng-click="weight.body_measurements_date_open()"> 
                                                   <i aria-hidden="true" class="fa fa-calendar"></i>
                                                </span>
                                    </div>
                                 </div>
                                 </p>
                                    <p>
                                      <div class="input-group date datepicker_new_design">
                                       <input type="text" number-validation="number-validation"   
                                       maxlength="6" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" 
                                       ng-model="weight.body_measurements.neck" 
                                       placeholder="Neck" name="neck" id="neck" class="form-control" required>
                                       <span class="input-group-addon">in.</span>
                                        </div>
                                    </p>
                                    <p>
                                       <div class="input-group date datepicker_new_design">
                                       <input type="text" number-validation="number-validation"   
                                       maxlength="6" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" 
                                       ng-model="weight.body_measurements.arms" 
                                       placeholder="Arms" name="arms" id="arms" class="form-control" required>
                                       <span class="input-group-addon">in.</span>
                                        </div>
                                    </p>
                                    <p>
                                       <div class="input-group date datepicker_new_design">
                                       <input type="text" number-validation="number-validation"   
                                       maxlength="6" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" 
                                       ng-model="weight.body_measurements.waist" 
                                       placeholder="Waist" name="waist" id="waist" class="form-control" required>
                                       <span class="input-group-addon">in.</span>
                                        </div>
                                    </p>
                                    <p>
                                       <div class="input-group date datepicker_new_design">
                                       <input type="text" number-validation="number-validation"   
                                       maxlength="6" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" 
                                       ng-model="weight.body_measurements.hips" 
                                       placeholder="Hips" name="hips" id="hips" class="form-control" required>
                                       <span class="input-group-addon">in.</span>
                                        </div>
                                    </p>
                                    <p>
                                       <div class="input-group date datepicker_new_design">
                                       <input type="text" number-validation="number-validation"   
                                       maxlength="6" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" 
                                       ng-model="weight.body_measurements.legs" 
                                       placeholder="Legs" name="legs" id="legs" class="form-control" required>   
                                       <span class="input-group-addon">in.</span>
                                        </div>
                                    </p>
                              </th>
                           </tr>
                           <tr>
                                 <td class="last_btnbottom"><button class="btn  btn-primary btn-block btndesign1" ng-disabled="BodyMeasurementForm.$invalid" ng-click="weight.logBodyMeasurements();">Log</button></td>
                           </tr>
                        </tbody>
                     </table>
                     </form>
                  </div>
                  <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2 body_meausre_part2">
                     <table class="table body_weight_goal_table">
                        <tbody>
                           <tr>
                              <th>
                                 <p class="text-primary theme_blue_color">GOAL DATE:</p>
                                 <p>@{{weight.body_measurement_goal_date}}</p>
                              </th>
                           </tr>
                           <tr>
                              <td>
                                 <p class="theme_blue_color">Neck Goal<span class="pull-right">@{{weight.body_measurement_goal_data.neck_g}}</span></p>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <p class="theme_blue_color">Arms Goal<span class="pull-right">@{{weight.body_measurement_goal_data.arms_g}}</span></p>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <p class="theme_blue_color">Waist Goal<span class="pull-right">@{{weight.body_measurement_goal_data.waist_g}}</span></p>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <p class="theme_blue_color">Hips Goal<span class="pull-right">@{{weight.body_measurement_goal_data.hips_g}}</span></p>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <p class="theme_blue_color">Legs Goal<span class="pull-right">@{{weight.body_measurement_goal_data.legs_g}}</span></p>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <p class="weight_mesaure_redlabel last_btnbottom">@{{weight.body_measurement_goal_days_left}} days left</p>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-8 text-center body_meausre_part3">
                     <table class="table body_weight_goal_table w_goal_m_othertable not-full_tbl_height">
                        <tbody>
                           <tr>
                              <th>
                                 <p class="text-primary text-center theme_blue_color">HISTORY (LAST 4 LOGS)</p>
                                 <div id="history_table_bodymeasure">
                                    <table class="table table-bordered history_table">
                                       <thead>
                                       <tr>
                                          <th class="text-center">Date</th>
                                          <th class="text-center">Neck</th>
                                          <th class="text-center">Arms</th>
                                          <th class="text-center">Waist</th>
                                          <th class="text-center">Hips</th>
                                          <th class="text-center">Legs</th>
                                       </tr>
                                       </thead>
                                       <tbody>
                                       <tr ng-repeat="measurement in weight.user_measurements_history | orderBy:'log_date':true | limitTo:4 ">
                                          <td id="history_tb_1st_td" data-title="Date" class="histoty_tbl_resp_td">@{{measurement.log_date | date:'MM-dd-yy'}}</td>
                                          <td data-title="Neck">@{{measurement.neck}}</td>
                                          <td data-title="Arms">@{{measurement.arms}}</td>
                                          <td data-title="Waits">@{{measurement.waist}}</td>
                                          <td data-title="Hips">@{{measurement.hips}}</td>
                                          <td data-title="Legs">@{{measurement.legs}}</td>
                                       </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </th>
                           </tr>
                            
                           <tr>
                              <td class="last_btnbottom history_btn"><button class="btn btn-primary btndesign">See All</button></td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- end of body measurement portion -->
   </div>
   <!-- end of weight diary tables -->
   <!-- chart row  -->
   <div class="row tab_chart_row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 tab_chart_part1">
         <div class="chartleft_contain">
            <ul class="nav nav-tabs">
               <li class="active"><a data-toggle="tab" href="#weight_tab_id" ng-click="weight.weightCharts('weight')">Weight</a></li>
               <li><a data-toggle="tab" href="#leanvsfat_tab_id" ng-click="weight.weightCharts('lean_fat')">Lean Vs Fat</a></li>
               <li><a data-toggle="tab" href="#bmi_tab_id" ng-click="weight.weightCharts('bmi')">BMI</a></li>
            </ul>
            <div class="tab-content">
               <div class="datepicker_contain_chart">

                  <div class="row">
                  <div class="col-sm-8">
                     <ul class="list-inline datepicker_ul left_align">
                        <li class="date_contain_li">
                           <div class="form-group">
                              <div id="datepicker3" class="input-group date white_datepickterbox">
                                 
                                  <input type="text"
                                                 class="form-control" 
                                                 uib-datepicker-popup = "@{{weight.format}}"
                                                  ng-model="weight.weight_start_date"
                                                  is-open="weight.weight_start_date_popup.opened"
                                                  datepicker-options="weight.dateOptions"
                                                  ng-required="true"
                                                  ng-change="weight.loggedWeight('change');"
                                                  show-button-bar=false
                                                  close-text="Close" />
                                                   <span class="input-group-addon caledar_custom" ng-click="weight.weight_start_date_open()"> 
                                                      <i aria-hidden="true" class="fa fa-calendar"></i>
                                                   </span>
                              </div>
                           </div>
                        </li>
                        <li><label class="data_label label_contain_li">to</label></li>
                        <li class="date_contain_li">
                           <div class="form-group">
                              <div id="" class="input-group date white_datepickterbox">
                                 <input type="text"
                                                 class="form-control" 
                                                 uib-datepicker-popup = "@{{weight.format}}"
                                                  ng-model="weight.weight_end_date"
                                                  is-open="weight.weight_end_date_popup.opened"
                                                  datepicker-options="weight.dateOptions"
                                                  ng-required="true"
                                                  ng-change="weight.loggedWeight('change');"
                                                  show-button-bar=false
                                                  close-text="Close" />
                                                  <span class="input-group-addon caledar_custom" ng-click="weight.weight_end_date_open()"> 
                                                      <i aria-hidden="true" class="fa fa-calendar"></i>
                                                  </span>
                              </div>
                           </div>
                        </li>
                     </ul>   
                  </div>
                  <!-- <div class="col-sm-4 text-right">
                     <ul class="list-inline no-padd-right">
                        <li><label ng-if="weight.weightGraph !='bmi'" class="data_label label_contain_li middle_lbl_li"><a class="text-primary">Change</a> 
                           <span ng-if="weight.weightGraph!='lean_fat'" class="weight_mesaure_@{{weight.diff_class}}label">@{{weight.weight_chart_diff}}</span> 
                           <span ng-if="weight.weightGraph!='lean_fat'"  class="weight_mesaure_@{{weight.diff_class}}label glyphicon glyphicon-arrow-@{{weight.weight_diff_arrow}}"></span>
                         
                           <span ng-if="weight.weightGraph=='lean_fat'" class="weight_mesaure_@{{weight.diff_class}}label">@{{weight.fat_chart_diff}}</span> 
                           <span ng-if="weight.weightGraph=='lean_fat'" class="weight_mesaure_@{{weight.diff_class}}label glyphicon glyphicon-arrow-@{{weight.fat_diff_arrow}}"></span>
                        </label>
                        </li>
                     </ul>
                  </div> -->
               </div>




               </div>
               <div id="weight_tab_id" class="tab-pane fade in active">
                 
                  <div class="chart_contain_part">
                     <div id="weight_chart" class="firstdiv" ></div>
                  </div>
               </div>
               <div id="leanvsfat_tab_id" class="tab-pane fade">
                
                  <div class="chart_contain_part">
                     <div id="lean_fat_chart" class="firstdiv" ></div>
                  </div>
               </div>
               <div id="bmi_tab_id" class="tab-pane fade">
                  
                  <div class="chart_contain_part">
                     <div id="bmi_chart" class="firstdiv" ></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-7 tab_chart_part2">
         <div class="chartleft_contain">
            <ul class="nav nav-tabs">
               <li class="active"><a data-toggle="tab" href="#all_tab_id" ng-click="weight.bodyMeasurementsChart('all')">All</a></li>
               <li><a data-toggle="tab" href="#neck_tab_id" ng-click="weight.bodyMeasurementsChart('neck')">Neck</a></li>
               <li><a data-toggle="tab" href="#arms_tab_id" ng-click="weight.bodyMeasurementsChart('arms')">Arms</a></li>
               <li><a data-toggle="tab" href="#waist_tab_id" ng-click="weight.bodyMeasurementsChart('waist')">Waist</a></li>
               <li><a data-toggle="tab" href="#hips_tab_id" ng-click="weight.bodyMeasurementsChart('hips')">Hips</a></li>
               <li><a data-toggle="tab" href="#legs_tab_id" ng-click="weight.bodyMeasurementsChart('legs')">Legs</a></li>
            </ul>
            <div class="tab-content">
               <div class="datepicker_contain_chart">

               <div class="row">
                  <div class="col-sm-6">
                        <ul class="list-inline datepicker_ul left_align">
                           <li class="date_contain_li rightpart_dateli">
                              <div class="form-group">
                                 <div id="datepicker4" class="input-group date white_datepickterbox">
                                    <input type="text"
                                                    class="form-control" 
                                                    uib-datepicker-popup = "@{{weight.format}}"
                                                     ng-model="weight.body_chart_start_date"
                                                     is-open="weight.body_chart_start_date_popup.opened"
                                                     datepicker-options="weight.dateOptions"
                                                     ng-required="true"
                                                     ng-change="weight.loggedBodyMeasurements('change');"
                                                     show-button-bar=false
                                                     close-text="Close" />
                                                      <span class="input-group-addon caledar_custom" ng-click="weight.body_chart_start_date_open()"> 
                                                         <i aria-hidden="true" class="fa fa-calendar"></i>
                                                      </span>
                                    </div>
                              </div>
                           </li>
                           <li><label class="data_label label_contain_li">to</label></li>
                           <li class="date_contain_li rightpart_dateli">
                              <div class="form-group">
                                 <div id="" class="input-group date white_datepickterbox">
                                       <input type="text"
                                                    class="form-control" 
                                                    uib-datepicker-popup = "@{{weight.format}}"
                                                     ng-model="weight.body_chart_end_date"
                                                     is-open="weight.body_chart_end_popup.opened"
                                                     datepicker-options="weight.dateOptions"
                                                     ng-required="true"
                                                     ng-change="weight.loggedBodyMeasurements('change');"
                                                     show-button-bar=false
                                                     close-text="Close" />
                                                      <span class="input-group-addon caledar_custom" ng-click="weight.body_chart_end_open()"> 
                                                         <i aria-hidden="true" class="fa fa-calendar"></i>
                                                       </span>
                                     </div>
                              </div>
                           </li>
                        </ul>
                  </div>
<!--                   <div class="col-sm-6 text-right">
                    <ul class="list-inline no-padd-right">
                      <li>
                           <label  class="data_label label_contain_li middle_lbl_li">
                              <a class="text-primary" ng-bind-html="ChangeButton"></a> 
                              <span ng-if="weight.selected_measurement!='all'" class="weight_mesaure_@{{weight.diff_class}}label">@{{weight.measurementChanges}}</span> 
                              <span ng-if="weight.selected_measurement!='all'"  class="weight_mesaure_@{{weight.diff_class}}label glyphicon glyphicon-arrow-@{{weight.measurementChanges_arrow}}">
                           </label>

                      </li>
                    </ul>
                  </div> -->
                  </div>
               </div>
               <div id="all_tab_id" class="tab-pane fade in active">
                  
                  <div class="chart_contain_part">
                     <div id="measurements_all" class="secondiv"></div>
                  </div>
               </div>
               <div id="neck_tab_id" class="tab-pane fade ">
        
                  <div class="chart_contain_part">
                     <div id="measurements_neck" class="secondiv"></div>
                  </div>
               </div>
               <div id="arms_tab_id" class="tab-pane fade">
                  
                  <div class="chart_contain_part">
                     <div id="measurements_arms" class="secondiv"></div>
                  </div>
               </div>
               <div id="waist_tab_id" class="tab-pane fade">
                
                  <div class="chart_contain_part">
                     <div id="measurements_waist" class="secondiv"></div>
                  </div>
               </div>
               <div id="hips_tab_id" class="tab-pane fade">
                  
                  <div class="chart_contain_part">
                     <div id="measurements_hips" class="secondiv"></div>
                  </div>
               </div>
               <div id="legs_tab_id" class="tab-pane fade">

                  <div class="chart_contain_part">
                     <div id="measurements_legs" class="secondiv"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- End of chart row -->
</section>


<!-- End Weight Diary -->
@endsection
@section('js')
{!! HTML::script("js/controllers/WeightController.js") !!}
{!! HTML::script("js/directives/inputDatepicker.js") !!}

{!! HTML::script("js/amcharts.js") !!}
{!! HTML::script("js/light.js") !!}
{!! HTML::script("js/serial.js") !!}
@append
@section('custom-js')
   <script type="text/javascript">
      $(document).ready(function(){

         if($(window).width() <= 520 ){
            //$('#history_table_bodymeasure tr td.histoty_tbl_resp_td').css({background:'#f2f2f2'});

            
           /*$('#history_table_bodymeasure tr td.histoty_tbl_resp_td').siblings().slideUp();
           $("#history_tb_1st_td").siblings().slideToggle();
           $('#history_table_bodymeasure tr td.histoty_tbl_resp_td').click(function(){
             $(this).siblings().slideToggle("slow");
           });*/
         }

      });
   </script>


@append