@extends('layout.master')
@section('title')
 Your BP & Pulse Diary
@endsection 
@section('custom-css')
 <style type="text/css">
 @media(min-width: 320px) and (max-width: 480px)
     { 
     .responsive_systolic{
        margin-left: 10px !important;
      }
     }
     @media(min-width: 768px) and (max-width: 1024px)
      { 
       .responsive_systolic{
        margin-left: 10px !important;
      }
     }
     @media only screen and (max-width: 480px){
          .sleep_goals{
            margin-bottom: 10px;
            float: left;
            width: 18px;
          }
        }

     .weight_mesaure_redlabel_red
    {
      color: #E5232D;
      font-weight: bold;
      font-size: 16px;
      text-align: center;
      vertical-align: middle !important;
    }
    .weight_mesaure_greenlabel_red
    {
      color: #006600;
      font-weight: bold;
      font-size: 16px;
      text-align: center;
      vertical-align: middle !important;
    }

     .weight_mesaure_greenlabel_red
    {
      color: #006600;
      font-weight: bold;
      font-size: 16px;
      text-align: center;
      vertical-align: middle !important;
    }
    .weight_mesaure_redlabel_second
    {
      font-size: 14px!important;
      margin-left: 7px;
      color: #E5232D;
      font-weight: bold;
      font-size: 16px;
      text-align: center;
      vertical-align: middle !important;
      font-family: "roboto_condensedregular";
      left: 5px;
      position: relative;
    }

    .weight_mesaure_redlabel_second_uniq
    {
      font-size: 14px!important;
      margin-left: 7px;
      color: #E5232D;
      font-weight: bold;
      font-size: 16px;
      text-align: center;
      vertical-align: middle !important;
      left: 5px;
      position: relative;
    }


    .weight_mesaure_greenlabel_second_uniq
    {
      font-size: 14px!important;
      margin-left: 7px;
      color: #006600;
      font-weight: bold;
      font-size: 16px;
      text-align: center;
      vertical-align: middle !important;
      left: 5px;
      position: relative;
    }
  

  


    ul.datepicker_ul li label.data_label_second {
      position: relative;
      top: 0px;
      padding: 5px;
    }


    #bp-pulse-calendar .fc-prev-button.fc-button.fc-state-default.fc-corner-left,
    #bp-pulse-calendar .fc-next-button.fc-button.fc-state-default.fc-corner-right
    {
        background: transparent;
        box-shadow: none;
        border: none;
        color: #0073BC;
    }
    #bp-pulse-calendar .fc-center > h2
    {
      font-size: 18px;
    }
    #bp-pulse-calendar .fc-prev-button.fc-button.fc-state-default.fc-corner-left
    {
      position: absolute;
      left: 40%;
    }
    #bp-pulse-calendar .fc-next-button.fc-button.fc-state-default.fc-corner-right
    {
      position: absolute;
      right: 38.5%;
    }
    .calendar_portion_total
    {
      padding-left: 0px;
    }
    #bp-pulse-calendar.fc td, .fc th
    {
      border-color: #b3dfff;
    }
    #bp-pulse-calendar .fc-view-container table thead tr th
    {
      background:#e0f2ff none repeat scroll 0 0;
      color: #0073bc;
      vertical-align: middle;
      padding-top: 10px;
      padding-bottom: 10px;

    }
    .calendar_portion_total_pbpulse
    {
      padding-left: 0px;
      position: relative;
      top:48px;
    }
    #bp-pulse-calendar .fc-toolbar
    {
      position: relative;
      left: 9%;
    }
    #bp-pulse-calendar .fc-scroller.fc-day-grid-container
    {
      overflow: hidden !important;
      height: auto !important;
    }
    #bp-pulse-calendar .fc-row.fc-week.fc-widget-content
    {
      height: 180px !important;
    }
    .text-excess{
    color: #FF1A1A !important;
    }
    .text-deficit{
      color: #33CC33 !important;
    }
    .text-neutral{
      color: #000000 !important;
    }
    
    /*responsive table for bp_pulse table*/
    @media(max-width: 800px) {
      #bppulse_new_reptable table,
       #bppulse_new_reptable thead,
       #bppulse_new_reptable tbody,
       #bppulse_new_reptable th,
       #bppulse_new_reptable td,
       #bppulse_new_reptable tr 
       {
         display: block;
       }
       #bppulse_new_reptable thead tr {
           position: absolute;
           top: -9999px;
           left: -9999px;
       }
       #bppulse_new_reptable tr { border: 1px solid #ccc; }
         #bppulse_new_reptable td {
         /* Behave like a "row" */
           border: none !important;
           /*border-bottom: 1px solid #eee;*/
           position: relative;
           padding-left: 45%;
           white-space: normal;
           text-align:left;
         }
         #bppulse_new_reptable table tbody tr td:before {
           /* Now like a table header */
           position: absolute;
           /* Top/left values mimic padding */
           top: 6px;
           left: 6px;
           width: 40%;
           padding-right: 10px;
           white-space: normal;
           text-align:left;
           font-weight: bold;
           padding-top: 15px;
           color: #0073BC;
           text-align: right;
         }
         #bppulse_new_reptable td:before { content: attr(data-title); }
    }
    .date_font_size
    {
      font-size: 13px;
    }
   

    /*end of responsive table for bp_pulse table*/
    .left_align{
      text-align: left!important;
    }
    .no-padd-right
    {
      padding-right: 0px !important;
    }
    .change_button_right{
      margin-right: 16px;
    }
    .weight_mesaure_greenlabel
    {
      color: #006600;
      font-weight: bold;
      font-size: 14px;
      text-align: center;
      vertical-align: middle !important;
    }



</style>
@endsection 
@section('content') 
<!-- Start sBp and Pulse -->
<section ng-controller="BpPulseController as bppulse" ng-init="bppulse.userId='{{$user_id}}'; 
                                        bppulse.loggedBpPulse();"  class="section-panel custom-section-panel">
  <!-- header title -->
  <div class="row">
      <div class="col-xs-12 text-center">
         <h2 class="text-primary head_title_main">YOUR BP <span class="title_head_line_pulse_bp">& PUL</span>SE DIARY</h2>
      </div>
  </div>
 
<div class="row" ng-show="bppulse.show_hide">
      <!-- <h4 class="col-sm-12 sub_head_blue">BP & PULSE GOALS</h4> -->
       
      <p class="col-sm-12 margin_bot">
           <strong><span class="text-light-blue">Last Log Date: @{{bppulse.last_log_date | date:'MMM-dd-yy'}}  </span></strong>
           <button class="btn btn-primary btndesign"><i class="fa fa-refresh"></i>&nbsp; Sync with device</button>
      </p>
       
      <p class="col-sm-12 panel_head_text">BP & PULSE GOALS</p>
      <div class="col-sm-12">
         <div id="bppulse_new_reptable">
            <table class="table new_sleepbox box_border_noradius">
               <thead class="box_head_background">
                  <tr>
                     <th>Goals</th>
                     <th>Last Reading <span class="date_font_size">@{{bppulse.last_log_date | date:'MMM-dd-yy'}}</span></th>
                     <th>Last 7 days</th>
                     <th>Last 30 days</th>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                     <td data-title="GOAL"><p class="text-primary">Systolic</p></td> 
                     <td data-title="LAST READING">
                         <p><span class="green"><b>@{{bppulse.systolic_last_reading}}% </b></span> of goal achieved</p>
                     </td>
                     <td data-title="THIS WEEK">
                      <p><span class="green"><b>@{{bppulse.systolic_last_seven_day}}</b></span> of <span class="text-primary"><b>7</b></span> times goal achieved.</p>
                     </td>
                     <td data-title="THIS MONTH">
                      <p><span class="green"><b>@{{bppulse.systolic_last_thirty_days}}</b></span> of <span class="text-primary"><b>30</b></span> times goal achieved</p>
                    </td>
                  </tr>
                  <tr>
                     <td data-title="GOAL"><p class="text-primary">Diastolic</p></td> 
                     <td data-title="LAST READING">
                         <p><span class="green"><b>@{{bppulse.diastolic_last_reading}}% </b></span> of goal achieved</p>
                     </td>
                     <td data-title="THIS WEEK">
                      <p><span class="green"><b>@{{bppulse.diastolic_last_seven_day}}</b></span> of <span class="text-primary"><b>7</b></span> times goal achieved.</p>
                     </td>
                     <td data-title="THIS MONTH">
                      <p><span class="green"><b>@{{bppulse.diastolic_last_thirty_days}}</b></span> of <span class="text-primary"><b>30</b></span> times goal achieved</p>
                    </td>
                  </tr>
                  <tr>
                     <td data-title="GOAL"><p class="text-primary">Heart rate</p></td> 
                     <td data-title="LAST READING">
                         <p><span class="green"><b>@{{bppulse.pulse_last_reading}}% </b></span> of goal achieved</p>
                     </td>
                     <td data-title="THIS WEEK">
                      <p><span class="green"><b>@{{bppulse.pulse_last_seven_day}}</b></span> of <span class="text-primary"><b>7</b></span> times goal achieved.</p>
                     </td>
                     <td data-title="THIS MONTH">
                      <p><span class="green"><b>@{{bppulse.pulse_last_thirty_days}}</b></span> of <span class="text-primary"><b>30</b></span> times goal achieved</p>
                    </td>
                  </tr>
               </tbody>
            </table>
         </div>
      </div>
</div>
<div class="saperator"></div>
<!-- <div class="row">
     <div class="col-xs-12">
       <span class="text-light-blue">Last Log Date: @{{bppulse.last_log_date | date:'MMM-dd-yy'}}  </span>
       <button class="btn btn-primary btndesign"><i class="fa fa-refresh"></i>&nbsp; Sync with device</button>
      </div>
</div> -->
   <!-- End of main header title -->
   <!-- Start Bp and Pulse tables row -->
   <div class="row tab_chart_row">
      <!-- Bp and Pulse portion -->
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 wight_body_portion">
         
         <div class="panel panel-primary weight_body_panel weight_diary_panel_size">
            <div class="panel-heading text-center panel_head_background bp_pulse_panelHead">BP & PULSE</div>
            <div class="panel-body panel-container panel_Bodypart">
               <div class="row weight_body_fat_row">
                  <div class="col-xs-8 col-sm-3 col-md-3 col-lg-3 datebox weight_body_part1">
                  <form name="BpPulseForm" novalidate="novalidate" class="animation-fadeInQuick">
                     <table class="table body_weight_goal_table w_goal_m_othertable">
                        <tbody>
                           <tr>
                              <th>
                                <p class="text-primary theme_blue_color">DATE:</p>
                                 <div class="form-group">
                                    <div class="input-group date sleepboxdatepicker">
                                             <input type="text"
                                              class="form-control" 
                                              uib-datepicker-popup = "@{{bppulse.format}}"
                                    0           ng-model="bppulse.log_date"
                                               is-open="bppulse.bp_pulse_date_popup.opened"
                                               datepicker-options="bppulse.dateOptions"
                                               ng-required="true"
                                               show-button-bar=false
                                               close-text="Close" />
                                                <span class="input-group-addon caledar_custom" ng-click="bppulse.bp_pulse_date_open()"> 
                                       <i aria-hidden="true" class="fa fa-calendar"></i>
                                       </span>
                                    </div>
                                 </div>
                                  <div class="saperator">
                                    </div>
                                      <div class="input-group date datepicker_new_design">
                                        <input type="text"  number-validation="number-validation" maxlength="6" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" ng-model="bppulse.systolic" name="systolic" id="systolic" class="form-control" placeholder="Systolic" required>
                                        <span class="input-group-addon">mmHg</span>
                                      </div>
                                  <div class="saperator"></div>
                                     <div class="input-group date datepicker_new_design">
                                      <input type="text"  number-validation="number-validation" maxlength="6" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" ng-model="bppulse.diastolic" name="diastolic" id="diastolic" class="form-control" placeholder="Diastolic" required>
                                      <span class="input-group-addon">mmHg</span>
                                    </div>                                     
                                  <div class="saperator"></div>
                                  <div class="input-group date datepicker_new_design">
                                  <input type="text"  number-validation="number-validation" maxlength="6" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" ng-model="bppulse.pulse" name="pulse" id="pulse" class="form-control" placeholder="Pulse" required>
                                  <span class="input-group-addon">BPM &nbsp;&nbsp;</span>
                                 </div>                                    
                                 <div class="saperator"></div>
                              </th>
                           </tr>
                           <tr>
                              <td class="last_btnbottom"><button class="btn btn-primary btn-block btndesign1" ng-disabled="BpPulseForm.$invalid || bppulse.log_BpPulse_error" ng-click="bppulse.logBpPulse();">Log</button></td>
                           </tr>
                        </tbody>
                     </table>
                  </form>                     
                  </div>
                  <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3 weight_body_part2">
                     <table class="table body_weight_goal_table">
                        <tbody>
                           <tr>
                              <th>
                                 <p class="text-primary theme_blue_color">GOAL DATE:</p>
                                  <p>@{{bppulse.bp_pulse_goal | date:'MM-dd-yy'}}</p>
                              </th>
                           </tr>
                           <tr>
                              <td>
                                 <p class="theme_blue_color">Systolic Goal <span class="font_left">@{{bppulse.systolic_goal}}</span></p>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <p class="theme_blue_color">Diastolic Goal <span class="font_left">@{{bppulse.diastolic_goal}}</span></p>
                              </td>
                           </tr>
                            <tr>
                              <td>
                                 <p class="theme_blue_color">Pulse Goal <span class="font_left">@{{bppulse.heart_rate}}</span></p>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <p class="weight_mesaure_redlabel last_btnbottom">@{{bppulse.bp_pulse_goal_days_left}} days left</p>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center weight_body_part3">
                     <table class="table body_weight_goal_table w_goal_m_othertable">
                        <tr>
                           <th>
                              <p class="text-primary text-center theme_blue_color">HISTORY (LAST 4 LOGS)</p>
                              <table class="table table-bordered history_table">
                                 <tr>
                                    <th>Date</th>
                                    <th>Systolic/<br>Diastolic</th>
                                    <th>Heart Rate</th>
                                 </tr>
                                  <tr ng-repeat="loggedbppulse in bppulse.user_bppulse_history | orderBy:'log_date':true | limitTo:4 ">
                                    <td>@{{loggedbppulse.log_date | date:'MM-dd-yy'}}</td>
                                    <td>@{{loggedbppulse.systolic  | number:0 }}/@{{loggedbppulse.diastolic  | number:0}}</td>
                                    <td>@{{loggedbppulse.pulse.pulse  | number:0}}</td>
                                 </tr>
                              </table>
                           </th>
                        </tr>
                        <tr>
                           <td>
                              <div class="saperator"></div>
                           </td>
                        </tr>
                        <tr>
                           <td class="last_btnbottom history_btn">
                              <button class="btn btn-primary btndesign">See All</button>
                           </td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- end of Bp and Pulse portion -->
      <!-- start graph three -->
       
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 body_measure_portion_bp">
        <div class="chartleft_contain">
            <ul class="nav nav-tabs">
               <li class="active responsive_systolic"><a data-toggle="tab" href="#systolic_tab_id" ng-click="bppulse.systolicDiastolicCharts('all')">Systolic / Diastolic</a></li>
               <li><a data-toggle="tab" href="#pulse_tab_id"  ng-click="bppulse.systolicDiastolicCharts('pulse')">Pulse</a></li>
            </ul>
              <div class="tab-content">
                 <div id="systolic_tab_id" class="tab-pane fade in active">
                    <div class="datepicker_contain_chart">
                        <div class="row">
                          <div class="col-xs-7">

                            <ul class="list-inline datepicker_ul left_align">
                              <li class="date_contain_li">
                                 <div class="form-group">
                                   
                                     <div class="input-group date sleepboxdatepicker date-width">
                                                   <input type="text"
                                                    class="form-control" 
                                                    uib-datepicker-popup = "@{{bppulse.format}}"
                                                     ng-model="bppulse.bp_sys_dys_start_date"
                                                     is-open="bppulse.bp_pulse_s_date_popup.opened"
                                                     datepicker-options="bppulse.dateOptions"
                                                     ng-required="true"
                                                     ng-change="bppulse.loggedBpPulse('change');"
                                                     show-button-bar=false
                                                     close-text="Close" />
                                                      <span class="input-group-addon caledar_custom" ng-click="bppulse.bp_pulse_s_date_open()"> 
                                                         <i aria-hidden="true" class="fa fa-calendar"></i>
                                                      </span>
                                    </div>
                                 </div>
                              </li>
                              <li><label class="data_label label_contain_li">to</label></li>
                              <li class="date_contain_li">
                                 <div class="form-group">
                                      <div class="input-group date sleepboxdatepicker date-width">
                                                   <input type="text"
                                                    class="form-control" 
                                                    uib-datepicker-popup = "@{{bppulse.format}}"
                                                     ng-model="bppulse.date_log_diastolic"
                                                     is-open="bppulse.bp_pulse_e_date_popup.opened"
                                                     datepicker-options="bppulse.dateOptions"
                                                     ng-required="true"
                                                     ng-change="bppulse.loggedBpPulse('change');"
                                                     show-button-bar=false
                                                     close-text="Close" />
                                                      <span class="input-group-addon caledar_custom" ng-click="bppulse.bp_pulse_e_date_open()"> 
                                                         <i aria-hidden="true" class="fa fa-calendar"></i>
                                                       </span>
                                     </div>
                                 </div>
                              </li>
                            </ul>
                          </div>
                          <!-- <div class="col-xs-5 text-right">
                            <ul class="list-inline nobottom-margin no-padd-right">
                              <li><label class="data_label_second label_contain_li"><span class="text-primary change_button_right"> Change</span> 
                                 <span class="weight_mesaure_@{{bppulse.diff_class}}label">Systolic @{{bppulse.systolic_chart_diff}} mmHg</span> <span  class="weight_mesaure_@{{bppulse.diff_class}}label_red glyphicon glyphicon-arrow-@{{bppulse.Systolic_diff_arrow}}"></span><br>
                                 <span class="weight_mesaure_@{{bppulse.diff_class}}label_second_uniq">&nbsp;&nbsp; Diastolic @{{bppulse.diastolic_chart_diff}} mmHg</span>
                                <span  class="weight_mesaure_@{{bppulse.diff_class}}label_red glyphicon glyphicon-arrow-@{{bppulse.Diastolic_diff_arrow}}"></span></label>
                              </li>
                           </ul>
                          </div> -->
                       </div>

                    </div>
                    <div class="chart_contain_part_bp">
                       <div id="Systolic_Chart" class="firstdiv" ></div>
                    </div>
                 </div>
                 <div id="pulse_tab_id" class="tab-pane fade">
                    <div class="datepicker_contain_chart">
                           <div class="row">
                                <div class="col-sm-8">
                                  <ul class="list-inline datepicker_ul left_align">
                                    <li class="date_contain_li">
                                         <div class="form-group">
                                            <div id="datepicker_log_from_pulse" class="input-group date  date-width">
                                               
                                               <input type="text"
                                                            class="form-control" 
                                                            uib-datepicker-popup = "@{{bppulse.format}}"
                                                             ng-model="bppulse.bp_sys_dys_start_date"
                                                             is-open="bppulse.bp_pulse_s_date_popup.opened"
                                                             datepicker-options="bppulse.dateOptions"
                                                             ng-required="true"
                                                             ng-change="bppulse.loggedBpPulse('change');"
                                                             show-button-bar=false
                                                             close-text="Close" class="rightborder" />
                                                              <span class="input-group-addon caledar_custom" ng-click="bppulse.bp_pulse_s_date_open()"> 
                                                                 <i aria-hidden="true" class="fa fa-calendar"></i>
                                                               </span>
                                            </div>
                                         </div>
                                    </li>
                                    <li><label class="data_label label_contain_li">to</label></li>
                                      <li class="date_contain_li">
                                         <div class="form-group">
                                             <div id="datepicker_log_to_pulse" class="input-group date  date-width">
                                                <input type="text"
                                                            class="form-control" 
                                                            uib-datepicker-popup = "@{{bppulse.format}}"
                                                             ng-model="bppulse.date_log_diastolic"
                                                             is-open="bppulse.bp_pulse_e_date_popup.opened"
                                                             datepicker-options="bppulse.dateOptions"
                                                             ng-required="true"
                                                             ng-change="bppulse.loggedBpPulse('change');"
                                                             show-button-bar=false
                                                             close-text="Close" />
                                                              <span class="input-group-addon caledar_custom" ng-click="bppulse.bp_pulse_e_date_open()"> 
                                                                 <i aria-hidden="true" class="fa fa-calendar"></i>
                                                              </span>
                                             </div>
                                         </div>
                                      </li>
                                  </ul>
                                 </div>
                                 <!-- <div class="col-sm-4 text-right">
                                   <ul class="list-inline nobottom-margin no-padd-right"> 
                                    <li><label class="data_label label_contain_li"><span class="text-primary">Change</span> 
                                       <span class="weight_mesaure_@{{bppulse.diff_class}}label"> @{{bppulse.pulse_chart_diff}} bpm</span> <span  class="weight_mesaure_@{{bppulse.diff_class}}label_red glyphicon glyphicon-arrow-@{{bppulse.pulse_diff_arrow}}"></label>
                                    </li>
                                   </ul>                              
                                </div> -->
                           </div>
                    </div>
                    <div class="chart_contain_part_bp">
                          <div id="Pulse_Chart" class="firstdiv" ></div>
                    </div>
                 </div>
        </div>
      </div>
      <!-- end of graph three portion -->
   </div>
   <!-- end of Bp and Pulse tables -->  
   <!-- start chart row  -->
  <section class="monthly-calorie-summary-section">
    <div class="row text-center">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="saperator"></div>
        <h2 class="text-primary head_title_main">MONTH<span class="title_head_line">LY SU</span>MMARY</h2>
      </div>
    </div>
    <div class="saperator"></div>

    <div class="row">
      <div class="col-xs-6 col-sm-10 col-md-10 col-lg-10 calendar_portion">
      <div ng-model="bppulse.eventSources"
       ui-calendar="bppulse.uiConfig.calendar"
        class="bp_pulse_calender angular-calender hidden-xs"
        calendar ="bpCalendar"
        ></div>
      </div>
      <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 calendar_week_avg">
        <table id="week_average" class="table table-bordered calendar_table_side calendar_blockequal right_side_table_cale">
          <thead>
            <tr class="tab_firsthead">
              <th class="calendar_table_head">Week Average</th>
            </tr>
          </thead>
          <tbody>
            <tr class="tablecell_blockqual">
              <td class="ex_smallcolumndiv">
                  <div class="tabletd-div calendar-week-cell week_1">
                    
                </div>
              </td>
            </tr>
            <tr class="tablecell_blockqual">
              <td class="ex_smallcolumndiv">
                <div class="tabletd-div calendar-week-cell week_2">
                </div>
              </td>
            </tr>
            <tr class="tablecell_blockqual">
              <td class="ex_smallcolumndiv">
                <div class="tabletd-div calendar-week-cell week_3">
                  
                </div>
              </td>
            </tr>
            <tr class="tablecell_blockqual">
              <td class="ex_smallcolumndiv">
               <div class="tabletd-div calendar-week-cell week_4">
                </div> 
              </td>
            </tr>
            <tr class="tablecell_blockqual">
              <td class="ex_smallcolumndiv">
                <div class="tabletd-div calendar-week-cell week_5">
                </div>
              </td>
            </tr>
            <tr class="tablecell_blockqual">
              <td class="ex_smallcolumndiv">
                <div class="tabletd-div calendar-week-cell week_6">
                </div>
              </td>
            </tr>
             
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-6 col-sm-10 col-md-10 col-lg-10 footercal_side">
        <div class="monthly_calendar_footer columnfooterdiv" id="monthly_left">
          <p class="footer_para">Month Average</p>
        </div>
      </div>
      <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 footercal_side footercal_side_right">
          <div class="monthly_calendar_footer monthly_rightside columnfooterdiv" id="monthly_right">
                  <p ng-bind-html="bppulse.sys" class="no_margin"></p>
                  <p ng-bind-html="bppulse.dys" class="no_margin"></p>
                  <p ng-bind-html="bppulse.pulse_month"  class="no_margin"></p>
          </div>
      </div>
    </div>
  </section>
   <!-- End of header title -->
   <!-- Bp and Pulse tables row -->
   <!-- End of chart row -->
</section>
<!-- End Bp and Pulse -->
@endsection
@section('js')
{!! HTML::script("js/controllers/BpPulseController.js") !!}
{!! HTML::script("js/amcharts.js") !!}
{!! HTML::script("js/serial.js") !!}
@append
@section('custom-js')

<script type="text/javascript">
      $(document).ready(function(){
      
        var height = Math.max($("#monthly_left").height(), $("#monthly_right").height());
        $("#monthly_left").height(height);
        $("#monthly_right").height(height);


       $(".bp_pulse_calender.angular-calender .fc-prev-button").find("span").removeClass("ui-icon ui-icon-circle-triangle-w").addClass("fa fa-chevron-left fa-fw");

       $(".bp_pulse_calender.angular-calender .fc-next-button").find("span").removeClass("ui-icon ui-icon-circle-triangle-w").addClass("fa fa-chevron-right fa-fw"); 

       $('.bp_pulse_calender.angular-calender .fc-row.fc-week.ui-widget-content').addClass("tablecell_blockqual");
   
          /*script for equal height of two elements For Calander*/
          $('.monthly-calorie-summary-section').each(function(){  
            var highestBox = 0;
            $('.tablecell_blockqual', this).each(function(){
              if($(this).height() > highestBox) {
                highestBox = $(this).height(); 
              }          
            });  
            $('.tablecell_blockqual', this).height(highestBox);           
          });

      
    });

</script>
@append
