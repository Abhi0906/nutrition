@extends('layout.app')
@section('content')
<style type="text/css">
   [class^="flaticon-"]:before, [class*=" flaticon-"]:before, [class^="flaticon-"]:after, [class*=" flaticon-"]:after {
   font-family: Flaticon;
   font-size: 14px;
   font-style: normal;
   margin-left: 20px;
   font-weight: 600 !important;
   }
   .control-label{
   margin-top: 9px !important;
   }
   button.btndesign:disabled
  {
    background: #3366cc;
    border:1px solid #2952a3;
  }

</style>
<?php
  $first_name = isset($auth0_user_data['first_name'])?$auth0_user_data['first_name']:"";
  $last_name = isset($auth0_user_data['last_name'])?$auth0_user_data['last_name']:"";
  $gender = isset($auth0_user_data['gender'])?strtolower($auth0_user_data['gender']):"male";
  $birth_date = isset($auth0_user_data['birth_date'])?date('F d, Y',strtotime($auth0_user_data['birth_date'])):date('F d, Y');


?>

<div class="row" ng-controller="DashboardController as dash">
    <div class="col-xs-12 col-md-6"  ng-init="dash.userId='{{$user_id}}';dash.getCalories();">
     <div class="block full">
        <div class="block-title">
           <h3>
              <i class="fa fa-envelope-o"></i>&nbsp;&nbsp;Calories Remaining
           </h3>
        </div>
        <div class="widget-extra-full">
           <div class="row ">
              <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                 <h4>
                    <strong>@{{dash.calories.current_calories}}</strong> <small></small><br>
                 </h4>
                 <small> Goal</small>
              </div>
              <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                 <h4>
                    <strong>-</strong><br>
                 </h4>
              </div>
              <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                 <h4>
                    <strong>@{{dash.calories.food}}</strong> <small></small><br>
                 </h4>
                 <small>Food</small>
              </div>
              <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                 <h4>
                    <strong>+</strong><br>
                 </h4>
              </div>
              <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                 <h4>
                    <strong>@{{dash.calories.exercise}}</strong> <small></small><br>
                 </h4>
                 <small>Exercise</small>
              </div>
              <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                 <h4>
                    <strong>=</strong><br>
                 </h4>
              </div>
              <div class="col-xs-2 col-sm-3 col-md-3 col-lg-3">
                 <h4>
                    <strong>@{{dash.calories.remaing_calories}}</strong> <small></small><br>
                 </h4>
                 <small>Remaining</small>
              </div>
           </div>
           </br>
           <div class="row text-center">
              <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                 <a href="{{URL::to('/food')}}" class="btn btn-block btn-primary">Log Food</a>
              </div>
              <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                 <a href="{{URL::to('/exercise')}}" class="btn btn-block btn-primary">Log Exercise</a>
              </div>
           </div>
        </div>
     </div>
       @if(isset($auth0_user_data['email_varify']) && !($auth0_user_data['email_varify']))
         <div class="block full">
            <div class="block-title" ng-hide="myVar">
               <div class="widget-options">
               </div>
               <h3 class="">
                  <i class="fa fa-envelope-o"></i>&nbsp;&nbsp;Email Verification
               </h3>
            </div>
            <div class="widget-extra-full" ng-hide="myVar">
               <div class="col-xs-12 " ng-hide="myVar">
                  &nbsp;&nbsp;&nbsp; Please check your email for verification.<br><br>
               <div class="alert alert-warning">
                  <strong> You can access only for 7 days</strong>
            </div>
            </div>
               <span id="passwordError" ng-show="dash.emailValidation_email">
                  <p><small class="text-danger">@{{dash.emailValidationMesg_email}}</small></p>
               </span>
               <span id="passwordSuccess" ng-show="dash.emailSuccessMsg_email">
                  <p><small class="text-success">@{{dash.emailSuccessMsg_email}}</small></p>
               </span>
               <div class="row text-center">
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                     <button type="button" class="btn btn-block btn-primary" ng-click="dash.resend_email()">Resend Email</button>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                     <button type="button" ng-click="update_email()" class="btn btn-block btn-primary">Update Email</button>
                  </div>
               </div>
            </div>
            <form id="myForm" action="" name="myForm"   method="post" novalidate>
               <div class="widget-extra themed-background-light-blue" ng-show="myVar">
                  <div class="widget-options" >
                  </div>
                  <h3 class="widget-content-light"  >
                     <strong><small><i class="fa fa-envelope-o"></i>&nbsp;&nbsp;Update Email id</small></strong>
                     <small></small>
                  </h3>
               </div>
               <div class="widget-extra-full" ng-show="myVar">
                  <div class="row " ng-show="myVar">
                     <div class="form-group">
                        <label for="example-hf-email"  class="col-md-1 control-label">Email: &nbsp;&nbsp; </label>
                        <div class="col-md-7">
                           <input type="email" id="email" placeholder="Enter Email.." class="form-control" ng-model="dash.email" name="email" id="example-hf-email" required>
                           <span class="text-danger"  ng-show="myForm.email.$error.required && myForm.email.$dirty">Please enter email</span>
                           <span class="text-danger" ng-show="!myForm.email.$error.required && myForm.email.$error.email && myForm.email.$dirty">Invalid email id</span>
                           <span id="passwordError" ng-show="dash.emailValidation">
                              <p ><small class="text-danger">@{{dash.emailValidationMesg}}</small></p>
                           </span>
                           <span id="passwordSuccess" ng-show="dash.emailSuccessMsg">
                              <p ><small class="text-success">@{{dash.emailSuccessMsg}}</small></p>
                           </span>
                        </div>
                     </div>
                  </div>
                  <div class="row text-center">
                     <div class="col-md-4 col-md-offset-1">
                        <button type="button"  ng-disabled="!myForm.$valid" class="btn btn-primary" ng-click="dash.update_user_email()"></i>Update</button>
                        <input type="button" value="Cancel"  ng-click="update_email()"  class="btn btn-warning">
                     </div>
                  </div>
            </form>
            </div>
         </div>
       @endif

       <div id="user_incomplete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog">
            <div class="modal-content">
               <!-- Modal Header -->
               <div class="modal-header text-center">
                  <h1><small><strong>Profile Information</strong></small></h1>
               </div>
               <!-- END Modal Header -->
               <!-- Modal Body -->
               <div class="modal-body">
                  <form  id="formAbout" class="form-horizontal form-bordered form-control-borderless" name="formAbout" >
                     <div class="form-group" >
                        <div class="col-xs-12 col-md-6">
                           <div class="input-group" ng-init="dash.about_data.first_name ='{{$first_name}}'">
                              <span class="input-group-addon"><i class="gi gi-user"></i></span>
                              <input type="text"
                                     name="first_name"
                                     ng-model="dash.about_data.first_name"
                                     ng-minlength="3"
                                     maxlength="30"
                                     class="form-control input-lg"
                                     placeholder="Firstname"
                                     required
                                      >
                           </div>
                           <div class="input-group">
                              <span class="input-group-addon"><i class=""></i></span>
                              <span ng-show="formAbout.first_name.$invalid && formAbout.first_name.$touched">
                              <span class="help-block animation-slideDown" ><span class="text-danger">FirstName is Required</span></span>
                              </span>
                           </div>
                        </div>
                        <div class="col-xs-12 col-md-6" ng-init="dash.about_data.last_name ='{{$last_name}}'">
                           <div class="input-group">
                              <span class="input-group-addon"></span>
                              <input type="text"
                                  name="last_name"
                                  maxlength="30"
                                  ng-model="dash.about_data.last_name"
                                  class="form-control input-lg"
                                  placeholder="Lastname"
                                  required>
                           </div>
                           <div class="input-group">
                              <span class="input-group-addon"><i class=""></i></span>
                              <span ng-show="formAbout.last_name.$invalid && formAbout.last_name.$touched">
                              <span class="help-block animation-slideDown" ><span class="text-danger">LastName is Required</span></span>
                              </span>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-xs-12">
                              <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-gift"></i></span>
                                  <input type="text" id="birth_date" class="form-control input-datepicker" value="<?php echo $birth_date; ?>" data-date-format="MM dd, yyyy" placeholder="MM DD, YYYY">
                              </div>

                              <div class="input-group">
                                  <span class="input-group-addon"><i class=""></i></span>
                                    <div ng-show="dash.ageValidation" class="help-block animation-slideDown"><span class="text-danger">You must be at least 13 years of age to register</span></div>
                                  </span>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-xs-12" ng-init="dash.about_data.gender ='{{$gender}}'">
                              <div class="input-group">
                                 <span class="input-group-addon"><i class="fa fa-transgender"></i></span>
                                 <label class="radio-inline" for="male" ng-init="dash.about_data.gender ='male'">
                                 <input type="radio" id="male"  ng-model="dash.about_data.gender"  value="male" name="gender"> Male
                                 </label>
                                 <label class="radio-inline" for="female" >
                                 <input type="radio" id="female" ng-model="  dash.about_data.gender"   value="female" name="gender"> Female
                                 </label>
                              </div>
                              <div class="col-xs-12 col-md-6">
                                 <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <span  class="text-danger text-center" ng-show="formAbout.gender.$invalid && formAbout.gender.$touched">Select any gender</span>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-xs-12 col-md-6">
                              <div class="input-group">
                                 <span class="input-group-addon"><i class="fa fa-male"></i></span>
                                 <input type="text" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" number-validation="number-validation"  maxlength="5" name="height_in_feet"  ng-model="dash.about_data.height_in_feet" class="form-control input-lg" required>
                                 <span class="input-group-btn">
                                <span class="input-group-addon btn-default">Feet</span>
                                 </span>
                              </div>
                           </div>
                                    <div class="col-xs-12 col-md-6">
                                       <div class="input-group">
                                         <div class="col-md-6">
                                           <div class="input-group ">
                                                <input type="text"  ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" number-validation="number-validation"   maxlength="6" name="height_in_inch" ng-model="dash.about_data.height_in_inch"   class="form-control input-lg" required>
                                           </div>
                                         </div>
                                         <span class="input-group-btn">
                                               <span class="input-group-addon btn-default">Inch</span>
                                         </span>
                                       </div>
                                    </div>
                              <div class="col-xs-12 col-md-6">
                                  <div class="input-group">
                                     <span class="input-group-addon"></span>
                                     <span ng-show="formAbout.height_in_feet.$invalid && formAbout.height_in_feet.$touched">
                                     <span class="help-block animation-slideDown" ><span class="text-danger">Enter valid Height</span></span>
                                  </div>
                              </div>
                              <div class="col-xs-12 col-md-6">
                                 <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <span ng-show="formAbout.height_in_inch.$invalid && formAbout.height_in_inch.$touched">
                                    <span class="help-block animation-slideDown" ><span class="text-danger">Enter valid Height</span></span>
                                 </div>
                              </div>
                        </div>
                        <div class="form-group">
                           <div class="col-xs-12">
                              <div class="input-group">
                                 <span  class="input-group-addon"><i class="wellness-icon-weight"></i></span>
                                 <input type="text" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" number-validation="number-validation" maxlength="6" name="baseline_weight"  ng-model="dash.about_data.baseline_weight" class="form-control input-lg" placeholder="Weight" required>
                                 <span class="input-group-btn">
                                 <span class="input-group-addon btn-default">lbs</span>
                                 </span>
                              </div>
                              <div class="col-xs-12 col-md-6">
                                 <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <span ng-show="formAbout.baseline_weight.$invalid && formAbout.baseline_weight.$touched">
                                    <span class="help-block animation-slideDown" ><span class="text-danger">Weight is Required</span></span>
                                 </div>
                              </div>
                           </div>
                         </div>
                         <div class="form-group form-actions">
                             <div class="col-xs-8 col-md-8">
                                <span class="text-danger" ng-hide="formAbout.$valid" >&nbsp;&nbsp; </span>
                             </div>
                             <div class="col-xs-3 col-md-4 text-right">
                               <button type="button" class="btn btn-sm btn-primary btndesign"  ng-disabled="formAbout.$invalid"  ng-click="dash.updateUserInfo()"><i class="fa fa-plus"></i> Save</button>
                             </div>
                          </div>
                        </form>
                      </div>
                     <!-- END Modal Body -->
                  </div>
               </div>
            </div>
       </div>

       <div id="user_verify" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
           <div class="modal-dialog">
               <div class="modal-content">
                  <!-- Modal Header -->
                 <div class="modal-header  has-warning text-center">
                     <div class="alert alert-warning">
                       <strong>Warning!</strong>
                      </div>
                      <div class="modal-body">
                          <p><strong>Please, You must verify your email address before you can continue.Please check you email for the verify account.</strong></p>
                      </div>
                  </div>
               </div>
           </div>
       </div>
   </div>
 </div>
 <div class="div-separator"></div>
 <div class="div-separator"></div>
 <div class="div-separator"></div>
 @endsection
 @section('inline_scripts')
   @if(Auth::User()->status == "Incomplete")
   <script type="text/javascript">
          $('#user_incomplete').modal('show');
   </script>
  @endif
  @if(isset($auth0_user_data['email_varify']) && !($auth0_user_data['email_varify']))
  @if($auth0_user_data['created_at_diff'] > 7)
  <script type="text/javascript">

        $('#user_verify').modal('show');

  </script>
  @endif

  @endif
  @endsection
  @section('page_scripts')
  {!! HTML::script("js/controllers/DashboardController.js") !!}
  {!! HTML::script("js/directives/inputDatepicker.js") !!}
  @append