<div class="saperator"></div>
<div class="text-right datepicker_contain_part">
  <ul class="list-inline btn-ul-date datepicker_ul">
    <li><button class="btn btn-primary btndesign btn-arrow" ng-click="food_diary.prevFoodDairy();"><span class="fa fa-chevron-left"></span></button></li>
    <li>
      
        <div class="input-group date sleepboxdatepicker big-date-width">
                 <input type="text"
                   class="form-control" 
                   uib-datepicker-popup = "@{{food_diary.format}}"
                    ng-model="food_diary.log_date"
                    is-open="food_diary.food_date_popup.opened"
                    datepicker-options="food_diary.dateOptions"
                    ng-required="true"
                    ng-change="food_diary.changeDate();"
                    show-button-bar=false
                    close-text="Close" />
                     <span class="input-group-addon caledar_custom" ng-click="food_diary.food_date_open()"> 
                        <i aria-hidden="true" class="fa fa-calendar"></i>
                      </span>
              </div> 
    </li>
    <li><button class="btn btn-primary btndesign btn-arrow" ng-click="food_diary.nextFoodDairy();"><span class="fa fa-chevron-right"></span></button></li>
    
  </ul>
</div>
<div class="food_dairy_block" id="responsive_food_table">             
    <table class="table table-bordered borderless_table margin_bottom_zero" id="simple-example-table">
      <thead> 
        <tr class="toptr-border">
          <th class="withborder"></th>
          <th class="calorie-subhead-td withborder">CALORIES</th>
          <th class="calorie-subhead-td withborder">FAT</th>
          <th class="calorie-subhead-td withborder">FIBER</th>
          <th class="calorie-subhead-td withborder">CARBS</th>
          <th class="calorie-subhead-td withborder">SODIUM</th>
          <th class="calorie-subhead-td withborder">PROTEIN</th>
        </tr>
      </thead>
      <tbody>
        <tr class="text-center toptr-border">
          <td id="food_1st_td" data-title=" " colspan="7" class="sidecolor text-leftside withborder head_slide_foodtbl_td">
            <h4 id="Breakfast_focus_id">BREAKFAST</h4>
            <ul class="list-inline">
              <li>
                <i class="fa fa-plus-circle facolor"></i> 
                <a ui-sref="log_food({ food_type : 'Breakfast'})" class="add_food" id="add_breakfast"  >
                  Add Food
                </a> 
                </li>
                <li ng-if="food_diary.breakfastFood.length>0">
                  <i class="fa fa-cog facolor"></i> 
                  <a ui-sref="create_meal({ food_type: 'Breakfast', meal_food: food_diary.breakfastFood, meal_food_total: food_diary.totalBreakfast })" class="add_food" >
                   Save Meal
                  </a>
                </li>
             <!--  <li><i class="fa fa-cog facolor"></i> Quick Tools</li> -->
            </ul>
          </td>
        </tr>
        <tr ng-repeat="(key, value) in food_diary.breakfastFood">
           <td data-title="Food" class="first-td-border">
           <a href="javascript:void(0);" ng-click="food_diary.deleteLoggedFood(value)">
             <i class="fa fa-times-circle facolor"></i>
           </a>
            @{{value.name}}
           </td>
           <td data-title="CALORIES" class="text-center black_text td-border-none">@{{value.calories}}</td>
           <td data-title="FAT" class="text-center black_text td-border-none">@{{value.fat}}</td>
           <td data-title="FIBER" class="text-center black_text td-border-none">@{{value.fiber}}</td>
           <td data-title="CARBS" class="text-center black_text td-border-none">@{{value.carb}}</td>
           <td data-title="SODIUM" class="text-center black_text td-border-none">@{{value.sodium}}</td>
           <td data-title="PROTEIN" class="text-center black_text td-border-none">@{{value.protein}}</td> 
        </tr>

         <tr class="total_tr">
           <td  class="first-td-border first_td">TOTAL</td>
           <td  data-title="CALORIES" class="text-center  td-border-none">@{{food_diary.totalBreakfast.calorie || 0}}</td>
           <td  data-title="FAT" class="text-center  td-border-none">@{{food_diary.totalBreakfast.fat || 0}}</td>
           <td  data-title="FIBER" class="text-center  td-border-none">@{{food_diary.totalBreakfast.fiber || 0}}</td>
           <td  data-title="CARBS" class="text-center  td-border-none">@{{food_diary.totalBreakfast.carbs || 0}}</td>
           <td  data-title="SODIUM" class="text-center  td-border-none">@{{food_diary.totalBreakfast.sodium || 0}}</td>
           <td  data-title="PROTEIN" class="text-center  td-border-none">@{{food_diary.totalBreakfast.protein || 0}}</td> 
        </tr>
        <tr class="text-center toptr-border">
          <td data-title=" " colspan="7" class="sidecolor text-leftside withborder head_slide_foodtbl_td">
            <h4 id="Snacks_focus_id">SNACK</h4>
            <ul class="list-inline">
              <li>
                <i class="fa fa-plus-circle facolor"></i>
                 <a ui-sref="log_food({ food_type : 'Snacks'})" class="add_food" id="add_snack">Add Food</a>
               </li>
               <li ng-if="food_diary.snackFood.length>0">
                  <i class="fa fa-cog facolor"></i> 
                  <a ui-sref="create_meal({ food_type: 'Snacks', meal_food: food_diary.snackFood, meal_food_total: food_diary.totalSnacks })" class="add_food" >
                   Save Meal
                  </a>
                </li>
              <!-- <li><i class="fa fa-cog facolor"></i> Quick Tools</li> -->
            </ul>
          </td>
        </tr>
        <tr id="Snacks_focus_id" ng-repeat="(key, value) in food_diary.snackFood">
           <td data-title="Food" class="first-td-border">
           <a href="javascript:void(0);" ng-click="food_diary.deleteLoggedFood(value)">
             <i class="fa fa-times-circle facolor"></i>
           </a>
            @{{value.name}}
           </td>
           <td data-title="CALORIES" class="text-center black_text td-border-none">@{{value.calories}}</td>
           <td data-title="FAT" class="text-center black_text td-border-none">@{{value.fat}}</td>
           <td data-title="FIBER" class="text-center black_text td-border-none">@{{value.fiber}}</td>
           <td data-title="CARBS" class="text-center black_text td-border-none">@{{value.carb}}</td>
           <td data-title="SODIUM" class="text-center black_text  td-border-none">@{{value.sodium}}</td>
           <td data-title="PROTEIN" class="text-center black_text td-border-none">@{{value.protein}}</td> 
        </tr>
        <tr class="total_tr">
           <td  class="first-td-border first_td">TOTAL</td>
           <td  data-title="CALORIES" class="text-center  td-border-none">@{{food_diary.totalSnacks.calorie || 0}}</td>
           <td  data-title="FAT" class="text-center  td-border-none">@{{food_diary.totalSnacks.fat || 0}}</td>
           <td  data-title="FIBER" class="text-center  td-border-none">@{{food_diary.totalSnacks.fiber || 0}}</td>
           <td  data-title="CARBS" class="text-center  td-border-none">@{{food_diary.totalSnacks.carbs || 0}}</td>
           <td  data-title="SODIUM" class="text-center  td-border-none">@{{food_diary.totalSnacks.sodium || 0}}</td>
           <td  data-title="PROTEIN" class="text-center  td-border-none">@{{food_diary.totalSnacks.protein || 0}}</td> 
        </tr>

        <tr class="text-center toptr-border">
          <td data-title=" " colspan="7" class="sidecolor text-leftside withborder head_slide_foodtbl_td">
            <h4 id="Lunch_focus_id">LUNCH</h4>
            <ul class="list-inline">
              <li >
                <i class="fa fa-plus-circle facolor"></i> 
                <a  ui-sref="log_food({ food_type : 'Lunch'})" class="add_food" id="add_lunch"> Add Food </a>
              </li>
              <li ng-if="food_diary.lunchFood.length>0">
                  <i class="fa fa-cog facolor"></i> 
                  <a ui-sref="create_meal({ food_type: 'Lunch', meal_food: food_diary.lunchFood, meal_food_total: food_diary.totalLunch })" class="add_food">
                    Save Meal
                  </a>
                </li>
             <!--  <li><i class="fa fa-cog facolor"></i> Quick Tools</li> -->
            </ul>
          </td>
        </tr>

        <tr  ng-repeat="(key, value) in food_diary.lunchFood">
           <td data-title="Food" class="first-td-border">
           <a href="javascript:void(0);" ng-click="food_diary.deleteLoggedFood(value)">
             <i class="fa fa-times-circle facolor"></i>
           </a>
            @{{value.name}}
           </td>
           <td data-title="CALORIES" class="text-center black_text td-border-none">@{{value.calories}}</td>
           <td data-title="FAT" class="text-center black_text  td-border-none">@{{value.fat}}</td>
           <td data-title="FIBER" class="text-center black_text td-border-none">@{{value.fiber}}</td>
           <td data-title="CARBS" class="text-center black_text td-border-none">@{{value.carb}}</td>
           <td data-title="SODIUM" class="text-center black_text td-border-none">@{{value.sodium}}</td>
           <td data-title="PROTEIN" class="text-center black_text td-border-none">@{{value.protein}}</td> 
        </tr>
         
         <tr class="total_tr">
           <td  class="first-td-border first_td">TOTAL</td>
           <td  data-title="CALORIES" class="text-center  td-border-none">@{{food_diary.totalLunch.calorie || 0}}</td>
           <td  data-title="FAT" class="text-center  td-border-none">@{{food_diary.totalLunch.fat || 0}}</td>
           <td  data-title="FIBER" class="text-center  td-border-none">@{{food_diary.totalLunch.fiber || 0}}</td>
           <td  data-title="CARBS" class="text-center  td-border-none">@{{food_diary.totalLunch.carbs || 0}}</td>
           <td  data-title="SODIUM" class="text-center  td-border-none">@{{food_diary.totalLunch.sodium || 0}}</td>
           <td  data-title="PROTEIN" class="text-center  td-border-none">@{{food_diary.totalLunch.protein || 0}}</td> 
         </tr>


        <tr class="text-center toptr-border">
          <td data-title=" " colspan="7" class="sidecolor text-leftside withborder head_slide_foodtbl_td">
            <h4 id="Dinner_focus_id">DINNER</h4>
            <ul class="list-inline">
              <li><i class="fa fa-plus-circle facolor"></i> 
                <a ui-sref="log_food({ food_type : 'Dinner'})" class="add_food" id="add_dinner">
                  Add Food
                </a>
                </li>
              <li ng-if="food_diary.dinnerFood.length>0">
                <i class="fa fa-cog facolor"></i> 
                <a ui-sref="create_meal({ food_type: 'Dinner', meal_food: food_diary.dinnerFood, meal_food_total: food_diary.totalDinner })" class="add_food"  >
                 Save Meal
                </a>
              </li>
             <!--  <li><i class="fa fa-cog facolor"></i> Quick Tools</li> -->
            </ul>
          </td>
        </tr>
         <tr ng-repeat="(key, value) in food_diary.dinnerFood">
           <td data-title="Food" class="first-td-border">
           <a href="javascript:void(0);" ng-click="food_diary.deleteLoggedFood(value)">
             <i class="fa fa-times-circle facolor"></i>
           </a>
            @{{value.name}}
           </td>
           <td data-title="CALORIES" class="text-center black_text td-border-none">@{{value.calories}}</td>
           <td data-title="FAT" class="text-center black_text td-border-none">@{{value.fat}}</td>
           <td data-title="FIBER" class="text-center black_text td-border-none">@{{value.fiber}}</td>
           <td data-title="CARBS" class="text-center black_text td-border-none">@{{value.carb}}</td>
           <td data-title="SODIUM" class="text-center black_text td-border-none">@{{value.sodium}}</td>
           <td data-title="PROTEIN" class="text-center black_text td-border-none">@{{value.protein}}</td> 
        </tr>

         <tr class="total_tr">
           <td  class="first-td-border first_td">TOTAL</td>
           <td  data-title="CALORIES" class="text-center  td-border-none">@{{food_diary.totalDinner.calorie || 0}}</td>
           <td  data-title="FAT" class="text-center  td-border-none">@{{food_diary.totalDinner.fat || 0}}</td>
           <td  data-title="FIBER" class="text-center  td-border-none">@{{food_diary.totalDinner.fiber || 0}}</td>
           <td  data-title="CARBS" class="text-center  td-border-none">@{{food_diary.totalDinner.carbs || 0}}</td>
           <td  data-title="SODIUM" class="text-center  td-border-none">@{{food_diary.totalDinner.sodium || 0}}</td>
           <td  data-title="PROTEIN" class="text-center  td-border-none">@{{food_diary.totalDinner.protein || 0}}</td> 
         </tr>

        <tr class="toptr-border">
          <td data-title=" " class="text-right withborder head_slide_foodtbl_td"><h4>YOUR DAILY GOAL</h4></td>
          <td data-title="CALORIES" class="sidecolor withborder">@{{food_diary.foodSummary.calorie_goal || 0}}</td>
          <td data-title="FAT" class="sidecolor withborder">&nbsp;</td>
          <td data-title="FIBER" class="sidecolor withborder">&nbsp;</td>
          <td data-title="CARBS" class="sidecolor withborder">&nbsp;</td>
          <td data-title="SODIUM" class="sidecolor withborder">&nbsp;</td>
          <td data-title="PROTEIN" class="sidecolor withborder">&nbsp;</td>
        </tr>
        <tr class="toptr-border">
          <td data-title=" " class="text-right withborder head_slide_foodtbl_td"><h4>TOTAL CONSUMED</h4></td>
          <td data-title="CALORIES" class="sidecolor withborder">@{{food_diary.foodSummary.total_consumed || 0}}</td>
          <td data-title="FAT" class="sidecolor withborder">&nbsp;</td>
          <td data-title="FIBER" class="sidecolor withborder">&nbsp;</td>
          <td data-title="CARBS" class="sidecolor withborder">&nbsp;</td>
          <td data-title="SODIUM" class="sidecolor withborder">&nbsp;</td>
          <td data-title="PROTEIN" class="sidecolor withborder">&nbsp;</td>
        </tr>
        <tr class="toptr-border">
          <td data-title=" " class="text-right withborder head_slide_foodtbl_td"><h4>% OF GOAL</h4></td>
          <td data-title="CALORIES" class="sidecolor withborder">@{{food_diary.foodSummary.calorie_percentage || 0}}</td>
          <td data-title="FAT" class="sidecolor withborder">&nbsp;</td>
          <td data-title="FIBER" class="sidecolor withborder">&nbsp;</td>
          <td data-title="CARBS" class="sidecolor withborder">&nbsp;</td>
          <td data-title="SODIUM" class="sidecolor withborder">&nbsp;</td>
          <td data-title="PROTEIN" class="sidecolor withborder">&nbsp;</td>
        </tr>
        <tr class="toptr-border">
          <td data-title=" " class="text-right withborder head_slide_foodtbl_td"><h4>TOTAL REMAINING</h4></td>
          <td data-title="CALORIES" class="sidecolor withborder">@{{food_diary.foodSummary.total_remaining || 0}}</td>
          <td data-title="FAT" class="sidecolor withborder">&nbsp;</td>
          <td data-title="FIBER" class="sidecolor withborder">&nbsp;</td>
          <td data-title="CARBS" class="sidecolor withborder">&nbsp;</td>
          <td data-title="SODIUM" class="sidecolor withborder">&nbsp;</td>
          <td data-title="PROTEIN" class="sidecolor withborder">&nbsp;</td>
        </tr>
        <tr class="toptr-border last_tr">
          <th class="withborder"></th>
          <th class="calorie-subhead-td withborder">CALORIES</th>
          <th class="calorie-subhead-td withborder">FAT</th>
          <th class="calorie-subhead-td withborder">FIBER</th>
          <th class="calorie-subhead-td withborder">CARBS</th>
          <th class="calorie-subhead-td withborder">SODIUM</th>
          <th class="calorie-subhead-td withborder">PROTEIN</th>
        </tr>
      </tbody>
    </table>
</div>

<div class="row" >
  <div class="col-xs-12 text-center">
    <br/>
    When you are finished logging all foods and exercise for this day, click here:<br/><br/>
    <button class="btn btn-primary btndesign"  ng-click="food_diary.addFoodCompleted()">Complete This Entry</button>
  </div>
</div>

<div class="row food_last_row headrow-samecolumnheight">
  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
    <div class="row">
      <div class="col-xs-9 col-sm-9 col-md-9">
          <h4>WATER CONSUMPTION</h4>
            We recommend that you drink at least 8 cups of water a day. <br/>
            Click the arrows to add or subtract cups of water.
      </div>
      <div class="col-xs-3 col-sm-3 col-md-3 text-right">
        <ul class="list-unstyled centertext">
          
          <li>
            <div style="width:93px">
              <div>
                <a href="javascript:void(0)" ng-click="food_diary.addWaterIntake()"><i class="fa fa-caret-up fa-2x"></i></a>
              </div>
              <div>
              <span id="water_intake" ng-bind="food_diary.water_intake"></span>
              <img id="glass" src="images/glass.jpg" width="" height="">
              </div>
              <div>
                <i ng-if="food_diary.water_intake<=0" class="fa fa-caret-down fa-2x fa-caret-up-down-disable"></i>
                <a ng-if="food_diary.water_intake>0" href="javascript:void(0)" ng-click="food_diary.deleteWaterIntake()"><i class="fa fa-caret-down fa-2x"></i></a>
              
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
    
  </div>
  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 second_row_foot">
    <div class="row footnoterow">
      <div class="col-xs-12 col-sm-12 col-md-12">
        <h4>
          TODAY'S FOOD NOTES  
          <small class="pull-right"><a href="#">Edit Note <i class="fa fa-pencil"></i></a></small>
        </h4>
        <!-- <textarea class="form-control textcolor" ng-disabled="true" rows="4"></textarea>  -->
          <a href="javascript:void(0);" editable-textarea="food_diary.food_notes" e-rows="20" e-cols="40" onaftersave="food_diary.update_notes();">
          <pre class="food_notes">@{{ food_diary.food_notes || 'no description' }}</pre>
        </a> 
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 text-center">
    <button class="btn btn-primary btndesign">View Full Report</button>
    <button class="btn btn-primary btndesign"><i class="fa fa-print"></i> Print Full Report</button>
  </div>
</div>


 

  