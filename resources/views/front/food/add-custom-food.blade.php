 
<div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12" ng-init="diary_index.userId='{{Auth::user()->id}}';diary_index.user_gender='{{Auth::user()->gender}}';">
    <h2 class="text-light-blue text-uppercase head_title_main">
      Add <span class="title_head_line">Custom</span> Food 
    </h2>
</div> 
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <a class="btn btn-primary btndesign" ui-sref="diary">Back</a>
    <div class="small-separator"></div>
</div>

<!-- <div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12" ng-init="diary_index.userId='{{Auth::user()->id}}';">
    
  <strong>
    <h2 class="text-light-blue text-uppercase">
      <a class="pull-left btn btn-primary btndesign" ui-sref="diary">Back</a>

      Add <span class="title_head_line">Custom</span> Food </h2>
  </strong>
  <div class="saperator"></div>
</div>
 -->
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
  
 <div class="panel panel-primary weight_body_panel weight_diary_panel_size goalsummary_tbl1 inner_diady_pages_panel">
        <div class="panel-heading panel_head_background panel_head_log_food">CREATE FOOD</div>
        <div class="panel-body panel-contain_div_logfood panel_Bodypart">
        <div ng-show="diary_index.log_newfoodloader"  class="custom_food_loader"></div> 
          <form name="customFoodForm" class="form-horizontal add_custom_food_form" ng-submit="diary_index.add_new_food()"> 
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-4 control-label text-right">Food Name <span class="error_red_text">*</span></label>
                  <div class="col-sm-7 input-group">
                    <input type="text"  ng-model="diary_index.custom_food.food_name"  class="form-control" id="inputEmail3" placeholder="" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-4 control-label text-right">Food Brand <span class="error_red_text">*</span></label>
                  <div class="col-sm-7 input-group">
                    <input type="text"  ng-model="diary_index.custom_food.brand_name"  class="form-control" id="inputEmail3" placeholder="" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-4 control-label text-right">Serving Size <span class="error_red_text">*</span></label>
                  <div class="col-sm-3 no-padding-left">
                    <input type="text"  ng-model="diary_index.custom_food.serving_quantity" class="form-control" id="inputEmail3" placeholder="Quantity" required>
                  </div>
                  <div class="col-sm-4 no-padding-left">
                    <input type="text"  ng-model="diary_index.custom_food.serving_size_unit"  class="form-control" id="inputEmail3" placeholder="Unit" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-4 control-label text-right">Calories <span class="error_red_text">*</span></label>
                  <div class="col-sm-7 input-group">
                    <input type="text" ng-model="diary_index.custom_food.calories" class="form-control" id="inputEmail3" placeholder="" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-4 control-label text-right">Total Fat</label>
                  <div class="col-sm-7 input-group">
                    <input type="text" ng-model="diary_index.custom_food.total_fat" class="form-control" id="inputEmail3" placeholder="">
                    <div class="input-group-addon">g</div>
                  </div>
                  
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-4 control-label text-right">Saturated Fat</label>
                  <div class="col-sm-7 input-group">
                    <input type="text" ng-model="diary_index.custom_food.saturated_fat" class="form-control" id="inputEmail3" placeholder="">
                    <div class="input-group-addon">g</div>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-4 control-label text-right">Trans Fat</label>
                  <div class="col-sm-7 input-group">
                    <input type="text" ng-model="diary_index.custom_food.trans_fat" class="form-control" id="inputEmail3" placeholder="">
                    <div class="input-group-addon">g</div>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-4 control-label text-right">Cholesterol</label>
                  <div class="col-sm-7 input-group">
                    <input type="text" ng-model="diary_index.custom_food.cholesterol" class="form-control" id="inputEmail3" placeholder="">
                    <div class="input-group-addon">mg</div>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-4 control-label text-right">Sodium</label>
                  <div class="col-sm-7 input-group">
                    <input type="text" ng-model="diary_index.custom_food.sodium" class="form-control" id="inputEmail3" placeholder="">
                    <div class="input-group-addon">mg</div>
                  </div>
                </div>              
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-4 control-label text-right">Potassium</label>
                  <div class="col-sm-7 input-group">
                    <input type="text" ng-model="diary_index.custom_food.potassium" class="form-control" id="inputEmail3" placeholder="">
                    <div class="input-group-addon">mg</div>
                  </div>
                </div>
              </div>

              <div class="col-sm-6">
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-4 control-label text-right">Total Carbs</label>
                  <div class="col-sm-7 input-group">
                    <input type="text" ng-model="diary_index.custom_food.total_carbs" class="form-control" id="inputEmail3" placeholder="">
                    <div class="input-group-addon">g</div>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-4 control-label text-right">Polyunsaturated</label>
                  <div class="col-sm-7 input-group">
                    <input type="text" ng-model="diary_index.custom_food.polyunsaturated" class="form-control" id="inputEmail3" placeholder="">
                    <div class="input-group-addon">g</div>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-4 control-label text-right">Dietary Fiber</label>
                  <div class="col-sm-7 input-group">
                    <input type="text" ng-model="diary_index.custom_food.dietary_fiber" class="form-control" id="inputEmail3" placeholder="">
                    <div class="input-group-addon">g</div>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-4 control-label text-right">Monounsaturated</label>
                  <div class="col-sm-7 input-group">
                    <input type="text" ng-model="diary_index.custom_food.monounsaturated" class="form-control" id="inputEmail3" placeholder="">
                    <div class="input-group-addon">g</div>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-4 control-label text-right">Sugar</label>
                  <div class="col-sm-7 input-group">
                    <input type="text" ng-model="diary_index.custom_food.sugar" class="form-control" id="inputEmail3" placeholder="">
                    <div class="input-group-addon">g</div>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-4 control-label text-right">Protein</label>
                  <div class="col-sm-7 input-group">
                    <input type="text" ng-model="diary_index.custom_food.protein" class="form-control" id="inputEmail3" placeholder="">
                    <div class="input-group-addon">g</div>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-4 control-label text-right">Vitamin A</label>
                  <div class="col-sm-7 input-group">
                    <input type="text" ng-model="diary_index.custom_food.vitamin_a" class="form-control" id="inputEmail3" placeholder="">
                    <div class="input-group-addon">%</div>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-4 control-label text-right">Vitamin C</label>
                  <div class="col-sm-7 input-group">
                    <input type="text" ng-model="diary_index.custom_food.vitamin_c" class="form-control" id="inputEmail3" placeholder="">
                    <div class="input-group-addon">%</div>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-4 control-label text-right">Calcium</label>
                  <div class="col-sm-7 input-group">
                    <input type="text" ng-model="diary_index.custom_food.calcium" class="form-control" id="inputEmail3" placeholder="">
                    <div class="input-group-addon">%</div>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-4 control-label text-right">Iron</label>
                  <div class="col-sm-7 input-group">
                    <input type="text" ng-model="diary_index.custom_food.iron" class="form-control" id="inputEmail3" placeholder="">
                    <div class="input-group-addon">%</div>
                  </div>
                </div>
              </div>
            </div>
            <div class="saperator"></div>
            
            <div class="form-group row">
              <div class="col-sm-offset-5 col-sm-12">
                <button type="submit" class="btn btn-primary btndesign" ng-disabled="customFoodForm.$invalid">Create</button>
              </div>
            </div>
          </form>
        </div>
 </div>
</div>