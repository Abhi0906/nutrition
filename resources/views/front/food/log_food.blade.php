
<div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <h2 id="log_food_focus_id" class="text-light-blue text-uppercase head_title_main">
      LOG <span class="title_head_line"><span ng-bind="add_food.getScheduleTime(add_food.food_type);"></span>
            </span>
    </h2>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" ng-init="add_food.favouriteFood(); add_food.allLoggedFood();add_food.recommended_food_type_id=1; add_food.getFoodRecommendedTypes();add_food.getMdRecommendedFoods();add_food.getMealsByUser();">
    <a class="btn btn-primary btndesign" ui-sref="diary">Back</a>
    <div class="small-separator"></div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
 <div class="panel panel-primary weight_body_panel weight_diary_panel_size goalsummary_tbl1 inner_diady_pages_panel">
        <div class="panel-heading panel_head_background panel_head_log_food">
          LOG FOOD
        </div>

        <div class="panel-body panel-contain_div_logfood panel_Bodypart">
          <form name="SearchFood" class="form-horizontal log_search_box">
            <div class="form-group margin_bot">
              <label class="control-label col-sm-2 theme_blue_color">Enter Food: </label>
              <div class="col-sm-8 padd_side">
                <div class="input-group btn_group_search">
                  <input type="text" class="form-control" ng-model="add_food.searchKeyFood" aria-describedby="basic-addon2">
                  <span class="input-group-addon no_padding">
                    <button type="submit" class="btn btn-primary btndesign no_margin search_button" ng-click="add_food.searchFood()">Search</button>
                  </span>
                </div>
              </div>
              <div class="col-sm-2 padd_side">
                <button class="btn btn-primary btndesign no_margin" ng-click="add_food.clear_logFood_searchResult();">Clear</button>
              </div>
            </div>
          </form>
        </div>
        <div class="panel-body panel_Bodypart topNoBorder">

          <div ng-hide="add_food.is_selected" class="logfood_tab_continer">
            <div id="log_food_tab" class="tabs_container">
              <ul class="nav nav-tabs" id="tabs_logfood" ng-show="add_food.food_tab_display">

                <li id="md_food_tab" class="active one"><a href="/log_food/#md_recfood_tab" data-toggle="tab">MD Recommended Foods</a></li>
                <li id="md_meal_tab" ><a href="/log_food/#md_recmeal_tab" data-toggle="tab">MD Recommended Meals</a></li>
                <li id="fav_tab"><a href="/log_food/#fav_log_tab" data-toggle="tab">Favorite</a></li>
                <li id="req_tab"><a href="/log_food/#fre_log_tab" data-toggle="tab">Frequent</a></li>
                <li id="rec_tab"><a href="/log_food/#rec_log_tab" data-toggle="tab">Recent</a></li>
                <li id="cust_tab"><a href="/log_food/#cust_log_tab" data-toggle="tab">Custom</a></li>
                <!-- <li id="sel_tab" ng-show="add_food.is_selected"><a href="/log_food/#selected_food_tab" data-toggle="tab">Selected</a></li>
                <li id="sel_meal_tab" ng-show="add_food.is_meal_selected"><a href="/log_food/#selected_meal_tab" data-toggle="tab">Selected</a></li> -->
                <li id="meal_tab"><a href="/log_food/#meal_food_tab" data-toggle="tab">My Meals</a></li>

              </ul>
              <div class="tab-content logfood_tab_details">
                 <div id="fav_log_tab" class="tab-pane fade in ">
                    @include('front.food.log-food.fav_log_tab')
                 </div>
                 <div id="fre_log_tab" class="tab-pane fade in" >
                    @include('front.food.log-food.fre_log_tab')
                 </div>
                 <div id="rec_log_tab" class="tab-pane fade in">
                    @include('front.food.log-food.rec_log_tab')
                 </div>
                 <div id="cust_log_tab" class="tab-pane fade in">
                    @include('front.food.log-food.cust_log_tab')
                 </div>
                 <div id="searched_food_tab" class="tab-pane fade in">
                    @include('front.food.log-food.searched_food_tab')
                 </div>

                 <div id="meal_food_tab" class="tab-pane fade in">
                    @include('front.food.log-food.meal_food_tab')
                 </div>

                 <div id="md_recfood_tab" class="tab-pane fade in active">
                   @include('front.food.log-food.md_recfood_tab')
                 </div>
                 <div id="md_recmeal_tab" class="tab-pane fade in ">
                   @include('front.food.log-food.md_recmeal_tab')
                 </div>
              </div>
            </div>
          </div>

          <div ng-show="add_food.is_selected" >
            <div class="row">
              <div class="col-xs-3">
                <label class="control-label font_15 theme_blue_color myroutine_p">
                  @{{add_food.selectedFoodTypeName}}
                </label>
              </div>
              <div class="col-xs-9 text-right">
                <!-- <button class="btn btn-primary btndesign no_margin" ng-click="add_food.selectLogFoodTab('cust_tab');">All @{{add_food.selectedFoodTypeName}}</button>           -->
                <button class="btn btn-primary btndesign no_margin" ng-click="add_food.clear_logFood_searchResult();">Cancel</button>
              </div>
            </div>
            <div class="saperator"></div>

            <div id="selected_food_tab" ng-show="!add_food.is_meal_selected" class="tab-pane fade in">
              @include('front.food.log-food.selected_food_tab')
            </div>
            <div id="selected_meal_tab" ng-show="add_food.is_meal_selected" class="tab-pane fade in">
              @include('front.food.log-food.selected_meal_tab')
           </div>

          </div>
        </div>
  </div>
</div>

<!-- view models -->
<div id="log_water_tmplt" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false" style="z-index:9999">
  @include('front.food.view_nutritions_detail')
</div>

<div id="meal_detail_tmplt" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
  @include('front.food.meal_detail')
</div>