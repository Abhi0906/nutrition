 
<div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12" ng-init="meal.getMeals()">
    <h2 class="text-light-blue text-uppercase head_title_main">
      M<span class="title_head_line">eal</span>s 
    </h2>
</div> 
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <a class="btn btn-primary btndesign" ui-sref="diary">Back</a>
    <div class="small-separator"></div>
</div>




<!-- <div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12" ng-init="meal.getMeals()">
    
  <strong>
    <h2 class="text-light-blue text-uppercase">
      <a class="pull-left btn btn-primary btndesign" ui-sref="diary">Back</a>

      M<span class="title_head_line">eal</span>s
    </h2>
  </strong>
  <div class="saperator"></div>
</div> -->

<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
  
 <div class="panel panel-primary weight_body_panel weight_diary_panel_size goalsummary_tbl1 inner_diady_pages_panel">
        
        <div class="panel-body panel_Bodypart">
          <div class="logfood_tab_continer">
            <div id="log_food_tab" class="tabs_container">
              <div class="tab-content logfood_tab_details">
                 <div id="fav_log_tab" class="tab-pane fade in active"> 
                    <div class="tbl_tab_food_log">
                      <table ng-if="meal.meals.length>0" class="table table-bordered tbl_log_food text-center">
                        <thead>
                          <tr>
                            <th><h4>MEAL NAME</h4></th>
                            <th><h4>TOTAL CALORIES</h4></th>
                            <th><h4 class="text-uppercase">Created at</h4></th>
                            <th class="td_fixed_width text-uppercase"><h4>Delete Meal</h4></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr ng-repeat="mealData in meal.meals">
                            <td class="text-left">
                              <p class="theme_text_p_lg_md">@{{mealData.meal_name}}</p>
                            </td>
                            <td>
                              <p class="food_log_normalbold">
                                @{{mealData.total_calories}}
                              </p>
                            </td>
                            <td>
                              @{{mealData.created_at | custom_date_format : 'MM/dd/yyyy'  }}
                            </td>
                            <td>
                              <button class="btn btn-primary btndesign" ng-click="meal.deleteMeal(mealData)">Delete</button>
                              
                            </td>
                          </tr>
                          <tr>
                            <td colspan="4"></td>
                          </tr>
                        </tbody>
                      </table>
                      <div ng-if="meal.meals.length==0" class="text-center">No meal found.</div>
                      <div ng-show="meal.meal_loader"  class="custom_food_grid_loader"></div>
                    </div>
                 </div>
              </div>
            </div>
          </div>
        </div>
 </div>
</div>