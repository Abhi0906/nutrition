<div class="tabs_food_foods">
<h3>FAVORITES</h3>
<div id="food_meal_tabs_foods" class="tabs_container">
  <ul class="nav nav-tabs" id="tabs3">
    <li class="active one"><a href="#meals_tab" data-toggle="tab">MY MEALS</a></li>
    <li class=""><a href="#foods_tab" data-toggle="tab">FOODS</a></li>
    <li class=""><a href="#customfoods_tab" data-toggle="tab">CUSTOM</a></li>    
  </ul>
  <div class="tab-content tabs_details">
    <div id="meals_tab" class="tab-pane fade in active">
      <table class="table table-hover table-bordered right_tbl_partlist">
        <thead>
          <tr>
            <th>Cals</th>
            <th>Click on an item to log it.</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="meal in food.meals">
            <td>@{{meal.total_calories}}</td>
            <td>
               <a ui-sref="log_food({ selected_from : 'meal', selected_food : @{{meal}} })">
                @{{meal.meal_name | wordlimit:true:35:' ...' }}
              </a>
            </td>
          </tr>

          <tr class="last_tr_white">
            <td colspan="2"><a ui-sref="add_custom_food" class="a_last_tr">
          <a ui-sref="meals" class="a_last_tr">Edit Meals</a></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div id="foods_tab" class="tab-pane fade in "> 
      <table class="table table-hover table-bordered right_tbl_partlist">
        <thead>
          <tr>
            <th>Cals</th>
            <th>Click on an item to log it.</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="fav_food in food.favourite_nutritions">
            <td>@{{fav_food.nutritions.nutrition_data.calories}}</td>
            <td>
                <a ui-sref="log_food({ selected_from : 'favourite', selected_food : @{{fav_food}} })">
                @{{fav_food.nutritions.name | wordlimit:true:35:' ...' }}
              </a>
            </td>
          </tr>
         
          <tr class="last_tr_white">
            <td colspan="2"><a ui-sref="add_custom_food" class="a_last_tr" >Create a new food</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <a ui-sref="custom_foods" class="a_last_tr">Edit my foods</a></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div id="customfoods_tab" class="tab-pane fade in">
      <table class="table table-hover table-bordered right_tbl_partlist">
        <thead>
          <tr>
            <th>Cals</th>
            <th>Click on an item to log it.</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="custom_food in food.custom_nutritions">
            <td>@{{custom_food.nutrition_data.calories}}</td>
            <td>
               <a ui-sref="log_food({ selected_from : 'custom', selected_food : @{{custom_food}} })">
                @{{custom_food.name | wordlimit:true:35:' ...' }}
              </a>
            </td>
          </tr>
          <tr class="last_tr_white">
            <td colspan="2"><a ui-sref="add_custom_food" class="a_last_tr">Create a new food</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <a ui-sref="custom_foods" class="a_last_tr">Edit my foods</a></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>

<div class="tabs_food_fav">
<h3>FOODS</h3>
<div id="food_meal_tabs_fav" class="tabs_container">
  <ul class="nav nav-tabs" id="tabs2">
    <li class="active one"><a href="#frequent_tab" data-toggle="tab">MOST LOGGED</a></li>
    <li><a href="#recent_tab" data-toggle="tab">RECENT</a></li>
  </ul>
  <div class="tab-content tabs_details">
     <div id="frequent_tab" class="tab-pane fade in active"> 
         <table class="table table-hover table-bordered right_tbl_partlist">
          <thead>
            <tr>
              <th>Cals</th>
              <th>Click on an item to log it.</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="freq_food in food.frequent_nutritions">
              <td>@{{freq_food.calories}}</td>
              <td>
                  <a ui-sref="log_food({ selected_from : 'frequent', selected_food : @{{freq_food}} })">
                    @{{freq_food.nutritions.name | wordlimit:true:35:' ...' }}
                  </a>
              </td>
            </tr>

            <tr class="last_tr_white">
              <td colspan="2"><a ui-sref="add_custom_food" class="a_last_tr">Create a new food</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a ui-sref="custom_foods" class="a_last_tr">Edit my foods</a></td>
            </tr>
          </tbody>
        </table>
     </div>
     <div id="recent_tab" class="tab-pane fade in"> 
         <table class="table table-hover table-bordered right_tbl_partlist">
          <thead>
            <tr>
              <th>Cals</th>
              <th>Click on an item to log it.</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="rec_food in food.recent_nutritions">
              <td>@{{rec_food.calories}}</td>
              <td>
                  <a ui-sref="log_food({ selected_from : 'recent', selected_food : @{{rec_food}} })">
                    @{{rec_food.nutritions.name | wordlimit:true:35:' ...' }}
                  </a>
              </td>
            </tr>

            <tr class="last_tr_white">
              <td colspan="2"><a ui-sref="add_custom_food" class="a_last_tr">Create a new food</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a ui-sref="custom_foods" class="a_last_tr">Edit my foods</a></td>
            </tr>
          </tbody>
        </table>
     </div>
  </div>
</div>
</div>