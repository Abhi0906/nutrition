
<div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <h2 class="text-light-blue text-uppercase head_title_main">
      C<span class="title_head_line">reate Mea</span>l
    </h2>
</div> 
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <a class="btn btn-primary btndesign" ui-sref="diary">Back</a>
    <div class="small-separator"></div>
</div>


<!-- <div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12">
    
  <strong>
    <h2 class="text-light-blue text-uppercase">
      <a class="pull-left btn btn-primary btndesign" ui-sref="diary">Back</a>

      C<span class="title_head_line">reate Mea</span>l </h2>
  </strong>
  <div class="saperator"></div>
</div> -->

<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
 <div class="panel panel-primary weight_body_panel weight_diary_panel_size goalsummary_tbl1 inner_diady_pages_panel">
        <div class="panel-heading panel_head_background panel_head_log_food">CREATE MEAL</div>
        <div class="panel-body panel-contain_div_logfood panel_Bodypart"> 


          <form name="createMeal" class="form-horizontal log_search_box">
            <div class="form-group">
              <label class="control-label col-sm-5 col-md-4 theme_blue_color">Name this meal (i.e. 'Healthy Supper')</label>
              <div class="col-sm-5 col-md-6 padd_side">
                <div class="input-group btn_group_search">
                  <input type="text" class="form-control" ng-model="meal.meal_name" aria-describedby="basic-addon2" placeholder="Enter meal name" required>
                  <span class="input-group-addon no_padding">
                    <button type="submit" class="btn btn-primary btndesign no_margin search_button" ng-disabled="createMeal.$invalid" ng-click="meal.create_meal()">Save Meal</button>
                  </span>
                </div>
              </div>
              <div class="col-sm-2 col-md-2 padd_side">
                <a class="btn btn-primary btndesign" ui-sref="diary">Cancel</a>
              </div>
            </div>
          </form>

          <!-- <form name="createMeal"  class="form-inline">    
            <p class="theme_text_p_lg">Name this meal (i.e. 'Healthy Supper')</p>

            <div class="row">
              <div class="col-sm-12">
                <div class="btn_group_search">
                  
                    <input style="width:75%"  type="text" ng-model="meal.meal_name" class="form-control formcontrol_height"  aria-describedby="basic-addon2" placeholder="Enter meal name" required>
                    <div class="space_between visible-xs"></div>
                    <button type="submit" class="btn btn-primary btndesign no_margin search_button" ng-disabled="createMeal.$invalid" ng-click="meal.create_meal()">Save Meal</button>
                    
                    <a class="btn btn-primary btndesign" ui-sref="diary">Cancel</a>
                </div>
              </div>
            </div>
          </form> -->

          <!-- <div class="saperator"></div> -->
          <hr class="hr_line hr_medium">
          <div class="food_dairy_block" id="responsive_food_table">             
            <table class="table table-bordered borderless_table margin_bottom_zero" id="simple-example-table">
              <thead> 
                <tr class="toptr-border">
                  <th class="calorie-subhead-td withborder">&nbsp;</th>
                  <th class="calorie-subhead-td withborder">CALORIES</th>
                  <th class="calorie-subhead-td withborder">FAT</th>
                  <th class="calorie-subhead-td withborder">FIBER</th>
                  <th class="calorie-subhead-td withborder">CARBS</th>
                  <th class="calorie-subhead-td withborder">SODIUM</th>
                  <th class="calorie-subhead-td withborder">PROTEIN</th>
                </tr>
              </thead>
              <tbody>
                
                <tr ng-repeat="(key, value) in meal.meal_food">
                   <td data-title="Food" class="first-td-border">
                    @{{value.name}}
                   </td>
                   <td data-title="CALORIES" class="text-center black_text td-border-none">@{{value.calories}}</td>
                   <td data-title="FAT" class="text-center black_text td-border-none">@{{value.fat}}</td>
                   <td data-title="FIBER" class="text-center black_text td-border-none">@{{value.fiber}}</td>
                   <td data-title="CARBS" class="text-center black_text td-border-none">@{{value.carb}}</td>
                   <td data-title="SODIUM" class="text-center black_text td-border-none">@{{value.sodium}}</td>
                   <td data-title="PROTEIN" class="text-center black_text td-border-none">@{{value.protein}}</td> 
                </tr>

                <tr class="total_tr">
                    <td  class="first-td-border first_td">TOTAL</td>
                    <td  data-title="CALORIES" class="text-center  td-border-none">@{{meal.meal_food_total.calorie || 0}}</td>
                    <td  data-title="FAT" class="text-center  td-border-none">@{{meal.meal_food_total.fat || 0}}</td>
                    <td  data-title="FIBER" class="text-center  td-border-none">@{{meal.meal_food_total.fiber || 0}}</td>
                    <td  data-title="CARBS" class="text-center  td-border-none">@{{meal.meal_food_total.carbs || 0}}</td>
                    <td  data-title="SODIUM" class="text-center  td-border-none">@{{meal.meal_food_total.sodium || 0}}</td>
                    <td  data-title="PROTEIN" class="text-center  td-border-none">@{{meal.meal_food_total.protein || 0}}</td> 
                </tr>
                </tbody>
            </table>
          </div>

        </div>
  </div>
</div>