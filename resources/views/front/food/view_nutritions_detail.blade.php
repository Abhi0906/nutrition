<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 modal-dialog" style="float:none">
  
 <div class="panel panel-primary weight_body_panel weight_diary_panel_size goalsummary_tbl1">
        <div class="panel-heading panel_head_background panel_head_log_food">
          <div class="row">
            <div class="col-xs-12">
            	<button type="button" class="close" data-dismiss="modal" >
			        <span aria-hidden="true" >&times;</span>
			        <span class="sr-only" >Close</span>
			    </button>
              <h4>
              	<strong>@{{add_food.food_detail.name}}</strong>
              	<p class="tag_text_normal">@{{add_food.food_detail.brand_name}}</p>
              </h4>
            </div>
          </div>
        </div>
        <div class="panel-body panel-contain_div_logfood">
          
            <div class="row">
              <div class="col-sm-6">

                <div class="row">
                  <label class="col-sm-7  text-right">Calories</label>
                  <div class="col-sm-5">
                    <p><strong>@{{add_food.food_detail.nutrition_data.calories}}</strong></p>	
                  </div>
                </div>

                <div class="row">
                  <label class="col-sm-7 text-right">Serving Size</label>
                  <div class="col-sm-5 no-padding-left">
                    <p><strong>@{{add_food.food_detail.nutrition_data.serving_quantity}} @{{add_food.food_detail.nutrition_data.serving_size_unit}}</span>	
                    	</strong></p>
                  </div>
                </div>

                <div class="row">
                  <label class="col-sm-7 text-right">Total Fat</label>
                  <div class="col-sm-5">
                    <p><strong>@{{add_food.food_detail.nutrition_data.total_fat}}</strong></p>
                  </div>
                  
                </div>
                <div class="row">
                  <label class="col-sm-7 text-right">Saturated Fat</label>
                  <div class="col-sm-5">
                    <p><strong>@{{add_food.food_detail.nutrition_data.saturated_fat}}</strong></p>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-7 text-right">Trans Fat</label>
                  <div class="col-sm-5">
                    <p><strong>@{{add_food.food_detail.nutrition_data.trans_fat}}</strong></p>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-7 text-right">Cholesterol</label>
                  <div class="col-sm-5">
                    <p><strong>@{{add_food.food_detail.nutrition_data.cholesterol}}</strong></p>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-7 text-right">Sodium</label>
                  <div class="col-sm-5">
                    <p><strong>@{{add_food.food_detail.nutrition_data.sodium}}</strong></p>
                  </div>
                </div>              
                <div class="row">
                  <label class="col-sm-7 text-right">Potassium</label>
                  <div class="col-sm-5">
                    <p><strong>@{{add_food.food_detail.nutrition_data.potassium}}</strong></p>
                  </div>
                </div>
              
                <div class="row">
                  <label class="col-sm-7 text-right">Total Carbs</label>
                  <div class="col-sm-5">
                    <p><strong>@{{add_food.food_detail.nutrition_data.total_carb}}</strong></p>
                  </div>
                </div>
              </div>

              <div class="col-sm-6">
                <div class="row">
                  <label class="col-sm-7 text-right">Polyunsaturated</label>
                  <div class="col-sm-5">
                    <p><strong>@{{add_food.food_detail.nutrition_data.polyunsaturated}}</strong></p>
                  </div>
                </div>

                <div class="row">
                  <label class="col-sm-7 text-right">Dietary Fiber</label>
                  <div class="col-sm-5">
                    <p><strong>@{{add_food.food_detail.nutrition_data.dietary_fiber}}</strong></p>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-7 text-right">Monounsaturated</label>
                  <div class="col-sm-5">
                    <p><strong>@{{add_food.food_detail.nutrition_data.monounsaturated}}</strong></p>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-7 text-right">Sugar</label>
                  <div class="col-sm-5">
                    <p><strong>@{{add_food.food_detail.nutrition_data.sugars}}</strong></p>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-7 text-right">Protein</label>
                  <div class="col-sm-5">
                    <p><strong>@{{add_food.food_detail.nutrition_data.protein}}</strong></p>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-7 text-right">Vitamin A</label>
                  <div class="col-sm-5">
                    <p><strong>@{{add_food.food_detail.nutrition_data.vitamin_a}}</strong></p>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-7 text-right">Vitamin C</label>
                  <div class="col-sm-5">
                    <p><strong>@{{add_food.food_detail.nutrition_data.vitamin_c}}</strong></p>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-7 text-right">Calcium</label>
                  <div class="col-sm-5">
                    <p><strong>@{{add_food.food_detail.nutrition_data.calcium_dv}}</strong></p>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-7 text-right">Iron</label>
                  <div class="col-sm-5">
                    <p><strong>@{{add_food.food_detail.nutrition_data.iron_dv}}</strong></p>
                  </div>
                </div>
              </div>
            </div>
        </div>
 </div>
</div>