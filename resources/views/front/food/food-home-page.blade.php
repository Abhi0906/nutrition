 <div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <strong><h2 class="text-light-blue">YOUR <span class="title_head_line">FOOD </span>DIARY</h2></strong>
    </div>
     <div class="saperator"></div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9" ng-init="food_diary.getFoodDiary();">
      
      @include('front.food.food-diary')

      <div class="saperator"></div>
      <div class="saperator"></div>

      @include('front.food.food-calorie-summary')
    </div>