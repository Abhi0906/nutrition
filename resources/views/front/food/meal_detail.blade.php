<style type="text/css">
  .meal_detail_modal{
    width: 70% !important;
  }
</style>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 modal-dialog meal_detail_modal" style="float:none">
  
 <div class="panel panel-primary weight_body_panel weight_diary_panel_size goalsummary_tbl1">

        <div class="panel-heading panel_head_background panel_head_log_food">
          <div class="row">
            <div class="col-xs-12">
              <button type="button" class="close" data-dismiss="modal" >
              <span aria-hidden="true" >&times;</span>
              <span class="sr-only" >Close</span>
          </button>
              <h4>
                <strong>@{{add_food.meal_name}}</strong>

              </h4>
            </div>
          </div>
        </div>
        <div class="panel-body panel-contain_div_logfood"> 
          <div class="saperator"></div>
          <div class="food_dairy_block" id="responsive_food_table">             
            <table class="table table-bordered borderless_table margin_bottom_zero" id="simple-example-table">
              <thead> 
                <tr class="toptr-border">
                  <th class="withborder"></th>
                  <th class="calorie-subhead-td withborder">CALORIES</th>
                  <th class="calorie-subhead-td withborder">FAT</th>
                  <th class="calorie-subhead-td withborder">FIBER</th>
                  <th class="calorie-subhead-td withborder">CARBS</th>
                  <th class="calorie-subhead-td withborder">SODIUM</th>
                  <th class="calorie-subhead-td withborder">PROTEIN</th>
                </tr>
              </thead>
              <tbody>
                
                <tr ng-repeat="(key, value) in add_food.meal_food">
                   <td data-title="Food" class="first-td-border">
                    @{{value.name}}
                   </td>
                   <td data-title="CALORIES" class="text-center black_text td-border-none">@{{value.calories}}</td>
                   <td data-title="FAT" class="text-center black_text td-border-none">@{{value.fat}}</td>
                   <td data-title="FIBER" class="text-center black_text td-border-none">@{{value.fiber}}</td>
                   <td data-title="CARBS" class="text-center black_text td-border-none">@{{value.carb}}</td>
                   <td data-title="SODIUM" class="text-center black_text td-border-none">@{{value.sodium}}</td>
                   <td data-title="PROTEIN" class="text-center black_text td-border-none">@{{value.protein}}</td> 
                </tr>

                <tr class="total_tr">
                    <td  class="first-td-border first_td">TOTAL</td>
                    <td  data-title="CALORIES" class="text-center  td-border-none">@{{add_food.total_calories || 0}}</td>
                    <td  data-title="FAT" class="text-center  td-border-none">@{{add_food.total_fat || 0}}</td>
                    <td  data-title="FIBER" class="text-center  td-border-none">@{{add_food.total_fiber || 0}}</td>
                    <td  data-title="CARBS" class="text-center  td-border-none">@{{add_food.total_carb || 0}}</td>
                    <td  data-title="SODIUM" class="text-center  td-border-none">@{{add_food.total_sodium || 0}}</td>
                    <td  data-title="PROTEIN" class="text-center  td-border-none">@{{add_food.total_protein || 0}}</td> 
                </tr>
                </tbody>
            </table>
          </div>

        </div>
  </div>
</div>