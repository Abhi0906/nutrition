<td>
<p class="margin_bot">
  <a ng-show="$last" href="javascript:void(0);" ng-click="add_food.removeField(key1, key2, count)">
   <i class="fa fa-times-circle facolor"></i>&nbsp;
 </a>
</p>
</td>
<td data-title="FOOD NAME" class="width_25">
<select class="form-control no-radius" 
        ng-options="item.id as item.nutritions.name for item in recommended_food.recommended_foods track by item.id" 
        ng-model="add_food.recommended_food[key1][key2].food"
        ng-change="add_food.selectServingSize(add_food.recommended_food[key1][key2].food, key1, key2);"
        >
  <option value="">SELECT FOOD</option>
</select>
</td>
<td data-title="SERVING SIZE" class="width_20">
  <select ng-options="item.label as item.label for item in add_food.recommended_food[key1][key2].serving_size_obj" 
          ng-model="add_food.recommended_food[key1][key2].serving_string" 
          ng-change="add_food.calculateRecomMealServingSize(key1, key2, add_food.recommended_food[key1][key2].fooddata, 'recommended_food');"
          class="form-control no-radius"
          >
    <option value="">SELECT SERVING SIZE</option>
  </select>
</td>
<td data-title="NO. OF SERVINGS" class="width_17">
      


    <select class="form-control no-radius"

            ng-model="add_food.recommended_food[key1][key2].no_of_servings"
            ng-options="range for range in add_food.serving_ranges"
            ng-change="add_food.calculateRecomMealServingSize(key1, key2, add_food.recommended_food[key1][key2].fooddata, 'recommended_food');">

    </select>
</td>
<td  data-title="CALORIES" class="width_13">
<p ng-bind="add_food.recommended_food[key1][key2].calories">
</p>
</td>
<td data-title="LOG DETAILS" class="td_fixed_width">
<a href="javascript:void(0);" ng-click="add_food.viewFoodDetail(add_food.recommended_food[key1][key2].fooddata, 'recommended_food',add_food.recommended_food[key1][key2].no_of_servings)" class="text-red_food">View Details</a>
</td>