<div class="tbl_tab_food_log md_recmeal_tbllog">
  <form name="recommended_food_form">
  <table class="table table-bordered tbl_log_food text-center">
    <thead>
      <tr>
        <th>
          <!-- <h4>&nbsp;</h4> -->
          <p class="margin_bot" style="visibility: hidden;">
                  <a href="javascript:void(0);">
                   <i class="fa fa-times-circle facolor"></i>&nbsp;
                 </a>
          </p>
        </th>
        <th class="width_25"><h4>FOOD NAME</h4></th>
        <th class="width_20"><h4>SERVING SIZE</h4></th>
        <th class="width_17"><h4>NO. OF SERVINGS</h4></th>
        <th class="width_13"><h4>CALORIES</h4></th>        
        <th class="td_fixed_width"><h4>FOOD DETAILS</h4></th>
      </tr>
    </thead>
    <tbody>
    <tr ng-repeat="(key1, recommended_food) in add_food.recommended_foods">
      <td colspan="6" class="no_padding">
        <table class="table inner_tableinTable">
          <tbody>
            <tr class="top_head_subtitle">
              <td class="text-left" colspan="6">
                <div class="saperator"></div>
                <p class="theme_text_p_lg_md margin_bot">
                  @{{recommended_food.name}}&nbsp;&nbsp;

                  <span class="small" ng-if="recommended_food.recommended_foods.length!=0">
                    <i class="fa fa-plus-circle facolor"></i>
                    <a href="javascript:void(0)" class="add_food" 

                        ng-click="add_food.addField(key1)"> Add Food </a>
                  </span>
                </p>
              </td>
            </tr>
            <tr ng-if="key1==0 && recommended_food.recommended_foods.length!=0" ng-repeat="(key2, count) in add_food.prote_object">
              @include("front.food.log-food.recommended_food_row_data")
            </tr>
            <tr ng-if="key1==1 && recommended_food.recommended_foods.length!=0" ng-repeat="(key2, count) in add_food.carb_object">
              @include("front.food.log-food.recommended_food_row_data")
            </tr>
            <tr ng-if="key1==2 && recommended_food.recommended_foods.length!=0" ng-repeat="(key2, count) in add_food.veg_object">
              @include("front.food.log-food.recommended_food_row_data")
            </tr>
            <tr ng-if="key1==3 && recommended_food.recommended_foods.length!=0" ng-repeat="(key2, count) in add_food.fat_object">
              @include("front.food.log-food.recommended_food_row_data")
            </tr>
            <tr ng-if="recommended_food.recommended_foods.length==0">
              <td align="left" class="black_text"> Food is not recommended yet.</td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    </tbody>
  </table> 
  <br>                   
  <div ng-show="add_food.recommendedFood_loader"  class="search_food_loader"></div>
  <br>
  <div class="row">
    <div class="col-sm-12 text-side">
      <div class="form-inline margin_10">
        <div class="form-group">
          <span ng-if="add_food.log_recommended_food_err" class="red-text">Select at least one food &nbsp;&nbsp;</span>
          <label for="email">Meal Type</label>
          <select class="form-control no-radius"
               ng-init="add_food.recommended_food.schedule_time = add_food.food_type" 
               ng-model="add_food.recommended_food.schedule_time" 
                 ng-options="schedule_time.id as schedule_time.name for schedule_time in add_food.schedule_times"
                required >
         </select>
        </div>
        <div class="form-group hidden-xs">&nbsp;&nbsp;&nbsp;</div>

        <div class="form-group">
         <button class="btn btn-primary btndesign no_margin btn-lg" 
                  ng-disabled="recommended_food_form.$invalid" 
                  ng-click="add_food.logRecommendedFoods()">Log Foods</button>
         </div>
      </div>
    </div>
  </div>
  </form>
</div>
