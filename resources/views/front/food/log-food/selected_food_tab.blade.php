<div class="tbl_tab_food_log">
  <table class="table table-bordered tbl_log_food text-center">
    <thead>
      <tr>
        <th><h4>FOOD NAME</h4></th>
        <th><h4>CALORIES</h4></th>
        <th><h4>Serving Size</h4></th>
        <th><h4>No. of Servings</h4></th>
        <th><h4>Meal Type</h4></th>
        <th class="td_fixed_width"><h4>FOOD LOG</h4></th>
      </tr>
    </thead>
    <tbody>
      <tr>
          <td class="text-left">
          <p class="theme_text_p_lg_md">@{{add_food.selectedfood.foodname}}</p>
          <p class="tag_text_normal">@{{add_food.selectedfood.brand_name}}, 
            @{{add_food.selectedfood.serving_quantity}} @{{add_food.selectedfood.serving_size_unit}}</p>
        </td>
        <td>
            <p ng-if="add_food.selected_from =='custom'" class="food_log_normalbold">
              
               @{{add_food.selectedfood.calories * add_food.selected_log.no_of_servings}}
            </p>
            <p ng-if="add_food.selected_from!='custom'" class="food_log_normalbold"
               ng-bind="add_food.selected_log.calories" 
               ng-init="add_food.selected_log.calories = add_food.selectedfood.calories">
            </p>
        </td>
        <td>
           <p ng-if="add_food.selected_from=='custom'" class="food_log_normalbold">@{{add_food.selectedfood.serving_quantity}}</p>  
           <!-- Or -->
           <select ng-if="add_food.selected_from=='favourite'" class="select_box"
                   ng-init="add_food.selected_log.serving_string = add_food.selectedfood.serving_data[0]" 
                   ng-model="add_food.selected_log.serving_string" 
                   ng-options="serving.label for serving in add_food.selectedfood.serving_data"
                   ng-change="add_food.calculateServingSize($index,add_food.selectedfood,'selected_favorite');">
            </select> 
           <!-- Or -->

            <select ng-if="add_food.selected_from=='frequent' 
                                    || add_food.selected_from=='recent' 
                                    || add_food.selected_from=='proteins_foods'
                                    || add_food.selected_from=='carb_foods'
                                    || add_food.selected_from=='vegetable_foods'
                                    || add_food.selected_from=='healthy_fats_food' " 
                    class="select_box"
                   ng-init="add_food.selected_log.serving_string = add_food.selectedfood.serving_string" 
                   ng-model="add_food.selected_log.serving_string" 
                   ng-options="serving.label as serving.label for serving in add_food.selectedfood.serving_data"
                   ng-change="add_food.calculateServingSize($index,add_food.selectedfood,'selected_rec_freq');">
            </select>                            
       
        </td>
        <td>


            <select class="select_box"
                    ng-if="add_food.selected_from=='favourite'"
                    ng-init="add_food.selected_log.no_of_servings = add_food.selectedfood.no_of_servings"
                    ng-model="add_food.selected_log.no_of_servings"
                    ng-options="range for range in add_food.serving_ranges"
                    ng-change="add_food.calculateServingSize($index,add_food.selectedfood,'selected_favorite');">

            </select>
              <!-- Or -->


            <select class="select_box"
                    ng-if="add_food.selected_from=='frequent'
                          || add_food.selected_from=='recent'
                          || add_food.selected_from=='proteins_foods'
                          || add_food.selected_from=='carb_foods'
                          || add_food.selected_from=='vegetable_foods'
                          || add_food.selected_from=='healthy_fats_food' "
                    ng-init="add_food.selected_log.no_of_servings = add_food.selectedfood.no_of_servings"
                    ng-model="add_food.selected_log.no_of_servings"
                    ng-options="range for range in add_food.serving_ranges"
                    ng-change="add_food.calculateServingSize($index,add_food.selectedfood,'selected_rec_freq');">

            </select>
              <!-- Or -->


            <select class="select_box"
                    ng-if="add_food.selected_from=='custom'"
                    ng-init="add_food.selected_log.no_of_servings = add_food.selectedfood.no_of_servings"
                    ng-model="add_food.selected_log.no_of_servings"
                    ng-options="range for range in add_food.serving_ranges">

            </select>

        </td>
        <td>
                <select ng-if="add_food.selected_from=='favourite' || add_food.selected_from=='custom' " class="select_box"
                       ng-init="add_food.selected_log.schedule_time = add_food.schedule_times[0].id" 
                       ng-model="add_food.selected_log.schedule_time" 
                       ng-options="schedule_time.id as schedule_time.name for schedule_time in add_food.schedule_times"
                       >
               </select>
                <!-- Or -->
               <select ng-if="add_food.selected_from=='frequent' 
                              || add_food.selected_from=='recent' 
                              || add_food.selected_from=='proteins_foods'
                              || add_food.selected_from=='carb_foods'
                              || add_food.selected_from=='vegetable_foods'
                              || add_food.selected_from=='healthy_fats_food'" class="select_box"
                       ng-init="add_food.selected_log.schedule_time = add_food.selectedfood.schedule_time" 
                       ng-model="add_food.selected_log.schedule_time" 
                       ng-options="schedule_time.name as schedule_time.name for schedule_time in add_food.schedule_times"
                       >
               </select>
            </td>
        <td>
          <button class="btn btn-primary btndesign " ng-click="add_food.logFood(add_food.selectedfood, $index, 'selected')">Log Food</button>
          <br>
          <a href="javascript:void(0);" ng-click="add_food.viewFoodDetail(add_food.selectedfood, 'selected',add_food.selected_log.no_of_servings)" class="text-red_food">View Details</a>
        </td>
      </tr>
    </tbody>
  </table>
</div>