<div class="tbl_tab_food_log">
  <table class="table table-bordered tbl_log_food text-center">
    <thead>
      <tr>
        <th class="width_25"><h4>FOOD NAME</h4></th>
        <th><h4>CALORIES</h4></th>
        <th><h4>SERVING SIZE</h4></th>
        <th><h4>NO. OF SERVINGS</h4></th>
        <th><h4>MEAL TYPE</h4></th>
        <th><h4>FOOD LOG</h4></th>
      </tr>
    </thead>
    <tbody>
      <tr ng-repeat="custFoodData in add_food.custom_nutritions">
        <td class="text-left" data-title="FOOD NAME">
          <p class="theme_text_p_lg_md content_fixed_width">
            @{{custFoodData.name}}
            <span ng-if="custFoodData.is_favourite==1" title="Remove from favorite">
              <a href="javascript:void(0)" ng-click="add_food.removeFromFavourite(custFoodData, 'custom')">
                <i class="fa fa-star favourite-star" aria-hidden="true"></i>
              </a>
            </span>
            <span ng-if="custFoodData.is_favourite==0" title="Add to favorite">
              <a href="javascript:void(0)" ng-click="add_food.addToFavourite(custFoodData, 'custom')">
                <i class="fa fa-star notfavourite-star" aria-hidden="true"></i>
              </a>
            </span>
          </p>
          <p class="tag_text_normal">@{{custFoodData.brand_name}}, 
            @{{custFoodData.nutrition_data.serving_quantity}} @{{custFoodData.nutrition_data.serving_size_unit}}</p>
        </td>
        <td data-title="CALORIES">
            <p>
              
               @{{custFoodData.nutrition_data.calories * add_food.custom_log[$index].no_of_servings}}
            </p>

        </td>
        <td data-title="SERVING SIZE">
           <p>@{{custFoodData.nutrition_data.serving_quantity}}</p>                               
       
        </td>
        <td data-title="NO. OF SERVINGS">

          <select class="form-control no-radius"
                  ng-init="add_food.custom_log[$index].no_of_servings = add_food.default_serving"
                  ng-model="add_food.custom_log[$index].no_of_servings"
                  ng-options="range for range in add_food.serving_ranges"
                  >

          </select>
        </td>
        <td data-title="MEAL TYPE">
                <select class="form-control no-radius"
                       ng-init="add_food.custom_log[$index].schedule_time = add_food.food_type" 
                       ng-model="add_food.custom_log[$index].schedule_time" 
                       ng-options="schedule_time.id as schedule_time.name for schedule_time in add_food.schedule_times"
                       >
               </select>
        </td>
        <td data-title="LOG FOOD">
          <button class="btn btn-primary btndesign " ng-click="add_food.logFood(custFoodData,$index,'custom')">Log Food</button>
          <br>
          <a href="javascript:void(0);" ng-click="add_food.viewFoodDetail(custFoodData, 'custom',add_food.custom_log[$index].no_of_servings)" class="text-red_food">View Details</a>
        </td>
      </tr>
    </tbody>
  </table>
  <div class="small-separator"></div>
  <div ng-if="add_food.custom_nutritions.length==0" class="text-center">No custom food found</div>
</div>