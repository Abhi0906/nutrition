<div class="tbl_tab_food_log">
  <table class="table table-bordered tbl_log_food text-center">
    <thead>
      <tr>
        <th class="width_25"><h4>FOOD NAME</h4></th>
        <th><h4>CALORIES</h4></th>
        <th><h4>SERVING SIZE</h4></th>
        <th><h4>NO. OF SERVING</h4></th>
        <th><h4>MEAL TYPE</h4></th>
        <th><h4>FOOD LOG</h4></th>
      </tr>
    </thead>
    <tbody>
      <tr ng-repeat="favFoodData in add_food.favourite_nutritions">
        <td class="text-left" data-title="FOOD NAME">
          <p class="theme_text_p_lg_md content_fixed_width">@{{favFoodData.nutritions.name}}
            <span title="Remove from favorite">
              <a href="javascript:void(0)" ng-click="add_food.removeFromFavourite(favFoodData, 'favourite')">
                <i class="fa fa-star favourite-star" aria-hidden="true"></i>
              </a>
            </span>
          </p>
          <p class="tag_text_normal">@{{favFoodData.nutritions.brand_name}}, 
            @{{favFoodData.nutritions.nutrition_data.serving_quantity}} @{{favFoodData.nutritions.nutrition_data.serving_size_unit}}</p>
        </td>
        <td data-title="CALORIES">
          <p ng-bind="add_food.fav_log[$index].calories" 
           ng-init="add_food.fav_log[$index].calories = (favFoodData.nutritions.nutrition_data.calories * add_food.default_serving)">
          </p>
        </td>
        <td data-title="SERVING SIZE">
          <select class="form-control no-radius"
                   ng-init="add_food.fav_log[$index].serving_string = favFoodData.nutritions.serving_data[0]" 
                   ng-model="add_food.fav_log[$index].serving_string" 
                   ng-options="serving.label for serving in favFoodData.nutritions.serving_data"
                   ng-change="add_food.calculateServingSize($index,favFoodData,'favorite');">
            </select>
        </td>
        <td data-title="NO. OF SERVINGS">

          <select class="form-control no-radius"
                  ng-init="add_food.fav_log[$index].no_of_servings = add_food.default_serving"
                  ng-model="add_food.fav_log[$index].no_of_servings"
                  ng-options="range for range in add_food.serving_ranges"
                  ng-change="add_food.calculateServingSize($index,favFoodData,'favorite');">

          </select>
          
        </td>
        <td data-title="MEAL TYPE">
            <select class="form-control no-radius"
                   ng-init="add_food.fav_log[$index].schedule_time = add_food.food_type" 
                   ng-model="add_food.fav_log[$index].schedule_time" 
                   ng-options="schedule_time.id as schedule_time.name for schedule_time in add_food.schedule_times"
                   >
           </select>
        </td>
        <td data-title="LOG FOOD">
          <button class="btn btn-primary btndesign" ng-click="add_food.logFood(favFoodData,$index,'favourite')">Log Food</button>
          <br>
          <a href="javascript:void(0);" ng-click="add_food.viewFoodDetail(favFoodData, 'favourite',add_food.fav_log[$index].no_of_servings)" class="text-red_food">View Details</a>
        </td>
      </tr>
    </tbody>
  </table>
  <div class="small-separator"></div>
  <div ng-if="add_food.favourite_nutritions.length==0" class="text-center">No favourite food found</div>
</div>