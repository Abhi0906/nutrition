<div class="tbl_tab_food_log">
  <table class="table table-bordered tbl_log_food text-center">
    <thead>
      <tr>
        <th class="width_25"><h4>FOOD NAME</h4></th>
        <th><h4>CALORIES</h4></th>
        <th><h4>SERVING SIZE</h4></th>
        <th><h4>NO. OF SERVING</h4></th>
        <th><h4>MEAL TYPE</h4></th>
        <th><h4>FOOD LOG</h4></th>
      </tr>
    </thead>
    <tbody>
      <tr ng-repeat="freqFoodData in add_food.frequent_nutritions">
        <td class="text-left" data-title="FOOD NAME">
          <p class="theme_text_p_lg_md content_fixed_width">
            @{{freqFoodData.nutritions.name}}
            <span ng-if="freqFoodData.is_favourite==1" title="Remove from favorite">
              <a href="javascript:void(0)" ng-click="add_food.removeFromFavourite(freqFoodData, 'frequent')">
                <i class="fa fa-star favourite-star" aria-hidden="true"></i>
              </a>
            </span>
            <span ng-if="freqFoodData.is_favourite==0" title="Add to favorite">
              <a href="javascript:void(0)" ng-click="add_food.addToFavourite(freqFoodData, 'frequent')">
                <i class="fa fa-star notfavourite-star" aria-hidden="true"></i>
              </a>
            </span>
          </p>
          <p class="tag_text_normal">@{{freqFoodData.nutritions.brand_name}}, 
            @{{freqFoodData.serving_quantity}} @{{freqFoodData.nutritions.nutrition_data.serving_size_unit}}</p>
        </td>
        <td data-title="CALORIES">
            <p ng-bind="add_food.freq_log[$index].calories" 
               ng-init="add_food.freq_log[$index].calories = freqFoodData.calories">
            </p>
        </td>
        <td data-title="SERVING SIZE">
           <select class="form-control no-radius"
                   ng-init="add_food.freq_log[$index].serving_string = freqFoodData.serving_string" 
                   ng-model="add_food.freq_log[$index].serving_string" 
                   ng-options="serving.label as serving.label for serving in freqFoodData.nutritions.serving_data"
                   ng-change="add_food.calculateServingSize($index,freqFoodData,'ferquent');">
           </select>                                

        </td>
        <td data-title="NO. OF SERVINGS">

          <select class="form-control no-radius"
                  ng-init="add_food.freq_log[$index].no_of_servings = freqFoodData.no_of_servings"
                  ng-model="add_food.freq_log[$index].no_of_servings"
                  ng-options="range for range in add_food.serving_ranges"
                  ng-change="add_food.calculateServingSize($index,freqFoodData,'ferquent');">

          </select>
        </td>
        <td data-title="MEAL TYPE">
            <select class="form-control no-radius"
                   ng-init="add_food.freq_log[$index].schedule_time = freqFoodData.schedule_time" 
                   ng-model="add_food.freq_log[$index].schedule_time" 
                   ng-options="schedule_time.id as schedule_time.name for schedule_time in add_food.schedule_times"
                   >
           </select>
        </td>
        <td data-title="LOG FOOD">
          <button class="btn btn-primary btndesign " ng-click="add_food.logFood(freqFoodData,$index,'frequent')">Log Food</button>
          <br>
          <a href="javascript:void(0);" ng-click="add_food.viewFoodDetail(freqFoodData, 'frequent',add_food.freq_log[$index].no_of_servings)" class="text-red_food">View Details</a>
        </td>
      </tr>
    </tbody>
  </table>
  <div class="small-separator"></div>
  <div ng-if="add_food.frequent_nutritions.length==0" class="text-center">
    No frequent food found
  </div>
</div>