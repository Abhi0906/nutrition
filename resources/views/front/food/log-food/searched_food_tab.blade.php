<div class="tbl_tab_food_log">
  <table ng-show="add_food.search_result" class="table table-bordered tbl_log_food text-center">
    <thead>
      <tr>
        <th><h4>FOOD NAME</h4></th>
        <th><h4>CALORIES</h4></th>
        <th><h4>Serving Size</h4></th>
        <th><h4>No. of Servings</h4></th>
        <th><h4>Meal Type</h4></th>
        <th class="td_fixed_width"><h4>FOOD LOG</h4></th>
      </tr>
    </thead>
    <tbody>
      <tr ng-repeat="fooddata in add_food.foodSearchdata">
          <td class="text-left" data-title="FOOD NAME">
              <p class="theme_text_p_lg_md content_fixed_width">
                  @{{fooddata.name}}
                  <span ng-if="fooddata.is_favourite==1" title="Remove from favourite">
                    <a href="javascript:void(0)" ng-click="add_food.removeFromFavourite(fooddata, 'search')">
                      <i class="fa fa-star favourite-star" aria-hidden="true"></i>
                    </a>
                  </span>
                  <span ng-if="fooddata.is_favourite==0" title="Add to favorite">
                    <a href="javascript:void(0)" ng-click="add_food.addToFavourite(fooddata, 'search')">
                      <i class="fa fa-star notfavourite-star" aria-hidden="true"></i>
                    </a>
                  </span>
              </p>
              <p class="tag_text_normal">@{{fooddata.brand_name}}, 
                @{{fooddata.nutrition_data.serving_quantity}} @{{fooddata.nutrition_data.serving_size_unit}}
              </p>
          </td>
          <td data-title="CALORIES">
          <p class="food_log_normalbold"
           ng-bind="add_food.search_log[$index].calories" 
           ng-init="add_food.search_log[$index].calories =(fooddata.nutrition_data.calories * add_food.default_serving)"
           ></p></td>
          <td data-title="SERVING SIZE">
            <select class="select_box"
             ng-init="add_food.search_log[$index].serving_string = fooddata.serving_data[0]" 
             ng-model="add_food.search_log[$index].serving_string" 
             ng-options="serving.label for serving in fooddata.serving_data"
             ng-change="add_food.calculateServingSize($index,fooddata,'search');">

            </select>
          </td>
          <td data-title="No. of Servings">
              <select class="select_box"
                      ng-init="add_food.search_log[$index].no_of_servings = add_food.default_serving"
                      ng-model="add_food.search_log[$index].no_of_servings"
                      ng-options="range for range in add_food.serving_ranges"
                      ng-change="add_food.calculateServingSize($index,fooddata,'search');">

              </select>
          </td>
          <td data-title="Meal Type">
              <select class="select_box"
                     ng-init="add_food.search_log[$index].schedule_time = add_food.food_type" 
                     ng-model="add_food.search_log[$index].schedule_time" 
                     ng-options="schedule_time.id as schedule_time.name for schedule_time in add_food.schedule_times"
                     >
             </select>
          </td>
          <td data-title="FOOD LOG">
            <button class="btn btn-primary btndesign" ng-click="add_food.logFood(fooddata,$index,'search')">Log Food</button>
            <br>
            <a href="javascript:void(0);" ng-click="add_food.viewFoodDetail(fooddata, 'search',add_food.search_log[$index].no_of_servings)" class="text-red_food">View Details</a>
          </td>
      </tr>
    </tbody>
  </table>
  <div class="small-separator"></div>
  <div ng-if="add_food.foodSearchdata.length==0" class="text-center">No data found</div>
  <div ng-show="add_food.search_loader"  class="search_food_loader"></div>
</div>