<div class="tbl_tab_food_log">
  <table class="table table-bordered tbl_log_food text-center">
    <thead>
      <tr>
        <th><h4>MEAL NAME</h4></th>
        <th><h4>TOTAL CALORIES</h4></th>
        <th><h4>MEAL TYPE</h4></th>
        <th class="td_fixed_width"><h4>MEAL LOG</h4></th>
      </tr>
    </thead>
    <tbody>
      <tr ng-repeat="mealFoodData in add_food.meal_foods">
        <td class="text-left" data-title="MEAL NAME">
          <p class="theme_text_p_lg_md">@{{mealFoodData.meal_name}}</p>
        </td>
        <td data-title="TOTAL CALORIES">
            <p> @{{mealFoodData.total_calories}} </p>
        </td>
     

        <td data-title="MEAL TYPE">
               <select class="form-control no-radius"
                       ng-init="add_food.meal_log[$index].schedule_time = add_food.food_type" 
                       ng-model="add_food.meal_log[$index].schedule_time" 
                       ng-options="schedule_time.id as schedule_time.name for schedule_time in add_food.schedule_times"
                       >
               </select>
            </td>
        <td data-title="FOOD LOG">
          <button class="btn btn-primary btndesign " ng-click="add_food.logMeal(mealFoodData, $index)">Log Meal</button>
          <br>
          <a href="javascript:void(0);" ng-click="add_food.mealDetail(mealFoodData)" class="text-red_food">View Details</a>
        </td>
      </tr>
    </tbody>
  </table>
</div>