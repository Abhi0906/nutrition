<div class="row padding_10">

  <div class="col-sm-8">
    <div class="form-horizontal">
          <div class="form-group margin_bot">
            <label class="control-label col-sm-4 cust-label-meal">Select Meal</label>
            <div class="col-sm-8">
               

                <!-- Meals -->
                <select class="customselect_meal" custom-select
                        ng-model="add_food.recommended_meal_id"
                        ng-options="recommended_meal.id as (recommended_meal.meal_name_display) for recommended_meal in add_food.recommended_meals"
                        ng-change="add_food.getRecommendedMealFoods()">
                </select>
                <!-- end: Meals -->  
                  
            </div>
          </div>
        </div>  
  </div>
  <div class="saperator visible-xs"></div>
  <div class="col-sm-4 text-side">
    <ul class="list-inline margin_bot">
      <li>
        <p class="theme_text_p_lg_md">Total Calories:</p>
      </li>
      <li>
        <p class="theme_text_p_lg_md">@{{add_food.total_meal_food_calorie | number: 2}}</p>
      </li>
    </ul> 
  </div>
</div>
<div class="saperator visible-xs"></div>
<hr class="hr_line hidden-xs"> 
<div class="tbl_tab_food_log md_recmeal_tbllog">
  <div ng-show="add_food.recommendedMeal_loader"  class="search_food_loader"></div>
  <form name="LogRecomMealForm">
    <div class="panel panel-default">
        
        <div class="panel-body no_padding">
          <div id="md_rec_table1" class="tbl_noborder_wrap">
            <table class="table table-bordered tbl_log_food text-center">
              <thead>
                <tr>
                  <th>
                    &nbsp;
                    <!-- <div class="checkbox checkbox-primary" style="visibility: hidden;">
                      <input type="checkbox" value="" checked="true"> <label></label>
                    </div> -->
                  </th>
                  <th class="width_25"><h4>FOOD NAME</h4></th>
                  <th class="width_20"><h4>SERVING SIZE</h4></th>
                  <th class="width_17"><h4>NO. OF SERVINGS</h4></th>
                  <th class="width_13"><h4>CALORIES</h4></th>        
                  <th class="td_fixed_width"><h4>FOOD DETAILS</h4></th>
                </tr>
              </thead>
              <tbody >
                <tr ng-repeat="(key1, recommended_meal_foods) in add_food.recommended_meal_foods_types" 
                    >
                  <td colspan="6" class="no_padding">
                    <table class="table inner_tableinTable">
                      <tbody>
                        <tr class="top_head_subtitle">
                          <td class="text-left" colspan="6">
                            <div class="saperator"></div>
                            <p class="theme_text_p_lg_md margin_bot">
                              @{{recommended_meal_foods.item_name}}
                            </p>
                          </td>
                        </tr>
                        <tr ng-repeat="(key2, recommended_meal_food) in recommended_meal_foods.item_foods">
                          <td>
                            <div class="checkbox checkbox-primary">
                             
                             <input type="checkbox" 
                                      ng-model="add_food.recommended_meal_food[key1][key2].checkbox"
                                      ng-init="add_food.recommended_meal_food[key1][key2].checkbox=true"
                                      ng-click="add_food.toggleMealFoodSelection(recommended_meal_food, key1, key2, recommended_meal_food.custom_index, add_food.recommended_meal_food[key1][key2].checkbox);"> <label></label>
                            </div>
                          </td>
                          <td data-title="FOOD NAME" class="width_25">
                            <p>@{{recommended_meal_food.nutritions.name}}</p>
                          </td>
                          <td data-title="SERVING SIZE" class="width_20">
                              <select ng-options="item.label as item.label for item in recommended_meal_food.nutritions.serving_data" 
                                      ng-model="add_food.recommended_meal_food[key1][key2].serving_string" 
                                      ng-init="add_food.recommended_meal_food[key1][key2].serving_string = recommended_meal_food.serving_string"
                                      ng-change="add_food.calculateRecomMealServingSize(key1, key2, recommended_meal_food, 'recommended_meal');
                                      add_food.toggleMealFoodSelection(recommended_meal_food, key1, key2, recommended_meal_food.custom_index, add_food.recommended_meal_food[key1][key2].checkbox);"
                                      class="form-control no-radius"
                                      required>
                              </select>
                          </td>
                          <td data-title="NO. OF SERVINGS" class="width_17">
                            <input type="text" class="form-control no-radius"
                                    ng-model="add_food.recommended_meal_food[key1][key2].no_of_servings"
                                    ng-init="add_food.recommended_meal_food[key1][key2].no_of_servings = recommended_meal_food.no_of_servings"
                                    ng-change="add_food.calculateRecomMealServingSize(key1, key2, recommended_meal_food, 'recommended_meal');
                                    add_food.toggleMealFoodSelection(recommended_meal_food, key1, key2, recommended_meal_food.custom_index, add_food.recommended_meal_food[key1][key2].checkbox);"
                                  >
                          </td>
                          <td  data-title="CALORIES" class="width_13">
                            <p 
                              ng-bind="add_food.recommended_meal_food[key1][key2].calories"
                              ng-init="add_food.recommended_meal_food[key1][key2].calories=recommended_meal_food.calories"
                            >
                            </p>
                          </td>
                          <td data-title="LOG DETAILS" class="td_fixed_width">
                            <a href="javascript:void(0);" ng-click="add_food.viewFoodDetail(recommended_meal_food, 'recommended_meal_food')" class="text-red_food">View Details</a>
                          </td>
                        </tr> 
                        <tr ng-if="recommended_meal_foods.item_foods.length==0">
                          <td align="left" colspan="6" class="black_text">Food is not recommended yet.</td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table> 
          </div>
        </div>
    </div>

    <div class="panel panel-default">
       
      <div class="panel-body no_padding">
        <div id="md_rec_table4" class="tbl_noborder_wrap">
        <table class="table table-bordered tbl_log_food borderless_table margin_bottom_zero" id="simple-example-table">
          <thead> 
            <tr class="toptr-border">
              <th class="withborder">&nbsp;</th>
              <th class="withborder"><h4>CALORIES</h4></th>
              <th class="withborder"><h4>FAT</h4></th>
              <th class="withborder"><h4>FIBER</h4></th>
              <th class="withborder"><h4>CARBS</h4></th>
              <th class="withborder"><h4>SODIUM</h4></th>
              <th class="withborder"><h4>PROTEIN</h4></th>
            </tr>
          </thead>
          <tbody>
            <tr>
               <td  class="first_td">TOTAL</td>
               <td  data-title="CALORIES" class="text-center">@{{add_food.total_meal_food_calorie | number: 2}}&nbsp;</td>
               <td  data-title="FAT" class="text-center">@{{add_food.total_meal_food_fat | number: 2}}&nbsp;</td>
               <td  data-title="FIBER" class="text-center">@{{add_food.total_meal_food_fibar | number: 2}}&nbsp;</td>
               <td  data-title="CARBS" class="text-center">@{{add_food.total_meal_food_carbs | number: 2}}&nbsp;</td>
               <td  data-title="SODIUM" class="text-center">@{{add_food.total_meal_food_sodium | number: 2}}&nbsp;</td>
               <td  data-title="PROTEIN" class="text-center">@{{add_food.total_meal_food_protein | number: 2}}&nbsp;</td> 
            </tr>
          </tbody>
          </table>
          </div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-sm-12 text-side">
        <div class="form-inline margin_10">
          <div class="form-group">
            <label for="email">Meal Type</label>
            
            <select class="form-control no-radius"
                 ng-init="add_food.recommended_meal.schedule_time = add_food.food_type" 
                 ng-model="add_food.recommended_meal.schedule_time" 
                   ng-options="schedule_time.id as schedule_time.name for schedule_time in add_food.schedule_times"
                  required >
           </select>
          </div>
          <div class="form-group hidden-xs">&nbsp;&nbsp;&nbsp;</div>
          <div class="form-group">
            <label for="pwd">Meal Time</label>
             <select class="form-control no-radius">
                  <option>1.00 am</option>
                  <option>2.00 am</option>
                  <option>3.00 am</option>
                  <option>4.00 am</option>
              </select>
          </div>
          <div class="form-group hidden-xs">&nbsp;&nbsp;&nbsp;</div>
          <div class="form-group">
            <button class="btn btn-primary btndesign no_margin btn-lg" ng-disabled="add_food.total_meal_food_calorie==0" ng-click="add_food.logRecommendedMeal()">Log Meal</button>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>

