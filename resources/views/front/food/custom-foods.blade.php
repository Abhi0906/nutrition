
<div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12" ng-init="custom_food.getCustomFoods()">
    <h2 class="text-light-blue text-uppercase head_title_main">
      Cus<span class="title_head_line">tom Fo</span>ods
    </h2>
</div> 
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <a class="btn btn-primary btndesign" ui-sref="diary">Back</a>
    <div class="small-separator"></div>
</div>




<!-- <div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12" ng-init="custom_food.getCustomFoods()">
    
  <strong>
    <h2 class="text-light-blue text-uppercase">
      <a class="pull-left btn btn-primary btndesign" ui-sref="diary">Back</a>

      Cus<span class="title_head_line">tom Fo</span>ods
    </h2>
  </strong>
  <div class="saperator"></div>
</div> -->

<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
  
 <div class="panel panel-primary weight_body_panel weight_diary_panel_size goalsummary_tbl1 inner_diady_pages_panel">
        
        <div class="panel-body panel_Bodypart">
          <div class="logfood_tab_continer">
            <div id="log_food_tab" class="tabs_container">
              <div class="tab-content logfood_tab_details">
                 <div id="fav_log_tab" class="tab-pane fade in active"> 
                    <div class="tbl_tab_food_log">
                      <table ng-show="custom_food.show_custom_foods" class="table table-bordered tbl_log_food text-center">
                        <thead>
                          <tr>
                            <th><h4>FOOD NAME</h4></th>
                            <th><h4>CALORIES</h4></th>
                            <th><h4 class="text-uppercase">Created at</h4></th>
                            <th class="td_fixed_width"><h4>Delete Food</h4></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr ng-repeat="customFoodData in custom_food.custom_nutritions">
                            <td class="text-left">
                              <p class="theme_text_p_lg_md">@{{customFoodData.name}}</p>
                              <p class="tag_text_normal">@{{customFoodData.brand_name}}, 
                                @{{customFoodData.nutrition_data.serving_quantity}} @{{customFoodData.nutrition_data.serving_size_unit}}</p>
                            </td>
                            <td>
                              <p class="food_log_normalbold">
                                @{{customFoodData.nutrition_data.calories}}
                              </p>
                            </td>
                            <td>
                              @{{customFoodData.created_at | custom_date_format : 'MM/dd/yyyy'  }}
                            </td>
                            <td>
                              <button class="btn btn-primary btndesign" ng-click="custom_food.deleteCustomFood(customFoodData)">Delete</button>
                              
                              <!-- <a href="javascript:void(0);" ng-click="add_food.viewFoodDetail(favFoodData)" class="text-red_food">View Details</a> -->
                            </td>
                          </tr>
                          <tr>
                            <td colspan="4"></td>
                          </tr>
                        </tbody>
                      </table>
                      <div ng-show="custom_food.noData" class="text-center">No custom food added yet.</div>
                      <div ng-show="custom_food.custom_food_loader"  class="custom_food_grid_loader"></div>
                    </div>
                 </div>
              </div>
            </div>
          </div>
        </div>
 </div>
</div>