@extends('layout.app')
@section('content')
@include('errors.error') 
    <div ng-controller="ProfileEditController as profile"  ng-init="profile.userId='{{$user_id}}';profile.getDemographics();">
        <div ng-controller="ProfileEditController as name"  ng-init="">
        <form name="profileForm"  novalidate="novalidate" class="animation-fadeInQuick">
                <div class="row">
                <div class="col-md-12 col-lg-6">
                    <!-- Account Form Block -->
                    <simple-block block-title="Account">
                        <!-- Horizontal Form Content -->
                        <div class="form-horizontal form-bordered">
                          
                            <div class="form-group" ng-class="{ 'has-error' : profileForm.username.$invalid && !profileForm.username.$pristine }">
                                {!! HTML::decode(Form::label('txt_username', 'Username<sup class="text-danger"></sup>', array('class'=>'col-md-3 control-label'))) !!}
                                <div class="col-md-9">
                                    <input type="text" ng-model="profile.user.username" name="username" maxlength="50" id="txt_username" placeholder="Username" class="form-control" ng-minlength="3"  required disabled>
                                    <span class="help-block" ng-hide="profileForm.username.$invalid && !profileForm.username.$pristine"></span>
                                    <p ng-show="profileForm.username.$invalid && !profileForm.username.$pristine" class="help-block">Your name is required Min. 3 characters required</p>
                                </div>
                            </div>


                            
                            <div class="form-group">
                                {!! Form::label('pwd_password', 'Password',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-9">
                                    <input type="password" ng-model="profile.user.password" name="password" maxlength="25" id="pwd_password" placeholder="Password" class="form-control">
                                    <span class="help-block">Please enter a new complex password</span>
                                </div>
                            </div>
                            <div class="form-group" ng-class="{ 'has-error' : profileForm.email.$invalid && !profileForm.email.$pristine }">
                                {!! HTML::decode(Form::label('txt_email', 'Email<sup class="text-danger">*</sup>', array('class'=>'col-md-3 control-label'))) !!}
                                <div class="col-md-9">
                                    <input type="email" ng-model="profile.user.email" name="email" maxlength="50" id="txt_email" placeholder="Enter Email" class="form-control" required>
                                    <span class="help-block" ng-hide="profileForm.email.$invalid && !profileForm.email.$pristine"></span>
                                    <p ng-show="profileForm.email.$invalid && !profileForm.email.$pristine" class="help-block">Your email is required in proper format.</p>
                                </div>
                            </div>
                            @if($user_type == 'patient')
                            <div class="form-group">
                                {!! Form::label('txt_subscription', 'News Subscription',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-9">
                                <input type="checkbox" ng-model="profile.user.news_letter" ng-true-value=1 ng-false-value=0>
                                </div>
                            </div>
                            @endif
                            @if($user_type == 'doctor')
                            <div class="form-group">
                                {!! Form::label('select_title', 'Title',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-9">

                                    <select name="doctor_title" ng-model="profile.user.doctor_title" ng-options="title.name as title.name for title in demographics.doctor_titles" id="select_title" class="form-control">
                                        <option value="">Please Status</option>
                                    </select>
                                   
                                </div>
                            </div>
                            @endif
                        </div>
                        <!-- END Horizontal Form Content -->
                    </simple-block>    
                    
                    <!-- END Account Form Block -->

                     <!-- Personal Form Elements Block -->
                     <simple-block block-title="Personal">
                     <!-- Basic Form Elements Content -->
                        <div class="form-horizontal form-bordered">
                                      
                            <div class="form-group" ng-class="{ 'has-error' : profileForm.first_name.$invalid && !profileForm.first_name.$pristine }">
                                {!! HTML::decode(Form::label('txt_first_name', 'FirstName<sup class="text-danger">*</sup>', array('class'=>'col-md-3 control-label'))) !!}
                                <div class="col-md-9">
                                    <input type="text" ng-model="profile.user.first_name" name="first_name" maxlength="40" id="txt_first_name" ng-minlength="3" placeholder="First name" class="form-control" required  > 
                                    <span class="help-block" ng-hide="profileForm.first_name.$invalid && !profileForm.first_name.$pristine"></span>
                                    <p ng-show="profileForm.first_name.$invalid && !profileForm.first_name.$pristine" class="help-block">Your first name is required Min. 3 characters required</p>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('txt_middle_name', 'Middle Name',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-9">
                                    <input type="text" ng-model="profile.user.middle_name" name="middle_name" maxlength="40" id="txt_middle_name" placeholder="Middle name" class="form-control">
                                    
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('txt_last_name', 'Last Name',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-9">
                                    <input type="text" ng-model="profile.user.last_name" name="last_name" maxlength="40" id="txt_last_name" placeholder="Last name" class="form-control">
                                  
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('gender', 'Gender',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-9">
                                    <label for="radio_male" class="radio-inline">
                                        <input type="radio" ng-model="profile.user.gender" value="male" name="gender" checked="checked" id="radio_male">Male
                                    </label>
                                    <label for="radio_female" class="radio-inline">
                                         <input type="radio"ng-model="profile.user.gender" value="female" name="gender" id="radio_female">Female
                                   </label>
                                    
                                </div>
                            </div>
                            
                            <div class="form-group">
                                {!! Form::label('txt_birth_date', 'Birth Date',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-9">                  
                                   <input-datepicker format="MM dd, yyyy" date="profile.user.birth_date"></input-datepicker>
                                   <label class="control-label ng-binding"  for="age"> &nbsp;&nbsp;@{{profile.age }}&nbsp; </label> <span help-block >Years</span>
                                </div>
                            </div>
                            
                            <div class="form-group" ng-class="{ 'has-error' : profileForm.baseline_weight.$invalid && !profileForm.baseline_weight.$pristine }">
                                {!! Form::label('Weight', 'Weight',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-9">
                                 <div class="input-group">
                                    <input type="text" numbers-only="numbers-only" 
                                    ng-model= "profile.user.profile.baseline_weight"
                                    maxlength="5"
                                    ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" 
                                   
                                    placeholder="Enter Weight"  name="baseline_weight" id="baseline_weight" class="form-control" required>
                                     <span class="input-group-btn">
                                        <span class="btn btn-primary">&nbsp;&nbsp;Lbs&nbsp;&nbsp;</span>
                                    </span>
                                    </div>
                                      <small>Enter numbers only</small>
                                </div>
                            </div>
                            <div class="form-group" ng-class="{ 'has-error' : profileForm.height_in_feet.$invalid && !profileForm.height_in_feet.$pristine }">
                                {!! Form::label('Height', 'Height',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-9">
                                 <div class="input-group">
                                    <input type="text" numbers-only="numbers-only" 
                                     ng-model= "profile.user.profile.height_in_feet"
                                     
                                     maxlength="4"
                                     ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" 
                                     name="height_in_feet" placeholder="Enter feet" id="feet_height" class="form-control" required>

                                    <span class="input-group-btn">
                                      <span class="input-group-btn"><span class="btn btn-primary">&nbsp;&nbsp;Feet&nbsp;&nbsp;</span></span>
                                       </span>
                                      <input type="text" numbers-only="numbers-only" 
                                      ng-model= "profile.user.profile.height_in_inch"
                                      maxlength="4"
                                      ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01"
                                      name="height_in_inch" placeholder="Enter inches"  id="inches_height" class="form-control" required>
                                      <span class="input-group-btn"><span class="btn btn-primary">&nbsp;&nbsp;Inches&nbsp;&nbsp;</span></span>
                                  </div>
                                       <!-- <span class="help-block">Enter numbers only </span>
                                       <span class="help-block">Enter numbers only </span> -->
                                     
                                </div> 
                                <div class="row">
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-5 col-sm-5 col-xs-5">
                                               <p style="margin-left:5%;">Enter numbers only </p>
                                            </div>
                                            <div class="col-md-7 col-sm-7 col-xs-5">
                                               <p style="margin-left:10%;">Enter numbers only</p>
                                            </div>
                                        </div>
                                     </div> 
                                  </div>
                                </div>

                            <!--   <div class="form-group">
                                {!! Form::label('select_status', 'Status',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-9">
                                    <select name="status" ng-model="profile.user.status" ng-options="status.id as status.name for status in demographics.users_status" id="select_status" class="form-control">
                                        <option value="">Please Status</option>
                                    </select>
                                </div>
                            </div> -->
                          
                          
                        </div>
                        <!-- END Basic Form Elements Content -->
                     </simple-block>
                     
                    <!-- END Personal Form Elements Block -->
                </div>
                <div class="col-md-12 col-lg-6">
                    <simple-block block-title="Photo">            
                        <!-- Dropzone Content -->
                        <!-- Dropzone.js, You can check out https://github.com/enyo/dropzone/wiki for usage examples -->
                        <div class="dropzone" file="profile.user.photo" upload-dropzone >
                             <div class="dz-default dz-message"><span>Drop file here to upload</span></div>                     
                        </div>
                        <!-- END Dropzone Content -->
                    </simple-block>

                    <simple-block block-title="Contact">
                    <!-- Horizontal Form Content -->
                        <div class="form-horizontal form-bordered">
                            <div class="form-group">
                                {!! Form::label('txt_phone', 'Phone',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-9">
                                    <input type="text" ng-model ="profile.user.phone" name="phone" maxlength="15" id="txt_phone" placeholder="Enter Phone" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('tex_address', 'Address',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-9">
                                    <input type="text" ng-model="profile.user.address" name="address" maxlength="100" id="tex_address" placeholder="Enter Address" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('txt_city', 'City',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-9">
                                    <input type="text" ng-model= "profile.user.city" name="city" maxlength="30" id="txt_city" placeholder="Enter City" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('txt_state', 'State',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-9">
                                    <input type="text" ng-model= "profile.user.state" name="state" maxlength="30" id="txt_state" placeholder="Enter State" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('txt_country', 'Country',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-9">
                                    <input type="text" ng-model= "profile.user.country" name="country" maxlength="30" id="txt_country" placeholder="Enter Country" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('txt_postal_code', 'Postal Code',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-9">
                                    <input type="text" ng-model= "profile.user.post_code" name="post_code" maxlength="15" id="txt_postal_code" placeholder="Enter Postal Code" class="form-control">
                                </div>
                            </div>
                          
                        </div>
                        <!-- END Horizontal Form Content -->
                    </simple-block>
                    <!-- END Contact Form Block -->
                </div>
                </div>
             <div class ="row">
                <div class="col-md-12">
                    <div class="form-horizontal form-bordered">
                        <div class="form-group form-actions">
                            <div class="col-md-12 col-md-offset-5">
                                <button class="btn btn-sm btn-primary" type="button" ng-disabled="profileForm.$invalid" ng-click="profile.updateUser(profileForm.$valid)">
                                    <i class="fa fa-angle-right"></i>Submit
                                </button>
                                <button class="btn btn-sm btn-warning" type="button" ng-click="profile.reset()">
                                  <i class="fa fa-repeat"></i> Reset
                                  </button>
                                <span class="text-danger" ng-hide="profileForm.$valid">&nbsp;&nbsp;(Fields with * are mandatory.)</span>
                            </div>
                        </div>
                   </div>
                </div> 
                </div>
         </form>
        </div>
    </div>
@endsection   
@section('page_scripts')
{!! HTML::script("js/directives/inputDatepicker.js") !!}
{!! HTML::script("js/controllers/ProfileEditController.js") !!}
{!! HTML::script("js/directives/uploadDropzone.js") !!}
@append