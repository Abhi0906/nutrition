
@extends('layout.app')

@section('content')
<div ng-controller="ExerciseController as exercise" ng-init="exercise.userId='{{$user_id}}';exercise.userWeight='{{$user_weight}}';exercise.loggedExercise()">
	<form name="ExerciseForm">		
		<div class="row">
			<div class="col-md-6 col-xs-12">
				<div class="block">
				    <div class="block-title">
				        <h3 class="widget-options pull-right">
				            <div class="btn-group btn-group-xs">
					            <a href="/diary?viewreports=exercise" class="btn btn-default" data-toggle="tooltip" title="" data-original-title="Reports">View Reports</a>
					        </div> 
				        </h3>
				        <h3>Log Exercise</h3>
				    </div>
				    <div class="widget-extra">

				        <div class="row">
					    	<div class="col-xs-12">
		                        <h5><label for="search_exercise" class="text-primary">Enter exercise:</label> 
		                        	</h5>
		                        
								<form name="searchExerciseForm">
			                        <div class="input-group">
			                        	
				                            <input type="text" size="37" ng-model="exercise.searchKeyExercise" id="search_exercise" class="form-control" placeholder="Search exercise" autocomplete="off">
				                            <span class="input-group-btn">
				                                <button type="submit" class="btn btn-primary" ng-click="exercise.searchExercise();"><i class="fa fa-search"></i>&nbsp;Search</button>
				                            </span>
			                        		<a href="javascript:void(0)" class="clear-search-btn pull-right" ng-click="exercise.clear_logExercise_searchResult()">Clear</a>
			                        </div>
		                        </form>
		                        <div class="dd-menu" ng-show="exercise.searchMenu">
			                        <ul class="dd-dataset">
						                <li ng-repeat="data in exercise.exerciseSearchdata | filter:food.new_nutrition.searchFood">
						                    <p ng-click="exercise.selectedExercise(data);" class="dd-suggestion"><strong>@{{data.name}} </strong> <br><small>@{{data.time}} minutes, @{{data.calories_burned}} cal</small>
						                    </p>
						                </li>
						                <li ng-show="exercise.exerciseSearchdata.length==0">
						                	<p class="text-center text-primary"><strong>No Results found.</strong></p>
						                </li>
						            </ul>
					        	</div>
		                   		<h5></h5>
		                   		<div class="alert alert-success animation-fadeInQuick" ng-show="exercise.showMessage">
		                   			<span>Exercise was added to your log.</span>
		                   		</div>
		                    </div>
					    	<div class="col-xs-12" ng-show="exercise.searchExerciseTabs" id="freq_rec_tabs">
			                    <ul class="nav nav-tabs nav-justified" >
								  <li role="presentation" id="frequent_btn" ng-click="exercise.frequent_recent_toggle('frequent')" class="active">
								  	<a href="javascript:void(0)"><strong>Frequent</strong></a></li>
								  <li role="presentation" id="recent_btn" ng-click="exercise.frequent_recent_toggle('recent')">
								  	<a href="javascript:void(0)" ><strong>Recent</strong></a></li>
								</ul>
		                        
		                        <div  id="frequently_exercise" ng-show="exercise.frequently_exercise_div" >
		                        	<table class="table">
						        		<tbody>
											<tr ng-repeat="frequent_exercise in exercise.logged_frequent_exercise">
												<td>
													<div  ng-click="exercise.selectedExercise(frequent_exercise);" class="dd-suggestion">
						                        		<strong>@{{frequent_exercise.name}} </strong>
						                        		<br><small>@{{frequent_exercise.time}} minutes, @{{frequent_exercise.calories_burned}} cal</small>
										            </div>
												</td>
											</tr>
											<tr ng-show="exercise.logged_frequent_exercise.length==0">
							                	<td>
							                		<div class="alert alert-info" >
														<span>No frequent exercise available</span>
													</div>
							                	</td>
							                </tr>
										</tbody>
						        	</table>
		                        </div>
			                    <div id="recently_exercise" ng-show="exercise.recently_exercise_div">
									<table class="table">
										<tbody>
											<tr ng-repeat="recent_exercise in exercise.logged_recent_exercise track by $index">
												<td>
													<div ng-click="exercise.selectedExercise(recent_exercise);" class="dd-suggestion">
														<strong>@{{recent_exercise.name}} </strong>
														<br><small>@{{recent_exercise.time}} minutes, @{{recent_exercise.calories_burned}} cal</small>
													</div>
												</td>
											</tr>
											<tr ng-show="exercise.logged_recent_exercise.length==0">
							                	<td>
							                		<div class="alert alert-info" >
														<span>No recent exercise available</span>
													</div>
							                	</td>
							                </tr>
										</tbody>
									</table>
			                    </div>
		                    </div>
				    	</div> 
				    	<span ng-show="exercise.log_loader"  class="log_exercise_loader"></span>
					    <div class="row" ng-show="exercise.displayExerciseSection">
					    	<div class="col-xs-12">
						    	<div class="well well-sm">
						    		<span>@{{exercise.new_exercise.name}}</span>
						        </div>
						    </div>
					    </div>
			            <h5></h5>	
			            <div class="row" ng-show="exercise.displayExerciseSection">
			                <div class="col-xs-6 col-md-3"><label class="text-primary">Type</label></div>
			                <div class="col-xs-6 col-md-4">
			                	<select ng-model="exercise.new_exercise.exercise_type" ng-options="exercise.name as exercise.name for exercise in exercise.exercise_types" class="form-control">         
			                    </select>
			                </div>
		                </div>
		                <h5></h5>
		               		<div class="row" ng-show="exercise.displayExerciseSection">
							<div class="form-group" ng-class="{ 'has-error' : ExerciseForm.minutes.$invalid && !ExerciseForm.minutes.$pristine }">
			                  {!! HTML::decode(Form::label('minutes', '<label class="text-primary">Minutes</label><sup class="text-danger">*</sup>', array('class'=>'col-md-3 control-label'))) !!}
			                   <div  class="col-xs-6 col-md-4">
			                      <div class="input-group">
			                         <input type="text" maxlength="4" number-validation="number-validation"  ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" 
			                            ng-model="exercise.new_exercise.time"
			                            ng-blur="exercise.valid_minutes(exercise.new_exercise.time);"
		                           		ng-keyup="exercise.valid_minutes(exercise.new_exercise.time);"
			                            placeholder="Minutes"  name="minutes" ng-keyup="exercise.calculateCaloriesBurned();"  
			                             id="minutes" class="form-control" required>
			                        	<span class="text-danger" ng-if="exercise.min_error">
											<small>Please enter valid Minutes</small>	
									</span>
			                       </div>
			                    </div>
			                  </div>
			               	</div>
		                <h5></h5> 
			                <div class="row" ng-show="exercise.displayExerciseSection">
				            	<div class="col-xs-6 col-md-3">
				            		<label class="text-primary">Calories burned</label>
				            	</div>
				                <div class="col-xs-6 col-md-4">
									    <input type="text"  class="form-control input-xs" ng-model="exercise.new_exercise.calories_burned" disabled>
				                </div>
			                </div>
		                <h5></h5>             
							<div class="row" ng-show="exercise.displayExerciseSection">
				                <div class="col-xs-6 col-md-3"><label class="text-primary">Select Exercise date</label></div>
				                <div class="col-xs-6 col-md-4">
				                	<input-datepicker format="MM dd, yyyy" date="exercise.new_exercise.exercise_date"></input-datepicker>
				                </div>
				                <div class="col-xs-12 col-md-5">							 
				                	<button type="button" class="btn btn-primary pull-right" id="exbutton"  ng-disabled="ExerciseForm.$invalid" ng-click="exercise.logExercise();">Log Exercise</button>
				                </div>

			                </div>
			                <div class="row div-separator">
			                </div>
			            
				    </div>

				</div>
			</div>
			<div class="col-md-6 col-xs-12">
				<div class="block full">
				    <div class="block-title">
				        <h3>
				            Logged Exercises Today
				        </h3>
				    </div>
				    <div class="widget-extra-full">
				        <div id="no-more-tables" class="table-responsive">
						    <table class="table table-striped">
						        <thead>
						        </thead>
						        <tbody>
						            <tr>
						            	<th class="hidden-xs">Date</th>
						            	<th class="hidden-xs">Activity</th>
						            	<th class="hidden-xs">Calories Burned</th>
						            	<th class="hidden-xs">Duration</th>
						            	<th class="hidden-xs">Exercise type</th>
						            	<th class="hidden-xs"></th>
						            </tr>
						            <tr ng-repeat="value in exercise.logged_exercise track by $index">
						            	<td data-title="Date">@{{value.exercise_date | date:'MMMM dd, yyyy'}}</td>
						            	<td data-title="Activity">@{{value.name}}</td>
						            	<td data-title="Calories Burned">@{{value.calories_burned}}</td>
						            	<td data-title="Duration">@{{value.time}}</td>
						            	<td data-title="Exercise type" class="uppercase">@{{value.exercise_type}}</td>
						            </tr>
						            <tr ng-show="exercise.logged_exercise.length==0">
					                	<td colspan=5>
					                		<div class="alert alert-info" >
												<span>No exercise history available</span>
											</div>
					                	</td>
					                </tr>
						        </tbody>
						    </table>
						</div>
				    </div>
				</div>
			</div>
			<div class="col-md-5 col-xs-12">
				
			</div>
		</div>
	</form>
</div>
 <div class="div-separator"></div>
@endsection

@section('page_scripts')
{!! HTML::script("js/controllers/ExerciseController.js") !!}
{!! HTML::script("js/directives/inputDatepicker.js") !!}
@append