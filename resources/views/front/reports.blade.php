@extends('layout.master')

@section('title')
 Reports  
@endsection

@section('custom-css')
 <style type="text/css">
.filter_active a {
  color:#FF0000;
}
</style>
@endsection
@section('content') 
<div ng-controller="ReportsController as repo" ng-init="repo.userId='{{$user_id}}';
                                                        repo.go_to='{{$go_to}}';  
                                                        repo.go_to_workout();                                                     
                                                        repo.displayCharts('last_week');">
  <!-- scroll tabs -->
  <section class="scroll_tab_section">
    <div class="row">
      <div class="col-md-12">
        <div id="scroll_tabs_dash" class="hidden-xs">
            <div class="tab_container_plot">
              <ul class="nav nav-tabs nav-justified main_tabs">
                  <li class="active">
                    <a class="scroll_divs_unique" id="calorie_report" data-target="#calorie_report_div" href="javascript:void(0)">CALORIE</a>
                  </li>
                  <li>
                  <a class="scroll_divs" id="body_measurement_reports" data-target="#body_measurement_reports_div" href="javascript:void(0)">BODY MEASUREMENTS</a>
                  </li>
                  <li>
                  <a class="scroll_divs" id="weight_report" data-target="#weight_report_div" href="javascript:void(0)">WEIGHT</a>
                  </li>
                  <li>
                    <a class="scroll_divs" id="bp_pulse_report" data-target="#bp_pulse_report_div" href="javascript:void(0)">BP & PULSE</a>
                  </li>
                  <li>
                  <a class="scroll_divs" id="sleep_report" data-target="#sleep_report_div" href="javascript:void(0)">SLEEP</a>
                  </li>
                  <li>
                  <a class="scroll_divs" id="water_report" data-target="#water_report_div" href="javascript:void(0)">WATER</a>
                  </li>
                  <li>
                  <a class="scroll_divs" id="workout_report" data-target="#workout_report_div" href="javascript:void(0)">WORKOUT</a>
                  </li>

              </ul>
            </div>
            <ul class="list-inline report_data_block text-center">
              <li>Filter:</li>
              <li class="filter_active"><a href="javascript:void(0);" ng-click="repo.displayCharts('last_week');">Last week</a></li>
              <li>|</li>
              <li><a href="javascript:void(0);" ng-click="repo.displayCharts('last_month');">Last month</a></li>
              <li>|</li>
              <li><a href="javascript:void(0);" ng-click="repo.displayCharts('last_four_months');">Last Quarter</a></li>
              <li>|</li>
              <li><a href="javascript:void(0);" ng-click="repo.displayCharts('last_six_months');">Last 6 months</a></li>
              <li>|</li>
              <li ><a href="javascript:void(0);"  ng-click="repo.displayCharts('last_year');">Last 12 months</a></li>
          </ul> 

        </div>
      </div>
    </div>
  </section>
  <!-- end scroll tabs -->
  <section class="calorie_report_section">
    <div class="row" id="calorie_report_div">
      <div class="col-sm-12 text-center">
         <h2 class="text-light-blue head_title_main">CALO<span class="title_head_line">RIE REP</span>ORT</h2>
        <div class="saperator"></div>
        <div class="chart_block tabs_contain_report" >
          <div ng-show="repo.chart_loader"  class="report_chart_loader"></div>
           <div  ng-show="repo.calorie_chart_display" id="calorie-chart" class="chart_reports_height"></div>
        </div>
      </div>

    </div>
  </section>
  <section class="body_measure_report_section">
    <div class="row" id="body_measurement_reports_div">
        <div class="col-sm-12"> 
          <h2 class="text-light-blue text-center head_title_main">BODY  MEASU<span class="title_head_line">REME</span>NT REPORT</h2>  
          <div class="saperator"></div>
          <div class="tabs_container tabs_contain_report">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#all_tab">All</a></li>
              <li ><a data-toggle="tab" href="#neck_tab">Neck</a></li>
              <li class=""><a data-toggle="tab" href="#arms_tab">Arms</a></li>
              <li class=""><a data-toggle="tab" href="#waits_tab">Waist</a></li>
              <li class=""><a data-toggle="tab" href="#hips_tab">Hips</a></li>
              <li class=""><a data-toggle="tab" href="#legs_tab">Legs</a></li>
            </ul>
            <div class="tab-content chart_block">
              <div class="tab-pane fade active in" id="all_tab">
                 <div ng-show="repo.chart_loader"  class="report_chart_loader"></div> 
                 <div ng-show="repo.bm_chart_display" id="all-bm-chart" class="chart_reports_height"></div>
              </div>
              <div class="tab-pane fade " id="neck_tab">
                  <div  id="neck-chart" class="chart_reports_height"></div>
              </div>
              <div class="tab-pane fade in" id="arms_tab">
                  <div id="arms-chart" class="chart_reports_height"></div>
              </div>
              <div class="tab-pane fade" id="waits_tab"> 
                  <div id="waist-chart" class="chart_reports_height"></div>
              </div>
              <div class="tab-pane fade" id="hips_tab">
                  <div id="hips-chart" class="chart_reports_height"></div>
              </div>
              <div class="tab-pane fade in" id="legs_tab">
                  <div id="legs-chart" class="chart_reports_height"></div>
              </div>
            </div>  
          </div>
        </div>
    </div>
    <div class="saperator"></div> 
  </section>
  <section class="weight_report_section">
    <div class="row" id="weight_report_div">
      <div class="col-sm-12"> 
        <h2 class="text-light-blue text-center head_title_main">WEIG<span class="title_head_line">HT RE</span>PORT</h2>  
        <div class="saperator"></div>
        <div class="tabs_container tabs_contain_report">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#weight_tab">Weight</a></li>
              <li><a data-toggle="tab" href="#bodyfat_tab">Body Fat</a></li>
            </ul>
            <div class="tab-content chart_block">
              <div class="tab-pane fade active in" id="weight_tab">
                  <div ng-show="repo.chart_loader"  class="report_chart_loader"></div> 
                  <div  ng-show="repo.weight_chart_display" id="weight-chart" class="chart_reports_height"></div>
              </div>
              <div class="tab-pane fade" id="bodyfat_tab">
                 <div  id="body-fat-chart" class="chart_reports_height"></div>
              </div>
            </div>  
        </div>
      </div>
    </div>
  </section>
  <section class="bppulse_report_section">
    <div class="row" id="bp_pulse_report_div">
      <div class="col-sm-12">
        <div class="saperator"></div>  
        <h2 class="text-light-blue text-center head_title_main">BP &amp; PUL<span class="title_head_line">SE R</span>EPORT</h2>  
        <div class="saperator"></div>
        <div class="tabs_container tabs_contain_report">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#systolic_d_tab">Systolic / Diastolic</a></li>
              <li class=""><a data-toggle="tab" href="#pulse_tab">Pulse</a></li>
            </ul>
            <div class="tab-content chart_block">
              <div class="tab-pane fade active in" id="systolic_d_tab"> 
                   <div ng-show="repo.chart_loader"  class="report_chart_loader"></div> 
                   <div ng-show="repo.bp_chart_display" id="bp-chart" class="chart_reports_height"></div>
              </div>
              <div class="tab-pane fade" id="pulse_tab">
                  <div id="pulse-chart" class="chart_reports_height"></div>
              </div>
            </div>  
        </div>
      </div>
    </div>
  </section>
  <section class="sleep_report_section">
    <div class="row" id="sleep_report_div">
      <div class="col-sm-12">
        <div class="saperator"></div>  
        <h2 class="text-light-blue text-center head_title_main">SLEE<span class="title_head_line">P REP</span>ORT</h2>  
        <div class="saperator"></div>
        <div class="chart_block tabs_contain_report">
          <div ng-show="repo.chart_loader"  class="report_chart_loader"></div>  
          <div ng-show="repo.sleep_chart_display" id="sleep-chart" class="chart_reports_height"></div>
        </div>  
        <div class="saperator"></div>
      </div>
    </div>
  </section>
  <section class="water_report_section">
    <div class="row" id="water_report_div">
      <div class="col-sm-12">
        <h2 class="text-light-blue text-center head_title_main">WATE<span class="title_head_line">R REP</span>ORT</h2>  
        <div class="saperator"></div>
         <div class="chart_block tabs_contain_report">
         <div ng-show="repo.chart_loader"  class="report_chart_loader"></div>   
                 <div  ng-show="repo.water_chart_display" id="waterintake-chart" class="chart_reports_height"></div>
        </div>

       
        
      </div>
    </div>
  </section>
  <section class="workout_report_section">
    <div class="row" id="workout_report_div">
      <div class="col-sm-12">
        <h2 class="text-light-blue text-center head_title_main">WORK<span class="title_head_line">OUT REP</span>ORT</h2>  
        <div class="saperator"></div>
         <div class="chart_block tabs_contain_report">
         <div ng-show="repo.chart_loader"  class="report_chart_loader"></div>   
                 <div  ng-show="repo.workout_chart_display" id="workout-chart" class="chart_reports_height"></div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('js')
{!! HTML::script("js/amcharts.js") !!}
{!! HTML::script("js/serial.js") !!}
{!! HTML::script("js/controllers/ReportsController.js") !!}
@append

@section('custom-js')
<script>
  $(document).ready(function() {

   $(".tab_container_plot ul li a").removeClass("scroll_divs");

      $(".tab_container_plot ul li a").addClass("scroll_divs_first");
      $(document).on('click','.scroll_divs_first',function(e){
        e.preventDefault();
        var id = $(this).attr('id');

        if($(window).width()<1200) {
          $('html, body').animate({
                'scrollTop' : $("#"+id+"_div").position().top - 205
          });
        } else {
          $('html, body').animate({
                'scrollTop' : $("#"+id+"_div").position().top - 325
          });
        }
            

        $(".tab_container_plot ul li a").removeClass("scroll_divs_first");
        $(".tab_container_plot ul li a").addClass("scroll_divs");  
        console.log("first click");
      });

      
      $(document).on('click','.scroll_divs',function(e){
        e.preventDefault();
        var id = $(this).attr('id');
         
        $('html, body').animate({
            'scrollTop' : $("#"+id+"_div").position().top - 190
        });
        console.log("second click");
      }); 

      $(document).on('click','.scroll_divs_unique',function(e){
     
        e.preventDefault();
        $('.tab_container_plot ul li a').removeClass("scroll_divs");
        $('.tab_container_plot ul li a').addClass("scroll_divs_first");
        console.log("Third click");
    });




      $(".main_tabs li").on('click',function() {
        $('ul.main_tabs').find('li').removeClass('active');
        $(this).addClass('active');
      });

      $(".report_data_block li").on('click',function() {
        $('.report_data_block').find('li').removeClass('filter_active');
        $(this).addClass('filter_active');
      });

  });


    


  /* if($(window).width()>1199){


      $(".tab_container_plot ul li a").removeClass("scroll_divs");
      $(".tab_container_plot ul li a").addClass("scroll_divs_first");

      $(".scroll_divs_first").on('click',function(e) {
        e.preventDefault();
        var id = $(this).attr('id');
        $('html, body').animate({
            'scrollTop' : $("#"+id+"_div").position().top - 325
        });
        $(".tab_container_plot ul li a").removeClass("scroll_divs_first");
        $(".tab_container_plot ul li a").addClass("scroll_divs");  
      });

      
      
      $(".scroll_divs").on('click',function(e) { 
        e.preventDefault();
        var id = $(this).attr('id');
         
        $('html, body').animate({
            'scrollTop' : $("#"+id+"_div").position().top - 190
        });
      }); 

       
    }
    else
    {
      $(".scroll_divs").on('click',function() {
      var id = $(this).attr('id');
      alert("else"+id);
        $('html, body').animate({
            'scrollTop' : $("#"+id+"_div").position().top - 90
        });
      }); 
    }*/


// activate tab on scroll
  if($(window).width()>1199){
    var screen_offset = 210;
  }
  else{
    var screen_offset = 180;
  }
  $('body').scrollspy({ target: '#scroll_tabs_dash', offset: screen_offset });
  //end: activate tab on scroll

</script>


 

@append