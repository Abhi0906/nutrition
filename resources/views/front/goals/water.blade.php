<div class="col-xs-12 col-12 col-md-12 col-lg-6 body_measure_portion">
     <div class="panel panel-primary weight_body_panel weight_diary_panel_size sleepgoal_watergoal_tbl3">
        <div class="panel-heading text-center panel_head_background">WATER GOALS</div>
          <div class="panel-body panel-container">
             <div class="row">
                <div class="col-sm-6 col-md-6 nopadding_column">
                   <table class="table table-bordered table_fixed_height_water">
                      <tr>
                         <td class="table fixed_colum_width">
                            <h5 class="text-primary">Water Goal</h5>
                         </td>
                         <td>
                            <progress-bar percent-value='@{{goals.water_goal.water_logged_percentage}}'></progress-bar>
                         </td>
                      </tr>
                      <tr class="table_fixed_bottom">
                         <td colspan="2" class="text-center">
                            <button class="btn btn-primary btndesign" onclick="window.location.href='/food'">Log</button>
                         </td>
                      </tr>
                   </table>
                </div>
                <div class="col-sm-6 col-md-6 nopadding_column">
                  <form class="form-horizontal" name="frmWaterGoal" novalidate>
                     <table class="table table-bordered table_fixed_height_water rightSide_tbl_tbl_fixed_height">
                        <tr>
                           <td class="form_contain_td">
                              
                                 <div class="form-group">
                                    <label class="control-label col-xs-4 col-sm-5 text-right" for="wzaterintake">Water Intake*</label>
                                    <div class="col-xs-8 col-sm-7">
                                        <div class="input-group date datepicker_new_design">
                                         <input class="form-control" type="text" name="goal_water" required id="wzaterintake" ng-model="goals.water_goal.water_g">
                                         <span class="input-group-addon">Oz</span>
                                      </div>
                                    </div>
                                 </div>
                             
                           </td>
                        </tr>
                        <tr class="table_fixed_bottom">
                           <td class="text-center"> 
                              <button class="btn btn-primary btndesign" onclick="window.location.href='/reports'">
                              &nbsp; View History</button>
                              <button class="btn btn-primary btndesign1" ng-click="goals.updateWaterGoal();" ng-disabled="frmWaterGoal.$invalid">Save</button>
                           </td>
                        </tr>
                     </table>
                   </form>
                </div>
             </div>
          </div>
     </div>
</div>