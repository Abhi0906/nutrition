 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 wight_body_portion">
     <div class="panel panel-primary weight_body_panel weight_diary_panel_size weightgolas_bppulse_tbl2">
        <div class="panel-heading text-center panel_head_background">WEIGHT GOALS</div>
        <div class="panel-body panel-container">
           <div class="row">
              <div class="col-sm-6 col-md-6 nopadding_column">
                 <table class="table table-bordered table_fixed_height">
                    <tr>
                       <td class="table fixed_colum_width">
                          <h5 class="text-primary">Weight</h5>
                       </td>
                       <td>
                           <progress-bar percent-value='@{{goals.weight_goal.weight_percentage}}'></progress-bar>
                       </td>
                    </tr>
                    <tr>
                       <td>
                          <h5 class="text-primary">Body Fat</h5>
                       </td>
                       <td>
                          <progress-bar percent-value='@{{goals.weight_goal.body_fat_percentage}}'></progress-bar>
                       </td>
                    </tr>
                    <tr class="table_fixed_bottom">
                       <td colspan="2" class="text-center">
                          <button class="btn btn-primary btndesign" onclick="window.location.href='/weight'">Log</button>
                          <button class="btn btn-primary btndesign"><i class="fa fa-refresh"></i>&nbsp; Sync with device</button>
                       </td>
                    </tr>
                 </table>
              </div>
              <div class="col-sm-6 col-md-6 nopadding_column">
                  <form class="form-horizontal" name="frmWeightGoal" novalidate>
                     <table class="table table-bordered table_fixed_height rightSide_tbl_tbl_fixed_height">
                        <tr>
                           <td class="form_contain_td">
                              
                                <div class="form-group">
                                    <label class="control-label col-sm-5 col-xs-4 text-right">Current Height*</label>
                                    <div class="col-sm-7 col-xs-8">

                                      <div class="row">
                                        <div class="col-xs-6 col-sm-6">
                                        <div class="input-group date datepicker_new_design">
                                          <input class="form-control col-sm-6"
                                                 type="text"
                                                 name="height_in_feet"
                                                 required
                                                 placeholder="Feet"
                                                 ng-model="goals.weight_goal.height_in_feet"
                                                 ng-change="goals.changeRecommendedCalorie();" 
                                                 >
                                                    <span class="input-group-addon">ft</span>
                                      </div>

                                        </div>


                                        <div class="col-xs-6 col-sm-6">
                                        <div class="input-group date datepicker_new_design">
                                          <input class="form-control col-sm-6"
                                                type="text"
                                                name="height_in_inch"
                                                required placeholder="Inch"
                                                ng-model="goals.weight_goal.height_in_inch"
                                                ng-change="goals.changeRecommendedCalorie();" 
                                              >
                                            <span class="input-group-addon">in</span>
                                      </div>
                                        </div>

                                      </div>
                                    </div>
                                 </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-5 col-xs-4 text-right" >Current Weight*</label>
                                    <div class="col-sm-7 col-xs-8">
                                    <div class="input-group date datepicker_new_design">
                                          <input class="form-control"
                                              type="text"
                                              name="baseline_weight"
                                              required
                                              ng-model="goals.weight_goal.baseline_weight"
                                              ng-change="goals.changeRecommendedCalorie();" 
                                           >
                                            <span class="input-group-addon">lbs</span>
                                      </div>
                                    </div>
                                 </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-sm-5 col-xs-4 text-right">Current Body Fat</label>
                                    <div class="col-sm-7 col-xs-8">
                                     <div class="input-group date datepicker_new_design">
                                       <input class="form-control" type="text" name="current_body_fat" ng-required='goals.weight_goal.body_fat_g'  ng-model="goals.weight_goal.current_body_fat">
                                       <span class="input-group-addon">&nbsp;%</span>
                                      </div>
                                    </div>
                                </div>



                                 <div class="form-group">
                                    <label class="control-label col-sm-5 col-xs-4 text-right">Goals Weight*</label>
                                    <div class="col-sm-7 col-xs-8">
                                     <div class="input-group date datepicker_new_design">
                                       <input class="form-control" type="text" name="weight_g" required ng-model="goals.weight_goal.weight_g">
                                        <span class="input-group-addon">lbs</span>
                                      </div>
                                    </div>
                                 </div>
                                 
                                  <div class="form-group">
                                    <label class="control-label col-sm-5 col-xs-4 text-right">Goal Body Fat</label>
                                    <div class="col-sm-7 col-xs-8">
                                     <div class="input-group date datepicker_new_design">
                                       <input class="form-control" type="text" name="body_fat_g" ng-model="goals.weight_goal.body_fat_g">
                                       <span class="input-group-addon">&nbsp;%</span>
                                      </div>
                                    </div>
                                 </div>
                                  <div class="form-group">
                                    <label class="control-label col-sm-5 col-xs-4 text-right">Weekly Goal*</label>
                                    <div class="col-sm-7 col-xs-8">
                                        <select ng-model="goals.weight_goal.weekly_goal_id"
                                          ng-options="weeklygoal.id as weeklygoal.name for weeklygoal in goals.weeklygoals" 
                                          class="form-control"
                                          ng-change="goals.changeCalories();"
                                          >
                                          <option value="">Select weekly goal</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-sm-5 col-xs-4 text-right text-primary recom_lbl">Recommended Daily Calorie Goal</label>
                                    
                                   <div class="col-sm-7 col-xs-8 text-left">
                                     <label class="control-label">@{{goals.weight_goal.recommended_daily_calorie}} Calories</label>
                                   </div>
                                 </div>
                             
                           </td>
                        </tr>
                         
                        <tr class="table_fixed_bottom">
                           <td class="text-center"> 
                              <button class="btn btn-primary btndesign" onclick="window.location.href='/reports'">
                              &nbsp; View History</button>
                              <button class="btn btn-primary btndesign1" ng-click="goals.updateWeightGoal();" ng-disabled="frmWeightGoal.$invalid">Save</button>
                           </td>
                        </tr>
                     </table>
                  </form>
              </div>
           </div>
        </div>
     </div>
 </div>