<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 wight_body_portion">
         <div class="panel panel-primary weight_body_panel weight_diary_panel_size sleepgoal_watergoal_tbl3">
            <div class="panel-heading text-center panel_head_background">SLEEP GOALS</div>
            <div class="panel-body panel-container">
               <div class="row">
                  <div class="col-sm-6 col-md-6 nopadding_column">
                     <table class="table table-bordered table_fixed_height_water">
                        <tr>
                           <td class="table fixed_colum_width">
                              <h5 class="text-primary">Sleep</h5>
                           </td>
                           <td>
                              <progress-bar percent-value='@{{goals.user_goal.sleep_percentage}}'></progress-bar>
                           </td>
                        </tr>
                        
                        <tr class="table_fixed_bottom">
                           <td colspan="2" class="text-center">
                              <button class="btn btn-primary btndesign" onclick="window.location.href='/sleep'">Log</button>
                           </td>
                        </tr>
                     </table>
                  </div>
                  <div class="col-sm-6 col-md-6 nopadding_column">
                    <form class="form-horizontal" name="frmSleepGoal" novalidate>
                       <table class="table table-bordered table_fixed_height_water rightSide_tbl_tbl_fixed_height">
                          <tr>
                             <td class="form_contain_td">
                                                                  
                                  <div class="form-group">
                                      <label class="control-label col-sm-5 col-xs-4 text-right">Goal Date*</label>
                                      <div class="col-sm-7 col-xs-8">
                                        <div class="input-group date datepicker_new_design">
                                           <input type="text"
                                              class="form-control" 
                                              uib-datepicker-popup = "@{{goals.format}}"
                                               ng-model="goals.sleep_goal.goal_date"
                                               is-open="goals.sleep_popup.opened"
                                               datepicker-options="goals.dateOptions"
                                               ng-required="true"
                                               show-button-bar=false
                                               close-text="Close" />
                                                <span class="input-group-addon caledar_custom" ng-click="goals.sleep_open()"> 
                                                   <i aria-hidden="true" class="fa fa-calendar"></i>
                                                 </span>
                                           </div>
                                      </div>
                                   </div>

                                   <div class="form-group">
                                       <label class="control-label col-xs-4 col-sm-5 text-right custom_class">Sleep*</label>
                                       <div class="col-xs-8 col-sm-7">
                                          <uib-timepicker ng-model="goals.sleep_goal.goal" show-meridian=false ></uib-timepicker>
                                      </div>
                                   </div>
                               
                             </td>

                          </tr>
                          <tr class="table_fixed_bottom">
                             <td class="text-center"> 
                                <button class="btn btn-primary btndesign" onclick="window.location.href='/reports'">
                                &nbsp; View History</button>
                                <button class="btn btn-primary btndesign1" ng-click="goals.updateSleepGoal();" ng-disabled="frmSleepGoal.$invalid">Save</button>
                             </td>
                          </tr>
                       </table>
                       </form>
                  </div>
               </div>
            </div>
         </div>
</div>