 <div class="col-xs-12 col-12 col-md-12 col-lg-6 body_measure_portion">
     <div class="panel panel-primary weight_body_panel weight_diary_panel_size goalsummary_tbl1">
        <div class="panel-heading text-center panel_head_background">BODY MEASUREMENTS</div>
        <div class="panel-body panel-container">
           <div class="row">
              <div class="col-sm-6 col-md-6 nopadding_column">
                 <table class="table table-bordered table_fixed_height">
                    <tr>
                       <td class="table fixed_colum_width">
                          <h5 class="text-primary">Neck</h5>
                       </td>
                       <td>
                           <progress-bar percent-value='@{{goals.bm_goal.neck_percentage}}'></progress-bar>
                       </td>
                    </tr>
                      <tr>
                       <td class="table fixed_colum_width">
                          <h5 class="text-primary">Arms</h5>
                       </td>
                       <td>
                          <progress-bar percent-value='@{{goals.bm_goal.arms_percentage}}'></progress-bar>
                       </td>
                    </tr>
                      <tr>
                       <td class="table fixed_colum_width">
                          <h5 class="text-primary">Waist</h5>
                       </td>
                       <td>
                          <progress-bar percent-value='@{{goals.bm_goal.waist_percentage}}'></progress-bar>
                       </td>
                    </tr>
                    <tr>
                       <td>
                          <h5 class="text-primary">Hips</h5>
                       </td>
                       <td>
                          <progress-bar percent-value='@{{goals.bm_goal.hips_percentage}}'></progress-bar>
                       </td>
                    </tr>
                     <tr>
                       <td>
                          <h5 class="text-primary">Legs</h5>
                       </td>
                       <td>

                          <progress-bar percent-value='@{{goals.bm_goal.legs_percentage}}'></progress-bar>

                       </td>
                    </tr>
                    <tr class="table_fixed_bottom">
                       <td colspan="2" class="text-center">
                          <button class="btn btn-primary btndesign" onclick="window.location.href='/weight'">Log</button>
                       </td>
                    </tr>
                 </table>
              </div>
              <div class="col-sm-6 col-md-6 nopadding_column">
                <form class="form-horizontal" name="frmBodyMeasurement" novalidate>
                   <table class="table table-bordered table_fixed_height rightSide_tbl_tbl_fixed_height">
                      <tr>
                         <td class="form_contain_td">
                            
                              <div class="form-group">
                                  <label class="control-label col-sm-4 col-xs-3 text-right">Goal Date*</label>
                                  <div class="col-sm-8 col-xs-9">
                                    

                                    <div class="input-group date datepicker_new_design">
                                      <input type="text"
                                              class="form-control" 
                                              uib-datepicker-popup = "@{{goals.format}}"
                                               ng-model="goals.bm_goal.goal_date"
                                               is-open="goals.body_date_popup.opened"
                                               datepicker-options="goals.dateOptions"
                                               ng-required="true"
                                               show-button-bar=false
                                               close-text="Close" />
                                                <span class="input-group-addon caledar_custom" ng-click="goals.body_date_open()"> 
                                                   <i aria-hidden="true" class="fa fa-calendar"></i>
                                                 </span>

                                    </div>


                                  </div>
                               </div>
                              

                                 <div class="form-group">
                                  <label class="control-label col-sm-4 col-xs-3 text-right">Neck*</label>
                                   <div class="col-sm-8 col-xs-9">
                                     <div class="input-group date datepicker_new_design">
                                     <input class="form-control" type="text" name="neck_g" required ng-model="goals.bm_goal.neck_g" />
                                     <span class="input-group-addon">inches</span>
                                    </div>
                                   </div>
                               </div>
                               <div class="form-group">
                                  <label class="control-label col-sm-4 col-xs-3 text-right">Arms*</label>
                                  <div class="col-sm-8 col-xs-9">
                                    <div class="input-group date datepicker_new_design">
                                      <input class="form-control" type="text" name="arms_g" required ng-model="goals.bm_goal.arms_g">
                                       <span class="input-group-addon">inches</span>
                                    </div>
                                  </div>
                               </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-4 col-xs-3 text-right">Waist*</label>
                                  <div class="col-sm-8 col-xs-9">
                                  <div class="input-group date datepicker_new_design">
                                     <input class="form-control" type="text" name="waist_g" required ng-model="goals.bm_goal.waist_g">
                                     <span class="input-group-addon">inches</span>
                                    </div>
                                  </div>
                               </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-4 col-xs-3 text-right">Hips*</label>
                                  <div class="col-sm-8 col-xs-9">
                                  <div class="input-group date datepicker_new_design">
                                     <input class="form-control" type="text" name="hips_g" required ng-model="goals.bm_goal.hips_g">
                                     <span class="input-group-addon">inches</span>
                                   </div>
                                  </div>
                               </div>
                               <div class="form-group">
                                  <label class="control-label col-sm-4 col-xs-3 text-right">Legs*</label>
                                  <div class="col-sm-8 col-xs-9">
                                  <div class="input-group date datepicker_new_design">
                                     <input class="form-control" type="text" name="legs_g" required ng-model="goals.bm_goal.legs_g">
                                     <span class="input-group-addon">inches</span>
                                    </div>
                                  </div>
                               </div>
                         </td>
                      </tr>
                      <tr class="table_fixed_bottom">
                         <td class="text-center"> 
                            <button class="btn btn-primary btndesign" onclick="window.location.href='/reports'">
                            &nbsp; View History</button>
                            <button class="btn btn-primary btndesign1" ng-click="goals.updateBodyMeasurement();" ng-disabled="frmBodyMeasurement.$invalid">Save</button>
                         </td>
                      </tr>
                   </table>
                 </form>
              </div>
           </div>
        </div>
     </div>
</div>