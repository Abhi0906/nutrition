<div class="col-xs-12 col-12 col-md-12 col-lg-6 body_measure_portion">
         <div class="panel panel-primary weight_body_panel weight_diary_panel_size weightgolas_bppulse_tbl2">
            <div class="panel-heading text-center panel_head_background">BP & PULSE</div>
            <div class="panel-body panel-container">
               <div class="row">
                  <div class="col-sm-6 col-md-6 nopadding_column">
                     <table class="table table-bordered table_fixed_height">
                        <tr>
                           <td class="table fixed_colum_width">
                              <h5 class="text-primary">Systolic</h5>
                           </td>
                           <td>
                               <progress-bar percent-value='@{{goals.bp_goal.systolic_percentage}}'></progress-bar>
                           </td>
                        </tr>
                          <tr>
                           <td class="table fixed_colum_width">
                              <h5 class="text-primary">Diastolic</h5>
                           </td>
                           <td>
                              <progress-bar percent-value='@{{goals.bp_goal.diastolic_percentage}}'></progress-bar>
                           </td>
                        </tr>
                          <tr>
                           <td class="table fixed_colum_width">
                              <h5 class="text-primary">Pulse</h5>
                           </td>
                           <td>
                              <progress-bar percent-value='@{{goals.bp_goal.pulse_percentage}}'></progress-bar>
                           </td>
                        </tr>
                       

                        <tr class="table_fixed_bottom">
                           <td colspan="2" class="text-center">
                              <button class="btn btn-primary btndesign" onclick="window.location.href='/bp_pulse'">Log</button>
                              
                              <button class="btn btn-primary btndesign"><i class="fa fa-refresh"></i>&nbsp; Sync with device</button>
                           </td>
                        </tr>
                     </table>
                  </div>
                  <div class="col-sm-6 col-md-6 nopadding_column">
                     <form class="form-horizontal" name="frmBpPulse" novalidate>
                       <table class="table table-bordered table_fixed_height rightSide_tbl_tbl_fixed_height">
                          <tr>
                             <td class="form_contain_td">
                                                                
                                   <div class="form-group">
                                      <label class="control-label col-sm-4 col-xs-3 text-right">Goal Date*</label>
                                      <div class="col-sm-8 col-xs-9">
                                        <div  class="input-group date datepicker_new_design">
                                         

                                         

                                           <input type="text"
                                              class="form-control" 
                                              uib-datepicker-popup = "@{{goals.format}}"
                                               ng-model="goals.bp_goal.goal_date"
                                               is-open="goals.bp_pulse_date_popup.opened"
                                               datepicker-options="goals.dateOptions"
                                               ng-required="true"
                                               show-button-bar=false
                                               close-text="Close" />
                                                <span class="input-group-addon caledar_custom" ng-click="goals.bp_pulse_date_open()"> 
                                                   <i aria-hidden="true" class="fa fa-calendar"></i>
                                                 </span>


                                        </div>
                                      </div>
                                   </div>

                                    <div class="form-group">
                                      <label class="control-label col-sm-4 col-xs-3 text-right">Systolic*</label>
                                      <div class="col-sm-8 col-xs-9">
                                         <div class="input-group date datepicker_new_design">
                                         <input class="form-control" type="text" name="systolic_g" required ng-model="goals.bp_goal.systolic_g">
                                         <span class="input-group-addon">mmHg</span>
                                        </div>
                                      </div>
                                   </div>

                                   <div class="form-group">
                                      <label class="control-label col-sm-4 col-xs-3 text-right">Diastolic*</label>
                                      <div class="col-sm-8 col-xs-9">
                                         <div class="input-group date datepicker_new_design">
                                         <input class="form-control" type="text" name="diastolic_g" required ng-model="goals.bp_goal.diastolic_g">
                                         <span class="input-group-addon">mmHg</span>
                                        </div>
                                      </div>
                                   </div>


                                   <div class="form-group">
                                      <label class="control-label col-sm-4 col-xs-3 text-right">Heart Rate*</label>
                                      <div class="col-sm-8 col-xs-9">
                                         <div class="input-group date datepicker_new_design">
                                         <input class="form-control" type="text" name="heart_rate_g" required ng-model="goals.bp_goal.heart_rate_g">
                                         <span class="input-group-addon">BPM &nbsp;&nbsp;</span>
                                        </div>
                                      </div>
                                   </div>

                                   
                                
                             </td>
                          </tr>
                          <tr class="table_fixed_bottom">
                             <td class="text-center"> 
                                <button class="btn btn-primary btndesign" onclick="window.location.href='/reports'">
                                &nbsp; View History</button>
                                <button class="btn btn-primary btndesign1"  ng-click="goals.updateBpPulse();" ng-disabled="frmBpPulse.$invalid">Save</button>
                             </td>
                          </tr>
                       </table>
                     </form>
                  </div>
               </div>
            </div>
         </div>
</div>