<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 wight_body_portion">
         <div class="panel panel-primary weight_body_panel weight_diary_panel_size goalsummary_tbl1">
            <div class="panel-heading text-center panel_head_background">CALORIE GOAL</div>
            <div class="panel-body panel-container">
               <div class="row">
                  <div class="col-sm-6 col-md-6 nopadding_column">
                     <table class="table table-bordered table_fixed_height">
                        <tr>
                           <td class="table fixed_colum_width">
                              <h5 class="text-primary">Calorie Consumed</h5>
                           </td>
                           <td>
                              <progress-bar percent-value='@{{goals.calorie_goal.calorie_intake_percentage}}'></progress-bar>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <h5 class="text-primary">Calorie Burned</h5>
                           </td>
                           <td>
                              <progress-bar percent-value='@{{goals.calorie_goal.calories_burned_percentage}}'></progress-bar>
                           </td>
                        </tr>
                        <tr class="table_fixed_bottom">
                           <td colspan="2" class="text-center">
                              <button class="btn btn-primary btndesign" onclick="window.location.href='/food'">Log</button>

                           </td>
                        </tr>
                     </table>
                  </div>
                  <div class="col-sm-6 col-md-6 nopadding_column">
                    <form class="form-horizontal" name="frmCalorieGoal" novalidate>
                       <table class="table table-bordered table_fixed_height rightSide_tbl_tbl_fixed_height">
                          <tr>
                             <td class="form_contain_td">
                                   <div class="form-group">
                                      <label class="control-label col-xs-5 col-sm-5 text-right">Calorie Intake Goal*</label>
                                      <div class="col-xs-7 col-sm-7">
                                         <input class="form-control" name="calorie_intake_g" type="text" ng-model="goals.calorie_goal.calorie_intake_g" required>
                                      </div>
                                   </div>
                                   <div class="form-group">
                                      <label class="control-label col-xs-5 col-sm-5 text-right">Calories Burned*</label>
                                      <div class="col-xs-7 col-sm-7">
                                         <input class="form-control" type="text" name="calorie_burned_g" ng-model="goals.calorie_goal.calorie_burned_g" required>
                                      </div>
                                   </div>

                             </td>
                          </tr>
                          <tr class="table_fixed_bottom">
                             <td class="text-center">
                                <button class="btn btn-primary btndesign" onclick="window.location.href='/reports'">
                                &nbsp; View History</button>
                                <button class="btn btn-primary btndesign1" ng-click="goals.updateCalorieGoal();" ng-disabled="frmCalorieGoal.$invalid">Save</button>
                             </td>
                          </tr>
                       </table>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>