@extends('layout.master')
@section('title')
Events
@endsection 
@section('custom-css')
 <style type="text/css">
 .fc-scroller {
   overflow-y: hidden !important;
  }

</style>
@endsection 
@section('content') 
<section class="section-panel custom-section-panel" ng-controller="EventsController as event" ng-init="event.getActiveEventSetting();">
	<div class="row text-center text-primary">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h2 class="text-uppercase head_title_main">Your E<span class="title_head_line">vent</span>s</h2>
      </div>
   	</div>
   	<div class="saperator"></div>
   	<div class="panel panel-primary weight_body_panel">
   		<div class="panel-heading panel_head_background text-uppercase"><strong>@{{event.settingTitle}}</strong></div>
   		<div class="panel-body panel_Bodypart">
   			<div class="event_box">
   			   
               <div class="row" ng-if="event.event_type == 'google_calendar'">
                  <div class="col-xs-12">
                       <div id='event_calendar' google-calendar calendar-id ='@{{event.google_calendarId}}'></div>  
                  </div>
               </div>

               <div class="row" ng-if="event.event_type == 'events_url'">
                  <div class="col-xs-12 text-center">
                     <iframe  ng-src="@{{event.event_url}}" width="1024" height="500" frameborder=0></iframe>
                  </div>
               </div>

               <div class="row" ng-if="event.event_type == 'events_pdf'">
                  <div class="col-xs-12 text-center">
                     <object ng-attr-data="@{{event.event_pdf_url}}" type="application/pdf" width="1024" height="500">
                     </object>
                  </div>
               </div>

   			</div>
   		</div>
   	</div>
</section>

@endsection

@section('js')
{!! HTML::script("js/controllers/EventsController.js") !!}
{!! HTML::script("js/gcal.js") !!}


@append
@section('custom-js')
@append
