@extends('layout.master')

@section('title')
  Diary
@endsection

<!-- add Internal css -->
@section('custom-css')
   <style type="text/css">

  </style>
@endsection


@section('content')

<section class="section-page custom-section-page custom-section-panel padding_top_0">

  <div class="row" ng-controller="DiaryIndexController as diary_index"
                    ng-init="diary_index.userId='{{Auth::user()->id}}';
                            diary_index.user_gender='{{ucfirst(Auth::user()->gender)}}';
                            diary_index.getFoodDiary();
                    ">
   <div ui-view></div>


   <!-- right part portion small -->
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 sidebar_foods">
      @include('front.diary.diary-rightbar')
    </div>
    <!-- end of right part portion -->

  </div>
</section>




@endsection
@section('js')
<!-- {!! HTML::script("js/jquery.sameheight.js") !!} -->
<!-- {!! HTML::script("js/controllers/FoodController.js") !!}
{!! HTML::script("js/controllers/FoodDiaryController.js") !!} -->
{!! HTML::script("js/controllers/DiaryIndexController.js") !!}
{!! HTML::script("js/controllers/DiaryController.js") !!}
{!! HTML::script("js/controllers/LogFoodController.js") !!}
{!! HTML::script("js/controllers/LogExerciseController.js") !!}
{!! HTML::script("js/controllers/CustomFoodController.js") !!}
{!! HTML::script("js/controllers/MealController.js") !!}

{!! HTML::script("js/directives/inputDatepicker.js") !!}
{!! HTML::script("js/filters/stringLimitFilter.js") !!}
{!! HTML::style("css/custom_radio.css") !!}
@append
@section('custom-js')

  <script type="text/javascript">

    $(document).ready(function(){
        $(window).scroll(function(){
          var sticky = $('.food_exe_stick'),
              scroll = $(window).scrollTop();

          if($(window).width() > 1200) {
              if (scroll >= 100) sticky.addClass('food_exe_stick_fixed');
              else sticky.removeClass('food_exe_stick_fixed');
          } else {
              if (scroll >= 100) sticky.addClass('food_exe_stick_fixed');
              else sticky.removeClass('food_exe_stick_fixed');
          }
        });

        /*$(window).scroll(function(){
          var sticky = $('.food_exe_stick'),
              scroll = $(window).scrollTop();

          if (scroll >= 100) sticky.addClass('fixed_sticky_food');
          else sticky.removeClass('fixed_sticky_food');
        });*/

        if($(window).width() < 768)
        {
          // setTimeout(function() {
          //   $('.ex_smallcolumndiv').sameheight();
          // }, 1000);
        }
        setTimeout(function(){
        $(".food_monthly_calandar.angular-calender .fc-prev-button").find("span").removeClass("ui-icon ui-icon-circle-triangle-w").addClass("fa fa-chevron-left fa-fw");
        $(".food_monthly_calandar.angular-calender .fc-next-button").find("span").removeClass("ui-icon ui-icon-circle-triangle-w").addClass("fa fa-chevron-right fa-fw");

        $('.food_monthly_calandar.angular-calender .fc-row.fc-week.ui-widget-content').addClass("tablecell_blockqual");

          /*script for equal height of two elements For Calander*/
          $('.food_monthly_calander_container').each(function(e){
            var highestBox = 0;
            $('.tablecell_blockqual', this).each(function(){
              if($(this).height() > highestBox) {
                highestBox = $(this).height();
              }
            });
            $('.tablecell_blockqual', this).height(highestBox);
          });
        }, 2000);

        $(".last_tr_white").hover(
          function() {
            $(this).find("a.a_last_tr").css("color","#fff");
          },
          function() {
            $(this).find("a.a_last_tr").css("color","#0073BC");
          }
        );

        if($(window).width() <= 800 ){
          $('#responsive_food_table tr td.head_slide_foodtbl_td').siblings().slideUp();
          $("#food_1st_td").siblings().slideToggle();
          $('#responsive_food_table tr td.head_slide_foodtbl_td').click(function(){
            $(this).siblings().slideToggle("slow");
          });
        }
    });

  </script>

@endsection