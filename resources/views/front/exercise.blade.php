@extends('layout.master')

@section('title')
  Exercise
@endsection

<!-- add Internal css -->
@section('custom-css')
   <style type="text/css">
      @media only screen and (max-width: 800px) {
        #exercise_1st_table_responsive table,
        #exercise_1st_table_responsive thead,
        #exercise_1st_table_responsive tbody,
        #exercise_1st_table_responsive th,
        #exercise_1st_table_responsive td,
        #exercise_1st_table_responsive tr,

        #exercise_2st_table_responsive table,
        #exercise_2st_table_responsive thead,
        #exercise_2st_table_responsive tbody,
        #exercise_2st_table_responsive th,
        #exercise_2st_table_responsive td,
        #exercise_2st_table_responsive tr,

        #exercise_2st_table_responsive_2part table,
        #exercise_2st_table_responsive_2part thead,
        #exercise_2st_table_responsive_2part tbody,
        #exercise_2st_table_responsive_2part th,
        #exercise_2st_table_responsive_2part td,
        #exercise_2st_table_responsive_2part tr,

        #exercise_3st_table_responsive table,
        #exercise_3st_table_responsive thead,
        #exercise_3st_table_responsive tbody,
        #exercise_3st_table_responsive th,
        #exercise_3st_table_responsive td,
        #exercise_3st_table_responsive tr                                            
        {
        display: block;
        }
        #exercise_1st_table_responsive thead tr,
        #exercise_2st_table_responsive thead tr,

        #exercise_2st_table_responsive_2part thead tr,
        #exercise_3st_table_responsive tr
        {
          position: absolute;
          top: -9999px;
          left: -9999px;
        }
        #exercise_1st_table_responsive tr {/* border: 1px solid #ccc; */}
        #exercise_1st_table_responsive td, 
        #exercise_2st_table_responsive td,
        #exercise_2st_table_responsive_2part td, 
        #exercise_3st_table_responsive td
        {
        /* Behave like a "row" */
          border: none;
          border-bottom: 1px solid #eee;
          position: relative;
          padding-left: 50%;
          white-space: normal;
          text-align:left;
        }
        #exercise_1st_table_responsive table tbody tr td:before,
        #exercise_2st_table_responsive table tbody tr td:before,

        #exercise_2st_table_responsive_2part table tbody tr td:before,
        #exercise_3st_table_responsive table tbody tr td:before
        {
          content: attr(data-title);
          position: absolute;
          top: 6px;
          left: 6px;
          width: 45%;
          font-size: 90%;
          padding-right: 10px;
          white-space: nowrap;
          text-align:left;
          font-weight: bold;
        }
        
        #exercise_1st_table_responsive tr td.head_slide_exec_tbl_td,
        #exercise_2st_table_responsive tr td.head_slide2_exec2_tbl_td,

        #exercise_2st_table_responsive_2part tr td.head_slide_exec_tbl_td,
        #exercise_3st_table_responsive tr td.head_slide2_exec2_tbl_td
        {
          border-bottom : 1px solid #cce7ff !important;
          font-weight: bold;
          color: #0073BC;
          font-size: 125%;
        }

        #exercise_1st_table_responsive ul, #exercise_2st_table_responsive ul,
        #exercise_2st_table_responsive_2part ul, #exercise_3st_table_responsive ul
        {
          padding: 0px;
        }

        #exercise_1st_table_responsive tr td.inner-table_td table tr,
        #exercise_2st_table_responsive tr td.inner-table_td table tr,

        #exercise_2st_table_responsive_2part tr td.inner-table_td table tr,
        #exercise_3st_table_responsive tr td.inner-table_td table tr
        {
          border-top: 1px solid #cce7ff;
          border-bottom: 1px solid #cce7ff;
        }
      }
      .data_color{
          color:black;
      }

     .text-excess{
      color: #FF1A1A !important;
      }
      .text-deficit{
        color: #33CC33 !important;
      }
      .text-neutral{
        color: #FDE674 !important;
      }     
      .cursor{
          cursor:default;
      } 
    
   </style>
@endsection
 
@section('content') 
<section class="section-page">
    <div class="row" ng-controller="ExerciseController as exercise" ng-init="exercise.userId='{{Auth::user()->id}}';">
      <div ui-view></div>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 sidebar_foods">
         @include('front.exercise.exercise-rightbar') 
      </div>
   </div>
</section>


@endsection
@section('js')
{!! HTML::script("js/jquery.sameheight.js") !!}

{!! HTML::script("js/controllers/ExerciseController.js") !!}
{!! HTML::script("js/controllers/ExerciseDiaryController.js") !!}
{!! HTML::script("js/controllers/LogExerciseController.js") !!}
{!! HTML::script("js/filters/stringLimitFilter.js") !!}
@append
@section('custom-js')
 
  <script type="text/javascript">

     
    $(document).ready(function(){
        
      $(".last_tr_white").hover(
          function() {
            $(this).find("a.a_last_tr").css("color","#fff");
          },
          function() {
            $(this).find("a.a_last_tr").css("color","#0073BC");
          }
      );


     
      setTimeout(function(){
        $(".exercise_monthly_calender.angular-calender .fc-prev-button").find("span").removeClass("ui-icon ui-icon-circle-triangle-w").addClass("fa fa-chevron-left fa-fw");
        $(".exercise_monthly_calender.angular-calender .fc-next-button").find("span").removeClass("ui-icon ui-icon-circle-triangle-w").addClass("fa fa-chevron-right fa-fw");


        $('.exercise_monthly_calender.angular-calender .fc-row.fc-week.ui-widget-content').addClass("tablecell_blockqual");

        /*script for equal height of two elements For Calander*/
        $('.calander_container_portion').each(function(){  
          var highestBox = 0;
          $('.tablecell_blockqual', this).each(function(){
            if($(this).height() > highestBox) {
              highestBox = $(this).height(); 
            }          
          });  
          $('.tablecell_blockqual', this).height(highestBox);           
        });
        /*End of script for equal height of two elements*/

      }, 2000);



      if($(window).width() <= 800 ){
        $('#exercise_1st_table_responsive tr td.head_slide_exec_tbl_td').siblings().slideUp();
        $("#exec_1st_td").siblings().slideToggle();
        $('#exercise_1st_table_responsive tr td.head_slide_exec_tbl_td').click(function(){
          $(this).siblings().slideToggle("slow");
        });

        $('#exercise_2st_table_responsive tr td.head_slide2_exec2_tbl_td').siblings().slideUp();
        $("#exec_2st_td2").siblings().slideToggle();
        $('#exercise_2st_table_responsive tr td.head_slide2_exec2_tbl_td').click(function(){
          $(this).siblings().slideToggle("slow");
        });
      }


    
    }); 
   

  </script>

@endsection

 