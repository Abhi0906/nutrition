<div ng-init=""> 
	<div class="block">
		<div class="block-title">
			<div class="block-options pull-right">
                    <a class="btn btn-alt btn-sm btn-default " 
                     title="Back" ui-sref="classes">Back</a>
            </div>  
            <h2><strong>Add Class&nbsp;</strong></h2>
		</div>
        <form onsubmit="return false;" class="form-horizontal form-bordered" method="post" action="page_forms_components.php">
            <div class="row">
                <div class="col-md-7">
                    
                        <div class="form-group">
                            <label for="example-colorpicker" class="col-sm-4 control-label">Class Name</label>
                            <div class="col-sm-7">
                                <input type="text" 
                                        value="" 
                                        class="form-control input-colorpicker colorpicker-element" 
                                        name="example-colorpicker" 
                                        id="example-colorpicker"
                                        ng-model="add_class.class_data.class_name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-colorpicker2" class="col-sm-4 control-label">Location</label>
                            <div class="col-sm-7">
                                <select class="form-control" ng-model="add_class.class_data.class_location">
                                    <option value="1">WestChester</option>
                                    <option value="2">Bronx</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-colorpicker3" class="col-sm-4 control-label">Instructor</label>
                            <div class="col-sm-7">
                                <input type="text" value="" 
                                                    class="form-control input-colorpicker-rgba colorpicker-element" 
                                                    name="example-colorpicker3" 
                                                    id="example-colorpicker3"
                                                    ng-model="add_class.class_data.instructor">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-colorpicker2" class="col-sm-4 control-label">Activity Type</label>
                            <div class="col-sm-7">
                                <select class="form-control" ng-model="add_class.class_data.activity_type">
                                    <option value="1">aerobics</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-colorpicker3" class="col-sm-4 control-label">Class Max Size</label>
                            <div class="col-sm-7">
                                <input type="text" value="" 
                                                    class="form-control input-colorpicker-rgba colorpicker-element" 
                                                    name="example-colorpicker3" 
                                                    id="example-colorpicker3"
                                                    ng-model="add_class.class_data.class_max_size">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="frequency" class="col-sm-4 control-label">Frequency</label>
                            <div class="col-sm-7">
                                <div class="radio">
                                    <label><input type="radio" name="frequency" value="Daily" ng-model="add_class.class_data.frequency" ng-checked="true">Daily</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label><input type="radio" name="frequency" value="Weekly" ng-model="add_class.class_data.frequency">Weekly</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label><input type="radio" name="frequency" value="Monthly" ng-model="add_class.class_data.frequency">Monthly</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="days" class="col-sm-4 control-label">Every</label>
                            <div class="col-sm-7">
                                <div class="input-group">
                                    <input type="number" name="days" 
                                            class="form-control" 
                                            ng-model="add_class.class_data.frequency_interval" 
                                            value="1" 
                                            step="1">
                                    <span class="input-group-addon">Day(s)</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="days" class="col-sm-4 control-label">Start</label>
                            <div class="col-sm-7">
                                <div class="input-group">
                                    <input type="text" name="start_day" 
                                            class="form-control"
                                            ng-model="add_class.class_data.class_start_date" 
                                            >
                                    <!-- <input id="txt_birth_date" class="form-control input-datepicker" 
                                            type="text" 
                                            name="birth_date" 
                                            data-date-format="MM dd, yyyy" 
                                            readonly="readonly" 
                                            placeholder="MM dd, yyyy"> -->
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-sm-7">
                                <div class="radio">
                                    <label><input type="radio" name="for_end_date"
                                                    ng-model="add_class.class_data.for_end_date">No End Date</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-sm-7">
                                <div class="radio">
                                    <label><input type="radio" name="for_end_date"
                                                ng-model="add_class.class_data.for_end_date"
                                                >End after</label>
                                    <input type="number" name="end" 
                                                class="input_occur" 
                                                value="1"
                                                ng-model="add_class.class_data.end_after_occurences"
                                                > Occurences    
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-sm-7">
                                <div class="radio">
                                    <label><input type="radio" name="for_end_date"
                                                    ng-model="add_class.class_data.for_end_date"
                                                    >End by</label>
                                    <div class="input-group">
                                        <input type="text" name="start_day" 
                                                class="form-control" 
                                                ng-model="add_class.class_data.class_end_date" >
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>  
                                </div>
                            </div>
                        </div>
                </div>
                <div class="col-md-5">
                    <div class="panel panel-default">
                        @include('schedules.class_days')
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button class="btn btn-sm btn-primary" type="submit" ng-click="add_class.saveClass()"><i class="fa fa-angle-right"></i> Submit</button>
                                <button class="btn btn-sm btn-warning" type="reset"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
	</div>
</div>