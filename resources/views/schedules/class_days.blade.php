<div class="panel-body add_break_contain">
    <ul class="list-inline ul_block_bottomline">
        <li class="width_25">Monday</li>
        <li> <select class="form-control" ng-model="add_class.class_data.class_days.monday_from">
                <option value="09:00:00">9:00 am</option>
            </select>
        </li>
        <li>to</li>
        <li>
             <select class="form-control" ng-model="add_class.class_data.class_days.monday_to">
                <option value="10:00:00">10:00 am</option>
            </select>
        </li>                             
    </ul>
    <ul class="list-inline ul_block_bottomline">
        <li class="width_25">Tuesday</li>
        <li> <select class="form-control" ng-model="add_class.class_data.class_days.tuesday_from">
                <option value="09:00:00">9:00 am</option>
            </select>
        </li>
        <li>to</li>
        <li>
             <select class="form-control" ng-model="add_class.class_data.class_days.tuesday_to">
                <option value="10:00:00">10:00 am</option>
            </select>
        </li>
    </ul>
    <ul class="list-inline ul_block_bottomline">
        <li class="width_25">Wednesday</li>
        <li> <select class="form-control" ng-model="add_class.class_data.class_days.wednesday_from">
                <option value="09:00:00">9:00 am</option>
            </select>
        </li>
        <li>to</li>
        <li>
             <select class="form-control" ng-model="add_class.class_data.class_days.wednesday_to">
                <option value="10:00:00">10:00 am</option>
            </select>
        </li>
    </ul>
    <ul class="list-inline ul_block_bottomline">
        <li class="width_25">Thursday</li>
        <li> <select class="form-control" ng-model="add_class.class_data.class_days.thursday_start">
                <option value="09:00:00">9:00 am</option>
            </select>
        </li>
        <li>to</li>
        <li>
             <select class="form-control" ng-model="add_class.class_data.class_days.thursday_to">
                <option value="10:00:00">10:00 am</option>
            </select>
        </li>
    </ul>
    <ul class="list-inline ul_block_bottomline">
        <li class="width_25">Friday</li>
        <li> <select class="form-control" ng-model="add_class.class_data.class_days.friday_start">
                <option value="09:00:00">9:00 am</option>
            </select>
        </li>
        <li>to</li>
        <li>
             <select class="form-control" ng-model="add_class.class_data.class_days.friday_to">
                <option value="10:00:00">10:00 am</option>
            </select>
        </li>
    </ul>
    <ul class="list-inline ul_block_bottomline">
        <li class="width_25">Saturday</li>
        <li> <select class="form-control" ng-model="add_class.class_data.class_days.saturday_start">
                <option value="09:00:00">9:00 am</option>
                <option value="null">OFF</option>
            </select>
        </li>
        <li>to</li>
        <li>
             <select class="form-control" ng-model="add_class.class_data.class_days.saturday_end">
                <option value="10:00:00">10:00 am</option>
            </select>
        </li>
    </ul>
    <ul class="list-inline ul_block_bottomline">
        <li class="width_25">Sunday</li>
        <li> <select class="form-control" ng-model="add_class.class_data.class_days.sunday_start">
                <option value="09:00:00">9:00 am</option>
                <option value="null">OFF</option>
            </select>
        </li>
        <li>to</li>
        <li>
             <select class="form-control" ng-model="add_class.class_data.class_days.sunday_end">
                <option value="10:00:00">10:00 am</option>
            </select>
        </li>
    </ul>
</div>