<!DOCTYPE html>
<html lang="en">
<head><title>
	<?php
	$login_page_title = "Health &amp; Wellness App";
	if(\Session::exists('login_page_title')){
		$login_page_title = \Session::get('login_page_title');
	}

	echo $login_page_title;
	?>
	</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">


<!-- For DropdownMenu Header -->

{!! HTML::style("css/font-awesome.min.css") !!}
{!! HTML::style("css/bootstrap.min.css") !!}
{!! HTML::style("css/bootstrap-datepicker.min.css") !!}
{!! HTML::style("css/vitals.css") !!}
{!! HTML::style("css/loader.css") !!}

{!! HTML::style("wp-css/formidablepro.css") !!}
{!! HTML::style("wp-css/bootstrap-theme.min.css") !!}
{!! HTML::style("wp-css/animate.css") !!}
{!! HTML::style("wp-css/flexslider.css") !!}
{!! HTML::style("wp-css/select2.min.css") !!}
{!! HTML::style("wp-css/print.css") !!}
{!! HTML::style("wp-fonts/helvetica/fonts.css") !!}

{!! HTML::style("wp-css/main.css") !!}


{!! HTML::style("wp-style.css") !!}



<script type="text/javascript">
	var Auth0Domain ="{{{config('laravel-auth0.domain')}}}";
	var Auth0ClientID ="{{{config('laravel-auth0.client_id')}}}";
	var Auth0Connection ="Username-Password-Authentication";
	var Auth0CallbackUrl ="{{{config('laravel-auth0.redirect_uri')}}}";
	var Auth0State = "<?php
													$urlStr = str_replace(url('/'), '', url()->previous());
													if($urlStr=="/auth/login"){
														echo 'dashboard';
													}
													elseif($urlStr=="/"){
														echo '/';
													}
													elseif(strpos($urlStr, '/')==0){
    												echo substr($urlStr,1);
													}
													else{
														echo 'dashboard';
													}
									?>";
</script>
	 <style>
		.text-box{   background-color: #fff;
		    border: 1px solid #eceded;
		    border-radius: 3px;
		    display: block;
		    font-size: 12px;
		    height: 51px;
		    padding: 0 0 0 30px;
		    width: 100%;
		  }
		.text-error{
		  color: #d41c20 !important;
		}
		.admin-login-div{
			padding-top: 250px !important;
			padding-bottom: 66px !important;
		}
		#innerpage.login-healthapp{
			background-position: center -70px !important;
    		padding: 62px 0 80px !important;
		}
	</style>
</head>
<!--  <body id="login-page"  ng-app="authLogin">  -->
 	<body id="login-page"  ng-app="authLogin" class="page page-id-1639 page-child parent-pageid-1150 page-template page-template-template-login-healthandwellnessapp page-template-template-login-healthandwellnessapp-php">



 	<!-- Second container for Login Page -->
	@yield('content')

 	<!-- End of Eight container for the Download the App -->


 	<!-- Eleven and Last Container for Footer -->
	@include('inc.footer')
 	<!-- End of Eleven and Last Container for Footer -->


  <!-- Here start the Jquery or javascript external file call -->

  <!-- jquery script for hide submenu in small screen and work by click -->

	{!! HTML::script("wp-js/vendor/modernizr-2.8.3.min.js") !!}
	{!! HTML::script("wp-js/vendor/jquery-1.11.3.min.js") !!}


	{!! HTML::script("wp-js/vendor/jquery.flexslider-min.js") !!}
	{!! HTML::script("wp-js/vendor/select2.min.js") !!}
	{!! HTML::script("wp-js/vendor/wow.min.js") !!}
	{!! HTML::script("wp-js/vendor/jquery.vmap.min.js") !!}
	{!! HTML::script("wp-js/vendor/jquery.carouFredSel-6.2.1-packed.js") !!}
	{!! HTML::script("wp-js/vendor/bootstrap.min.js") !!}
	{!! HTML::script("wp-js/main.js") !!}
	{!! HTML::script("wp-js/vendor/jquery.vmap.usa.js") !!}
	{!! HTML::script("js/vendor/moment-with-locales.js") !!}
	<!-- {!! HTML::script("js/vendor/bootstrap-datepicker.js") !!} -->
	{!! HTML::script("js/vendor/angular.min.js") !!}
  	<script src="https://cdn.auth0.com/js/auth0/9.5.1/auth0.min.js"></script>
	<!-- {!! HTML::script("js/vendor/angular-storage.js") !!}
	{!! HTML::script("js/vendor/angular-jwt.js") !!} -->
	{!! HTML::script("js/vendor/angular-route.min.js") !!}
	{!! HTML::script("js/vendor/angular-cookies.min.js") !!}
	<!-- {!! HTML::script("js/auth0/auth0App.js") !!} -->
	{!! HTML::script("js/app.services.js") !!}
	{!! HTML::script("js/services/storeData.fct.js") !!}
	{!! HTML::script("js/services/getData.fct.js") !!}
	{!! HTML::script("js/auth0/AuthController.js") !!}
	{!! HTML::script("js/directives/passwordVerify.js") !!}
	{!! HTML::script("js/directives/showMessageOnTab.js") !!}


 	 <script>
		/*$(document).ready(function(){
				$('.nav_bar').click(function(){
					$('.navigation').toggleClass('visible');
		      $('body').toggleClass('opacity');
		    });


		});*/

		// $(window).scroll(function() {
		// 		if ($(this).scrollTop() > 1){
		// 			$('header').addClass("sticky");
		// 		}
		// 		else{
		// 			$('header').removeClass("sticky");
		// 		}
		// });
		$(document).ready(function(){
		//$("#register_email").focus();
	});
	</script>

	@yield('inline_scripts')
 </body>
</html>
