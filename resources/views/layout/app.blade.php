@include('inc.template_start')

@include('inc.page_head')
@include('inc.admin-header')
<style type="text/css">
	.no_radius
	{
		border-radius: 0px !important;
	}

	.admin-footer{
		padding: 10px !important;
		background: #F9FAFC;
	}


	/**
 * The dnd-list should always have a min-height,
 * otherwise you can't drop to it once it's empty
 */
.simpleDemo ul[dnd-list] {
    min-height: 42px;
    padding-left: 0px;
}

/**
 * The dndDraggingSource class will be applied to
 * the source element of a drag operation. It makes
 * sense to hide it to give the user the feeling
 * that he's actually moving it.
 */
.simpleDemo ul[dnd-list] .dndDraggingSource {
    display: none;
}

/**
 * An element with .dndPlaceholder class will be
 * added to the dnd-list while the user is dragging
 * over it.
 */
.simpleDemo ul[dnd-list] .dndPlaceholder {
    background-color: #ddd;
    display: block;
    min-height: 42px;
}

.simpleDemo ul[dnd-list] li {
    background-color: #fff;
  /*  border: 1px solid #ddd;*/
    border-top-right-radius: 4px;
    border-top-left-radius: 4px;
    display: block;
    padding: 10px 15px;
    margin-bottom: -1px;
}

/**
 * Show selected elements in green
 */
.simpleDemo ul[dnd-list] li.selected {
    background-color: #dbe1e8;
    color: #3c763d;
}

</style>
<!-- Page content -->
<div id="mainpanel">

  <!-- IF PAGE LIG IN / SIGN UP -->
  <div id="" class="">
  		@include('inc.ineer-app-menu')				
		<div id="page-content">

		      @yield('content')
		</div>
		<!-- END Page Content -->

	</div>
</div>
@include('inc.page_footer')
@include('inc.template_scripts')
@yield('page_scripts')
<script type="text/javascript">
	$( document ).ready(function() {
  		@yield('jquery_ready')
	});
</script>
<script>
	
	$(document).ready(function(){
		
		$('.nav_bar').click(function(){
			$('.navigation').toggleClass('visible');
	      	$('body').toggleClass('opacity');
	    });
	    // for hide in login
	    $('.hideafterlogin').hide();

	});

	// $(window).scroll(function(event) {
	// 	var j = jQuery.noConflict();
	// 	if (j(this).scrollTop() > 1) {  
	// 		j('header').addClass("sticky");
	// 	}
	// 	else {
	// 		j('header').removeClass("sticky");
	// 	}
	// });

</script>
@yield('inline_scripts')

@include('inc.template_end')
