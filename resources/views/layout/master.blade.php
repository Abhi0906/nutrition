<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
      <meta charset="utf-8">

      <title>@yield('title')</title>

      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="robots" content="">

      <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
      <meta name="csrf-token" content="{!! csrf_token() !!}">

     <!-- Stylesheets -->
      {!! HTML::style("css/bootstrap.min.css") !!}

      <!-- {!! HTML::style("wp-css/formidablepro.css") !!} -->
      <!-- {!! HTML::style("wp-css/bootstrap-theme.min.css") !!} -->
      <!-- {!! HTML::style("wp-css/animate.css") !!} -->
      <!-- {!! HTML::style("wp-css/flexslider.css") !!} -->
      <!-- {!! HTML::style("wp-css/select2.min.css") !!} -->
      <!-- {!! HTML::style("wp-css/print.css") !!} -->
      <!-- {!! HTML::style("wp-fonts/helvetica/fonts.css") !!} -->
      {!! HTML::style('css/style.css') !!}
      {!! HTML::style('css/responsive2.css') !!}
      {!! HTML::style('css/animate.css') !!}
      {!! HTML::style('sticky-menu/core.css') !!}
      {!! HTML::style("wp-css/font-awesome.min.css") !!}

      {!! HTML::style("wp-css/main.css") !!}

      {!! HTML::style("css/responsive.css") !!}
      {!! HTML::style("css/responsive_extended.css") !!}

      {!! HTML::style("css/custom-genemedics.css") !!}
      {!! HTML::style("css/fullcalendar.min.css") !!}
      {!! HTML::style("css/jquery-ui.min.css") !!}

      {!! HTML::style("css/jquery-gauge.css") !!}

      {!! HTML::style("css/bootstrap-datepicker.css") !!}

      {!! HTML::style("css/footable.bootstrap.css") !!}
      {!! HTML::style("css/sweetalert.css") !!}
      {!! HTML::style("css/angular_calender.css") !!}
      {!! HTML::style("css/xeditable.css") !!}

      {!! HTML::style("css/selectric.css") !!}

      @yield('css-link')
      @yield('custom-css')

      <!-- END stylesheets -->
  </head>
  <body ng-app="wellnessApp" class="genemedics_wholebody">
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    @include('inc/fixed-head')

    <!-- Page main panel -->
    <div id="mainpanel" @if(Request::segment(1) == 'patient_info') class="margin_top_120 custom-mainpanel-wrapper" @else class="custom-mainpanel-wrapper" @endif>

      <!-- <div class="row">
        <div class="col-xs-12"> -->
          @yield('content')
        <!-- </div>
      </div> -->
    </div>   <!-- end Page main panel -->
    @include('inc/wp-footer')

    <div class="back-top">
    <i class="fa fa-chevron-up"></i>
    </div>

    {!! HTML::script("wp-js/vendor/modernizr-2.8.3.min.js") !!}
    {!! HTML::script("wp-js/vendor/jquery-1.11.3.min.js") !!}
    {!! HTML::script("js/footable.js") !!}


    {!! HTML::script("wp-js/vendor/jquery.flexslider-min.js") !!}
    {!! HTML::script("wp-js/vendor/select2.min.js") !!}
    {!! HTML::script("wp-js/vendor/wow.min.js") !!}
   <!--  {!! HTML::script("wp-js/vendor/jquery.vmap.min.js") !!} -->
   <!--  {!! HTML::script("wp-js/vendor/jquery.carouFredSel-6.2.1-packed.js") !!} -->
    {!! HTML::script("wp-js/vendor/bootstrap.min.js") !!}
    {!! HTML::script("wp-js/main.js") !!}
   <!--  {!! HTML::script("wp-js/vendor/jquery.vmap.usa.js") !!} -->
    {!! HTML::script("js/moment.js") !!}

    {!! HTML::script("js/jquery-gauge.min.js") !!}
    {!! HTML::script("js/bootstrap-datepicker.js") !!}
    <script src="https://cdn.auth0.com/js/auth0/9.5.1/auth0.min.js"></script>
    {!! HTML::script("js/sweetalert-dev.js") !!}
    {!! HTML::script("js/front_common.js") !!}

    {!! HTML::script("js/vendor/angular.min.js") !!}
    {!! HTML::script("js/ui-bootstrap-tpls-1.3.3.min.js") !!}

    {!! HTML::script("js/vendor/angular-animate.min.js") !!}
    {!! HTML::script("js/vendor/angular-sanitize.min.js") !!}
    {!! HTML::script("js/angular-ui-router.min.js") !!}


    {!! HTML::script("js/front_app.js") !!}
    {!! HTML::script("js/front.app.core.js") !!}
    {!! HTML::script("js/front.app.routes.js") !!}
    {!! HTML::script("js/app.services.js") !!}


    <!-- Services -->
    {!! HTML::script("js/services/getData.fct.js") !!}
    {!! HTML::script("js/services/storeData.fct.js") !!}
    {!! HTML::script("js/services/utility.fct.js") !!}
      <!-- Controllers -->

     {!! HTML::script("js/directives/elif.js") !!}
     {!! HTML::script("js/directives/calendar.js") !!}
     {!! HTML::script("js/fullcalendar.js") !!}
     {!! HTML::script("js/directives/xeditable.js") !!}
     {!! HTML::script("js/filters/dateFormatFilter.js") !!}
     {!! HTML::script("js/datetime-picker.min.js") !!}

     {!! HTML::script("js/jquery.selectric.js") !!}


     @yield('js')

    @yield('custom-js')
    <script type="text/javascript">
      jQuery(function($) {
        $('.navbar .dropdown').hover(function() {
          $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();
        }, function() {
          $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();
        });
        $('.navbar .dropdown > a').click(function(){
          location.href = this.href;
        });
      });
      /*$(window).scroll(function() {
          var scroll = $(window).scrollTop();
          if($(window).width()>1199){
            if (scroll > 10) {
                $("#header").addClass("header-top-fixed-menu");

                if($('#scroll_tabs_dash').length>0){
                  @if(Request::segment(1) == 'patient_info')
                    $("#mainpanel").addClass("mainpanel-scroll-0");
                  @else
                    $("#mainpanel").addClass("mainpanel-scroll");
                  @endif

                  $("#calorie_log_div").addClass("content-start-after-scroll");
                }
                else{
                  $("#mainpanel").addClass("mainpanel-scroll-other");
                }


            } else {
                $("#header").removeClass("header-top-fixed-menu");
                if($('#scroll_tabs_dash').length>0){
                  $("#mainpanel").removeClass("mainpanel-scroll");
                  $("#mainpanel").removeClass("mainpanel-scroll-0");
                  $("#calorie_log_div").removeClass("content-start-after-scroll");
                }
                else{
                  $("#mainpanel").removeClass("mainpanel-scroll-other");
                }
            }
          }

      });

      $(function () {
          setNavigation();
      });

      function setNavigation() {

          var path = window.location.pathname;
          path = path.replace(/\/$/, "");
          path = decodeURIComponent(path);

          $(".patient-login li a").each(function () {
              var href = $(this).attr('href');

              if (typeof href != "undefined" &&  path.substring(0, href.length) === href) {
                  $(this).closest('li').addClass('current-menu-item');
              }
          });
      }*/
    </script>
    <script src="{{ asset('js/vendor/bootstrap.min.js') }}" defer></script>
    <script src="{{ asset('sticky-menu/core.js') }}" defer></script>
    <script src="{{ asset('js/vendor/viewportchecker.js') }}" defer></script>
    <script src="{{ asset('js/vendor/index.js') }}" defer></script>
    <script>
      //nav submenu
       $('ul.nav li.menu-item-has-children').hover(function () {
          $(this).find('.sub-menu').stop(true, true).delay(200).fadeIn(500);
      }, function () {
          $(this).find('.sub-menu').stop(true, true).delay(200).fadeOut(500);
      });
    </script>
    <!-- Top tab js -->
    <!--sticky menu-->
    <script type="text/javascript">
        $(document).ready(function () {
          $(function(){
            var getLocation = function(href) {
              var l = document.createElement("a");
              l.href = href;
              return l;
            };
            var current = location.pathname;
            $('#menu-header-menu li a').each(function(){
              var $this = $(this);
              var curUrl = getLocation($this.attr('href'));
              if((current=="/weight" && $this.parent().attr('id')=="menu-item-32822")||(current=="/sleep" && $this.parent().attr('id')=="menu-item-324")||(current=="/bp_pulse" && $this.parent().attr('id')=="menu-item-529")||(current=="/reports" && $this.parent().attr('id')=="menu-item-1206")||(current=="/deals_list_front" && $this.parent().attr('id')=="menu-item-530")||(current=="/challenges_list_front" && $this.parent().attr('id')=="menu-item-525")||(current=="/event" && $this.parent().attr('id')=="menu-item-526")||(current=="/my_plans" && $this.parent().attr('id')=="menu-item-528")){
                if($('#dynamicLI').text()==""){
                  document.getElementById("menu-item-2232").style.display = "block";
                  if(current=="/bp_pulse"){
                      $('#dynamicLI').text('BP & PULSE');
                  }else if (current=='/deals_list_front') {
                    $('#dynamicLI').text('DEALS');
                  }else if (current=='/challenges_list_front') {
                    $('#dynamicLI').text('CHALLENGES');
                  }else if (current=='/my_plans') {
                    $('#dynamicLI').text('MY PLANS');
                  }else{
                    $('#dynamicLI').text((current.split('/')[1]).toUpperCase());
                  }
                  $("#dynamicLI").attr("href", location);
                  document.getElementById($this.parent().attr('id')).style.display = "none";
                }
              }
            });
            $('#menu-header-menu li a').each(function(){
              var $this = $(this);
              // if the current path is like this link, make it active
              var curUrl = getLocation($this.attr('href'));
              if($this.text()!=='MORE...'){
                if(curUrl.pathname == current){
                  $this.addClass('menuactive');
                }
              }
            })
          });

          jQuery('.menu-item-has-children span.arrow').click(function () {
            jQuery(this).parent().siblings().removeClass('open');
          	jQuery(this).parent().toggleClass('open');
          });
            /*menu?*/
            var trigger = $('.hamburger'),
             overlay = $('.overlay'),
            isClosed = false;

            trigger.click(function () {
                hamburger_cross();
            });

            function hamburger_cross() {
                if (isClosed == true) {
                    overlay.hide();
                    trigger.removeClass('is-open');
                    trigger.addClass('is-closed');
                    isClosed = false;
                } else {
                    overlay.show();
                    trigger.removeClass('is-closed');
                    trigger.addClass('is-open');
                    isClosed = true;
                }
            }

            $('[data-toggle="offcanvas"]').click(function () {
                $('#wrapper').toggleClass('toggled');
            });

            /*menu?*/
            $(window).scroll(function () {
                if ($(window).width() > 500) {
                    if ($(this).scrollTop() > 10) {
                        $('#trueHeader').css("box-shadow", "0 4px 10px 0 rgba(165, 165, 165, 0.2), 0 5px 15px 0 rgba(220, 220, 220, 0.19)");
                        $('#truenav').addClass("stickyheader");
                    }
                    if ($(this).scrollTop() < 5) {
                        $('#trueHeader').css("box-shadow", "none");
                        $('#truenav').removeClass("stickyheader");
                    }
                }
            });

            // Iterate over each select element

        });
    </script>
    <!--sticky menu-->
    <!-- Scroll up js -->
    <script type='text/javascript'>
        $(document).ready(function () {
            /*$(window).scroll(function () {
                if ($(this).scrollTop() > 100) {
                    $('#scroll').fadeIn();
                } else {
                    $('#scroll').fadeOut();
                }
            });
            $('#scroll').click(function () {
                $("html, body").animate({ scrollTop: 0 }, 600);
                return false;
            });*/
            $('.img-thumb').addClass("hidden1").viewportChecker({
                classToAdd: 'visible animated bounceInUp', // Class to add to the elements when they are visible
                offset: 100
            });
            $('.in-up').addClass("hidden1").viewportChecker({
                classToAdd: 'visible animated bounceInDown', // Class to add to the elements when they are visible
                offset: 100
            });
            $('.title-ani').addClass("hidden1").viewportChecker({
                classToAdd: 'visible animated fadeIn', // Class to add to the elements when they are visible
                offset: 100
            });
            jQuery('.div-ani1').addClass("hidden1").viewportChecker({
                classToAdd: 'visible animated bounceInRight', // Class to add to the elements when they are visible
                offset: 100
            });
            jQuery('.div-ani').addClass("hidden1").viewportChecker({
                classToAdd: 'visible animated bounceInLeft', // Class to add to the elements when they are visible
                offset: 100
            });
            jQuery('.post').addClass("hidden1").viewportChecker({
                classToAdd: 'visible animated fadeInDown', // Class to add to the elements when they are visible
                offset: 100
            });
            jQuery('.testimonial-ani').addClass("hidden1").viewportChecker({
                classToAdd: 'visible animated flipInX', // Class to add to the elements when they are visible
                offset: 100
            });
        });
    </script>
  </body>
</html>