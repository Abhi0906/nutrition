
@if (session()->has('flash_message'))
    <script language="javascript">
        showFlashMessage("{{ session('flash_message.title') }}",
                '{{ session('flash_message.message') }}',
                '{{ session('flash_message.level') }}');
    </script>
@endif


@if (session()->has('flash_message_overlay'))
    <script language="javascript">
        showFlashMessageOverlay("{{ session('flash_message_overlay.title') }}",
                '{{ session('flash_message_overlay.message') }}',
                '{{ session('flash_message_overlay.level') }}');
    </script>
@endif
