@extends('layout.master')

@section('title')
  Home
@endsection
@section('content')
            <div class="helth-app-banner">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-12 in-up animated3"><img src="images/mob-app.png" class="mob-center" alt="" title=""></div>
                                    <div class="col-md-8 col-sm-8 col-xs-12 pad-top-header">
                                        <div class="banner_animate">
                                            <h1 class="p-0">The Ultimate, One-of-a-Kind Health & Wellness App has Arrived!</h1>
                                        </div>
                                        <div class="arrived img-thumb animated3 ">
                                            <ul>
                                                <li class="icon1">
                                                    <b>Your Personal Trainer – Professional Exercise Videos</b>
                                                    <p>Our app is the only app with the comprehensive Genemedics® exercise video library built right into the app. </p>
                                                </li>
                                                <li class="icon2">
                                                    <b>Customized and Monitored Programs by Medical Doctor/Lifestyle Coach</b>
                                                    <p>We closely monitor your nutrition, supplement, & exercise progress on your journey to a healthier, better you! </p>
                                                </li>
                                                <li class="icon3">
                                                    <b>Video with Screen Sharing</b>
                                                    <p>Staying connected to your support team makes it easier to achieve your fitness dreams! </p>
                                                </li>
                                                <li class="icon4">
                                                    <b>Community</b>
                                                    <p>Get connected. Get motivated. And get inspired!  </p>
                                                </li>
                                                <li class="icon5">
                                                    <b>Food Log with Scanner</b>
                                                    <p>Our huge library gives you greater accuracy and control over your diet than almost any other health app.  </p>
                                                </li>
                                                <li class="icon6">
                                                    <b>Exercise Log</b>
                                                    <p>Track your calories burned and fitness goals while integrating your fitness program into your nutrition plan. </p>
                                                </li>
                                                <li class="icon7">
                                                    <b>Reminders to Keep you on Track</b>
                                                    <p>Keeps track of your supplement and medication schedule with helpful reminders so you’ll never fall off track. </p>
                                                </li>
                                                <li class="icon8">
                                                    <b>GPS – Track Steps and Distance</b>
                                                    <p>Our app makes it easy to track your steps per day and figure out how much distance you’re covering.  </p>
                                                </li>
                                                <li class="icon9">
                                                    <b>Goal Tracking</b>
                                                    <p>Set and track goals for blood pressure, fat loss, body fat percentage, muscle gains & Much more..  </p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- content start here -->

                        <div class="blue-bg">
                            <div class="container">
                                <div class="row ">
                                    <div class="col-md-offset-3 col-md-4 col-sm-6 text-center">
                                        <p>Download the app</p>
                                    </div>
                                    <div class="col-md-5 col-sm-4">
                                        <a target="_blank" href="https://itunes.apple.com/in/app/genemedicshealth-health-tracker/id1061451973"><img src="images/app-store.png" alt="" title="" class="app-store" /></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- banner start here -->
                        <div class="fitness-watch-banner">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-8 pad-top-header">
                                        <div class="banner_animate">
                                            <h3 class="p-0 wireless-title">Wirelessly Syncs With Fitness Watches</h3>
                                        </div>
                                        <div class="wireless-fitness">
                                            <ul>
                                                <li><a href="#">Apple Watch</a></li>
                                                <li><a href="#">Fitbit</a></li>
                                                <li><a href="#">Jawbone</a></li>
                                                <li><a href="#">Garmin</a></li>
                                                <li><a href="#">Withings</a></li>
                                                <li><a href="#">And many others!</a></li>
                                            </ul>
                                        </div>
                                        <div class="watch img-thumb animated2">
                                            <ul>
                                                <li class="icon10">
                                                    <b>Heart Rate</b>
                                                    <p>Track your heart rate using data from the Apple Watch heart rate sensor. You’ll also get real-time stats like elevation change, average speed, and distance covered. </p>
                                                </li>
                                                <li class="icon11">
                                                    <b>Calories Burned</b>
                                                    <p>When you’re doing yoga using your iPhone, Pocket Yoga can show you a picture of the current pose, pose name, time remaining, and calories burned. </p>
                                                </li>
                                                <li class="icon12">
                                                    <b>Steps Taken & Distance Travelled</b>
                                                    <p>Whether you’re walking, running, cycling, or using the most popular fitness equipment, Apple Watch knows how to keep you motivated.  </p>
                                                </li>
                                                <li class="icon13">
                                                    <b>Active Minutes</b>
                                                    <p>With multi-sport functionality and SmartTrack™ automatic exercise recognition, Fitbit Surge lets you track every aspect of your training. Use multi-sport modes for the most precise tracking during workouts and real-time stats on display.  </p>
                                                </li>
                                                <li class="icon14">
                                                    <b>Hours Slept & Quality of Sleep</b>
                                                    <p>Make the most of your day by getting more out of your night. Surge automatically monitors how long and how well you sleep, syncs those stats to your smartphone and computer, and displays your trends as detailed charts on the Fitbit dashboard.   </p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--mob-->

                        <div class="white-bg">
                            <div class="container padd-50">
                                <div class="container">
                                    <div class="row">
                                        <div class="padd-20">
                                            <h3 class="wireless-title title-ani text-center">Wirelessly Syncs With Body Scales</h3>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 col-xs-12 text-center div-ani animated2 with-body">
                                            <h3><b>Weight, BMI & Body Fat Percentage</b></h3>
                                            <p>Get high-accuracy weight and body fat measurements, as well as your Body Mass Index. Pick the right body type to optimize the fat mass measurement — if you exercise vigorously on a regular basis, switch to "Athlete mode".</p>
                                            <h3><b>Heart Rate</b></h3>
                                            <p>The Smart Body Analyzer takes your pulse everytime you weigh yourself. An effortless and ingenious way to give you the information you need to make the right decisions for your health.</p>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 text-center img-thumb animated2">
                                            <img src="images/withings.jpg" alt="" title="" />
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 text-center div-ani1 animated2 with-body">
                                            <h3><b>Charts & Graphs</b></h3>
                                            <p>Displays weight stats and progress trends in easy-to-read charts & graphs. With your history available 24/7, you can see what works, reinforce positive behaviors and stay focused on the big picture.</p>
                                            <h3><b>Air Quality Screening</b></h3>
                                            <p>As the scale checks your bedroom indoor air quality through temperature and carbon dioxide measuring, the app builds a CO2-level graph with the important thresholds and lets you know when to clear the air.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- banner start here -->
                        <div class="blood-presser-banner">
                            <div class="container">
                                <div class="row">
                                    <div class="banner_animate">
                                        <h3 class="wireless-title">Wirelessly Syncs With Blood Pressure Monitor</h3>
                                    </div>
                                    <div class="col-md-6 col-sm-4 div-ani animated2"><img src="images/blood-machine.png" class="mob-center" alt="" title=""></div>
                                    <div class="col-md-6 col-sm-8 pad-top-header">
                                        <div class="blood-presser div-ani1 animated2">
                                            <ul>
                                                <li class="icon15">
                                                    <b>Blood Pressure Monitoring Made Simple</b>
                                                    <p>Simply slip on the cuff, turn on the Wireless Blood Pressure Monitor and the Health Mate app will automatically launch. Following a brief set of instructions, you are ready to take your blood pressure. </p>
                                                </li>
                                                <li class="icon16">
                                                    <b>Instant Feedback</b>
                                                    <p>The app gives you instant color coded feedback based on the ESH (European Society of Hypertension) and the AHA (American Heart Association) recommendations for hypertension. </p>
                                                </li>
                                                <li class="icon17">
                                                    <b>Your History, On Hand</b>
                                                    <p>Because tracking over time helps you better understand heart health, the Health Mate app stores all your BP readings, syncs with the Withings Health Cloud and creates an easy-to-understand chart.</p>
                                                </li>
                                                <li class="icon18">
                                                    <b>Medically Approved</b>
                                                    <p>The Wireless Blood Pressure Monitor's results have scientific value: It has received clearance from the Food and Drug Administration (FDA) in the USA and is compliant with European medical device regulations. It is also medically approved in Canada, Australia and New Zealand.</p>
                                                </li>
                                                <li class="icon19">
                                                    <b>Keep your Doctor in the Loop</b>
                                                    <p>Use the Wireless Blood Pressure Monitor for prevention or to check the efficacy of your treatment. Either way, a few taps on your smartphone is all it takes to share measurements with your doctor.  </p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="white-bg" id="downloadapp">
                        		<div class="container padd-50">
                        				<div class="col-md-6 col-sm-6 app_section">
                        					<h3 class="wireless-title">Your Virtual Health Assistant</h3>
                        					<div class="mob-gen"> <p class="takes">Genemedics Health app takes care of Daily Routine, Meals, Exercises and Much more...</p></div>

                        					<div class="smsform">
                        					  <form method="POST" action="{{ url('/sendMessage') }}" name="SMS" enctype="multipart/form-data ">
                                    {{ csrf_field() }}
                        						<input name="phone" class="form-control pull-left" placeholder="Mobile number" required="" type="tel">
                        						<button type="submit" name="submit" class="send-btn">Text me the app</button>
                        						</form>
                        					</div>
                        					<div class="row pad30" ><a target="_blank" href="https://itunes.apple.com/in/app/genemedicshealth-health-tracker/id1061451973"><img src="images/app-store.png" alt="" title="" class="app-store app-bhn-m-l"></a></div>
                        				</div>

                        				<div class="col-md-6 col-sm-6 post">
                        					<img class="mob-img"src="images/mob-1-1.png" alt="" title="">
                        				</div>
                        		</div>
                        	</div>
                          @endsection
