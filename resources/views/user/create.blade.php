@extends('layout.app')
@section('content')
    <!-- Forms General Header -->
    <!--  @include('user.partials.registration_form_header_partial')
    <!-- END Forms General Header -->
    <form enctype="multipart/form-data" method="post" action="/admin/{!! $user_type !!}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        @include('user.partials.registration_form_partial')
    </form>

@endsection