<?php /* <style type="text/css">
  .content-header-media {
      height: 125px !important;
  }
</style>
<div class="content-header content-header-media genemedics-content-header-border">
    <div class="patients-head header-section genemedics-content-header">
       	@if(isset($user->photo) && $user->photo != "")
       		 {!! HTML::image("uploads/".$user->photo,"avatar", array('class' => 'img-circle pull-right','id'=>'user_profle_pic','width'=>'64','height'=>'64')) !!} 
        @else
            {!! HTML::image("uploads/male.jpg","avatar", array('class' => 'img-circle pull-right','id'=>'user_profle_pic','width'=>'64','height'=>'64')) !!} 

        @endif
        <h1>
             @if($user_type == 'patient')
               {!! isset($user->full_name) ? $user->full_name : '' !!}
             @elseif($user_type == 'doctor')
               {!! isset($user->name_with_title)? $user->name_with_title  :''!!}
             @endif

        <br><small>Health &amp; Wellness</small></h1>
    </div>
        
</div> /* ?>
<!-- <ul class="breadcrumb breadcrumb-top">
     <li><a href="/{!! $user_type !!}">{!! ucfirst($user_type) !!}s</a></li>
    <li>Demographics</li>
</ul> -->