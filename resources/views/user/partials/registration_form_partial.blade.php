
<!-- Demographics details start -->
<div class="row block-title patient_title">
    <div class="col-sm-2">
        <h3><strong> &nbsp; <!--  @{{edit_exercise.selected.name | uppercase}}  --> Create {{ucfirst($user_type) }} </strong></h3>
    </div>
    <div class="col-sm-10 text-alignRight">
        <h3>
            <div class="form-group">
                <button class="btn btn-default" type="submit">
                        <i class="fa gi gi-disk_saved"></i> Save & Close
                </button>
                @if($user_type == "patient")
                        <a href="/admin/patient" class="btn btn-default"> <i class="gi gi-remove_2"></i>
                         &nbsp;Cancel</a>
                <!-- <button class="btn btn-default" type="reset">
                            <i class="fa fa-repeat"></i> Reset
                 </button> -->

                @endif
                @if($user_type == "doctor")
                        <a href="/admin/doctor" class="btn btn-default"> <i class="gi gi-remove_2"></i>
                        &nbsp;Cancel</a>
                @endif
            </div>
        </h3>
      </div>
</div>
@include('errors.error')
    <div class="row">
        <div class="col-md-12 col-lg-6">
            <!-- Account Form Block -->
            <div class="block">
                <!-- Horizontal Form Title -->
                <div class="block-title">
                   <!--  <div class="block-options pull-right">
                        <a title="" data-toggle="button" class="btn btn-alt btn-sm btn-default toggle-bordered enable-tooltip" href="javascript:void(0)" data-original-title="Toggles .form-bordered class">No Borders</a>
                    </div> -->
                    <h2><strong>Account</strong></h2>
                </div>
                <!-- END Horizontal Form Title -->

                <!-- Horizontal Form Content -->
                <div class="form-horizontal form-bordered">

                  <!--   <div class="form-group">
                        {!! Form::label('txt_username', 'Username',array('class'=>'col-md-3 control-label')) !!}                        <div class="col-md-9">
                            {!! Form::text('username', null, array('class' => 'form-control','placeholder'=>"Username",'id'=>'txt_username','maxlength'=>'50')) !!}
                            <span class="help-block">Please enter your username</span>
                        </div>
                    </div>   -->

                    <div class="form-group">
                        {!! Form::label('txt_email', 'Email',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            {!! Form::email('email',null, array('class' => 'form-control','placeholder'=>"Enter Email",'id'=>'txt_email','maxlength'=>'50')) !!}
                            <span class="help-block">Please enter your email</span>
                        </div>
                    </div>
                    @if($user_type == 'doctor')
                        <div class="form-group">
                            {!! Form::label('pwd_password', 'Password',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                {!! Form::password('password',array('class' => 'form-control','placeholder'=>"Password",'id'=>'pwd_password','maxlength'=>'50')) !!}
                                <span class="help-block">Please enter a complex password</span>
                            </div>
                        </div>
                     @endif
                    @if($user_type == 'patient')
                    <div class="form-group">
                        {!! HTML::decode(Form::label('txt_membership_code', 'Membership Code', array('class'=>'col-md-3 control-label'))) !!}
                        <div class="col-md-9">

                            {!! Form::text('membership_code',null, array('class' => 'form-control','placeholder'=>"Membership Code",'id'=>'txt_membership_code','maxlength'=>'50')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('txt_subscription', 'News Subscription',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                           {!! Form::checkbox('news_letter', 1, true) !!}
                        </div>
                    </div>
                    @endif
                    @if($user_type == 'doctor')
                    <div class="form-group">
                        {!! Form::label('select_title', 'Title',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            {!! Form::select('doctor_title',$doctor_titles
                                                 , Input::old('doctor_title'),array('class'=>'form-control','id'=>'select_title')) !!}
                        </div>
                    </div>
                    @endif
                </div>
                <!-- END Horizontal Form Content -->
            </div>
            <!-- END Account Form Block -->

             <!-- Personal Form Elements Block -->
             <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <!-- <div class="block-options pull-right">
                        <a title="" data-toggle="button" class="btn btn-alt btn-sm btn-default toggle-bordered enable-tooltip" href="javascript:void(0)" data-original-title="Toggles .form-bordered class">No Borders</a>
                    </div> -->
                    <h2><strong>Personal</strong> </h2>
                </div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <div class="form-horizontal form-bordered">

                     <div class="form-group">
                        {!! Form::label('txt_first_name', 'First name',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                           {!! Form::text('first_name',null, array('class' => 'form-control','placeholder'=>"First name",'id'=>'txt_first_name','maxlength'=>'30')) !!}
                            <span class="help-block">Please enter your first name</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('txt_middle_name', 'Middle Name',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                           {!! Form::text('middle_name',null, array('class' => 'form-control','placeholder'=>"Middle name",'id'=>'txt_middle_name','maxlength'=>'30')) !!}
                            <span class="help-block">Please enter your middle name</span>
                        </div>
                    </div>

                     <div class="form-group">
                        {!! Form::label('txt_last_name', 'Last Name',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            {!! Form::text('last_name',null, array('class' => 'form-control','placeholder'=>"Last name",'id'=>'txt_last_name','maxlength'=>'50')) !!}
                            <span class="help-block">Please enter your last name</span>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('gender', 'Gender',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            <label for="radio_male" class="radio-inline">
                                {!! Form::radio('gender', 'male', true, ['id' => 'radio_male']) !!}Male
                            </label>
                            <label for="radio_female" class="radio-inline">
                                {!! Form::radio('gender', 'female',false, ['id' => 'radio_female']) !!}Female
                           </label>

                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('select_template', 'Meal Template',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            <span id="dis_male_template">
                            {!! Form::select('meal_template_id',
                                             $meal_templates_male->toArray(), null ,array('class'=>'form-control','id'=>'meal_template_id')) !!}
                            </span>
                            <span id="dis_female_template">
                            {!! Form::select('meal_template_id',
                                             $meal_templates_female->toArray(), null ,array('class'=>'form-control','id'=>'meal_template_id')) !!}
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('txt_birth_date', 'Birth Date',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            {!! Form::text('birth_date',null, array('class' => 'form-control input-datepicker','placeholder'=>"MM dd, yyyy",'id'=>'txt_birth_date','readonly'=>'readonly','data-date-format'=>'MM dd, yyyy')) !!}
                            <span class="help-block">Please enter your birth date</span>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('select_status', 'Status',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                             {!! Form::select('status',
                                                 array('' => 'Please Status',
                                                       'Active' => 'Active',
                                                       'Inactive'=>'Inactive'
                                                 ),'Active' ,array('class'=>'form-control','id'=>'select_status')) !!}

                        </div>
                    </div>
                     @if($user_type == 'patient')
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="Weight">Weight</label>
                               <div class="col-md-9">
                                <div class="input-group">
                                   <input type="text" class="form-control" id="baseline_weight" name="baseline_weight" placeholder="Enter Weight" maxlength="5"
                                    ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" >
                                    <span class="input-group-btn">
                                       <span class="btn btn-primary">&nbsp;&nbsp;Lbs&nbsp;&nbsp;</span>
                                    </span>
                                    </div>
                                      <small>Enter numbers only</small>
                                </div>
                    </div>

                     <div class="form-group">
                                {!! Form::label('Height', 'Height',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-9">
                                 <div class="input-group">
                                    <input type="text"
                                    maxlength="5" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01"
                                     name="height_in_feet" placeholder="Enter feet" id="feet_height" class="form-control">

                                    <span class="input-group-btn">
                                      <span class="input-group-btn"><span class="btn btn-primary">&nbsp;&nbsp;Feet&nbsp;&nbsp;</span></span>
                                       </span>
                                      <input type="text"
                                       maxlength="5" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01"
                                       name="height_in_inch" placeholder="Enter inches"  id="inches_height" class="form-control">
                                      <span class="input-group-btn"><span class="btn btn-primary">&nbsp;&nbsp;Inches&nbsp;&nbsp;</span></span>
                                  </div>
                                       <!-- <span class="help-block">Enter numbers only </span>
                                       <span class="help-block">Enter numbers only </span> -->

                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-5">
                                               <p style="margin-left:5%; font-size: 13px;">Enter numbers only </p>
                                            </div>
                                            <div class="col-md-7">
                                               <p style="margin-left:8%; font-size: 13px;">Enter numbers only</p>
                                            </div>
                                        </div>
                                     </div>
                               </div>
                    </div>
                    @endif
                </div>
                <!-- END Basic Form Elements Content -->
             </div>
            <!-- END Personal Form Elements Block -->
        </div>
        <div class="col-md-12 col-lg-6">

           <div class="block full">
                <!-- Dropzone Title -->
                <div class="block-title">
                    <!-- <div class="block-options pull-right">
                        <a title="" data-toggle="tooltip" class="btn btn-alt btn-sm btn-default" href="javascript:void(0)" data-original-title="Settings"><i class="gi gi-cogwheel"></i></a>
                    </div> -->
                    <h2><strong>Photo</strong></h2>
                </div>
                <!-- END Dropzone Title -->

                <!-- Dropzone Content -->
                <!-- Dropzone.js, You can check out https://github.com/enyo/dropzone/wiki for usage examples -->
                <div class="dropzone"  id="profile_pic">
                     <div class="dz-default dz-message"><span>Drop file here to upload</span></div>
                     {!! Form::hidden('photo',null, array('id'=>'hnphoto')) !!}
                </div>
                <!-- END Dropzone Content -->
           </div>

             <!-- Contact Form Block -->
            <div class="block">
                <!-- Horizontal Form Title -->
                <div class="block-title">
                    <!-- <div class="block-options pull-right">
                        <a title="" data-toggle="button" class="btn btn-alt btn-sm btn-default toggle-bordered enable-tooltip" href="javascript:void(0)" data-original-title="Toggles .form-bordered class">No Borders</a>
                    </div> -->
                    <h2><strong>Contact</strong></h2>
                </div>
                <!-- END Horizontal Form Title -->

                <!-- Horizontal Form Content -->
                <div class="form-horizontal form-bordered">

                    <div class="form-group">
                        {!! Form::label('txt_phone', 'Phone',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            {!! Form::text('phone',null, array('class' => 'form-control','placeholder'=>"Enter Phone",'id'=>'txt_phone','maxlength'=>'15')) !!}
                            <span class="help-block">Please enter your phone</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('tex_address', 'Address',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            {!! Form::text('address',null, array('class' => 'form-control','placeholder'=>"Enter Address",'id'=>'tex_address','maxlength'=>'700')) !!}
                            <span class="help-block">Please enter your address</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('txt_city', 'City',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            {!! Form::text('city',null, array('class' => 'form-control','placeholder'=>"Enter City",'id'=>'txt_city','maxlength'=>'30')) !!}
                            <span class="help-block">Please enter your city</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('txt_state', 'State',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            {!! Form::text('state',null, array('class' => 'form-control','placeholder'=>"Enter State",'id'=>'txt_state','maxlength'=>'30')) !!}
                            <span class="help-block">Please enter your state</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('txt_country', 'Country',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            {!! Form::text('country',null, array('class' => 'form-control','placeholder'=>"Enter Country",'id'=>'txt_country','maxlength'=>'30')) !!}
                            <span class="help-block">Please enter your country</span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('txt_postal_code', 'Postal Code',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            {!! Form::text('post_code',null, array('class' => 'form-control','placeholder'=>"Enter Postal Code",'id'=>'txt_postal_code','maxlength'=>'15')) !!}
                            <span class="help-block">Please enter your Postal Code</span>
                        </div>
                    </div>

                </div>
                <!-- END Horizontal Form Content -->
            </div>
            <!-- END Contact Form Block -->
        </div>
    </div>
    <div class ="row">
        <div class="col-md-12">
            <div class="form-horizontal form-bordered">
                <div class="form-group form-actions">
                    <div class="col-md-12 col-md-offset-5">
                        <button class="btn btn-default" type="submit">
                            <i class="fa gi gi-disk_saved"></i> Submit
                        </button>
                        <button class="btn btn-default" type="reset">
                            <i class="fa fa-repeat"></i> Reset
                        </button>
                    </div>
                </div>
           </div>
        </div>
    </div>
<!-- Demographics details end -->
@section('jquery_ready')
    Dropzone.autoDiscover = false;
    $("div#profile_pic").dropzone({
                    url: "/api/v3/upload",
                    maxFiles: 1,
                    paramName:'user_photo',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    addRemoveLinks:true,
                    uploadMultiple:false,
                    init: function() {
                        this.on("complete", function(data) {
                                       if(data.status == 'success'){
                                         var response = eval('(' + data.xhr.response + ')');
                                         var destination_path = '/uploads/'+response.file_name;
                                         $('#user_profle_pic').attr('src',destination_path);
                                         $('#hnphoto').val(response.file_name);
                                       }

                        });
                        this.on("removedfile",function(file){
                             var file_name = file.name;

                             HTTPRequest('', 'get', '/api/v3/removephoto/'+file_name, function(data,status,xhr){
                                if(status == 'success'){
                                    if(data.error == false){

                                    }
                                }
                               });

                        });
                        this.on("addedfile", function() {
                          if (this.files[1]!=null){
                            this.removeFile(this.files[0]);
                           }
                        });

                   }

               });

    //Start: Display template according to meal template

    // fetch the id name of checked post-format
    var idName = $('input[type=radio][name=gender]:checked').attr('id');

    // display the metabox if the fetched value matched
        if ( idName === 'radio_male' ) {
            $('#dis_male_template').show();
            $('#dis_female_template').hide();

            $("#dis_female_template select").prop( "disabled", true );
            $("#dis_male_template select").prop( "disabled", false );
        }
        else{
            $('#dis_male_template').hide();
            $('#dis_female_template').show();

            $("#dis_male_template select").prop( "disabled", true );
            $("#dis_female_template select").prop( "disabled", false );
        }

        // show or hide
        $('input[type=radio][name=gender]').click(function(){
            idName = $(this).attr('id');
            if ( $(this).is(':checked') ) {
                if ( idName === 'radio_male' ) {
                    $('#dis_male_template').show();
                    $('#dis_female_template').hide();

                    $("#dis_female_template select").prop( "disabled", true );
                    $("#dis_male_template select").prop( "disabled", false );
                }
                else{
                    $('#dis_male_template').hide();
                    $('#dis_female_template').show();

                    $("#dis_male_template select").prop( "disabled", true );
                    $("#dis_female_template select").prop( "disabled", false );
                }
            }
        });
    //End: Display template according to meal template
@append