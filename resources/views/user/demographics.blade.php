

<!-- Demographics details start -->
<script type="text/ng-template" id="demographics" >
    @include('errors.error') 
    <form name="demographicsForm" novalidate="novalidate" class="animation-fadeInQuick">
<div class="row block-title patient_title">
    <div class="col-sm-5">
        <h3>
            <ul class="list-inline margin_bot_0">
                <li> @if(isset($user->photo) && $user->photo != "")
                 {!! HTML::image("uploads/".$user->photo,"avatar", array('class' => 'img-circle','id'=>'user_profle_pic','width'=>'40','height'=>'40')) !!} 
                @else
                    {!! HTML::image("uploads/male.jpg","avatar", array('class' => 'img-circle','id'=>'user_profle_pic','width'=>'40','height'=>'40')) !!} 

                @endif 
                </li>
                <li><strong> @if($user_type == 'patient')
                   Edit {!! isset($user->full_name) ? $user->full_name : '' !!}
                 @elseif($user_type == 'doctor')
                   Edit {!! isset($user->name_with_title)? $user->name_with_title  :''!!}
                 @endif</strong> 
                 </li>
            </ul>
           
        </h3>
    </div>  
    <div class="col-sm-7 text-alignRight">
        <h3>
            <div class="form-group">
                 <button class="btn btn-default" ng-click="demographics.updateUser(demographicsForm.$valid)">
                        <i class="fa gi gi-disk_saved"></i> Save & Close
                 </button>
                
                 <button class="btn btn-default">
                        <i class="fa gi gi-bin"></i> Delete
                 </button>
                 @if($user_type == "patient")
                        <a href="/patient" class="btn btn-default"> <i class="gi gi-remove_2"></i>
                         &nbsp;Cancel</a>
                  
                 @endif
                 @if($user_type == "doctor") 
                        <a href="/doctor" class="btn btn-default"> <i class="gi gi-remove_2"></i>
                        &nbsp;Cancel</a>
                 @endif
            </div>
        </h3>
  </div>
</div> 
    
        <div class="row">
        <div class="col-md-12 col-lg-6">
            <!-- Account Form Block -->
            <simple-block block-title="Account">
                <!-- Horizontal Form Content -->
                <div class="form-horizontal form-bordered">
                  
                    <div class="form-group" ng-class="{ 'has-error' : demographicsForm.username.$invalid && !demographicsForm.username.$pristine }">
                        {!! HTML::decode(Form::label('txt_username', 'Username<sup class="text-danger"></sup>', array('class'=>'col-md-3 control-label'))) !!}
                        <div class="col-md-9">
                            <input type="text" ng-model="demographics.user.username" name="username" maxlength="50" id="txt_username"  disabled placeholder="Username" class="form-control" ng-minlength="3"  required >
                            <p ng-show="demographicsForm.username.$invalid && !demographicsForm.username.$pristine" class="help-block">Your name is required Min. 3 characters required</p>
                            
                        </div>

                    </div>
                      @if($user_type == 'doctor')
                        <div class="form-group">
                            {!! Form::label('pwd_password', 'Password',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                <input type="password" ng-model="demographics.user.password" name="password" maxlength="30" id="pwd_password" placeholder="Password" class="form-control">
                                <span class="help-block">Please enter a new complex password</span>
                            </div>
                        </div>
                     @endif
                    <div class="form-group" ng-class="{ 'has-error' : demographicsForm.email.$invalid && !demographicsForm.email.$pristine }">
                        {!! HTML::decode(Form::label('txt_email', 'Email<sup class="text-danger">*</sup>', array('class'=>'col-md-3 control-label'))) !!}
                        <div class="col-md-9">
                            <input type="email" ng-model="demographics.user.email" name="email" maxlength="50" id="txt_email" placeholder="Enter Email" class="form-control" required>
                            <p ng-show="demographicsForm.email.$invalid && !demographicsForm.email.$pristine" class="help-block">Your email is required in proper format.</p>
                        </div>
                    </div>
                    @if($user_type == 'patient')
                    <div class="form-group">
                        {!! HTML::decode(Form::label('txt_membership_code', 'Membership Code', array('class'=>'col-md-3 control-label'))) !!}
                        <div class="col-md-9">
                            <input type="text" ng-model="demographics.user.membership_code" name="membership_code" maxlength="50" id="txt_membership_code" placeholder="Enter Membership Code" class="form-control">
                            
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('txt_subscription', 'News Subscription',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                        <input type="checkbox" ng-model="demographics.user.news_letter" ng-true-value=1 ng-false-value=0>
                        </div>
                    </div>
                    @endif
                    @if($user_type == 'doctor')
                    <div class="form-group">
                        {!! Form::label('select_title', 'Title',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">

                            <select name="doctor_title" ng-model="demographics.user.doctor_title" ng-options="title.name as title.name for title in demographics.doctor_titles" id="select_title" class="form-control">
                                <option value="">Please Status</option>
                            </select>
                           
                        </div>
                    </div>
                    @endif
                </div>
                <!-- END Horizontal Form Content -->
            </simple-block>    
            
            <!-- END Account Form Block -->

             <!-- Personal Form Elements Block -->
             <simple-block block-title="Personal">
             <!-- Basic Form Elements Content -->
                <div class="form-horizontal form-bordered">
                              
                    <div class="form-group" ng-class="{ 'has-error' : demographicsForm.first_name.$invalid && !demographicsForm.first_name.$pristine }">
            {!! HTML::decode(Form::label('txt_first_name', 'First Name<sup class="text-danger">*</sup>', array('class'=>'col-md-3 control-label'))) !!}
                        <div class="col-md-9">
                            <input type="text" ng-model="demographics.user.first_name" name="first_name" maxlength="50" id="txt_first_name" ng-minlength="3" placeholder="First name" class="form-control" required>
                            <p ng-show="demographicsForm.first_name.$invalid && !demographicsForm.first_name.$pristine" class="help-block">Your first name is required Min. 3 characters required</p>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('txt_middle_name', 'Middle Name',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            <input type="text" ng-model="demographics.user.middle_name" name="middle_name" maxlength="50" id="txt_middle_name" placeholder="Middle name" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('txt_last_name', 'Last Name',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            <input type="text" ng-model="demographics.user.last_name" name="last_name" maxlength="50" id="txt_last_name" placeholder="Last name" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('gender', 'Gender',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            <label for="radio_male" class="radio-inline">
                                <input type="radio" ng-model="demographics.user.gender" value="male" name="gender" checked="checked" id="radio_male">Male
                            </label>
                            <label for="radio_female" class="radio-inline">
                                 <input type="radio"ng-model="demographics.user.gender" value="female" name="gender" id="radio_female">Female
                           </label>
                            
                        </div>
                    </div>
                    
                    <div class="form-group">
                        {!! Form::label('txt_birth_date', 'Birth Date',array('class'=>'col-md-3 control-label')) !!}
                        
                        <div class="col-md-9">
                        
                          <input-datepicker format="MM dd, yyyy" date="demographics.user.birth_date"></input-datepicker>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('select_status', 'Status',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            <select name="status" ng-model="demographics.user.status" ng-options="status.id as status.name for status in demographics.users_status" id="select_status" class="form-control">
                                <option value="">Please Status</option>
                            </select>
                        </div>
                    </div>
                  
                  
                </div>
                <!-- END Basic Form Elements Content -->
             </simple-block>
             
            <!-- END Personal Form Elements Block -->
        </div>
        <div class="col-md-12 col-lg-6">
            <simple-block block-title="Photo">            
                <!-- Dropzone Content -->
                <!-- Dropzone.js, You can check out https://github.com/enyo/dropzone/wiki for usage examples -->
                <div class="dropzone" file="demographics.user.photo" upload-dropzone >
                     <div class="dz-default dz-message"><span>Drop file here to upload</span></div>                     
                </div>
                <!-- END Dropzone Content -->
            </simple-block>

            <simple-block block-title="Contact">
            <!-- Horizontal Form Content -->
                <div class="form-horizontal form-bordered">
                    
                    <div class="form-group">
                        {!! Form::label('txt_phone', 'Phone',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            <input type="text" ng-model ="demographics.user.phone" name="phone" maxlength="15" id="txt_phone" placeholder="Enter Phone" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('tex_address', 'Address',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            <input type="text" ng-model="demographics.user.address" name="address" maxlength="100" id="tex_address" placeholder="Enter Address" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('txt_city', 'City',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            <input type="text" ng-model= "demographics.user.city" name="city" maxlength="30" id="txt_city" placeholder="Enter City" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('txt_state', 'State',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            <input type="text" ng-model= "demographics.user.state" name="state" maxlength="30" id="txt_state" placeholder="Enter State" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('txt_country', 'Country',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            <input type="text" ng-model= "demographics.user.country" name="country" maxlength="30" id="txt_country" placeholder="Enter Country" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('txt_postal_code', 'Postal Code',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            <input type="text" ng-model= "demographics.user.post_code" name="post_code" maxlength="20" id="txt_postal_code" placeholder="Enter Postal Code" class="form-control">
                        </div>
                    </div>
                  
                </div>
                <!-- END Horizontal Form Content -->
            </simple-block>
            <!-- END Contact Form Block -->
        </div>
        </div>
        <div class ="row">
        <div class="col-md-12">
            <div class="form-horizontal form-bordered">
                <div class="form-group form-actions">
                    <div class="col-md-12 col-md-offset-5">
                        <button class="btn btn-default" type="button" ng-disabled="demographicsForm.$invalid" ng-click="demographics.updateUser(demographicsForm.$valid)">
                            <i class="fa fa-angle-right"></i> Submit
                        </button>
                        <button class="btn btn-default" type="button" ng-click="demographics.reset()">
                            <i class="fa fa-repeat"></i> Reset
                        </button>
                        <span class="text-danger" ng-hide="demographicsForm.$valid">&nbsp;&nbsp;(Fields with * are mandatory.)</span>
                    </div>
                </div>
           </div>
        </div> 
        </div>
    </form>
</script>   
<!-- Demographics details end -->
@section('jquery_ready')
    
@append