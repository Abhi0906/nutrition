@extends('layout.app')
@section('content')
{{ Form::model($user, array('route' => array('patient.update', $user->id), 'method' => 'PUT')) }}
    <input type="hidden" name="user_type" value="{{$user->user_type}}">
<div class="row block-title patient_title">
    <div class="col-sm-5">
        <h3>
            <ul class="list-inline margin_bot_0">
                <li> @if(isset($user->photo) && $user->photo != "")
                 {!! HTML::image("uploads/".$user->photo,"avatar", array('class' => 'img-circle','id'=>'user_profle_pic','width'=>'40','height'=>'40')) !!}
                @else
                    {!! HTML::image("uploads/male.jpg","avatar", array('class' => 'img-circle','id'=>'user_profle_pic','width'=>'40','height'=>'40')) !!}

                @endif
                </li>
                <li><strong> @if($user_type == 'patient')
                   Edit {!! isset($user->full_name) ? $user->full_name : '' !!}
                 @elseif($user_type == 'doctor')
                   Edit {!! isset($user->name_with_title)? $user->name_with_title  :''!!}
                 @endif</strong>
                 </li>
            </ul>

        </h3>
    </div>
    <div class="col-sm-7 text-alignRight">
        <h3>
            <div class="form-group">
                 <button class="btn btn-default">
                        <i class="fa gi gi-disk_saved"></i> Save & Close
                 </button>

                <!--  <button class="btn btn-default">
                        <i class="fa gi gi-bin"></i> Delete
                 </button> -->
                 @if($user_type == "patient")
                        <a href="/admin/patient" class="btn btn-default"> <i class="gi gi-remove_2"></i>
                         &nbsp;Cancel</a>

                 @endif
                 @if($user_type == "doctor")
                        <a href="/admin/doctor" class="btn btn-default"> <i class="gi gi-remove_2"></i>
                        &nbsp;Cancel</a>
                 @endif
            </div>
        </h3>
  </div>
</div>
@include('errors.error')
    <div class="row">
        <div class="col-md-12 col-lg-6">
            <!-- Account Form Block -->
            <div class="block">
                <!-- Horizontal Form Title -->
                <div class="block-title">
                   <!--  <div class="block-options pull-right">
                        <a title="" data-toggle="button" class="btn btn-alt btn-sm btn-default toggle-bordered enable-tooltip" href="javascript:void(0)" data-original-title="Toggles .form-bordered class">No Borders</a>
                    </div> -->
                    <h2><strong>Account</strong></h2>
                </div>
                <!-- END Horizontal Form Title -->

                <!-- Horizontal Form Content -->
                <div class="form-horizontal form-bordered">

                    <div class="form-group">
                        {!! Form::label('txt_username', 'Username',array('class'=>'col-md-3 control-label')) !!}
                       <div class="col-md-9">
                            {!! Form::text('username', $user->username, array('class' => 'form-control','placeholder'=>"Username",'disabled'=>'disabled','id'=>'txt_username','maxlength'=>'50')) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('txt_email', 'Email',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            {!! Form::email('email',$user->email, array('class' => 'form-control','placeholder'=>"Enter Email",'id'=>'txt_email','maxlength'=>'50')) !!}
                        </div>
                    </div>
                    @if($user_type == 'doctor')
                        <div class="form-group">
                            {!! Form::label('pwd_password', 'Password',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                {!! Form::password('password',array('class' => 'form-control','placeholder'=>"Password",'id'=>'pwd_password','maxlength'=>'50')) !!}
                                <span class="help-block">Please enter a complex password</span>
                            </div>
                        </div>
                     @endif
                    @if($user_type == 'patient')
                    <div class="form-group">
                        {!! HTML::decode(Form::label('txt_membership_code', 'Membership Code', array('class'=>'col-md-3 control-label'))) !!}
                        <div class="col-md-9">

                            {!! Form::text('membership_code',$user->membership_code, array('class' => 'form-control','placeholder'=>"Membership Code",'id'=>'txt_membership_code','maxlength'=>'50')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('txt_subscription', 'News Subscription',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                           {!! Form::checkbox('news_letter', 1, true) !!}
                        </div>
                    </div>
                    @endif
                    @if($user_type == 'doctor')
                    <div class="form-group">
                        {!! Form::label('select_title', 'Title',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            {!! Form::select('doctor_title',$doctor_titles
                                                 , $user->doctor_title,array('class'=>'form-control','id'=>'select_title')) !!}
                        </div>
                    </div>
                    @endif
                </div>
                <!-- END Horizontal Form Content -->
            </div>
            <!-- END Account Form Block -->

             <!-- Personal Form Elements Block -->
             <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <!-- <div class="block-options pull-right">
                        <a title="" data-toggle="button" class="btn btn-alt btn-sm btn-default toggle-bordered enable-tooltip" href="javascript:void(0)" data-original-title="Toggles .form-bordered class">No Borders</a>
                    </div> -->
                    <h2><strong>Personal</strong> </h2>
                </div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <div class="form-horizontal form-bordered">

                     <div class="form-group">
                        {!! Form::label('txt_first_name', 'First name',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                           {!! Form::text('first_name',$user->first_name, array('class' => 'form-control','placeholder'=>"First name",'id'=>'txt_first_name','maxlength'=>'30')) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('txt_middle_name', 'Middle Name',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                           {!! Form::text('middle_name',$user->middle_name, array('class' => 'form-control','placeholder'=>"Middle name",'id'=>'txt_middle_name','maxlength'=>'30')) !!}
                        </div>
                    </div>

                     <div class="form-group">
                        {!! Form::label('txt_last_name', 'Last Name',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            {!! Form::text('last_name',$user->last_name, array('class' => 'form-control','placeholder'=>"Last name",'id'=>'txt_last_name','maxlength'=>'50')) !!}
                        </div>
                    </div>
                    <div class="form-group patient_gender">
                        {!! Form::label('gender', 'Gender',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            <label for="radio_male" class="radio-inline">
                                {!! Form::radio('gender', 'male', $user->gender, ['id' => 'radio_male']) !!}Male
                            </label>
                            <label for="radio_female" class="radio-inline">
                                {!! Form::radio('gender', 'female',$user->gender, ['id' => 'radio_female']) !!}Female
                           </label>

                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('select_template', 'Meal Template',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            <span id="dis_male_template">
                            {!! Form::select('meal_template_id',
                                             ["Select Template"] + $meal_templates_male->toArray(),
                                             isset($user_template->meal_template_id)  ? $user_template->meal_template_id: null
                                             ,array('class'=>'form-control','id'=>'meal_template_id')) !!}
                            </span>
                            <span id="dis_female_template">
                            {!! Form::select('meal_template_id',
                                             ["Select Template"] + $meal_templates_female->toArray(),
                                             isset($user_template->meal_template_id)  ? $user_template->meal_template_id: null
                                             ,array('class'=>'form-control','id'=>'meal_template_id')) !!}
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('txt_birth_date', 'Birth Date',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            {!! Form::text('birth_date',$user->birth_date, array('class' => 'form-control input-datepicker','placeholder'=>"MM dd, yyyy",'id'=>'txt_birth_date','readonly'=>'readonly','data-date-format'=>'MM dd, yyyy')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('select_status', 'Status',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                             {!! Form::select('status',
                                                 array('' => 'Please Status',
                                                       'Active' => 'Active',
                                                       'Inactive'=>'Inactive'
                                                 ),$user->status ,array('class'=>'form-control','id'=>'select_status')) !!}

                        </div>
                    </div>
                     @if($user_type == 'patient')
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="Weight">Weight</label>
                               <div class="col-md-9">
                                <div class="input-group">
                                   <input type="text" class="form-control" id="baseline_weight" name="baseline_weight" placeholder="Enter Weight" maxlength="5" value="{{$user->profile->baseline_weight}}">
                                    <span class="input-group-btn">
                                       <span class="btn btn-primary">&nbsp;&nbsp;Lbs&nbsp;&nbsp;</span>
                                    </span>
                                    </div>
                                      <small>Enter numbers only</small>
                                </div>
                    </div>

                     <div class="form-group">
                                {!! Form::label('Height', 'Height',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-9">
                                 <div class="input-group">
                                    <input type="text" maxlength="5" name="height_in_feet" placeholder="Enter feet" id="feet_height" class="form-control" value="{{$user->profile->height_in_feet}}">

                                    <span class="input-group-btn">
                                      <span class="input-group-btn"><span class="btn btn-primary">&nbsp;&nbsp;Feet&nbsp;&nbsp;</span></span>
                                       </span>
                                      <input type="text" maxlength="5" name="height_in_inch" value="{{$user->profile->height_in_inch}}" placeholder="Enter inches"  id="inches_height" class="form-control">
                                      <span class="input-group-btn"><span class="btn btn-primary">&nbsp;&nbsp;Inches&nbsp;&nbsp;</span></span>
                                  </div>
                                       <!-- <span class="help-block">Enter numbers only </span>
                                       <span class="help-block">Enter numbers only </span> -->

                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-5">
                                               <p style="margin-left:5%; font-size: 13px;">Enter numbers only </p>
                                            </div>
                                            <div class="col-md-7">
                                               <p style="margin-left:8%; font-size: 13px;">Enter numbers only</p>
                                            </div>
                                        </div>
                                     </div>
                               </div>
                    </div>
                    @endif
                </div>
                <!-- END Basic Form Elements Content -->
             </div>
            <!-- END Personal Form Elements Block -->
        </div>
        <div class="col-md-12 col-lg-6">

           <div class="block full">
                <!-- Dropzone Title -->
                <div class="block-title">
                    <!-- <div class="block-options pull-right">
                        <a title="" data-toggle="tooltip" class="btn btn-alt btn-sm btn-default" href="javascript:void(0)" data-original-title="Settings"><i class="gi gi-cogwheel"></i></a>
                    </div> -->
                    <h2><strong>Photo</strong></h2>
                </div>
                <!-- END Dropzone Title -->

                <!-- Dropzone Content -->
                <!-- Dropzone.js, You can check out https://github.com/enyo/dropzone/wiki for usage examples -->
                <div class="dropzone"  id="profile_pic">
                     <div class="dz-default dz-message"><span>Drop file here to upload</span></div>
                     {!! Form::hidden('photo',null, array('id'=>'hnphoto')) !!}
                </div>
                <!-- END Dropzone Content -->
           </div>

             <!-- Contact Form Block -->
            <div class="block">
                <!-- Horizontal Form Title -->
                <div class="block-title">
                    <!-- <div class="block-options pull-right">
                        <a title="" data-toggle="button" class="btn btn-alt btn-sm btn-default toggle-bordered enable-tooltip" href="javascript:void(0)" data-original-title="Toggles .form-bordered class">No Borders</a>
                    </div> -->
                    <h2><strong>Contact</strong></h2>
                </div>
                <!-- END Horizontal Form Title -->

                <!-- Horizontal Form Content -->
                <div class="form-horizontal form-bordered">

                    <div class="form-group">
                        {!! Form::label('txt_phone', 'Phone',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            {!! Form::text('phone',$user->phone, array('class' => 'form-control','placeholder'=>"Enter Phone",'id'=>'txt_phone','maxlength'=>'15')) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('tex_address', 'Address',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            {!! Form::text('address',$user->address, array('class' => 'form-control','placeholder'=>"Enter Address",'id'=>'tex_address','maxlength'=>'700')) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('txt_city', 'City',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            {!! Form::text('city',$user->city, array('class' => 'form-control','placeholder'=>"Enter City",'id'=>'txt_city','maxlength'=>'30')) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('txt_state', 'State',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            {!! Form::text('state',$user->state, array('class' => 'form-control','placeholder'=>"Enter State",'id'=>'txt_state','maxlength'=>'30')) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('txt_country', 'Country',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            {!! Form::text('country',$user->country, array('class' => 'form-control','placeholder'=>"Enter Country",'id'=>'txt_country','maxlength'=>'30')) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('txt_postal_code', 'Postal Code',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-9">
                            {!! Form::text('post_code',$user->post_code, array('class' => 'form-control','placeholder'=>"Enter Postal Code",'id'=>'txt_postal_code','maxlength'=>'15')) !!}
                        </div>
                    </div>

                </div>
                <!-- END Horizontal Form Content -->
            </div>
            <!-- END Contact Form Block -->
        </div>
    </div>
    <!-- <div class ="row">
        <div class="col-md-12">
            <div class="form-horizontal form-bordered">
                <div class="form-group form-actions">
                    <div class="col-md-12 col-md-offset-5">
                        <button class="btn btn-default" type="submit">
                            <i class="fa gi gi-disk_saved"></i> Submit
                        </button>
                        <button class="btn btn-default" type="reset">
                            <i class="fa fa-repeat"></i> Reset
                        </button>
                    </div>
                </div>
           </div>
        </div>
    </div> -->
    </form>

@endsection
@section('jquery_ready')
    Dropzone.autoDiscover = false;
    $("div#profile_pic").dropzone({
                    url: "/api/v2/admin/upload",
                    maxFiles: 1,
                    paramName:'user_photo',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    addRemoveLinks:true,
                    uploadMultiple:false,
                    init: function() {
                        this.on("complete", function(data) {
                                       if(data.status == 'success'){
                                         var response = eval('(' + data.xhr.response + ')');
                                         var destination_path = '/uploads/'+response.file_name;
                                         $('#user_profle_pic').attr('src',destination_path);
                                         $('#hnphoto').val(response.file_name);
                                       }

                        });
                        this.on("removedfile",function(file){
                             var file_name = file.name;

                             HTTPRequest('', 'get', '/api/v2/admin/removephoto/'+file_name, function(data,status,xhr){
                                if(status == 'success'){
                                    if(data.error == false){

                                    }
                                }
                               });

                        });
                        this.on("addedfile", function() {
                          if (this.files[1]!=null){
                            this.removeFile(this.files[0]);
                           }
                        });

                   }

               });

    //Start: Display template according to meal template

    // fetch the id name of checked post-format
    var idName = $('input[type=radio][name=gender]:checked').attr('id');

    // display the metabox if the fetched value matched
        if ( idName === 'radio_male' ) {
            $('#dis_male_template').show();
            $('#dis_female_template').hide();

            $("#dis_female_template select").prop( "disabled", true );
            $("#dis_male_template select").prop( "disabled", false );
        }
        else{
            $('#dis_male_template').hide();
            $('#dis_female_template').show();

            $("#dis_male_template select").prop( "disabled", true );
            $("#dis_female_template select").prop( "disabled", false );
        }

        // show or hide
        $('input[type=radio][name=gender]').click(function(){
            idName = $(this).attr('id');
            if ( $(this).is(':checked') ) {
                if ( idName === 'radio_male' ) {
                    $('#dis_male_template').show();
                    $('#dis_female_template').hide();

                    $("#dis_female_template select").prop( "disabled", true );
                    $("#dis_male_template select").prop( "disabled", false );
                }
                else{
                    $('#dis_male_template').hide();
                    $('#dis_female_template').show();

                    $("#dis_male_template select").prop( "disabled", true );
                    $("#dis_female_template select").prop( "disabled", false );
                }
            }
        });
    //End: Display template according to meal template
@append
