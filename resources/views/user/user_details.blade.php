@extends('layout.app')

@section('base')
<base href="/{{$user_type}}/{{$user_id}}/"></base>
@endsection  

@section('content')
@include('user.demographics')


  <!-- Forms General Header -->
    @include('user.partials.registration_form_header_partial')
    <!-- END Forms General Header -->

    <!-- Forms General Header -->
    <!-- END Forms General Header -->

    <input type="hidden" name="_token" value="{{ csrf_token() }}">
	<!-- Page content -->
	<div id="page-content">
	    <!-- Tickets Content -->
	    <div class="row" ng-controller="UserDetailsController as userDetails">
	       
	        <div class="col-sm-12 col-lg-12"  ng-init="userDetails.userId='{{$user_id}}';userDetails.userType='{{$user_type}}';userDetails.setMenu()">
	            <!-- Tickets Block -->
	            <div class="block">
	                <!-- Tickets Title -->
	                <div class="block-title">
						<div class="block-options pull-right" ng-if="userDetails.selectedBlockTitle == 'Schedule'">
			                <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default enable-tooltip" data-toggle="button" title="" style="cursor:pointer" ng-click ="userDetails.editSchedule()">Assign workouts
			                </a>
			                <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default enable-tooltip" data-toggle="button" title="" style="cursor:pointer" ng-click="userDetails.expandAll()">@{{userDetails.right_block_title}}</a>
			            </div> 
	                    <h2><strong><span id="block_tittle">@{{userDetails.selectedBlockTitle}}</span></strong></h2>  
                    </div>
	                <!-- END Tickets Title -->

	                <!-- Tabs Content -->
	                <div>
	                    <!-- Tickets List -->
	                    <div class="tab-pane">
							<div>
								<div ng-view></div>
							</div>
	                    </div>
	                   
	                    <!-- END Ticket View -->
	                </div>
	                <!-- END Tabs Content -->
	            </div>
	            <!-- END Tickets Block -->
	        </div>
	    </div>
	    <!-- END Tickets Content -->
	</div>
	<!-- END Page Content -->

@endsection   


@section('page_scripts')
{!! HTML::script("js/controllers/UserDetailsController.js") !!}
{!! HTML::script("js/controllers/DemographicsController.js") !!}
{!! HTML::script("js/controllers/ProfileController.js") !!}
<!-- {!! HTML::script("js/controllers/VitalsController.js") !!}
{!! HTML::script("js/controllers/MedicationsController.js") !!}
{!! HTML::script("js/controllers/ScheduleController.js") !!}
{!! HTML::script("js/controllers/AssignpatientsController.js") !!}
 -->{!! HTML::script("js/directives/uploadDropzone.js") !!}
{!! HTML::script("js/directives/inputDatepicker.js") !!}
<!-- {!! HTML::script("js/directives/inputTimepicker.js") !!} -->
<!-- {!! HTML::script("js/controllers/HistoryController.js") !!}
{!! HTML::script("js/controllers/DiaryController.js") !!}
{!! HTML::script("js/controllers/WeightController.js") !!} -->

{!! HTML::script("js/directives/chosen.js") !!}
<!-- {!! HTML::script("js/directives/inputTypeahead.js") !!}
{!! HTML::script("js/directives/searchVideo.js") !!} -->
@append
@section('inline_scripts')
<script type="text/javascript">
	
	// $('#expand_collapse_all').click(function(){
 //        var blockContent = $('[data-toggle="block-toggle-content"]').closest('.block').find('.block-content');
 //        if ($(this).hasClass('active')) {
 //            blockContent.slideUp();
 //            $('#expand_collapse_all').text('Expand All');
 //        } else {
 //            blockContent.slideDown();
 //             $('#expand_collapse_all').text('Collapse All');
 //        }
 //        $(this).toggleClass('active');
	// });
	
</script>


@append

@section('jquery_ready')
	App.sidebar('toggle-sidebar');

@append