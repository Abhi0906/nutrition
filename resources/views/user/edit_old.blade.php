@extends('layout.app')

@section('base')
<base href="/patient/"></base>
@endsection  

@section('content')
@include('user.demographics')
@include('patient.alerts')
@include('patient.history')
@include('patient.medication')
@include('patient.vitals')
@include('patient.profile_goals')
@include('patient.schedules')
  <!-- Forms General Header -->
    @include('user.partials.registration_form_header_partial')
    <!-- END Forms General Header -->

    <!-- Forms General Header -->
    <!-- END Forms General Header -->
    {!! Form::open(array('onsubmit' => 'return false;','method' => 'PUT','class' => 'form-horizontal form-bordered',)) !!}

    <input type="hidden" name="_token" value="{{ csrf_token() }}">
	<!-- Page content -->
	<div id="page-content">
	    <!-- Tickets Content -->
	    <div class="row">
	        <div class="col-sm-4 col-lg-3">
	            <!-- Menu Block -->
	            <div class="block full">
	                <!-- Menu Title -->
	                <div class="block-title clearfix">
	                    <h2><i class="fa fa-user"></i> &nbsp;&nbsp;<strong>Details</strong></h2>
	                </div>
	                <!-- END Menu Title -->

	                <!-- Menu Content -->
	                <ul class="nav nav-pills nav-stacked" data-toggle="tabs">
	                    <li class ="active">
	                        <a ng-href="demographics/{{$user_id}}" onclick="changeTitle('Demographics');">
	                            <i class="fa fa-user fa-fw"></i> <strong>Demographics</strong>
	                        </a>
	                    </li>
	                    @if($user_type == 'patient')
	                    <li>
	                        <a ng-href="alerts/{{$user_id}}" onclick="changeTitle('Alerts');">
	                            <i class="fa fa-exclamation-triangle fa-fw"></i> <strong>Alerts</strong>
	                        </a>
	                    </li>
	                    <li>
	                        <a ng-href="history/{{$user_id}}" onclick="changeTitle('History');">
	                            <i class="fa fa-history fa-fw"></i> <strong>History</strong>
	                        </a>
	                    </li>
	                    <li>
	                        <a ng-href="medication/{{$user_id}}" onclick="changeTitle('Medication');">
	                            <i class="wellness-icon-medicine"></i> <strong>Medication</strong>
	                        </a>
	                    </li>
	                    <li>
	                        <a ng-href="vitals/{{$user_id}}"  onclick="changeTitle('Vital Thresholds');">
	                            <i class="wellness-icon-vital"></i> <strong>Vital Thresholds</strong>
	                        </a>
	                    </li>
	                    <li>
	                        <a ng-href="profile_goals/{{$user_id}}"  onclick="changeTitle('Profile & Goals');">
	                            <i class="fa fa-bullseye fa-fw"></i> <strong>Profile & Goals</strong>
	                        </a>
	                    </li>
	                    <li>
	                        <a ng-href="schedules/{{$user_id}}"  onclick="changeTitle('Schedules');">
	                            <i class="fa fa-calendar fa-fw"></i> <strong>Schedules</strong>
	                        </a>
	                    </li>
	                    @endif
	                </ul>
	                <!-- END Menu Content -->
	            </div>
	            <!-- END Menu Block -->

	           
	        </div>
	        <div class="col-sm-8 col-lg-9">
	            <!-- Tickets Block -->
	            <div class="block">
	                <!-- Tickets Title -->
	                <div class="block-title">
	                     <h2><strong><span id="block_tittle">Demographics</span></strong></h2>
	                    <div class="block-options pull-right hide" id="expand_all">
                       	   <a id="expand_collapse_all" href="javascript:void(0)">Expand all</a>
                   		</div> 
	                </div>
	                <!-- END Tickets Title -->

	                <!-- Tabs Content -->
	                <div class="tab-content">
	                    <!-- Tickets List -->
	                    <div class="tab-pane">
							<div class="animation-fadeInQuick">
								<div ng-view></div>
							</div>
	                    </div>
	                   
	                    <!-- END Ticket View -->
	                </div>
	                <!-- END Tabs Content -->
	            </div>
	            <!-- END Tickets Block -->
	        </div>
	    </div>
	    <!-- END Tickets Content -->
	</div>
	<!-- END Page Content -->
    {!! Form::close() !!}  
@endsection   
@section('jquery_ready')
	App.sidebar('toggle-sidebar');

@append
@section('inline_scripts')
<script type="text/javascript">
	
	function changeTitle(blocktitle){
		$('#block_tittle').text(blocktitle);
		$('.tab-pane').show();
		if(blocktitle == 'Vital Thresholds'){
			$('#expand_all').removeClass('hide');
		}else{
			$('#expand_all').addClass('hide');
		}
	}
	
	$('#expand_collapse_all').click(function(){
        var blockContent = $('[data-toggle="block-toggle-content"]').closest('.block').find('.block-content');
        if ($(this).hasClass('active')) {
            blockContent.slideUp();
            $('#expand_collapse_all').text('Expand All');
        } else {
            blockContent.slideDown();
             $('#expand_collapse_all').text('Collapse All');
        }
        $(this).toggleClass('active');
	});
	
</script>


@append

