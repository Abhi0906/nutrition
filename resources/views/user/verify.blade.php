<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Genemedics</title>
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=no"/>
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="csrf-token" content="{!! csrf_token() !!}">
        {!! HTML::style("flat-ui/bootstrap/css/bootstrap.css") !!}
        {!! HTML::style("flat-ui/css/flat-ui.css") !!}
        <!-- Using only with Flat-UI (free)-->
        {!! HTML::style("common-files/css/icon-font.css") !!}
        <!-- end -->
        {!! HTML::style("css/style_pages.css") !!}
    </head>
    <body>
        <div class="page-wrapper">
            <!-- header-2 -->
            <header class="header-2">
                <div class="container">
                    <div class="row">
                        <div class="navbar col-sm-12 navbar-fixed-top" role="navigation">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle"></button>
                                <a class="brand" href="https://genemedicshealth.com/">
                                    {!! HTML::image('img/logo.png', 'Genemedics Logo', []) !!} </a>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <section class="header-2-sub bg-midnight-blue">
                <div class="background">
                    &nbsp;
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <div class="hero-unit">
                                <h1>Great! You've verified your email.<br/>
                                  Now start tracking your food and exercises. You're on your way now.</h1>
                            </div>
                            <div class="btns">
                                <button type="button" class="btn btn-danger"  onclick="window.location='{{ URL::to('/auth0/#login')}}'">Get Started</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
         </div>
     </body>
</html>