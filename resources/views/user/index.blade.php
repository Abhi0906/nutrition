@extends('layout.app')

@section('content')

<!-- Table Responsive Header -->
<!-- <div class="content-header">
    <div class="header-section">
        <h1>
            <i class="gi gi-iphone"></i>Users List<br><small>Play nice on various screen sizes!</small>
        </h1>
    </div>
</div> -->
    <?php  /* @include('user.partials.form_header_partial') */  ?>



<!-- END Table Responsive Header -->

<!-- Responsive Full Block -->

<div ng-controller="UserController as vm" ng-init="vm.type='{{$user_type}}';vm.getUsers(1);">

<div class="row block-title patient_title">
    <div class="col-sm-1">
        <h3><strong>{{ucfirst($user_type) }}s</strong></h3>
      </div>
    <div class="col-sm-11 text-alignRight">
        <h3>
          <form class="form-inline">

           <!--  <span class="hidden-sm">&nbsp;&nbsp;</span>
            <div class="form-group">
              <label>Show: </label>
              <select class="form-control" ng-options="value for value in vm.displayRecords"  ng-model="vm.itemPerPage">
             </select>
            </div>
            <div class="form-group">
              <div class="input-group">
                <input type="text" placeholder="Search" ng-model="vm.searchkey" class="form-control" name="example-input1-group2" id="example-input1-group2">
                <span class="input-group-btn">
                  <button class="btn btn-default"  ng-click="vm.getUsers(1)" type="submit"><i class="fa fa-search"></i></button>
                </span>
            </div></div> -->
            <div class="form-group">
              <a href="/admin/{{$user_type}}/create" class="btn btn-default"><span><i class="fa fa-plus"></i></span>&nbsp;New  {{  ucfirst($user_type) }}</a>
            </div>
          </form>
        </h3>
    </div>
</div>



    <div class="block">
    <!-- Responsive Full Title -->
    <div class="block-title">
        @if($user_type == "patient")
        <div class="block-options block_option_titlehead_height">
            <div class="row padding_top_bottom">
                <div class="col-sm-2">
                    <h2><strong><span style="font-size: 12px!important;">{{ ucfirst($user_type) }}s @{{vm.from}} to @{{vm.to}} of @{{vm.total_count}}</span></strong></h2>
                </div>
                <div class="col-sm-5 text-right">
                  <ul uib-pagination
                           total-items="vm.total_count"
                           ng-model="vm.current_page"
                           max-size="5"
                           class="pagination-sm margin_top_no"
                           boundary-links="false"
                           boundary-link-numbers="true"
                           rotate="false"
                           items-per-page ="vm.itemPerPage"
                           ng-change="vm.pageChanged()">
                  </ul>
                </div>


                <div class="col-sm-5 text-right">

                          <div class="form-inline">
                            <div class="form-group">
                              <label>Show: </label>
                              <select class="form-control" ng-options="value for value in vm.displayRecords"  ng-model="vm.itemPerPage">
                             </select>
                            </div>
                            <div class="form-group">
                              <div class="input-group">
                                <input type="text" placeholder="Search" ng-model="vm.searchkey" class="form-control" name="example-input1-group2" id="example-input1-group2">
                                <span class="input-group-btn">
                                  <button class="btn btn-default"  ng-click="vm.getUsers(1)" type="submit"><i class="fa fa-search"></i></button>
                                </span>
                              </div>
                            </div>

                            <span class="btn-group">
                              <button type="button" class="btn btn-default" ng-class="{'active':vm.cardLayout}" ng-click="vm.switchLayout('cardLayout')">&nbsp;&nbsp;<i class="fa fa-th-large text-muted"></i>&nbsp;&nbsp;</button>
                              <button type="button" ng-class="{'active': !vm.cardLayout}" class="btn btn-default" ng-click="vm.switchLayout('listLayout')">&nbsp;&nbsp;<i class="fa fa-th-list text-muted"></i>&nbsp;&nbsp;</button>
                            </span>
                          </div>
                </div>
            </div>
        </div>
        @endif
        @if($user_type == "doctor")
     <div class="block-options block_option_titlehead_height">
            <div class="row padding_top_bottom">
                <div class="col-sm-2">
                  <h2><strong><span style="font-size: 12px!important;">{{ ucfirst($user_type) }}s @{{vm.from}} to @{{vm.to}} of @{{vm.total_count}}</span></strong></h2>

                </div>
                 <div class="col-sm-4 text-right">
                        <ul uib-pagination
                           total-items="vm.total_count"
                           ng-model="vm.current_page"
                           max-size="5"
                           class="pagination-sm margin_top_no"
                           boundary-links="false"
                           boundary-link-numbers="true"
                           rotate="false"
                           items-per-page ="vm.itemPerPage"
                           ng-change="vm.pageChanged()"
                           >
                        </ul>
                </div>
                <div class="col-sm-6 text-right">

                </div>
            </div>
        </div>
     @endif
     </div>

    <!-- END Responsive Full Title -->

    <!-- Responsive Full Content -->
    @if($user_type == "doctor")

    <div class="row">
    @endif
    @if($user_type == "patient")
    <div class="row" ng-show="vm.cardLayout">
    @endif

        <div ng-show="vm.users.length > 0" class="col-xs-12 col-md-6 col-sm-6 col-lg-4 animation-fadeInQuick"
            ng-repeat="user in vm.users|filter:getFilter()">
            <interactive-block block-title="vm.getHTMLTitle(user)" on-remove="vm.deleteUser(user)" >
                <block-content>
                    <div class="row">
                        <ul class="fa-ul list-li-push pull-left">
                            <li><i class="fa fa-li fa-envelope-o"></i> @{{user.email | limitTo: 36 }}</li>
                            <li>
                            <i class="fa fa-li fa fa-phone fa-fw"></i>
                            <span ng-if="user.phone != '' && user.phone != null ">@{{user.phone |  limitTo: 15}}</span>
                            <span ng-else>Enter Phone</span></li>
                            @if($user_type == "patient")
                            <li ng-if="user.created_at != '' && user.created_at != null "><i class="fa fa-li fa fa-calendar"></i> <span ng-bind="vm.getDate(user.created_at)"></span></li>
                            <li class="pull-left" ng-if="user.profile.baseline_weight != '' && user.profile.baseline_weight != null ">
                                <i class="fa fa-li wellness-icon-weight"></i>@{{user.profile.baseline_weight}}
                            </li><span ng-if="user.profile.baseline_weight != '' && user.profile.baseline_weight != null "> &nbsp; | &nbsp; </span>
                                <span ng-if="user.profile.calories != '' && user.profile.calories != null ">
                                <i class="gi gi-fire" style="vertical-align: baseline;"></i>&nbsp;&nbsp;@{{user.profile.calories}}
                                </span>
                                <li ng-if="user.profile.baseline_weight == null">&nbsp;</li>
                            @endif
                        </ul>
                        <span>
                        <a ng-href="/admin/{{$user_type}}/@{{user.id}}">
                          <img ng-if="user.photo != null" ng-src="/uploads/@{{user.photo}}" alt="@{{user.full_name}}" class="img-circle pull-right" width="50px" />
                          <img ng-if="user.photo == null" ng-src="/uploads/no_img.png" alt="@{{user.full_name}}" class="img-circle pull-right" width="50px" />
                        </a>
                        <br/><br/><br/>
                            <span ng-if="user.user_type == 'patient'">
                                <small ng-if="user.email_varify == '1' " class="pull-right text-primary" style="margin-right: 10px;">

                                    @{{user.status }}
                                </small>
                                <small ng-if="user.email_varify == '0' " class="pull-right text-warning">

                                    Unverified
                                </small>
                            </span>
                            <span ng-if="user.user_type == 'doctor'">
                                <small class="pull-right text-primary" style="margin-right: 10px;">
                                @{{user.status }}
                                </small>
                            </span>
                        </span>
                    </div>
                </block-content>

            </interactive-block>
        </div>

        <div class="alert alert-danger animation-fadeInQuick" ng-cloak ng-show="vm.users.length <= 0 && !vm.isloading">
            <button data-dismiss="alert" class="close" type="button">
              <i class="ace-icon fa fa-times"></i>
            </button>

            <strong>
              No {{ ucfirst($user_type) }}s available!
            </strong>
       </div>
    </div>

    @if($user_type == "patient")
    <div class="row" ng-show="!vm.cardLayout">

        <div id="no-more-tables" class="table-responsive">
        <table class="table table-vcenter table-striped animation-fadeInQuick">
            <thead>
                <tr>
                    <th   class="text-center"></th>
                    <th  ng-click="vm.sort('full_name')" class="hidden-xs cursor table_list text-primary">Name<small ng-show="vm.sortKey=='full_name'" ng-class="{'fa fa-caret-up fa-fw':vm.reverse,'fa fa-caret-down fa-fw':!vm.reverse}"></small>
                        </th>
                    <th  ng-click="" class="hidden-xs cursor text-primary table_list"><small ng-show="" ng-class="{'fa fa-caret-up fa-fw':vm.reverse,'fa fa-caret-down fa-fw':!vm.reverse}"></small>&nbsp;&nbsp;&nbsp;</th>
                    <th class="hidden-xs cursor text-primary table_list">Mem. Code</th>
                    <th  ng-click="vm.sort('profile.baseline_weight')" class="hidden-xs cursor text-primary table_list">Weight<small ng-show="vm.sortKey=='profile.baseline_weight'" ng-class="{'fa fa-caret-up fa-fw':vm.reverse,'fa fa-caret-down fa-fw':!vm.reverse}"></small>&nbsp;&nbsp;&nbsp;</th>

                    <th ng-click="vm.sort('profile.calories')" class="hidden-xs cursor text-primary table_list">Calories<small ng-show="vm.sortKey=='profile.calories'" ng-class="{'fa fa-caret-up fa-fw':vm.reverse,'fa fa-caret-down fa-fw':!vm.reverse}"></small>&nbsp;&nbsp;&nbsp;</th>

                    <!-- <th width="10%" class="hidden-xs text-primary table_list">Assign Doctor</th> -->

                    <!-- <th class="cursor hidden-xs text-primary table_list" ng-click="vm.sort('created_at')">Created<small ng-show="vm.sortKey=='created_at'" ng-class="{'fa fa-caret-up fa-fw':vm.reverse,'fa fa-caret-down fa-fw':!vm.reverse}"></th> -->

                    <th class="hidden-xs text-primary table_list cursor" ng-click="vm.sort('status')">Status<small ng-show="vm.sortKey=='status'" ng-class="{'fa fa-caret-up fa-fw':vm.reverse,'fa fa-caret-down fa-fw':!vm.reverse}"></small></th>
                    <th class="hidden-xs text-primary table_list cursor" ng-click="vm.sort('last_active')">Last Active<small ng-show="vm.sortKey=='last_active'" ng-class="{'fa fa-caret-up fa-fw':vm.reverse,'fa fa-caret-down fa-fw':!vm.reverse}"></small></th>

                    <th class="hidden-xs table_list text-center text-primary">Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="user in vm.users | orderBy:vm.sortKey:vm.reverse|filter:getFilter()">
                    <td class="text-center">
                    <a ng-href="/admin/{{$user_type}}/@{{user.id}}">
                        <img ng-if = "user.photo != null" ng-src="/uploads/@{{user.photo}}" alt="@{{user.full_name}}" class="img-circle" width="50px" />
                        <img ng-if = "user.photo == null" ng-src="/uploads/no_img.png" alt="@{{user.full_name}}" class="img-circle img-responsive" width="50px" />
                    </a>
                    </td>
                    <td data-title="Name">
                        <a href='@{{user.user_type}}/@{{user.id}}'>
                            <span ng-bind-html="vm.getHTMLTitle(user)"></span>
                        </a><br/>
                        <span>@{{user.email |  limitTo: 35}}</span><br/>

                        <span ng-if="user.phone != '' && user.phone != null ">@{{user.phone |  limitTo: 15}}</span>
                    </td>
                    <td data-title="Impersonate">
                        <a class="btn btn-primary btndesign" href="{{ URL::to('api/v2/admin/patient/switch_to_dashborad')}}/@{{user.id}}"><i class="fa"></i>&nbsp;Impersonate</a>

                          <a class="btn btn-primary btndesign" href="{{ URL::to('admin/assign_plan')}}/@{{user.id}}"><i class="fa"></i>&nbsp;Assign Plan</a>
                    </td>


                    <td data-title="Mem. Code">
                        <span>@{{user.membership_code}}</span>
					</td>
                    <td data-title="Weight">
                        <span class="label label-default">@{{user.profile.baseline_weight}}</span>
                    </td>
                    <td data-title="Calories">
                        <span class="label label-default">@{{user.profile.calories}}</span>
                    </td>
                   <!--  <td data-title="Assign Doctor">
                        <tags-input ng-model="user.assign_doctor"
                                    on-tag-added="vm.assignedDoctor($tag,user)"
                                    on-tag-removing="vm.removeDoctor($tag,user)"
                                    display-property="full_name"
                                    placeholder=""
                                    replace-spaces-with-dashes="false"
                                    template="tag-template"
                                    >
                          <auto-complete source="vm.loadDoctorsList(user)"
                                         display-property="full_name"
                                         min-length="0"
                                         load-on-focus="true"
                                         load-on-empty="true"
                                         max-results-to-show="32"
                                         template="autocomplete-template"
                                         ></auto-complete>
                        </tags-input>
                        <script type="text/ng-template" id="tag-template">

                            <div class="tag-template" style="font-size:12px;verticle-align:middle">
                                <img ng-src="/uploads/@{{data.photo}}" ng-if="data.photo" class="img-responsive img-circle" align="left" style="width:25px;height:25px"/>
                                    &nbsp;&nbsp;<span>@{{$getDisplayText()}}</span>
                                <a style="font-size:12px;cursor:pointer" class="remove-button;padding-top:-1px" ng-click="$removeTag()">&#10006;</a>
                            </div>
                        </script>
                        <script type="text/ng-template" id="autocomplete-template">
                            <div>
                                <img ng-src="/uploads/@{{data.photo}}" class="img-responsive img-circle" align="left" style="width:25px;height:25px"/>&nbsp;
                                <span style="position:relative;top:4px;font-size:14px" ng-bind-html="$highlight($getDisplayText())"></span>
                            </div>
                        </script>
                    </td> -->
                    <!-- <td ng-bind="vm.getDate(user.created_at)" data-title="Created"></td> -->
                    <td data-title="Status">
                        <span ng-if="user.email_varify=='1'">
                            <span ng-if="user.status=='Active'" class="label label-success">@{{user.status }}</span>
                            <span ng-if="user.status=='Inactive'"  class="label label-warning">@{{user.status }}</span>
                            <span ng-if="user.status=='Guest'"  class="label label-info">@{{user.status }}</span>
                            <span ng-if="user.status=='Incomplete'"  class="label label-danger">@{{user.status }}</span>
                        </span>
                        <span ng-if="user.email_varify=='0'"  class="label label-warning">Unverified</span>
                    </td>
                    <td ng-bind="vm.getDate(user.last_active)" data-title="Last Active"></td>
                    <td class="text-center" data-title="Actions">
                        <div class="btn-group btn-group-xs">
                            <a href="/admin/{{$user_type}}/@{{user.id}}" data-toggle="tooltip" title="Edit" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                            <a href="javascript:void(0)" ng-click="vm.deleteUser(user);" data-toggle="tooltip" title="Delete" class="btn btn-danger"><i class="fa fa-times"></i></a>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        </div>

        <div class="alert alert-danger animation-fadeInQuick" ng-cloak ng-show="vm.users.length <= 0 && !vm.isloading" >
            <button data-dismiss="alert" class="close" type="button">
              <i class="ace-icon fa fa-times"></i>
            </button>

            <strong>
              No {{ ucfirst($user_type) }}s available!
            </strong>
        </div>
    </div>
    @endif
    <!-- END Responsive Full Content -->

    <div ng-if="vm.isloading">
      @include('layout.loading')
    </div>
    <!-- pagination controls  -->

    <div class="row">
             <div class="col-sm-2">
                <h2><strong><span style="font-size: 12px!important; color: #1BBAE1 !important;">{{ ucfirst($user_type) }}s @{{vm.from}} to @{{vm.to}} of @{{vm.total_count}}</span></strong></h2>
             </div>
             <div class="col-sm-10" style="text-align:center; font-size: 13px!important; ">
              <ul style="margin-right: 188px !important;" uib-pagination
                   total-items="vm.total_count"
                   ng-model="vm.current_page"
                   max-size="8"
                   class="pagination-sm"
                   boundary-links="true"
                   boundary-link-numbers="true"
                   rotate="false"
                   items-per-page ="vm.itemPerPage"
                   ng-change="vm.pageChanged()"
               >
               </ul>
             </div>
    </div>




    <!-- <div style="">
        <h2><strong><span style="font-size: 13px!important; color: #1BBAE1 !important;">{{ ucfirst($user_type) }} @{{vm.from}} to @{{vm.to}} of @{{vm.total_count}}</span></strong></h2>
    </div> -->


    <!-- <div style="text-align:center" > -->
        <!-- <dir-pagination-controls
            max-size="8"
            direction-links="true"
            boundary-links="true"
            on-page-change="vm.getUsers(newPageNumber)">
        </dir-pagination-controls> -->

       <!--  <ul style="margin-right: 188px !important;" uib-pagination
           total-items="vm.total_count"
           ng-model="vm.current_page"
           max-size="10"
           class="pagination-sm"
           boundary-links="true"
           boundary-link-numbers="true"
           rotate="false"
           items-per-page ="vm.itemPerPage"
           ng-change="vm.pageChanged()"
           >
           </ul>
    </div> -->
    <!-- End pagination controls  -->

    </div>
</div>

<!-- END Responsive Full Block -->


@endsection