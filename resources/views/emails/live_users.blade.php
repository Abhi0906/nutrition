<html>
<head>
	<title></title>
</head>
<body style="margin:0 auto;">
<table style="width:900px; border:1px solid #ccc; margin:0 auto;border-collapse: collapse;padding: 0; box-sizing: border-box;">
	<tbody>
		<tr style="background: url('https://genemedicshealth.com/images/new_backheader.jpg');
		background-repeat: repeat-x;">
			<td colspan="2" style="padding:10px;"><img src="https://genemedicshealth.com/images/new_logo-genemedics.png" width="200" /></td>
		</tr>
		<tr style="background: url('https://genemedicshealth.com/images/new_about-bg.png'); background-repeat:no-repeat; height:250px; background-size:cover;">
			<td style="vertical-align:top; color:#0069C5;">
			
			<p style="position:relative; right:10px; text-align:right; font-size: 32px; font-weight:bold;">Genemedics Nutrition</p>

			</td>
		</tr>
		<tr>
			<td colspan="2">
			<p style="font-weight: 700;color: #003f70;font-size: 22px;padding-top:5px">Dear  <strong contenteditable="false" data-token="{leadfield=firstname}">{{$name}}</strong>,</p>
			<p>Thank you for being a  Genemedics Nutrition Customer!</p>


			<p>At Genemedics, we strive to be the very best and are continually working to improve our systems to better serve you.  We recently made an upgrade to our server, which will require you to log in with the following information:<br/><br/>

		       <span>Username:  {{$toemail}} </span><br/>
			   <span>Password:  {{$password}} </span><br/>
			  
			</p>
			<p>You will also receive a verification email from ‘Genemedics Health Institute’, please follow instructions in that email to verify your account on our server.</p>	

			<p>Thank you for continuing to make Genemedics your partner in health and wellness!  Please do not hesitate to contact us at (800) 277-4041 if you have any questions.<br />
			</p>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<p>
			 Sincerely,<br>
			 Genemedics Nutrition Team 
			</p>
			<td>
		</tr>
		<tr>
			<td colspan="2">
			<p style="font-size: 12px; text-align:center; margin: 0 auto; height: 25px; background: #fff; padding-top: 10px; color: #000; font-weight: bold; text-transform: uppercase;">
			  Visit us online at <a href="https://www.genemedicsnutrition.com" style="color: #3696c2; font-size: 12px;">
			   https://www.genemedicsnutrition.com</a></p>
			</td>
		</tr>
		<tr style="background: #1290e3;">
			<td colspan="2">
			<table style="width:90%;margin: 0 auto;">
				<tbody>
					<tr>
						<td width="77%">
						<p style="padding-top:15px;padding-bottom:15px; font-weight: normal; text-rendering: optimizeLegibility !important; -webkit-font-smoothing: antialiased !important; -moz-osx-font-smoothing: grayscale; text-transform: uppercase; color: #3b3b3b;">
						If you have any questions, please feel free to contact us at<br />
						<a href="mailto:info@genemedicsnutrition.com" style="color: white;">
						info@genemedics.com</a> or by phone at 
						<span style="color: white; text-transformation: underline;">(800) 277-4041</span></p>
						</td>
						<td width="23%"> 
							<a href="#"><img src="https://genemedicshealth.com/images/new_footer_fafacebooksquare.png" width="30" style="border-radius:0px;" /></a>
							<a href="#"><img src="https://genemedicshealth.com/images/new_footer_fawittersquare.png" width="30" style="border-radius:0px;" /></a>
							<a href="#"><img src="https://genemedicshealth.com/images/new_footer_Plus.png" width="30" style="border-radius:0px;" /></a>
							<a href="#"><img src="https://genemedicshealth.com/images/new_fagoogleplussquare.png" width="30" style="border-radius:0;" /></a>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr style="background: #1d88d1;">
			<td colspan="2">
			<p style="font-size: 12px; font-weight: bold; color: #fff; text-rendering: optimizeLegibility !important; -webkit-font-smoothing: antialiased !important; -moz-osx-font-smoothing: grayscale; text-transform: uppercase;text-align:center;padding:10px;">
					Genemedics Health Institute, P.C. All rights reserved. genemedicsnutrition.com</p>
			</td>
		</tr>
	</tbody>
</table>
</body>
</html>