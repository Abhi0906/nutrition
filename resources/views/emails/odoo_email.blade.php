<html>
<head>
	<title></title>
</head>
<body style="margin:0 auto;">
<table style="width:900px; border:1px solid #ccc; margin:0 auto;border-collapse: collapse;padding: 0; box-sizing: border-box;">
	<tbody>
		<tr style="background: url('https://genemedicshealth.com/images/new_backheader.jpg');
		background-repeat: repeat-x;">
			<td colspan="2" style="padding:10px;"><img src="https://genemedicshealth.com/images/new_logo-genemedics.png" width="200" /></td>
		</tr>
		<tr style="background: url('https://genemedicshealth.com/images/new_about-bg.png'); background-repeat:no-repeat; height:250px; background-size:cover;">
			<td style="vertical-align:top; color:#0069C5;">
			<p style="position:relative; right:10px; margin-bottom:-15px; text-align:right; font-weight:700; font-size:25px;">
				Thank You for contacting
			</p>
			<p style="position:relative; right:10px; text-align:right; font-size: 32px; font-weight:bold;">Genemedics Health Institute!</span></p>

			</td>
		</tr>
		<tr>
			<td><h3>User Information:<h3></td>
		</tr>

		<tr>
			<td>Name: &nbsp;&nbsp;<span> {{$first_name}}  {{$last_name}}</span></td>
		</tr>

		<tr>
			<td>Email: &nbsp;&nbsp;<span> {{$email}} </span></td>
		</tr>

		<tr>
			<td>Gender: &nbsp;&nbsp;<span> {{ucfirst($gender)}} </span></td>
		</tr>
		
		<tr>
			<td>Birth date: &nbsp;&nbsp;<span>{{$birth_date}} </span></td>
		</tr>

		<tr>
			<td>Weight: &nbsp;&nbsp;<span>{{$baseline_weight}}</span>&nbsp;(lbs)</td>
		</tr>

		<tr>
			<td>Height: &nbsp;&nbsp;<span>{{$height_in_feet}}<sup>'</sup>{{$height_in_inch}}<sup>"</sup></span></td>
		</tr>

		<tr>
			<td>Calories: &nbsp;&nbsp;<span>{{$calories}}</span></td>
		</tr>

		<tr>
			<td>Date Time: &nbsp;&nbsp;<span>{{$datetime}}</span></td>
		</tr>

		<tr>
			<td>Ip Address: &nbsp;&nbsp;<span>{{$ip_address}}</span></td>
		</tr>
		
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td colspan="2">
			<p style="font-size: 12px; text-align:center; margin: 0 auto; height: 25px; background: #fff; padding-top: 10px; color: #000; font-weight: bold; text-transform: uppercase;">Visit us Online at <a href="https://genemedicshealth.com" style="color: #3696c2; font-size: 12px;">www.genemedicshealth.com</a></p>
			</td>
		</tr>
		<tr style="background: #1290e3;">
			<td colspan="2">
			<table style="width:90%;margin: 0 auto;">
				<tbody>
					<tr>
						<td width="77%">
						<p style="padding-top:15px;padding-bottom:15px; font-weight: normal; text-rendering: optimizeLegibility !important; -webkit-font-smoothing: antialiased !important; -moz-osx-font-smoothing: grayscale; text-transform: uppercase; color: #3b3b3b;">If you have any questions, please feel free to contact us at<br />
						<a href="mailto:info@genemedicsnutrition.com" style="color: white;">md@genemedics.com</a> or by phone at <span style="color: white; text-transformation: underline;">(800) 277-4041</span></p>
						</td>
						<td width="23%"> 
							<a href="#"><img src="https://genemedicshealth.com/images/new_footer_fafacebooksquare.png" width="30" style="border-radius:0px;" /></a>
							<a href="#"><img src="https://genemedicshealth.com/images/new_footer_fawittersquare.png" width="30" style="border-radius:0px;" /></a>
							<a href="#"><img src="https://genemedicshealth.com/images/new_footer_Plus.png" width="30" style="border-radius:0px;" /></a>
							<a href="#"><img src="https://genemedicshealth.com/images/new_fagoogleplussquare.png" width="30" style="border-radius:0;" /></a>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr style="background: #1d88d1;">
			<td colspan="2">
			<p style="font-size: 12px; font-weight: bold; color: #fff; text-rendering: optimizeLegibility !important; -webkit-font-smoothing: antialiased !important; -moz-osx-font-smoothing: grayscale; text-transform: uppercase;text-align:center;padding:10px;">Genemedicshealth.com, LLC. All rights reserved. Genemedicshealth.com</p>
			</td>
		</tr>
	</tbody>
</table>
</body>
</html>
