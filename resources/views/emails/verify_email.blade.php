
<html>
<head>
	<title></title>
</head>
<body style="margin:0 auto;">
<table style="width:900px; border:1px solid #ccc; margin:0 auto;border-collapse: collapse;padding: 0; box-sizing: border-box;">
	<tbody>
		<tr style="background: url('https://genemedicshealth.com/images/new_backheader.jpg');
		background-repeat: repeat-x;">
			<td colspan="2" style="padding:10px;"><img src="https://genemedicshealth.com/images/new_logo-genemedics.png" width="200" /></td>
		</tr>
		<tr style="background: url('https://genemedicshealth.com/images/new_about-bg.png'); background-repeat:no-repeat; height:250px; background-size:cover;">
			<td style="vertical-align:top; color:#0069C5;">
			<p style="position:relative; right:10px; margin-bottom:-15px; text-align:right; font-weight:700; text-transform: uppercase; font-size:25px;">
			Thank you for registering with  </p>
			<p style="position:relative; right:10px; text-align:right; font-size: 32px; font-weight:bold;">
				Genemedics Nutrition!
			</p>

			</td>
		</tr>
		<tr>
			<td colspan="2">
			
		
				<p>Please verify your e-mail address below to continue on the Genemedics path to look and feel your very best!</p>

				
				<a href="{{ URL::to('register/verify/'. $confirmation_code) }}"
											 style="background-color: #0072bc;
												    border: 0 none;
												    color: #fff;
												    cursor: pointer;
												    display: inline-block;
												    font-size: 18px;
												    margin-top: 20px;
												    padding: 11px 54px;
												    width: auto;
												    text-decoration: none;">
										    Verify Email</a>  
										    <br/>
			 Sincerely,<br>
			 Your Genemedics Team 
			 <br><br>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<p style="font-size: 12px; text-align:center; margin: 0 auto; height: 25px; padding-top: 10px; color: #000; font-weight: bold; text-transform: uppercase;">Visit us Online at <a href="https://genemedicshealth.com" style="color: #3696c2; font-size: 12px;">www.genemedicshealth.com</a></p>
			</td>
		</tr>
		<tr style="background: #1290e3;">
			<td colspan="2">
			<table style="width:90%;margin: 0 auto;">
				<tbody>
					<tr>
						<td width="77%">
						<p style="padding-top:15px;padding-bottom:15px; font-weight: normal; text-rendering: optimizeLegibility !important; -webkit-font-smoothing: antialiased !important; -moz-osx-font-smoothing: grayscale; text-transform: uppercase; color: #3b3b3b;">If you have any questions, please feel free to contact us at<br />
						<a href="mailto:info@genemedicsnutrition.com" style="color: white;">info@genemedics.com</a> or by phone at <span style="color: white; text-transformation: underline;">(800) 277-4041</span></p>
						</td>
						<td width="23%"> 
							<a href="#"><img src="https://genemedicshealth.com/images/new_footer_fafacebooksquare.png" width="30" style="border-radius:0px;" /></a>
							<a href="#"><img src="https://genemedicshealth.com/images/new_footer_fawittersquare.png" width="30" style="border-radius:0px;" /></a>
							<a href="#"><img src="https://genemedicshealth.com/images/new_footer_Plus.png" width="30" style="border-radius:0px;" /></a>
							<a href="#"><img src="https://genemedicshealth.com/images/new_fagoogleplussquare.png" width="30" style="border-radius:0;" /></a>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr style="background: #1d88d1;">
			<td colspan="2">
			<p style="font-size: 12px; font-weight: bold; color: #fff; text-rendering: optimizeLegibility !important; -webkit-font-smoothing: antialiased !important; -moz-osx-font-smoothing: grayscale; text-transform: uppercase;text-align:center;padding:10px;">Genemedics Health Institute, P.C. ALL RIGHTS RESERVED.</p>
			</td>
		</tr>
	</tbody>
</table>
</body>
</html>
