
<html>
<head>
	
<meta name="robots" content="noindex">	<link rel="stylesheet" href="/themes/genemedics/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="/themes/genemedics/css/font-awesome.min.css" type="text/css" />
    <style type="text/css">
    	@font-face {
  			font-family: 'Roboto Bold';	
  			src: url('fonts/Roboto_Condensed/RobotoCondensed-Bold.ttf');
  		}
  		@font-face {
  			font-family: 'Roboto Condensed';	
  			src: url('fonts/Roboto_Condensed/RobotoCondensed-Regular.ttf');
  		}
    </style>
</head>
<body style="margin: 0 !important; padding: 0 !important; font-family:'Roboto Condensed';">
<!-- HIDDEN PREHEADER TEXT -->
<div style="display:
 none; font-size: 1px; color: #fefefe; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 
0; overflow: hidden;">
    Entice the open with some amazing preheader text. Use a little mystery and get those subscribers to read through...
</div>
<!-- HEADER -->
<table class="emailWrapper" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr style="background: #1272BC;">
        <td>
           <table border="0" cellpadding="0" cellspacing="0" width="100%" class="wrapper">
                <tr>
                    <td style="padding: 20px;" width="50%;">
                        <a href="http://litmus.com" target="_blank">
                            <img alt="Logo" src="http://www.plenartech.com/newsletter/assets/img/logo-genemedics.png" style="display: block; color: #ffffff; font-size: 16px;" border="0">
                        </a>
                    </td>
					<td style="padding-top: 20px;text-align:right;" width="50%;">
								
								<span>
									<img src="https://genemedicshealth.com/images/new_footer_fafacebooksquare.png" width="30" style="border-radius:0px;" />
								</span>&nbsp;
								<span><img src="https://genemedicshealth.com/images/new_footer_fawittersquare.png" width="30" style="border-radius:0px;" /></span>&nbsp;

								<span><img src="https://genemedicshealth.com/images/new_fagoogleplussquare.png" width="30" style="border-radius:0px;" /></span>&nbsp;
								<span><img src="https://genemedicshealth.com/images/youtub.png" width="30" style="border-radius:0px;" /></span>&nbsp;
					</td>
				</tr>
			</table>
		</td>
    </tr>
    <tr>
		<td>
			<table width="100%" cellspacing="0" cellpadding="0" style="align-items: center;background: #1c4e89 none repeat scroll 0 0;">
				<tr>
					<td style="color:#fff; padding:20px;" width="50%">
						<h1 style="font-size:42px; font-family:'Roboto Bold';">LAB RESULTS RECEIVED</h1>
					</td>
					<td width="50%">
		<img src="http://www.plenartech.com/newsletter/assets/img/newLab-results-received.png" style="width:100%;">
					</td>
				</tr>
			</table>
			
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%">
				<tr>
					<td style="padding: 25px 25px;">
						<h3><span style="color:#7D7D7D;">Dear<span> <span style="color:#2D5A91;">{{$first_name}}, </span></span></span></h3>
						<p style="color:#1171BB; font-weight:bold; font-size:18px; margin-bottom:3px; line-height:18px;">Thank you for your time today.</p>
						<p>
					Your most recent lab results are final and have been received. Please contact our office to schedule your medical consultation appointment at 
							(800) 277-4041  during our normal business hours: Monday-Friday between 
							the hours of 9am to 5pm EST.  You may also schedule your appointment 
							online by clicking the following button:
						</p>
						<button class="btn btn-danger" style="background:#DC101B; border-radius:0px; color:#fff; border:1px solid #f1414a; padding:5px 12px;"><strong>THE ONLINE SCHEDULER</strong></button>
						<p style="margin-top:10px; color:#257CC0; font-size:13px; font-weight:bold;">Thank you for making Genemedics your partner in Health and Wellness! </p>
						<address style="color:#808080; margin-bottom:0px;">
						Sincerely,<br>
						John Dzahristos, B.S. Sports Medicine<br>
						Clinic Specialist<br>
					</address>
					<p></p>
					</td>
					
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding: 10px 20px 5px;background: #cde9ff none repeat scroll 0 0;">
			<p style="color:#1875BD; font-size:14px;">If you have any questions, please feel free to contact us at
			</p>
				<table>
					<tr>
						<td>
							<span>
							<img src="https://genemedicshealth.com/images/phone.png" width="32" style="border-radius:0px;" />
							<span style="vertical-align:middle; color:#1875BD; font-size:18px;">(800) 277-4041</span>&nbsp;&nbsp;
						</td>
						<td>
							<span>
							<img src="https://genemedicshealth.com/images/envelope.png" width="32" style="border-radius:0px;" />
							<span style="vertical-align:middle; color:#1875BD; font-size:18px;">info@genemedics.com</span>&nbsp;&nbsp;
						</td>
						<td>
							<span>
							<img src="https://genemedicshealth.com/images/worldwideweb.png" width="32" style="border-radius:0px;" /></span>&nbsp;
							<span style="vertical-align:middle; color:#1875BD; font-size:18px;">www.genemedics.com</span>&nbsp;&nbsp;
						</td>
					</tr>
				</table>
		</td>
	</tr>
	<tr>
		<table width="100%">
			<tr>
				<td class="col-xs-12">
					<p style="font-size:13px; text-align: center; margin-top:10px; font-family:'Roboto Condensed';">Genemedics Health Institute, P.C. ALL RIGHTS RESERVED.</p>
				</td>
			</tr>
		</table>
	</tr>
</table>
 </body>
</html>
