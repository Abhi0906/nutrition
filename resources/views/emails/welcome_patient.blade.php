<html>
<head>
	<title></title>
</head>
<body style="margin:0 auto;">
<table style="width:900px; border:1px solid #ccc; margin:0 auto;border-collapse: collapse;padding: 0; box-sizing: border-box;">
	<tbody>
		<tr style="background: url('https://genemedicshealth.com/images/new_backheader.jpg');
        background-repeat: repeat-x;">
            <td colspan="2" style="padding:10px;"><img src="https://genemedicshealth.com/images/new_logo-genemedics.png" width="200" /></td>
        </tr>
        <tr style="background: url('https://genemedicshealth.com/images/new_about-bg.png'); background-repeat:no-repeat; height:250px; background-size:cover;">                    
            <td style="vertical-align:top; color:#0069C5;">
			<p style="position:relative; right:10px; margin-bottom:-15px; text-align:right; font-weight:700; font-size:25px;">
				Thank You for contacting
			</p>
			<p style="position:relative; right:10px; text-align:right; font-size: 32px; font-weight:bold;">Genemedics Health Institute!</p>

			
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<p style="font-weight: 700;color: #003f70;font-size: 22px;padding-top:5px">Hi <strong contenteditable="false" data-token="{leadfield=firstname}">{{$name}} </strong>,</p>
			<p>Please install app from here <a href="javascript:void(0)">Download link</a></p>

			<p>Use the following parameters for login:<br/><br/>

		       <span>Email:  {{$email }} </span><br/>
			    <a href="{{URL::to('auth/create_password/'.$token)}}"> click here</a> to Create your password.<br/>
			</p>
			<p>Thank you for contacting Genemedics Health Institute!<br />
			<br />
			We are leaders in hormone replacement therapy, with locations across the country, including Arizona, California, Florida, Georgia, Illinois, Michigan, Oregon, and Texas. Our founder, Dr. George Shanlikian, M.D., is a nationally recognized expert in bioidentical hormone replacement therapy and rejuvenation. Dr. Shanlikian&#39;s unique program relies on hormone monitoring and replacement, exercise, nutrition, and education to help patients feel younger and reduce their risk of the diseases of aging.<br />
			<br />
			A clinical specialist will be contacting you within 1 business day to talk about how the Genemedics Health Institute can help you.<br />
			<br />
			&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<p style="font-size: 12px; text-align:center; margin: 0 auto; height: 25px; background: #fff; padding-top: 10px; color: #000; font-weight: bold; text-transform: uppercase;">Visit us Online at <a href="https://genemedicshealth.com" style="color: #3696c2; font-size: 12px;">www.genemedicshealth.com</a></p>
			</td>
		</tr>
		<tr style="background: #1290e3;">
			<td colspan="2">
			<table style="width:90%;margin: 0 auto;">
				<tbody>
					<tr>
						<td width="77%">
						<p style="padding-top:15px;padding-bottom:15px; font-weight: normal; text-rendering: optimizeLegibility !important; -webkit-font-smoothing: antialiased !important; -moz-osx-font-smoothing: grayscale; text-transform: uppercase; color: #3b3b3b;">If you have any questions, please feel free to contact us at<br />
						<a href="mailto:md@genemedics.com" style="color: white;">md@genemedics.com</a> or by phone at <span style="color: white; text-transformation: underline;">(800) 277-4041</span></p>
						</td>
						<td width="23%"> 
                            <a href="#"><img src="https://genemedicshealth.com/images/new_footer_fafacebooksquare.png" width="30" style="border-radius:0px;" /></a>
                            <a href="#"><img src="https://genemedicshealth.com/images/new_footer_fawittersquare.png" width="30" style="border-radius:0px;" /></a>
                            <a href="#"><img src="https://genemedicshealth.com/images/new_footer_Plus.png" width="30" style="border-radius:0px;" /></a>
                            <a href="#"><img src="https://genemedicshealth.com/images/new_fagoogleplussquare.png" width="30" style="border-radius:0;" /></a>
                        </td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr style="background: #1d88d1;">
			<td colspan="2">
			<p style="font-size: 12px; font-weight: bold; color: #fff; text-rendering: optimizeLegibility !important; -webkit-font-smoothing: antialiased !important; -moz-osx-font-smoothing: grayscale; text-transform: uppercase;text-align:center;padding:10px;">Genemedicshealth.com, LLC. All rights reserved. Genemedicshealth.com</p>
			</td>
		</tr>
	</tbody>
</table>
</body>
</html>
