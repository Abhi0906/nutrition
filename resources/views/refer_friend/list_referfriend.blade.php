<div class="row block-title patient_title" ng-init="list_referfriend.referFriendList(1);">
      <div class="col-sm-6">
        &nbsp; &nbsp;&nbsp;&nbsp;<h3><strong>Referrals</strong></h3>
      </div>  
      <div class="col-sm-6 text-alignRight">
      </div>
</div>

<div class="block md_recommended_proui" >
    <div class="md_rec_food_class" id="mdrecommeded_meal_table">

      <table class="table recommeded_table">
        <thead>
          <tr class="">
          <th class="text-center bg-primary width_10">Referer</th>
            <th class="text-center bg-primary width_10">FirstName</th>
            <th class="text-center bg-primary width_10">LastName</th>
            <th class="text-center bg-primary width_15">Email</th>
            <th class="text-center bg-primary width_15">PhoneNumber</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="listreferfriend in list_referfriend.get_referfriend">
            <td data-title="FirstName" class="text-center">@{{listreferfriend.user.full_name}}</td>
             <td data-title="FirstName" class="text-center">@{{listreferfriend.first_name}}</td>
             <td data-title="LastName" class="text-center">@{{listreferfriend.last_name}}</td>
             <td data-title="Email" class="text-center" >@{{listreferfriend.email}}</td>
             <td data-title="PhoneNumber" class="text-center" >@{{listreferfriend.phone_number}}</td>
          </tr>
          <tr class="text-center" ng-if="list_referfriend.get_referfriend.length==0">
          <td colspan="8">
            No Refer a Friend yet
          </td>
        </tr>
        </tbody>

      </table>

    </div>

    <!-- END Block Content -->
</div>