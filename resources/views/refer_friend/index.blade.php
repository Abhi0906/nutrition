@extends('layout.app')
@section('content')

<div ng-controller="ReferFriendController as vm">
      <!-- Main Row -->
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
              <div ui-view></div>
            </div>
        </div>
    </div>
    
</div>

<!-- END Main Row -->
@endsection
@section('page_scripts')
{!! HTML::script("js/controllers/ReferFriendController.js") !!}
@append
@section('jquery_ready')
@append