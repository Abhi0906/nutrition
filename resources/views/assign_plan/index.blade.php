@extends('layout.app')
@section('content')

<div ng-controller="AssignPlanController as ap" ng-init="ap.gender='{{$user->gender}}';ap.user_id='{{$user->id}}'; ap.full_name='{{$user->full_name}}';ap.calendar_editable=false">
    <div ui-view></div>
</div>

@endsection

@section('page_scripts')
{!! HTML::script("js/controllers/AssignPlanController.js") !!}
{!! HTML::script("js/controllers/ViewPlanController.js") !!}
{!! HTML::script("js/controllers/DayViewPlanController.js") !!}
{!! HTML::script("js/filters/dateFormatFilter.js") !!}
{!! HTML::script("js/directives/timePicker.js") !!}
{!! HTML::script("js/directives/singleVideoImage.js") !!}

@append