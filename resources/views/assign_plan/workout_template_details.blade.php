 <div class="block full">
      <div class="block-title">
          <h2><strong ng-bind-html="vm.getWorkoutTitle(vm.workout);"></strong></h2>
          <div class="block-options pull-right">
          <time-picker ng-model="vm.workout_template_time"></time-picker>
        </div>
      </div>
      <div class="gallery container-fluid">
              <div class="row">
                    <div class="col-xs-12 col-sm-4" ng-repeat="exercise in vm.workout.exercise_lists">                     
                      
                      <single-video-image  not-editable not-deletable
                        view = "vm.viewExercise(exercise)"
                        videoimage="exercise"
                        width="250" height="150"
                        >                            
                      </single-video-image>
              </div>
              </div>
      </div>
</div>