 <div class="row block-title patient_title" ng-init="view_plan.getUserPlan();">
      <div class="col-sm-6">
        <h3><strong><span class="text-primary">@{{view_plan.meal_plan_name}}</span></strong></h3>
      </div>
      <div class="col-sm-6 text-alignRight">

      	<a href="javascript:void(0);" ng-show="view_plan.showBtnUnAssign" class="btn btn-default" data-original-title="Save" ng-click="view_plan.updateUserMealPlan()"><span><i class="fa gi gi-disk_saved"></i></span>&nbsp;Save</a>

      	<button class="btn btn-default" ng-show="view_plan.showBtnAssign" ng-click="view_plan.assignPlan(view_plan.meal_plan_details)">Assign</button>

        <button class="btn btn-default" ng-show="view_plan.showBtnUnAssign" ng-click="view_plan.unassignPlan(view_plan.meal_plan_details)">Unassign</button>
        
        <a ui-sref="list_plans" class="btn btn-default"><span>&nbsp;</span>Cancel</a>
      </div>
  </div>

 @include('common/select_complete_plan')