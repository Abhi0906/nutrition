<div class="row block-title patient_title" ng-init="meal_plan.getMealPlans();">
    <div class="col-sm-6">
      <h3><strong>Assign Plan to @{{meal_plan.full_name}} </strong></h3>
    </div>  
   <div class="col-sm-6 text-alignRight">
                   
       <a href ="/patient" class="btn btn-default"> &nbsp;Back</a>
    </div>
</div>


<div class="block md_recommended_proui" >

    <div class="md_rec_food_class">
      <table class="table recommeded_table">
        <thead>
          <tr class="">            
            <th class="text-center bg-primary width_50">Complete Plan Name</th>
            <th class="text-center bg-primary ">Created At</th>
            <th class="text-center bg-primary ">Updated At</th>
            <th class="text-center bg-primary width_10">View Plans</th>
            <th class="text-center bg-primary">ACTION</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="template in meal_plan.meal_plans | orderBy: '-id' | filter:{plan_type:'complete_plan'}">
             <td data-title="TEMPLATE NAME" class="text-left">
                @{{template.name}}                
             </td>

             <td data-title="Created At" class="text-left">
                 @{{template.created_at | custom_date_format: 'dd-MMM-yy hh:mm a'}}                
             </td>

             <td data-title="Updated At" class="text-left">
                @{{template.updated_at | custom_date_format: 'dd-MMM-yy hh:mm a'}}                
             </td>
             <td>
                <a ui-sref="view_complete_plan({id: template.id})" >View Plans</a>
             </td>
             
              <td data-title="ACTION" class="text-center">
                <div class="btn-group btn-group-xs">
                  <button class="btn btn-primary" ng-show="meal_plan.showBtnAssign[template.id]" ng-click="meal_plan.assignPlan(template)">Assign</button>

                   <button class="btn btn-danger" ng-show="meal_plan.showBtnUnAssign[template.id]" ng-click="meal_plan.unassignPlan(template)">Unassign</button>
                </div>
              </td>
          </tr>
        </tbody>
      </table>
    </div>
</div>
