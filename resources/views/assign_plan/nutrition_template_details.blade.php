 <simple-block  block-title="@{{vm.nutrition_template_name}}" >

    <div class="md_rec_food_class" id="mdrecommeded_meal_table">
      <table class="table recommeded_table">
        <thead>
          <tr class="">
            <th class="text-center bg-primary width_60">MEAL NAME</th>
            <th class="text-center bg-primary width_15">MEAL TIME</th>
            <th class="text-center bg-primary width_15">MAX CALORIE</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="recommended_meal in vm.recommended_meals">
             <td data-title="MEAL NAME" class="text-left">
                <a href="javascript:void(0)" ui-sref="meal-items({recommended_meal_id: recommended_meal.id, recommended_meal_name: recommended_meal.meal_name})">@{{recommended_meal.meal_name}}</a>
                <small class="meal_food_name" ng-if="recommended_meal.meal_foods_names.length!=0">
                  &nbsp;
                  [<span ng-repeat="food_name in recommended_meal.meal_foods_names track by $index">
                    <a class="meal_food_name" href="javascript:void(0)" title="@{{ food_name}}">@{{ food_name }}</a>
                    <span ng-if="recommended_meal.meal_foods_names.length-1!=$index">, </span> 
                  </span>]
                </small>
             </td>
             
             <td data-title="MEAL TIME" class="text-center">
               <time-picker
                 ng-init="vm.meal[$index].meal_time = recommended_meal.meal_time" 
                 ng-model="vm.recommended_meals[$index].meal_time" 
                 ></time-picker>
             </td>
             <td data-title="MAX CALORIE" class="text-center">
                @{{recommended_meal.max_calorie}}
             </td>
              
              
          </tr>
        </tbody>
      </table>
    </div>

 </simple-block>