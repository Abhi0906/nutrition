<div class="block">
  <div class="block-title">
     <h3><strong><span style="font-size:14px!important;">Choose Plans</span></strong></h3>
   </div>
   <div class="container-fluid">
      <div>
          <div class="form-group">
              <label for="body_parts">Choose Plans</label>
              <div>
                <select class="form-control"
                    ng-options="plan.id as plan.name for plan in vm.plans"
                    ng-model="vm.selected_plan"
                    ng-change="vm.getMDTemplates();" >
                </select>
              </div>
          </div>
          <div class="form-group" ng-show="vm.template_div">
              <label for="keyword">@{{vm.template_label}}</label>
              <div >       
                <select 
                      class="form-control"
                      
                      ng-model="vm.selected_template"
                      ng-change="vm.getTemplateDetails();" >
                      <option value="0">@{{vm.template_label}}</option>
                     <option ng-repeat="template in vm.templates" value="@{{template.id}}">@{{template.template_name}}</option>
                </select>
              </div>
          </div>
          <div class="form-group">
              <label for="week_days"> Week day(s)</label>
              <div >       
                <label style="margin-left:10px" 
                   for="example-inline-checkbox1" 
                   class="checkbox-inline" 
                   ng-repeat= "(key, week_day) in vm.week">
                <input type="checkbox"
                      checklist-value="week_day.dayNumber"
                      checklist-model="vm.select_days"
                      > @{{week_day.dayName}}
               </label>
              </div>
          </div>
      </div>
   </div>
</div>