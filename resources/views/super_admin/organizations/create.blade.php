@extends('layout.app')

@section('content')
<form name="createOrganizationForm" method="POST" action="/admin/organization" enctype="multipart/form-data">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="row block-title patient_title">
  <div class="col-sm-2">
    <h3><strong>Create Organization&nbsp;</strong></h3>
  </div>
  <div class="col-sm-10 text-alignRight">

     <button class="btn btn-default" type="submit">
      <i class="fa fa-angle-right"></i> Submit</button>
     <a href="/admin/organization" class="btn btn-default"> <i class="gi gi-remove_2"></i>
     &nbsp;Cancel</a>
  </div>
</div>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

  <div class="row form-horizontal form-bordered">
      <div class="col-md-6">
          <!-- Basic Form Elements Block -->
          <div class="block">
              <div class="block-title">
                <h2>
                <strong>Information</strong>
                </h2>
              </div>
              <!-- Basic Form Elements Content -->
                  <div class="form-group">
                      <label class="col-md-4 control-label" for="txt-organization-name">Organization name<span class="text-danger">&nbsp;*</span></label>
                      <div class="col-md-8">
                          <input type="text" id="txt-organization-name"
  	                          name="name"
  	                          class="form-control"
                              value="{{old('name')}}"
  	                          placeholder="Organization name">
                      </div>
                  </div>
  				        <div class="form-group">
                      <label class="col-md-4 control-label" for="txt_first_name">First name<span class="text-danger">&nbsp;*</span></label>
                      <div class="col-md-8">
                          <input type="text" id="txt_first_name"
                                            name="contact_first_name"
                                            class="form-control"
                                            value="{{old('contact_first_name')}}"
                                            placeholder="First name">
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-md-4 control-label" for="txt_last_name">Last name<span class="text-danger">&nbsp;*</span></label>
                      <div class="col-md-8">
                          <input type="text" id="txt_last_name"
                                            name="contact_last_name"
                                            class="form-control"
                                            value="{{old('contact_last_name')}}"
                                            placeholder="Last name">
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-md-4 control-label" for="txt_email">Email<span class="text-danger">&nbsp;*</span></label>
                      <div class="col-md-8">
                          <input type="text" id="txt_email"
                                            name="email"
                                            class="form-control"
                                            value="{{old('email')}}"
                                            placeholder="Email">
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-md-4 control-label" for="example-text-input">Theme Color<span class="text-danger">&nbsp;*</span></label>
                      <div class="col-md-8">
                          <div class="input-group input-colorpicker colorpicker-element">
                                  <input id="example-colorpicker2" name="theme_color" class="form-control" value="#1bbae1" type="text">
                                  <span class="input-group-addon"><i style="background-color: rgb(178, 188, 182);"></i></span>
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-md-4 control-label" for="txt_sub_domain">Sub Domain<span class="text-danger">&nbsp;*</span></label>
                      <div class="col-md-8">

                              <span class="font_12"><strong>{{$_SERVER['REQUEST_SCHEME']}}://</strong></span>
                              <input id="txt_sub_domain"
                               name="sub_domain"
                                class="form-control subdomain_input"
                                value="{{old('sub_domain')}}"
                                placeholder="Sub Domain"
                                type="text">
                              <span class="font_12"><strong>{{$_SERVER['HTTP_HOST']}}</strong></span>

                              <button class="btn btn-default btn-sm" type="button" id="btnCheck">Check Now</button>

                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-md-4 control-label" for="txt_email">Domain</label>
                      <div class="col-md-8">
                          <input type="text" id="txt_email"
                                            name="domain"
                                            class="form-control"
                                            value="{{old('domain')}}"
                                            placeholder="Domain">
                      </div>
                  </div>

              <!-- END Basic Form Elements Content -->
          </div>
          <!-- END Basic Form Elements Block -->
      </div>
      <div class="col-md-6">
        	<div class="block">
  	        <div class="block-title">
  	          <h2>
  	          <strong>Logo</strong>
  	          </h2>
  	        </div>
  	        <div class="form-group">
  	            <div class="col-md-12">
  	                <div class="dropzone" id="organization_logo">
  	                     <div class="dz-default dz-message"><span>Drop Image here to upload</span></div>
  	                     <input type="hidden" name="logo" id="hn_org_logo">
                    </div>
  	            </div>
  	        </div>
  	      </div>

  		<div class="row">
  			<div class="col-md-12">
  				<div class="block">
  					<div class="block-title">
  						<h2>
  						<strong>Contact Info</strong>
  						</h2>
  					</div>

  					<div class="form-group">
  	                    <label class="col-md-4 control-label" for="txt_mobile_num">Mobile No.<span class="text-danger">&nbsp;*</span></label>
  	                    <div class="col-md-8">
  	                        <input type="text" id="txt_mobile_num"
  	                                           name="mobile"
  	                                           class="form-control"
                                               value="{{old('mobile')}}"
  	                                           placeholder="Mobile no.">
  	                    </div>
  	               	</div>
                  	<div class="form-group">
  	                    <label class="col-md-4 control-label" for="txt_telephone">Telephone</label>
  	                    <div class="col-md-8">
  	                        <input type="text" id="txt_telephone"
  	                                           name="telephone"
                                               value="{{old('telephone')}}"
  	                                           class="form-control"
  	                                           placeholder="Telephone">
  	                    </div>
  	               	</div>
  	               	<div class="form-group">
  	                    <label class="col-md-4 control-label" for="textarea_address">Address<span class="text-danger">&nbsp;*</span></label>
  	                    <div class="col-md-8">
  	                        <textarea id="textarea_address"
  	                                  name="address"
  	                                  rows="4"
  	                                  class="form-control"
  	                                  placeholder="Address"></textarea>
  	                    </div>

                      </div>
                      <div class="form-group">
  	                    <label class="col-md-4 control-label" for="txt_postcode">Postcode</label>
  	                    <div class="col-md-8">
  	                        <input type="text" id="txt_postcode"
  	                                           name="postcode"
  	                                           class="form-control"
                                               value="{{old('postcode')}}"
  	                                           placeholder="Postcode">
  	                    </div>
  	               	</div>
  				</div>
  			</div>
  		</div>
      </div>
  </div>
</form>


@endsection

@section('jquery_ready')
    Dropzone.autoDiscover = false;
    $("div#organization_logo").dropzone({
                    url: "/api/v3/organization/upload",
                    maxFiles: 1,
                    paramName:'org_logo',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    addRemoveLinks:true,
                    uploadMultiple:false,
                    init: function() {
                        this.on("complete", function(data) {
                                       if(data.status == 'success'){
                                         var response = eval('(' + data.xhr.response + ')');
                                         $('#hn_org_logo').val(response.file_name);
                                       }

                        });
                        this.on("removedfile",function(file){
                             var file_name = file.name;

                             HTTPRequest('', 'get', '/api/v3/organization/remove/'+file_name, function(data,status,xhr){
                                if(status == 'success'){
                                    if(data.error == false){
                                     $('#hn_org_logo').val("");
                                    }
                                }
                               });

                        });
                        this.on("addedfile", function() {
                          if (this.files[1]!=null){
                            this.removeFile(this.files[0]);
                           }
                        });

                   }

    });

    $( "#btnCheck" ).click(function() {
       var sub_domain_name = $('#txt_sub_domain').val();
             $.ajax({
                url: '/api/v3/organization/checksubDomain',
                data: {
                   sub_domain: sub_domain_name,
                   "id":""
                },
                error: function() {
                   $('#info').html('<p>An error has occurred</p>');
                },
                dataType: 'jsonp',
                success: function(data) {
                   if(data.isExist){
                    $("#btnCheck").parent('div').removeClass('has-success');
                    $( "#btnCheck").parent('div').addClass('has-error');
                   }else{
                    $( "#btnCheck").parent('div').removeClass('has-error');
                    $("#btnCheck").parent('div').addClass('has-success');
                   }
                },
                type: 'POST'
             });

    });

@append