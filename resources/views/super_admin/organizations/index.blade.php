@extends('layout.app')

@section('content')

 <div class="row block-title patient_title">
      <div class="col-sm-6">
        <h3><strong>Manage Organization</strong></h3>
      </div>
      <div class="col-sm-6 text-alignRight">
        <a href="/admin/organization/create" class="btn btn-default"><span><i class="fa fa-plus"></i></span>&nbsp;New Organization</a>
      </div>
 </div>
<div class="block" ng-controller="OrganizationController as org">
	<div class="block-title">
		<div class="block-options block_option_titlehead_height">
            <div class="row padding_top_bottom">
                <div class="col-sm-4">
					<h2><strong><span style="font-size: 12px!important;">Organizations @{{org.organizations.from}} to @{{org.organizations.to}} of @{{org.organizations.total}}</span></strong></h2>
				</div>
				<div class="col-sm-8 text-right">

					<form class="form-inline">
						<div class="form-group">
							<label>Show: </label>
							<select class="form-control" ng-options="value for value in org.displayRecords"  ng-model="org.itemPerPage">
							</select>
						</div>
						<div class="form-group">

								<div class="input-group">

									<input type="text" placeholder="Search" class="form-control" ng-model="org.keyOrg">
									<span class="input-group-btn">
										<button class="btn btn-default" type="submit" ng-click="org.searchOrg();"><i class="fa fa-search"></i></button>
									</span>
								</div>
						</div>
					</form>
                </div>

			</div>
		</div>
	</div>

	<div class="row">
        <div id="no-more-tables" class="table-responsive">
	        <table class="table table-vcenter table-striped animation-fadeInQuick">
	            <thead>
	                <tr>
	                    <th class="hidden-xs cursor table_list text-primary">Organization Name</th>
	                    <th class="hidden-xs text-primary table_list">Login As</th>
	                    <th class="hidden-xs cursor text-primary table_list">Contact Name</th>
	                    <th class="hidden-xs cursor text-primary table_list">Email</th>
	                    <th class="hidden-xs cursor text-primary table_list">Sub Domain</th>
	                    <th  class="hidden-xs text-primary table_list">Mobile</th>
   	                    <th class="hidden-xs text-primary table_list cursor" >Created_at</th>
	                    <th class="hidden-xs table_list text-center text-primary">Actions</th>
	                </tr>
	            </thead>
	            <tbody>
	                <tr ng-repeat="(key, value) in org.organizations.data">
		                <td data-title="Organization Name">
		                	<a href='admin/organization/@{{value.id}}'>
                           		 <span>@{{value.name}}</span>
                        	</a>
		                </td>
		                <td data-title="Impersonate">
                       		 <a class="btn btn-primary btndesign" href="/admin/organization/impersonate/@{{value.id}}"><i class="fa"></i>&nbsp;Impersonate</a>
                    	</td>
						<td data-title="Contact Name">
		                	<span>@{{value.contact_first_name}} @{{value.contact_last_name}}</span>
                        </td>
                        <td data-title="Email">
		                	<span>@{{value.email}}</span>
                        </td>

                        <td data-title="Sub Domain">
		                	<span>@{{value.sub_domain}}</span>
                        </td>

                        <td data-title="Mobile">
		                	<span>@{{value.mobile}}</span>
                        </td>

                         <td data-title="Created_at">
		                	<span ng-bind="org.getDate(value.created_at)"></span>
                        </td>

                        <td class="text-center" data-title="Actions">
	                        <div class="btn-group btn-group-xs">
	                            <a href="/admin/organization/@{{value.id}}" data-toggle="tooltip" title="Edit" class="btn btn-default"><i class="fa fa-pencil"></i></a>
	                            <a href="javascript:void(0)" ng-click="org.deleteOrganization(value);" data-toggle="tooltip" title="Delete" class="btn btn-danger"><i class="fa fa-times"></i></a>
	                        </div>
                   		</td>

	                </tr>
	            </tbody>
	        </table>
		</div>
    </div>
</div>

@endsection
@section('page_scripts')
{!! HTML::script("js/controllers/OrganizationController.js") !!}
@endsection