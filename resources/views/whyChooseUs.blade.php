@extends('layout.master')

@section('title')
  Why Choose Us
@endsection
@section('content')
            <div class="maindiv connectivity-app-banner" >
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12 in-up animated3 hidden-xs">
                            <p class="w-c-u">Why Choose Us</p>
                            <div class="w-c-u-nav">
                                <ul>
                                    <li><a href="#" class="act-bttn active" onclick="showdiv('connectivity-i');">Connectivity</a></li>
                                    <li><a href="#" class="act-bttn" onclick="showdiv('food-log-i');">Food Log</a></li>
                                    <li><a href="#" class="act-bttn" onclick="showdiv('exercise-log-i');">Exercise Log</a></li>
                                    <li><a href="#" class="act-bttn" onclick="showdiv('reminders-i');">Reminders</a></li>
                                    <li><a href="#" class="act-bttn" onclick="showdiv('gps-i');">GPS</a></li>
                                    <li><a href="#" class="act-bttn" onclick="showdiv('goal-tracking-i');">Goal Tracking</a></li>
                                    <li><a href="#" class="act-bttn" onclick="showdiv('personallised-i');">Personalised Programs</a></li>
                                    <li><a href="#" class="act-bttn" onclick="showdiv('education-i');">Education</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 in-up animated3 hidden-sm hidden-md hidden-lg">
                            <p class="w-c-u">Why Choose Us</p>
                            <div class="col-md-12">
                                <select id="mob1" class="app-dropdown" onchange="showmob();">
                                    <option value="connectivity-i">Connectivity</option>
                                    <option value="food-log-i">Food Log</option>
                                    <option value="exercise-log-i">Exercise Log</option>
                                    <option value="reminders-i">Reminders</option>
                                    <option value="gps-i">Connectivity</option>
                                    <option value="goal-tracking-i">Food Log</option>
                                    <option value="personallised-i">Exercise Log</option>
                                    <option value="education-i">Reminders</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-7 col-sm-8 col-xs-12 pad-top-header">
                            <div class="contentdiv connectivity-i">
                                <p class="w-c-u-title">Connectivity </p>
                                <p class="w-c-u-detail div-ani1 animated3">The Genemedics® Health App is the most flexible health app available today. It connects seamlessly with virtually every smart device on the market. Use the app on its own or sync it with your Apple Watch, Fitbit, Withings, or almost any preferred wearable health device. </p>
                                <p class="padd-10 div-ani1 animated3"><a href="#" class="link-w-c-u">syncs with</a></p>
                                <div class="row title-ani animated2">
                                    <div class="col-md-3 col-sm-4 col-xs-4"><a href="#"><img src="images/watch.png" title="" alt=""></a></div>
                                    <div class="col-md-3 col-sm-4 col-xs-4"><a href="#"><img src="images/fitbit.png" title="" alt=""></a></div>
                                    <div class="col-md-3 col-sm-4 col-xs-4"><a href="#"><img src="images/Withthings.png" title="" alt=""></a></div>
                                </div>
                            </div>
                            <div class="contentdiv food-log-i dis-none">
                                <p class="w-c-u-title">Food Log with Scanner </p>
                                <p class="w-c-u-detail div-ani1 animated3">Our food database is simply the best! Scan your foods for easy upload into the Genemedics® Health App and log your calories, protein, fat, and carbohydrates. Our huge library gives you greater accuracy and control over your diet than almost any other health app.  </p>
                            </div>
                            <div class="contentdiv exercise-log-i dis-none">
                                <p class="w-c-u-title">Exercise Log</p>
                                <p class="w-c-u-detail div-ani1 animated3">Don’t have a wearable device to track your steps and calories? No problem. The Genemedics® Health App makes it easy to track your calories burned and fitness goals while integrating your fitness program into your nutrition plan.</p>
                            </div>
                            <div class="contentdiv reminders-i dis-none">
                                <p class="w-c-u-title">Reminders to Keep you on Track</p>
                                <p class="w-c-u-detail div-ani1 animated3">The Genemedics® Health App is your personal fitness assistant. Let it keep track of your supplement and medication schedule with helpful reminders so you’ll never fall off track. </p>
                                <p class="w-c-u-detail div-ani1 animated3">The app even works as a faithful workout partner, reminding you to work out and logging your progress in the gym. Your lifestyle coach, trainer, or health care practitioner can use this information to tweak and design your optimal exercise program. It’s never been easier to reach your health and fitness goals. </p>
                            </div>
                            <div class="contentdiv gps-i dis-none">
                                <p class="w-c-u-title">GPS - Track Steps & Distance</p>
                                <p class="w-c-u-detail div-ani1 animated3">Are you taking 10,000 steps a day? The Genemedics® Health App makes it easy to track your steps per day and figure out how much distance you’re covering.  </p>
                            </div>
                            <div class="contentdiv goal-tracking-i dis-none">
                                <p class="w-c-u-title">Goal Tracking</p>
                                <p class="w-c-u-detail div-ani1 animated3">Goals are an essential element to getting and staying healthy, and no health app makes it easier to track your progress. The Genemedics® Health App makes it easy for you, your physician or your lifestyle coach to set goals for blood pressure, fat loss, body fat percentage, muscle gains, fitness levels (VO2 max), resting metabolic rate, and much more. You’ll have better information than ever before.</p>
                            </div>
                            <div class="contentdiv personallised-i dis-none">
                                <p class="w-c-u-title">Your Personal Trainer – Professional Exercise Videos </p>
                                <p class="w-c-u-detail div-ani1 animated3">Our app is the only app with the comprehensive Genemedics® exercise video library built right into the app. You can access videos while you’re exercising to make sure your form and technique is perfect so you get the maximum results. Before/after pictures and a description of the exercise are also available. That’s right: the Genemedics® Health App is your new personal trainer!</p>
                            </div>
                            <div class="contentdiv education-i dis-none">
                                <p class="w-c-u-title">Customized and Monitored Programs by Medical Doctor/Lifestyle Coach </p>
                                <p class="w-c-u-detail div-ani1 animated3">The Genemedics® Health App is the ultimate tool when it’s used in conjunction with your personalized Genemedics® fitness and health plan. The app makes it possible for your lifestyle coach to closely monitor your nutrition, supplement, and exercise progress on your journey to a healthier, better you! Once-a-month meetings are generally recommended with your lifestyle coach, but you and your lifestyle coach will determine the best meeting frequency for your schedule as well as helping to keep you on track towards reaching all of your health and fitness goals.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--Food Log-->
            <!--<div class="food-log" id="food-log-i">
                <div class="container">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-6 col-sm-8 col-xs-12 pad-top-header">
                            <p class="w-c-u-title">Food Log with Scanner </p>
                            <p class="w-c-u-detail">Our food database is simply the best! Scan your foods for easy upload into the Genemedics® Health App and log your calories, protein, fat, and carbohydrates. Our huge library gives you greater accuracy and control over your diet than almost any other health app.  </p>
                        </div>
                    </div>
                </div>
            </div>-->
            <!--exercise Log-->
            <!--<div class="exercise-log" id="exercise-log-i">
                <div class="container">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-6 col-sm-8 col-xs-12 pad-top-header">
                            <p class="w-c-u-title">Exercise Log</p>
                            <p class="w-c-u-detail">Don’t have a wearable device to track your steps and calories? No problem. The Genemedics® Health App makes it easy to track your calories burned and fitness goals while integrating your fitness program into your nutrition plan.</p>
                        </div>
                    </div>
                </div>
            </div>-->
            <!--Reminders to Keep you on Track-->
            <!--<div class="reminders" id="reminders-i">
                <div class="container">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-6 col-sm-8 col-xs-12 pad-top-header">
                            <p class="w-c-u-title">Reminders to Keep you on Track</p>
                            <p class="w-c-u-detail">The Genemedics® Health App is your personal fitness assistant. Let it keep track of your supplement and medication schedule with helpful reminders so you’ll never fall off track. </p>
                            <p class="w-c-u-detail">The app even works as a faithful workout partner, reminding you to work out and logging your progress in the gym. Your lifestyle coach, trainer, or health care practitioner can use this information to tweak and design your optimal exercise program. It’s never been easier to reach your health and fitness goals. </p>
                        </div>
                    </div>
                </div>
            </div>-->
            <!--GPS - Track Steps & Distance-->
            <!--<div class="gps" id="gps-i">
                <div class="container">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-6 col-sm-8 col-xs-12 pad-top-header">
                            <p class="w-c-u-title">GPS - Track Steps & Distance</p>
                            <p class="w-c-u-detail">Are you taking 10,000 steps a day? The Genemedics® Health App makes it easy to track your steps per day and figure out how much distance you’re covering.  </p>
                        </div>
                    </div>
                </div>
            </div>-->
            <!--Goal Tracking-->
            <!--<div class="goal-tracking" id="goal-tracking-i">
                <div class="container">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-6 col-sm-8 col-xs-12 pad-top-header">
                            <p class="w-c-u-title">Goal Tracking</p>
                            <p class="w-c-u-detail">Goals are an essential element to getting and staying healthy, and no health app makes it easier to track your progress. The Genemedics® Health App makes it easy for you, your physician or your lifestyle coach to set goals for blood pressure, fat loss, body fat percentage, muscle gains, fitness levels (VO2 max), resting metabolic rate, and much more. You’ll have better information than ever before.</p>
                        </div>
                    </div>
                </div>
            </div>-->
            <!--Your Personal Trainer – Professional Exercise Videos-->
            <!--<div class="yr-personal" id="personallised-i">
                <div class="container">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-6 col-sm-8 col-xs-12 pad-top-header">
                            <p class="w-c-u-title">Your Personal Trainer – Professional Exercise Videos </p>
                            <p class="w-c-u-detail">Our app is the only app with the comprehensive Genemedics® exercise video library built right into the app. You can access videos while you’re exercising to make sure your form and technique is perfect so you get the maximum results. Before/after pictures and a description of the exercise are also available. That’s right: the Genemedics® Health App is your new personal trainer!</p>
                        </div>
                    </div>
                </div>
            </div>-->
            <!--Customized and Monitored Programs by Medical Doctor/Lifestyle Coach-->
            <!--<div class="customized" id="education-i">
                <div class="container">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-7 col-sm-8 col-xs-12 pad-top-header">
                            <p class="w-c-u-title">Customized and Monitored Programs by Medical Doctor/Lifestyle Coach </p>
                            <p class="w-c-u-detail">The Genemedics® Health App is the ultimate tool when it’s used in conjunction with your personalized Genemedics® fitness and health plan. The app makes it possible for your lifestyle coach to closely monitor your nutrition, supplement, and exercise progress on your journey to a healthier, better you! Once-a-month meetings are generally recommended with your lifestyle coach, but you and your lifestyle coach will determine the best meeting frequency for your schedule as well as helping to keep you on track towards reaching all of your health and fitness goals.</p>
                        </div>
                    </div>
                </div>
            </div>-->
            <!-- content start here -->

            <!-- contact us -->
            <div class="white-bg" id="downloadapp">
            <div class="container padd-50">
            <div class="col-md-6 col-sm-6 app_section">
            <h3 class="wireless-title">Your Virtual Health Assistant</h3>
            <div class="mob-gen"> <p class="takes">Genemedics Health app takes care of Daily Routine, Meals, Exercises and Much more...</p></div>
            <div class="smsform">
            <input name="phone" class="form-control pull-left" placeholder="Mobile number" required="" type="tel">
            <button type="submit" class="send-btn">Text me the app</button>
            </div>
            <div class="row pad30" ><a target="_blank" href="https://itunes.apple.com/in/app/genemedicshealth-health-tracker/id1061451973"><img src="images/app-store.png" alt="" title="" class="app-store app-bhn-m-l"></a></div>
            </div>

            <div class="col-md-6 col-sm-6 post">
            <img class="mob-img"src="images/mob-1-1.png" alt="" title="">
            </div>
            </div>
            </div>
            <!-- contact us -->
            <!-- contact us -->
@endsection
