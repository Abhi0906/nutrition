<div class="row block-title">
      <div class="col-sm-6">
        <h3><strong>Food</strong></h3>
      </div>
      <div class="col-sm-6 text-alignRight">
          <a ui-sref="add-food" class="btn btn-default"> <i class="fa fa-plus"></i>&nbsp;Add Food</a>
      </div>
</div>
<div>
    <!--MD Recommended Food Block -->
    <div class="block">
        <div class="block">
          <div class="block-title">
          <div>
            <form class="form-horizontal">
              <div class="form-group"> <br>
                <label class="col-sm-2 control-label title_blue" for="add_meal">Enter Food</label>
                <div class="col-sm-8 space_col_right_8">
                    <div class="input-group">
                      <input type="text" ng-model="manage_food.searchKeyFood" class="form-control">
                      <span class="input-group-addon no_padding">
                        <button type="submit" class="btn btn-info text-white" ng-click="manage_food.searchFood(1)">Search</button>
                      </span>
                    </div>
                </div>
                <div class="col-sm-2 no_col_padding">
                  <button class="btn btn-warning margin_top_10" ng-click="manage_food.clear_logFood_searchResult()">Clear</button>
                </div>
              </div>
            </form>
          </div>
          <hr/>
          <div class="block-options block_option_titlehead_height">
            <div class="row padding_top_bottom">
                <div class="col-sm-2">
                    <h2><strong><span ng-if="manage_food.total_count > 0" style="font-size: 12px!important;">Foods @{{manage_food.from}} to @{{manage_food.to}} of @{{manage_food.total_count}}</span></strong></h2>
                </div>
                <div class="col-sm-5 text-right">
                  <ul uib-pagination
                      total-items="manage_food.total_count"
                      ng-model="manage_food.current_page"
                      max-size="5"
                      class="pagination-sm margin_top_no"
                      boundary-links="false"
                      boundary-link-numbers="true"
                      rotate="false"
                      items-per-page ="manage_food.itemPerPage"
                      ng-change="manage_food.pageChanged()">
                  </ul>
                </div>
                <div class="col-sm-5 text-right">
                  <div class="form-inline">
                    <div class="form-group">
                      <label>Show: </label>
                      <select class="form-control" ng-options="value for value in manage_food.displayRecords"  ng-model="manage_food.itemPerPage" ng-change="manage_food.searchFood(1)">
                     </select>
                    </div>
                    <div class="form-group">

                    </div>
                  </div>
                </div>
            </div>
          </div>
          <div id="addrecommeded_food_table">
            <table class="table table-bordered recommeded_table manage_food_tbl">
              <thead class="bg-primary">
                <tr>
                  <th class="text-center width_35">FOOD NAME</th>
                  <th class="text-center">CALORIES</th>
                  <th class="text-center">Serving Size</th>
                  <th class="text-center">Action</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="fooddata in manage_food.foodSearchdata">
                  <td class="text-left" data-title="FOOD NAME">
                    <a ui-sref="edit-food({id: fooddata.id})" style="cursor:pointer">
                    <label class="theme_text_p_lg_md text-primary">@{{fooddata.name}}
                      <span></span>
                    </label></a><br>
                    <label class="tag_text_normal text-default">@{{fooddata.brand_name}},
                      @{{fooddata.nutrition_data.serving_quantity}} @{{fooddata.nutrition_data.serving_size_unit}}
                    </label>
                  </td>
                  <td data-title="CALORIES" class="text-center">
                    <label class="food_log_normalbold text-dark"
                     ng-bind="manage_food.recommended_food[$index].calories"
                     ng-init="manage_food.recommended_food[$index].calories = fooddata.nutrition_data.calories">
                    </label>
                  </td>
                  <td data-title="SERVING SIZE">
                    <label class="text-center text-dark"
                     ng-bind="manage_food.recommended_food[$index].serving_string.label"
                     ng-init="manage_food.recommended_food[$index].serving_string = fooddata.serving_data[0]">
                    </label>
                  </td>
                  <td data-title="LOG FOOD" class="text-center">
                    <div class="btn-group btn-group-xs">
                       <a ui-sref="edit-food({id: fooddata.id})" class="btn btn-default" ><i class="fa fa-pencil-square-o"></i></a>
                       <a ng-click="manage_food.deleteFoodDetail(fooddata)" class="btn btn-danger" ><i class="fa fa-times"></i></a>
                       <a ng-click="manage_food.viewFoodDetail(fooddata)" class="btn btn-default"><i class="fa fa-eye"></i></a>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
            <!-- <div ng-if="!manage_food.foodSearchdata.length || manage_food.foodSearchdata.length==0" class="text-center">Search food to add in recommendation</div>    -->
            <div ng-show="manage_food.search_loader"  class="search_food_loader"></div>
            <br><br><br><br>
          </div>
        </div>
        <!-- END Block Content -->
    </div>
    <!-- END of MD Recommended Food Block -->

</div>
<!-- view models -->
<div id="view_food_detail_tmplt" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
  <view-food-detail details="manage_food.food_detail"></view-food-detail>
</div>