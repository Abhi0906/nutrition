
<div class="row block-title patient_title">
      <div class="col-sm-6">
        <h3><strong>Foods</strong></h3>
      </div>  
      <div class="col-sm-6 text-alignRight">
          <button class="btn btn-default" 
                                      ng-disabled="customFoodForm.$invalid"
                                      ng-click="add_edit_food.add_new_food()">
                                      <i class="fa fa-angle-right"></i>&nbsp;@{{add_edit_food.btnLabel}}</button>
          <a ng-click="add_edit_food.gotoManageFoodPage()" class="btn btn-default"> <i class="gi gi-remove_2"></i>
                 &nbsp;Cancel</a>
      </div>
</div>

<div class="row form-horizontal form-bordered">
  <div class="col-md-12">
    <!-- Basic Form Elements Block -->
    <div class="block">
        <div class="block-title">
          <h2>
          <strong ng-bind="add_edit_food.FormLabel"></strong>
          </h2>
        </div>
        <div ng-show="add_edit_food.log_newfoodloader"  class="custom_food_loader"></div>
        <!-- Basic Form Elements Content -->
        <div class="form-group">
        <form name="customFoodForm" class="form-horizontal add_edit_food_form" ng-submit="add_edit_food.add_new_food()">
          <div class="col-sm-6">

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-food-name">Food Type<span class="text-danger">&nbsp;*</span></label>
              <div class="col-sm-7">
                  <select class="form-control" ng-model="add_edit_food.custom_food.source_name">
                    <option value="food">Food</option>
                    <option value="supplement">Supplement</option>
                  </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-food-name">Food Name<span class="text-danger">&nbsp;*</span></label>
              <div class="col-sm-7">
                  <input type="text" ng-model="add_edit_food.custom_food.food_name"
                      name="food_name"
                      id="txt-food-name"
                      class="form-control"
                      placeholder="Food Name" required>
                      <span class="text-danger" ng-show="customFoodForm.food_name.$invalid && customFoodForm.food_name.$touched">Please enter food name</span>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-food-brand">Food Brand<span class="text-danger">&nbsp;*</span></label>
              <div class="col-sm-7">
                  <input type="text" ng-model="add_edit_food.custom_food.brand_name"
                      name="brand_name"
                      id="txt-food-brand"
                      class="form-control"
                      placeholder="Food Brand" required>
                      <span class="text-danger" ng-show="customFoodForm.brand_name.$invalid && customFoodForm.brand_name.$touched">Please enter brand name</span>
              </div>
            </div>

            <div class="form-group row">
              <form  class="form-bordered">
                <label class="col-sm-4 control-label" for="txt-serving-size">Serving Size<span class="text-danger">&nbsp;*</span></label>
                <div class="col-sm-8">
                  <div class="row" ng-repeat="food in add_edit_food.serving_size">
                      <div class=""></div>
                      <div class="col-sm-5 no-padding-left">
                          <input type="text" ng-model="food.serving_quantity"
                              name="serving_quantity"
                              id="txt-serving-size"
                              class="form-control"
                              placeholder="Quantity" required>
                          <span class="text-danger" ng-show="customFoodForm.serving_quantity.$invalid && customFoodForm.serving_quantity.$touched">Please enter quantity</span>
                      </div>
                      <div class="col-sm-4 no-padding-left">
                          <input type="text" ng-model="food.serving_size_unit"
                              id="txt-serving-size"
                              name="serving_size_unit"
                              class="form-control"
                              placeholder="Unit" required>
                          <span class="text-danger" ng-show="customFoodForm.serving_size_unit.$invalid && customFoodForm.serving_size_unit.$touched">Please enter unit</span>
                      </div>
                      <button ng-if="$index == 0" class="btn btn-sm btn-primary" type="button" ng-click="add_edit_food.addNewServing();"><i class="fa fa-plus"></i></button>
                      <button ng-if="$index > 0" class="btn btn-sm btn-danger" type="button" ng-click="add_edit_food.removeServing($index);"><i class="fa fa-minus"></i></button> 
                      <div class="saperator_5"></div>
                  </div>
                </div>
              </form>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-calories">Calories<span class="text-danger">&nbsp;*</span></label>
              <div class="col-sm-7">
                  <input type="text" ng-model="add_edit_food.custom_food.calories"
                      id="txt-calories"
                      class="form-control"
                      placeholder="Calories" required>
                  <span class="text-danger" ng-show="customFoodForm.calories.$invalid && customFoodForm.calories.$touched">Please enter calories</span>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-total-fat">Total Fat</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_edit_food.custom_food.total_fat"
                      id="txt-total-fat"
                      class="form-control"
                      placeholder="Total Fat">
                  <div class="input-group-addon">g</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-saturated-fat">Saturated Fat</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_edit_food.custom_food.saturated_fat"
                      id="txt-saturated-fat"
                      class="form-control"
                      placeholder="Saturated Fat">
                  <div class="input-group-addon">g</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-trans-fat">Trans Fat</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_edit_food.custom_food.trans_fat"
                      id="txt-trans-fat"
                      class="form-control"
                      placeholder="Trans Fat">
                  <div class="input-group-addon">g</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-cholesterol">Cholesterol</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_edit_food.custom_food.cholesterol"
                      id="txt-cholesterol"
                      class="form-control"
                      placeholder="Cholesterol">
                  <div class="input-group-addon">mg</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-sodium">Sodium</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_edit_food.custom_food.sodium"
                      id="txt-sodium"
                      class="form-control"
                      placeholder="Sodium">
                  <div class="input-group-addon">mg</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-potassium">Potassium</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_edit_food.custom_food.potassium"
                      id="txt-potassium"
                      class="form-control"
                      placeholder="Potassium">
                  <div class="input-group-addon">mg</div>
              </div>
            </div>

          </div>

          <div class="col-sm-6">
            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-total-carbs">Total Carbs</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_edit_food.custom_food.total_carbs"
                      id="txt-total-carbs"
                      class="form-control"
                      placeholder="Total Carbs">
                  <div class="input-group-addon">g</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-polyunsaturated">Polyunsaturated</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_edit_food.custom_food.polyunsaturated"
                      id="txt-polyunsaturated"
                      class="form-control"
                      placeholder="Polyunsaturated">
                  <div class="input-group-addon">g</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-dietary-fiber">Dietary Fiber</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_edit_food.custom_food.dietary_fiber"
                      id="txt-dietary-fiber"
                      class="form-control"
                      placeholder="Dietary Fiber">
                  <div class="input-group-addon">g</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-monounsaturated">Monounsaturated</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_edit_food.custom_food.monounsaturated"
                      id="txt-monounsaturated"
                      class="form-control"
                      placeholder="Monounsaturated">
                  <div class="input-group-addon">g</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-sugar">Sugar</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_edit_food.custom_food.sugar"
                      id="txt-sugar"
                      class="form-control"
                      placeholder="Sugar">
                  <div class="input-group-addon">g</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-protein">Protein</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_edit_food.custom_food.protein"
                      id="txt-protein"
                      class="form-control"
                      placeholder="Protein">
                  <div class="input-group-addon">g</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-vitamin-a">Vitamin A</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_edit_food.custom_food.vitamin_a"
                      id="txt-vitamin-a"
                      class="form-control"
                      placeholder="Vitamin A">
                  <div class="input-group-addon">%</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-vitamin-c">Vitamin C</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_edit_food.custom_food.vitamin_c"
                      id="txt-vitamin-c"
                      class="form-control"
                      placeholder="Vitamin C">
                  <div class="input-group-addon">%</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-calcium">Calcium</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_edit_food.custom_food.calcium"
                      id="txt-calcium"
                      class="form-control"
                      placeholder="Calcium">
                  <div class="input-group-addon">%</div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-4 control-label" for="txt-iron">Iron</label>
              <div class="col-sm-7 input-group">
                  <input type="text" ng-model="add_edit_food.custom_food.iron"
                      id="txt-iron"
                      class="form-control"
                      placeholder="Iron">
                  <div class="input-group-addon">%</div>
              </div>
            </div>

          </div>
        </form>
        <!-- END Basic Form Elements Content -->
        </div>
    </div>
    <!-- END Basic Form Elements Block -->
  </div>
</div>