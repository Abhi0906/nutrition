@extends('layout.app')

@section('content')

<div ng-controller="ManageFoodController as vm">  
    <div ui-view></div>
</div>

@endsection

@section('page_scripts')

{!! HTML::script("js/controllers/ManageFoodController.js") !!}
{!! HTML::script("js/controllers/AddEditFoodController.js") !!}
{!! HTML::script("js/directives/viewFoodDetail.js") !!}

@append

@section('jquery_ready')

@append