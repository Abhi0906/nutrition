@extends('layout.app')
@section('content')
<div ng-controller="SocialMediaController as sm" >
   <div class="row block-title patient_title" ng-init="sm.getAllSocialMedia();">
     <div class="col-sm-6">
        &nbsp;&nbsp;&nbsp;&nbsp; 
        <h3><strong>Social Media</strong></h3>
     </div>
     <div class="col-sm-6 text-alignRight">
        <button class="btn btn-default"
                ng-disabled="SocialMediaForm.$invalid"
                ng-click="sm.updateSocialMedia()">
        <i class="fa fa-angle-right"></i> Update</button>
     </div>
   </div>
   <div class="block md_recommended_proui" >
      <div class="md_rec_food_class" id="mdrecommeded_meal_table">
      <form class="form-horizontal" name="SocialMediaForm" >
        <div ng-repeat="media in sm.socialMedia">
          <div class="form-group">
            <label class="control-label col-sm-offset-2 col-sm-2" for="url_@{{media.slug}}">@{{media.title}}
            <span class="text-danger">*</span></label>
              <div class="col-md-6">
                    <div class="input-group">
                      <input id="url_@{{media.slug}}" ng-model="media.url"  name="url_@{{media.slug}}" class="form-control" required>

                      <span ng-if="media.slug == 'fb'" class="input-group-addon"><i class="fa si si-facebook"></i></span>
                      <span ng-if="media.slug == 'Tw'" class="input-group-addon"><i class="fa si si-twitter"></i></span>
                      <span ng-if="media.slug == 'Gp'" class="input-group-addon"><i class="fa si si-google_plus"></i></span>
                      <span ng-if="media.slug == 'Yt'"class="input-group-addon"><i class="fa si si-youtube"></i></span>
                    </div>
                         <span class="text-danger" ng-show="SocialMediaForm.url_@{{media.slug}}.$invalid && SocialMediaForm.url_@{{media.slug}}.$touched">
                          You must be enter your valid Social Media URL
                         </span>
                    
               </div>
          </div>
        </div>
        <div ng-if="sm.socialMedia.length==0">
          <div class="text-center" ng-if="sm.socialMedia.length==0">
            No Social Media added yet
          </div><br>
        </div>
      </form>
      </div>
   </div>
</div>
@endsection
@section('page_scripts')
{!! HTML::script("js/controllers/SocialMediaController.js") !!}
@append
@section('jquery_ready')
@append